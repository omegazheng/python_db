set ansi_nulls, quoted_identifier on
go

create or alter function [admin_all].[fc_splitDelimiterStringToSolidGameInfoFields](
  @StringWithDelimiter VARCHAR(8000),
  @Delimiter VARCHAR(8))

  RETURNS @ItemTable TABLE(GameID VARCHAR(100), GameName varchar(100), GameCategoryID int, SubPlatformID int)

AS
  BEGIN
    declare @ErrorString varchar(1000);
    declare @MaxLen int;
    set @MaxLen = LEN(@StringWithDelimiter)

    DECLARE @StartingPosition INT;
    DECLARE @Item varchar(100);
    DECLARE @GameID VARCHAR(100), @GameName varchar(100), @GameCategoryID int, @SubPlatformID int

    IF LEN(@StringWithDelimiter) = 0 OR @StringWithDelimiter IS NULL
      RETURN;

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    if @StartingPosition = 1
      begin
        set @ErrorString = cast('Error:: @StringWithDelimiter should not begin with the delimiter' as int);
        return;
      end
    SET @GameID = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @GameName = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @Item = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @Item = ltrim(rtrim(@Item))
    SET @GameCategoryID = cast(@Item as int)
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    if @StartingPosition > 0
      begin
        set @ErrorString = cast('Error:: Expected 3 delimiters in @StringWithDelimiter but found more than 3' as int);
        return;
      end
    SET @Item = @StringWithDelimiter
    SET @Item = ltrim(rtrim(@Item))
    SET @SubPlatformID = cast(@Item as int)
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    insert into @ItemTable(GameID, GameName, GameCategoryID, SubPlatformID)
    values(@GameID, @GameName, @GameCategoryID, @SubPlatformID)

    RETURN
  END

go
