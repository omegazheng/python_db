set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_CheckSystemDatabaseAutoCreateStats()
returns varchar(max)
begin
	return (case when exists(select * from sys.databases where database_id = db_id() and is_auto_create_stats_on = 0) then 'Database auto create statistics should be turned on.' end)
end
go
