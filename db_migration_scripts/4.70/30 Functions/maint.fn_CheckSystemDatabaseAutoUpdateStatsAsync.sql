set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_CheckSystemDatabaseAutoUpdateStatsAsync()
returns varchar(max)
begin
	return (case when exists(select * from sys.databases where database_id = db_id() and is_auto_update_stats_async_on = 0) then 'Database asynchronous auto update statistics should be turned on.' end)
end
go
--select maint.fn_CheckSystemDatabaseAutoUpdateStatsAsync()