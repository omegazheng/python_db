set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_CheckSystemDatabaseAutoUpdateStats()
returns varchar(max)
begin
	return (case when exists(select * from sys.databases where database_id = db_id() and is_auto_update_stats_on = 0) then 'Database auto update statistics should be turned on.' end)
end
go
