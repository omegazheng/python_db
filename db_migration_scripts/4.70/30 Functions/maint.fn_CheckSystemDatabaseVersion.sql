
set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_CheckSystemDatabaseVersion()
returns varchar(max)
begin
	return (
				select  'Database compatibility level should be 130 or up, current is ' + cast(compatibility_level as varchar(20))
				from sys.databases 
				where name = db_name() 
					and compatibility_level < 130
			)
	
end
go

