set xact_abort, quoted_identifier, ansi_nulls on

if not exists( select * from sys.indexes i where object_id = object_id('external_mpt.USER_CONF') and name = 'IDX_external_mpt_USER_CONF_BRANDID_EMAIL')
begin
	begin transaction

	update u
		set u.email = u.email + '#' + cast(PARTYID as nvarchar)
	--select *
	from  (
			select *, row_number() over(Partition By email, brandid order by PartyID) RN
			from external_mpt.USER_CONF
			where  email is not null 
			) u
	where u.EMAIL is not null
		and RN > 1

	print N'Duplicate records updated: ' + cast(@@rowcount as varchar)   

    create unique nonclustered index IDX_external_mpt_USER_CONF_BRANDID_EMAIL ON external_mpt.USER_CONF(BRANDID, EMAIL) where EMAIL is not null
	commit
end
go

