
set xact_abort on
if (
	select count(*)
	from sys.indexes i
		inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
	where i.name = 'PK_admin_all_USER_LOGIN_LOG'
		and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
		and col_name(ic.object_id, ic.column_id) in ('ID', 'PARTYID')

	) = 1
begin
	begin transaction
	declare @SQL nvarchar(max)
	select @SQL = (select 'drop index admin_all.USER_LOGIN_LOG.'+quotename(name) + ';' from sys.indexes where object_id = object_id('admin_all.USER_LOGIN_LOG') and is_primary_key = 0 for xml path(''), type) .value('.', 'nvarchar(max)')
	exec(@SQL)
	alter table admin_all.USER_LOGIN_LOG drop constraint PK_admin_all_USER_LOGIN_LOG
	alter table admin_all.USER_LOGIN_LOG add constraint PK_admin_all_USER_LOGIN_LOG primary key clustered(PartyID, ID)
	commit
end
go
if not exists(
				select * 
				from sys.indexes i
					inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
				where i.name = 'IDX_admin_all_USER_LOGIN_LOG_ID'
					and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
				)
begin
	create unique index IDX_admin_all_USER_LOGIN_LOG_ID on admin_all.USER_LOGIN_LOG (ID)
end
go
if not exists(
				select * 
				from sys.indexes i
					inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
				where i.name = 'IDX_admin_all_USER_LOGIN_LOG_country'
					and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
				)
begin
	create index IDX_admin_all_USER_LOGIN_LOG_country ON admin_all.USER_LOGIN_LOG(country)
end
go
if not exists(
				select * 
				from sys.indexes i
					inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
				where i.name = 'IDX_admin_all_USER_LOGIN_LOG_IP_PARTYID'
					and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
				)
begin
	create index IDX_admin_all_USER_LOGIN_LOG_IP_PARTYID ON admin_all.USER_LOGIN_LOG(ip, partyid)
end
go
if not exists(
				select * 
				from sys.indexes i
					inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
				where i.name = 'IDX_ADMIN_ALL_USER_LOGIN_LOG_LOGIN_TYPE_EXECUTED_BY_IS_MOBILE_LOGIN_TIME'
					and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
				)
begin
	create index IDX_ADMIN_ALL_USER_LOGIN_LOG_LOGIN_TYPE_EXECUTED_BY_IS_MOBILE_LOGIN_TIME ON admin_all.USER_LOGIN_LOG(LOGIN_TYPE, EXECUTED_BY, IS_MOBILE, login_time) include (partyid, DEVICE_OS)
end
go

if not exists(
				select * 
				from sys.indexes i
					inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
				where i.name = 'IDX_admin_all_USER_LOGIN_LOG_LOGIN_TYPE_login_time'
					and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
				)
begin
	create index IDX_admin_all_USER_LOGIN_LOG_LOGIN_TYPE_login_time ON admin_all.USER_LOGIN_LOG(LOGIN_TYPE, login_time) include(partyid)
end
go
if not exists(
				select * 
				from sys.indexes i
					inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
				where i.name = 'IDX_admin_all_USER_LOGIN_LOG_partyid_LOGIN_TYPE_login_time'
					and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
				)
begin
	create index IDX_admin_all_USER_LOGIN_LOG_partyid_LOGIN_TYPE_login_time ON admin_all.USER_LOGIN_LOG(PartyID, LOGIN_TYPE, login_time) 
end
go
if not exists(
				select * 
				from sys.indexes i
					inner join sys.index_columns ic on ic.index_id = i.index_id and i.object_id = ic.object_id
				where i.name = 'IDX_admin_all_USER_LOGIN_LOG_SESSION_KEY_partyid'
					and i.object_id = object_id('admin_all.USER_LOGIN_LOG')
				)
begin
	create index IDX_admin_all_USER_LOGIN_LOG_SESSION_KEY_partyid ON admin_all.USER_LOGIN_LOG(SESSION_KEY, PartyID)
end
go