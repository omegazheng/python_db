SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BRAND_COUNTRY]') = object_id and name = 'MinDeductionThreshold')
    begin
        ALTER TABLE [admin_all].[BRAND_COUNTRY]
            ADD [MinDeductionThreshold] numeric(38,18) null
    end
go

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BRAND_COUNTRY]') = object_id and name = 'DeductionRatePercentage')
    begin
        ALTER TABLE [admin_all].[BRAND_COUNTRY]
            ADD [DeductionRatePercentage] numeric(38,18) null
    end
go

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BRAND_COUNTRY]') = object_id and name = 'FundType')
    begin
        ALTER TABLE [admin_all].[BRAND_COUNTRY]
            ADD [FundType] nvarchar(100) NULL
    end
go

