set xact_abort on 
begin transaction
declare @SQL nvarchar(max)
select @SQL = 'drop index admin_all.Daily_Balance_History.'+quotename(i.name)
from  sys.index_columns ic
	inner join sys.indexes i on i.object_id = ic.object_id and i.index_id = ic.index_id
where ic.object_id = object_id('admin_all.Daily_Balance_History')
	and col_name(ic.object_id, ic.column_id) = 'ID'
exec(@SQL)
if exists(select * from sys.columns where object_id = object_id('admin_all.Daily_Balance_History') and name = 'ID')
	alter table admin_all.Daily_Balance_History drop column ID
commit