
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[CryptoAddress]') AND type in (N'U'))
  BEGIN
    CREATE TABLE [admin_all].[CryptoAddress]
    (
      [ID]       [int] IDENTITY (1,1) NOT NULL,
      [PSPName]  [NVARCHAR](50)       NOT NULL,
      [Address]  [NVARCHAR](50)       NOT NULL,
      [Tag]      [NVARCHAR](50)       NULL,
      [Currency] [NVARCHAR](10)       NOT NULL,
      [PartyID]  [int]                NOT NULL,
      [DateTime] [datetime]           NOT NULL
          constraint DF_CryptoAddress_DateTime default (getdate())
    )
  END
GO


IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_CryptoAddress_PartyId]') AND parent_object_id = OBJECT_ID(N'[admin_all].[CryptoAddress]'))
  ALTER TABLE [admin_all].[CryptoAddress]  WITH CHECK ADD  CONSTRAINT [FK_admin_all_CryptoAddress_PartyId] FOREIGN KEY([PartyID])
  REFERENCES [external_mpt].[USER_CONF] ([PARTYID])
GO

