set ansi_nulls, quoted_identifier on
go

if exists(select * from sys.columns where object_id = object_id('admin_all.USER_COMMENTS'))
alter table admin_all.USER_COMMENTS alter column COMMENT varchar(max) not null

go
