create or alter procedure admin_all.usp_GetBrandCountry
(
    @BrandID int,
    @Country nvarchar(3)
)
as
begin

    select ID as id, BRANDID as brandId, COUNTRY as country, MinDeductionThreshold as minDeductionThreshold,
           DeductionRatePercentage as deductionRatePercentage, FundType as fundType
                         from admin_all.BRAND_COUNTRY
    where BRANDID = @BrandID and COUNTRY= @Country
end
go


