create or alter procedure admin_all.usp_GetCurrencyConfig
as
begin
    set nocount on
        select c.iso_code as IsoCode, c.name as Name, c.numeric_code as NumericCode,
               c.symbol as Symbol, c.is_default as IsDefault, c.symbol_code as SymbolCode, c.NUMBER_DECIMAL_POINT as NumberDecimalPoint,
               c.is_virtual as IsVirtual, c.type as Type, c.REFRESH_INTERVAL as RefreshInterval,
               c.alertThreshold as AlertThreshold, c.StopThreshold as stopThreshold, bc.EnabledCount
        from (
        select bc.iso_code as iso_code, count(bc.ISO_CODE) as EnabledCount
        from admin_all.Brand_Currency bc where bc.IS_ENABLED = 1 group by bc.iso_code
             ) bc
        join admin_all.Currency c on bc.iso_code = c.iso_code
end
go




