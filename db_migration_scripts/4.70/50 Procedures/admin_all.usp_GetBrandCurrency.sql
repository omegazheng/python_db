create or alter procedure admin_all.usp_GetBrandCurrency
(
    @BrandID int = null,
    @Currency nvarchar(12) = null,
    @IsDefault bit = null    
)
as
begin
    set nocount on
    declare @SQL nvarchar(max) = 'select ID as ID, BRANDID as BrandID, ISO_CODE as Currency, ISO_CODE as IsoCode,' +
                                 'IS_DEFAULT as IsDefault, IS_ENABLED as IsEnabled
        from admin_all.BRAND_CURRENCY where 1=1';
    declare @BrandFilter nvarchar(max) = null
    declare @CurrencyFilter nvarchar(max) = null
    declare @DefaultFilter nvarchar(max) = null
    if @BrandID is not null
        begin
        set @BrandFilter =' and BrandID = ' + CAST(@BrandID as varchar(10))
        end
    if @Currency is not null
        begin
        set @CurrencyFilter = ' and ISO_CODE = ''' + @Currency + '''';
        end
    if @IsDefault is not null
        begin
        set @DefaultFilter = ' and IS_DEFAULT = ' + CAST(@IsDefault as varchar(2))
        end
    if @BrandFilter is not null
        begin
        Set @SQL = @SQL + @BrandFilter
        end
    if @CurrencyFilter is not null
        begin
        Set @SQL = @SQL + @CurrencyFilter
        end
    if @DefaultFilter is not null
        begin
        Set @SQL = @SQL + @DefaultFilter
        end
    print @SQL
    exec sp_executesql @SQL, N'@BrandID int, @Currency nvarchar(12), @IsDefault bit', @BrandID, @Currency, @IsDefault
end
go


--exec admin_all.usp_GetBrandCurrency @Currency = 'EUR', @BrandID = 2, @IsDefault = 1

