set ansi_nulls, quoted_identifier on
go
create or alter view security.v_CMSMapping_ROLE_RESOURCE
as
select ROLE_ID, API, RolePrincipalID, oa.Operation
from security.v_CMSMapping_ROLE r
	cross join (select API, Operation  from security.OperationAPI where Method = 'ACTION') oa 
where exists(
				select *
				from security.PrincipalSecurable ps
					inner join security.SecurableOperation so on so.SecurableID = ps.SecurableID --or -r.ROLE_ID = ps.SecurableID
				where (r.RolePrincipalID = ps.PrincipalID or r.RolePrincipalID < 0 and r.RolePrincipalID = - ps.SecurableID)
					and oa.Operation = so.Operation
			)
union
select 1, API, 1, Operation
from security.OperationAPI where Method = 'ACTION'
go
--order by 1, 2

--select ROLE_ID, URL from admin_all.ROLE_RESOURCE order by 1, 2
--select * from security.v_CMSMapping_ROLE_RESOURCE 
--select * from admin_all.ROLE

--select * from security.SecurableOperation where SecurableID = 2014--

