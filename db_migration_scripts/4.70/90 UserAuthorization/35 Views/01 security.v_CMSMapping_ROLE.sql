set ansi_nulls, quoted_identifier on
go
create or alter view security.v_CMSMapping_ROLE
as
select r.ID ROLE_ID, p.PrincipalID RolePrincipalID
from admin_all.ROLE r
	inner join security.Principal p on p.Name = r.ROLE_NAME and p.PrincipalID between 500 and 599
union all 
select 1, 1
union all
select -SecurableID, -SecurableID
from security.Securable s
where exists(
				select * 
				from security.SecurableOperation so
					inner join security.OperationAPI oa on oa.Operation = so.Operation
				where so.SecurableID = s.SecurableID
					and oa.Method = 'ACTION'
			)

go



--select * from security.v_CMSMapping_ROLE