set ansi_nulls, quoted_identifier on
go
create or alter trigger security.TRI_Operation on security.Operation
for insert, update, delete
as
begin
	if @@rowcount = 0
		return
	set nocount on 
	insert into security.Audit(TableName, OldValues, NewValues, PrincipalID, SID)
	select	'Operation', 
			(select d.* for xml raw, type) OldValues,
			(select i.* for xml raw, type) NewValues,
			isnull(sc.PrincipalID, -1),
			isnull(sc.SID, 'Unknown')
	from deleted d
		full outer join inserted i on i.Operation = d.Operation
		outer apply security.fn_GetSecurityContext() sc
end
go

