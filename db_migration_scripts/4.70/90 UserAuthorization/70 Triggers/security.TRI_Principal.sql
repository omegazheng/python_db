set ansi_nulls, quoted_identifier on
go
create or alter trigger security.TRI_Principal on security.Principal
for insert, update, delete
as
begin
	if @@rowcount = 0
		return
	set nocount on 
	if exists(
				select *
				from inserted i 
					inner join deleted d on i.PrincipalID = d.PrincipalID
				where i.Type <> d.Type
			)
	begin
		throw 50000, 'Principal Type cannot be changed.', 16
	end
	insert into security.Audit(TableName, OldValues, NewValues, PrincipalID, SID)
	select	'Principal', 
			(select d.* for xml raw, type) OldValues,
			(select i.* for xml raw, type) NewValues,
			isnull(sc.PrincipalID, -1),
			isnull(sc.SID, 'Unknown')
	from deleted d
		full outer join inserted i on i.PrincipalID = d.PrincipalID
		outer apply security.fn_GetSecurityContext() sc
end
go



