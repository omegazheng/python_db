set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_STAFF_AUTH_TBL on admin_all.STAFF_AUTH_TBL
for insert, update, delete
as
begin
    if @@rowcount = 0
      return
	set nocount on
	insert into admin_all.STAFF_BRAND_TBL(STAFFID,BRANDID)
		select i.STAFFID, b.BRANDID
		from inserted i
		cross join admin.CASINO_BRAND_DEF b
		where i.SUPER_ADMIN = 1
			and not exists(select *
							from admin_all.STAFF_BRAND_TBL sb
							where sb.BRANDID = b.BRANDID
									and sb.STAFFID = i.STAFFID
							)
	update t
		set t.ACTIVE = 1, t.NON_WL_ALLOWED = 1
	from admin_all.STAFF_AUTH_TBL t
		inner join inserted s on t.STAFFID = s.STAFFID
	where s.SUPER_ADMIN = 1 and (isnull(t.ACTIVE, 0) = 0 or isnull(t.NON_WL_ALLOWED, 0) = 0)

	declare @IsSuperAdmin bit, @IsDelete bit, @SID varchar(128), @PrincipalID int, @StaffID int
	declare c cursor local static for
		select isnull(i.STAFFID, d.STAFFID), case when i.STAFFID is not null then i.SUPER_ADMIN else 0 end, case when i.STAFFID is null then 1 else 0 end
		from inserted i
			full outer join deleted d on i.STAFFID = d.STAFFID
		where i.STAFFID is null or d.STAFFID is null or i.SUPER_ADMIN <> d.SUPER_ADMIN
	open c
	fetch next from c into @StaffID, @IsSuperAdmin, @IsDelete
	while @@fetch_status = 0
	begin
		exec security.usp_SetSecurityContext @PrincipalID = 2

		select @SID = security.fn_ComposeSID('STAFF', @StaffID)
		select @PrincipalID = security.fn_GetPrincipalID(@SID)
		if @IsDelete = 1
		begin
			if @PrincipalID is not null
				exec security.usp_RemovePrincipal @PrincipalID = @PrincipalID
		end
		else
		begin
			if @PrincipalID is null
			begin
				exec security.usp_UpdatePrincipal @PrincipalType = 'STAFF', @ID = @StaffID, @PrincipalID = @PrincipalID output --, @Name = @SID
			end
		end
		
		if @IsSuperAdmin = 1
		begin
			exec security.usp_AddRoleMember 1, @PrincipalID
		end
		else
			exec security.usp_RemoveRoleMember 1, @PrincipalID 

		exec security.usp_SetSecurityContext @ID = -1
		fetch next from c into @StaffID, @IsSuperAdmin, @IsDelete
	end
	close c
	deallocate c

	insert into admin_all.AUTH_USER_ROLE(USER_ID, USER_TYPE, ROLE_ID)
		select i.STAFFID, 'STAFF', 1
		from inserted i
		where i.SUPER_ADMIN = 1
			and not exists(select *
							from admin_all.AUTH_USER_ROLE a
							where i.STAFFID = a.USER_ID
								and a.USER_TYPE = 'STAFF'
								and a.ROLE_ID = 1
							)
	
		
	
	;with x0 as
	(
		select Permission
		from (values('ENABLE_EXPORT'),('SHOW_EMAIL'),('SHOW_MOBILE'),('SHOW_PHONE')) p(Permission)
	),
	x1 as
	(
		select i.STAFFID as UserID, x0.Permission
		from x0
			cross join inserted i
		where i.SUPER_ADMIN = 1
	)
	merge admin_all.AUTH_PERMISSION t
	using x1 as s on t.USER_ID = s.UserID and t.USER_TYPE = 'STAFF' and t.PERMISSION_TYPE = s.Permission
	when matched and t.ENABLE = 0 then
		update set t.ENABLE = 1
	when not matched then
		insert (USER_ID, USER_TYPE, PERMISSION_TYPE, ENABLE)
			values(s.UserID, 'STAFF', s.Permission, 1)
	;
	exec security.usp_SetSecurityContext @ID = -1


	insert into admin_all.STAFF_ACTION_LOG(STAFFID, DATE, ACTION_TYPE, ACTION_FIELD, OLD_VALUE, NEW_VALUE, COMMENT)
		select i.STAFFID, getdate(), 'DATA_EDIT', 'FA_REQUIRED', cast(d.FA_REQUIRED as nvarchar(20)), cast(i.FA_REQUIRED as nvarchar(20)), 'App:' + isnull(app_name(), 'unknown') + '. Host:' + isnull(host_name(), 'unknnown') + '. Login:' + isnull(system_user, 'unknown') + '.'
			--' IP:' + isnull((select top 1 client_net_address from sys.dm_exec_connections where session_id = @@SPID), 'Unkown') --- remove IP since we might not have view state permission
			from inserted i
				inner join deleted d on i.STAFFID = d.STAFFID
			where isnull(i.FA_REQUIRED, 0) <> isnull(d.FA_REQUIRED, 0)
			--select * from sys.columns where name like '%required'
	insert into admin_all.STAFF_ACTION_LOG(STAFFID, DATE, ACTION_TYPE, ACTION_FIELD, OLD_VALUE, NEW_VALUE, COMMENT)
		select i.STAFFID, getdate(), 'DATA_EDIT', 'NON_WL_ALLOWED', cast(d.NON_WL_ALLOWED as nvarchar(20)), cast(i.NON_WL_ALLOWED as nvarchar(20)), 'App:' + isnull(app_name(), 'unknown') + '. Host:' + isnull(host_name(), 'unknnown') + '. Login:' + isnull(system_user, 'unknown') + '.'
			--' IP:' + isnull((select top 1 client_net_address from sys.dm_exec_connections where session_id = @@SPID), 'Unkown') --- remove IP since we might not have view state permission
			from inserted i
				inner join deleted d on i.STAFFID = d.STAFFID
			where isnull(i.NON_WL_ALLOWED, 0) <> isnull(d.NON_WL_ALLOWED, 0)

	declare @Staffs table (StaffID int primary key, SuperAdmin varchar(20), Comment varchar(max), Date datetime)
	insert into admin_all.STAFF_ACTION_LOG(STAFFID, DATE, ACTION_TYPE, ACTION_FIELD, OLD_VALUE, NEW_VALUE, COMMENT)
		output inserted. STAFFID, inserted.NEW_VALUE, inserted.COMMENT, inserted.DATE into @Staffs
		select i.STAFFID, getdate(), 'DATA_EDIT', 'SUPER_ADMIN', d.SUPER_ADMIN, i.SUPER_ADMIN, 'App:' + isnull(app_name(), 'unknown') + '. Host:' + isnull(host_name(), 'unknnown') + '. Login:' + isnull(system_user, 'unknown') + '.'
			--' IP:' + isnull((select top 1 client_net_address from sys.dm_exec_connections where session_id = @@SPID), 'Unkown') --- remove IP since we might not have view state permission
			from inserted i
				inner join deleted d on i.STAFFID = d.STAFFID
			where i.SUPER_ADMIN <> d.SUPER_ADMIN

	if exists(select * from @Staffs where SuperAdmin = 1)
	begin
		declare @EmailList nvarchar(max), @Body nvarchar(max)
		begin try
			select @EmailList =  stuff(
										(
											select ';' + EMAIL
											from admin_all.STAFF_AUTH_TBL s
											where s.SUPER_ADMIN = 1
												and not exists(select * from @Staffs ss where ss.StaffID = s.STAFFID and ss.SuperAdmin = 1)
												and EMAIL is not null
											for xml path(''), type
										).value('.', 'nvarchar(max)')
										, 1, 1, '')
			if @EmailList is not null
			begin
				select @Body = (
								select '(StaffID:' + cast(s.STAFFID as varchar(20)) + ', Staff Login Name:' + s.LOGINNAME + ', Change Date:'+convert(varchar(50), ss.Date, 120)+' ,Info:'+ss.Comment + ')
'
								from admin_all.STAFF_AUTH_TBL s
									inner join @Staffs ss on ss.StaffID = s.STAFFID and ss.SuperAdmin = 1
								for xml path(''), type
								).value('.', 'nvarchar(max)')
				exec maint.usp_SendMail @recipients = @EmailList, @subject = 'Staff is changed to super admin', @body = @Body, @body_format = 'TEXT'
				--print @EmailList
				--print @Body
			end
		end try
		begin catch
		---ignore errors.
		end catch
	end
 end
 go
 
--go
--set xact_abort on
--begin transaction
--select SUPER_ADMIN, * from admin_all.STAFF_AUTH_TBL where SUPER_ADMIN = 0 and STAFFID = 2
--update a set SUPER_ADMIN = 1 from admin_all.STAFF_AUTH_TBL a where SUPER_ADMIN = 0 and STAFFID = 2
------select * from security.Principal
------select * from security.RoleMember
--exec security.usp_RemovePrincipal 2510
--select * from security.Principal
--rollback



--begin transaction
--SELECT TOP (10) [ID]
--      ,[STAFFID]
--      ,[DATE]
--      ,[ACTION_TYPE]
--      ,[ACTION_ID]
--      ,[ACTION_FIELD]
--      ,[OLD_VALUE]
--      ,[NEW_VALUE]
--      ,[COMMENT]
--  FROM [admin_all].[STAFF_ACTION_LOG]
--  order by 1 desc
--  update admin_all.STAFF_AUTH_TBL set SUPER_ADMIN = 0 where STAFFID = 7315

--SELECT TOP (10) [ID]
--      ,[STAFFID]
--      ,[DATE]
--      ,[ACTION_TYPE]
--      ,[ACTION_ID]
--      ,[ACTION_FIELD]
--      ,[OLD_VALUE]
--      ,[NEW_VALUE]
--      ,[COMMENT]
--  FROM [admin_all].[STAFF_ACTION_LOG]
--  order by 1 desc

--  update admin_all.STAFF_AUTH_TBL set SUPER_ADMIN = 1 where STAFFID = 7315
--  SELECT TOP (10) [ID]
--      ,[STAFFID]
--      ,[DATE]
--      ,[ACTION_TYPE]
--      ,[ACTION_ID]
--      ,[ACTION_FIELD]
--      ,[OLD_VALUE]
--      ,[NEW_VALUE]
--      ,[COMMENT]
--  FROM [admin_all].[STAFF_ACTION_LOG]
--  order by 1 desc
----select * from admin_all.STAFF_AUTH_TBL where SUPER_ADMIN = 1
--rollback


--exec security.usp_SetSecurityContext @ID = -1
