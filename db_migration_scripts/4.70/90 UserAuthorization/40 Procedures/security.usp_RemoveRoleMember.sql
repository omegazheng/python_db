set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_RemoveRoleMember
(
	@RolePrincipalID int,
	@MemberPrincipalIDs varchar(max)
)
as
begin
	set nocount, xact_abort on
	--if security.fn_IsAdmin() = 0
	--	throw 50000, 'Only administrator can perform this operation', 16;
	delete rm 
	from security.RoleMember rm
	where RolePrincipalID = @RolePrincipalID 
		and exists(
					select v
					from (
							select try_cast(rtrim(ltrim(value)) as int)  as v
							from string_split(@MemberPrincipalIDs, ',')
						) a
					where v = rm.MemberPrincipalID
				)
end