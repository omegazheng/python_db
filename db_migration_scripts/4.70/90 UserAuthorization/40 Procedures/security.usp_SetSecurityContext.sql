set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_SetSecurityContext
(
	@PrincipalType varchar(30)='STAFF', --USER
	@ID sql_variant = null,
	@SID varchar(128) = null,
	@PrincipalID int = null
)
as
begin
	set nocount on
	if @ID is not null
	begin
		select @SID = security.fn_ComposeSID(@PrincipalType, @ID)
	end
	if @SID is not null
	begin
		select @PrincipalID = security.fn_GetPrincipalID(@SID)
		if @PrincipalID is not null
			goto ___Found___
	end
	if @PrincipalID is not null
	begin
		select @SID = security.fn_GetSID(@PrincipalID)
		if @SID is not null
			goto ___Found___
	end;
___Found___:
	declare @PrincipalID1 sql_variant, @SID1 sql_variant
	
	
	if @PrincipalID is not null
	begin
		select @PrincipalID1 = session_context(N'Security.PrincipalID'), @SID1 = session_context(N'Security.SID')
		exec sp_set_session_context 'Security.PrincipalID', @PrincipalID
		exec sp_set_session_context 'Security.SID', @SID

		exec sp_set_session_context 'Security.PrincipalID.Previous', @PrincipalID1
		exec sp_set_session_context 'Security.SID.Previous', @SID1
	end
	else
	begin
		select @PrincipalID1 = session_context(N'Security.PrincipalID.Previous'), @SID1 = session_context(N'Security.SID.Previous')
		exec sp_set_session_context 'Security.PrincipalID', @PrincipalID1
		exec sp_set_session_context 'Security.SID', @SID1

		exec sp_set_session_context 'Security.PrincipalID.Previous', null
		exec sp_set_session_context 'Security.SID.Previous', null
	end
end



--select * from admin_all.staff



--exec security.usp_SetSecurityContext null


--select session_context(N'Security.PrincipalID'), session_context(N'Security.SID')


--select * from security.Principal