set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_RemovePrincipal
(
	@PrincipalID int = null,
	@SID varchar(128) = null
)
as
begin
	set nocount, xact_abort on
	--if security.fn_IsAdmin() = 0
	--	throw 50000, 'Only administrator can perform this operation', 16;
	if @SID is not null
		select @PrincipalID = security.fn_GetPrincipalID(@SID)
	if not exists(select * from security.Principal where PrincipalID = @PrincipalID)
		return
	if exists(
				select * 
				from security.Principal p
					cross apply security.fn_ExtractSID(p.SID) es
					inner join admin_all.STAFF_AUTH_TBL s on s.STAFFID = cast(es.ID as int)
				where p.PrincipalID = @PrincipalID and Type = 'STAFF'
			)
	begin
	
		print 'Staff still exists in the system, operation, removing principal,  is ignored.'
		return
	end
	begin transaction
	delete security.RoleMember where MemberPrincipalID = @PrincipalID
	delete security.RoleMember where RolePrincipalID = @PrincipalID
	delete security.PrincipalSecurable where PrincipalID = @PrincipalID
	delete security.Principal where PrincipalID = @PrincipalID
	commit
end