set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_Grant
(
	@PrincipalID int,
	@SecurableIDs varchar(max),
	@OverWrite bit = 0
)
as
begin
	set nocount, xact_abort on
	;with s as 
	(
		select distinct v, sc.PrincipalID
		from (
				select try_cast(rtrim(ltrim(value)) as int)  as v
				from string_split(@SecurableIDs, ',')
			) a
			cross apply security.fn_GetSecurityContext() sc
		where v is not null
	),
	t as 
	(
		select * 
		from security.PrincipalSecurable
		where PrincipalID = @PrincipalID
	)
	merge t
	using s on t.SecurableID = s.v
	when not matched then
		insert (PrincipalID, SecurableID, CreatedBy, CreationDate, Status)
			values(@PrincipalID, s.v, s.PrincipalID, getdate(), 'GRANT')
	when not matched by source and @OverWrite = 1 then
		delete
	;
end
go

--begin transaction
--select * from security.PrincipalSecurable where PrincipalID = 1002
--exec security.usp_Grant 1002, '2,3', 0
--select * from security.PrincipalSecurable where PrincipalID = 1002
--rollback
