set ansi_nulls, quoted_identifier on
go
if object_id('security.Operation') is null 
begin
	create table security.Operation
	(
		Operation varchar(128) not null,
		Description nvarchar(max),
		constraint PK_security_Operation primary key (Operation),
	)
end
--drop table security.Operation
/*
insert into security.Operation
select NAME, DESCRIPTION from admin_all.AUTH_OPERATION
*/

