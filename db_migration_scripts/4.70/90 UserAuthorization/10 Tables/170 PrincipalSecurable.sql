set ansi_nulls, quoted_identifier on
go
if object_id('security.PrincipalSecurable') is null 
begin
	create table security.PrincipalSecurable
	(
		PrincipalID int not null,
		SecurableID int not null,
		Status varchar(10) not null ,
		CreatedBy int not null,
		CreationDate datetime not null,
		constraint PK_security_PrincipalSecurable primary key (PrincipalID, SecurableID),
		constraint PQ_security_PrincipalSecurable unique (SecurableID, PrincipalID),
		constraint FK_security_PrincipalSecurable_PrincipalID_Principal foreign key (PrincipalID) references security.Principal(PrincipalID),
		constraint FK_security_PrincipalSecurable_SecurableID_Securable foreign key (SecurableID) references security.Securable(SecurableID)
	)
end

--drop table security.PrincipalSecurable