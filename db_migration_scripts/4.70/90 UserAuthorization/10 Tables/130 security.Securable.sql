set ansi_nulls, quoted_identifier on

go
if object_id('security.Securable') is null 
begin
	create table security.Securable
	(
		SecurableID int not null,
		ParentSecurableID int null,
		Name nvarchar(128) not null,
		Type varchar(128) not null, 
		Description nvarchar(512) null,
		DisplayOrder int null,
		Reference varchar(128), 
		constraint PK_security_Securable primary key (SecurableID),
		constraint UQ_security_Securable_Name_ParentSecurableID unique(Name, ParentSecurableID),
		constraint FK_security_Securable_ParentSecurableID foreign key(ParentSecurableID) references security.Securable (SecurableID),
		index IDX_security_Securable_ParentSecurableID(ParentSecurableID)
	)
end
go
--drop table security.Securable
/*
insert into security.Securable(SecurableID, ParentSecurableID, Name, Type, Description, DisplayOrder)
select r.id, r.ParentID, r.NAME, 'API', r.DESCRIPTION, r.DisplayOrder--, ro.OPERATION_NAME
from admin_all.AUTH_ROLE r
	--left join admin_all.AUTH_ROLE_OPERATION ro on ro.ROLE_ID = r.ID

	update security.Securable set Name = 'API', Description = 'API Root' where SecurableID = 1

	insert into security.Securable(SecurableID, ParentSecurableID, Name, Type, Description, DisplayOrder)
		select 10000, null, 'Data', 'DATA','Data Root', 0

	insert into security.Securable(SecurableID, ParentSecurableID, Name, Type, Description, DisplayOrder)
		select 10001, 10000, 'SHOW_PHONE', 'DATA','Show user''s phone number', 0
	insert into security.Securable(SecurableID, ParentSecurableID, Name, Type, Description, DisplayOrder)
		select 10002, 10000, 'SHOW_EMAIL', 'DATA','Show user''s email', 0
	insert into security.Securable(SecurableID, ParentSecurableID, Name, Type, Description, DisplayOrder)
		select 10003, 10000, 'SHOW_MOBILE', 'DATA','Show user''s mobile phone', 0
	insert into security.Securable(SecurableID, ParentSecurableID, Name, Type, Description, DisplayOrder)
		select 10004, 10000, 'ENABLE_EXPORT', 'DATA','Can export user''s information', 0
	select * from security.Securable
	*/

