set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_IsAdmin()
returns bit
as
begin
	if exists(
				select * 
				from security.fn_GetSecurityContext()  sc
					cross apply security.fn_GetPrincipalRole(sc.PrincipalID) a 
				where a.RolePrincipalID = 1)
		return 1
	return 0
end