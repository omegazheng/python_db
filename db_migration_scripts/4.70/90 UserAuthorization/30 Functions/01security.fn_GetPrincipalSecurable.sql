set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_GetPrincipalSecurable
(
	@PrincipalID int
)
returns table
as
return 
(
	select ps.SecurableID
	from security.PrincipalSecurable ps
		cross apply security.fn_GetPrincipalRole(@PrincipalID)
)