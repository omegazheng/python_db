set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_GetSecurityContext()
returns table
as
return
(
	select cast(session_context(N'Security.PrincipalID') as int) as PrincipalID, cast(session_context(N'Security.SID') as varchar(128)) as SID
)