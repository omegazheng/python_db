set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_GetSecurableByPrincipal(@PrincipalID int, @ShowOperation bit)
returns table
as
return 
(
with x0 as
(
	select RolePrincipalID, MemberPrincipalID
	from security.fn_GetPrincipalRole(@PrincipalID)
),
x1 as
(
	select SecurableID
	from security.PrincipalSecurable 
	where PrincipalID = @PrincipalID
),
x2 as
(
	select	s.SecurableID, s.ParentSecurableID, cast('/'+ cast(s.SecurableID as varchar(max))  + '/'  as varchar(max)) Path, 1 as Level, s.Name, s.Description, s.DisplayOrder, s.Reference, s.Type,
			cast(case when exists(select * from x1 where s.SecurableID = x1.SecurableID) then 1 else 0 end as bit) IsDirectGranted,
			stuff((
						select ','+ cast(RolePrincipalID as varchar(20)) 
						from (
								select x0.RolePrincipalID
								from x0 
								where exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID) or x0.RolePrincipalID = 1 
								union 
								select @PrincipalID 
								where @PrincipalID = 1 
							) x
						order by RolePrincipalID for xml path('')), 1, 1, '') RoleDirectlyGranted,
			0 as IsInherited,
			',' + stuff((select ','+ cast(x0.RolePrincipalID as varchar(20)) from x0 where exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID) or x0.RolePrincipalID = 1 order by RolePrincipalID for xml path('')), 1, 1, '') PermissionGrantedByRole
	from security.Securable s
	where s.ParentSecurableID is null
	union all
	select	s.SecurableID, s.ParentSecurableID, x2.Path + cast(cast(s.SecurableID as varchar(max))  + '/' as varchar(max))  Path, x2.Level + 1 as Level, s.Name, s.Description, s.DisplayOrder, s.Reference, s.Type,
			cast(case when exists(select * from x1 where s.SecurableID = x1.SecurableID) then 1 else 0 end as bit) IsDirectGranted,
			stuff((select ','+ cast(x0.RolePrincipalID as varchar(20)) from x0 where exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID) order by RolePrincipalID for xml path('')), 1, 1, '') RoleDirectlyGranted,
			case when x2.IsInherited = 1 or x2.IsDirectGranted = 1 or x2.RoleDirectlyGranted is not null then 1 else 0 end as IsInherited,
			nullif(isnull(x2.PermissionGrantedByRole, '') + isnull(',' + stuff((select ','+ cast(x0.RolePrincipalID as varchar(20)) from x0 where not exists(select * from string_split(x2.PermissionGrantedByRole, ',') ss where ss.value = cast(x0.RolePrincipalID as varchar(10)))  and exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID) order by RolePrincipalID for xml path('')), 1, 1, ''), ''), '')
	from security.Securable s
		inner join x2 on x2.SecurableID = s.ParentSecurableID
)
select	x2.SecurableID, x2.ParentSecurableID, x2.Path, x2.Level, x2.Name, 
		x2.Description, x2.DisplayOrder, x2.Reference, x2.Type,
		x2.IsDirectGranted, x2.RoleDirectlyGranted, x2.IsInherited,
		stuff(x2.PermissionGrantedByRole, 1, 1, '') PermissionGrantedByRole,
		case when x2.IsInherited = 1 or x2.IsDirectGranted = 1 or x2.RoleDirectlyGranted is not null then 1 else 0 end HasPermission,
		case when isnull(@ShowOperation, 0) = 1 and (x2.IsInherited = 1 or x2.IsDirectGranted = 1 or x2.RoleDirectlyGranted is not null) then
				(
							(
								select oa.Operation ,oa.Method, oa.API
								from security.OperationAPI oa
								where exists(select * from security.SecurableOperation so where oa.Operation = so.Operation and so.SecurableID = x2.SecurableID)
								for json auto
							)
					
				) 
		end Operation
from x2

)
go
--select * from security.fn_GetSecurableByPrincipal(6180,0)
--select  * from  security.fn_GetSecurableByPrincipal(1014, 1)


/*




begin transaction

set identity_insert security.Principal on
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(21, 'John Test 1', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(22, 'John Test 2', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(23, 'John Test 3', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(24, 'John Test 4', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(25, 'John Test 5', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(26, 'John Test 6', 'ROLE', -1, getdate(), -1, getdate())
set identity_insert security.Principal off

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(23, 109, 'Grant', -1, getdate())

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(23, 122, 'Grant', -1, getdate())

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(24, 131, 'Grant', -1, getdate())

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(22, 50, 'Grant', -1, getdate())


	insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(22, 116, 'Grant', -1, getdate())

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(25, 116, 'Grant', -1, getdate())

insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
			values(24, 23, -1, getdate())
insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
			values(25, 24, -1, getdate())

insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
			values(22, 23, -1, getdate())


--insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
--			values(1, 25, -1, getdate())


select * from security.fn_GetSecurableByPrincipal(23, 1) order by path

rollback
*/
