set ansi_nulls,quoted_identifier on
go
create or alter procedure admin_all.usp_UpdateGameBonusBucketWhenReleaseBonus
(
	@BonusID int
)
as
begin
	set nocount on	 
	update admin_all.GAME_BONUS_BUCKET 
		set RELEASED_BONUS_WINNINGS = RELEASED_BONUS_WINNINGS + PLAYABLE_BONUS_WINNINGS, 
			RELEASED_BONUS = RELEASED_BONUS + PLAYABLE_BONUS,
			MAX_RELEASED_BONUS = MAX_RELEASED_BONUS + MAX_PLAYABLE_BONUS, 
			PLAYABLE_BONUS_WINNINGS =0,
			PLAYABLE_BONUS = 0, 
			MAX_PLAYABLE_BONUS = 0
	where BONUS_ID = @BonusID
end