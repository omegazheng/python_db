set ansi_nulls, quoted_identifier on
go
if not exists(select * from sys.columns where object_id = object_id('admin_all.BONUS_PLAN') and name = 'MinBalanceThreshold')
	alter table admin_all.BONUS_PLAN add MinBalanceThreshold numeric(38, 18)

if not exists(select * from sys.columns where object_id = object_id('admin_all.BONUS_PLAN') and name = 'MinBalanceAction')
	alter table admin_all.BONUS_PLAN add MinBalanceAction varchar(20)
go
if not exists(select * from sys.check_constraints where parent_object_id  = object_id('admin_all.BONUS_PLAN') and name = 'CK_admin_all_BONUS_PLAN_MinBalanceAction: Must be NONE, EXPIRE and RELEASE')
	alter table admin_all.BONUS_PLAN add constraint [CK_admin_all_BONUS_PLAN_MinBalanceAction: Must be NONE, EXPIRE and RELEASE] check  (isnull(MinBalanceAction, 'NONE') in ('NONE', 'EXPIRE','RELEASE'))


	--alter table admin_all.BONUS_PLAN drop constraint [CK_admin_all_BONUS_PLAN_MinBalanceAction: Must be NONE, EXPIRE and RELEASE]
	--alter table admin_all.BONUS_PLAN drop column MinBalanceThreshold, MinBalanceAction

	--sp_help 'admin_all.BONUS_PLAN'