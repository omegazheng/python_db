set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_BaseReleaseBonus
(
	@BonusID int,
	@IsExternalBonus bit = null,
	@PlatformID int,
	@AccountTranID bigint = null output
)
as

begin

  set nocount, xact_abort on

--   truncate table logs.releaseBonus
--   insert into logs.releaseBonus(bonusID, isExternalBonus, platformID)
--   values (@BonusID, @IsExternalBonus, @PlatformID)

  declare @PartyID int, @BonusPlanID int, @PaymentID int, @Status varchar(25),
  @BonusAmount numeric(38,18), @PlayableBonusWinnings numeric(38, 18), @PlayableBonus numeric(38,18)

  declare @IsPlayable bit

  begin transaction
  select @PartyID = partyid, @BonusPlanID = BONUS_PLAN_ID, @PaymentID = payment_id,
  @Status = status, @BonusAmount = amount, @PlayableBonusWinnings = PLAYABLE_BONUS_WINNINGS, @PlayableBonus = PLAYABLE_BONUS
  from admin_all.Bonus where id = @BonusID

  if @Status = 'RELEASED'
    return

  declare @AccountID int
  select @AccountID = id from admin_all.ACCOUNT where PARTYID = @PartyID
  exec admin_all.usp_LockAccountForBalanceUpdate @AccountID = @AccountID
--   declare @AccountTranID bigint = null
  select @IsPlayable = IS_PLAYABLE from admin_all.BONUS_PLAN where ID = @BonusPlanID
  if @IsPlayable = 1
    begin
      declare @ReleasedBonusAmount numeric(38,18), @ReleasedBonusWinningsAmount numeric(38, 18), @MaxReleaseLimitAmount numeric(38, 18)
      exec admin_all.usp_CalculateReleaseBonusAmount @BonusID = @BonusID,
      @ReleasedBonusAmount = @ReleasedBonusAmount output, @ReleasedBonusWinningsAmount = @ReleasedBonusWinningsAmount output,
      @MaxReleaseLimitAmount = @MaxReleaseLimitAmount output

      UPDATE admin_all.bonus SET
         amount_wagered = WAGER_REQUIREMENT, status = 'RELEASED', release_date = getdate(),
         released_bonus = released_bonus + @ReleasedBonusAmount, released_bonus_amount = released_bonus_amount + @ReleasedBonusAmount,
         released_bonus_winnings = released_bonus_winnings + @ReleasedBonusWinningsAmount, released_bonus_winnings_amount = released_bonus_winnings_amount + @ReleasedBonusWinningsAmount,
         playable_bonus = 0, playable_bonus_winnings = 0
      WHERE id = @BonusID;

      exec admin_all.usp_UpdateGameBonusBucketWhenReleaseBonus @BonusID = @BonusID

--       print 'test1'

      declare @ReleasedBonusIncrement numeric(38, 18), @PlayableBonusDecrement numeric(38, 18)
      select @ReleasedBonusIncrement = @ReleasedBonusAmount + @ReleasedBonusWinningsAmount
      select @PlayableBonusDecrement = @PlayableBonus + @PlayableBonusWinnings

      declare @AccountPlayableBonus numeric(38, 18)
      select @AccountID = ID, @AccountPlayableBonus = PLAYABLE_BONUS from admin_all.ACCOUNT where PARTYID = @PartyID
      select @PlayableBonusDecrement =
      case
        when @AccountPlayableBonus <= @PlayableBonusDecrement then @AccountPlayableBonus
        else @PlayableBonusDecrement
      end
      select @PlayableBonusDecrement = -@PlayableBonusDecrement

--       print 'test2'

      if @IsExternalBonus is not null and @IsExternalBonus = 1
        select @PlayableBonusDecrement = 0;

      declare @Reference varchar(100) = null
      declare @MaxReleaseEnabled bit
      select @MaxReleaseEnabled = MAX_RELEASE_ENABLED from admin_all.BONUS_PLAN where id = @BonusPlanID
      if @MaxReleaseEnabled is not null and @MaxReleaseEnabled = 1 and @PlayableBonus + @PlayableBonusWinnings > @MaxReleaseLimitAmount
        select @Reference = 'Remaining playable bonus amount of ' + cast(@PlayableBonus + @PlayableBonusWinnings as varchar(100)) + ' exceeds max release amount';

--       print 'test3'

      exec admin_all.usp_BaseUpdateAccountAndInsertAccountTran
          @PartyID = @PartyID, @AmountReal = 0, @AmountReleasedBonus = @ReleasedBonusIncrement, @AmountPlayableBonus = @PlayableBonusDecrement, @AmountRawLoyalty = 0,
          @TranType = 'BONUS_REL', @PlatformID = @PlatformID, @PlatformTranID = null, @GameTranID = null, @GameID = null,
          @PaymentID = null, @RollbackTranID = null, @Reference = @Reference,
          @MachineID = null, @AccountTranID = @AccountTranID output

--       print 'test4'
    end
  else
    begin
--       print 'test5'
      UPDATE admin_all.bonus SET
         amount_wagered= WAGER_REQUIREMENT, status = 'RELEASED', release_date = getdate(),
         released_bonus = AMOUNT, released_bonus_amount = AMOUNT, amount = AMOUNT
      WHERE id = @BonusID
      exec admin_all.usp_BaseUpdateAccountAndInsertAccountTran
          @PartyID = @PartyID, @AmountReal = 0, @AmountReleasedBonus = @BonusAmount, @AmountPlayableBonus = 0, @AmountRawLoyalty = 0,
          @TranType = 'BONUS_REL', @PlatformID = @PlatformID, @PlatformTranID = null, @GameTranID = null, @GameID = null,
          @PaymentID = null, @RollbackTranID = null, @Reference = null,
          @MachineID = null, @AccountTranID = @AccountTranID output
    end

  commit
  select * from admin_all.ACCOUNT_TRAN where id = @AccountTranID

end


