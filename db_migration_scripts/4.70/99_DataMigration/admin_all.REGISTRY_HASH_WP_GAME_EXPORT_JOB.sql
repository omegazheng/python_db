        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'portalv3.exportGameListJob')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('portalv3.exportGameListJob', 'false');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'PORTALV3_BACKEND_USERNAME')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('PORTALV3_BACKEND_USERNAME', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'PORTALV3_BACKEND_PASSWORD')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('PORTALV3_BACKEND_PASSWORD', 'TBD');
            END
        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'portalv3.backendURL')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('portalv3.backendURL', 'TBD');
            END
            if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'portalv3.export.enabled')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('portalv3.export.enabled', 'false');
            END

