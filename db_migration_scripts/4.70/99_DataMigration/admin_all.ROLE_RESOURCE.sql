begin
 if not exists(select *
               from admin_all.ROLE_RESOURCE
               where URL = '/j/role/StaffRoleRedirect.action')
   BEGIN
       insert into admin_all.ROLE_RESOURCE (ROLE_ID, URL)
       select ROLE_ID, '/j/role/StaffRoleRedirect.action' from admin_all.ROLE_RESOURCE
       WHERE URL = '/j/role/StaffMemberRole.action'
       and ROLE_ID not in
        (SELECT ROLE_ID from admin_all.ROLE_RESOURCE where  URL = '/j/role/StaffRoleRedirect.action')
   END

 if not exists(select *
               from admin_all.ROLE_RESOURCE
               where URL = '/j/OmegaCashierCORE.action')
   BEGIN
       insert into admin_all.ROLE_RESOURCE (ROLE_ID, URL)
       select ROLE_ID, '/j/OmegaCashierCORE.action' from admin_all.ROLE_RESOURCE
       WHERE URL = '/j/cashier/PlayerSearch.action'
       and ROLE_ID not in
        (SELECT ROLE_ID from admin_all.ROLE_RESOURCE where  URL = '/j/OmegaCashierCORE.action')
   END

 if not exists(select *
               from admin_all.RESOURCE
               where URL = '/j/role/StaffRoleRedirect.action')
   BEGIN
     insert into admin_all.RESOURCE (OPERATION, URL)
     VALUES ('AddUser', '/j/role/StaffRoleRedirect.action');
     insert into admin_all.RESOURCE (OPERATION, URL)
     VALUES ('UserListView', '/j/role/StaffRoleRedirect.action');
   END

 if not exists(select *
               from admin_all.RESOURCE
               where URL = '/j/OmegaCashierCORE.action')
   BEGIN
     insert into admin_all.RESOURCE (OPERATION, URL)
     VALUES ('Cashier', '/j/OmegaCashierCORE.action');
   END
end

