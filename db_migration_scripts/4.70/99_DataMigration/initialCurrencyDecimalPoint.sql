-- Make sure the NUMBER_DECIMAL_POINT is not null
update admin_all.CURRENCY set NUMBER_DECIMAL_POINT = 2 where NUMBER_DECIMAL_POINT is null
go
