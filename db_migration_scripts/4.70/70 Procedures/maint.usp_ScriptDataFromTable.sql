
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or alter procedure maint.usp_ScriptDataFromTable
(
	@TableName sysname,
	@ForceIdentityColumnInsert bit = 1,
	@ForceMatchedDataUpdate bit = 1,
	@PrimaryKeys varchar(max) = null,
	@ExcludedColumns varchar(max) = null
)
as
begin
	set nocount on
	/*
	select schema_name(t.schema_id)+'.'+quotename(t.name), p.rows
	from sys.partitions	p
		inner join sys.tables t on p.object_id = t.object_id
	where p.rows >0

	*/
	declare @Ret nvarchar(max) = '', @SQL nvarchar(max)

	select *, quotename(schemaName) + '.' + quotename(ObjectName) as FullName
		into #Schema 
	from maint.fn_GetLocalTableSchema(@TableName) 

	if @PrimaryKeys is not null
	begin
		update #Schema set PrimaryKeyOrder = 0 where PrimaryKeyOrder <>0
		update #Schema 
			set PrimaryKeyOrder = 1 
		where ColumnName in (
								select rtrim(ltrim(item)) 
								from admin_all.fc_splitDelimiterString(@PrimaryKeys, ',')
							) 
	end
	delete #Schema where IsTimestamp = 1 or IsComputed = 1
	delete #Schema 
	where ColumnName in (
								select rtrim(ltrim(item)) 
								from admin_all.fc_splitDelimiterString(@ExcludedColumns, ',')
							) 
		and PrimaryKeyOrder = 0

	if @ForceIdentityColumnInsert = 0
	begin
		delete #Schema where IsIdentity = 1
	end
	if not exists(select * from #Schema where PrimaryKeyOrder >0)
	begin
		raiserror('No priamry key found', 16, 1)
		return
	end

	create table #Data(ID int identity(1,1) primary key, Data nvarchar(max))

	insert into #Data(Data) values('if object_id(''tempdb..#1'') is not null drop table #1')
	insert into #Data(Data) values('go')

	insert into #Data(Data) values('select top 0 * into #1 from ' + @TableName )
	declare @HasIdentity bit = 0
	if exists(select * from #Schema where IsIdentity = 1)
		select @HasIdentity = 1
	if @HasIdentity = 1
		insert into #Data(Data) values('set identity_insert #1 on;' )

	select @SQL = 'select  ''insert into #1 ('+stuff((select ','+ quotename(columnName) from #Schema order by ColumnID for xml path('')), 1, 1,'')+') values ('''+stuff((
										select '+'',''+'+
											
													case 
														when BaseType in ('image', 'binary', 'varbinary', 'timestamp'/*, 'Hierarchyid'*/) then 'case when '+quotename(ColumnName)+' is null then ''null'' else convert(varchar(max),cast('+quotename(ColumnName)+' as varbinary(max)), 1) end'
														when BaseType in ('tinyint', 'smallint', 'int', 'real', 'float', 'bit', 'decimal', 'numeric', 'bigint', 'smallmoney', 'money') then 'case when '+quotename(ColumnName)+' is null then ''null'' else cast('+quotename(ColumnName)+' as varchar(100)) end'
														when BaseType in ('date', 'time', 'datetime2', 'datetimeoffset', 'smalldatetime', 'datetime') then  'case when '+quotename(ColumnName)+' is null then ''null'' else ''N''''''+replace(convert(nvarchar(max) , '+quotename(ColumnName)+', 120), '''''''', '''''''''''')+'''''''' end'
														else 'case when '+quotename(ColumnName)+' is null then ''null'' else ''N''''''+replace(convert(nvarchar(max) , '+quotename(ColumnName)+'), '''''''', '''''''''''')+'''''''' end'
													end 
													
										from #Schema 
										order by ColumnID 
										asc for xml path(''), type
										).value('.', 'nvarchar(max)'),1,4,'') 
									+ '+'');''
from ' + @TableName + '
order by ' + stuff((select ',' + quotename(ColumnName) from #Schema where PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'nvarchar(max)'),1,1,'') 
	insert into #Data(Data)
		exec(@SQL)


	if @HasIdentity = 1
		insert into #Data(Data) values('set identity_insert #1 off;' )
	

	select @Ret = '	--'+@TableName+'
'
	if @HasIdentity = 1 
	begin
		select top 1 @Ret = @Ret +'	set identity_insert ' + FullName  + ' on;
'
		from #Schema
	end
	--select * from #data
	select @Ret = @Ret + '	;with s as 
	(
		select '+stuff((select ','+quotename(ColumnName)  from #Schema order by ColumnID asc for xml path(''), type).value('.', 'varchar(max)'),1,1,'') + '
		from #1
	)
	merge '+@TableName+' t
	using s on '+stuff((select 'and s.'+ quotename(columnName)+'= t.'+ quotename(columnName) from #Schema where PrimaryKeyOrder > 0 order by ColumnID for xml path('')), 1, 4,'')+'
	when not matched then
		insert ('+stuff((select ','+ quotename(columnName) from #Schema order by ColumnID for xml path('')), 1, 1,'')+')
		values('+stuff((select ',s.'+ quotename(columnName) from #Schema order by ColumnID for xml path('')), 1, 1,'')+')
	'+case when @ForceMatchedDataUpdate = 1 and exists(select * from #Schema where PrimaryKeyOrder = 0) then 
		'when matched and ('+stuff((select ' or s.'+ quotename(columnName)+' is null and t.'+ quotename(columnName)+' is not null or s.'+ quotename(columnName)+' is not null and t.'+ quotename(columnName)+' is null or s.'+ quotename(columnName)+' <> t.'+ quotename(columnName) from #Schema where PrimaryKeyOrder = 0 order by ColumnID for xml path(''), type).value('.','nvarchar(max)'), 1, 4,'')+') then 
		update set '+stuff((select ', t.'+ quotename(columnName)+'= s.'+ quotename(columnName) from #Schema where PrimaryKeyOrder = 0 order by ColumnID for xml path('')), 1, 1,'')
	else ''
	end+'
	;
'

	if @HasIdentity = 1 
	begin
		select top 1 @Ret = @Ret +	'	set identity_insert ' + FullName  + ' off;
'
		from #Schema
	end
	insert into #Data(Data) values(@Ret)
	insert into #Data(Data) values('go')
	select * from #Data order by ID
end
go

--exec maint.usp_ScriptDataFromTable 'external_mpt.user_conf'
