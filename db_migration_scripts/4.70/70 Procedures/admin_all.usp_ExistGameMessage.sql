set quoted_identifier, ansi_nulls on 
go
create or alter procedure admin_all.usp_ExistGameMessage
(
	@PartyID int
)
as
begin
	set nocount on
	select case when exists(select * from admin_all.GAME_MESSAGE where PARTYID = @PartyID) then 1 else 0 end  as Exist
end
