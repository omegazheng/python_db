set ansi_nulls, quoted_identifier on
go
-- use @partyid for player with one currency only
-- use @partyIds for player with multi-currency
-- DO NOT INPUT BOTH @partyid and @partyIds
create or alter procedure [admin_all].[usp_GetPlayerGamePlaySummary]
  (
  @partyId int = null,
  @startDate DATETIME,
  @endDate DATETIME,
  @partyIds VARCHAR(1000) = null
  )
AS
  BEGIN

    SET DATEFIRST 1
    DECLARE @partyIdLocal INT
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @partyIdLocal = @partyId
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      platform,
      game_name,
      sum(game_count)            as game_count,
      sum(bet_count)             as bet_count,
      sum(handle_real)           as handle_real,
      sum(HANDLE_RELEASED_BONUS) as handle_released_bonus,
      sum(HANDLE_PLAYABLE_BONUS) as handle_playable_bonus,
      sum(pnl_real)              as pnl_real,
      sum(PNL_RELEASED_BONUS)    as pnl_released_bonus,
      sum(PNL_PLAYABLE_BONUS)    as pnl_playable_bonus,
      currency
    FROM
      dw_game_player_daily
    WHERE
      summary_date >= @startDateLocal
      AND summary_date < @endDateLocal
      and (@partyid is null or PARTY_ID = @partyid)
      and (@partyIds is null or PARTY_ID in (SELECT * FROM [admin_all].fc_splitDelimiterString(@partyIds, ',')))
    GROUP BY
      platform, game_name, currency
  END

--exec admin_all.[usp_GetPlayerGamePlaySummary] @partyId = 91429948, @startDate = '2018-01-15', @endDate = '2018-08-15'

go
