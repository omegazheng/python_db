create or alter procedure admin_all.usp_GetAggregateCreditCardCountByPrimaryPartyIdStartDateTypeAndStatus
(
    @BrandId int,
    @PrimaryPartyID int,
    @StartDate datetime,
    @PaymentType varchar(25),
    @PaymentStatus varchar(25)
)
as
begin
    set nocount on

    select count(*) from
        admin_all.payment payment with(NOLOCK), admin_all.account account with(NOLOCK),
        admin_all.payment_method_brand method with(NOLOCK), external_mpt.UserAssociatedAccount uaa with(NOLOCK)
    where
        account.partyid = uaa.associatedpartyid and payment.account_id = account.id  and
        payment.method = method.payment_method  and method.brandid = @BrandId and method.is_creditcard = 1 and
        uaa.partyid = @PrimaryPartyID and payment.request_date >= @StartDate and
        payment.type = @PaymentType and payment.status = @PaymentStatus

end
go