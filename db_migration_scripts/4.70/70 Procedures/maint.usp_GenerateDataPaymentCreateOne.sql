set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_GenerateDataPaymentCreateOne
(
	@PartyID int,
	@PaymentType varchar(25),
	@PaymentStatus varchar(25),
	@PaymentMethod varchar(60),
	@Amount numeric(38,18),
	@Date datetime = null
)
as
begin
	set nocount, xact_abort on
	declare @PaymentID int, @AccountID int, @AccountTranID bigint, @NAmount numeric(38, 18), @Info varchar(100) = 'Test Data ' + convert(varchar(20), getdate(), 120)
	select @AccountID = ID, @Date = isnull(@Date, getdate()), @NAmount = -@Amount
	from admin_all.ACCOUNT 
	where PARTYID = @PartyID;
	begin transaction
	insert into admin_all.PAYMENT (
									TYPE, METHOD, REF_NUMBER, IP_ADDRESS, REQUEST_DATE, 
									PROCESS_DATE, STATUS, AMOUNT, ACCOUNT_ID, MISC, 
									INFO, AMOUNT_REAL, AMOUNT_RELEASED_BONUS, REQ_BONUS_PLAN_ID, FEE, 
									WORLD_PAY_CARD_ID, MANUAL_BANK_FIELDS, MANUAL_BANK_AGENT_ID, LAST_UPDATED_DATE, REJECTED_REASON, 
									IS_CONVERTED, CONV_RATE, CONV_MARGIN_PERCENT, CONV_MARGIN_AMOUNT, CONV_AMOUNT, 
									CONV_CURRENCY, CONV_RATE_AFTER_MARGIN, REQUEST_AMOUNT, REQUEST_METHOD, VOUCHER, 
									WITHDRAWAL_HISTORY_ID, SUBMETHOD 
								)
		select						
									@PaymentType TYPE, @PaymentMethod METHOD, null REF_NUMBER, null IP_ADDRESS, 
									case when @PaymentStatus not in ('PENDING') then dateadd(minute, -3, @Date) else @Date end REQUEST_DATE, 
									case when @PaymentStatus not in ('PENDING') then @Date end PROCESS_DATE, 
									@PaymentStatus STATUS, @Amount AMOUNT, @AccountID ACCOUNT_ID, null MISC, 
									@Info INFO, @Amount AMOUNT_REAL, 0 AMOUNT_RELEASED_BONUS, null REQ_BONUS_PLAN_ID, 0 FEE, 
									null WORLD_PAY_CARD_ID, null MANUAL_BANK_FIELDS, null MANUAL_BANK_AGENT_ID, @Date LAST_UPDATED_DATE, null REJECTED_REASON, 
									0 IS_CONVERTED, null CONV_RATE, null CONV_MARGIN_PERCENT, null CONV_MARGIN_AMOUNT, null CONV_AMOUNT, 
									null CONV_CURRENCY, null CONV_RATE_AFTER_MARGIN, null REQUEST_AMOUNT, null REQUEST_METHOD, null VOUCHER, 
									null WITHDRAWAL_HISTORY_ID, null SUBMETHOD 
	select @PaymentID = scope_identity()
	if @PaymentType = 'DEPOSIT' and @PaymentStatus = 'COMPLETED'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @Amount, @TranType = 'DEPOSIT', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
		--select * from admin_all.ACCOUNT_TRAN where id = @AccountTranID
		--select * from admin_all.ACCOUNT where id = @AccountID
	end
	else if @PaymentType = 'DEPOSIT' and @PaymentStatus = 'DP_ROLLBACK'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @Amount, @TranType = 'DEPOSIT', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'DP_RBACK', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	else if @PaymentType = 'WITHDRAWAL' and @PaymentStatus = 'CANCELLED'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'WITHDRAWAL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @Amount, @TranType = 'WD_CANCEL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	else if @PaymentType = 'WITHDRAWAL' and @PaymentStatus = 'COMPLETED'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'WITHDRAWAL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	else if @PaymentType = 'WITHDRAWAL' and @PaymentStatus = 'FAILED'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'WITHDRAWAL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @Amount, @TranType = 'WD_CANCEL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	else if @PaymentType = 'WITHDRAWAL' and @PaymentStatus = 'PENDING'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'WITHDRAWAL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	else if @PaymentType = 'WITHDRAWAL' and @PaymentStatus = 'REJECTED'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'WITHDRAWAL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @Amount, @TranType = 'WD_REJECT', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	else if @PaymentType = 'WITHDRAWAL' and @PaymentStatus = 'SENT_TO_AGENT'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'WITHDRAWAL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	else if @PaymentType = 'WITHDRAWAL' and @PaymentStatus = 'WD_ROLLBACK'
	begin
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @NAmount, @TranType = 'WITHDRAWAL', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
		exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @Amount, @TranType = 'WD_RBACK', @PaymentID = @PaymentID, @DateTime = @Date, @AccountTranID = @AccountTranID output
	end
	commit
	

end
go
--begin transaction
--exec maint.usp_GenerateDataPaymentCreateOne 10000, 'DEPOSIT', 'COMPLETED', 'ALWDIRECT_MASTERCARD',  103

----171042
--rollback