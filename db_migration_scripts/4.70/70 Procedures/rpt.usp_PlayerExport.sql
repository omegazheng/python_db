set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_PlayerExport
(
	@BrandIDs varchar(max) = null
)
as
begin
	set nocount on
	create table #BrandID(BrandID int, BrandName nvarchar(1000), primary key (BrandID))
	insert into #BrandID(BrandID, BrandName)
		select BRANDID, BRANDNAME
		from admin.CASINO_BRAND_DEF
		where @BrandIDs is null
			or BRANDID in (
							select cast(value as int) 
							from (select value from string_split(@BrandIDs, ',') s)  a
						)
	select 
			ua.PartyID [Account ID],
			u.USERID [User ID],
			u.NICKNAME Nickname,
			u.FIRST_NAME [First Name],
			u.LAST_NAME [Last Name],
			u.PHONE Phone,
			u.GENDER Gender,
			u.MOBILE_PHONE [Mobile Phone],
			u.CITY City,
			u.COUNTRY Country,
			u.EMAIL Email,
			u.ALLOW_EMAILS Subscription,
			u.BIRTHDATE BirthDate,
			u.REG_DATE [Registration Date],
			u.CONFIRMATION_CODE [Confirmation Code],
			u.REGISTRATION_STATUS [Registration Status],
			case when u.ACTIVE_FLAG = 1 then 'ACTIVE' else 'SUSPENDED' end [Active Status],
			u.LOCKED_STATUS [Locked Status],
			u.LOCKED_SINCE [Locked Since],
			u.LOCKED_UNTIL [Locked Until],
			b.BrandName [Brand Name],
			u1.CURRENCY Currency,
			u.LANGUAGE Language,
			l.login_time [Last Login Date],
			case when ua.IsPrimary = 1 then dlp.DAILY end [Deposit Limit Amount],
			p.FirstAmount [First Deposit Amount],
			p.FirstDate [First Deposit Date],
			p.cnt [Deposit Count Lifetime],
			atg.AmountReleasedBonus [Bonus Released Lifetime],
			atg.AmountBonusCreated [Bonus Granted Lifetime],
			a1.BALANCE_REAL [Balance Breakdown By Currency]
	from external_mpt.UserAssociatedAccount ua
		inner join  external_mpt.USER_CONF u on ua.PartyID = u.PARTYID
		inner join  external_mpt.USER_CONF u1 on ua.AssociatedPartyID = u1.PARTYID
		inner join #BrandID b on b.BrandID = u.BRANDID
		inner join admin_all.ACCOUNT a on a.PARTYID = ua.PARTYID
		inner join admin_all.ACCOUNT a1 on a1.PARTYID = ua.AssociatedPartyID
		left join admin_all.DEPOSIT_LIMIT_PLAYER dlp on dlp.ACCOUNT_ID = a.id
		outer apply(select top 1 ull.login_time from admin_all.USER_LOGIN_LOG ull where ull.LOGIN_TYPE = 'LOGIN' and ull.STATUS = 'SUCCESS' and ull.partyid = ua.PARTYID order by ull.login_time desc) l
		outer apply(
						select 
								max(case when rn = 1 then p.AMOUNT end) as FirstAmount,
								max(case when rn = 1 then p.PROCESS_DATE end) as FirstDate,
								count(*) cnt
						from (
								select p.AMOUNT, p.PROCESS_DATE, row_number() over(order by p.PROCESS_DATE asc) rn
								from admin_all.PAYMENT p 
								where p.TYPE = 'DEPOSIT'
									and p.STATUS = 'COMPLETED'
									and p.ACCOUNT_ID = a1.id 
								) p
					) p
		outer apply(
					select 
							sum(case when atg.TranType = 'BONUS_REL' then atg.AmountReleasedBonus else 0 end) AmountReleasedBonus,
							sum(case when atg.TranType = 'BONUS_CRE' then atg.AmountReleasedBonus else 0 end) AmountBonusCreated
					from admin_all.AccountTranAggregate atg
					where atg.AggregateType = 0
						and atg.Period = 'Y'
						and atg.TranType in( 'BONUS_REL', 'BONUS_CRE')
						and atg.PartyID = ua.AssociatedPartyID
				) atg
--	where ua.PartyID = 91429948
--	sp_help 'admin_all.DEPOSIT_LIMIT_PLAYER'
end
go
--exec rpt.usp_PlayerExport 



