set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_DailyAffiliateRegistration
(
	@Date date = null,
	@BrandIDs varchar(max) = null,
	@OutputColumns varchar(max) = null output
)
as
begin
    DECLARE @DateFrom DATETIME
    DECLARE @DateTo DATETIME
	set nocount on
    SET @DateFrom = isnull(@Date, getdate())
    SET @DateTo = DATEADD(DD, 1, @DateFrom);
	create table #BrandID(BrandID int, BrandName nvarchar(1000), primary key (BrandID))
	insert into #BrandID(BrandID, BrandName)
		select BRANDID, BRANDNAME
		from admin.CASINO_BRAND_DEF
		where @BrandIDs is null
			or BRANDID in (
							select cast(value as int) 
							from (select value from string_split(@BrandIDs, ',') s)  a
						)
    select utc.VALUE as btag,
           cbd.BrandName as brand,
           uc.REG_DATE as account_opening_date,
           uc.PARTYID as player_id,
           uc.USERID as username,
           uc.COUNTRY
    into #temp
    from admin_all.USER_TRACKING_CODE utc

             join external_mpt.USER_CONF uc on uc.PARTYID = utc.PARTYID
             join #BrandID cbd on cbd.BrandID = uc.BRANDID
    where CODE_KEY = 'btag' and uc.REG_DATE > @DateFrom and uc.REG_DATE < @DateTo;

	--from x0

	select @OutputColumns = stuff((select ','+name as column_name from tempdb.sys.columns where object_id = object_id('tempdb..#temp') order by column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '');
    select * from #temp
	--select @OutputColumns
end
go
--exec rpt.usp_DailyAffiliateRegistration '2000-01-01'
go

