SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_SynchronizeSolidGameList
(
    @PlatformID int,
    @GameListString varchar(8000),
    @UpdatedCount int output

)
as

BEGIN

  declare @SegmentID int
  select @SegmentID = id from admin_all.segment where name = 'Casino'
  if (@SegmentID is null)
      RAISERROR('Segment Casino does NOT exist', 16, 1);

  create table #Games(Game varchar(400))

  insert into #Games(Game)
  select ltrim(rtrim(Item))
  from admin_all.fc_splitDelimiterString(@GameListString, ';;')
  where ltrim(rtrim(Item)) <> ''

  begin transaction
  declare @CurrentGame varchar(400), @GameID varchar(100), @GameName varchar(100), @GameCategoryID int, @SubPlatformID int
  set @UpdatedCount = 0
  while EXISTS(select Game from #Games)
    begin
        SELECT TOP 1 @CurrentGame = Game FROM #Games;

        select @GameID = GameID, @GameName = GameName,
           @GameCategoryID = GameCategoryID, @SubPlatformID = SubPlatformID
        from admin_all.fc_splitDelimiterStringToSolidGameInfoFields(@CurrentGame, ',,')

        --         print '@GameID=' + @GameID
        --             + ', @GameName=' + @GameName
        --             + ', @GameCategoryID=' + cast(@GameCategoryID as varchar(100))
        --             + ', @SubPlatformID=' + cast(@SubPlatformID as varchar(100));

        --  sanity check
        if @GameID is null or LEN(@GameID) = 0
            begin
              RAISERROR('@GameID is empty', 16, 1);
              rollback;
            end
        if @GameName is null or LEN(@GameName) = 0
          begin
            RAISERROR('@GameName is empty', 16, 1);
            rollback;
          end
        if @GameCategoryID is null
          begin
            RAISERROR('@GameCategoryID is empty', 16, 1);
            rollback;
          end
        if @SubPlatformID is null
          begin
            RAISERROR('@SubPlatformID is empty', 16, 1);
            rollback;
          end

        if NOT exists (select * from admin_all.GAME_INFO where PLATFORM_ID = @PlatformID and GAME_ID = @GameID)
           begin
               insert into admin_all.GAME_INFO(PLATFORM_ID, GAME_ID, NAME, GAME_CATEGORY_ID, GAME_LAUNCH_ID, SUB_PLATFORM_ID, SEGMENT_ID)
               values(@PlatformID, @GameID, @GameName, @GameCategoryID, @GameID, @SubPlatformID, @SegmentID);
               set @UpdatedCount = @UpdatedCount + 1;
           end

        delete from #Games where Game = @CurrentGame;
    end
  print 'Update ' + cast(@UpdatedCount as varchar(100)) + ' records'
  commit

END

-- exec admin_all.usp_SynchronizeSolidGameList
--      @PlatformID = 198,
--      @GameListString = 'EVO_LIVE_AMERICAN_ROULETTE_MST,Evolution Live American Roulette (MST),108,123;EVO_LIVE_AMERICAN_ROULETTE_SLD,Evolution Live American Roulette (SLD),108,123;',
--      @UpdatedCount = null