SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetExternalJobInfo
(
    @InternalReferenceID nvarchar(50) = null,
    @ExternalJobProvider nvarchar(50) =  null,
    @ExternalJobID nvarchar(255) = null
)
as

BEGIN
  if (@InternalReferenceID is not null)
    select ID id, INTERNAL_REFERENCE_ID internalReferenceId, EXTERNAL_JOB_PROVIDER externalJobProvider, EXTERNAL_JOB_ID externalJobId,
          STATUS status, CREATED_TIME createdTime, LAST_UPDATE_TIME lastUpdateTime, REQUEST request, RESPONSE response
    from admin_all.ExternalJobInfo
    where INTERNAL_REFERENCE_ID = @InternalReferenceID;
  else if (@ExternalJobProvider is not null and @ExternalJobID is not null)
    select ID id, INTERNAL_REFERENCE_ID internalReferenceId, EXTERNAL_JOB_PROVIDER externalJobProvider, EXTERNAL_JOB_ID externalJobId,
          STATUS status, CREATED_TIME createdTime, LAST_UPDATE_TIME lastUpdateTime, REQUEST request, RESPONSE response
    from admin_all.ExternalJobInfo
    where EXTERNAL_JOB_PROVIDER = @ExternalJobProvider and EXTERNAL_JOB_ID = @ExternalJobID;
  else
    RAISERROR('Either @InternalReferenceID or @ExternalJobProvider and @ExternalJobID must be not null', 16, 1)
END
