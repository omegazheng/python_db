set ansi_nulls, quoted_identifier on
go


-- use @partyid for player with one currency only
-- use @partyIds for player with multi-currency
-- DO NOT INPUT BOTH @partyid and @partyIds
create or alter procedure [admin_all].[usp_GetPlayerTransactionsReportSummary]
  (
    @partyid   INT = null,
    @startDate DATETIME,
    @endDate DATETIME,
    @partyIds VARCHAR(1000) = null
  )
AS
  BEGIN
    DECLARE @partyidLocal INT
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = @endDate
    SET @partyidLocal = @partyid

    SELECT
      COUNT(*) as totalRecord
    from
      ACCOUNT_TRAN a with(NOLOCK)
      left join PLATFORM p with(NOLOCK) on p.id = a.platform_id
      left join GAME_INFO g with(NOLOCK) on a.GAME_ID = g.GAME_ID and a.platform_id = g.PLATFORM_ID
      left join TRANSACTION_PLATFORM_CONVERSION pc with(NOLOCK) on pc.TRANSACTION_ID = a.id
      join admin_all.ACCOUNT aa with(NOLOCK) on aa.id = a.account_id
      join external_mpt.user_conf u with(NOLOCK) on u.partyid = aa.partyid
    where
      a.datetime >= @startDateLocal
      and a.datetime < @endDateLocal
      and (@partyidLocal is null or u.partyid = @partyidLocal)
      and (@partyIds is null or u.partyid in (SELECT * FROM [admin_all].fc_splitDelimiterString(@partyIds, ',')))
  END

go
