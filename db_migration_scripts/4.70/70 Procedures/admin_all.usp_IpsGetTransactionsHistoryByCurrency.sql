SET ANSI_NULLS, QUOTED_IDENTIFIER ON
GO
create or alter procedure admin_all.usp_IpsGetTransactionsHistoryByCurrency
(
		@AccountId int,
		@StartDate datetime,
		@EndDate datetime,
		@Type varchar(255),
		@TranTypes VARCHAR(1000),
		@ProductIds VARCHAR(1000) = null,
		@PageNum INT = 1,
		@PageSize INT = 100,
		@OrderBy VARCHAR(255) = 'DATETIMEDESC'
)
as
begin
	set nocount on 
    create table #TranType(Type nvarchar(100) primary key)
    create table #Product(ProductId int primary key)

	insert into #TranType(Type)
    select distinct cast(rtrim(ltrim(value)) as nvarchar(100)) as Type
    from string_split(@TranTypes, ',')


    insert into #Product(ProductId)
    select p.id
    from admin_all.PLATFORM p 
	where exists(select * from string_split(@ProductIds, ',') s where try_cast(rtrim(ltrim(s.value)) as int) = p.ID)

    Select @EndDate = ISNULL(@endDate, getdate())

    declare @SQL nvarchar(max)
	select @SQL = '
     ;with x0 as
	 (       
            SELECT 
					count(*) over()												 as totalRecords,
                   a.ID,
                   a.DATETIME                                                    AS dateTime,
                   a.TRAN_TYPE                                                   AS tranType,
                   -- platformCode to be replaced by productCode
                   p.code                                                        AS platformCode,
                   p.code                                                        AS productCode,
                   s.Name                                                        As Segment,
                   g.game_launch_id                                              AS gameLaunchId,
                   g.name                                                        AS gameName,
                   ISNULL(a.amount_real, 0) + ISNULL(a.AMOUNT_RELEASED_BONUS, 0) +
                   ISNULL(a.AMOUNT_PLAYABLE_BONUS, 0)                            AS amount,
                   ISNULL(a.amount_real, 0) + ISNULL(a.AMOUNT_RELEASED_BONUS, 0) AS amountReal,
                   ISNULL(a.AMOUNT_PLAYABLE_BONUS, 0)                            AS amountBonus,
                   ISNULL(a.balance_real, 0) + ISNULL(a.balance_released_bonus, 0) +
                   ISNULL(a.balance_playable_bonus, 0)                           AS postBalance,
                   ISNULL(a.balance_real, 0) + ISNULL(a.balance_released_bonus,
                                                      0)                         AS postBalanceReal,
                   ISNULL(a.balance_playable_bonus,
                          0)                                                     AS postBalanceBonus,
                   a.GAME_TRAN_ID                                                AS gameTranId
            FROM admin_all.ACCOUNT_TRAN a
                     LEFT JOIN admin_all.PLATFORM p ON p.id = a.platform_id
                     LEFT JOIN admin_all.SEGMENT s on p.segment_id = s.id
                     LEFT JOIN admin_all.GAME_INFO g ON a.GAME_ID = g.GAME_ID AND a.platform_id = g.PLATFORM_ID
                     '+ case when exists(select * from #Product) then 'JOIN #Product on a.PLATFORM_ID = #Product.ProductId' else '' end + '
            WHERE a.ACCOUNT_ID = @AccountId
              AND a.datetime >= @StartDate
              AND a.datetime < @EndDate
              AND a.TRAN_TYPE LIKE @Type
              and a.TRAN_TYPE IN (select x.Type from #TranType x)
      )
	  ,x1 as 
	  (
		select 
				'+
					CASE
						WHEN @OrderBy = 'DATETIMEASC' THEN 'ROW_NUMBER() OVER (ORDER BY dateTime)'
						WHEN @OrderBy = 'DATETIMEDESC' THEN 'ROW_NUMBER() OVER (ORDER BY dateTime DESC)'
						WHEN @OrderBy = 'TRANTYPEASC' THEN 'ROW_NUMBER() OVER (ORDER BY tranType)'
						WHEN @OrderBy = 'TRANTYPEDESC' THEN 'ROW_NUMBER() OVER (ORDER BY tranType DESC)'
						WHEN @OrderBy = 'GAMENAMEASC' THEN 'ROW_NUMBER() OVER (ORDER BY gameName)'
						WHEN @OrderBy = 'GAMENAMEDESC' THEN 'ROW_NUMBER() OVER (ORDER BY gameName DESC)'
						WHEN @OrderBy = 'AMOUNTASC' THEN 'ROW_NUMBER() OVER (ORDER BY amount)'
						WHEN @OrderBy = 'AMOUNTDESC' THEN 'ROW_NUMBER() OVER (ORDER BY amount DESC)'
						WHEN @OrderBy = 'AMOUNTREALASC' THEN 'ROW_NUMBER() OVER (ORDER BY amountReal)'
						WHEN @OrderBy = 'AMOUNTREALDESC' THEN 'ROW_NUMBER() OVER (ORDER BY amountReal DESC)'
						WHEN @OrderBy = 'AMOUNTBONUSASC' THEN 'ROW_NUMBER() OVER (ORDER BY amountBonus)'
						WHEN @OrderBy = 'AMOUNTBONUSDESC' THEN 'ROW_NUMBER() OVER (ORDER BY amountBonus DESC)'
						WHEN @OrderBy = 'POSTBALANCEASC' THEN 'ROW_NUMBER() OVER (ORDER BY postBalance)'
						WHEN @OrderBy = 'POSTBALANCEDESC' THEN 'ROW_NUMBER() OVER (ORDER BY postBalance DESC)'
						WHEN @OrderBy = 'POSTBALANCEREALASC' THEN 'ROW_NUMBER() OVER (ORDER BY postBalanceReal)'
						WHEN @OrderBy = 'POSTBALANCEREALDESC' THEN 'ROW_NUMBER() OVER (ORDER BY postBalanceReal DESC)'
						WHEN @OrderBy = 'POSTBALANCEBONUSASC' THEN 'ROW_NUMBER() OVER (ORDER BY postBalanceBonus)'
						WHEN @OrderBy = 'POSTBALANCEBONUSDESC' THEN 'ROW_NUMBER() OVER (ORDER BY postBalanceBonus DESC)'
						WHEN @OrderBy = 'GAMETRANIDASC' THEN 'ROW_NUMBER() OVER (ORDER BY gameTranId)'
						WHEN @OrderBy = 'GAMETRANIDDESC' THEN' ROW_NUMBER() OVER (ORDER BY gameTranId DESC)'
					END+ ' AS RowNum, 
				* 
		from x0
	)
	select *
	from x1
	where RowNum BETWEEN @PageSize * (@PageNum - 1) + 1 AND @PageSize * @PageNum
	order by RowNum
	'
	exec sp_executesql @SQL, N'@AccountId int, @StartDate datetime,	@EndDate datetime,	@Type varchar(255),@PageNum INT, @PageSize INT = 100', @AccountId, @StartDate,	@EndDate,	@Type,@PageNum, @PageSize
END
go
--exec admin_all.usp_IpsGetTransactionsHistoryByCurrency 69, '2000-01-01', '2020-01-01', 'GAME_BET', 'GAME_BET', null, 2, 100
go

