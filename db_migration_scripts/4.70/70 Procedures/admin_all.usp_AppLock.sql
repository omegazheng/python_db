set ansi_nulls, quoted_identifier on
go

/**
exec admin_all.usp_AppLock @Resource='cashInLP_1484741', @LockTimeout='0'
exec admin_all.usp_AppLock @Resource='cashInLP_1484741', @LockTimeout='-1'

LockTimeout : should be -1,
-1: it means if the resource is owned by one session, the other session will wait until the first one is released.
 0: have a peek on the resource. If it’s available, acquire the lock, otherwise error out(timeout).
Any other number ie 30000: try to acquire a lock, if the lock cannot be acquired in 30 seconds, timeout.
*/
create or alter procedure admin_all.usp_AppLock
  (
    @Resource NVARCHAR(255), @LockTimeout INT
  )
AS
  BEGIN
    SET NOCOUNT, xact_abort ON
    DECLARE @ret int

    IF @Resource IS NULL
      BEGIN
        RAISERROR ('Resource can not be null', 16, 1)
        RETURN
      END

    BEGIN transaction
    EXEC @ret = sp_getapplock @Resource, 'Exclusive', 'Transaction', @LockTimeout
    if @ret < 0
      begin
        rollback
        raiserror('Resource %s is being acquired by another session.', 16, 1, @Resource)
        return
      end
    COMMIT
  END

go
