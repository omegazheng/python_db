SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_InsertExternalJobInfo
(
    @InternalReferenceID nvarchar(50),
    @ExternalJobProvider nvarchar(50),
    @ExternalJobID nvarchar(255),
    @Status nvarchar(50),
    @CreatedTime datetime,
    @LastUpdateTime datetime,
    @Request nvarchar(max),
    @Response nvarchar(max)
)
as

BEGIN
  insert into admin_all.ExternalJobInfo
  (INTERNAL_REFERENCE_ID, EXTERNAL_JOB_PROVIDER, EXTERNAL_JOB_ID, STATUS, CREATED_TIME, LAST_UPDATE_TIME, REQUEST, RESPONSE)
  values
  (@InternalReferenceID, @ExternalJobProvider, @ExternalJobID, @Status, @CreatedTime, @LastUpdateTime, @Request, @Response)
END
