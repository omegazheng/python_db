set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_GenerateDataPayment
(
	@PartyIDs varchar(max),
	@PaymentTypes varchar(max),
	@PaymentStatuses varchar(max),
	@PaymentMethods varchar(max),
	@CountOfRecordsFrom int,
	@AmountFrom numeric(38,18),
	@CountOfRecordsTo int = null,
	@AmountTo numeric(38,18) = null,
	@DateFrom datetime = null,
	@DateTo datetime = null
)
as
begin 
	set nocount on
	declare @Partys table (PartyID int, SortOrder uniqueidentifier)
	declare @Types table (PaymentType varchar(25), SortOrder uniqueidentifier)
	declare @Statuses table (PaymentStatus varchar(25), SortOrder uniqueidentifier)
	declare @Methods table (PaymentMethod varchar(25), SortOrder uniqueidentifier)

	declare @PartyID int, @PaymentType varchar(25), @PaymentStatus varchar(25), @PaymentMethod varchar(60), @Amount numeric(38,18), @Date datetime = null, @Count int, @i int
	select @AmountTo = isnull(@AmountTo, @AmountFrom), @CountOfRecordsTo = isnull(@CountOfRecordsTo, @CountOfRecordsFrom), @DateFrom = isnull(@DateFrom, getdate())
	select @DateTo = isnull(@DateTo, @DateFrom)
	insert into @Types(PaymentType, SortOrder)
		select upper(value), newid() 
		from string_split(@PaymentTypes, ',')
		where value in ('DEPOSIT','WITHDRAWAL')
	insert into @Statuses(PaymentStatus, SortOrder)
		select upper(value), newid() 
		from string_split(@PaymentStatuses, ',')
		where value in ('COMPLETED', 'DP_ROLLBACK', 'FAILED', 'CANCELLED', 'PENDING', 'REJECTED', 'SENT_TO_AGENT', 'WD_ROLLBACK')
	insert into @Methods(PaymentMethod, SortOrder)
		select upper(value), newid() 
		from string_split(@PaymentMethods, ',') M
		where exists(select * from admin_all.PAYMENT_METHOD pm where pm.CODE = m.value)
	insert into @Partys(PartyID, SortOrder)
		select p.PartyID, newid()
		from (select try_cast(rtrim(value) as int) PartyID from string_split(@PartyIDs, ',')) p
		where exists(select * from admin_all.USER_REGISTRY ur where ur.MAP_KEY = 'testPlayer' and ur.VALUE = 'true' and ur.PARTYID = p.PartyID)




	declare c cursor local fast_forward for
		select  t.PaymentType, s.PaymentStatus, m.PaymentMethod, p.PartyID
		from @Types t
			cross join @Statuses s
			cross join @Methods m
			cross join @Partys p
		where t.PaymentType = 'DEPOSIT' and s.PaymentStatus in ('COMPLETED', 'DP_ROLLBACK', 'FAILED', 'CANCELLED', 'PENDING', 'REJECTED')
			or t.PaymentType = 'WITHDRAWAL' and s.PaymentStatus in ('COMPLETED', 'FAILED', 'CANCELLED', 'PENDING', 'REJECTED', 'SENT_TO_AGENT', 'WD_ROLLBACK')
		order by t.SortOrder, s.SortOrder, m.SortOrder, p.SortOrder
	open c
	fetch next from c into @PaymentType, @PaymentStatus, @PaymentMethod, @PartyID
	while @@fetch_status = 0
	begin
		select @Count = @CountOfRecordsFrom + ceiling((@CountOfRecordsTo-@CountOfRecordsFrom)*rand(checksum(newid()))), @i = 1
		while @i <= @Count
		begin
			select	@Amount = cast(@AmountFrom + (@AmountTo-@AmountFrom)*rand(checksum(newid())) as numeric(38,2)),
					@Date = dateadd(minute, ceiling(datediff(minute, @DateFrom, @DateTo) *rand(checksum(newid()))) , @DateFrom)
			if @PaymentType = 'WITHDRAWAL'
			begin
				select @Amount = case when BALANCE_REAL - @Amount <0 then BALANCE_REAL else @Amount end
				from admin_all.ACCOUNT
				where PARTYID = @PartyID
			end
			if @Amount <> 0
			begin
				exec maint.usp_GenerateDataPaymentCreateOne @PartyID = @PartyID, @PaymentType = @PaymentType, @PaymentStatus = @PaymentStatus, @PaymentMethod = @PaymentMethod, @Amount = @Amount, @Date = @Date
			end
			else
			begin
				print 'Insufficient amount for party ' + cast(@PartyID as varchar(20))
			end
			select @i = @i + 1
		end
		fetch next from c into @PaymentType, @PaymentStatus, @PaymentMethod, @PartyID
	end
	close c 
	deallocate c
end
go
/*
begin transaction
declare @PaymentID int, @AccountTranID bigint
select @PaymentID = max(ID) from admin_all.PAYMENT
select @AccountTranID = max(ID) from admin_all.ACCOUNT_TRAN

exec maint.usp_GenerateDataPayment @PartyIDs = '100122231,100144903,100144261,100144265,100143584,91429638',
									@PaymentTypes = 'DEPOSIT,WITHDRAWAL',
									@PaymentStatuses = 'COMPLETED,DP_ROLLBACK,FAILED,CANCELLED,PENDING,REJECTED,SENT_TO_AGENT,WD_ROLLBACK',
									@PaymentMethods = 'ALWDIRECT_MASTERCARD',
									@CountOfRecordsFrom = 10,
									@CountOfRecordsTo = 20,
									@AmountFrom = 100.2,
									@AmountTo = 100015.6,
									@DateFrom = '2010-01-01',
									@DateTo = '2020-12-31'
select * from admin_all.PAYMENT where id > @PaymentID
select * from admin_all.ACCOUNT_TRAN where ID  >@AccountTranID
rollback


*/