SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetUserTrackingCode
(
    @PartyID int,
    @CodeKey varchar(20)
)
as

BEGIN
  select ID, PARTYID as partyId, CODE_KEY as codeKey, VALUE
  from admin_all.USER_TRACKING_CODE
  where PARTYID = @PartyID and CODE_KEY = @CodeKey
END
