set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_ListSlowResponse
(
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@ThresholdInSec int = 10,
	@ProductID int = null,
	@TopN int = 1000
)
as
begin
	set nocount on
	select @DateFrom = isnull(@DateFrom, dateadd(day, -1, getdate()))
	select @DateTo = isnull(@DateTo, getdate())
	select u.BrandID, l.*
	from Logs.GameRequestHistory l
		left join external_mpt.USER_CONF u on u.PARTYID = l.PartyID
	where l.RequestTime >= @DateFrom
		and l.RequestTime <= @DateTo
		and datediff(second, l.RequestTime, l.ResponseTime) >= @ThresholdInSec
		and ProductID = isnull(@ProductID , l.ProductID)
end
go
--exec rpt.usp_ListSlowResponse