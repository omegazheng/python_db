set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetUniquePlayersCountByStaffAndBrandsAndSegments]
(
	@staffid int, 
	@brands nvarchar(2000), 
	@segments nvarchar(2000),
	@AggregateType tinyint = 0
)
AS
  BEGIN
    set nocount on

    declare @SourceFrom varchar(100)
    select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
    if @SourceFrom is null
    begin
      exec admin_all.usp_SetRegistry 'core.dashboard.source', 'dw'
      select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
    end

    SET DATEFIRST 1
    DECLARE @startDateLocal date
    DECLARE @endDateLocal date
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(DAY, -64, CAST(GETDATE() AS DATE))
--     SET @startDateLocal = @endDateLocal
--     if @startDateLocal > DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))
--       select  @startDateLocal = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))
--     if @startDateLocal > CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
--       select  @startDateLocal = CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
--     if @startDateLocal > CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
--       select  @startDateLocal = CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
--     if @startDateLocal > CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal)-1, 0) AS DATE)
--       select  @startDateLocal = CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal)-1, 0) AS DATE)
    --DATEADD(DAY, -366, CAST(@endDateLocal AS DATE))

    create table #Brands (BrandID int primary key)
    insert into #Brands(BrandID)
      SELECT distinct BRANDID
      FROM [admin_all].[STAFF_BRAND_TBL]
      WHERE STAFFID = @staffid
            AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands,','))

    create table #Segments(SegmentID int primary key)
    insert into #Segments(SegmentID)
      select cast(Item as int) segmentId from admin_all.fc_splitDelimiterString(@segments,',')

    create table #Temp(
      DATE date, BRAND_ID int, PARTY_ID int
    )

    IF @SourceFrom = 'dw'
      BEGIN
		insert into #Temp(DATE, BRAND_ID, PARTY_ID)
        SELECT
          SUMMARY_DATE               AS DATE,
          dw.BRAND_ID                AS BRAND_ID,
          dw.PARTY_ID                AS PARTY_ID
        FROM ADMIN_ALL.DW_GAME_PLAYER_DAILY DW
          inner join admin_all.PLATFORM pl on dw.PLATFORM_ID = pl.ID
        WHERE DW.SUMMARY_DATE >= @startDateLocal
              AND
              DW.SUMMARY_DATE <= @endDateLocal
              AND DW.BRAND_ID IN (select b.BrandID from #Brands b)
              AND pl.segment_id in (select s.SegmentID from #Segments s)
        GROUP BY SUMMARY_DATE, dw.BRAND_ID, PARTY_ID;
      END
	else
      BEGIN
        -- Using Aggregrate Table.
		  insert into #Temp(DATE, BRAND_ID, PARTY_ID)
          select
            dw.Date as Date,
            dw.BrandID  as Brand_ID,
            dw.PARTYID AS PARTY_ID
          FROM admin_all.fn_GetPlayerDailyAggregate(@AggregateType, @startDateLocal, dateadd(day, 1, @endDateLocal)) DW
            left join admin_all.PLATFORM pl on dw.ProductID = pl.ID
          WHERE  DW.BrandID IN (select x.BrandID from #Brands x)
                 AND pl.segment_id in (select x.SegmentID from #Segments x)
          GROUP BY DW.Date, DW.BrandID, DW.PARTYID
      END
	create clustered index idx on #Temp(Date)


            SELECT
              'TODAY'        AS PERIOD,
              count(DISTINCT PARTY_ID) AS COUNT_FOR_PERIOD
            FROM #Temp
            WHERE DATE = CAST(@endDateLocal AS DATE)
            UNION ALL
            SELECT
              'YESTERDAY'    AS PERIOD,
              count(DISTINCT PARTY_ID)       AS COUNT_FOR_PERIOD
            FROM #Temp
            WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))
            UNION ALL

            -- UNIQUE PLAYER IS TO THE SCOPE OF THE ENTIRE PERIOD, CAN NOT BE AGGREGATED FROM DAILY VALUE
            SELECT
              'WTD'               AS PERIOD,
              count(DISTINCT PARTY_ID) AS COUNT_FOR_PERIOD
            FROM #Temp as dw
            WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                  AND Date <= CAST(@endDateLocal AS DATE)
            UNION ALL
            -- UNIQUE PLAYER IS TO THE SCOPE OF THE ENTIRE PERIOD, CAN NOT BE AGGREGATED FROM DAILY VALUE
            SELECT
              'MTD'               AS PERIOD,
              COUNT(DISTINCT PARTY_ID) AS COUNT_FOR_PERIOD
            FROM #Temp
            WHERE Date >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                  AND Date <= CAST(@endDateLocal AS DATE)
            UNION ALL
            -- UNIQUE PLAYER IS TO THE SCOPE OF THE ENTIRE PERIOD, CAN NOT BE AGGREGATED FROM DAILY VALUE
            SELECT
              'SPLM'               AS PERIOD,
              COUNT(DISTINCT PARTY_ID) AS COUNT_FOR_PERIOD
            FROM #Temp

            WHERE Date >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal)-1, 0) AS DATE)
                  AND Date <= cast(DATEADD(MONTH, -1, @endDateLocal) AS DATE)
   END

go
