SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_CheckProductCurrencyConversionConfiguration
(
    @ProductID int
)
as

BEGIN
    if exists (
        select * from admin_all.SubProductCurrencyConversion
        where SubProductID in (
            select ID from admin_all.SUB_PLATFORM where PLATFORM_ID = @ProductID
        )
    )
    and
       exists (
       select * from admin_all.PLATFORM_CURRENCY where PLATFORM_ID = @ProductID
    )
        RAISERROR('Currency conversion CANNOT be configured at both Product and SubProduct levels', 16, 1)
END
