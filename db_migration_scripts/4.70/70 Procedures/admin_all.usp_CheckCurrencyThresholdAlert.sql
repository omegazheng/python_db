set ansi_nulls, quoted_identifier on
go
-- create or alter procedure admin_all.usp_CheckCurrencyThresholdAlert
-- as
--   begin
--
--   set nocount on
--   set xact_abort off
--     ;with rate as (
--     select max(LastUpdateDate) as lastUpdate, CurrencyFrom from admin_all.CurrencyConversionRateCurrent group by CurrencyFrom
--   ),
--    ccy as (
--     select DateAdd(MINUTE , -1*AlertThreshold, getdate()) as expiryTime, iso_code from admin_all.CURRENCY
-- --      select DateAdd(MINUTE , -1*AlertThreshold, getdate()) as expiryTime, iso_code from admin_all.CURRENCY where STATUS = 0
--   )
--   select rate.CurrencyFrom as currency,ccy.expiryTime from rate
--    inner join ccy on rate.lastUpdate < ccy.expiryTime and rate.CurrencyFrom = ccy.iso_code
--    ;
-- end

go
--
-- exec admin_all.usp_CheckCurrencyThreadAlert