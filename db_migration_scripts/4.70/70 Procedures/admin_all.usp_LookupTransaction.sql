set quoted_identifier, ansi_nulls on 
go
create or alter procedure admin_all.usp_LookupTransaction
(
	@ProductTranID nvarchar(100),
	@ProductID int
)
as
begin
	set nocount on 
	select ID 
	from admin_all.ACCOUNT_TRAN 
	where PLATFORM_TRAN_ID = @ProductTranID 
		and PLATFORM_ID = @ProductID
end
go