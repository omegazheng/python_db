SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetSubProductCurrencyConversionBySubProductId
(
    @SubProductID int
)
as

BEGIN
  select * from admin_all.SubProductCurrencyConversion where SubProductID = @SubProductID
END
