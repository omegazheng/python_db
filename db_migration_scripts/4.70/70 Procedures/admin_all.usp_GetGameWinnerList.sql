set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetGameWinnerList
(
	@StartDate datetime = null, -- if not specify it will be now - 1 hour
	@EndDate datetime = null, --if not specify it will be now()
	@BrandIDs nvarchar(max)= null, -- the winners within the specified brand, NULL means all brands
	@TopN int = 10, -- top N records
	@UniquePlayerOnly bit = 0-- 0/1
)
as
begin
	set nocount on
	create table #Brands(BrandID int primary key)
-- 	Setup Brands temporary table
	if @BrandIDs is not null
	begin
		insert into #Brands(BrandID)
			select cast(ltrim(rtrim(Item)) as int)
			from admin_all.fc_splitDelimiterString(@BrandIDs, ',')
			where isnumeric(ltrim(rtrim(Item))) = 1
				and nullif(ltrim(rtrim(Item)), '') is not null
	end
	else
	begin
		insert into #Brands(BrandID)
			select BRANDID from admin.CASINO_BRAND_DEF
	end
-- 	Round up to the beginning of the hour. If getdate() is 9:30 am, StartDate will be 9:00
	select @StartDate =isnull(@StartDate, dateadd(hour, datediff(hour, '1990-01-01', getdate()), '1990-01-01'))
	select @EndDate = isnull(@EndDate, dateadd(hour, 1, @StartDate))
	if @UniquePlayerOnly = 0
	begin
		select top (@TopN)
				gw.PartyID, u.USERID UserID, u.FIRST_NAME FirstName, u.LAST_NAME LastName, 
				gi.GAME_ID GameID, gi.GAME_LAUNCH_ID GameLaunchID, gi.PLATFORM_ID ProductID, gi.NAME GameInfoName, gi.IS_TOUCH IsTouch, 
				gw.WonAmount, gw.Currency, gw.ConvertedWonAmount, u.NICKNAME NickName, p.CODE ProductCode, u.COUNTRY
		from admin_all.GameWinnerList gw
			inner join admin_all.GAME_INFO gi on gi.ID = gw.GameInfoID
			inner join admin_all.PLATFORM p on p.ID = gi.PLATFORM_ID
			inner join external_mpt.USER_CONF u on u.PARTYID = gw.PartyID
		where gw.Datetime >= @StartDate 
			and gw.Datetime <@EndDate
			and u.WINNERS_LIST = 1
			and exists(select * from #Brands b where b.BrandID = gw.BrandID)
		order by gw.ConvertedWonAmount desc
	end
	else
	begin

		select	top (@TopN)
				gw.PartyID, gw.UserID, gw.FirstName, gw.LastName, 
				gw.GameID, gw.GameLaunchID, gw.ProductID, gw.GameInfoName, gw.IsTouch, 
				gw.WonAmount, gw.Currency, gw.ConvertedWonAmount, gw.NickName, gw.ProductCode, gw.Country
		from (
				select 
						gw.PartyID, u.USERID UserID, u.FIRST_NAME FirstName, u.LAST_NAME LastName, 
						gi.GAME_ID GameID, gi.GAME_LAUNCH_ID GameLaunchID, gi.PLATFORM_ID ProductID, gi.NAME GameInfoName, gi.IS_TOUCH IsTouch, 
						gw.WonAmount, gw.Currency, gw.ConvertedWonAmount, u.NICKNAME NickName, p.CODE ProductCode, u.COUNTRY,
						row_number() over( partition by gw.PartyID order by gw.ConvertedWonAmount desc) rn
				from admin_all.GameWinnerList gw
					inner join admin_all.GAME_INFO gi on gi.ID = gw.GameInfoID
					inner join external_mpt.USER_CONF u on u.PARTYID = gw.PartyID
					inner join admin_all.PLATFORM p on p.ID = gi.PLATFORM_ID
				where gw.Datetime >= @StartDate 
					and gw.Datetime <@EndDate
					and u.WINNERS_LIST = 1
					and exists(select * from #Brands b where b.BrandID = gw.BrandID)
			)gw
			where gw.rn = 1
		order by gw.ConvertedWonAmount desc
		/*
		select top (@TopN)
				gw.PartyID, u.USERID UserID, u.FIRST_NAME FirstName, u.LAST_NAME LastName, 
				gi.GAME_ID GameID, gi.GAME_LAUNCH_ID GameLaunchID, gi.PLATFORM_ID ProductID, gi.NAME GameInfoName, gi.IS_TOUCH IsTouch, 
				gw.WonAmount, gw.Currency, gw.ConvertedWonAmount, u.NICKNAME NickName, p.CODE ProductCode, u.COUNTRY
		from external_mpt.USER_CONF u
			cross apply (
							select top 1 * 
							from admin_all.GameWinnerList g 
							where g.PartyID = u.PARTYID
								and g.Datetime >= @StartDate 
								and g.Datetime <@EndDate
								and exists(select * from #Brands b where b.BrandID = g.BrandID)
							order by g.ConvertedWonAmount desc
						) gw 
			inner join admin_all.GAME_INFO gi on gi.ID = gw.GameInfoID
			inner join admin_all.PLATFORM p on p.ID = gi.PLATFORM_ID
		order by gw.ConvertedWonAmount desc
		*/
	end
end

go
