set ansi_nulls, quoted_identifier on 
go
if object_id('rpt.usp_GetMachinePendingTransaction') is null
begin
	exec('create procedure rpt.usp_GetMachinePendingTransaction as --')
end
go
-- use @partyid for player with one currency only
-- use @partyIds for player with multi-currency
-- DO NOT INPUT BOTH @partyid and @partyIds
-- add currency as output
alter procedure rpt.usp_GetMachinePendingTransaction
(
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@PartyID int = null,
	@Status varchar(100) = 'Pending,InReview',
    @partyIds VARCHAR(1000) = null
)
as
begin
	set nocount on
	create table #Status (Status varchar(30) collate database_default not null  primary key)
	insert into #Status(Status)
		select distinct Status
		from(
				select rtrim(ltrim(item)) Status
				from admin_all.fc_splitDelimiterString(@Status, ',') a
			)a
		where isnull(a.Status , '') <> ''

	declare @SQL nvarchar(max)
	select @SQL = '
	select	mt.MachineTranID, mt.MachineID, mt.PartyID, eft.EFTID,
			mt.StartDate, eft.AmountReal, eft.PlayableBonus, eft.ReleasedBonus ,eft.Status, eft.Reference, eft.TransferType,
			eft.Datetime EFTDate, eft.ProductTranID,
			u.FIRST_NAME + '' '' + u.LAST_NAME as UserName, u.USERID, u.CURRENCY, pl.CODE as ProductCode, pl.name as ProductName
	from admin_all.MachineTran mt
		inner join admin_all.MachineTranEFT eft on mt.MachineTranID = eft.MachineTranID 
		inner join external_mpt. USER_CONF u on u.PARTYID = mt.PartyID
		inner join admin_all.Machine m on m.MachineID = mt.MachineID
		left join admin_all.PLATFORM pl on m.ProductID = pl.ID
	where mt.MachineID is not null'
	+ case when @DateFrom is not  null then ' and mt.StartDate >= @DateFrom ' else '' end
	+ case when @DateTo is not  null then ' and mt.StartDate < @DateTo ' else '' end
	+ case when @PartyID is not  null then ' and mt.PartyID = @PartyID ' else '' end
	+ case when @partyIds is not null then ' and mt.PartyID in'  + ' (SELECT * FROM [admin_all].fc_splitDelimiterString(@partyIds, '','')) ' else '' end
	+ case when exists(select * from #Status) then ' and eft.Status in (select s.Status from #Status s) ' else '' end
	print @SQL
	exec sp_executesql @SQL, N'@DateFrom datetime, @DateTo Datetime, @PartyID int, @partyIds varchar(1000)', @DateFrom, @DateTo, @PartyID, @partyIds
end
go
--exec rpt.usp_GetMachinePendingTransaction '1900-01-01', '1900-02-01', 1000000
--exec rpt.usp_GetMachinePendingTransaction '1900-01-01', '1900-02-01', 1000000
-- exec rpt.usp_GetMachinePendingTransaction '2018-01-01', '2020-02-01', null, 'InReview', '1000000,100121600'
