set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.GetBrandRevenueByYearCurrency
(
	@YearFrom datetime, -- inclusive
	@YearTo datetime,  -- inclusive
	@Brands varchar(max)
)
as
begin
	set nocount on

	create table #Brands(BrandID int primary key)
	insert into #Brands
		select distinct v
		from (select try_cast(rtrim(ltrim(value)) as int) v from string_split(@Brands, ',')) t
	where v is not null
	SELECT
			YEAR as DISPLAY_YEAR,
			CURRENCY,
			SUM(PNL_REAL+ PNL_RELEASED_BONUS+ PNL_PLAYABLE_BONUS) PNL,
			SUM(HANDLE_REAL+ HANDLE_RELEASED_BONUS+ HANDLE_PLAYABLE_BONUS) HANDLE
	FROM admin_all.DW_GAME_PLAYER_DAILY
	WHERE YEAR >= @YearFrom  and YEAR <=   @YearTo 
        and BRAND_ID in (select b.BrandID from #Brands b)
	GROUP BY YEAR, CURRENCY
	ORDER BY YEAR ASC
	/*
	select ath.Date Datetime, ath.Currency, 
		sum(
				CASE
				WHEN ath.TranType in ('GAME_PLAY', 'GAME_WIN', 'GAME_BET', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
				THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
				ELSE 0
				END) as PNL,
		sum(
				CASE
				WHEN ((ath.TranType = 'GAME_PLAY' AND (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) < 0) OR
					ath.TranType in ('GAME_BET','STAKE_DEC', 'REFUND'))
				THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
				ELSE 0
				END) as Handle
	from admin_all.AccountTranAggregate ath
	where (
					ath.TranType in ('GAME_PLAY', 'GAME_WIN', 'GAME_BET', 'CASH_OUT', 'STAKE_DEC', 'REFUND', 'TIPS')  OR
					(ath.TranType = 'BONUS_REL' AND ath.ProductID <> -1 AND ath.GameID <> '-UNKNOWN-')
			) and Period = 'Y'
			and ath.BrandID in (select b.BrandID from #Brands b)
			and ath.Date >= @YearFrom
			and ath.Date < @YearTo
	group by ath.Date, ath.Currency
	order by 1, 2
	*/
end

go
--exec rpt.GetBrandRevenueByYearCurrency '2011-01-01', '2020-01-01','508,509,517,522,40,41,42,43,44,45,46,47,48,50,127,1,2,3,4,5,6,7,8,9,17,22,23,29,33,49,55,79,90,98,105,113,117,128,135,140,150,472,473,477,482,483,484,486,519,525,526,527,528,529,530,531,533,534,131,10,11,12,13,14,15,16,21,24,25,26,27,28,34,35,36,37,38,39,51,52,53,54,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,80,81,82,83,84,85,86,87,88,89,91,92,93,94,95,96,97,99,100,101,102,103,111,112,114,115,116,118,119,120,129,130,132,133,134,136,137,138,139,141,142,143,144,145,146,147,148,149,151,152,179,301,314,315,316,317,345,346,347,348,349,350,474,475,476,478,479,480,481,485,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,514,515,516,518,520,521,523,524,532,'


--select cast(BRANDID as varchar(20)) + ',' from admin.CASINO_BRAND_DEF for xml path('')