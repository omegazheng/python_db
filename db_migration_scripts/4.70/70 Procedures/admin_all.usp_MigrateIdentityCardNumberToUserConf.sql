set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_MigrateIdentityCardNumberToUserConf]
AS
  BEGIN
    UPDATE external_mpt.USER_CONF
    set
        external_mpt.USER_CONF.NATIONAL_REG_NUMBER = admin_all.USER_DOCUMENTS.NUMBER
    from external_mpt.USER_CONF left join admin_all.USER_DOCUMENTS
                                          on USER_CONF.PARTYID = USER_DOCUMENTS.PARTYID
    where type in ('Identity Card High-resolution',
                   'Carta d''Identità', '"ID"', 'Identity Card', 'ID')
      and external_mpt.USER_CONF.NATIONAL_REG_NUMBER is not null ;

  END

go
