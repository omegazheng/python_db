SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('admin_all.usp_CalculateAccountTranAggregate') is null
	exec('create procedure admin_all.usp_CalculateAccountTranAggregate as --')
go
alter procedure admin_all.usp_CalculateAccountTranAggregate
as
begin
	
	set nocount, xact_abort on
	declare @Period char(1), @Date date
	declare c cursor local static for
		select Period, Date
		from admin_all.AccountTranAggregatePendingCalculation
		order by Period, Date --calulate day first, then month, then year
	open c
	fetch next from c into @Period, @Date
	while @@fetch_status = 0
	begin
		begin transaction

		if @Period = 'D'
		begin
			;with t as
			(
				select  *
				from admin_all.AccountTranAggregate
				where Period = @Period
					and Date = @Date
			),
			s as
			(
				select AggregateType, BrandID, PartyID, Currency, TranType, ProductID, GameID, sum(AmountReal) as AmountReal, sum(AmountReleasedBonus) AmountReleasedBonus, sum(AmountPlayableBonus) AmountPlayableBonus,sum(GameCount)GameCount, sum(TranCount) TranCount, 
						sum(BonusPlanTran_AmountPlayableBonus) BonusPlanTran_AmountPlayableBonus, sum(BonusPlanTran_AmountPlayableBonusWinnings) BonusPlanTran_AmountPlayableBonusWinnings, sum(BonusPlanTran_AmountReleasedBonus) BonusPlanTran_AmountReleasedBonus, sum(BonusPlanTran_AmountReleasedBonusWinnings) BonusPlanTran_AmountReleasedBonusWinnings, sum(RawLoyalty) RawLoyalty
				from admin_all.AccountTranHourlyAggregate
				where Datetime >= cast(@Date as datetime)
					and Datetime < cast(dateadd(day, 1, @Date) as datetime)
				group by AggregateType, BrandID, PartyID, Currency, TranType, ProductID, GameID
			)
			merge t
			using s on t.AggregateType = s.AggregateType and t.PartyID = s.PartyID and t.TranType = s.TranType and t.ProductID = s.ProductID and t.GameID = s.GameID
			when not matched then
				insert (Period, AggregateType, Date, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, TranCount, GameCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)
					values(@Period, AggregateType, @Date, s.BrandID, s.PartyID, s.Currency, s.TranType, s.ProductID, s.GameID, s.AmountReal, s.AmountReleasedBonus, s.AmountPlayableBonus, s.TranCount, s.GameCount, s.BonusPlanTran_AmountPlayableBonus, s.BonusPlanTran_AmountPlayableBonusWinnings, s.BonusPlanTran_AmountReleasedBonus, s.BonusPlanTran_AmountReleasedBonusWinnings, s.RawLoyalty)
			when matched and (s.Currency <> t.Currency or s.AmountReal <> t.AmountReal or s.AmountReleasedBonus <> t.AmountReleasedBonus or s.AmountPlayableBonus <> t.AmountPlayableBonus  or s.TranCount <> t.TranCount or s.GameCount <> t.GameCount or isnull(s.BonusPlanTran_AmountPlayableBonus, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonus,0) or isnull(s.BonusPlanTran_AmountPlayableBonusWinnings, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonusWinnings, 0) or isnull(s.BonusPlanTran_AmountReleasedBonus, 0) <> isnull(t.BonusPlanTran_AmountReleasedBonus, 0) or isnull(s.BonusPlanTran_AmountReleasedBonusWinnings, 0)<> isnull(t.BonusPlanTran_AmountReleasedBonusWinnings, 0) or isnull(s.RawLoyalty,0) <> isnull(t.RawLoyalty, 0)) then 
				update set t.Currency = s.Currency, t.AmountReal = s.AmountReal, t.AmountReleasedBonus = s.AmountReleasedBonus, t.AmountPlayableBonus = s.AmountPlayableBonus, t.TranCount = s.TranCount, t.GameCount = s.GameCount, t.BonusPlanTran_AmountPlayableBonus = s.BonusPlanTran_AmountPlayableBonus, t.BonusPlanTran_AmountPlayableBonusWinnings = s.BonusPlanTran_AmountPlayableBonusWinnings, t.BonusPlanTran_AmountReleasedBonus = s.BonusPlanTran_AmountReleasedBonus, t.BonusPlanTran_AmountReleasedBonusWinnings = s.BonusPlanTran_AmountReleasedBonusWinnings, t.RawLoyalty = s.RawLoyalty
			when not matched by source then 
				delete
			;
		end
		else
		begin
			;with t as
			(
				select  *
				from admin_all.AccountTranAggregate
				where Period = @Period
					and Date = @Date
			),
			s as
			(
				select AggregateType, BrandID, PartyID, Currency, TranType, ProductID, GameID, sum(AmountReal) as AmountReal, sum(AmountReleasedBonus) AmountReleasedBonus, sum(AmountPlayableBonus) AmountPlayableBonus,sum(GameCount)GameCount, sum(TranCount) TranCount,
						sum(BonusPlanTran_AmountPlayableBonus) BonusPlanTran_AmountPlayableBonus, sum(BonusPlanTran_AmountPlayableBonusWinnings) BonusPlanTran_AmountPlayableBonusWinnings, sum(BonusPlanTran_AmountReleasedBonus) BonusPlanTran_AmountReleasedBonus, sum(BonusPlanTran_AmountReleasedBonusWinnings) BonusPlanTran_AmountReleasedBonusWinnings, sum(RawLoyalty) RawLoyalty
				from admin_all.AccountTranAggregate
				where Date >= @Date
					and Date < case when @Period = 'M' then dateadd(month, 1, @Date) else dateadd(year, 1, @Date) end
					and Period = case when @Period = 'M' then 'D' else 'M' end
				group by AggregateType, BrandID, PartyID, Currency, TranType, ProductID, GameID
			)
			merge t
			using s on t.AggregateType = s.AggregateType and t.PartyID = s.PartyID and t.TranType = s.TranType and t.ProductID = s.ProductID and t.GameID = s.GameID
			when not matched then
				insert (Period, AggregateType, Date, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, TranCount, GameCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)
					values(@Period, AggregateType, @Date, s.BrandID, s.PartyID, s.Currency, s.TranType, s.ProductID, s.GameID, s.AmountReal, s.AmountReleasedBonus, s.AmountPlayableBonus, s.TranCount, s.GameCount, s.BonusPlanTran_AmountPlayableBonus, s.BonusPlanTran_AmountPlayableBonusWinnings, s.BonusPlanTran_AmountReleasedBonus, s.BonusPlanTran_AmountReleasedBonusWinnings, s.RawLoyalty)
			when matched and (s.Currency <> t.Currency or s.AmountReal <> t.AmountReal or s.AmountReleasedBonus <> t.AmountReleasedBonus or s.AmountPlayableBonus <> t.AmountPlayableBonus  or s.TranCount <> t.TranCount or s.GameCount <> t.GameCount or isnull(s.BonusPlanTran_AmountPlayableBonus, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonus,0) or isnull(s.BonusPlanTran_AmountPlayableBonusWinnings, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonusWinnings, 0) or isnull(s.BonusPlanTran_AmountReleasedBonus, 0) <> isnull(t.BonusPlanTran_AmountReleasedBonus, 0) or isnull(s.BonusPlanTran_AmountReleasedBonusWinnings, 0)<> isnull(t.BonusPlanTran_AmountReleasedBonusWinnings, 0) or isnull(s.RawLoyalty,0) <> isnull(t.RawLoyalty, 0)) then 
				update set t.Currency = s.Currency, t.AmountReal = s.AmountReal, t.AmountReleasedBonus = s.AmountReleasedBonus, t.AmountPlayableBonus = s.AmountPlayableBonus, t.TranCount = s.TranCount, t.GameCount = s.GameCount, t.BonusPlanTran_AmountPlayableBonus = s.BonusPlanTran_AmountPlayableBonus, t.BonusPlanTran_AmountPlayableBonusWinnings = s.BonusPlanTran_AmountPlayableBonusWinnings, t.BonusPlanTran_AmountReleasedBonus = s.BonusPlanTran_AmountReleasedBonus, t.BonusPlanTran_AmountReleasedBonusWinnings = s.BonusPlanTran_AmountReleasedBonusWinnings, t.RawLoyalty = s.RawLoyalty
			when not matched by source then 
				delete
			;
		end
		delete admin_all.AccountTranAggregatePendingCalculation where Period = @Period and Date = @Date
		commit 
		fetch next from c into @Period, @Date
	end
	close c
	deallocate c
end
go

--exec admin_all.usp_CalculateAccountTranAggregate