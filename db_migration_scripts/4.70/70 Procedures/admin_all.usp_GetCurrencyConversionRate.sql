SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetCurrencyConversionRate
(
  @CurrencyFrom nvarchar(10),
	@CurrencyTo nvarchar(10)
)
as

BEGIN
  select RateID rateId, CurrencyFrom currencyFrom, CurrencyTo currencyTo,
         Rate rate, EffectiveDate effectiveDate, LastUpdateDate lastUpdateDate
 from CurrencyConversionRateCurrent
  where CurrencyFrom =  @CurrencyFrom and CurrencyTo = @CurrencyTo
END
