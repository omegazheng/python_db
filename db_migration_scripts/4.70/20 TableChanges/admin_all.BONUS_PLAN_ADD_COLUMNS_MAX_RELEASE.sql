SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN]') = object_id and name = 'MAX_RELEASE_ENABLED')
  BEGIN
    ALTER TABLE [admin_all].[BONUS_PLAN]
      ADD [MAX_RELEASE_ENABLED] bit NULL;
  END
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN]') = object_id and name = 'MAX_RELEASE_MULTIPLIER_TYPE')
  BEGIN
    ALTER TABLE [admin_all].[BONUS_PLAN]
      ADD [MAX_RELEASE_MULTIPLIER_TYPE] nvarchar(30) NULL;
  END
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN]') = object_id and name = 'MAX_RELEASE_MULTIPLIER_VALUE')
  BEGIN
    ALTER TABLE [admin_all].[BONUS_PLAN]
      ADD [MAX_RELEASE_MULTIPLIER_VALUE] numeric(38,18) NULL;
  END
GO


