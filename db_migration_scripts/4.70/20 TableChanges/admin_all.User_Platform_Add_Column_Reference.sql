SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_PLATFORM]') = object_id and name = 'REFERENCE')
  BEGIN
    ALTER TABLE [admin_all].[USER_PLATFORM]
      ADD [REFERENCE] nvarchar(4000) NULL;
  END
GO



