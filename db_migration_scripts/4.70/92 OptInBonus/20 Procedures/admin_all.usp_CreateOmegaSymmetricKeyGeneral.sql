set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CreateOmegaSymmetricKeyGeneral
as
begin
	set nocount on
	if not exists(select * from sys.symmetric_keys where name = 'OmegaSymmetricKeyGeneral')
	begin
		declare @SQL nvarchar(max) ='create symmetric key OmegaSymmetricKeyGeneral with algorithm = AES_256 encryption by password = '+quotename(admin_all.fn_GetOmegaSymmetricKeyGeneralPassword(), '''')+';';
		exec(@SQL)
	end
end