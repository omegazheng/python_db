set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_SaveBonus]
	(
		@BonusID int,
		@PartyID int,
		@BonusPlanID int,
		@TriggerDate datetime,
		@ExpiryDate datetime,
		@ReleaseDate datetime,
		@Status varchar(50),
		@AmountWagered numeric(38,18),
		@WagerRequirement numeric(38,18),
		@Amount numeric(38,18),
		@PaymentID int,
		@AmountWithdrawn numeric(38,18),
		@PlayableBonus numeric(38,18) = 0,
		@PlayableBonusWinnings numeric(38,18) = 0,
		@ReleasedBonus numeric(38,18) = 0,
		@ReleasedBonusWinnings numeric(38,18) = 0,
    @ReleasedBonusAmount numeric(38,18) = 0,
		@ReleasedBonusWinningsAmount numeric(38,18) = 0,
		@BonusIncrementalID int,
		@ExternalBonusId nvarchar(50),
		@OptInDate datetime
	)
as
	begin
		set nocount on
		set xact_abort on

		begin transaction

		UPDATE
			admin_all.bonus
		SET
      PARTYID = @PartyID,
      BONUS_PLAN_ID = @BonusPlanID,
      TRIGGER_DATE = @TriggerDate,
      EXPIRY_DATE = @ExpiryDate,
      RELEASE_DATE = @ReleaseDate,
      STATUS = @Status,
      AMOUNT_WAGERED = @AmountWagered,
      WAGER_REQUIREMENT = @WagerRequirement,
      AMOUNT = @Amount,
      PAYMENT_ID = @PaymentID,
      AMOUNT_WITHDRAWN = @AmountWithdrawn,
      PLAYABLE_BONUS = @PlayableBonus,
      PLAYABLE_BONUS_WINNINGS = @PlayableBonusWinnings,
      RELEASED_BONUS = @ReleasedBonus,
      RELEASED_BONUS_WINNINGS =  @ReleasedBonusWinnings,
      RELEASED_BONUS_AMOUNT = @ReleasedBonusAmount,
      RELEASED_BONUS_WINNINGS_AMOUNT = @ReleasedBonusWinningsAmount,
      BONUS_INCREMENTAL_ID = @BonusIncrementalID,
      EXTERNAL_BONUS_ID = @ExternalBonusId,
      OptInDate = @OptInDate
		WHERE id = @BonusID

		commit

	end
go
