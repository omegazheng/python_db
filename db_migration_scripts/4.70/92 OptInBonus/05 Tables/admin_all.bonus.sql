set ansi_nulls, quoted_identifier on
go
if not exists(select * from sys.columns where name  = 'OptInDate' and object_id = object_id('admin_all.bonus'))
begin 
	alter table admin_all.Bonus add OptInDate datetime
end
go

if  exists(select * from sys.indexes where object_id = object_id('admin_all.Bonus') and name = 'IDX_admin_all_BONUS_PaymentID')
    begin
        drop index IDX_admin_all_BONUS_PaymentID on admin_all.BONUS
    end
go

if  object_id('admin_all.FK_admin_all_BONUS_PAYMENT_PaymentID') is not null
    begin
        alter table admin_all.BONUS drop constraint FK_admin_all_BONUS_PAYMENT_PaymentID
    end
go


if exists(select * from sys.columns where name  = 'PaymentID' and object_id = object_id('admin_all.bonus'))
begin 
	alter table admin_all.Bonus drop column PaymentID
end
go

if not exists(select * from sys.indexes where object_id = object_id('admin_all.Bonus') and name = 'IDX_admin_all_BONUS_OptInDate')
begin
	create index IDX_admin_all_BONUS_OptInDate on admin_all.BONUS(OptInDate)
end
go


