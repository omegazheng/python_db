set ansi_nulls, quoted_identifier on
go
--drop table admin_all.OptInBonusHandle
go
if object_id('admin_all.OptInBonusHandle') is null
begin
	create table admin_all.OptInBonusHandle
	(
		OptInHandle uniqueidentifier not null,
		BonusID int not null,
		constraint PK_admin_all_OptInBonusHandle primary key(OptInHandle), 
		constraint UQ_admin_all_OptInBonusHandle unique(BonusID)
	)
end
go




--select * from admin_all.BONUS where PARTYID = 91433326 
--select * from admin_all.BONUS_PLAN where id = 3

--select * from admin_all.BONUS where STATUS = 'ACTIVE'