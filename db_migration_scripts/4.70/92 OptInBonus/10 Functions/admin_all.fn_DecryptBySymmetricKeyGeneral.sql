set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_DecryptBySymmetricKeyGeneral(@CipherText varbinary(max))
returns varbinary(max)
as
begin
	return DecryptByKey(@CipherText)
end
go
