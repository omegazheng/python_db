set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_UpdateSessionIPAddresses(
    @id int,
    @ip varchar(50)
)

as
begin
    set nocount, xact_abort  on
    declare @primaryPartyId int
    declare @rows int

    set @primaryPartyId = (select PARTYID from admin_all.USER_WEB_SESSION where id = @id)
    if @primaryPartyId is null return

    select AssociatedPartyId from external_mpt.UserAssociatedAccount where PARTYID = @primaryPartyId
    set @rows = @@rowcount

    if @rows = 0 or @rows = 1 update admin_all.USER_WEB_SESSION set ip = @ip where id = @id
    if @rows > 1
        begin
            update admin_all.USER_WEB_SESSION set ip = @ip where PARTYID in (
                select AssociatedPartyId from external_mpt.UserAssociatedAccount where PARTYID = @primaryPartyId
            )
        end
end

go