set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_RemoveUserWebSessions(
    @partyid int
)

as
begin
    set nocount, xact_abort  on
    declare @rows int
    if @partyid is null return

    select AssociatedPartyId from external_mpt.UserAssociatedAccount where PARTYID = @partyid
    set @rows = @@rowcount

    if @rows = 0 or @rows = 1 delete from admin_all.USER_WEB_SESSION where PARTYID = @partyid
    if @rows > 1
        begin
            delete from admin_all.USER_WEB_SESSION where PARTYID in (
                select AssociatedPartyId from external_mpt.UserAssociatedAccount where PARTYID = @partyid
            )
        end
end

go