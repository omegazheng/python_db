set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_UpdateAllSessionsLastAccess(
    @partyid int
)

as
begin
    set nocount, xact_abort  on
    declare @rows int
    declare @now datetime

    if @partyid is null return
    set @now = GETDATE()

    select AssociatedPartyId from external_mpt.UserAssociatedAccount where PARTYID = @partyid
    set @rows = @@rowcount

    if @rows = 0 or @rows = 1 update admin_all.USER_WEB_SESSION set LAST_ACCESS_TIME = @now where PARTYID = @partyid
    if @rows > 1
        begin
            update admin_all.USER_WEB_SESSION set LAST_ACCESS_TIME = @now where PARTYID in (
                select AssociatedPartyId from external_mpt.UserAssociatedAccount where PARTYID = @partyid
            )
        end
end

go