set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_IpsCreatePrimaryAssociateAccount
(
	@PartyID int
)
as
begin
	set nocount, xact_abort on
	begin transaction
	select @PartyID = PartyID from external_mpt.UserAssociatedAccount where AssociatedPartyID = @PartyID
	if @@rowcount = 0
	begin
		insert into external_mpt.UserAssociatedAccount(AssociatedPartyID, PartyID, IsPrimary)
			values(@PartyID, @PartyID, 1)
    select @@rowcount	
	end
	commit
end

go
