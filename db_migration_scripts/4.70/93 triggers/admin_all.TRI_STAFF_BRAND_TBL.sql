set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_STAFF_BRAND_TBL on admin_all.STAFF_BRAND_TBL
for update, delete, insert
as
begin
	if @@rowcount = 0
		return
	set nocount on
	insert into admin_all.STAFF_ACTION_LOG(STAFFID, DATE, ACTION_TYPE, ACTION_FIELD, OLD_VALUE, NEW_VALUE, COMMENT)
		select isnull(i.STAFFID, d.STAFFID), getdate(), 'DATA_EDIT', 'BRANDID', cast(d.BRANDID as nvarchar(20)), cast(i.BRANDID as nvarchar(20)), 'App:' + isnull(app_name(), 'unknown') + '. Host:' + isnull(host_name(), 'unknnown') + '. Login:' + isnull(system_user, 'unknown') + '.'
			--' IP:' + isnull((select top 1 client_net_address from sys.dm_exec_connections where session_id = @@SPID), 'Unkown') --- remove IP since we might not have view state permission
		from inserted i
			full outer join deleted d on i.STAFFID = d.STAFFID and i.BRANDID = d.BRANDID
		where i.BRANDID is null or d.BRANDID is null
end