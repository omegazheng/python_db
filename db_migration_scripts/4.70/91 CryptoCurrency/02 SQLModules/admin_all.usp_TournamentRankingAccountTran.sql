
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.usp_TournamentRankingAccountTran') is null
	exec('create procedure admin_all.usp_TournamentRankingAccountTran as --')
GO
ALTER PROCEDURE admin_all.usp_TournamentRankingAccountTran
(
    @BrandID              INT,
    @StartDate            DATETIME,
    @TournamentIDsForUser VARCHAR(255) = NULL,
    @TournamentIDsForGame VARCHAR(255) = NULL
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @SQL NVARCHAR(MAX)
    CREATE TABLE #UserTournamentIDs (
      ID INT PRIMARY KEY
    )
    CREATE TABLE #GameTournamentIDs (
      ID INT PRIMARY KEY
    )

    INSERT INTO #UserTournamentIDs (ID)
      SELECT cast(Item AS INT)
      FROM [admin_all].fc_splitDelimiterString(@TournamentIDsForUser, ',') a
      WHERE isnumeric(item) = 1
            AND nullif(rtrim(item), '') IS NOT NULL

    INSERT INTO #GameTournamentIDs (ID)
      SELECT cast(Item AS INT)
      FROM [admin_all].fc_splitDelimiterString(@TournamentIDsForGame, ',') a
      WHERE isnumeric(item) = 1
            AND nullif(rtrim(item), '') IS NOT NULL
    CREATE TABLE #Accounts (
      ACCOUNT_ID INT      NOT NULL,
      PARTYID    INT      NOT NULL,
      CURRENCY   NVARCHAR(10) NOT NULL,
      PRIMARY KEY (ACCOUNT_ID)
    )
    INSERT INTO #Accounts (ACCOUNT_ID, PARTYID, CURRENCY)
      SELECT
        a.ID,
        a.PARTYID,
        u.CURRENCY
      FROM external_mpt.USER_CONF u
        INNER JOIN admin_all.ACCOUNT a ON a.PARTYID = u.PARTYID
      WHERE u.BRANDID = @BrandID
            AND u.WINNERS_LIST = 1
	if exists(select* from #GameTournamentIDs)
	begin
		select  PLATFORM_ID, GAME_ID into #GameTournamentPlatform
        from admin_all.TOURNAMENT_GAME tg
			inner join #GameTournamentIDs x on x.ID = tg.TOURNAMENT_ID		
		create index idx on #GameTournamentPlatform(Platform_id, Game_ID)
	end
	create table #Ret
	(
		ID bigint, ACCOUNT_ID int, DATETIME datetime, TRAN_TYPE varchar(10), AMOUNT_REAL numeric(38,8),
		AMOUNT_RELEASED_BONUS numeric(38, 18), GAME_TRAN_ID nvarchar(100), GAME_ID varchar(100), PLATFORM_ID int,
		BRAND_ID int, PARTYID int, CURRENCY nvarchar(10)
	)
    SELECT @SQL = '
    SELECT
      at.ID,
      at.ACCOUNT_ID,
      at.DATETIME,
      (CASE
          WHEN at.TRAN_TYPE = ''SYSTEM_EFT'' THEN ''GAME_WIN''
          WHEN at.TRAN_TYPE = ''MACHIN_EFT'' THEN ''GAME_BET'' ELSE at.TRAN_TYPE END) as TRAN_TYPE,
      at.AMOUNT_REAL,
      at.AMOUNT_RELEASED_BONUS,
      at.GAME_TRAN_ID,
      at.GAME_ID,
      at.PLATFORM_ID,
      at.BRAND_ID,
      u.PARTYID,
      RTRIM(u.CURRENCY) AS CURRENCY
    FROM #Accounts u
		inner join admin_all.ACCOUNT_TRAN at with(forceseek) on at.ACCOUNT_ID = u.ACCOUNT_ID
    WHERE at.DATETIME >= @StartDate
           AND at.TRAN_TYPE IN (''GAME_BET'', ''GAME_WIN'', ''SYSTEM_EFT'', ''MACHIN_EFT'')
           AND at.ROLLED_BACK = 0
                  ' + CASE WHEN exists(SELECT *
                                       FROM #UserTournamentIDs)
      THEN
        'AND exists(select  *
            from admin_all.TOURNAMENT_USER tu
              inner join #UserTournamentIDs x on x.ID = tu.TOURNAMENT_ID
            where u.PartyID = tu.PartyID
            )'
                      ELSE ''
                      END + '
                  ' + CASE WHEN exists(SELECT *
                                       FROM #GameTournamentIDs)
      THEN
        'AND exists(
			select  *
            from #GameTournamentPlatform tg
            where at.GAME_ID = tg.GAME_ID
              and at.PLATFORM_ID = tg.PLATFORM_ID
            )'
                      ELSE ''
                      END
	--print @SQL
    
	declare @MaxRowCount int = 10000000
	insert into #Ret(ID, ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, AMOUNT_RELEASED_BONUS, GAME_TRAN_ID, GAME_ID, PLATFORM_ID, BRAND_ID, PARTYID, CURRENCY)
    EXEC sp_executesql @SQL, N'@BrandID int, @StartDate datetime', @BrandID, @StartDate
	if @@rowcount <= @MaxRowCount
	begin
		select * from #Ret
		return
	end
    raiserror('Returning record count exceed %d.', 16, 1, @MaxRowCount)
END
go

--exec admin_all.usp_TournamentRankingAccountTran 23, '2018-08-01'

--select brandid,count(*)
--from external_mpt.USER_CONF
--group by BRANDID
--order by 2 desc