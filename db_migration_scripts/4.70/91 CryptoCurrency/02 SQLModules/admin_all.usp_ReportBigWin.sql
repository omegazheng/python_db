set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_ReportBigWin 
(
	@StartTime datetime,
	@EndTime datetime, 
	@BrandIDs varchar(max) = null,
	@Currencies varchar(max) = null,
	@PlatformIDs varchar(max) = null,
	@PayoutRate numeric(18,4) = null,
	@WinAmount numeric(18,4) = null,
	@ReturnCountOfRecords bit = 0,
	@OrderBy nvarchar(max) = null
)
as
begin
	set nocount on
	declare @SQL nvarchar(max)
	create table #BrandIDs(BrandID int primary key)
	create table #PlatformIDs(PlatFormID int primary key)
	create table #Currencies(Currency nvarchar(10) collate database_default primary key)
	insert into #BrandIDs(BrandID)
		select distinct cast(Item as int)
		from admin_all.fc_splitDelimiterString(@BrandIDs,',') 
		where rtrim(Item)<> '' 
			and isnumeric(Item) = 1
	insert into #PlatformIDs(PlatFormID)
		select distinct cast(Item as int)
		from admin_all.fc_splitDelimiterString(@PlatformIDs,',') 
		where rtrim(Item)<> '' 
			and isnumeric(Item) = 1
	insert into #Currencies(Currency)
		select distinct ltrim(rtrim(replace(Item, '''', '')))
		from admin_all.fc_splitDelimiterString(@Currencies,',') 
		where rtrim(Item)<> '' 
	
	select @OrderBy = isnull(rtrim(case when @ReturnCountOfRecords = 0 then @OrderBy end ), '')
	---sum(Win)/sum(Bet) x 100%
	-- GAME_BET is the start time, amount_real * -1
	-- GAME_WIN is the end time, amount_real
	--net_win_loss, GAME_WIN- GAME_BET
	-- GAME_TRAN_ID to identify a game run
	select @SQL = 'SELECT '+
					case when @ReturnCountOfRecords = 1 then ' count(*) count'
						else
						'gameinstance.GAME_TRAN_ID as GAMEINSTANCE, gameinstance.PLATFORM_ID as productId, gameinstance.GAME_ID as gameId,userConf.partyId as PARTYID,
						userConf.userid AS USERID,  userConf.CURRENCY AS currency,
						gameInstance.GAME_TRAN_ID AS gameInstanceId,
						gameInstance.start_time AS startTime,	gameInstance.end_time AS endTime,
						gameInstance.bet_amount AS betAmount, gameInstance.win_amount AS winAmount, gameInstance.net_win_loss AS netWinLoss,
   				 gameInstance.payout_rate AS payoutRate,
						gameInfo.NAME AS gameName, platform.name AS platformName
					'end +'
FROM (
		select min(A.ID) ID, A.GAME_ID, A.ACCOUNT_ID, A.PLATFORM_ID, A.GAME_TRAN_ID,
				sum(case when a.TRAN_TYPE = ''GAME_BET'' then -a.AMOUNT_REAL end) BET_AMOUNT,
				sum(case when a.TRAN_TYPE = ''GAME_WIN'' then a.AMOUNT_REAL end) WIN_AMOUNT,
				sum( a.AMOUNT_REAL) NET_WIN_LOSS,
				max(case when a.TRAN_TYPE = ''GAME_BET'' then a.DATETIME end) as START_TIME,
				max(case when a.TRAN_TYPE = ''GAME_WIN'' then a.DATETIME end) as END_TIME,
				case when sum(case when a.TRAN_TYPE = ''GAME_BET'' then -a.AMOUNT_REAL end) = 0 then 0
					else
						sum(case when a.TRAN_TYPE = ''GAME_WIN'' then a.AMOUNT_REAL end)*100.0/sum(case when a.TRAN_TYPE = ''GAME_BET'' then -a.AMOUNT_REAL end)
				end as PAYOUT_RATE
		from admin_all.account_tran a
		where a.TRAN_TYPE in (''GAME_BET'', ''GAME_WIN'')
			and a.ROLLED_BACK = 0
			and a.datetime >= @StartTime
			and a.datetime <= @EndTime
		group by A.GAME_ID, A.ACCOUNT_ID, A.PLATFORM_ID, A.GAME_TRAN_ID
	) gameInstance
	inner join admin_all.account account on gameInstance.account_id = account.id
	inner join external_mpt.USER_CONF userConf on account.PARTYID = userConf.PARTYID
	left join admin_all.game_info gameInfo on gameInstance.game_id = gameInfo.game_id and gameInstance.PLATFORM_ID = gameInfo.PLATFORM_ID
	inner join admin_all.platform platform on platform.id = gameInstance.platform_id
WHERE 1=1
	' + case when exists(select * from #BrandIDs) then 'and userConf.brandid in (select b.BrandID from #BrandIDs b) ' else '' end + '
	' + case when exists(select * from #Currencies) then 'and userConf.currency in (select c.Currency from #Currencies c) ' else '' end + '
	' + case when exists(select * from #PlatformIDs) then 'and gameInstance.platform_id in (select p.PlatFormID from #PlatformIDs p) ' else '' end + '
	' + case when @PayoutRate is not null then 'and gameInstance.payout_rate >= @PayoutRate' else '' end + '
	' + case when @WinAmount is not null then 'and gameInstance.win_amount >= @WinAmount' else '' end + '
' + case when @OrderBy <> '' then 'order by ' + @OrderBy else '' end
	print @SQL
	exec sp_executesql @SQL, N'@StartTime datetime, @EndTime datetime, @PayoutRate numeric(18,4), @WinAmount numeric(18,4) ', @StartTime, @EndTime, @PayoutRate, @WinAmount
end

go
