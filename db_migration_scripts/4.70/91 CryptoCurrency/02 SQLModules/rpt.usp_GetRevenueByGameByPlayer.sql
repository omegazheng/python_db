set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetRevenueByGameByPlayer
(
	@AggregateType tinyint,
	@StartDate datetime,
	@EndDate datetime,
	@GameID varchar(100), 
	@ProductID int,
	@Currencies nvarchar(max),
	@Brands nvarchar(max),
	@VIPLevels nvarchar(max),
	@Countries nvarchar(max),
	@FundTypes nvarchar(max), -- REAL, RELEASED_BONUS, PLAYABLE_BONUS
	@IncludeInGameRelease bit = 0,
	@UseRegistrationDate bit = 0,
	@AllowNULLVIPLevels bit = 0
)
as
begin
	set nocount on
	create table #Currencies (Currency nvarchar(10) primary key)
	create table #Brands(BrandID int primary key, BrandName varchar(100))
	create table #VIPStatuses(VIP_Status int primary key)
	create table #Countries (Country char(2) primary key)
	create table #User(PartyID int, UserID nvarchar(100), BrandID int, BrandName varchar(100))
	insert into #Brands(BrandID, BrandName)
		select BRANDID, BRANDNAME
		from admin.CASINO_BRAND_DEF
		where BRANDID in (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@Brands,','))
	insert into #VIPStatuses(VIP_Status)
		select cast(Item as int)  
		from admin_all.fc_splitDelimiterString(@VIPLevels,',')
	insert into #Currencies(Currency)
		select Item 
		from admin_all.fc_splitDelimiterString(@Currencies,',')
	insert into #Countries(Country)
		select Item 
		from admin_all.fc_splitDelimiterString(@Currencies,',')
	declare @SQL nvarchar(max), @Handle varchar(max), @Hold varchar(max)
	select @Handle = '', @Hold = ''

	;with x0 as
	(
		select isnull(rtrim(ltrim(Item)), '') FundType
		from admin_all.fc_splitDelimiterString(@FundTypes,',') a
	)
	select 
		@Handle =	@Handle + 
					case 
						when FundType = 'REAL' then '+HandleReal'
						when FundType = 'RELEASED_BONUS' then '+HandleReleasedBonus'--+case when @IncludeInGameRelease = 1 then '+InGameReleasedBonusRelease' else '' end
						when FundType = 'PLAYABLE_BONUS' then '+HandlePlayableBonus'--+case when @IncludeInGameRelease = 1 then '+InGamePlayableBonusRelease' else '' end
					end,
		@Hold =	@Hold + 
					case 
						when FundType = 'REAL' then '+PNLReal'
						when FundType = 'RELEASED_BONUS' then '+PNLReleasedBonus'+case when @IncludeInGameRelease = 1 then '+InGameReleasedBonusRelease' else '' end
						when FundType = 'PLAYABLE_BONUS' then '+PNLPlayableBonus'+case when @IncludeInGameRelease = 1 then '+InGamePlayableBonusRelease' else '' end
					end
	from x0
	where FundType <> ''
	select @Hold = stuff(@hold, 1,1, ''), @Handle = stuff(@Handle, 1, 1, '')
	if @UseRegistrationDate = 1
	begin 
		select @SQL = 'select u.PARTYID, u.USERID, b.BrandID, b.BrandName
	from external_mpt.USER_CONF u
		inner join #Brands b on b.BrandID = u.BRANDID
	where exists(select * from #Countries c where c.Country = u.Country)
		and ' +
			case 
				when @AllowNULLVIPLevels = 0 then 'exists(select * from #VIPStatuses v where v.VIP_Status = u.VIP_STATUS)' 
				else '(exists(select * from #VIPStatuses v where v.VIP_Status = u.VIP_STATUS) or u.VIP_STATUS is null)' 
			end +
			case when @UseRegistrationDate = 1 then '
		and u.REG_DATE >= @StartDate 
		and u.REG_DATE< @EndDate '
				else ''
			end
		insert into #User(PartyID, userID, BrandID, BrandName)
			exec sp_executesql @SQL, N'@StartDate datetime, @EndDate datetime', @StartDate, @EndDate 
	end
	
	select @SQL = cast('
with p as
( '+
	case 
		when @UseRegistrationDate = 1 then 
			'select * from #User ' 
		else '
	select u.PartyID, u.UserID, b.BrandID, b.BrandName
	from external_mpt.USER_CONF u
		inner join #Brands b on b.BrandID = u.BRANDID
	where exists(select * from #Countries c where c.Country = u.Country)
		and ' +
			case 
				when @AllowNULLVIPLevels = 0 then 'exists(select * from #VIPStatuses v where v.VIP_Status = u.VIP_STATUS)' 
				else '(exists(select * from #VIPStatuses v where v.VIP_Status = u.VIP_STATUS) or u.VIP_STATUS is null)' 
			end
		end as nvarchar(max)) +'
), d as
(
	select	ath.PartyID, ath.Currency, p.UserID, p.BrandID, p.BrandName,
			sum('+@Handle+') as Handle,
			sum('+@Hold+') as Hold,
			sum(ath.GameCount) GameCount,
			sum(BetCount) BetCount
	from admin_all.v_PlayerAggregateBase ath
		inner join p on p.PartyID = ath.PartyID
		inner join #Currencies c on c.Currency = ath.Currency
	where ath.AggregateType = @AggregateType
		'+case when @UseRegistrationDate = 0 then 'and ath.Datetime >= @StartDate AND ath.Datetime < @EndDate' else '' end +'
	group by ath.PartyID, ath.Currency, p.UserID, p.BrandID, p.BrandName
)
select *
from d
order by hold desc
	
'
	--exec maint.PrintString @sql
	exec sp_executesql @SQL, N'@AggregateType tinyint, @StartDate datetime, @EndDate datetime', @AggregateType, @StartDate, @EndDate 
end

go
