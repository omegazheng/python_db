set ansi_nulls, quoted_identifier on
go
if object_id('[chronos].[Staff_Interface]') is null
	exec('create view [chronos].[Staff_Interface] as select 1 as one')
go
alter view [chronos].[Staff_Interface] 
as 
	select 
				cast([PartyID] as int) as [PartyID],
				cast([UserID] as varchar(100)) as [UserID],
				cast([Nickname] as varchar(100)) as [Nickname],
				cast([Email] as varchar(100)) as [Email],
				cast([FirstName] as varchar(100)) as [FirstName],
				cast([LastName] as varchar(100)) as [LastName],
				cast([Mobile] as varchar(100)) as [Mobile],
				cast([Language] as nvarchar(10)) as [Language],
				cast([RegistrationDate] as datetime) as [RegistrationDate],
				cast([VIPStatus] as int) as [VIPStatus],
				cast(UserType as int) UserType,
				cast([Hierarchy] as hierarchyid) as [Hierarchy],  
				1 as Active,
				___IsDeleted___ 
	from [chronos].[Party]
go
--select * from admin_all.STAFF_AUTH_TBL

--select  * from maint.ReplicationConfiguration
----delete maint.ReplicationLastExecution where ConfigurationID = 294
--select  * from maint.ReplicationLastExecution where ConfigurationID = 294
--select  * from maint.ReplicationHistory where ConfigurationID = 294 order by 1 desc
--select * from chronos.Party --where usertype is not null
----select * from external_mpt.USER_CONF 