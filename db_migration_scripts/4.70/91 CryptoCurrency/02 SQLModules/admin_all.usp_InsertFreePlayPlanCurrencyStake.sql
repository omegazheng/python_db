set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_InsertFreePlayPlanCurrencyStake]
  (
    @FREEPLAY_PLAN_ID INT,
    @CURRENCY nvarchar(10),
    @BET_PER_ROUND numeric(38,18)
  )
as
  begin
    DECLARE @ID bigint

    insert into admin_all.FREEPLAY_PLAN_CURRENCY_STAKE
    (
      FREEPLAY_PLAN_ID,
      CURRENCY,
      BET_PER_ROUND
    )
    values
      (
        @FREEPLAY_PLAN_ID,
        @CURRENCY,
        @BET_PER_ROUND
      )

    select @ID = @@IDENTITY
    select  id as id,
            freeplay_plan_id as freePlayPlanId,
            currency as currency,
            isnull(bet_per_round,0) as betPerRound
    from admin_all.FREEPLAY_PLAN_CURRENCY_STAKE where ID = @ID

  end

go
