set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_addCurrencyAccount
(
	@PartyID int,
	@Currency nvarchar(10),
	@NewPartyID int = null output
)
as
begin
	set nocount, xact_abort on
	declare @SQL nvarchar(max), @UserID nvarchar(100), 	@ParentID int, @BrandID int
	select @NewPartyID = null
	begin transaction
	select @NewPartyID = 1
	select @PartyID = PartyID from external_mpt.UserAssociatedAccount where AssociatedPartyID = @PartyID
	if @@rowcount = 0
	begin
		insert into external_mpt.UserAssociatedAccount(AssociatedPartyID, PartyID, IsPrimary)
			values(@PartyID, @PartyID, 1)
	end
	select @NewPartyID = u.PARTYID
	from external_mpt.USER_CONF u
		inner join external_mpt.UserAssociatedAccount ua on u.PARTYID = ua.AssociatedPartyID
	where ua.PartyID = @PartyID
		and u.CURRENCY = @Currency

	if @@rowcount >0
		goto ___Exit___
	select @UserID = UserID, @ParentID = ParentID,  @BrandID = BRANDID from external_mpt.USER_CONF where PARTYID = @PartyID
	if @@rowcount = 0
	begin
		raiserror('Could not find party %d', 16, 1, @PartyID)
		goto ___Exit___
	end
	if @ParentID is not null
	begin
		select @ParentID = u.PARTYID 
		from external_mpt.UserAssociatedAccount ua1
			inner join external_mpt.UserAssociatedAccount ua on ua1.PartyID = ua.PartyID
			inner join  external_mpt.USER_CONF u on ua.AssociatedPartyID = u.PARTYID
		where ua1.AssociatedPartyID = @ParentID
			and u.CURRENCY = @Currency
	end
	select @UserID = @UserID +'___' + @Currency
	select @SQL = cast('insert into external_mpt.USER_CONF(CURRENCY, USERID, PASSWORD, ParentID' as nvarchar(max))
					+ (select ', ' + quotename(name) from sys.columns where object_id = object_id('external_mpt.USER_CONF') and is_identity =0 and is_computed = 0 and name not in ('PARTYID', 'CURRENCY', 'USERID', 'PASSWORD', 'ParentID') order by column_id for xml path(''), type).value('.', 'nvarchar(max)')
					+ ')
'
					+'select @CURRENCY, @USERID, newid(), @ParentID'
					+ (select ', ' + quotename(name) from sys.columns where object_id = object_id('external_mpt.USER_CONF') and is_identity =0 and is_computed = 0 and name not in ('PARTYID', 'CURRENCY', 'USERID', 'PASSWORD', 'ParentID') order by column_id for xml path(''), type).value('.', 'nvarchar(max)')
					+' from external_mpt.USER_CONF where PARTYID = @PartyID;
'
					+ 'select @NewPartyID = scope_identity()'
	exec sp_executesql @SQL, N'@CURRENCY nvarchar(10), @USERID nvarchar(100), @PartyID int, @ParentID int, @NewPartyID int output', @Currency, @UserID, @PartyID, @ParentID, @NewPartyID output

  insert into external_mpt.UserAssociatedAccount (PartyID, AssociatedPartyID, IsPrimary)
	values (@PartyID, @NewPartyID, 0)

	insert into admin_all.account (PARTYID) values (@NewPartyID)

___Exit___:
	commit
end

go
