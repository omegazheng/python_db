set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getCashLiability]
  (
    @startDate DATETIME,
    @endDate   DATETIME,
    @currency  NVARCHAR(10),
    @brands    VARCHAR(255)
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @currencyLocal NVARCHAR(10)
    DECLARE @brandsLocal VARCHAR(255)
    SET @startDateLocal = @startDate
    SET @endDateLocal = @endDate
    SET @currencyLocal = @currency
    SET @brandsLocal = @brands
    SELECT
      CURRENCY,
      SUMMARY_DATE,
      SUM(OPENING_LIABILITY)   AS OPENING_LIABILITY,
      SUM(DEPOSIT)             AS DEPOSIT,
      SUM(FEE)                 AS FEE,
      SUM(TIP)                 AS TIP,
      SUM(WITHDRAWAL_APPROVED) AS WITHDRAWAL_APPROVED,
      SUM(ADJUSTMENT)          AS ADJUSTMENT,
      SUM(TRANSFER)            AS TRANSFER,
      SUM(HOUSE_HOLD)          AS HOUSE_HOLD,
      SUM(CLOSING_LIABILITY)   AS CLOSING_LIABILITY,
      SUM(PENDING_WITHDRAWAL)  AS PENDING_WITHDRAWAL,
      SUM(CLOSING_BALANCE)     AS CLOSING_BALANCE,
      SUM(UNDERFLOW)           AS UNDERFLOW,
      SUM(CLOSING_LIABILITY -
          (OPENING_LIABILITY + DEPOSIT - WITHDRAWAL_APPROVED + ADJUSTMENT + TRANSFER - HOUSE_HOLD + UNDERFLOW - FEE -
           TIP))               AS CHECKSUM
    FROM
      (
        SELECT
          a.CURRENCY                                                     AS CURRENCY,
          a.SUMMARY_DATE                                                 AS SUMMARY_DATE,
          (a.OPENING_BALANCE + a.OPENING_BALANCE_RELEASED_BONUS) +
          (b.WITHDRAWAL_PENDING + b.WITHDRAWAL_PENDING_RELEASED_BONUS)   AS OPENING_LIABILITY,
          a.DEPOSIT                                                      AS DEPOSIT,
          a.FEE                                                          AS FEE,
          (a.TIPS + a.TIPS_RELEASED_BONUS)                               AS TIP,
          (a.WITHDRAWAL_APPROVED + a.WITHDRAWAL_APPROVED_RELEASED_BONUS) AS WITHDRAWAL_APPROVED,
          (a.ADJUSTMENT + a.ADJUSTMENT_BONUS)                            AS ADJUSTMENT,
          (a.TRANSFER + a.TRANSFER_RELEASED_BONUS)                       AS TRANSFER,
          (a.HOUSE_HOLD + a.HOUSE_HOLD_RELEASED_BONUS)                   AS HOUSE_HOLD,
          (a.CLOSING_BALANCE + a.CLOSING_BALANCE_RELEASED_BONUS) +
          (a.WITHDRAWAL_PENDING + a.WITHDRAWAL_PENDING_RELEASED_BONUS)   AS CLOSING_LIABILITY,
          (a.WITHDRAWAL_PENDING + a.WITHDRAWAL_PENDING_RELEASED_BONUS)   AS PENDING_WITHDRAWAL,
          (a.CLOSING_BALANCE + a.CLOSING_BALANCE_RELEASED_BONUS)         AS CLOSING_BALANCE,
          a.UNDERFLOW
        FROM
          v_cash_liability a,
          v_cash_liability b
        WHERE
          a.CURRENCY = b.CURRENCY
          AND (@currencyLocal IS NULL OR a.CURRENCY = @currencyLocal)
          AND (@currencyLocal IS NULL OR b.CURRENCY = @currencyLocal)
          AND a.SUMMARY_DATE = DateAdd("d", 1, b.SUMMARY_DATE)
          AND a.BRANDID = b.BRANDID
          AND a.SUMMARY_DATE >= @startDateLocal AND a.SUMMARY_DATE < @endDateLocal
          AND (@brandsLocal IS NULL OR a.brandid IN (@brandsLocal))
      ) c
    GROUP BY SUMMARY_DATE, CURRENCY
    ORDER BY SUMMARY_DATE
  END

go
