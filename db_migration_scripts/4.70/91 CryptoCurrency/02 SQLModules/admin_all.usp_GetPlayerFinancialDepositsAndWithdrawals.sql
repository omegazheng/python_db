set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerFinancialDepositsAndWithdrawals]
    (@partyid int)
AS
BEGIN
    SET DATEFIRST 1
    declare @currency nvarchar(10) = (select currency from external_mpt.user_conf where partyid=@partyid)
    DECLARE @startDate DATETIME = DATEADD(YEAR, 100, 0)
    DECLARE @endDate DATETIME = GETDATE()

    SELECT *
    INTO #Temp
    FROM (
        SELECT
          'Deposit'    AS TRAN_TYPE,
          CURRENCY,
          CAST(SUMMARY_DATE AS DATE) DATE,
          sum(deposit) AS AMOUNT
        FROM
          admin_all.v_accounting_player_daily WITH ( NOLOCK )
        WHERE
          summary_date >= @startDate
        AND summary_date <= @endDate
        AND partyid = @partyId
        AND currency = @currency
        GROUP BY CAST(SUMMARY_DATE AS DATE), CURRENCY

        UNION ALL

        SELECT
          'Withdrawal'    AS TRAN_TYPE,
          CURRENCY,
          CAST(SUMMARY_DATE AS DATE) DATE,
          sum(withdrawal) AS AMOUNT
        FROM
          admin_all.v_accounting_player_daily WITH ( NOLOCK )
        WHERE
          summary_date >= @startDate
        AND summary_date <= @endDate
        AND partyid = @partyId
        AND currency = @currency
        GROUP BY CAST(SUMMARY_DATE AS DATE), CURRENCY
    ) FINANCIAL

    SELECT
      tt.PERIOD                     AS PERIOD,
      tt.CURRENCY                   AS CURRENCY,
      tt.TRAN_TYPE                  AS TRAN_TYPE,
      SUM(tt.AMOUNT)                AS AMOUNT
    FROM (
           SELECT top 5
             'TODAY'               AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT                AS AMOUNT
           FROM #Temp
           WHERE DATE = CAST(@endDate AS DATE)

           UNION

           SELECT top 5
             'YESTERDAY'           AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT                AS AMOUNT
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDate AS DATE))

           UNION

           SELECT top 5
             'WTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT)                AS AMOUNT
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDate), @endDate) AS DATE)
                 AND DATE <= CAST(@endDate AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

           UNION

           SELECT top 5
             'MTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT)                AS AMOUNT
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDate), 0) AS DATE)
                 AND DATE <= CAST(@endDate AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

           UNION

           SELECT top 5
             'LTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT)                AS AMOUNT
           FROM #Temp
           WHERE DATE <= CAST(@endDate AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

         ) tt
    GROUP BY tt.PERIOD, tt.CURRENCY, TRAN_TYPE

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END

END

go
