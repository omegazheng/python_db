
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]') and name = 'CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE] alter column [CURRENCY] nvarchar(10)  not null;