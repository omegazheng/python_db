if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[DW_CASH_LIABILITY_SUMMARY]') and name = 'PK_admin_all_DW_CASH_LIABILITY_SUMMARY')
	alter table [admin_all].[DW_CASH_LIABILITY_SUMMARY] add constraint[PK_admin_all_DW_CASH_LIABILITY_SUMMARY] primary key clustered([SUMMARY_DATE],[BRANDID],[CURRENCY]);
go