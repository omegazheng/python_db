set ansi_nulls, quoted_identifier on
go
if not exists(select * from sys.columns where object_id = object_id('[external_mpt].[USER_CONF]') and name = 'CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
begin
	begin try
		begin transaction
		exec('alter view [external_mpt].[v_UserPrimaryCurrency] --with schemabinding
as
	select a.ID as AssociatedAccountID, aa.AssociatedPartyID, aa.PartyID, u.Currency, u.BrandID, ubc.Currency AssociatedCurrency
	from external_mpt.UserAssociatedAccount aa
		inner join external_mpt.USER_CONF u on u.PARTYID = aa.PartyID
		inner join admin_all.ACCOUNT a on a.PARTYID = aa.AssociatedPartyID
		inner join external_mpt.UserBrandCurrency ubc on ubc.PartyID = aa.AssociatedPartyID')
		exec('alter table [external_mpt].[USER_CONF] alter column [CURRENCY] nvarchar(10)  not null;')

		if not exists(select * from sys.columns where object_id = object_id('[external_mpt].[UserBrandCurrency]') and name = 'Currency' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
			alter table [external_mpt].[UserBrandCurrency] alter column [Currency] nvarchar(10)  not null;

		exec('alter view [external_mpt].[v_UserPrimaryCurrency] with schemabinding
as
	select a.ID as AssociatedAccountID, aa.AssociatedPartyID, aa.PartyID, u.Currency, u.BrandID, ubc.Currency AssociatedCurrency
	from external_mpt.UserAssociatedAccount aa
		inner join external_mpt.USER_CONF u on u.PARTYID = aa.PartyID
		inner join admin_all.ACCOUNT a on a.PARTYID = aa.AssociatedPartyID
		inner join external_mpt.UserBrandCurrency ubc on ubc.PartyID = aa.AssociatedPartyID')
		exec('create unique clustered index PK_external_mpt_v_UserPrimaryCurrency on external_mpt.v_UserPrimaryCurrency(AssociatedPartyID);
create index IDX_external_mpt_v_UserPrimaryCurrency_AssociatedAccountID on external_mpt.v_UserPrimaryCurrency(AssociatedAccountID)include (Currency, PartyID, BrandID);
create index IDX_external_mpt_v_UserPrimaryCurrency_Currency on external_mpt.v_UserPrimaryCurrency(Currency)include (PartyID, AssociatedAccountID, BrandID);
create index IDX_external_mpt_v_UserPrimaryCurrency_PartyID on external_mpt.v_UserPrimaryCurrency(PartyID);')


		commit
	end try
	begin catch
		rollback;
		throw;
	end catch
end