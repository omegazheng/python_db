
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[CURRENCY_CONV_RATE]') and name = 'TO_CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[CURRENCY_CONV_RATE] alter column [TO_CURRENCY] nvarchar(10)  not null;