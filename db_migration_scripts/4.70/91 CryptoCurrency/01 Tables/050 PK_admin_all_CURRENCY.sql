if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[CURRENCY]') and name = 'PK_admin_all_CURRENCY')
	alter table [admin_all].[CURRENCY] add constraint[PK_admin_all_CURRENCY] primary key clustered([iso_code]);
go