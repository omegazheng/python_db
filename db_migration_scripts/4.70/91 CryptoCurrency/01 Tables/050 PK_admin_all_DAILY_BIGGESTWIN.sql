if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[DAILY_BIGGESTWIN]') and name = 'PK_admin_all_DAILY_BIGGESTWIN')
	alter table [admin_all].[DAILY_BIGGESTWIN] add constraint[PK_admin_all_DAILY_BIGGESTWIN] primary key clustered([DATE],[BRAND_ID],[CURRENCY],[PLATFORM_ID],[GAME_ID]);
go