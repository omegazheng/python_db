if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BONUS_PLAN_CURRENCY]') and name = 'IDX_admin_all_Bonus_Plan_Currency_Bonus_Plan_ID')
	create nonclustered index [IDX_admin_all_Bonus_Plan_Currency_Bonus_Plan_ID] on [admin_all].[BONUS_PLAN_CURRENCY]([BONUS_PLAN_ID]) include([CURRENCY_CODE]);
go