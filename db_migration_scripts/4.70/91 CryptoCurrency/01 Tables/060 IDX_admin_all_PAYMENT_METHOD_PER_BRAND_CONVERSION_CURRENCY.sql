if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[PAYMENT_METHOD_PER_BRAND]') and name = 'IDX_admin_all_PAYMENT_METHOD_PER_BRAND_CONVERSION_CURRENCY')
	create nonclustered index [IDX_admin_all_PAYMENT_METHOD_PER_BRAND_CONVERSION_CURRENCY] on [admin_all].[PAYMENT_METHOD_PER_BRAND]([CONVERSION_CURRENCY]) ;
go