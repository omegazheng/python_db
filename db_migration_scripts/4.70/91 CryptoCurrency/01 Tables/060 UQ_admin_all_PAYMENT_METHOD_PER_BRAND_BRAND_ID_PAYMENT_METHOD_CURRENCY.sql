if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[PAYMENT_METHOD_PER_BRAND]') and name = 'UQ_admin_all_PAYMENT_METHOD_PER_BRAND_BRAND_ID_PAYMENT_METHOD_CURRENCY')
	alter table [admin_all].[PAYMENT_METHOD_PER_BRAND] add constraint[UQ_admin_all_PAYMENT_METHOD_PER_BRAND_BRAND_ID_PAYMENT_METHOD_CURRENCY] unique nonclustered([BRAND_ID],[PAYMENT_METHOD],[CURRENCY]);
go