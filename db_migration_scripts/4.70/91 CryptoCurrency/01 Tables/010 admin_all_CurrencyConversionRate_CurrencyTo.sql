
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[CurrencyConversionRate]') and name = 'CurrencyTo' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[CurrencyConversionRate] alter column [CurrencyTo] nvarchar(10)  not null;