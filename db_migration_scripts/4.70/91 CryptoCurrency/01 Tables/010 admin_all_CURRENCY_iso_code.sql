
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[CURRENCY]') and name = 'iso_code' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[CURRENCY] alter column [iso_code] nvarchar(10)  not null;