if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[SPORTSBOOK_BET_DETAILS]') and name = 'IDX_admin_all_SPORTSBOOK_BET_DETAILS_PLATFORM_CODE')
	create nonclustered index [IDX_admin_all_SPORTSBOOK_BET_DETAILS_PLATFORM_CODE] on [admin_all].[SPORTSBOOK_BET_DETAILS]([PLATFORM_CODE]) include([ID],[CURRENCY],[AMOUNT],[TYPE],[WAGER_ID]);
go