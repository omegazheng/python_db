if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[CURRENCY_CONV_RATE]') and name = 'IDX_admin_all_CURRENCY_CONV_RATE_CURRENCY_CONV_ID_TO_CURRENCY')
	create nonclustered index [IDX_admin_all_CURRENCY_CONV_RATE_CURRENCY_CONV_ID_TO_CURRENCY] on [admin_all].[CURRENCY_CONV_RATE]([CURRENCY_CONV_ID],[TO_CURRENCY]) include([RATE]);
go