if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BONUS_PLAN_CURRENCY]') and name = 'IDX_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CODE')
	create nonclustered index [IDX_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CODE] on [admin_all].[BONUS_PLAN_CURRENCY]([CURRENCY_CODE]) ;
go