if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BRAND_CURRENCY]') and name = 'UQ_admin_all_BRAND_CURRENCY_BRANDID_ISO_CODE')
	alter table [admin_all].[BRAND_CURRENCY] add constraint[UQ_admin_all_BRAND_CURRENCY_BRANDID_ISO_CODE] unique nonclustered([BRANDID],[ISO_CODE]);
go