if not exists(select * from sys.indexes i where object_id = object_id('[chronos].[UserAggregate]') and name = 'PK_Chronos_UserAggregate')
	alter table [chronos].[UserAggregate] add constraint[PK_Chronos_UserAggregate] primary key clustered([PartyID],[Currency]);
go