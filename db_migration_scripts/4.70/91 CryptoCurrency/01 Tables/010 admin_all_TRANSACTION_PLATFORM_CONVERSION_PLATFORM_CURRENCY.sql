
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[TRANSACTION_PLATFORM_CONVERSION]') and name = 'PLATFORM_CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[TRANSACTION_PLATFORM_CONVERSION] alter column [PLATFORM_CURRENCY] nvarchar(10)  not null;