if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[DW_POKER]') and name = 'PK_admin_all_DW_POKER')
	alter table [admin_all].[DW_POKER] add constraint[PK_admin_all_DW_POKER] primary key clustered([SUMMARY_DATE],[PARTYID],[CURRENCY]);
go