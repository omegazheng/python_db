if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[AccountTranCurrencyExtension]') and name = 'PK_admin_all_AccountTranCurrencyExtension')
	alter table [admin_all].[AccountTranCurrencyExtension] add constraint[PK_admin_all_AccountTranCurrencyExtension] primary key clustered([Currency],[AccountTranID]);
go