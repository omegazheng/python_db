if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]') and name = 'UQ_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_FREEPLAY_PLAN_ID_CURRENCY')
	alter table [admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE] add constraint[UQ_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_FREEPLAY_PLAN_ID_CURRENCY] unique nonclustered([FREEPLAY_PLAN_ID],[CURRENCY]);
go