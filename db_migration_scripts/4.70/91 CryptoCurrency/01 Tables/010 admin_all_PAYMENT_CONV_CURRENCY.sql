
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[PAYMENT]') and name = 'CONV_CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[PAYMENT] alter column [CONV_CURRENCY] nvarchar(10) ;