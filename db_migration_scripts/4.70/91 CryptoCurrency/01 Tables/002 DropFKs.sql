set xact_abort on
begin transaction
if object_id('[admin_all].[FK_admin_all_BATCH_WITHDRAWAL_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[BATCH_WITHDRAWAL]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[BATCH_WITHDRAWAL] drop constraint [FK_admin_all_BATCH_WITHDRAWAL_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CURRENCY_CODE]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[BONUS_PLAN_CURRENCY]') and name = 'CURRENCY_CODE' and max_length < 20)
	alter table [admin_all].[BONUS_PLAN_CURRENCY] drop constraint [FK_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CURRENCY_CODE];
if object_id('[admin_all].[FK_admin_all_BRAND_CCLEVEL_LIMIT_CURRENCY_ISO_CODE]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[BRAND_CCLEVEL_LIMIT]') and name = 'ISO_CODE' and max_length < 20)
	alter table [admin_all].[BRAND_CCLEVEL_LIMIT] drop constraint [FK_admin_all_BRAND_CCLEVEL_LIMIT_CURRENCY_ISO_CODE];
if object_id('[admin_all].[FK_admin_all_BRAND_CURRENCY_CURRENCY_ISO_CODE]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[BRAND_CURRENCY]') and name = 'ISO_CODE' and max_length < 20)
	alter table [admin_all].[BRAND_CURRENCY] drop constraint [FK_admin_all_BRAND_CURRENCY_CURRENCY_ISO_CODE];
if object_id('[admin_all].[FK_admin_all_BRAND_JACKPOTS_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[BRAND_JACKPOTS]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[BRAND_JACKPOTS] drop constraint [FK_admin_all_BRAND_JACKPOTS_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_BRAND_PLATFORM_CURRENCY_CURRENCY_ISO_CODE]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[BRAND_PLATFORM_CURRENCY]') and name = 'ISO_CODE' and max_length < 20)
	alter table [admin_all].[BRAND_PLATFORM_CURRENCY] drop constraint [FK_admin_all_BRAND_PLATFORM_CURRENCY_CURRENCY_ISO_CODE];
if object_id('[admin_all].[FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_FROM_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CONVERSION_RATE_TEMP]') and name = 'FROM_CURRENCY' and max_length < 20)
	alter table [admin_all].[CONVERSION_RATE_TEMP] drop constraint [FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_FROM_CURRENCY];
if object_id('[admin_all].[FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_TO_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CONVERSION_RATE_TEMP]') and name = 'TO_CURRENCY' and max_length < 20)
	alter table [admin_all].[CONVERSION_RATE_TEMP] drop constraint [FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_TO_CURRENCY];
if object_id('[admin_all].[FK_admin_all_CURRENCY_CONV_CURRENCY_BASE_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CURRENCY_CONV]') and name = 'BASE_CURRENCY' and max_length < 20)
	alter table [admin_all].[CURRENCY_CONV] drop constraint [FK_admin_all_CURRENCY_CONV_CURRENCY_BASE_CURRENCY];
if object_id('[admin_all].[FK_admin_all_CURRENCY_CONV_RATE_CURRENCY_TO_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CURRENCY_CONV_RATE]') and name = 'TO_CURRENCY' and max_length < 20)
	alter table [admin_all].[CURRENCY_CONV_RATE] drop constraint [FK_admin_all_CURRENCY_CONV_RATE_CURRENCY_TO_CURRENCY];
if object_id('[admin_all].[FK_admin_all_CurrencyConversionRate_CurrencyFrom_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CurrencyConversionRate]') and name = 'CurrencyFrom' and max_length < 20)
	alter table [admin_all].[CurrencyConversionRate] drop constraint [FK_admin_all_CurrencyConversionRate_CurrencyFrom_CURRENCY];
if object_id('[admin_all].[FK_admin_all_CurrencyConversionRate_CurrencyTo_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CurrencyConversionRate]') and name = 'CurrencyTo' and max_length < 20)
	alter table [admin_all].[CurrencyConversionRate] drop constraint [FK_admin_all_CurrencyConversionRate_CurrencyTo_CURRENCY];
if object_id('[admin_all].[FK_admin_all_DEPOSIT_LIMIT_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[DEPOSIT_LIMIT]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[DEPOSIT_LIMIT] drop constraint [FK_admin_all_DEPOSIT_LIMIT_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE] drop constraint [FK_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_PAYMENT_CURRENCY_CONV_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[PAYMENT]') and name = 'CONV_CURRENCY' and max_length < 20)
	alter table [admin_all].[PAYMENT] drop constraint [FK_admin_all_PAYMENT_CURRENCY_CONV_CURRENCY];
if object_id('[admin_all].[FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CONVERSION_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[PAYMENT_METHOD_PER_BRAND]') and name = 'CONVERSION_CURRENCY' and max_length < 20)
	alter table [admin_all].[PAYMENT_METHOD_PER_BRAND] drop constraint [FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CONVERSION_CURRENCY];
if object_id('[admin_all].[FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[PAYMENT_METHOD_PER_BRAND]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[PAYMENT_METHOD_PER_BRAND] drop constraint [FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLATFORM_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[PLATFORM_CURRENCY]') and name = 'PLATFORM_CURRENCY' and max_length < 20)
	alter table [admin_all].[PLATFORM_CURRENCY] drop constraint [FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLATFORM_CURRENCY];
if object_id('[admin_all].[FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLAYER_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[PLATFORM_CURRENCY]') and name = 'PLAYER_CURRENCY' and max_length < 20)
	alter table [admin_all].[PLATFORM_CURRENCY] drop constraint [FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLAYER_CURRENCY];
if object_id('[admin_all].[FK_admin_all_TOURNAMENT_CURRENCY_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[TOURNAMENT_CURRENCY]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[TOURNAMENT_CURRENCY] drop constraint [FK_admin_all_TOURNAMENT_CURRENCY_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_TOURNAMENT_POINTS_ACC_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[TOURNAMENT_POINTS_ACC]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[TOURNAMENT_POINTS_ACC] drop constraint [FK_admin_all_TOURNAMENT_POINTS_ACC_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_TRANSACTION_PLATFORM_CONVERSION_CURRENCY_PLATFORM_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[TRANSACTION_PLATFORM_CONVERSION]') and name = 'PLATFORM_CURRENCY' and max_length < 20)
	alter table [admin_all].[TRANSACTION_PLATFORM_CONVERSION] drop constraint [FK_admin_all_TRANSACTION_PLATFORM_CONVERSION_CURRENCY_PLATFORM_CURRENCY];
if object_id('[admin_all].[FK_admin_all_WITHDRAWAL_LIMIT_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[WITHDRAWAL_LIMIT]') and name = 'CURRENCY' and max_length < 20)
	alter table [admin_all].[WITHDRAWAL_LIMIT] drop constraint [FK_admin_all_WITHDRAWAL_LIMIT_CURRENCY_CURRENCY];
if object_id('[external_mpt].[FK_external_mpt_USER_CONF_CURRENCY_CURRENCY_CURRENCY]') is not null and exists(select * from sys.columns c where object_id = object_id('[external_mpt].[USER_CONF]') and name = 'CURRENCY' and max_length < 20)
	alter table [external_mpt].[USER_CONF] drop constraint [FK_external_mpt_USER_CONF_CURRENCY_CURRENCY_CURRENCY];
if object_id('[admin_all].[FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyFrom]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CurrencyConversionRate]') and name = 'CurrencyFrom' and max_length < 20)
	alter table [admin_all].[CurrencyConversionRate] drop constraint [FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyFrom];
if object_id('[admin_all].[FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyTo]') is not null and exists(select * from sys.columns c where object_id = object_id('[admin_all].[CurrencyConversionRate]') and name = 'CurrencyTo' and max_length < 20)
	alter table [admin_all].[CurrencyConversionRate] drop constraint [FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyTo];
commit
