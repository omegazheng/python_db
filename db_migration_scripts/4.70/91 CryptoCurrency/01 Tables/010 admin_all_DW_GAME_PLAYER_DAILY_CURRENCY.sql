
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[DW_GAME_PLAYER_DAILY]') and name = 'CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[DW_GAME_PLAYER_DAILY] alter column [CURRENCY] nvarchar(10) ;