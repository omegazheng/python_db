if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[PAYMENT]') and name = 'IDX_admin_all_PAYMENT_CONV_CURRENCY')
	create nonclustered index [IDX_admin_all_PAYMENT_CONV_CURRENCY] on [admin_all].[PAYMENT]([CONV_CURRENCY]) ;
go