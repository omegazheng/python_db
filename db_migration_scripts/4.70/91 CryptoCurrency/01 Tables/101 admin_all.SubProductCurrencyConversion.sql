set ansi_nulls, quoted_identifier on
go

if object_id('admin_all.SubProductCurrencyConversion') is null
begin
	CREATE TABLE admin_all.SubProductCurrencyConversion
	(
		ID int identity(1,1) PRIMARY KEY,
		SubProductID int not null ,
		ConvertCurrencies nvarchar(1000) not null ,
		ToCurrency nvarchar(10) not null
		CONSTRAINT UK_admin_all_SubProductCurrencyConversion_SubProductID UNIQUE(SubProductID),
		CONSTRAINT FK_admin_all_SubProductCurrencyConversion_SubProductID FOREIGN KEY(SubProductID) REFERENCES admin_all.SUB_PLATFORM(ID),
        CONSTRAINT FK_admin_all_SubProductCurrencyConversion_ToCurrency FOREIGN KEY(ToCurrency) REFERENCES admin_all.CURRENCY(iso_code)
	)
end

GO

