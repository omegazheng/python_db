if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[TRANSACTION_PLATFORM_CONVERSION]') and name = 'IDX_admin_all_TRANSACTION_PLATFORM_CONVERSION_PLATFORM_CURRENCY')
	create nonclustered index [IDX_admin_all_TRANSACTION_PLATFORM_CONVERSION_PLATFORM_CURRENCY] on [admin_all].[TRANSACTION_PLATFORM_CONVERSION]([PLATFORM_CURRENCY]) ;
go