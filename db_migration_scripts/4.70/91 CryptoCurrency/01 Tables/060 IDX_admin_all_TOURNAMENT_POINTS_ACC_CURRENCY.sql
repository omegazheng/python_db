if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[TOURNAMENT_POINTS_ACC]') and name = 'IDX_admin_all_TOURNAMENT_POINTS_ACC_CURRENCY')
	create nonclustered index [IDX_admin_all_TOURNAMENT_POINTS_ACC_CURRENCY] on [admin_all].[TOURNAMENT_POINTS_ACC]([CURRENCY]) ;
go