if exists(select * from sys.columns where object_id = object_id('[admin_all].[STAFF_AUTH_TBL]') and name = 'TWO_FA_ALWAYS_REQUIRED')
    update admin_all.STAFF_AUTH_TBL set TWO_FA_ALWAYS_REQUIRED = 0 where TWO_FA_ALWAYS_REQUIRED is null
go

