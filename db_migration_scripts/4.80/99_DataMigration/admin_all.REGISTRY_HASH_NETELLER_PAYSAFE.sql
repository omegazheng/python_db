SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM dbo.CUSTOMER WHERE  NAME in (N'MTL', N'REB', N'DEMO'))
    BEGIN

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'neteller.paysafe.payment.handles.url')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('neteller.paysafe.payment.handles.url', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'neteller.paysafe.payments.url')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('neteller.paysafe.payments.url', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'neteller.paysafe.standalone.credits.url')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('neteller.paysafe.standalone.credits.url', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'neteller.paysafe.clientId')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('neteller.paysafe.clientId', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'neteller.paysafe.clientSecret')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('neteller.paysafe.clientSecret', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'neteller.paysafe.deposit.successURL')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('neteller.paysafe.deposit.successURL', '/ps/pub/DepositGenericPSPCallback.action');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'neteller.paysafe.deposit.failURL')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('neteller.paysafe.deposit.failURL', '/ps/pub/DepositGenericPSPCallback.action');
            END

        IF  NOT EXISTS (SELECT * FROM admin_all.PAYMENT_METHOD WHERE  CODE in (N'NETELLER_V2'))
            BEGIN
                INSERT INTO admin_all.PAYMENT_METHOD (CODE, NAME, IS_MANUAL_BANK, PAYMENT_TIMEOUT, IS_CCLEVELREADY, IS_RG_ENABLED, IS_POPUP_REQ) VALUES ('NETELLER_V2', 'Neteller V2', 0, -1, 0, -1, 0);
            END

    END
