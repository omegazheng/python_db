exec maint.usp_CreateSynchronizationConfiguration
			@Name ='PreDeployment-admin_all.PAYMENT', 
			@InitializationQuery = 'select 
		[id], [TYPE], [METHOD], [REF_NUMBER], [IP_ADDRESS], 
		[REQUEST_DATE], [PROCESS_DATE], [STATUS], [AMOUNT], [ACCOUNT_ID], 
		[MISC], [INFO], [AMOUNT_REAL], [AMOUNT_RELEASED_BONUS], [REQ_BONUS_PLAN_ID], 
		[FEE], [WORLD_PAY_CARD_ID], [MANUAL_BANK_FIELDS], [MANUAL_BANK_AGENT_ID], [LAST_UPDATED_DATE], 
		[REJECTED_REASON], [IS_CONVERTED], [CONV_RATE], [CONV_MARGIN_PERCENT], [CONV_MARGIN_AMOUNT], 
		[CONV_AMOUNT], [CONV_CURRENCY], [CONV_RATE_AFTER_MARGIN], [REQUEST_AMOUNT], [REQUEST_METHOD], 
		[VOUCHER], [WITHDRAWAL_HISTORY_ID], [SUBMETHOD], cast(0 as binary(8)) as Rowversion, cast(0 as bit) ___IsDeleted___
from admin_all.PAYMENT', 
			@FilterProcedure = 'maint.usp_SynchronizationFilterChangeTracking', 
			@FilteredQuery = 'select 
		p.[id], p.[TYPE], p.[METHOD], p.[REF_NUMBER], p.[IP_ADDRESS], 
		p.[REQUEST_DATE], p.[PROCESS_DATE], p.[STATUS], p.[AMOUNT], p.[ACCOUNT_ID], 
		p.[MISC], p.[INFO], p.[AMOUNT_REAL], p.[AMOUNT_RELEASED_BONUS], p.[REQ_BONUS_PLAN_ID], 
		p.[FEE], p.[WORLD_PAY_CARD_ID], p.[MANUAL_BANK_FIELDS], p.[MANUAL_BANK_AGENT_ID], p.[LAST_UPDATED_DATE], 
		p.[REJECTED_REASON], p.[IS_CONVERTED], p.[CONV_RATE], p.[CONV_MARGIN_PERCENT], p.[CONV_MARGIN_AMOUNT], 
		p.[CONV_AMOUNT], p.[CONV_CURRENCY], p.[CONV_RATE_AFTER_MARGIN], p.[REQUEST_AMOUNT], p.[REQUEST_METHOD], 
		p.[VOUCHER], p.[WITHDRAWAL_HISTORY_ID], p.[SUBMETHOD], cast(0 as binary(8)) as Rowversion, cast(0 as bit) ___IsDeleted___
from changetable(changes admin_all.PAYMENT, @FromValue ) ct
	left join admin_all.PAYMENT p on p.id = ct.id
where ct.SYS_CHANGE_CONTEXT is null	', 
			@SourceTable = 'admin_all.PAYMENT', 
			@FilterField = null, 
			@TargetObject = 'admin_all.Payment_PreDeploy_Interface', 
			@TargetObjectFieldList = '[id], [TYPE], [METHOD], [REF_NUMBER], [IP_ADDRESS], [REQUEST_DATE], [PROCESS_DATE], [STATUS], [AMOUNT], [ACCOUNT_ID], [MISC], [INFO], [AMOUNT_REAL], [AMOUNT_RELEASED_BONUS], [REQ_BONUS_PLAN_ID], [FEE], [WORLD_PAY_CARD_ID], [MANUAL_BANK_FIELDS], [MANUAL_BANK_AGENT_ID], [LAST_UPDATED_DATE], [REJECTED_REASON], [IS_CONVERTED], [CONV_RATE], [CONV_MARGIN_PERCENT], [CONV_MARGIN_AMOUNT], [CONV_AMOUNT], [CONV_CURRENCY], [CONV_RATE_AFTER_MARGIN], [REQUEST_AMOUNT], [REQUEST_METHOD], [VOUCHER], [WITHDRAWAL_HISTORY_ID], [SUBMETHOD], [RowVersion], [___IsDeleted___]', 
			@BatchSize = 1500, 
			@IsActive = 1,
			@ClearProgress = 0