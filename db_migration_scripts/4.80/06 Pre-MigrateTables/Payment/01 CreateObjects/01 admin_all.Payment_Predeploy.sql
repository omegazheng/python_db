set ansi_nulls, quoted_identifier on
go
--select * from maint.fn_GetObjectDefinition('admin_all.Payment', 'admin_all.Payment_PreDeploy') where type not in ('AddColumn')

--drop table [admin_all].[Payment_PreDeploy]
go
if object_id('[admin_all].[Payment_PreDeploy]') is null
begin
	create table [admin_all].[Payment_PreDeploy]([id] int not null identity(1,1) ,[TYPE] varchar(25) not null,[METHOD] nvarchar(30) null,[REF_NUMBER] nvarchar(max) null,[IP_ADDRESS] varchar(100) null,[REQUEST_DATE] datetime null,[PROCESS_DATE] datetime null,[STATUS] varchar(25) null,[AMOUNT] numeric(38,18) not null constraint [DF_admin_all_Payment_PreDeploy_AMOUNT] default((0)),[ACCOUNT_ID] int not null,[MISC] nvarchar(2048) null,[INFO] nvarchar(2048) null,[AMOUNT_REAL] numeric(38,18) not null constraint [DF_admin_all_Payment_PreDeploy_AMOUNT_REAL] default((0)),[AMOUNT_RELEASED_BONUS] numeric(38,18) not null constraint [DF_admin_all_Payment_PreDeploy_AMOUNT_RELEASED_BONUS] default((0)),[REQ_BONUS_PLAN_ID] varchar(20) null,[FEE] numeric(38,18) not null constraint [DF_admin_all_Payment_PreDeploy_FEE] default((0)),[WORLD_PAY_CARD_ID] int null,[MANUAL_BANK_FIELDS] nvarchar(1000) null,[MANUAL_BANK_AGENT_ID] int null,[LAST_UPDATED_DATE] datetime null,[REJECTED_REASON] nvarchar(max) null,[IS_CONVERTED] bit not null constraint [DF_admin_all_Payment_PreDeploy_IS_CONVERTED] default((0)),[CONV_RATE] numeric(38,18) null,[CONV_MARGIN_PERCENT] numeric(38,18) null constraint [DF_admin_all_Payment_PreDeploy_CONV_MARGIN_PERCENT] default((0)),[CONV_MARGIN_AMOUNT] numeric(38,18) null constraint [DF_admin_all_Payment_PreDeploy_CONV_MARGIN_AMOUNT] default((0)),[CONV_AMOUNT] numeric(38,18) null,[CONV_CURRENCY] nvarchar(10) null,[CONV_RATE_AFTER_MARGIN] numeric(38,18) null,[REQUEST_AMOUNT] numeric(38,18) null,[REQUEST_METHOD] nvarchar(30) null,[VOUCHER] nvarchar(25) null,[WITHDRAWAL_HISTORY_ID] int null,[SUBMETHOD] nvarchar(100) null, RowVersion rowversion not null)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'PK_admin_all_Payment_PreDeploy')
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [PK_admin_all_Payment_PreDeploy] primary key clustered(id asc) with(pad_index = off)
end
go

if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_ACCOUNT_ID')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_ACCOUNT_ID] on [admin_all].[Payment_PreDeploy](ACCOUNT_ID asc) include(TYPE,METHOD,REQUEST_DATE,PROCESS_DATE,STATUS,AMOUNT) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_ACCOUNT_ID_TYPE_STATUS_PROCESS_DATE')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_ACCOUNT_ID_TYPE_STATUS_PROCESS_DATE] on [admin_all].[Payment_PreDeploy](ACCOUNT_ID asc,TYPE asc,STATUS asc,PROCESS_DATE asc) include(METHOD,REQUEST_DATE,AMOUNT) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_CONV_CURRENCY')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_CONV_CURRENCY] on [admin_all].[Payment_PreDeploy](CONV_CURRENCY asc) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_MANUAL_BANK_AGENT_ID')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_MANUAL_BANK_AGENT_ID] on [admin_all].[Payment_PreDeploy](MANUAL_BANK_AGENT_ID asc) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_METHOD_INFO')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_METHOD_INFO] on [admin_all].[Payment_PreDeploy](METHOD asc,INFO asc) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_PROCESS_DATE')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_PROCESS_DATE] on [admin_all].[Payment_PreDeploy](PROCESS_DATE asc) include(METHOD,AMOUNT,ACCOUNT_ID,FEE) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_TYPE_REQUEST_DATE_STATUS')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_TYPE_REQUEST_DATE_STATUS] on [admin_all].[Payment_PreDeploy](TYPE asc,REQUEST_DATE asc,STATUS asc) include(ACCOUNT_ID) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_TYPE_STATUS_ACCOUNT_ID')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_TYPE_STATUS_ACCOUNT_ID] on [admin_all].[Payment_PreDeploy](TYPE asc,STATUS asc,ACCOUNT_ID asc) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_WORLD_PAY_CARD_ID')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_WORLD_PAY_CARD_ID] on [admin_all].[Payment_PreDeploy](WORLD_PAY_CARD_ID asc) with(pad_index = off)
end
go
if not exists(select * from sys.indexes where object_id =  object_id('[admin_all].[Payment_PreDeploy]') and name = 'IDX_admin_all_Payment_PreDeploy_Rowversion')
begin
	create nonclustered index [IDX_admin_all_Payment_PreDeploy_Rowversion] on [admin_all].[Payment_PreDeploy](Rowversion asc) with(pad_index = off)
end
go
if object_id('[admin_all].[DF_admin_all_Payment_PreDeploy_AMOUNT]') is null
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [DF_admin_all_Payment_PreDeploy_AMOUNT] default ((0)) for [AMOUNT];
end
go
if object_id('[admin_all].[DF_admin_all_Payment_PreDeploy_AMOUNT_REAL]') is null
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [DF_admin_all_Payment_PreDeploy_AMOUNT_REAL] default ((0)) for [AMOUNT_REAL];
end
go
if object_id('[admin_all].[DF_admin_all_Payment_PreDeploy_AMOUNT_RELEASED_BONUS]') is null
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [DF_admin_all_Payment_PreDeploy_AMOUNT_RELEASED_BONUS] default ((0)) for [AMOUNT_RELEASED_BONUS];
end
go
if object_id('[admin_all].[DF_admin_all_Payment_PreDeploy_FEE]') is null
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [DF_admin_all_Payment_PreDeploy_FEE] default ((0)) for [FEE];
end
go
if object_id('[admin_all].[DF_admin_all_Payment_PreDeploy_IS_CONVERTED]') is null
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [DF_admin_all_Payment_PreDeploy_IS_CONVERTED] default ((0)) for [IS_CONVERTED];
end
go
if object_id('[admin_all].[DF_admin_all_Payment_PreDeploy_CONV_MARGIN_PERCENT]') is null
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [DF_admin_all_Payment_PreDeploy_CONV_MARGIN_PERCENT] default ((0)) for [CONV_MARGIN_PERCENT];
end
go
if object_id('[admin_all].[DF_admin_all_Payment_PreDeploy_CONV_MARGIN_AMOUNT]') is null
begin
	alter table [admin_all].[Payment_PreDeploy] add constraint [DF_admin_all_Payment_PreDeploy_CONV_MARGIN_AMOUNT] default ((0)) for [CONV_MARGIN_AMOUNT];
end
go