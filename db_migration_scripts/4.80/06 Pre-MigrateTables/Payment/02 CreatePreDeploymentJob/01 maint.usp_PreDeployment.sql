set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_PreDeployment
as
begin
	set nocount on
	begin try
		----Start pre-deployment
		if exists(select * from sys.columns where object_id = object_id('admin_all.PAYMENT') and name = 'RowVersion')
			return
		exec maint.usp_SynchronizeOne 'PreDeployment-admin_all.PAYMENT'
		----End pre-deployment
	end try
	begin catch
		declare @PreDeploymentError nvarchar(4000) = error_message()
		exec maint.usp_SendAlert @Subject = 'Predeployment failed', @Body = @PreDeploymentError;
		throw;
	end catch
end