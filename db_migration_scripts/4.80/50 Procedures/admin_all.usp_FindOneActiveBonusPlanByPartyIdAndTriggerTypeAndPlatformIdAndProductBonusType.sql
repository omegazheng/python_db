set ansi_nulls on
go
set quoted_identifier on
go
CREATE OR ALTER PROCEDURE  admin_all.usp_FindOneActiveBonusPlanByPartyIdAndTriggerTypeAndPlatformIdAndProductBonusType
(
  @PartyID int = null,
  @TriggerType nvarchar(100) = null,
  @PlatformID int = null,
  @ProductBonusType nvarchar(50) = null,
  @BonusPlanID NVARCHAR(10) = null output
)
as
begin
    DECLARE @SQL nvarchar(max);
    DECLARE @BRANDID INT;
    DECLARE @BpId nvarchar(10);

    SELECT @BRANDID = brandid
    FROM   external_mpt.user_conf
    WHERE  partyid = @PartyID;

    SET @SQL = 'SELECT top 1 @BpId = bp.id
                   FROM admin_all.bonus_plan_brand bpbrand
                   LEFT JOIN admin_all.bonus_plan bp
                   ON bpbrand.bonus_plan_id = bp.id
                   LEFT JOIN admin_all.platform p
                   ON bp.product_id = p.id
                   WHERE  bpbrand.brand_id =' +  STR(@BRANDID) +
                  ' AND bp.start_date <= Getdate()
                   AND bp.end_date > Getdate() - 1
                   AND bp.status = ''' + 'ACTIVE' + '''
                   AND bp.trigger_type =''' +  @TriggerType +
                  ''' AND bp.product_id =' + STR(@PlatformID) +
                 CASE
                    WHEN @ProductBonusType is NOT NULL THEN
                        'AND bp.product_bonus_type =''' + @ProductBonusType + ''' ORDER  BY priority;'
                    ELSE
                        ' ORDER  BY priority;'
                 END

    exec sp_executesql @SQL, N'@BpId nvarchar(50) OUTPUT', @BpId = @BonusPlanID OUTPUT
    select @BonusPlanID

    return
end

-- exec admin_all.usp_FindOneActiveBonusPlanByPartyIdAndTriggerTypeAndPlatformId @PartyID = 100118986, @TriggerType = 'PRODUC_BON', @PlatformID = 161
