SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetGameInfoByIdOrProductIdAndGameId
(
    @GameInfoID int,
    @ProductID int,
    @GameID varchar(100)
)
as

BEGIN
    if (@GameInfoID is not null)
        select ID, PLATFORM_ID productId, GAME_ID gameId,
            name gameName, game_category_id gameCategoryId, game_launch_id gameLaunchId,
            IS_TOUCH isTouch, IS_FREESPIN_ENABLE isFreespinEnable, SUB_PLATFORM_ID subProductId,
            SEGMENT_ID segmentId, REFERENCE reference, IMAGE_URL imageUrl, BACKGROUND_IMAGE_URL backgroundImageUrl
        from admin_all.GAME_INFO
        where id = @GameInfoID
    else
        select ID, PLATFORM_ID productId, GAME_ID gameId,
            name gameName, game_category_id gameCategoryId, game_launch_id gameLaunchId,
            IS_TOUCH isTouch, IS_FREESPIN_ENABLE isFreespinEnable, SUB_PLATFORM_ID subProductId,
            SEGMENT_ID segmentId, REFERENCE reference, IMAGE_URL imageUrl, BACKGROUND_IMAGE_URL backgroundImageUrl
        from admin_all.GAME_INFO
        where PLATFORM_ID = @ProductID and GAME_ID = @GameID
END
