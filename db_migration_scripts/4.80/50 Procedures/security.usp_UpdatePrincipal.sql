set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_UpdatePrincipal
(
	@PrincipalType varchar(30)='STAFF', --USER
	@ID varchar(100) = null,
	@SID varchar(128) = null,
	@PrincipalID int = null output,
	@Name nvarchar(128) = null,
	@Description nvarchar(512) = null,
	@RetrieveRecord bit = 0
)
as
begin
	set nocount on
	if @PrincipalID is not null
		goto ___Update___;
	if @SID is not null
	begin
		select @PrincipalID = security.fn_GetPrincipalID(@SID)
		if @PrincipalID is not null
			goto ___Update___;
		throw 50000, 'Invalid SID.', 16;
	end
	if @ID is not null and @PrincipalType is not null
	begin
		select @SID = security.fn_ComposeSID(@PrincipalType, @ID)
		select @PrincipalID = security.fn_GetPrincipalID(@SID)
		if @PrincipalID is not null
			goto ___Update___;
		else
			goto ___Insert___;
	end

	goto ___Insert___
___Update___:
	update p 
		set Name = @Name, 
			Description = @Description,
			ModifiedBy = isnull(c.PrincipalID, -1),
			ModificationDate = getdate()
	from security.Principal p
		cross apply security.fn_GetSecurityContext() c
	where p.PrincipalID = @PrincipalID
	if @@rowcount > 0
		goto ___Exit___
___Insert___:
	
	insert into security.Principal(Name, Type, Description, SID, CreatedBy, CreationDate, ModifiedBy, ModificationDate)
		select 
			isnull(@Name, @SID), @PrincipalType, @Description, @SID, isnull(sc.PrincipalID, -1), getdate(), isnull(sc.PrincipalID, -1), getdate()
		from security.fn_GetSecurityContext() sc
		select @PrincipalID = scope_identity()
___Exit___:
	if @RetrieveRecord = 1
		select * from security.v_Principal where Name = isnull(@Name, @SID)
end


--select * from sys.database_principals
--select * from admin_all.staff
