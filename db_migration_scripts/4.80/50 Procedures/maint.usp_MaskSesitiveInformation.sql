set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_MaskSesitiveInformation
as
begin
	set nocount, xact_abort  on
	begin transaction
	alter table external_mpt.USER_CONF disable trigger all
	update external_mpt.USER_CONF
		set FIRST_NAME = maint.fn_GetRandomString(FIRST_NAME),
			LAST_NAME = maint.fn_GetRandomString(LAST_NAME),
			ADDRESS = maint.fn_GetRandomString(ADDRESS),
			PHONE = maint.fn_GetRandomString(PHONE),
			PHONE2 = maint.fn_GetRandomString(PHONE2),
			BIRTHDATE = maint.fn_GetRandomDate(BIRTHDATE),
			EMAIL = maint.fn_GetRandomString(EMAIL),
			NATIONAL_REG_NUMBER = maint.fn_GetRandomString(NATIONAL_REG_NUMBER)
	alter table external_mpt.USER_CONF enable trigger all
	update admin_all.PaymentCredential
		set Info = maint.fn_GetRandomString(Info),
			Credential = maint.fn_GetRandomString(Credential)
	commit
end
go
--set xact_abort on
--begin transaction 
--select * from external_mpt.USER_CONF
--exec maint.usp_MaskSesitiveInformation
--select * from external_mpt.USER_CONF
--rollback