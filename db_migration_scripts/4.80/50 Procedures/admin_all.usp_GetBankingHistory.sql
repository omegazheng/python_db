set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.usp_GetBankingHistory') is null
    begin
        exec ('create procedure admin_all.usp_GetBankingHistory as --')
    end
go

alter procedure admin_all.usp_GetBankingHistory
    (
        @partyIds VARCHAR(1000),
        @dateFrom datetime = null,
        @dateTo datetime = null
        )
    as
    begin
        set nocount on
        declare @StartDate datetime, @EndDate datetime
        if @dateFrom != null
            SET @StartDate =  @dateFrom
        ELSE
            SET @StartDate = DATEADD(DAY, -7, getdate())

        if @dateTo != null
            SET @EndDate = DATEADD(DAY, 1, @dateTo)
        ELSE
            SET @EndDate = DATEADD(DAY, 1, getdate())

        create table #PartyId (PartyId int primary key)
        insert into #PartyId(PartyId)
            select cast(Item as int) partyId from admin_all.fc_splitDelimiterString(@partyIds,',')

        select p.ID                                                             as ID,
               (case
                    when a.TRAN_TYPE = 'WD_CANCEL' then a.DATETIME
                    else p.REQUEST_DATE end)                                    as REQUEST_DATE,
               (case
                    when a.TRAN_TYPE = 'WD_CANCEL' then a.DATETIME
                    else p.PROCESS_DATE end)                                    as PROCESS_DATE,
               (case when a.TRAN_TYPE is null then p.TYPE else a.TRAN_TYPE end) as TRAN_TYPE,
               (case when a.TRAN_TYPE is null then p.TYPE else a.TRAN_TYPE end) as TRAN_TYPE_PLAIN,
               (case
                    when p.TYPE = 'WITHDRAWAL' then a.BALANCE_REAL + p.AMOUNT
                    else a.BALANCE_REAL - p.AMOUNT end)                         as PRE_BALANCE,
               (case
                    when a.amount_real is null then p.amount
                    else a.amount_real + a.amount_released_bonus end)           as AMOUNT_REAL,
               a.BALANCE_REAL + a.BALANCE_RELEASED_BONUS                        as POST_BALANCE,
               p.METHODNAME                                                     as METHOD,
               p.STATUS                                                         as STATUS,
               p.STATUS                                                         as STATUS_PLAIN,
               p.ID                                                             as PAYMENT_ID,
               p.IS_MANUAL                                                      as IS_MANUAL,
               p.METHODCODE                                                     as METHODCODE,
               p.REQUEST_METHOD                                                 as REQUESTMETHODCODE,
               P.FEE                                                            as FEE,
               a.CURRENCY,
               a.PARTYID,
               a.ACCOUNT_ID
        from (
                 select payment.*,
                        method.IS_MANUAL_BANK as IS_MANUAL,
                        method.name           as METHODNAME,
                        method.code           as METHODCODE
                 from [admin_all].[PAYMENT] payment WITH (NOLOCK)
                     join [admin_all].PAYMENT_METHOD method WITH (NOLOCK) on payment.method = method.code
                     join admin_all.Account ac WITH (NOLOCK) on payment.ACCOUNT_ID = ac.id
                 where request_date >= @StartDate
                   and request_date < @EndDate
                   and ac.PARTYID in (SELECT x.PartyId from #PartyId x )
             ) p
                 left join
             (
                 select tr.*, uu.CURRENCY, uu.PARTYID
                 from [admin_all].[ACCOUNT_TRAN] tr WITH (NOLOCK)
                 join admin_all.Account ac WITH (NOLOCK) on tr.ACCOUNT_ID = ac.id
                 join external_mpt.USER_CONF uu WITH (NOLOCK) on ac.PARTYID = uu.PARTYID
                 where tr.datetime >= @StartDate
                   and tr.datetime < @EndDate
                   and uu.PARTYID in (SELECT x.PartyId from #PartyId x )
             ) a
             on p.id = a.payment_id
        where p.request_date >= @StartDate
          and p.request_date < @EndDate
        order by REQUEST_DATE
    end
go
