set ansi_nulls, quoted_identifier on
go

-- OmegaService contains the short live jobs that are supposed to finished execution within 1 sec or less
-- They will be running consecutively one after the other
-- currently omega service is running every 10 seconds
create or alter procedure admin_all.usp_OmegaService
as
begin 
	set nocount on
	exec admin_all.usp_AutoLogout
    exec admin_all.usp_CleanupExpiredDocuments
	exec admin_all.usp_InactivateExpiredBonusPlan
end