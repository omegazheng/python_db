set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AutoLogout
as
begin
	set nocount on
	set xact_abort on
	if cast(connectionproperty('local_net_address') as varchar(100)) <>  cast(connectionproperty('client_net_address') as varchar(100))
	begin
		return;
	end
	declare @WebSessionTimeout int, @BatchSize int = 50, @ret int
	
	select @WebSessionTimeout = isnull(cast((select value from admin_all.REGISTRY_HASH where MAP_KEY = 'psWebSessionTimeout') as int), 30)
	--select @GamePlayTimeout = isnull(cast((select value from admin_all.REGISTRY_HASH where MAP_KEY = 'psGamePlayTimeout') as int), 5)

	
	create table #1(SESSION_KEY nvarchar(50) not null primary key)
	create table #2(PartyID int primary key with(ignore_dup_key = on), SESSION_KEY varchar(50), ip varchar(50))
	while(1=1)
	begin
		truncate table #1;
		truncate table #2;

		begin transaction
		exec @ret = sp_getapplock 'admin_all.usp_AutoLogout','Exclusive','Transaction', 0
		if @WebSessionTimeout < 0
		begin
			commit;
			break;
		end
		insert into #1(SESSION_KEY)
			select top (@BatchSize) s.SESSION_KEY
			from admin_all.USER_WEB_SESSION s
			where s.LAST_ACCESS_TIME< dateadd(minute, -@WebSessionTimeout, getdate())
				and s.SessionType = 'ONLINE'
		if @@rowcount = 0
		begin
			commit;
			break;
		end

		delete t
			output deleted.PARTYID, deleted.SESSION_KEY, deleted.IP into #2
		from #1 s
			inner loop join admin_all.USER_WEB_SESSION t with(readpast) on t.SESSION_KEY = s.SESSION_KEY
	
		delete t
		from #2 s
			inner loop join admin_all.USER_PLATFORM t on t.PARTYID = s.PartyID
		where not exists(select * from admin_all.USER_WEB_SESSION w where w.PARTYID = s.PARTYID)
		
		insert into admin_all.USER_LOGIN_LOG(partyid, login_time, LOGIN_TYPE, EXECUTED_BY, SESSION_KEY, ip)
		select
			distinct PARTYID, getdate(), 'TIMEOUT' LOGIN_TYPE, 'SYSTEM' EXECUTED_BY, SESSION_KEY, IP
		from #2
		commit
	end
	
end
go
--exec admin_all.usp_AutoLogout
--update admin_all.USER_WEB_SESSION  set SessionType = 'ONLINE'
--select * from admin_all.USER_WEB_SESSION 
--select * from admin_all.USER_PLATFORM
--select * from admin_all.USER_LOGIN_LOG where LOGIN_TYPE = 'TIMEOUT'
--sp_help 'admin_all.USER_LOGIN_LOG'
--sp_helpindex 'admin_all.USER_WEB_SESSION'