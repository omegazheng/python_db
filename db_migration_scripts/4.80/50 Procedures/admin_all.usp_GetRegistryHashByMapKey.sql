SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetRegistryHashByMapKey
(
    @MapKey nvarchar(255)
)
as
set nocount on
BEGIN
    select id, MAP_KEY mapkey, value, encrypted, KEY_IDX keyIdx
    from admin_all.REGISTRY_HASH
    where MAP_KEY = @MapKey
END
