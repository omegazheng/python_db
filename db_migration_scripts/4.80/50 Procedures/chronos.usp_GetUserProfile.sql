set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_GetUserProfile') is null
	exec('create procedure chronos.usp_GetUserProfile as --')
go
alter procedure chronos.usp_GetUserProfile @ConsumerID uniqueidentifier
as
begin
	set nocount on 
	set xact_abort on
	set lock_timeout 0
	if @@trancount = 0
		throw 50000, 'chronos.usp_GetUserProfile must be run within a transaction', 16;
	declare @FromVersion binary(8) = 0, @CurrentVersion binary(8) = min_active_rowversion()
	update a
		set 
				@FromVersion = FromVersion,
				ToVersion = @CurrentVersion
	from chronos.SynchronizationStatus a (rowlock)
	where Name = 'UserProfile'
		and ConsumerID = @ConsumerID
	if @@rowcount = 0
	begin
		insert into chronos.SynchronizationStatus(ConsumerID, Name, FromVersion, ToVersion)
			select @ConsumerID, 'UserProfile', @FromVersion, @CurrentVersion
	end
	if @FromVersion = 0
	begin
		select	 @ConsumerID as ApplicationID, current_transaction_id() TransactionID, p.PartyID, p.UserID, p.Nickname, p.Email, p.Title, p.FirstName, p.LastName, p.Gender, p.BirthDate, p.Address, p.City, p.Province, p.Country, p.Mobile, p.Language, p.Currency, p.RegistrationDate, p.UserType,
				v.Code LoyaltyStatus, p.Hierarchy.ToString() Path, 
				'+00:00' Timezone,
				cast(case when exists(select * from chronos.UserTrackingCode ut where ut.PartyID = p.PartyID and ut.___IsDeleted___ = 0) then 1 else 0 end as bit) IsAffiliate, 
				cast(case when exists(select * from chronos.UserReferral ur where ur.UserReferralID = p.PartyID and ur.___IsDeleted___ = 0) then 1 else 0 end as bit) IsReferral,
				chronos.fn_IsDeleted(p.___IsDeleted___,v.___IsDeleted___) ___IsDeleted___
		from Chronos.Party p
			left join chronos.VIPStatus v on p.VIPStatus = v.VIPStatusID
			where p.PartyId in (select PartyId from external_mpt.user_conf where BRANDID = 1)
	            or abs(p.PartyId) in (select STAFFID from admin_all.STAFF_AUTH_TBL)
    end
	else
	begin
		declare @ChangedParty table(PartyID int primary key with(ignore_dup_key=on))
		
		insert into @ChangedParty(PartyID)
			select p.PartyID 
			from Chronos.Party p 
				left outer join chronos.VIPStatus v on p.VIPStatus = v.VIPStatusID
			where p.___RowVersion___ >= @FromVersion
				or (v.___RowVersion___ is not null and v.___RowVersion___ >= @FromVersion)

		insert into @ChangedParty(PartyID)
			select PartyID 
			from Chronos.UserTrackingCode  a
			where ___RowVersion___ >= @FromVersion
				and not exists(select * from @ChangedParty c where c.PartyID = a.PartyID)

		insert into @ChangedParty(PartyID)
			select PartyID 
			from Chronos.UserReferral  a
			where ___RowVersion___ >= @FromVersion
				and not exists(select * from @ChangedParty c where c.PartyID = a.PartyID)

		select  @ConsumerID as ApplicationID, current_transaction_id() TransactionID, p.PartyID, p.UserID, p.Nickname, p.Email, p.Title, p.FirstName, p.LastName, p.Gender, p.BirthDate, p.Address, p.City, p.Province, p.Country, p.Mobile, p.Language, p.Currency, p.RegistrationDate, p.UserType,
				v.Code LoyaltyStatus, p.Hierarchy.ToString() Path, 
				'+00:00' Timezone,
				v.Code LoyaltyStatus, p.Hierarchy.ToString() Path, 
				cast(case when exists(select * from chronos.UserTrackingCode ut where ut.PartyID = p.PartyID and ut.___IsDeleted___ = 0) then 1 else 0 end as bit) IsAffiliate, 
				cast(case when exists(select * from chronos.UserReferral ur where ur.PartyID = p.PartyID and ur.___IsDeleted___ = 0) then 1 else 0 end as bit) ,
				chronos.fn_IsDeleted(p.___IsDeleted___,v.___IsDeleted___) ___IsDeleted___
		from Chronos.Party p
			left join chronos.VIPStatus v on p.VIPStatus = v.VIPStatusID
		where exists(select * from @ChangedParty c where p.PartyID = c.PartyID)
	end
end
go
--begin transaction
--exec chronos.usp_GetUserProfile
--rollback

