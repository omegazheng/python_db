set ansi_nulls, quoted_identifier on
--drop table rpt.PaymentMethod
go
if object_id('rpt.PaymentMethod') is null
begin
	create table rpt.PaymentMethod
	(
		PaymentMethodID smallint not null,
		PaymentMethod varchar(50) not null,
		Description varchar(128) null
		constraint PK_rpt_PaymentMethod primary key clustered (PaymentMethodID) on [Report],
		constraint UQ_rpt_PaymentMethod_PaymentMethod unique (PaymentMethod) on [Report]
	)  on [Report]
end
