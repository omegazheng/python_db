set ansi_nulls, quoted_identifier on
--drop table rpt.LoginReference
go
if object_id('rpt.LoginReference') is null
begin
	create table rpt.LoginReference
	(
		LoginReferenceID int identity(1,1) not null ,
		LoginReference nvarchar(800) not null,
		Description varchar(128) null
		constraint PK_rpt_LoginReference primary key clustered (LoginReferenceID) on [Report],
		constraint UQ_rpt_LoginReference_LoginReference unique (LoginReference) on [Report]
	)  on [Report]
end


--set identity_insert rpt.LoginReference on
--insert into rpt.LoginReference(LoginReferenceID, LoginReference) values('-1', 'UNKNOWN')
--set identity_insert rpt.LoginReference off
