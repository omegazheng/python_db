set ansi_nulls, quoted_identifier on
--drop table rpt.LoginStatus
go
if object_id('rpt.LoginStatus') is null
begin
	create table rpt.LoginStatus
	(
		LoginStatusID smallint not null,
		LoginStatus varchar(50) not null,
		Description varchar(128) null
		constraint PK_rpt_LoginStatus primary key clustered (LoginStatusID) on [Report],
		constraint UQ_rpt_LoginStatus_LoginStatus unique (LoginStatus) on [Report]
	)  on [Report]
end
