set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetLastDayOfYear(@Date date)
returns Date
as
begin
	return datefromparts(year(@Date), 12, 31)
end
go

