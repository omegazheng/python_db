set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetFirstMondayOfYear(@Date date)
returns date
as
begin
	declare @FirstDay date = rpt.fn_GetFirstDayOfYear(@Date)
	return dateadd(day, (@@datefirst - datepart(weekday, @FirstDay) +   (8 - @@datefirst) * 2) % 7, @FirstDay)
end
go
--select rpt.fn_GetFirstMondayOfYear('2020-01-01')
