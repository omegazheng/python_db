set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetDatePeriod(@Date datetime2(0))
returns table
as
return (
			select 'H' Period, rpt.fn_GetStartTimeOfHour(@Date) as Date
			union all
			select 'D' Period, cast(@Date as date) Date
			union all
			select 'M' Period, rpt.fn_GetFirstDayOfMonth(@Date)
			union all
			select 'W' Period, rpt.fn_GetFirstDayOfWeek(@Date)
			union all
			select 'Y' Period, rpt.fn_GetFirstDayOfYear(@Date)
		)
