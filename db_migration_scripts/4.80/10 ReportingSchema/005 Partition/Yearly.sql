if not exists(select * from sys.partition_functions where name = 'PF_Report_Yearly')
begin
	create partition function PF_Report_Yearly(date) as range right for values()
end

if not exists(select * from sys.partition_schemes where name = 'PS_Report_Yearly')
begin
	create partition scheme PS_Report_Yearly as partition PF_Report_Yearly all to ([Report])
end
go

