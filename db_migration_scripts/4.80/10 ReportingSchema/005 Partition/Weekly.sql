if not exists(select * from sys.partition_functions where name = 'PF_Report_Weekly')
begin
	create partition function PF_Report_Weekly(date) as range right for values()
end

if not exists(select * from sys.partition_schemes where name = 'PS_Report_Weekly')
begin
	create partition scheme PS_Report_Weekly as partition PF_Report_Weekly all to ([Report])
end
go

