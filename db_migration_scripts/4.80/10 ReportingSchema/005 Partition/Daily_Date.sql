--drop partition scheme FS_Report_Daily_Date
-- drop partition function PF_Report_Daily_Date
if not exists(select * from sys.partition_functions where name = 'PF_Report_Daily_Date')
begin
	create partition function PF_Report_Daily_Date(date) as range right for values()
end

if not exists(select * from sys.partition_schemes where name = 'PS_Report_Daily_Date')
begin
	create partition scheme PS_Report_Daily_Date as partition PF_Report_Daily_Date all to ([Report])
end
go

