if not exists(select * from sys.partition_functions where name = 'PF_Report_Monthly')
begin
	create partition function PF_Report_Monthly(date) as range right for values()
end

if not exists(select * from sys.partition_schemes where name = 'PS_Report_Monthly')
begin
	create partition scheme PS_Report_Monthly as partition PF_Report_Monthly all to ([Report])
end
go

