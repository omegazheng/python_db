--drop partition scheme FS_Report_Daily
-- drop partition function PF_Report_Daily
if not exists(select * from sys.partition_functions where name = 'PF_Report_Daily')
begin
	create partition function PF_Report_Daily(datetime2(0)) as range right for values()
end

if not exists(select * from sys.partition_schemes where name = 'PS_Report_Daily')
begin
	create partition scheme PS_Report_Daily as partition PF_Report_Daily all to ([Report])
end
go

