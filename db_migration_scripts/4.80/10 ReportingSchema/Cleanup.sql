if object_id('rpt.AccountTranAggregateDaily') is not null
	drop table rpt.AccountTranAggregateDaily;
if object_id('rpt.AccountTranAggregateHourly') is not null
	drop table rpt.AccountTranAggregateHourly;
if object_id('rpt.AccountTranAggregateLTD') is not null
	drop table rpt.AccountTranAggregateLTD;
if object_id('rpt.AccountTranAggregateMonthly') is not null
	drop table rpt.AccountTranAggregateMonthly;
if object_id('rpt.AccountTranAggregatePending') is not null
	drop table rpt.AccountTranAggregatePending;
if object_id('rpt.AccountTranAggregatePending_Bak') is not null
	drop table rpt.AccountTranAggregatePending_Bak;
if object_id('rpt.AccountTranAggregateWeekly') is not null
	drop table rpt.AccountTranAggregateWeekly;
if object_id('rpt.AccountTranAggregateYearly') is not null
	drop table rpt.AccountTranAggregateYearly;
if object_id('rpt.AccountTranStaging') is not null
	drop table rpt.AccountTranStaging;
if object_id('rpt.PaymentAggregateDaily') is not null
	drop table rpt.PaymentAggregateDaily;
if object_id('rpt.PaymentAggregateGlobalDaily') is not null
	drop table rpt.PaymentAggregateGlobalDaily;
if object_id('rpt.PaymentAggregateGlobalHourly') is not null
	drop table rpt.PaymentAggregateGlobalHourly;
if object_id('rpt.PaymentAggregateGlobalLTD') is not null
	drop table rpt.PaymentAggregateGlobalLTD;
if object_id('rpt.PaymentAggregateGlobalMonthly') is not null
	drop table rpt.PaymentAggregateGlobalMonthly;
if object_id('rpt.PaymentAggregateGlobalWeekly') is not null
	drop table rpt.PaymentAggregateGlobalWeekly;
if object_id('rpt.PaymentAggregateGlobalYearly') is not null
	drop table rpt.PaymentAggregateGlobalYearly;
if object_id('rpt.PaymentAggregateHourly') is not null
	drop table rpt.PaymentAggregateHourly;
if object_id('rpt.PaymentAggregateLTD') is not null
	drop table rpt.PaymentAggregateLTD;
if object_id('rpt.PaymentAggregateMonthly') is not null
	drop table rpt.PaymentAggregateMonthly;
if object_id('rpt.PaymentAggregatePending') is not null
	drop table rpt.PaymentAggregatePending;
if object_id('rpt.PaymentAggregateUserPrimaryDaily') is not null
	drop table rpt.PaymentAggregateUserPrimaryDaily;
if object_id('rpt.PaymentAggregateUserPrimaryHourly') is not null
	drop table rpt.PaymentAggregateUserPrimaryHourly;
if object_id('rpt.PaymentAggregateUserPrimaryLTD') is not null
	drop table rpt.PaymentAggregateUserPrimaryLTD;
if object_id('rpt.PaymentAggregateUserPrimaryMonthly') is not null
	drop table rpt.PaymentAggregateUserPrimaryMonthly;
if object_id('rpt.PaymentAggregateUserPrimaryWeekly') is not null
	drop table rpt.PaymentAggregateUserPrimaryWeekly;
if object_id('rpt.PaymentAggregateUserPrimaryYearly') is not null
	drop table rpt.PaymentAggregateUserPrimaryYearly;
if object_id('rpt.PaymentAggregateWeekly') is not null
	drop table rpt.PaymentAggregateWeekly;
if object_id('rpt.PaymentAggregateYearly') is not null
	drop table rpt.PaymentAggregateYearly;

if object_id('rpt.UserLoginAggregateDaily') is not null
	drop table rpt.UserLoginAggregateDaily;
if object_id('rpt.UserLoginAggregateHourly') is not null
	drop table rpt.UserLoginAggregateHourly;
if object_id('rpt.UserLoginAggregateLTD') is not null
	drop table rpt.UserLoginAggregateLTD;
if object_id('rpt.UserLoginAggregateMonthly') is not null
	drop table rpt.UserLoginAggregateMonthly;
if object_id('rpt.UserLoginAggregatePending') is not null
	drop table rpt.UserLoginAggregatePending;
if object_id('rpt.UserLoginAggregateWeekly') is not null
	drop table rpt.UserLoginAggregateWeekly;
if object_id('rpt.UserLoginAggregateYearly') is not null
	drop table rpt.UserLoginAggregateYearly;
if object_id('rpt.UserLoginStaging') is not null
	drop table rpt.UserLoginStaging;


if object_id('rpt.AccountTranAggregateHourly_Interface') is not null
	drop view rpt.AccountTranAggregateHourly_Interface;
if object_id('rpt.AccountTranStaging_Interface') is not null
	drop view rpt.AccountTranStaging_Interface;
if object_id('rpt.PaymentStaging_Interface') is not null
	drop view rpt.PaymentStaging_Interface;
if object_id('rpt.UserLoginStaging_Interface') is not null
	drop view rpt.UserLoginStaging_Interface;
