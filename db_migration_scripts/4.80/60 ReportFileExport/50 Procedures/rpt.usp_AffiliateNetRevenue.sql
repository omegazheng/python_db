set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_AffiliateNetRevenue
(
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@Offset char(6) = null, -- '+00:00'  for UTC
	@BrandIDs varchar(max) = null,
	@CodeKey varchar(20) = 'btag',
	@OutputTarget varchar(20) = 'Table' -- File
)
as
begin

	set nocount on
    select @DateFrom = admin_all.fn_ConvertDatetimeZone(isnull(@DateFrom, cast(getdate() as date)), @Offset, null)
	select @DateTo = admin_all.fn_ConvertDatetimeZone(isnull(@DateTo, dateadd(d, 1, @DateFrom)), @Offset, null)

	declare @AffiliateEgassJackpotContributionIsEnabled bit = isnull(cast(admin_all.fn_GetRegistry('affiliate.egass.jackpotContribution.isEnabled') as bit), 0), --If true, allow jackpot contribution to be included as fee adjustment. If false, fee adjustment will be zero.
			@AffiliateEgassManualBonusAdjustmentPercentage numeric(38, 18) = isnull(cast(admin_all.fn_GetRegistry('affiliate.egass.manualBonusAdjustmentPercentage') as numeric(38, 18)), 0), --the contribution percentage to calculate the manual bonus adjustment
			@AffiliateEgassBonusCalculationIsEnabled bit = isnull(cast(admin_all.fn_GetRegistry('affiliate.egass.bonusCalculation.isEnabled') as bit), 0) --if true, allow released bonus to be included in net revenue calculation
	

	create table #BrandID(BrandID int, BrandName nvarchar(1000), primary key (BrandID))
	insert into #BrandID(BrandID, BrandName)
		select BRANDID, BRANDNAME
		from admin.CASINO_BRAND_DEF
		where @BrandIDs is null
			or BRANDID in (
							select cast(value as int) 
							from (select value from string_split(@BrandIDs, ',') s)  a
						)

	--select * from admin_all.GAME_INFO
	--select * from admin_all.PLATFORM
	--select * from omegasys_dev.rpt.TranType order by TranType
	create table #Ratio(BrandID int, ProductID int, GameID varchar(100), Ratio numeric(38, 18), primary key (BrandID, ProductID, GameID))
	insert into #Ratio(BrandID, ProductID, GameID, Ratio)
		select b.BrandID,gi.PLATFORM_ID ProductID, gi.GAME_ID GameID, Ratio/100.0
		from admin_all.GAME_INFO gi
			cross join #BrandID b  
			cross apply (
							select top 1 RATIO
							from admin_all.BRAND_GAME_JP_RATIO jp
							where jp.BRANDID = b.BrandID
								and gi.ID = jp.GAME_INFO_ID
							order by last_update desc
						) r 
		
	;with x0 as
	(
		select 
				h.BrandID, h.PartyID, cast(admin_all.fn_ConvertDatetimeZone(h.Datetime, null, @Offset) as date) Date, h.ProductID, h.TranType, h.GameID,
				h.AmountReal, h.AmountReleasedBonus, h.AmountPlayableBonus
		from admin_all.USER_TRACKING_CODE utc
			inner join admin_all.AccountTranHourlyAggregate h on h.AggregateType = 0 and  utc.PARTYID = h.PartyID
		where CODE_KEY = @CodeKey 
			and h.Datetime >= @DateFrom and h.Datetime < @DateTo
			and h.BrandID in (select a.BrandID from #BrandID a)
			and h.TranType in ('GAME_BET', 'GAME_WIN', 'STAKE_DEC', 'REFUND', 'CASH_OUT', 'BONUS_REL', 'CHARGE_BCK', 'MAN_BONUS')
	),
	x1 as
	(
		select x0.BrandID, x0.PartyID, x0.Date, x0.ProductID, x0.GameID,x0.TranType, sum(x0.AmountReal) AmountReal, sum(x0.AmountReleasedBonus) AmountReleasedBonus, sum(x0.AmountPlayableBonus) AmountPlayableBonus
		from x0
		group by x0.BrandID, x0.PartyID, x0.Date, x0.ProductID, x0.GameID,x0.TranType
	),
	x2 as
	(
		select	pc.PartyID, x1.PartyID as AssociatePartyID, x1.Date, pc.Currency, pc.AssociatedCurrency,
 
				sum(
					case 
						when isnull(p.PLATFORM_TYPE, '') = 'CASINO' and x1.TranType in ('GAME_BET', 'GAME_WIN') then -(x1.AmountReal + x1.AmountReleasedBonus)
						when isnull(p.PLATFORM_TYPE, '') = 'SPORTSBOOK' and x1.TranType in ('GAME_BET', 'GAME_WIN',  'STAKE_DEC', 'REFUND', 'CASH_OUT') then -(x1.AmountReal + x1.AmountReleasedBonus)
						else 0
					end
				) GrossRevenue,
						--
				sum(
					case 
						when @AffiliateEgassBonusCalculationIsEnabled = 1 and x1.TranType in ('BONUS_REL') then x1.AmountReleasedBonus
						else 0
					end
				) Bonus,
				sum(
					case
						when @AffiliateEgassJackpotContributionIsEnabled = 1 and x1.TranType in ('GAME_BET') and r.Ratio is not null then 
								(x1.AmountReal + AmountReleasedBonus + AmountPlayableBonus) * r.Ratio
						else 0
					end
					+ case
						when x1.TranType in ('MAN_BONUS') then @AffiliateEgassManualBonusAdjustmentPercentage * (x1.AmountReal + AmountReleasedBonus + AmountPlayableBonus) / 100
						else 0
					end
					- case
						when x1.TranType in ('CHARGE_BCK') then - (x1.AmountReal + AmountReleasedBonus + AmountPlayableBonus)
						else 0
					end
				) Adjustment

		from x1
			inner join external_mpt.v_UserPrimaryCurrency pc with(noexpand) on pc.AssociatedPartyID = x1.PartyID
			left join admin_all.PLATFORM p on p.ID = x1.ProductID
			left join #Ratio r on r.ProductID = x1.ProductID and r.GameID = x1.GameID and r.BrandID = x1.BrandID
		where (
					p.ID is not null and  p.PLATFORM_TYPE in ('CASINO', 'SPORTSBOOK') 
					or p.ID is null
				)
		group by pc.PartyID, x1.PartyID, x1.Date, pc.Currency, pc.AssociatedCurrency
	)
	select *, x2.GrossRevenue - x2.Bonus - x2.Adjustment Amount
		into #temp
	from x2
	
	--select @OutputColumns = stuff((select ','+name as column_name from tempdb.sys.columns where object_id = object_id('tempdb..#temp') order by column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '');
	
	if @OutputTarget = 'File'
	begin
		declare @Query nvarchar(max) = 'select PartyID, convert(varchar(100), Date, 120) AmountDate, convert(numeric(38,2), Amount) Amount, Currency AmountCurrency, null AmountCategory  from #temp where GrossRevenue <> 0 or Bonus <> 0 or Adjustment <> 0'
		exec rpt.usp_GenerateReportFileExportContent @Query = @Query, 
				@Mapping = '<Mappings><Mapping SourceColumn="PartyID" ReportColumn="PIN"/><Mapping SourceColumn="AmountDate"/><Mapping SourceColumn="Amount"/><Mapping SourceColumn="AmountCurrency"/><Mapping SourceColumn="AmountCategory"/></Mappings>',
				@Test = 0
		return
	end
    select * 
	from #temp
	where GrossRevenue <> 0
		or Bonus <> 0
		or Adjustment <> 0
	--select @OutputColumns
end
go
--exec rpt.usp_AffiliateNetRevenue @DateFrom = '2019-01-30', @OutputTarget = 'File'