set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_SetReportFileExportID @ReportFileExportID uniqueidentifier = null
as
begin
	set nocount on
	exec sp_set_session_context N'@ReportFileExportID', @ReportFileExportID
end
go

