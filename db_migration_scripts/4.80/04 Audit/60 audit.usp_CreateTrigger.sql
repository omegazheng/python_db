set ansi_nulls, quoted_identifier on
--drop procedure audit.usp_CreateTrigger
go
create or alter procedure audit.usp_CreateTrigger
(
	@SchemaName sysname,
	@TableName sysname
)
as
begin
	set nocount on
	select @SchemaName = object_schema_name(object_id(quotename(@SchemaName) + '.' + quotename(@TableName))),
			@TableName = object_name(object_id(quotename(@SchemaName) + '.' + quotename(@TableName)))
	declare @InserTriggerName sysname, @DeleteTriggerName sysname, @UpdateTriggerName sysname, @SQL nvarchar(max), @FullObjectName nvarchar(256)

	select	@InserTriggerName = quotename(@SchemaName)+'.' + quotename('TRI_Audit_Insert_' + @SchemaName + '_' + @TableName),
			@UpdateTriggerName = quotename(@SchemaName)+'.' + quotename('TRI_Audit_Update_' + @SchemaName + '_' + @TableName),
			@DeleteTriggerName = quotename(@SchemaName)+'.' + quotename('TRI_Audit_Delete_' + @SchemaName + '_' + @TableName),
			@FullObjectName = quotename(@SchemaName) + '.' + quotename(@TableName)
	begin try
	-- insertion
	if exists(select * from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditInsert = 1)
	begin
		select @SQL = 'create or alter trigger ' + @InserTriggerName + ' on ' + @FullObjectName + '
for insert
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @ClientInfoID uniqueidentifier
	exec audit.usp_CreateClientInfo @ClientInfoID output
	insert into audit.Trail(TableName, Operation, PrimaryKey, OldValue, NewValue, ClientInfoID)
		select	'+quotename(@FullObjectName, '''')+',''I'', 
				(select '+ stuff((select ',' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '') + ' for xml path(''''), type, binary base64) as PrimaryKey,
				null,
				(select '+ stuff((select ',' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditInsert = 1 order by ColumnID for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '') + ' for xml path(''''), type, binary base64) as NewValue,
				@ClientInfoID
		from inserted
end'
		exec(@SQL)
	end
	else if not exists(select * from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditInsert = 1)
	begin
		select @SQL = 'drop trigger if exists ' + @InserTriggerName
		exec (@SQL)
	end

	-- deletion
	if exists(select * from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditDelete = 1)
	begin
		select @SQL = 'create or alter trigger ' + @DeleteTriggerName + ' on ' + @FullObjectName + '
for delete
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @ClientInfoID uniqueidentifier
	exec audit.usp_CreateClientInfo @ClientInfoID output
	insert into audit.Trail(TableName, Operation, PrimaryKey, OldValue, NewValue, ClientInfoID)
		select	'+quotename(@FullObjectName, '''')+',''D'', 
				(select '+ stuff((select ',' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '') + ' for xml path(''''), type) as PrimaryKey,
				(select '+ stuff((select ',' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditDelete = 1 order by ColumnID for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '') + ' for xml path(''''), type) as OldValue,
				null,
				@ClientInfoID
		from deleted
end'
		exec(@SQL)
	end
	else if not exists(select * from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditDelete = 1)
	begin
		select @SQL = 'drop trigger if exists ' + @DeleteTriggerName
		exec (@SQL)
	end

	-- update
	if exists(select * from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditUpdate = 1)
	begin
		select @SQL = 'create or alter trigger ' + @UpdateTriggerName + ' on ' + @FullObjectName + '
for update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @ClientInfoID uniqueidentifier
	exec audit.usp_CreateClientInfo @ClientInfoID output
	insert into audit.Trail(TableName, Operation, PrimaryKey, OldValue, NewValue, ClientInfoID)
		select	'+quotename(@FullObjectName, '''')+',''U'', 
				(select '+ stuff((select ', isnull(i.' + quotename(ColumnName) + ', d.' + quotename(ColumnName) + ') ' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '') + ' for xml path(''''), type) as PrimaryKey,
				(select '+ stuff((select ',d.' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditUpdate = 1 order by ColumnID for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '') + ' for xml path(''''), type) as OldValue,
				(select '+ stuff((select ',i.' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditUpdate = 1 order by ColumnID for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '') + ' for xml path(''''), type) as NewValue,
				@ClientInfoID
		from inserted i
			full outer join deleted d on '+ stuff((select ' and i.' + quotename(ColumnName) + '= d.' + quotename(ColumnName) from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'nvarchar(max)'), 1, 5, '') + '
		where ' + stuff((
						select ' or (i.' + quotename(ColumnName) + ' is null and d.' + quotename(ColumnName) + ' is not null or i.' + quotename(ColumnName) + ' is not null and d.' + quotename(ColumnName) + ' is null or ' + case when DataType in ('xml', 'text', 'ntext', 'image') then 'cast(i.' + quotename(ColumnName) + ' as varbinary(max)) <> cast(d.' + quotename(ColumnName) + ' as varbinary(max))' else 'i.' + quotename(ColumnName) + ' <> d.' + quotename(ColumnName) end + ')'
						from audit.Configuration 
						where SchemaName = @SchemaName 
							and TableName = @TableName 
							and AuditUpdate = 1 
						order by ColumnID for xml path(''), type).value('.', 'nvarchar(max)'), 1, 4, '') + '
				
end'
	--print @SQL
		exec(@SQL)
	end
	else if not exists(select * from audit.Configuration where SchemaName = @SchemaName and TableName = @TableName and AuditUpdate = 1)
	begin
		select @SQL = 'drop trigger if exists ' + @UpdateTriggerName
		exec (@SQL)
	end

	end try
	begin catch
		throw;
	end catch
end
go
/*
-- demo
begin transaction
--- no triggers
select * from sys.triggers where parent_id = object_id('admin_all.time_zone')
-- no configurations
select * from audit.Configuration

-- create audit for  insert, delete, and update
update audit.v_Configuration set AuditInsert = 1, AuditDelete = 1, AuditUpdate = 1  where schemaname = 'admin_all' and TableName = 'TIME_ZONE' and ColumnName = 'Location'
-- configuration is populated
select * from audit.Configuration
--- 3 triggers
select * from sys.triggers where parent_id = object_id('admin_all.time_zone')

-- trigger insert
insert into admin_all.TIME_ZONE(NAME, LOCATION, OFFSET) values('test', 'test', '0')
select* from audit.Trail

--trigger update
update admin_all.TIME_ZONE set LOCATION = 'a' where name = 'test'
select* from audit.Trail

-- trigger delete
delete admin_all.TIME_ZONE where name = 'test'
select* from audit.Trail

--remove audit
update audit.v_Configuration set AuditInsert = 0 where schemaname = 'admin_all' and TableName = 'TIME_ZONE'  and ColumnName = 'Location'
--2 triggers left
select * from sys.triggers where parent_id = object_id('admin_all.time_zone')
select * from audit.Configuration

-- remove all audit
delete audit.v_Configuration where schemaname = 'admin_all' and TableName = 'TIME_ZONE' 
-- all triggers are removed
select * from sys.triggers where parent_id = object_id('admin_all.time_zone')
select * from audit.Configuration

select * from audit.ClientInfo
rollback

*/