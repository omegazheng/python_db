set ansi_nulls, quoted_identifier on
go
create or alter procedure audit.usp_CreateClientInfo @ClientInfoID uniqueidentifier = null output
as
begin
	set nocount on
	declare @ApplicationName nvarchar(128) = isnull(app_name(), ''),
			@WorkStationName nvarchar(128) = isnull(host_name(), ''),
			@ClientAddress nvarchar(128) = isnull(cast(connectionproperty('client_net_address') as nvarchar(128)), ''),
			@Transport nvarchar(128) = isnull(cast(connectionproperty('net_transport') as nvarchar(128)), ''),
			@Protocol nvarchar(128) = isnull(cast(connectionproperty('protocol_type') as nvarchar(128)), ''),
			@LoginName nvarchar(128) = isnull(system_user, '')
	select @ClientInfoID = cast(hashbytes('md5', cast(cast( @ApplicationName + '-' + @WorkStationName + '-' + @ClientAddress + '-' + @Transport + '-' + @Protocol + '-' + @LoginName as nvarchar(max)) as varbinary(max)) ) as uniqueidentifier)
	if not exists(select * from audit.ClientInfo where ClientInfoID = @ClientInfoID)
	begin
		insert into audit.ClientInfo(ClientInfoID, ApplicationName, WorkStationName, ClientAddress, Transport, Protocol, LoginName)
			values(@ClientInfoID, @ApplicationName, @WorkStationName, @ClientAddress, @Transport, @Protocol, @LoginName)
	end
end
go
--declare @c uniqueidentifier
--exec audit.usp_CreateClientInfo @c output
--select @c
--select * from audit.ClientInfo

--select connectionproperty('local_net_address')