set ansi_nulls, quoted_identifier on
--drop table audit.Configuration
go
if object_id('audit.Configuration') is null
begin
	create table audit.Configuration
	(
		SchemaName nvarchar(128) not null,
		TableName nvarchar(128) not null,
		ColumnName nvarchar(128) not null,
		ColumnID int not null,
		DataType nvarchar(128) not null,
		PrimaryKeyOrder int not null,
		AuditInsert bit not null,
		AuditDelete bit not null,
		AuditUpdate bit not null,
		CreationDate datetime2(7) not null,
		LastModificationDate datetime2(7) not null,
		RowVersion RowVersion not null
		constraint PK_audit_Configuration primary key(SchemaName, TableName, ColumnName),
		index IDX_audit_Configuration_RowVersion unique (RowVersion)
	)
end
go
