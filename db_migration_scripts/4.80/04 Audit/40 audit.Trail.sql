set ansi_nulls, quoted_identifier on
--drop table audit.Trail
go
if object_id('audit.Trail') is null
begin
	create table audit.Trail
	(
		TrailID bigint not null identity(1,1),
		Date datetime2(7) not null constraint DF_audit_Trail_Date default(sysdatetime()),
		TableName nvarchar(256) not null,
		Operation nvarchar(128) not null,
		PrimaryKey xml not null,
		OldValue xml null,
		NewValue xml null,
		ClientInfoID uniqueidentifier not null,
		constraint PK_audit_Trail primary key(TrailID) with(data_compression=page),
		index IDX_audit_Trail_ClientInfoID(ClientInfoID) with(data_compression=page)
	)
end
