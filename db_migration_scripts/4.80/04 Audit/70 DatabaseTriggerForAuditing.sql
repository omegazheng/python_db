set ansi_nulls, quoted_identifier on
go
create or alter trigger DatabaseTriggerForAuditing
on database for alter_table, drop_table, rename
as
begin
	set nocount on 
	declare @Event xml = eventdata(), @ClientInfoID uniqueidentifier
	exec audit.usp_CreateClientInfo @ClientInfoID output
	;with x0 as
	(
		select	@Event Event, 
				@Event.value('(/EVENT_INSTANCE/EventType)[1]', 'nvarchar(128)') EventType, 
				@Event.value('(/EVENT_INSTANCE/SchemaName)[1]', 'nvarchar(128)') SchemaName, 
				@Event.value('(/EVENT_INSTANCE/ObjectName)[1]', 'nvarchar(128)') TableName
	)
	insert into audit.Trail(TableName, Operation, PrimaryKey, ClientInfoID)
	select 
			quotename(SchemaName) + '.' + quotename(TableName),
			EventType Operation,
			Event as PrimaryKey,
			@ClientInfoID
	from x0;
	declare @AffectedTable table(SchemaName sysname, TableName sysname)
	merge audit.Configuration t
	using audit.v_configuration s on t.TableName = s.TableName and t.SchemaName = s.SchemaName and t.ColumnName = s.ColumnName
	when matched and (t.PrimaryKeyOrder <> s.PrimaryKeyOrder or t.DataType <> s.DataType)then 
		update set t.PrimaryKeyOrder = s.PrimaryKeyOrder, t.DataType = s.DataType
	when not matched by source then
		delete
	output deleted.SchemaName, deleted.TableName into @AffectedTable
	;
	if not exists(select * from @AffectedTable)
		return
	declare @SchemaName sysname, @TableName sysname
	declare cDatabaseTriggerForAuditing cursor for
		select SchemaName, TableName from @AffectedTable
	open cDatabaseTriggerForAuditing
	fetch next from cDatabaseTriggerForAuditing into @SchemaName, @TableName
	while @@fetch_status = 0
	begin
		exec audit.usp_CreateTrigger @SchemaName, @TableName
		fetch next from cDatabaseTriggerForAuditing into @SchemaName, @TableName
	end
	close cDatabaseTriggerForAuditing
	deallocate cDatabaseTriggerForAuditing
end
go
/*
begin transaction
exec('create table a(id int not null constraint aa primary key, name1 varchar(100) not null, name2 varchar(100) )')
select * from audit.v_Configuration where SchemaName = 'dbo' and TableName = 'a'
update audit.v_Configuration set AuditUpdate = 1 where SchemaName = 'dbo' and TableName = 'a'
exec sp_helptext 'TRI_Audit_Update_dbo_a'
exec('alter table a alter column name2 xml')
exec sp_helptext 'TRI_Audit_Update_dbo_a'
--exec('alter table a drop constraint aa')
select * from sys.triggers where parent_id = object_id('a')
select * from audit.configuration
select * from  audit.Trail
rollback
*/
/*
begin transaction
exec('create table a(id int not null)')
exec('create table c(id int not null)')
exec('alter table a add a int')
exec('alter table a alter column a bigint')
exec('exec sp_rename ''dbo.a'', ''b'', ''object''')
exec('alter table b add constraint xyz primary key(id)')
exec('alter table b drop constraint xyz ')
exec('drop table b, c')
select * from  audit.Trail
rollback
*/
go


--select * from sys.all_objects where name like '%trigger%'
/*
;with x0 as
(
	select t.type, t.type_name, t.parent_type, 1 as level
	from sys.trigger_event_types t
	where t.type = 10016
	union all
	select t.type, t.type_name, t.parent_type, x0.level + 1 as level
	from sys.trigger_event_types t
		inner join x0 on x0.type = t.parent_type
)
select *
from x0
--where type_name like '%pri%'
*/

