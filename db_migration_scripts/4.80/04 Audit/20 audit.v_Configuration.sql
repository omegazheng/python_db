set ansi_nulls, quoted_identifier on
go
create or alter view audit.v_Configuration
as
with x0 as
(
	select schema_name(t.schema_id) SchemaName, t.name as TableName, c.name as ColumnName, type_name(c.user_type_id) DataType,isnull(ic.key_ordinal, 0) PrimaryKeyOrder, c.object_id as ObjectID, c.column_id ColumnID
	from sys.columns c
		inner join sys.tables t on t.object_id = c.object_id
		inner join sys.types st on st.user_type_id = c.user_type_id
		inner join sys.indexes i on i.object_id = t.object_id and i.is_primary_key = 1 and i.is_disabled = 0
		left join sys.index_columns ic on ic.object_id = i.object_id and ic.index_id = i.index_id and ic.column_id = c.column_id
	where t.schema_id not in (isnull(schema_id('maint'), -1), schema_id('sys'), isnull(schema_id('log'), -1))
		and c.is_computed = 0
		and st.is_assembly_type = 0
		and st.is_table_type = 0
		and st.name not in ('geometry','geography', 'timestamp')
		--and not (
		--			t.schema_id = schema_id('admin_all') and t.name in ('ACCOUNT_TRAN_REALTIME', 'ACCOUNT_TRAN_ALL')
		--		)
)
select x0.* , isnull(c.AuditInsert, 0) AuditInsert, isnull(c.AuditUpdate, 0) AuditUpdate, isnull(c.AuditDelete, 0) AuditDelete, c.CreationDate, c.LastModificationDate
from x0
	left join  audit.Configuration c on c.TableName = x0.TableName and c.SchemaName = x0.SchemaName and c.ColumnName = x0.ColumnName
go
--select * from audit.v_Configuration where DataType not in ('nvarchar', 'bit','int','bit', 'varchar','datetime', 'char','numeric', 'bigint', 'tinyint','date', 'smallint','uniqueidentifier', 'varbinary','float','nchar','binary','money','sysname','datetime2')
--order by 1,2

	