set ansi_nulls, quoted_identifier on
go
create or alter trigger audit.TRI_v_Configuration on audit.v_Configuration
instead of update, delete
as
begin
	if @@rowcount = 0
		return
	set nocount on
	
	;with s0 as
	(
		select isnull(i.SchemaName, d.SchemaName) SchemaName, isnull(i.TableName, d.TableName) TableName, isnull(i.ColumnName, d.ColumnName) ColumnName, isnull(i.ObjectID, d.ObjectID) ObjectID,
				isnull(i.AuditInsert, 0) AuditInsert, isnull(i.AuditDelete, 0) AuditDelete, isnull(i.AuditUpdate, 0) AuditUpdate
		from inserted i
			full outer join deleted d on i.ObjectID = d.ObjectID and i.ColumnName = d.ColumnName
		where isnull(i.AuditDelete, 0) <> isnull(d.AuditDelete, 0)
			or isnull(i.AuditInsert, 0) <> isnull(d.AuditInsert, 0)
			or isnull(i.AuditUpdate, 0) <> isnull(d.AuditUpdate, 0)
	),
	s as
	(
		select c.SchemaName, c.TableName, c.ColumnName, c.ColumnID ,c.DataType, c.PrimaryKeyOrder, isnull(s0.AuditDelete, 0) AuditDelete, isnull(s0.AuditInsert, 0) AuditInsert, isnull(s0.AuditUpdate, 0) AuditUpdate
		from audit.v_Configuration c
			left outer join s0 on c.ObjectID = s0.ObjectID and c.ColumnName = s0.ColumnName
		where exists(select * from s0 where c.ObjectID = s0.ObjectID)
			
	),
	t as
	(
		select * 
		from audit.Configuration c
		where exists(select * from s0 where c.SchemaName = s0.SchemaName and c.TableName = s0.TableName)
	)
	--select * from s
	merge  t
	using s on t.SchemaName = s.SchemaName and t.TableName = s.TableName and t.ColumnName  =s.ColumnName
	when not matched then
		insert (SchemaName, TableName, ColumnName, ColumnID, DataType, PrimaryKeyOrder, AuditInsert, AuditDelete, AuditUpdate, CreationDate, LastModificationDate)
			values(s.SchemaName, s.TableName, s.ColumnName, s.ColumnID, s.DataType, s.PrimaryKeyOrder, s.AuditInsert, s.AuditDelete, s.AuditUpdate, sysdatetime(), sysdatetime())
	when matched and (t.DataType <> s.DataType or t.PrimaryKeyOrder <> s.PrimaryKeyOrder or t.AuditInsert <> s.AuditInsert or t.AuditDelete <> s.AuditDelete or t.AuditUpdate <> s.AuditUpdate or s.ColumnID <> t.ColumnID) then
		update set t.DataType = s.DataType, t.PrimaryKeyOrder = s.PrimaryKeyOrder, t.AuditInsert = s.AuditInsert, t.AuditDelete = s.AuditDelete, t.AuditUpdate = s.AuditUpdate, t.ColumnID = s.ColumnID
	when not matched by source then
		delete
	;
	declare @SchemaName sysname, @TableName sysname
	declare c cursor local fast_forward for
		select SchemaName, TableName
		from inserted
		union
		select SchemaName, TableName
		from deleted
	open c
	fetch next from c into @SchemaName, @TableName
	while @@fetch_status = 0
	begin
		exec audit.usp_CreateTrigger @SchemaName, @TableName
		fetch next from c into @SchemaName, @TableName
	end
	close c
	deallocate c
end
go


--begin transaction
--exec('create table a(id int not null primary key, name1 varchar(100), name2 varchar(100) )')
--select * from audit.v_Configuration where SchemaName = 'dbo' and TableName = 'a'
--update audit.v_Configuration set AuditInsert = 1 where SchemaName = 'dbo' and TableName = 'a'
--select * from sys.triggers where parent_id = object_id('a')
----select * from audit.configuration
--select * from  audit.Trail
--rollback
--begin transaction
----select * from audit.v_Configuration where schemaname = 'admin_all' and TableName = 'TIME_ZONE'
--select * from audit.Configuration
--update audit.v_Configuration set AuditInsert = 1  where schemaname = 'admin_all' and TableName = 'TIME_ZONE' and ColumnName = 'Location'
--select * from audit.Configuration
--delete audit.v_Configuration where schemaname = 'admin_all' and TableName = 'TIME_ZONE' and ColumnName = 'Location'
----select * from audit.v_Configuration where schemaname = 'admin_all' and TableName = 'TIME_ZONE'
--select * from audit.Configuration
--rollback



--select * from admin_all.TIME_ZONE