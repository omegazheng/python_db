set ansi_nulls, quoted_identifier on
go
if object_id('maint.usp_ForceNamingConvention') is null
	exec('create procedure maint.usp_ForceNamingConvention as --')
go
alter procedure maint.usp_ForceNamingConvention 
(
	@TableName sysname = null, 
	@ExecuteDirectly bit = 1,
	@IgnoreError bit = 0
)
as
begin
	set nocount on 
	set xact_abort off
	declare @SQL nvarchar(max)
	create table #Query(ID int identity(1,1), SQL nvarchar(max))
	--Indexes
	insert into #Query(SQL)
		select 'exec sp_rename ' + quotename(quotename(SchemaName) + '.' + quotename(ObjectName) + '.' + quotename(IndexName), '''')+','+quotename(ExpectedIndexName, '''') + ',' + quotename('Index', '''') +';'
			--, *
		from maint.fn_GetIndexStructure(@TableName)
		where SchemaName not in ('sys')
			and ExpectedIndexName <> IndexName
			and IsPrimaryKey = 0
			and IsUniqueConstraint = 0
	-- PK and UQ
	insert into #Query(SQL)
		select 'exec sp_rename ' + quotename(quotename(SchemaName) + '.' + quotename(IndexName), '''')+','+quotename(ExpectedIndexName, '''') + ',' + quotename('Object', '''') +';'
			--, *
		from maint.fn_GetIndexStructure(@TableName)
		where SchemaName not in ('sys')
			and ExpectedIndexName <> IndexName
			and (IsPrimaryKey = 1 or  IsUniqueConstraint = 1)
	--Default
	insert into #Query(SQL)
		select 'exec sp_rename ' + quotename(quotename(SchemaName) + '.' + quotename(DefaultObjectName), '''')+','+quotename(ExpectedDefaultObjectName, '''') + ',' + quotename('Object', '''') +';'
		from maint.fn_GetDefaultStructure(@TableName)
		where SchemaName not in ('sys')
			and ExpectedDefaultObjectName <> DefaultObjectName
	--FK
	insert into #Query(SQL)
		select 'exec sp_rename ' + quotename(quotename(SchemaName) + '.' + quotename(ObjectName), '''')+','+quotename(ExpectedObjectName, '''') + ',' + quotename('Object', '''') +';'
		from maint.fn_GetForeignKeyStructure(@TableName, null)
		where SchemaName not in ('sys')
			and ExpectedObjectName <> ObjectName
	
	exec maint.usp_RunOrShowQuery @RunQuery = @ExecuteDirectly,  @TempTable = '#Query', @IngoreError = @IgnoreError
end
go
--exec maint.usp_ForceNamingConvention 