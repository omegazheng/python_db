set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_TestCursor @SQL nvarchar(max)
as
begin
	set statistics io, time on
	declare @handle int, @rc int
	exec sp_cursoropen @cursor = @handle output,@stmt = @SQL, @scrollopt = 4098, @ccopt = 8193,@rowcount = @rc output, @paramdef = N'@ConfigurationID int',  @ConfigurationID = 10
	exec sp_cursorclose @handle
	set statistics io, time off
end