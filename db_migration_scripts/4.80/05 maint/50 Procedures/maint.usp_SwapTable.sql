set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_SwapTable 
(
	@Table1 nvarchar(256), 
	@Table2 nvarchar(256),
	@AddForeignKeys bit = 1,
	@DropOriginal bit = 0
)
as
begin
	set nocount, xact_abort on
	declare @SourceSchemaName sysname, @SourceTableName sysname, @TargetSchemaName sysname, @TargetTableName sysname, @FullSourceTableName nvarchar(256), @FullTargetTableName nvarchar(256), @TempTableName sysname, @FullTempTableName nvarchar(256),
			@DropFK nvarchar(max), @AddFK nvarchar(max), @SQL nvarchar(max)
	select	@SourceSchemaName = object_schema_name(object_id(@Table1)),
			@SourceTableName = object_name(object_id(@Table1)),
			@TargetSchemaName = object_schema_name(object_id(@Table2)),
			@TargetTableName = object_name(object_id(@Table2)),
			@TempTableName = object_name(object_id(@Table1)) + '-'+cast(newid() as nvarchar(128))
	select	@FullSourceTableName = quotename(@SourceSchemaName) + '.' + quotename(@SourceTableName),
			@FullTempTableName = quotename(@SourceSchemaName) + '.' + quotename(@TempTableName),
			@FullTargetTableName = quotename(@TargetSchemaName) + '.' + quotename(@TargetTableName)
	if @SourceSchemaName is null
	begin
		raiserror('Cound not find table1 %s', 16, 1, @Table1)
		return
	end
	if @TargetSchemaName is null
	begin
		raiserror('Cound not find table2 %s', 16, 1, @Table2)
		return
	end
	if @TargetSchemaName <> @SourceSchemaName
	begin
		raiserror('Schema name for Table1 and Table2 are not nthe same.', 16, 1)
		return
	end
	select @DropFK = (
		select Definition + ';
' from maint.fn_GetObjectDefinition(@FullSourceTableName, @FullTempTableName) where Type in('DropForeignKey', 'DropForeignKeyReferencing') for xml path(''), type
	).value('.', 'nvarchar(max)'),
			@AddFK = (
		select Definition + ';
' from maint.fn_GetObjectDefinition(@FullSourceTableName, @FullTempTableName) where Type in('AddForeignKey', 'AddForeignKeyReferencing') for xml path(''), type
	).value('.', 'nvarchar(max)')

	--select * from maint.fn_GetObjectDefinition(@FullSourceTableName, @FullTempTableName) where Type in('DropForeignKey', 'DropForeignKeyReferencing') 
	set transaction isolation level serializable
	begin transaction
	begin try
		exec(@DropFk)
		if exists(select * from sys.change_tracking_tables where object_id = object_id(@FullSourceTableName))
		begin
			select @SQL = 'alter table ' + @FullSourceTableName + ' disable change_tracking;' 
			exec(@SQL)
		end
		
		
		exec sp_rename @FullSourceTableName, @TempTableName, 'Object'
		exec maint.usp_ForceNamingConvention @FullTempTablename
		
		exec sp_rename @FullTargetTableName, @SourceTableName, 'Object'
		exec maint.usp_ForceNamingConvention @FullSourceTableName
		if @AddForeignKeys = 1
			exec(@AddFK)
			
		exec maint.usp_ForceNamingConvention @FullTargetTableName

		exec sp_rename @FullTempTableName, @TargetTableName, 'Object'
		exec maint.usp_ForceNamingConvention @FullTargetTableName

		if @DropOriginal = 1
		begin
			select @SQL = 'drop table if exists ' + @FullTargetTableName
			exec(@SQL)
		end
	end try
	begin catch
		rollback;
		throw;
	end catch
	commit
end
go

--begin transaction
--exec maint.usp_SwapTable 'admin_all.payment', 'admin_all.payment_predeploy'
--rollback