set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_RemoveDuplicatedIndex
(
	@ObjectName nvarchar(255) = null, 
	@RemoveDirectly bit = 0
)
as
begin
	set nocount on;
	declare @SQL nvarchar(max);
	;with x0 as
	(
		select	SchemaName, ObjectName, IndexName, IndexType, IsUnique, IsUniqueConstraint,IndexColumns, IncludedColumns, FilterDefinition,
				row_number() over(partition by SchemaName, ObjectName, IndexType, IsUnique, IsUniqueConstraint,IndexColumns, IncludedColumns, FilterDefinition order by ObjectID, IndexID) RowNumber
		from maint.fn_GetIndexStructure(@ObjectName)
		where SchemaName not in ('sys')
			and IsPrimaryKey = 0
	)
	select 
			case 
				when IsUniqueConstraint = 1 then 
					'alter table ' + quotename(SchemaName) + '.' + quotename(ObjectName) + ' drop constraint '+ quotename(IndexName) + ';'
				else
					'drop index ' + quotename(SchemaName) + '.' + quotename(ObjectName) + '.' + quotename(IndexName) + ';' 
			end SQL,
			identity(int, 1,1) ID
		into #Query
	from x0 
	where RowNumber > 1
	exec maint.usp_RunOrShowQuery @RemoveDirectly
end
go
--exec maint.usp_RemoveDuplicatedIndex 1