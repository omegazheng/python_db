set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_RemovePreDeploymentJob
as
begin
	exec maint.usp_RemoveServiceJob 'PreDeployment'
end