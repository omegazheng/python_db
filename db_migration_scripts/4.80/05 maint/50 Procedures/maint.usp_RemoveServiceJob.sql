set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_RemoveServiceJob
(
	@Name nvarchar(255)
)
as
begin
	set nocount on
	declare @JobID uniqueidentifier 
	select @JobID = maint.fn_GetServiceJobID(@Name)
	if @JobID is not null
		exec msdb..sp_delete_job @job_id = @JobID;
	
end