set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_SynchronizationCallBack
(
	@ConfigurationID int, 
	@BatchCount int, 
	@RowsInBatch int, 
	@TotalRows bigint, 
	@BatchStartDate datetime2(7), 
	@BatchEndDate datetime2(7)
)
as
begin 
	set nocount on
	if @RowsInBatch not in(-1, -2)  -- start
	begin
		exec maint.usp_UpdateSynchronizationProgress @ConfigurationID = @ConfigurationID, @BatchCount = @BatchCount, @RowsInBatch = @RowsInBatch, @TotalRows = @TotalRows,  @BatchStartDate = @BatchStartDate, @BatchEndDate = @BatchEndDate
	end

end