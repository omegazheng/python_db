set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CreateSynchronizationConfiguration
(
	@Name varchar(128), 
	@InitializationQuery nvarchar(max), 
	@FilterProcedure nvarchar(256), 
	@FilteredQuery nvarchar(max), 
	@SourceTable nvarchar(256), 
	@FilterField nvarchar(128), 
	@TargetObject nvarchar(256), 
	@TargetObjectFieldList nvarchar(max), 
	@BatchSize int, 
	@IsActive bit,
	@ClearProgress bit =0,
	@ConfigurationID int = null output
)
as
begin
	set nocount, xact_abort on
	update maint.SynchronizationConfiguration
		set @ConfigurationID = ConfigurationID,
			Name = @Name,
			InitializationQuery = @InitializationQuery,
			FilterProcedure = @FilterProcedure,
			FilteredQuery = @FilteredQuery,
			SourceTable = @SourceTable,
			FilterField = @FilterField,
			TargetObject = @TargetObject,
			TargetObjectFieldList = @TargetObjectFieldList,
			BatchSize = @BatchSize,
			IsActive = @IsActive
	where Name = @Name
	if @@rowcount = 0
	begin
		insert into maint.SynchronizationConfiguration(Name, InitializationQuery, FilterProcedure, FilteredQuery, SourceTable, FilterField, TargetObject, TargetObjectFieldList, BatchSize, IsActive)
			values(@Name, @InitializationQuery, @FilterProcedure, @FilteredQuery, @SourceTable, @FilterField, @TargetObject, @TargetObjectFieldList, @BatchSize, @IsActive)
		select @ConfigurationID = scope_identity()
	end

	if @ClearProgress = 1
	begin
		delete maint.SynchronizationProgress where ConfigurationID = @ConfigurationID
		delete maint.SynchronizationHistory where ConfigurationID = @ConfigurationID
	end
end