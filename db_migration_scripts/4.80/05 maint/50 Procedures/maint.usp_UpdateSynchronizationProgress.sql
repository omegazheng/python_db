set ansi_nulls, quoted_identifier on
GO
create or alter procedure maint.usp_UpdateSynchronizationProgress
(
	@ConfigurationID int = null,
	@FromValue sql_variant = null,
	@ToValue sql_variant = null,
	@FromValueAdjusted sql_variant = null,
	@ToValueAdjusted sql_variant = null,

	@BatchCount int = null,
	@RowsInBatch int = null,
	@TotalRows bigint = null,
		
	@BatchStartDate datetime2(7) = null,
	@BatchEndDate datetime2(7) = null,

	@StartDate datetime2(7) = null,
	@EndDate datetime2(7) = null,

	@Error varchar(max) = null	
)
as
begin
	set nocount on
	if not exists(select* from maint.SynchronizationProgress where ConfigurationID = @ConfigurationID)
	begin
		insert into maint.SynchronizationProgress(ConfigurationID, StartDate, SessionID, ApplicationName) 
			values(@ConfigurationID, isnull(@StartDate, sysdatetime()), @@spid, app_name())
	end
	if @StartDate is not null
	begin
		update maint.SynchronizationProgress
			set StartDate = @StartDate,
				EndDate = null,
				Error = null,
				SessionID = @@spid,
				ApplicationName = app_name(),
				LoginName = system_user,
				SynchronizationHistoryID = null,
				BatchCount = null,
				RowsInBatch = null,
				TotalRows = null,
				BatchStartDate = null,
				BatchEndDate = null
		where ConfigurationID = @ConfigurationID
		--select name + ' = case when @' + name + ' is null then ' + name + ' else @'+ name+' end' from sys.columns where object_id = object_id('maint.SynchronizationProgress')
	end
	update maint.SynchronizationProgress
		set FromValue = case when @FromValue is null then FromValue else @FromValue end,
			ToValue = case when @ToValue is null then ToValue else @ToValue end,
			FromValueAdjusted = case when @FromValueAdjusted is null then FromValueAdjusted else @FromValueAdjusted end,
			ToValueAdjusted = case when @ToValueAdjusted is null then ToValueAdjusted else @ToValueAdjusted end,
			SessionID = @@spid,
			BatchCount = case when @BatchCount is null then BatchCount else @BatchCount end,
			RowsInBatch = case when @RowsInBatch is null then RowsInBatch else @RowsInBatch end,
			TotalRows = case when @TotalRows is null then TotalRows else @TotalRows end,
			BatchStartDate = case when @BatchStartDate is null then BatchStartDate else @BatchStartDate end,
			BatchEndDate = case when @BatchEndDate is null then BatchEndDate else @BatchEndDate end,
			EndDate = case when @EndDate is null then EndDate else @EndDate end,
			Error = case when @Error is null then Error else @Error end, 
			SynchronizationHistoryID = null,
			ApplicationName = app_name(),
			LoginName = system_user
	where ConfigurationID = @ConfigurationID
	if @EndDate is null -- not completed yet
		return
	declare @ReplicationHistoryID bigint
	if @EndDate is not null and @Error is null and exists(select * from maint.SynchronizationProgress where ConfigurationID = @ConfigurationID and TotalRows > 0)
	begin
		insert into maint.SynchronizationHistory(ConfigurationID, FromValue, ToValue, FromValueAdjusted, ToValueAdjusted, BatchCount, TotalRows, StartDate, EndDate, ApplicationName, LoginName)
			select ConfigurationID, FromValue, ToValue, FromValueAdjusted, ToValueAdjusted,BatchCount, TotalRows, StartDate, EndDate, ApplicationName, LoginName
			from maint.SynchronizationProgress 
			where ConfigurationID = @ConfigurationID 
		select @ReplicationHistoryID = scope_identity()
		update maint.SynchronizationProgress
			set SynchronizationHistoryID = @ReplicationHistoryID
		where ConfigurationID = @ConfigurationID
	end
end

