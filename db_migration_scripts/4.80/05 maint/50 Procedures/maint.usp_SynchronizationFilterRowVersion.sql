set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SynchronizationFilterRowVersion
(
	@ConfigurationID int,
	@FromValue sql_variant output, 
	@ToValue sql_variant output,
	@FromValueAdjusted sql_variant output,
	@ToValueAdjusted sql_variant output,
	@ShouldInitializate bit output
)
as
begin
	set nocount on
	select @ShouldInitializate = 0

	declare @SourceTable nvarchar(256), @SQL nvarchar(max), @FilterField nvarchar(128)


	select @SourceTable = SourceTable, @ToValue = null, @FilterField = FilterField
	from maint.SynchronizationConfiguration
	where ConfigurationID = @ConfigurationID

	select @SQL = 'select top 1 @ToValue = cast(' + @FilterField + ' as binary(8)) from ' + @SourceTable + ' order by ' + @FilterField + ' desc'
	exec sp_executesql @SQL, N'@ToValue sql_variant output', @ToValue output
	
	select @FromValue = ToValue
	from maint.SynchronizationProgress
	where ConfigurationID = @ConfigurationID
		--and FromValue is not null
		and ToValue is not null
	if @@rowcount = 0 or @ToValue is null
	begin
		select @ShouldInitializate = 1
	end
	if @FromValue is null
		select @FromValue = cast(0 as binary(8))
	select @FromValueAdjusted = @FromValue, @ToValueAdjusted = @ToValue
	return
end

go
--declare @FromValue sql_variant, @ToValue sql_variant, @FromValueAdjusted sql_variant, @ToValueAdjusted sql_variant, @ShouldInitializate bit
--exec maint.usp_SynchronizationFilterRowVersion  2, @FromValue output,  @ToValue output, @FromValueAdjusted output, @ToValueAdjusted output, @ShouldInitializate output
--select @FromValue ,  @ToValue , @FromValueAdjusted , @ToValueAdjusted , @ShouldInitializate 
