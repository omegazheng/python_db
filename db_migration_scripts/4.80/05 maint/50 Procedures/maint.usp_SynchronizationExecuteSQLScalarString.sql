set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SynchronizationExecuteSQLScalarString @ServerName varchar(128), @DatabaseName varchar(128), @Query nvarchar(max), @Result nvarchar(max) output
as
begin
	set nocount on
	declare @t table(Result nvarchar(max))
	insert into @t
		exec maint.usp_SynchronizationExecuteSQL @ServerName, @DatabaseName, @Query
	select @Result = (select top 1 Result from @t)
end
go
--exec maint.usp_ReplicationExecuteSQL null, 'gtc', 'select 1'