set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SynchronizationFilterIdentity
(
	@ConfigurationID int,
	@FromValue sql_variant output, 
	@ToValue sql_variant output,
	@FromValueAdjusted sql_variant output,
	@ToValueAdjusted sql_variant output,
	@ShouldInitializate bit output
)
as
begin
	set nocount on
	select @ShouldInitializate = 0

	declare @SourceTable nvarchar(256), @SQL nvarchar(max)


	select @SourceTable = SourceTable, @ToValue = null
	from maint.SynchronizationConfiguration
	where ConfigurationID = @ConfigurationID

	select @SQL = 'select top 1 @ToValue = $identity from ' + @SourceTable + ' order by $identity desc'
	exec sp_executesql @SQL, N'@ToValue sql_variant output', @ToValue output

	select @FromValue = ToValue
	from maint.SynchronizationProgress
	where ConfigurationID = @ConfigurationID
		--and FromValue is not null
		and ToValue is not null
	if @@rowcount = 0 or @ToValue is null
	begin
		select @ShouldInitializate = 1
	end
	select @FromValueAdjusted = @FromValue, @ToValueAdjusted = @ToValue
	return
end

--select  max($identity) from admin_all.ACCOUNT