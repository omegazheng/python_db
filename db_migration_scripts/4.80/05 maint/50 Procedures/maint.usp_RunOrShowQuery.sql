set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_RunOrShowQuery
(
	@RunQuery bit = 0, 
	@TempTable sysname = '#Query',
	@IngoreError bit = 0
)
as
begin
	set nocount on;
	set xact_abort off;
	
	if object_id('tempdb..'+@TempTable) is null
	begin
		print 'Could not find temp table (ID, Query)'
		return
	end
	if @RunQuery = 0
	begin
		exec('select * from #Query order by ID')
	end
	else
	begin
		declare @SQL nvarchar(max)
		declare c cursor local for
			select SQL
			from #Query
			order by ID
		open c
		fetch next from c into @SQL
		while @@fetch_status = 0
		begin
			begin try
				exec(@SQL);
			end try
			begin catch
				select @SQL = 'Error while executing query ..
'+ @SQL + '
'+error_message();
				if @IngoreError = 0
					throw 50000, @SQL, 16
				else 
					print @SQL;
			end catch
			fetch next from c into @SQL
		end
		close c
		deallocate c
	end	
end