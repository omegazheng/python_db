set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_ShowOverlappedIndex 
as
begin
	set nocount on;
	;with x0 as
	(
		select	SchemaName, ObjectName, IndexName, IndexType, IsUnique, IsUniqueConstraint,IndexColumns, IncludedColumns, FilterDefinition
		from maint.fn_GetIndexStructure(null)
		where SchemaName not in ('sys')
	)
	select 
			quotename(x0.SchemaName) + '.' + quotename(x0.ObjectName) + '.' + quotename(x0.IndexName) + '(' + x0.IndexColumns + ')' + case when x0.IncludedColumns is not null then 'include(' + x0.IncludedColumns+')' else '' end + case when x0.FilterDefinition is not null then 'Filter' + x0.FilterDefinition else '' end CoveringIndex,
			quotename(x1.SchemaName) + '.' + quotename(x1.ObjectName) + '.' + quotename(x1.IndexName) + '(' + x1.IndexColumns + ')' + case when x1.IncludedColumns is not null then 'include(' + x1.IncludedColumns+')' else '' end + case when x1.FilterDefinition is not null then 'Filter' + x1.FilterDefinition else '' end Overlapped,
'if exists(select * from sys.indexes where object_id = object_id(' + quotename(quotename(x0.SchemaName) + '.' + quotename(x0.ObjectName), '''') + ') and name = ' + quotename(x0.IndexName, '''') + ')
begin
	if exists(select * from sys.indexes where object_id = object_id(' + quotename(quotename(x1.SchemaName) + '.' + quotename(x1.ObjectName), '''') + ') and name = ' + quotename(x1.IndexName, '''') + ')
	begin
		'+
			case when x1.IsUniqueConstraint = 1 then 
				'alter table ' + quotename(x1.SchemaName) + '.' + quotename(x1.ObjectName) + ' drop constraint ' + quotename(x1.IndexName) + ';'
				else
				'drop index ' + quotename(x1.SchemaName) + '.' + quotename(x1.ObjectName) + '.' + quotename(x1.IndexName) + ';'
			end
		+'
	end
end
' QueryToDropOverLappedIndex
	from x0
		inner join x0 x1 on  x0.SchemaName = x1.SchemaName and x0.ObjectName = x1.ObjectName
	where x0.IndexName <> x1.IndexName
		and isnull(x0.FilterDefinition, '') = isnull(x1.FilterDefinition, '')
		and x0.IndexColumns+',' like x1.IndexColumns + ',%'
		and isnull(case when x0.IndexType = 'CLUSTERED' then x1.IncludedColumns else x0.IncludedColumns end, '') + ',' like isnull(x1.IncludedColumns, '') + ',%'
		and (x0.IsUnique = 1 or x0.IsUnique = x1.IsUnique )
		and x1.IndexType not in ('CLUSTERED')
		and x1.IndexType not like '%COLUMNSTORE%'
		and x0.IndexType not like '%COLUMNSTORE%'
end
go
--exec maint.usp_ShowOverlappedIndex
