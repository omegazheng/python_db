set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SetSynchronizationRowCount(@RowCount int)
as
begin
	exec sp_set_session_context N'SynchronizationRowCount', @RowCount
end