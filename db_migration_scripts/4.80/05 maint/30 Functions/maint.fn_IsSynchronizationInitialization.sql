set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_IsSynchronizationInitialization()
returns bit
as
begin
	return isnull(cast(session_context(N'IsSynchronizationInitialization') as int), 0)
end