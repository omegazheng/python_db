set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_GetExpectedName
(
	@Type varchar(50), -- Index, UniqueKey, PrimaryKey, ColumnStore, ForeignKey, Default
	@ParentSchemaName sysname, 
	@ParentObjectName sysname, 
	@ParentColumns nvarchar(max), --col1,col2...no space
	@ReferencedSchemaName sysname,
	@ReferencedObjectName sysname
)
returns nvarchar(128)
as
begin
	return	case 
				when @Type in ('Index') then 'IDX_' + @ParentSchemaName + '_' + @ParentObjectName + '_' + replace(@ParentColumns, ',', '_')
				when @Type in ('UniqueKey') then 'UQ_' + @ParentSchemaName + '_' + @ParentObjectName + '_' + replace(@ParentColumns, ',', '_')
				when @Type in ('PrimaryKey') then 'PK_' + @ParentSchemaName + '_' + @ParentObjectName 
				when @Type in ('ColumnStore') then 'IDX_CS_' + @ParentSchemaName + '_' + @ParentObjectName 
				when @Type in ('ForeignKey') then 'FK_' + @ParentSchemaName + '_' + @ParentObjectName
													+ case when @ParentSchemaName = @ReferencedSchemaName then '' else '_' + @ReferencedSchemaName end
													+ '_' + @ReferencedObjectName  
													+ '_' + replace(@ParentColumns, ',', '_')
				when @Type in ('Default') then 'DF_' + @ParentSchemaName + '_' + @ParentObjectName + '_' + @ParentColumns
			end
			
end