set ansi_nulls, quoted_identifier on 
go

create or alter function maint.fn_GetForeignKeyStructure(@ParentObject nvarchar(255), @ReferencedObject nvarchar(255))
returns table
as
return  (
			select 
					fk.object_id as ObjectID,
					object_schema_name(fk.object_id) SchemaName,
					fk.name as ObjectName,
					maint.fn_GetExpectedName('ForeignKey',object_schema_name(fk.parent_object_id), object_name(fk.parent_object_id), stuff((select '_'+col_name(fkc.parent_object_id, fkc.parent_column_id) from sys.foreign_key_columns fkc where fkc.constraint_object_id = fk.object_id order by fkc.constraint_column_id for xml path('')), 1, 1, ''), object_schema_name(fk.referenced_object_id), object_name(fk.referenced_object_id)) ExpectedObjectName,
					fk.parent_object_id ParentObjectID,
					object_name(fk.parent_object_id) ParentTableName,
					fk.referenced_object_id ReferenceObjectID,
					object_schema_name(fk.referenced_object_id) ReferencedSchemaName,
					object_name(fk.referenced_object_id) ReferencedTableName,
					stuff((select ',' + col_name(fkc.parent_object_id, fkc.parent_column_id) from sys.foreign_key_columns fkc where fkc.constraint_object_id = fk.object_id order by fkc.constraint_column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '') ParentColumns,
					stuff((select ',' + col_name(fkc.referenced_object_id, fkc.referenced_column_id) from sys.foreign_key_columns fkc where fkc.constraint_object_id = fk.object_id order by fkc.constraint_column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '') ReferencedColumns,
					fk.delete_referential_action_desc DeleteAction,
					fk.update_referential_action_desc UpdateAction,
					fk.is_system_named IsSystemNamed,
					fk.is_disabled IsDisabled,
					fk.is_not_trusted IsNotTusted
			from sys.foreign_keys fk
			where (@ParentObject is null or fk.parent_object_id = object_id(@ParentObject))
				and (@ReferencedObject is null or fk.referenced_object_id = object_id(@ReferencedObject))
)
go
--select * from maint.fn_GetForeignKeyStructure(null, null)  where ObjectName <> ExpectedObjectName





