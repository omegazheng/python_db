set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_ReplaceFirstOccurence(@String nvarchar(max), @SubString nvarchar(max), @StringReplacement nvarchar(max))
returns nvarchar(max)
as
begin
	return isnull(stuff(@String, charindex(@SubString, @String), LEN(@SubString), @StringReplacement),@String) 
end
go
--select maint.fn_ReplaceFirstOccurence('abcdeabcde', 'xyz', 'a')