set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_IsAmazonDatabase()
returns bit
as
begin
	if db_id('rdsadmin') is null
		return 0
	if @@servername like 'EC2AMAZ%%'
		return 1
	return 0
end
