set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_ObjectNameToNamingConvention(@Name nvarchar(300))
returns nvarchar(300)
as
begin
	return case when parsename(@Name, 2) is not null then parsename(@Name, 2) + '_' else '' end +  parsename(@Name, 1)
end
go
--select maint.fn_ObjectNameToNamingConvention('11.[22]')
