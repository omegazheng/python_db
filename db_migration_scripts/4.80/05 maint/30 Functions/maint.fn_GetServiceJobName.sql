set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_GetServiceJobName (@Name nvarchar(255))
returns nvarchar(255)
as
begin
	return @Name +' - '+quotename(db_name())
end