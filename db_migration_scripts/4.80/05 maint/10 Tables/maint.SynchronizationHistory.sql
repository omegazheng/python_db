set ansi_nulls, quoted_identifier on
--drop table maint.SynchronizationHistory
go
if object_id('maint.SynchronizationHistory') is null
begin
	create table maint.SynchronizationHistory
	(
		SynchronizationHistoryID bigint identity(1,1),
		ConfigurationID int not null,
		FromValue sql_variant,
		ToValue sql_variant,
		FromValueAdjusted sql_variant,
		ToValueAdjusted sql_variant,

		BatchCount int not null,
		TotalRows bigint not null,

		StartDate datetime2(7) not null,
		EndDate datetime2(7) not null,

		ApplicationName varchar(128) ,
		LoginName varchar(128),
		constraint PK_maint_SynchronizationHistory primary key(SynchronizationHistoryID),
		index IDX_maint_SynchronizationHistory_ConfigurationID(ConfigurationID)
	)
end
go


