if exists(select * from sys.indexes where name = 'IDX_admin_all_BRAND_GAME_JP_RATIO_BRANDID' and object_id = object_id('admin_all.BRAND_GAME_JP_RATIO'))
begin
	drop index admin_all.BRAND_GAME_JP_RATIO.IDX_admin_all_BRAND_GAME_JP_RATIO_BRANDID
end
if not exists(select * from sys.indexes where name = 'IDX_admin_all_BRAND_GAME_JP_RATIO_BRANDID_GAME_INFO_ID_LAST_UPDATE' and object_id = object_id('admin_all.BRAND_GAME_JP_RATIO'))
begin
	create index IDX_admin_all_BRAND_GAME_JP_RATIO_BRANDID_GAME_INFO_ID_LAST_UPDATE on admin_all.BRAND_GAME_JP_RATIO(BRANDID, GAME_INFO_ID, LAST_UPDATE) include(RATIO)
end

--drop index admin_all.BRAND_GAME_JP_RATIO.IDX_admin_all_BRAND_GAME_JP_RATIO_BRANDID_GAME_INFO_ID_LAST_UPDATE