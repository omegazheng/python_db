if not exists(select * from sys.indexes where object_id = object_id('admin_all.USER_DOCUMENTS') and name = 'IDX_admin_all_USER_DOCUMENTS_EXPIRY_DATE')
	create index IDX_admin_all_USER_DOCUMENTS_EXPIRY_DATE on admin_all.USER_DOCUMENTS(EXPIRY_DATE)
