set ansi_nulls, quoted_identifier on
go

if isnull(objectproperty(object_id('admin_all.v_AccountTranLiveToDate'), 'IsSchemaBound'), 0) = 0
	or not exists(select * from sys.sql_modules where object_id = object_id('admin_all.v_AccountTranLiveToDate') and definition like '%admin_all.AccountTranAggregate%')
exec('create or alter view admin_all.v_AccountTranLiveToDate with schemabinding
as
	select	
		AggregateType,
		PartyID, 
		TranType,
		sum(AmountReal) AmountReal, 
		sum(AmountReleasedBonus) AmountReleasedBonus, 
		sum(AmountPlayableBonus) AmountPlayableBonus,
		Count_Big(*) Count
	from admin_all.AccountTranAggregate
	where Period = ''Y''
	group by AggregateType,TranType,PartyID
')
go
if not exists(select * from sys.indexes where object_id = object_id('[admin_all].[v_AccountTranLiveToDate]') and name = 'PK_admin_all_v_AccountTranLiveToDate' and type_desc = 'CLUSTERED')
CREATE UNIQUE CLUSTERED INDEX [PK_admin_all_v_AccountTranLiveToDate]
    ON [admin_all].[v_AccountTranLiveToDate]([AggregateType] ASC, [TranType] ASC, [PartyID] ASC);
GO
