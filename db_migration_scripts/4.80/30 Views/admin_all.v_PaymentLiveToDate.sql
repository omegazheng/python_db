set ansi_nulls, quoted_identifier on
go
if isnull(objectproperty(object_id('admin_all.v_PaymentLiveToDate'), 'IsSchemaBound'), 0) = 0
exec('create or alter view admin_all.v_PaymentLiveToDate with schemabinding
as
	select	
		ACCOUNT_ID As AccountID,
		TYPE as Type, 
		METHOD as Method,
		STATUS as Status,
		sum(AMOUNT) Amount,
		sum(AMOUNT_REAL) AmountReal, 
		sum(AMOUNT_RELEASED_BONUS) AmountReleasedBonus, 
		sum(FEE) Fee,
		Count_Big(*) Count
	from admin_all.PAYMENT
	group by ACCOUNT_ID, TYPE, METHOD, STATUS
')
go
if not exists(select * from sys.indexes where object_id = object_id('[admin_all].[v_PaymentLiveToDate]') and name = 'PK_admin_all_v_PaymentLiveToDate')
CREATE UNIQUE CLUSTERED INDEX [PK_admin_all_v_PaymentLiveToDate]
    ON [admin_all].[v_PaymentLiveToDate]([AccountID] ASC, [Type] ASC, [Method] ASC, [Status] ASC);
GO
