
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[VOUCHER]') AND type in (N'U'))
  BEGIN
    CREATE TABLE [admin_all].[VOUCHER]
    (
      [ID]              [int] IDENTITY (1,1)    NOT NULL,
      [BrandId]         [int]                   NOT NULL,
      [Code]            [NVARCHAR](50)          NOT NULL,
      [Status]          [VARCHAR](25)           NOT NULL,
      [Retailer]        [NVARCHAR](50)          NULL,
      [Amount]          [NUMERIC](38,18)        NOT NULL,
      [Currency]        [NCHAR](3)              NOT NULL,
      [PaymentId]       [int]                   NULL,
      [PartyId]         [int]                   NULL,
      [ExpiryDate]      [datetime]              NOT NULL,
      [RedemptionDate]  [datetime]              NULL,
      [CreateDate]      [datetime]              NOT NULL
          constraint DF_VOUCHER_CreateDate default (getdate())
    )
  END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_Voucher_BrandId]') AND parent_object_id = OBJECT_ID(N'[admin_all].[VOUCHER]'))
  ALTER TABLE [admin_all].[VOUCHER]  WITH CHECK ADD CONSTRAINT [FK_admin_all_VOUCHER_BrandId] FOREIGN KEY([BrandId])
  REFERENCES [admin].[CASINO_BRAND_DEF] ([BRANDID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_Voucher_PaymentId]') AND parent_object_id = OBJECT_ID(N'[admin_all].[VOUCHER]'))
  ALTER TABLE [admin_all].[VOUCHER]  WITH CHECK ADD CONSTRAINT [FK_admin_all_VOUCHER_PaymentId] FOREIGN KEY([PaymentId])
  REFERENCES [admin_all].[PAYMENT] ([ID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_Voucher_PartyId]') AND parent_object_id = OBJECT_ID(N'[admin_all].[VOUCHER]'))
  ALTER TABLE [admin_all].[VOUCHER]  WITH CHECK ADD CONSTRAINT [FK_admin_all_VOUCHER_PartyId] FOREIGN KEY([PartyId])
  REFERENCES [external_mpt].[USER_CONF] ([PARTYID])
GO
