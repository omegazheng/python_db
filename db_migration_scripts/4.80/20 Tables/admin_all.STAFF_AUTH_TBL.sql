if exists(select * from sys.columns where object_id = object_id('[admin_all].[STAFF_AUTH_TBL]') and name = 'FA_REQUIRED')
    exec sp_rename 'admin_all.STAFF_AUTH_TBL.FA_REQUIRED', 'TWO_FA_ALWAYS_REQUIRED', 'COLUMN'
go

