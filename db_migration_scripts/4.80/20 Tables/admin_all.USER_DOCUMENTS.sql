SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Staff, User, Agent Enum
IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'UPLOADED_BY_ENTITY')
    begin
        ALTER TABLE [admin_all].[USER_DOCUMENTS]
            ADD [UPLOADED_BY_ENTITY] [nvarchar](10) NULL
    end
go

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'UPLOADER_ID')
    begin
        ALTER TABLE [admin_all].[USER_DOCUMENTS]
            ADD [UPLOADER_ID] [int] NULL
    end
go

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'REVIEWED_BY_ENTITY')
    begin
        ALTER TABLE [admin_all].[USER_DOCUMENTS]
            ADD [REVIEWED_BY_ENTITY] [nvarchar](10) NULL
    end
go

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'REVIEWER_ID')
    begin
        ALTER TABLE [admin_all].[USER_DOCUMENTS]
            ADD [REVIEWER_ID] [int] NULL
    end
go
