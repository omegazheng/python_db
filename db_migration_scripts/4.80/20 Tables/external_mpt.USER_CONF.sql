if exists(select * from sys.columns where object_id = object_id('[external_mpt].[USER_CONF]') and name = 'FA_REQUIRED')
    exec sp_rename 'external_mpt.USER_CONF.FA_REQUIRED', 'TWO_FA_ALWAYS_REQUIRED', 'COLUMN';
go

if not exists(select * from sys.columns where object_id = object_id('[external_mpt].[USER_CONF]') and name = 'NATIONALITY')
    alter table external_mpt.USER_CONF add NATIONALITY [char](2) NULL
go

set xact_abort on
begin transaction
    if exists(select * from sys.columns where object_id = object_id('[external_mpt].[USER_CONF]') and name = 'EMAIL' and max_length < 512)
        begin
            if exists(select * from sys.indexes i where i.object_id = object_id('external_mpt.user_conf') and i.name = 'IDX_external_mpt_USER_CONF_EMAIL_BRANDID')
                drop index external_mpt.USER_CONF.IDX_external_mpt_USER_CONF_EMAIL_BRANDID
            if exists(select * from sys.indexes i where i.object_id = object_id('external_mpt.user_conf') and i.name = 'IDX_external_mpt_USER_CONF_BRANDID_EMAIL')
                drop index external_mpt.USER_CONF.IDX_external_mpt_USER_CONF_BRANDID_EMAIL

            alter table external_mpt.USER_CONF alter column EMAIL NVARCHAR(512) NULL
                ​
        end
commit
go

if not exists(select * from sys.indexes i where i.object_id = object_id('external_mpt.user_conf') and i.name = 'IDX_external_mpt_USER_CONF_EMAIL_BRANDID')
    create index IDX_external_mpt_USER_CONF_EMAIL_BRANDID on external_mpt.USER_CONF (EMAIL, BRANDID)

if not exists(select * from sys.indexes i where i.object_id = object_id('external_mpt.user_conf') and i.name = 'IDX_external_mpt_USER_CONF_BRANDID_EMAIL')
    create unique index IDX_external_mpt_USER_CONF_BRANDID_EMAIL on external_mpt.USER_CONF (BRANDID, EMAIL) where [EMAIL] IS NOT NULL
