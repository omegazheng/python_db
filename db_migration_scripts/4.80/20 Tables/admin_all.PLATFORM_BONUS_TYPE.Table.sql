SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[PRODUCT_BONUS_TYPE]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[PRODUCT_BONUS_TYPE]
(
    [ID]          [int] IDENTITY (1,1) NOT NULL,
    [PRODUCT_ID] [int]                NOT NULL,
    [BONUS_TYPE]  [varchar](50)        NOT NULL,
    CONSTRAINT [PK_PRODUCT_BONUS_TYPE] PRIMARY KEY CLUSTERED
        (
         [ID] ASC
        )
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[PRODUCT_ID]') AND parent_object_id = OBJECT_ID(N'[admin_all].[PRODUCT_BONUS_TYPE]'))
ALTER TABLE [admin_all].[PRODUCT_BONUS_TYPE]  WITH CHECK ADD CONSTRAINT [PRODUCT_ID] FOREIGN KEY([PRODUCT_ID])
    REFERENCES [admin_all].[PLATFORM] ([ID])
GO
