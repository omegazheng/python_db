Here's the example to test the task.

#### Create Job
declare @i int
exec admin_all.usp_CreateBatchJob @BatchJobID = @i output, @Type = 4, @Name = 'Bulk Upload Test'
select @i
select * from admin_all.BatchJob

#####add tasks
```sql
begin transaction
    declare @Content nvarchar(max)
    select @Content =
           '[{"brandId":"1", "userId":"omegazheng","bonusGameType":"CASINO","bonusAmount":"10.00","wagerRequirement":"100.00","expiryDate":"2020-12-29","comment":"comment1"},{"brandId":"1", "userId":"omegazhengeur","bonusGameType":"SPORTSBOOK","bonusAmount":"10.00","wagerRequirement":"100.00","expiryDate":"2020-12-31","comment":"comment2"}]'
    exec admin_all.usp_AddBatchJobTask   1, @Content
    select *
    from admin_all.BatchJobTask where BatchJobID = 1
rollback
```

#####get a task
```sql
begin transaction
exec admin_all.usp_UpdateBatchJob @BatchJobID = 1, @BatchJobStatus= 'RUNNING'
exec admin_all.usp_GetBatchJobTask
```

#####select * from admin_all.BatchJobTask
```sql
exec admin_all.usp_GetBatchJobTask
rollback

```

#####Show tasks
```sql
exec admin_all.usp_ShowBatchJobTask 1
```
