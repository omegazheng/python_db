set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypePresenterJson
(
	@Content nvarchar(max)
)
returns table
as
return
(
	select @Content as Content
)
go
--select * from admin_all.fn_BatchJobTypePresenterJson(cast(N'{"playerId":"1005145","bonusProgramId":10485}' as nvarchar(max)))


