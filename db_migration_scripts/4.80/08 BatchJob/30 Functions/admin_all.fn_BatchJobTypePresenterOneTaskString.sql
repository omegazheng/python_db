set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypePresenterOneTaskString
(
	@Content nvarchar(max)
)
returns table
as
return
(
	select @Content as Content
)
go

