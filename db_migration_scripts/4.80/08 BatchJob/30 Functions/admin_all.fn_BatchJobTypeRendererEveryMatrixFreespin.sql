set ansi_nulls, quoted_identifier on
GO
create or alter function admin_all.fn_BatchJobTypeRendererEveryMatrixFreespin
(
	@Content nvarchar(max)
)
returns table
as
return
(

	with x1 as
	(
		select	
				cast(json_value(@Content, '$.partyID')  as int) as PartyID,
				cast(json_value(@Content, '$.subPlatformName')  as nvarchar(100)) as SubPlatformName,
                cast(json_value(@Content, '$.expiryDate')  as nvarchar(100)) as ExpiryDate,
                cast(json_value(@Content, '$.gameInfoId')  as nvarchar(100)) as GameInfoId,
                cast(json_value(@Content, '$.rounds')  as int) as Rounds,
                cast(json_value(@Content, '$.coins')  as int) as Coins,
                cast(json_value(@Content, '$.coinVal')  as int) as CoinVal,
                cast(json_value(@Content, '$.betLevel')  as int) as BetLevel,
                cast(json_value(@Content, '$.denomination')  as float) as Denomination,
                cast(json_value(@Content, '$.coinSize')  as int) as CoinSize,
                cast(json_value(@Content, '$.lineVal')  as int) as LineVal,
                cast(json_value(@Content, '$.coinValue')  as int) as CoinValue
	)
	select x1.*, u.USERID, u.CURRENCY, u.BRANDID, u.EMAIL
	from x1
		left join external_mpt.USER_CONF u on u.PARTYID = x1.PartyID
)
go
