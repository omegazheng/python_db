set ansi_nulls, quoted_identifier on
GO
create or alter function admin_all.fn_BatchJobTypeRendererKambiFreeBet
(
	@Content nvarchar(max)
)
returns table
as
return
(

	with x1 as
	(
		select	
				cast(json_value(@Content, '$.rewardTemplateId')  as int) as RewardTemplateID,
				cast(json_value(@Content, '$.playerId')  as int) as PartyID--,

	)
	select x1.PartyID, x1.RewardTemplateID, u.USERID, u.CURRENCY, u.BRANDID, u.EMAIL
	from x1
		left join external_mpt.USER_CONF u on u.PARTYID = x1.PartyID
)
go

--select * from admin_all.fn_BatchJobTypeRendererKambiFreeBet(cast(N'{"playerId":"999999","bonusProgramId":10485}' as nvarchar(max)))


--select top 10 * from external_mpt.USER_CONF
