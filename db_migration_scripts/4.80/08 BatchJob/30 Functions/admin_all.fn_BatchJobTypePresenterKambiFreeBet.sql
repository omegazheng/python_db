set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypePresenterKambiFreeBet
(
	@Content nvarchar(max)
)
returns table
as
return
(
	with x0 as
	(
		select @Content as Content
	),
	x1 as
	(
		select	cast(p.value as int) as PartyID, 
				cast(json_value(x0.Content, '$.rewardTemplateId')  as int) as RewardTemplatID
		from x0	
			cross apply openjson(x0.Content, '$.playerIds') p
	)
	select x1.PartyID, x1.RewardTemplatID, u.USERID, u.CURRENCY, u.BRANDID, u.EMAIL
	from x1
		left join external_mpt.USER_CONF u on u.PARTYID = x1.PartyID
		


)
go
--select * from admin_all.fn_BatchJobTypePresenterKambiFreeBet(cast(cast(N'{"playerIds":["100062694", "100062695"],"rewardTemplateId":6595}' as nvarchar(max)) as nvarchar(max)))


--select * from openjson(N'{"playerIds":["100062694", "100062695"],"rewardTemplateId":6595}', '$.playerIds')