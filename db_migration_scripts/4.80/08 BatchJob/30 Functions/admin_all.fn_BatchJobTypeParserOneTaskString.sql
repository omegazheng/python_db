set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypeParserOneTaskString
(
	@Content nvarchar(max)
)
returns table
as
return
(
	select @Content Content
	
)
go
--select * from admin_all.fn_BatchJobTypeParserTest(cast(N'1,2,3' as nvarchar(max)))
