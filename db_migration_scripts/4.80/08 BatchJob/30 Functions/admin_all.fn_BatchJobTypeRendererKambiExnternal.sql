set ansi_nulls, quoted_identifier on
GO
create or alter function admin_all.fn_BatchJobTypeRendererKambiExnternal
(
	@Content nvarchar(max)
)
returns table
as
return
(

	with x1 as
	(
		select	
				cast(json_value(@Content, '$.bonusProgramId')  as int) as BonusProgramId,
				cast(json_value(@Content, '$.playerId')  as int) as PartyID

	)
	select x1.PartyID, x1.BonusProgramId, u.USERID, u.CURRENCY, u.BRANDID, u.EMAIL
	from x1
		left join external_mpt.USER_CONF u on u.PARTYID = x1.PartyID
)
go

