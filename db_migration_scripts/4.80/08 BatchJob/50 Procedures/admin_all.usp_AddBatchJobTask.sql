set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_AddBatchJobTask 
(
	@BatchJobID int, 
	@Content nvarchar(max)
)
as
begin
	set nocount on
	declare @SQL nvarchar(max), @Parser nvarchar(128)
	select @Parser = t.Parser
	from admin_all.BatchJobType t
		inner join admin_all.BatchJob j on t.BatchJobTypeID = j.Type
	where j.BatchJobID = @BatchJobID
	select @SQL = 'select @BatchJobID BatchJobID, Content from ' + @Parser + '(@Content) a'
	insert into admin_all.BatchJobTask(BatchJobID, Content)
	exec sp_executesql @SQL, N'@BatchJobID int, @Content nvarchar(max)', @BatchJobID, @Content
	
end
go
/*
begin transaction
declare @Content nvarchar(max)
select @Content = cast(cast(stuff((select top 1000 ',' + cast(PARTYID as nvarchar(20)) from external_mpt.USER_CONF for xml path('')), 1, 1, '') as nvarchar(max)) as nvarchar(max))
exec admin_all.usp_AddBatchJobTask  1, @Content


--select * from admin_all.BatchJob
commit
*/

