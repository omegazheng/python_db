set ansi_nulls, quoted_identifier on
go

-- clean up old sent email job contents thats older than x months
create or alter procedure admin_all.usp_CleanupSentEmails
as
begin
	set nocount on
	begin transaction
	exec sp_getapplock 'BatchJob', 'Exclusive', 'Transaction', -1
	--select j.BatchJobID , t.*
	delete t
	from admin_all.BatchJob j
		inner join admin_all.BatchJobTask t on j.BatchJobID = t.BatchJobID
	where j.Type = -1 and t.Status = 'COMPLETED'
		and t.EndDate is not null
		and t.EndDate < dateadd(month, -3, getdate())
	commit
end
go
