set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CreateBatchJobType
(
	@Name nvarchar(128)= null,
	@Description nvarchar(256)= null, 
	@ProdctID int =  null,
	@MaxAllowableTasks int= null,
    @Parser nvarchar(256)= null,
    @Renderer nvarchar(256)= null,
    @Presenter nvarchar(256)= null,
	@BatchJobTypeID int = null output
)
as
begin
	set nocount, xact_abort on
    insert into admin_all.BatchJobType (Name, Description, ProductID, MaxAllowableTasks, Parser, Renderer, Presenter)
		values(@Name, @Description, @ProdctID, @MaxAllowableTasks, @Parser, @Renderer, @Presenter)
	select @BatchJobTypeID = scope_identity()
end
go
