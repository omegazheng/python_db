set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_UpdateBatchJob 
(
	@BatchJobID int = null,
	@BatchJobType int = null, 
	@BatchJobName nvarchar(128) = null, 
	@BatchJobDescription nvarchar(256)= null, 
	@BatchJobStaffID int =  null,
	@BatchJobStopBatchWhenError bit = null,
	@BatchJobStatus nvarchar(30) = null,

	@BatchJobTaskIDs nvarchar(max) = null,
	@BatchJobTaskStatus nvarchar(30) = null,
	@BatchJobInfo nvarchar(max) = null
)
as
begin
	set nocount on
	begin transaction
	exec sp_getapplock 'BatchJob', 'Exclusive', 'Transaction', -1
	if @BatchJobID is not null
	begin
		update admin_all.BatchJob
			set Type = isnull(@BatchJobType, Type),
				Name = isnull(@BatchJobName, Name),
				Description = isnull(@BatchJobDescription, Description),
				StaffID = isnull(@BatchJobStaffID, StaffID),
				StopBatchWhenError = isnull(@BatchJobStopBatchWhenError, StopBatchWhenError),
				Status = isnull(@BatchJobStatus, Status)
		where BatchJobID = @BatchJobID
	end
	if @BatchJobTaskIDs is not null
	begin
		update admin_all.BatchJobTask
			set Status = isnull(@BatchJobTaskStatus, Status),
				Info = isnull(@BatchJobInfo, Info)
		where BatchJobTaskID in (select cast(value as bigint) from string_split(@BatchJobTaskIDs, ','))
	end
	commit
end
go

--begin transaction
--exec admin_all.usp_UpdateBatchJob @BatchJobID = 2, @BatchJobStatus = 'RUNNING'

--exec admin_all.usp_BatchJobGetTask
--select * from admin_all.BatchJob
--select * from admin_all.BatchJobTask
--rollback