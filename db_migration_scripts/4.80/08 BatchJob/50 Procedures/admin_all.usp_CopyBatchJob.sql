set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CopyBatchJob
(
	@SourceBatchJobID int,
	@TargetBatchJobName	nvarchar(128) = null, 
	@TargetDescription	nvarchar(256) = null, 
	@BatchJobTaskIDs varchar(max) = null,
	@TaskStatuses varchar(max) = null,
	@NewBatchJobID int output
)
as
begin
	set xact_abort, nocount on
	declare @rc int
	create table #IDs(BatchJobTaskID bigint primary key)
	insert into #IDs(BatchJobTaskID)
		select distinct BatchJobTaskID
		from (select try_cast(value as bigint)BatchJobTaskID  from string_split(@BatchJobTaskIDs, ','))x
		where x.BatchJobTaskID is not null

	create table #Status(Status nvarchar(30) primary key)
	insert into #Status(Status)
		select distinct Status
		from (select nullif(ltrim(rtrim(value)), '') Status  from string_split(@TaskStatuses, ','))x
		where x.Status is not null


	begin transaction
	insert into admin_all.BatchJob(Type, Name, Description, StaffID, StopBatchWhenError, BrandID)
		select Type, @TargetBatchJobName, @TargetDescription, StaffID, StopBatchWhenError, BrandID
		from admin_all.BatchJob
		where BatchJobID = @SourceBatchJobID
	select @rc = @@rowcount, @NewBatchJobID = scope_identity()
	if @rc = 0
	begin
		raiserror('Could not find SourceBatchJobID, %d.', 16, 1, @SourceBatchJobID)
		rollback
		return
	end
	insert into admin_all.BatchJobTask(BatchJobID, Content)
		select @NewBatchJobID, t.Content
		from admin_all.BatchJobTask t
		where t.BatchJobID = @SourceBatchJobID
			and(
					@BatchJobTaskIDs is null 
					or
					exists(select * from #IDs x where x.BatchJobTaskID = t.BatchJobTaskID)
				)
			and(
					@TaskStatuses is null
					or
					exists(select * from #Status x where x.Status = t.Status)
				)
	commit
end
go
--begin transaction
--declare @NewBatchJobID int 
----select * from admin_all.BatchJobTask where BatchJobID = 235
--exec admin_all.usp_CopyBatchJob 235, 'new', 'new', 2, @NewBatchJobID output
--select * from admin_all.BatchJob where BatchJobID = @NewBatchJobID
--select * from admin_all.BatchJobTask where BatchJobID = @NewBatchJobID
--rollback
