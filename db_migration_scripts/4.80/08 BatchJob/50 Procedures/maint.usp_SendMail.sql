SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create or alter procedure maint.usp_SendMail
(
	@profile_name sysname = null,        
	@recipients varchar(max) = null, 
	@copy_recipients varchar(max) = null,
	@blind_copy_recipients varchar(max) = null,
	@subject nvarchar(255) = null,
	@body nvarchar(max) = null, 
	@body_format varchar(20) = 'TEXT',  -- HTML
	@importance varchar(6) = 'NORMAL', --'HIGH, LOW'
	@sensitivity varchar(12) = 'NORMAL',--Personal, Private, Confidential
	@reply_to varchar(max) = null,
	@from_address varchar(max) = null
)
as
begin
	set nocount on
	if isnull(admin_all.fn_GetRegistry('System.Mail.Enabled'),'1') <>'1'
		return
	if isnull(admin_all.fn_GetRegistry('System.Mail.UseMSDB'),'1') = 1
	begin
		exec msdb..sp_send_dbmail @profile_name = @profile_name, @recipients = @recipients,  @copy_recipients = @copy_recipients, @blind_copy_recipients = @blind_copy_recipients, @subject = @subject, @body = @body,  @body_format = @body_format, @importance = @importance, @sensitivity = @sensitivity, @reply_to = @reply_to, @from_address = @from_address
		return
	end
	--insert into maint.MailItem(ProfileName, Recipients, CopyRecipients, BlindCopyRecipients, Subject, Body, BodyFormat, Importance, Sensitivity, ReplyTo, FromAddress)
	declare @Json nvarchar(max), @BatchJobID int
	select @Json = (select * from (select @profile_name ProfieName, @recipients Recipients, @copy_recipients CopyRecipients, @blind_copy_recipients BlindCopyRecipients, @subject Subject, @body Body, @body_format BodyFormat, @importance Importance, @sensitivity Sesitivity, @reply_to ReplyTo, @from_address FromAddress)t for json auto)
	begin transaction
	select @BatchJobID = BatchJobID from admin_all.BatchJob where Type = -1
	if @@rowcount  = 0
	begin
		exec admin_all.usp_CreateBatchJob @Name = 'SendEmail', @Type = -1, @BatchJobID = @BatchJobID output
	end
	exec admin_all.usp_AddBatchJobTask @BatchJobID, @Json
	if not exists(select * from admin_all.BatchJob where Status = 'RUNNING' and BatchJobID = @BatchJobID)
		exec admin_all.usp_UpdateBatchJob @BatchJobID = @BatchJobID, @BatchJobStatus = 'RUNNING'
	commit
end
go

--exec maint.usp_SendMail @profile_name = 'a', @recipients = 'b', @copy_recipients ='copy', @blind_copy_recipients = 'bcc', @subject = 'test', @body = 'body',  @reply_to = 'reply', @from_address = 'dfdafasd'



