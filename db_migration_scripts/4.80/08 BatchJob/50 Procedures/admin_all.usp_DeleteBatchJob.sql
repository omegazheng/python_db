set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_DeleteBatchJob 
(
	@BatchJobID int = null,
	@BatchJobTaskIDs nvarchar(max) = null
	
)
as
begin
	set nocount, xact_abort on
	begin transaction
	if @BatchJobID is not null
	begin
		delete admin_all.BatchJobTask where BatchJobID = @BatchJobID
		delete admin_all.BatchJob where BatchJobID = @BatchJobID
	end
	if @BatchJobTaskIDs is not null
	begin
		delete admin_all.BatchJobTask where BatchJobTaskID in (select cast(value as bigint) from string_split(@BatchJobTaskIDs, ','))
	end
	commit
end
go
