set ansi_nulls, quoted_identifier on
go
---drop table admin_all.BatchJob
go
if object_id('admin_all.BatchJob') is null
begin
	create table admin_all.BatchJob
	(
		BatchJobID int not null identity(1,1),
		Name nvarchar(128) null,
		Description nvarchar(max),
		BrandID int,
		StaffID int,
		
		Type int not null,
		Priority tinyint not null constraint DF_admin_all_BatchJob_Priority default(5), 
		Status nvarchar(30) not null  constraint DF_admin_all_BatchJob_Status default('PENDING'),

		TotalTasks int not null constraint DF_admin_all_BatchJob_TotalTasks default(0),
		ProcessedTasks int not null constraint DF_admin_all_BatchJob_ProcessedTasks default(0),
		FailedTasks int not null constraint DF_admin_all_BatchJob_FailedTasks default(0),
		PendingTasks int not null constraint DF_admin_all_BatchJob_PendingTasks default(0),
		RunningTasks int not null constraint DF_admin_all_BatchJob_RunningTasks default(0),
		
		CreationDate datetime2(7) not null constraint DF_admin_all_BatchJob_CreationDate default(getdate()),
		StartDate datetime2(7) null,
		EndDate datetime2(7) null,
		LastExecutionDate datetime2(7) null,

		StopBatchWhenError bit not null constraint DF_admin_all_BatchJob_StopBatchWhenError default(0),
		constraint PK_admin_all_BatchJob primary key (BatchJobID),
		constraint FK_admin_all_BatchJob_BatchJobType foreign key (Type) references admin_all.BatchJobType(BatchJobTypeID),
		constraint [CK_admin_all_BatchJob: Priority value should be between 0 and 10] check(Priority between 0 and 10),
		index IDX_admin_all_BatchJob_Name (Name),
		index IDX_admin_all_BatchJob_Type (Type),
		index IDX_admin_all_BatchJob_Status (Status)
	)
end
		
go
set xact_abort on
if not exists(select * from sys.columns where object_id = object_id('admin_all.BatchJob') and name  = 'Priority')
begin
	begin transaction
	begin try
		alter table admin_all.BatchJob add Priority tinyint not null constraint DF_admin_all_BatchJob_Priority default(5);
		exec('alter table admin_all.BatchJob add constraint [CK_admin_all_BatchJob: Priority value should be between 0 and 10] check(Priority between 0 and 10)')
	end try
	begin catch
		rollback;
		throw;
		return
	end catch
	commit
end