set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_admin_all_BatchJob on admin_all.BatchJob
for update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	; with x0 as
	(
		select 
				i.BatchJobID,
				case 
					when d.Status <> 'RUNNING' and i.Status = 'RUNNING' then 'Starting' -- Starting is always manual.
					when d.Status <> 'COMPLETED' and i.Status in('COMPLETED', 'CANCELLED', 'CANCELED') then 'ManualCompleting'
					when d.Status = 'RUNNING' and i.Status = 'RUNNING'
							and (
										i.TotalTasks = i.ProcessedTasks
									or	i.TotalTasks = i.ProcessedTasks + i.FailedTasks
									or	i.StopBatchWhenError = 1 and i.FailedTasks > 0
								) then 'AutoCompleting'
				end Status
		from inserted i
			inner join deleted d on i.BatchJobID = d.BatchJobID
	)

	update b
		set b.Status =	case
							when x0.Status = 'AutoCompleting' and b.FailedTasks = 0 then 'COMPLETED'
		                    else
		                        case
		                            when x0.Status = 'AutoCompleting' and b.FailedTasks > 0 then 'FAILED'
		                            else b.Status
		                        end
						end,
			b.StartDate = case
							when x0.Status = 'Starting' then sysdatetime()
							when b.StartDate is null and x0.Status in ('ManualCompleting') then sysdatetime()
							else b.StartDate
						end,
			b.EndDate = case
							when x0.Status in ('ManualCompleting', 'AutoCompleting') then sysdatetime()
							when x0.Status = 'Starting' then null
							else b.EndDate
						end

	from  x0
		inner loop join admin_all.BatchJob b on x0.BatchJobID = b.BatchJobID
	where x0.Status is not null
end
