set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_ReadLine(@Text nvarchar(max))
returns table
as
return 
(
		select row_number() over(order by ID) RowID, 
			case when left(value, 1) = char(0x0A) then stuff(value, 1, 1, '') else value end as Line
		from (
					select 1 as ID, value
					from string_split(@Text, char(0x0D)) v
			) a
)
go
