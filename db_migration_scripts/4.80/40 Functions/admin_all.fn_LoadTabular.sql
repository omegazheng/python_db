set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_LoadTabular(@Text nvarchar(max), @Separator varchar(1))
returns table
as
return
(
		select l.RowID, c.ColumnID, c.Value
		from admin_all.fn_ReadLine(@Text) l
			cross apply admin_all.fn_ParseLine(l.Line, @Separator) c
)
go
--select *
--from admin_all.fn_LoadTabular('a|2|2
--s|d|f|d', '|')