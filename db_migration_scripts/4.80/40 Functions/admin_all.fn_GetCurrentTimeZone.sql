set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetCurrentTimeZone()
returns char(7)
as 
begin
	return right(convert(varchar(121), SYSDATETIMEOFFSET ( ) ,121), 6)
end
go

--select admin_all.fn_GetCurrentTimeZone()