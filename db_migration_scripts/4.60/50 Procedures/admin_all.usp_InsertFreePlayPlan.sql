SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.usp_InsertFreePlayPlan') is null
	exec ('create procedure admin_all.usp_InsertFreePlayPlan as--')
GO

ALTER procedure [admin_all].[usp_InsertFreePlayPlan]
  (
    @CODE varchar(30),
    @NAME varchar(255),
    @STATUS varchar(30),
    @DESCRIPTION varchar(300),
    @BRAND_ID INT,
    @START_DATE datetime,
    @END_DATE datetime,
    @EXPIRY_DATE datetime,
    @PRODUCT_ID INT,
    @CHANNEL varchar(30),
    @BET_PER_ROUND numeric(38,18),
    @ROUNDS INT,
    @BONUS_PLAN_FREEPLAY_ID INT
  )
as
  begin
    DECLARE @ID bigint

    insert into admin_all.FREEPLAY_PLAN
    (
      CODE,
      NAME,
      STATUS,
      DESCRIPTION,
      BRAND_ID,
      START_DATE,
      END_DATE,
      EXPIRY_DATE,
      PRODUCT_ID,
      CHANNEL,
      BET_PER_ROUND,
      ROUNDS,
      BONUS_PLAN_FREEPLAY_ID
    )
    values
      (
        @CODE,
        @NAME,
        @STATUS,
        @DESCRIPTION,
        @BRAND_ID,
        @START_DATE,
        @END_DATE,
        @EXPIRY_DATE,
        @PRODUCT_ID,
        @CHANNEL,
        @BET_PER_ROUND,
        @ROUNDS,
        @BONUS_PLAN_FREEPLAY_ID
      )

    select @ID = @@IDENTITY
    select  id as id,
            code as code,
            name as name,
            status as status,
            description as description,
            brand_id as brandId,
            start_date as startDate,
            end_date as endDate,
            expiry_date as expiryDate,
            product_id as productId,
            channel as channel,
            isnull(bet_per_round,0) as betPerRound,
            rounds as rounds,
            bonus_plan_freeplay_id as bonusPlanFreeplayId
    from admin_all.FREEPLAY_PLAN where ID = @ID

  end
