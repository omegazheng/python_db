
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.usp_UpdateFreePlayPlan') is null
	exec ('create procedure admin_all.usp_UpdateFreePlayPlan as--')
GO
-- update FreePlayPlan
-- can not change: id, cide, name

ALTER procedure [admin_all].[usp_UpdateFreePlayPlan]
  (
    @ID INT,
    @STATUS varchar(30),
    @DESCRIPTION varchar(300),
    @START_DATE DATETIME,
    @END_DATE DATETIME,
    @EXPIRY_DATE DATETIME,
    @CHANNEL varchar(30),
    @BET_PER_ROUND numeric(38,18),
    @ROUNDS INT,
    @BONUS_PLAN_FREEPLAY_ID INT
  )
as
  BEGIN
    set nocount on
 --   begin transaction

      update admin_all.FREEPLAY_PLAN
      set STATUS = @STATUS, DESCRIPTION = @DESCRIPTION, START_DATE = @START_DATE, END_DATE = @END_DATE, EXPIRY_DATE = @EXPIRY_DATE,
          CHANNEL = @CHANNEL, BET_PER_ROUND = @BET_PER_ROUND, ROUNDS = @ROUNDS, BONUS_PLAN_FREEPLAY_ID = @BONUS_PLAN_FREEPLAY_ID
      where ID = @ID

 --   commit

  END
