
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
go
if object_id('maint.usp_TakeSnapshotActiveSessions') is null
begin
	exec('create procedure maint.usp_TakeSnapshotActiveSessions as --')
end
GO
alter procedure maint.usp_TakeSnapshotActiveSessions @SQL nvarchar(max) = null output
as
begin
	set nocount, xact_abort on
	declare @Date datetime, @rc int
	select @Date = getdate(),@SQL = null
	begin transaction
	exec sp_getapplock 'maint.usp_TakeSnapshotActiveSessions', 'Exclusive'
	insert into maint.ActiveSessions(SnapshotDate, SessionID, StartDate, BlockingSessionID, DurationInSecond, WaitType, WaitTime, WaitResource, LastWaitType, CPU, Reads, Writes, LogicalReads, TotalElapsedTime, SQLText, QueryPlan, GrantedMemory, NestedLevel, [RowCount], TransactionIsolationLevel, ExecutingManagedCode, Command, Status, DatabaseName, LoginName, HostName, ApplicationName, HostAddress)
		select  @Date SnapshotDate,
		 		SessionID, StartDate, BlockingSessionID, DurationInSecond, WaitType, WaitTime, WaitResource, LastWaitType, CPU, Reads, Writes, LogicalReads, TotalElapsedTime, SQLText, QueryPlan, GrantedMemory, NestedLevel, [RowCount], TransactionIsolationLevel, ExecutingManagedCode,
				Command, Status, DatabaseName, LoginName, HostName, ApplicationName, HostAddress
		from maint.v_ActiveSessions
	select @rc = @@rowcount
	delete maint.ActiveSessions where SnapshotDate <= dateadd(month, -3, @Date)
	commit
	waitfor delay '00:00:00.010'
	if @rc >0
		select @SQL= 'select * from maint.ActiveSessions where SnapshotDate = '+quotename(convert(varchar(max), @Date, 121), '''')
end
go







