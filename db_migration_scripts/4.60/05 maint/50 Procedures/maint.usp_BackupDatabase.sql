set ansi_nulls, quoted_identifier on
go
if object_id('maint.usp_BackupDatabase') is null
	exec('create procedure maint.usp_BackupDatabase as --')
go
alter procedure maint.usp_BackupDatabase
(
	@Path nvarchar(4000)= null,
	@DatabaseName nvarchar(128) = null
)
as
begin
	set nocount on
	select @DatabaseName = rtrim(isnull(@DatabaseName, db_name()))
	if db_id(@DatabaseName) is null
	begin
		raiserror('Could not find database %s.', 16, 1, @DatabaseName)
		return 
	end
	if @Path is null
	begin
		exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory', @Path output, 'no_output'
	end
	if maint.fn_IsLinux() = 1
		select @Path = case when right(@Path, 1) ='/' then @Path else @Path + '/' end
	else 
		select @Path = case when right(@Path, 1) ='\' then @Path else @Path + '\' end
	 
	select @Path = @Path + @DatabaseName +'_V'+ replace(cast(Version as nvarchar(20)), '.', '_')+ '_' + replace(replace(replace(convert(nvarchar(100), getdate(), 120), '-', ''), ':', ''), ' ', '_') + '.bak'
	from dbo.DatabaseVersion
	backup database @DatabaseName to disk = @Path with compression, init, stats=1
	print 'Backup file is ' + @Path
end
go

--xp_cmdshell N'del "F:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\Backup\omegasys_demo_V4_50_20190911_103252.bak"'
--exec maint.usp_BackupDatabase 


