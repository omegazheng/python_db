SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or alter procedure maint.usp_CreateDatabaseReplica
(
	@Identifier nvarchar(128) = 'Replica',
	@IsNewReplica bit = 0
)
as
begin
	set nocount, xact_abort  on
	declare @SQL nvarchar(max), @CurrentDatabase sysname = db_name(), @TargetDatabase sysname, @SourceConnectionID int, @SourceConnectionName varchar(128), @TargetConnectionID int, @TargetConnectionName varchar(128),
			@ConfigurationName varchar(128), @SchemaName sysname, @TableName sysname, @InterfaceName sysname, @TargetConnectionString nvarchar(max), @ProcessName sysname, @ServiceName sysname,
			@JobName sysname, @ConfigurationID int
	create table #t(Name varchar(128), TableName varchar(128), id int identity(1,1))
	if @TargetDatabase is null
		select @TargetDatabase = @CurrentDatabase + '_'+ @Identifier
	if db_id (@TargetDatabase) is null
	begin
		dbcc clonedatabase (@CurrentDatabase, @TargetDatabase)
		select @SQL = 'alter database ' + quotename(@TargetDatabase) + ' set read_write'
		exec(@SQL)
		select @SQL = 'delete ' + quotename(@TargetDatabase) + '.dbo.DatabaseVersion; insert into ' + quotename(@TargetDatabase) + '.dbo.DatabaseVersion	select * from dbo.DatabaseVersion'
		exec(@SQL)
		select @SQL = 'exec ' + quotename(@TargetDatabase) + '.maint.usp_RebuildIndexWihCompression'
		exec(@SQL)
		select @IsNewReplica = 1
	end

	select	@SourceConnectionName = @CurrentDatabase + '_'+@Identifier+'.Source',
			@TargetConnectionName = @CurrentDatabase + '_'+@Identifier+'.Target',
			@ProcessName = 'maint.usp_ReplicateDatabase_' + @Identifier,
			@ServiceName = 'Replicate Database ' + @Identifier
	select @JobName = @ServiceName +' - '+quotename(db_name())
	if exists(select * from msdb.dbo.sysjobs where name = @JobName)
	begin
		exec msdb.dbo.sp_delete_job @job_name = @JobName
	end

	select @SourceConnectionID = ConnectionID from maint.Connection where Name = @SourceConnectionName
	if @@rowcount = 0
	begin
		insert into maint.Connection([Name], [Comment], [ServerName], [DatabaseName], [UserName], [Password], [IsContextConnection],  [AutoUpdateVersion])
			values(@SourceConnectionName, 'Replication for ' + @CurrentDatabase, @@servername, @CurrentDatabase, null, null, 0,  1)
		select @SourceConnectionID = scope_identity()
	end
	select @TargetConnectionID = ConnectionID from maint.Connection where Name = @TargetConnectionName
	if @@rowcount = 0
	begin
		insert into maint.Connection([Name], [Comment], [ServerName], [DatabaseName], [UserName], [Password], [IsContextConnection],  [AutoUpdateVersion])
			values(@TargetConnectionName, 'Replication for ' + @CurrentDatabase, @@servername, @TargetDatabase, null, null, 0,  0)
		select @TargetConnectionID = scope_identity()
	end
	--select @SourceConnectionID, @TargetConnectionID
	select @TargetConnectionString = maint.fn_GetSQLConnectionStringByID(@TargetConnectionID)
	update maint.Connection 
		set CurrentRowVersion =  case when min_active_rowversion()< @@dbts  then min_active_rowversion() else @@dbts end,
			CurrentChangeTrackingVersion =  isnull(change_tracking_current_version (), 0)
	where ConnectionID = @SourceConnectionID
	declare c cursor static for
		select schema_name(t.schema_id), t.name
		from sys.tables t
			inner join sys.indexes i on t.object_id = i.object_id 
		where i.is_primary_key = 1
			and not (t.schema_id = schema_id('dbo') and t.name in ('DatabaseVersion', 'DatabaseUpdateLog', 'DatabaseUpdateBatch'))
			and not (t.schema_id = schema_id('admin_all') and t.name like 'QRTZ%')
			and not (t.schema_id = schema_id('admin_all') and t.name in( 'USER_SESSION', 'USER_WEB_SESSION', 'GAME_BONUS_BUCKET','ACCOUNT_TRAN_REALTIME', 'ALERT_LOG', 'AlertLog', 'DataFeedMissingRecordLog', 'STAFF_LOGIN_LOG', 'USER_LOGIN_LOG'))
			and not (t.schema_id = schema_id('maint') and t.name in( 'ActiveSessions', 'ReplicationHistory', 'ReplicationLastExecution'))
			and not (t.schema_id = schema_id('Logs'))
			
			--and t.name = 'ACCOUNT'
	open c
	fetch next from c into @SchemaName, @TableName
	while @@fetch_status = 0
	begin
		select @SQL = '	alter table ' + quotename(@SchemaName) + '.' + quotename(@TableName) + ' nocheck constraint all ;
						alter table ' + quotename(@SchemaName) + '.' + quotename(@TableName) + ' disable trigger all;'
		exec maint.usp_ExecuteSQL @TargetConnectionString, @SQL
		select @SQL = quotename(@SchemaName) + '.' + quotename(@TableName) 
		exec maint.usp_ReplicationCreateTarget @TargetConnectionName, @SQL

		select	@InterfaceName = maint.fn_GetInterfaceName(@SchemaName, @TableName),
				@ConfigurationName = @Identifier+'_' + @SchemaName+ '_'+ @TableName

		update maint.ReplicationConfiguration
			set SourceConnectionID = @SourceConnectionID,
				SourceTableOrQuery = quotename(@SchemaName) + '.' + quotename(@TableName),
				FilterType = 'ChangeTracking',
				TargetConnectionID = @TargetConnectionID,
				TargetSchemaName = quotename(parsename(@InterfaceName, 2)),
				TargetObjectName = quotename(parsename(@InterfaceName, 1)),
				IsActive = 1,
				@ConfigurationID = ConfigurationID
		where Name = @ConfigurationName
		if @@rowcount =0
		begin
			insert into maint.ReplicationConfiguration([Name], [Comment], [SourceConnectionID], [SourceTableOrQuery], [Columns], [FilterType], [TargetConnectionID], [TargetSchemaName], [TargetObjectName], [BatchSize], [IsActive])
				select @ConfigurationName [Name], null [Comment], @SourceConnectionID [SourceConnectionID], quotename(@SchemaName) + '.' + quotename(@TableName) [SourceTableOrQuery], null [Columns], 'ChangeTracking' [FilterType], @TargetConnectionID [TargetConnectionID], quotename(parsename(@InterfaceName, 2)) [TargetSchemaName], quotename(parsename(@InterfaceName, 1)) [TargetObjectName], 1500 [BatchSize], 1 [IsActive]
			select @ConfigurationID = scope_identity()
		end

		if @IsNewReplica = 1
			delete maint.ReplicationLastExecution where ConfigurationID = @ConfigurationID
		insert into #t values (@ConfigurationName, quotename(@SchemaName) + '.' + quotename(@TableName))
		fetch next from c into @SchemaName, @TableName
	end
	close c
	deallocate c
	
	select @SQL = 'create procedure ' + @ProcessName + ' as -- '
	if object_id(@ProcessName) is null
		exec(@SQL)
	select @SQL = 'alter procedure ' + @ProcessName + ' as 
begin
	set nocount on
	exec maint.usp_ConnectionExecute '+cast(@SourceConnectionID as varchar(20))+', ''exec maint.usp_ReplicationReadBrandFilterConfiguration;'''+'
	if maint.fn_ExecuteSQLScalarString(maint.fn_GetSQLConnectionStringByID('+cast(@SourceConnectionID as varchar(20))+'), ''select case when exists(select * from maint.ReplicationBrandFilterConfiguration) then ''''Yes'''' else ''''No'''' end'') = ''No''
	begin
		print ''No replication needed.''
		return
	end
	declare @NeedBrandFilter varchar(3)
	--select @NeedBrandFilter = maint.fn_ExecuteSQLScalarString(maint.fn_GetSQLConnectionStringByID('+cast(@SourceConnectionID as varchar(20))+'), ''select case when exists(select * from maint.ReplicationBrandFilterConfiguration where EnableCurrent = 0 or EnablePrevious = 0 and EnableCurrent = 1 )then ''''Yes'''' else ''''No'''' end'')
	select @NeedBrandFilter = maint.fn_ExecuteSQLScalarString(maint.fn_GetSQLConnectionStringByID('+cast(@SourceConnectionID as varchar(20))+'), ''select case when exists(select * from maint.ReplicationBrandFilterConfiguration where EnableCurrent = 1 ) then ''''Yes'''' else ''''No'''' end'')
	exec maint.usp_UpdateConnectionVersions
	
'+(select '	exec maint.usp_ReplicateOne ' + quotename(Name, '''')+';
' 
from #t 
where TableName not in ('[admin_all].[PAYMENT]', '[admin].[CASINO_BRAND_DEF]', '[admin_all].[ACCOUNT]', '[admin_all].[ACCOUNT_TRAN_ALL]', '[admin_all].[ACCOUNT_TRAN_REALTIME]','[external_mpt].[USER_CONF]')
order by TableName
for xml path(''), type).value('.', 'nvarchar(max)')+'
	if @NeedBrandFilter = ''No''
	begin
'+(select '		exec maint.usp_ReplicateOne ' + quotename(Name, '''')+';
' 
from #t 
where TableName in ('[admin_all].[PAYMENT]', '[admin].[CASINO_BRAND_DEF]', '[admin_all].[ACCOUNT]', '[admin_all].[ACCOUNT_TRAN_ALL]', '[admin_all].[ACCOUNT_TRAN_REALTIME]','[external_mpt].[USER_CONF]')
order by TableName
for xml path(''), type).value('.', 'nvarchar(max)')+'
	end
	else
	begin
		exec maint.usp_ConnectionExecute '+cast(@TargetConnectionID as varchar(20))+', ''exec maint.usp_ReplicationReadBrandFilterConfiguration;'''+'
'+(select case TableName 
			when '[admin_all].[PAYMENT]' then '		exec maint.usp_ReplicateOnePayment ' + quotename(Name, '''')
			when '[admin].[CASINO_BRAND_DEF]' then '		exec maint.usp_ReplicateOneUser ' + quotename(Name, '''')
			when '[admin_all].[ACCOUNT]' then '		exec maint.usp_ReplicateOneAccount ' + quotename(Name, '''')
			when '[admin_all].[ACCOUNT_TRAN_ALL]' then '		exec maint.usp_ReplicateOneAccountTran ' + quotename(Name, '''')
			when '[admin_all].[ACCOUNT_TRAN_REALTIME]' then '		exec maint.usp_ReplicateOneAccountTran ' + quotename(Name, '''')
			when '[external_mpt].[USER_CONF]' then '		exec maint.usp_ReplicateOneUser ' + quotename(Name, '''')
			else ' wrong settings '
		end+';
' 
from #t 
where TableName in ('[admin_all].[PAYMENT]', '[admin].[CASINO_BRAND_DEF]', '[admin_all].[ACCOUNT]', '[admin_all].[ACCOUNT_TRAN_ALL]', '[admin_all].[ACCOUNT_TRAN_REALTIME]','[external_mpt].[USER_CONF]')
for xml path(''), type).value('.', 'nvarchar(max)')+'
	end
	exec maint.usp_ReplicationRaiseError
	exec maint.usp_ConnectionExecute '+cast(@TargetConnectionID as varchar(20))+', ''exec maint.usp_ReplicationDataCleanupAtTarget 65;'''+'
end'
	exec(@SQL)
	exec maint.usp_CreateServiceJob @ServiceName, @ProcessName, null, 0, 600
	
end
