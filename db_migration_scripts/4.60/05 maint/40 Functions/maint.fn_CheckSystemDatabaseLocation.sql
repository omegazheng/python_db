set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseLocation()
returns varchar(max)
begin
	return (select top 1 'None of the database files should be on C: Drive' from sys.database_files where physical_name like 'C:\%')
end