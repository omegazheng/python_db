set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseDelayedDurability()
returns varchar(max)
begin
	return (
			select 'Set database delayed durability to FORCED is recommended' 
			from sys.databases 
			where name = db_name() 
				and delayed_durability_desc <> 'FORCED'
			)
end