set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemVersion()
returns varchar(max)
begin
	declare @Major int, @Minor int, @Build int, @Revision int
	select	@Major = parsename(version, 4),
			@Minor = parsename(version, 3),
			@Build = parsename(version, 2),
			@Revision = parsename(version, 1)
	from (select cast(serverproperty('ProductVersion') as sysname) as version) v

	if cast(ServerProperty('ProductMajorVersion') as int) < 13
	begin 
		return 'Server must be SQL Server 2016 SP2 and up'
	end
	else if cast(ServerProperty('ProductMajorVersion') as int) = 13
	begin
		if @Minor = 0
		begin
			if @Build < 5026
			begin
				return 'SP2 is required'
			end
		end
	end
	return null
end