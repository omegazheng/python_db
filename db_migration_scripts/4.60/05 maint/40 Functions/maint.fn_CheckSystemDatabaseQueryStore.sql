set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseQueryStore()
returns varchar(max)
begin
	return (case when not exists(select * from sys.database_query_store_options where desired_state_desc = 'READ_WRITE') then 'Query store should be enabled.' end)
end

