set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetErrorTraceName()
returns nvarchar(200)
as
begin
	return 'Omega error trace - ' + rtrim(db_name())
end
go
