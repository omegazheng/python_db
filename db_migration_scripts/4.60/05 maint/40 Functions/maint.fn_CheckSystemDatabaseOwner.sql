set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseOwner()
returns varchar(max)
begin
	return (select 'Database owner should be sa.' from sys.databases where name = db_name() and maint.fn_IsHostedDatabase() = 0 and owner_sid <> 0x01)
end
go


