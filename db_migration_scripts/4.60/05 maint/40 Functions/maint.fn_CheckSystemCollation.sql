set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemCollation()
returns varchar(max)
begin
	return (
				select 'Server collation ' + cast(ServerProperty('Collation') as sysname) + ' does not match database collation '+ collation_name + '.' 
				from sys.databases 
				where name = db_name() 
					and collation_name <> cast(ServerProperty('Collation') as sysname)
			)
	
end