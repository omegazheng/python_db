set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_ExtractErrorEvent(@x xml)
returns table
as
return (
select	
		d.value('@timestamp', 'datetime') EventDateUTC,
		d.value('(action[@name="event_sequence"]/value)[1]', 'bigint') EventSequence,
		d.value('(data[@name="error_number"]/value)[1]', 'int') ErrorNumber,
		d.value('(data[@name="severity"]/value)[1]', 'int') Severity,
		d.value('(data[@name="state"]/value)[1]', 'int') State,
		d.value('(data[@name="user_defined"]/value)[1]', 'bit') IsUserDefined,
		d.value('(data[@name="category"]/value)[1]', 'int') ErrorCategory,
		d.value('(data[@name="category"]/text)[1]', 'nvarchar(100)') ErrorCategoryText,

		d.value('(data[@name="destination"]/value)[1]', 'varchar(100)') ErrorDestination,
		d.value('(data[@name="destination"]/text)[1]', 'nvarchar(100)') ErrorDestinationText,

		d.value('(data[@name="is_intercepted"]/value)[1]', 'bit') IsIntercepted,

		d.value('(data[@name="message"]/value)[1]', 'nvarchar(max)') ErrorMessage,
		d.value('(action[@name="sql_text"]/value)[1]', 'nvarchar(max)') SQLText,
		d.value('(action[@name="session_server_principal_name"]/value)[1]', 'nvarchar(128)') SessionServerPrincipalname,
		d.value('(action[@name="server_instance_name"]/value)[1]', 'nvarchar(128)') SessionInstanceName,
		d.value('(action[@name="database_name"]/value)[1]', 'nvarchar(128)') DatabaseName,
		d.value('(action[@name="context_info"]/value)[1]', 'varchar(512)') ContextInfo,
		d.value('(action[@name="client_hostname"]/value)[1]', 'nvarchar(128)') HostName,
		d.value('(action[@name="client_app_name"]/value)[1]', 'nvarchar(128)') ApplicationName
from @x.nodes('RingBufferTarget/event') n(d)
)