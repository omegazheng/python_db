set ansi_nulls, quoted_identifier on
go
if object_id('maint.ErrorTrace') is null
begin
	create table maint.ErrorTrace
	(
		EventDateUTC datetime not null,
		EventSequence bigint not null,
		ErrorNumber int,
		Severity int,
		State int,
		IsUserDefined bit,
		ErrorCategory int,
		ErrorCategoryText nvarchar(100),
		ErrorDestination varchar(100),
		ErrorDestinationText nvarchar(100),
		IsIntercepted bit,
		ErrorMessage nvarchar(max),
		SQLText nvarchar(max),
		SessionServerPrincipalname nvarchar(128),
		SessionInstanceName nvarchar(128),
		DatabaseName nvarchar(128),
		ContextInfo varchar(512),
		HostName nvarchar(128),
		ApplicationName nvarchar(128),
		constraint PK_maint_ErrorTrace primary key (EventDateUTC, EventSequence) with(ignore_dup_key = on)
	) 
end
GO


