IF EXISTS (SELECT * FROM admin_all.auth_operation_api WHERE operation = 'LoyaltyPurchaseHistoryView'
                                                        and api = '/loyalty-purchase/history')
delete from admin_all.auth_operation_api WHERE
    operation = 'LoyaltyPurchaseHistoryView' and api = '/loyalty-purchase/history'

IF EXISTS (SELECT * FROM admin_all.auth_operation_api WHERE operation = 'BonusPlansListView'
                                                        and api = '/bonusplan/list')
    delete from admin_all.auth_operation_api WHERE
            operation = 'BonusPlansListView' and api = '/bonusplan/list'

GO
