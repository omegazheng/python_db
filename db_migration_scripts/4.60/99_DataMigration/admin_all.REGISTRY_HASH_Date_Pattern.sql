  if not exists(select *
	                from admin_all.REGISTRY_HASH
	                where MAP_KEY = 'date.pattern')
	    BEGIN
	      insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
	      VALUES ('date.pattern', 'dd-MM-yyyy HH:mm:ss');
	    END
