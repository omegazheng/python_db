set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetSemaphoreSlot @SemaphoreName nvarchar(200), @LockType nvarchar(50) = 'Shared', @Timeout int = 0, @SemaphoreSize int = 2, @SlotNumber int = null
as
begin
	set nocount on 
	if @@trancount = 0
		throw 50000, 'admin_all.usp_GetSemaphoreSlot must be running within a transaction.', 16
	declare @Resource nvarchar(220) , @i int = 0, @ret int, @OneExclusiveAllowed bit = 1
	if @SlotNumber is not null
	begin
		if @SlotNumber >= @SemaphoreSize or @SlotNumber < 0
			return -1
		select @i = @SlotNumber, @SemaphoreSize = @SlotNumber + 1
	end 
	exec sp_getapplock @Resource = @SemaphoreName, @LockMode = N'Shared', @LockOwner = 'Transaction', @LockTimeout = -1
	while @i < @SemaphoreSize
	begin
		select @Resource = cast(@i as nvarchar(20)) + N'-'+ @SemaphoreName
		exec @ret = sp_getapplock @Resource = @Resource, @LockMode = @LockType, @LockOwner = 'Transaction', @LockTimeout = @Timeout
		if @ret >= 0
			return @i
		select @i = @i + 1
	end
	return -1
end