set ansi_nulls, quoted_identifier on
set statistics xml off
go
if object_id('tempdb..#1') is not null
    drop table #1
go
begin transaction
go
if exists(select * from sys.tables where object_id = object_id('admin_all.AccountTranAggregateHourlyPendingCalculation'))
    begin
        select * into #1 from admin_all.AccountTranAggregateHourlyPendingCalculation
        drop table admin_all.AccountTranAggregateHourlyPendingCalculation
    end
go
if object_id('admin_all.AccountTranAggregateHourlyPendingCalculation0') is null
    begin
        create table admin_all.AccountTranAggregateHourlyPendingCalculation0
        (
            Datetime datetime not null,
            Slot int not null,
            constraint PK_admin_all_AccountTranAggregateHourlyPendingCalculation0 primary key (Slot, Datetime) with(ignore_dup_key = on),
            constraint CK_admin_all_AccountTranAggregateHourlyPendingCalculation0 check (Slot = 0)
        )
    end
go
if object_id('admin_all.AccountTranAggregateHourlyPendingCalculation1') is null
    begin
        create table admin_all.AccountTranAggregateHourlyPendingCalculation1
        (
            Datetime datetime not null,
            Slot int not null,
            constraint PK_admin_all_AccountTranAggregateHourlyPendingCalculation1 primary key (Slot, Datetime) with(ignore_dup_key = on),
            constraint CK_admin_all_AccountTranAggregateHourlyPendingCalculation1 check (Slot = 1)
        )
    end
go
create or alter view admin_all.AccountTranAggregateHourlyPendingCalculation
as
select * from admin_all.AccountTranAggregateHourlyPendingCalculation0
union all
select * from admin_all.AccountTranAggregateHourlyPendingCalculation1
go
--set statistics xml on
--insert into admin_all.AccountTranAggregateHourlyPendingCalculation values(getdate(), 1)
--select * from admin_all.AccountTranAggregateHourlyPendingCalculation where slot = 0
--set statistics xml off
go
if object_id('admin_all.AccountTranHourlyAggregateRecalculate') is not null
    begin
        insert into admin_all.AccountTranAggregateHourlyPendingCalculation(Datetime, Slot)
        select distinct Datetime , 0
        from admin_all.AccountTranHourlyAggregateRecalculate
        drop table admin_all.AccountTranHourlyAggregateRecalculate
    end
go
delete admin_all.REGISTRY_HASH where MAP_KEY in('AccountTranHourlyAggregate.LastExecutionTime', 'AccountTranHourlyAggregate.CurrentVersion', 'AccountTranHourlyAggregate.JobInterval(sec)')
go
commit
go

