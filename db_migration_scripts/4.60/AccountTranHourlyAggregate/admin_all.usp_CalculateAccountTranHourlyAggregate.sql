SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('admin_all.usp_CalculateAccountTranHourlyAggregate') is null
	exec('create procedure admin_all.usp_CalculateAccountTranHourlyAggregate as --')
go
alter procedure admin_all.usp_CalculateAccountTranHourlyAggregate
as
begin
	
	set nocount, xact_abort on
	
	begin try
		declare @BatchSize int = 5, @i int, @Datetime datetime, @Ret int, @RowFetched int = 5
		exec @Ret = sp_getapplock @Resource = N'admin_all.usp_CalculateAccountTranHourlyAggregate', @LockMode = N'Exclusive', @LockOwner = 'Session', @LockTimeout = 0
		if @Ret < 0
		begin 
			print 'admin_all.usp_CalculateAccountTranHourlyAggregate is running by other sessions'
			exec maint.usp_CreateErrorTraceEntry 'admin_all.usp_CalculateAccountTranHourlyAggregate is running by other sessions'
			return
		end
		select @i = 0
		while @i < 2
		begin
			select @RowFetched = @BatchSize
			while (@BatchSize = @RowFetched)
			begin 
				begin tran
				exec @ret = admin_all.usp_GetSemaphoreSlot @SemaphoreName =N'AccountTranHourlyAggregateCalculation', @LockType = 'Exclusive', @Timeout  = 300000, @SemaphoreSize  = 2, @SlotNumber  = @i  -- 5 minute timeout
				if @ret not in (0,1)
					throw 50000, 'Could not get exclusive access to the slot of AccountTranHourlyAggregateCalculation semaphore.', 16;
				declare c cursor local static for
					select top (@BatchSize) Datetime 
					from admin_all.AccountTranAggregateHourlyPendingCalculation
					where Slot = @Ret
					order by 1
				open c
				select @RowFetched = @@cursor_rows
				fetch next from c into @Datetime
				while @@fetch_status = 0
				begin
					exec admin_all.usp_CalculateAccountTranHourlyAggregateForOne @Datetime
					if @Ret = 0
						delete admin_all.AccountTranAggregateHourlyPendingCalculation0 where Datetime = @Datetime and Slot = @Ret
					else
						delete admin_all.AccountTranAggregateHourlyPendingCalculation1 where Datetime = @Datetime and Slot = @Ret
					fetch next from c into @Datetime
				end
				close c
				deallocate c
				commit
			end
			select @i = @i + 1
		end
		exec admin_all.usp_CalculateAccountTranAggregate
	end try
	begin catch
		if @@trancount > 0
			rollback;
		declare @Error nvarchar(max) = error_message();
		exec maint.usp_CreateErrorTraceEntry @Error;
		exec sp_releaseapplock @Resource = N'admin_all.usp_CalculateAccountTranHourlyAggregate', @LockOwner = 'Session';
		throw;
	end catch
	exec sp_releaseapplock @Resource = N'admin_all.usp_CalculateAccountTranHourlyAggregate', @LockOwner = 'Session'
end
go
--exec admin_all.usp_CalculateAccountTranHourlyAggregate


--select convert(datetime, admin_all.fn_GetRegistry('AccountTranHourlyAggregate.LastExecutionTime'), 120), getdate()