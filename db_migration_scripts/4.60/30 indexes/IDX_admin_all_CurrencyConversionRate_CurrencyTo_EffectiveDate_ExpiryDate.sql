if exists(select * from sys.indexes i where i.name = 'IDX_admin_all_CurrencyConversionRate_CurrencyTo' and i.object_id = object_id('admin_all.CurrencyConversionRate'))
begin
	drop index admin_all.CurrencyConversionRate.IDX_admin_all_CurrencyConversionRate_CurrencyTo
end
if not exists(select * from sys.indexes i where i.name = 'IDX_admin_all_CurrencyConversionRate_CurrencyTo_EffectiveDate_ExpiryDate' and i.object_id = object_id('admin_all.CurrencyConversionRate'))
begin
	create index IDX_admin_all_CurrencyConversionRate_CurrencyTo_EffectiveDate_ExpiryDate on admin_all.CurrencyConversionRate(CurrencyTo, EffectiveDate, ExpiryDate) include(Rate, CurrencyFrom)
end
--drop index admin_all.CurrencyConversionRate.IDX_admin_all_CurrencyConversionRate_CurrencyTo_EffectiveDate_ExpiryDate