if not exists(select * from sys.indexes where object_id = object_id('admin_all.BONUS_PLAN') and name ='IDX_admin_all_BONUS_PLAN_STATUS_END_DATE')
begin
	create index IDX_admin_all_BONUS_PLAN_STATUS_END_DATE  on admin_all.BONUS_PLAN(STATUS, END_DATE)
end
