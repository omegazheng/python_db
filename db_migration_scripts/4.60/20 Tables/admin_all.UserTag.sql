set quoted_identifier, ansi_nulls on
if object_id('admin_all.UserTag') is null
begin
	create table admin_all.UserTag
	(
		Code varchar(10) not null,
		PartyID int not null,
		Name varchar(255) not null,
		constraint PK_admin_all_UserTag primary key(Code, PartyID),
		constraint FK_admin_all_UserTag_external_mpt_USER_CONF foreign key (PartyID) references external_mpt.USER_CONF(PARTYID),
		index IDX_admin_all_UserTag (PartyID, Code)
	)
end
