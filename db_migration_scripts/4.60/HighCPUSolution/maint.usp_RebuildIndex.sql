set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_RebuildIndex
as
begin 
	set nocount, xact_abort on
	set lock_timeout 5000
	if exists(
				select case when max(record_count) > 0 then max(version_ghost_record_count)/ max(record_count) else 0 end, max(version_ghost_record_count), max(record_count)
				from sys.dm_db_index_physical_stats(db_id(), object_id('admin_all.ACCOUNT_TRAN_REALTIME'),1, 0, 'detailed' )
				having case when max(record_count) > 0 then max(version_ghost_record_count)/ max(record_count) else 0 end > 15
					and max(record_count) > 100
			)
	begin
		alter index all on admin_all.ACCOUNT_TRAN_REALTIME rebuild
	end
	if exists(
				select case when max(record_count) > 0 then max(version_ghost_record_count)/ max(record_count) else 0 end, max(version_ghost_record_count), max(record_count)
				from sys.dm_db_index_physical_stats(db_id(), object_id('admin_all.USER_WEB_SESSION'),1, 0, 'detailed' )
				having case when max(record_count) > 0 then max(version_ghost_record_count)/ max(record_count) else 0 end > 5
					and max(record_count) > 100
			)
	begin
		alter index all on admin_all.USER_WEB_SESSION rebuild
	end
	if exists(
				select case when max(record_count) > 0 then max(version_ghost_record_count)/ max(record_count) else 0 end, max(version_ghost_record_count), max(record_count)
				from sys.dm_db_index_physical_stats(db_id(), object_id('admin_all.USER_PLATFORM'),1, 0, 'detailed' )
				having case when max(record_count) > 0 then max(version_ghost_record_count)/ max(record_count) else 0 end > 10
					and max(record_count) > 100
			)
	begin
		alter index all on admin_all.USER_PLATFORM rebuild
	end
end
go
exec maint.usp_CreateServiceJob 'RebuildIndex', 'maint.usp_RebuildIndex', 'Rebuild index for small tables', 0, 1800