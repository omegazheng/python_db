SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_ConfigureKYC6
AS

BEGIN
if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.apiKey')
  insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.apiKey', 'TBD');

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.URL')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.URL', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.referThreshold')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.referThreshold', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.failedThreshold')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.failedThreshold', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.PEP')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.PEP', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.previousSanctions')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.previousSanctions', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.currentSanctions')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.currentSanctions', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.lawEnforcement')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.lawEnforcement', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.financialRegulator')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.financialRegulator', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.insolvency')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.insolvency', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.disqualifiedDirector')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.disqualifiedDirector', 'TBD')

if not exists (select * from admin_all.REGISTRY_HASH where map_key = 'kyc6.personSearch.adverseMedia')
insert into admin_all.REGISTRY_HASH(MAP_KEY, VALUE) values ('kyc6.personSearch.adverseMedia', 'TBD')

END
