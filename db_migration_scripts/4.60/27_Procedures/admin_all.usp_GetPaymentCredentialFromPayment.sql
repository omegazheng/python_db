SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [admin_all].[usp_GetPaymentCredentialFromPayment]
  (@accountId int, @startDate DATETIME, @endDate DATETIME)
  AS
  BEGIN
    set nocount on

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    select pc.*
    FROM [admin_all].[PaymentCredential] pc WITH (NOLOCK)
           INNER JOIN (
      SELECT distinct pcp.PaymentCredentialID
      FROM [admin_all].[PaymentCredentialPayment] pcp WITH (NOLOCK)
             INNER JOIN [admin_all].[payment] p WITH (NOLOCK) ON pcp.PaymentID = p.id
             INNER JOIN [admin_all].[account] a WITH (NOLOCK) ON p.account_id = a.id
      WHERE p.request_date >= @startDateLocal
        AND p.request_date < @endDateLocal
        AND a.id = @accountId
    ) xx on xx.PaymentCredentialID = pc.ID
  END
GO

--set statistics io, time on
--exec admin_all.[usp_GetPaymentCredentialFromPayment] @accountId = 41352, @startDate = '2018-01-01', @endDate = '2018-01-07'
--set statistics io, time off

