
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.usp_InsertAccountTran') is null
	exec ('create procedure admin_all.usp_InsertAccountTran as--')
GO

ALTER procedure [admin_all].[usp_InsertAccountTran]
	(
		@ACCOUNT_ID int,
		@DATETIME datetime,
		@TRAN_TYPE varchar(10),
		@AMOUNT_REAL numeric(38,18),
		@BALANCE_REAL numeric(38,18),
		@PLATFORM_TRAN_ID nvarchar(100) = null,
		@GAME_TRAN_ID nvarchar(100) = null,
		@GAME_ID varchar(100) = null,
		@PLATFORM_ID int = null,
		@PAYMENT_ID int = null,
		@ROLLED_BACK int = 0,
		@ROLLBACK_TRAN_ID bigint = null,
		@AMOUNT_RELEASED_BONUS numeric(38,18) = 0,
		@AMOUNT_PLAYABLE_BONUS numeric(38,18) = 0,
		@BALANCE_RELEASED_BONUS numeric(38,18) = 0,
		@BALANCE_PLAYABLE_BONUS numeric(38,18) = 0,
		@UNDERFLOW_REAL numeric(38,18) = 0,
		@UNDERFLOW_RELEASED_BONUS numeric(38,18) = 0,
		@UNDERFLOW_PLAYABLE_BONUS numeric(38,18) = 0,
		@AMOUNT_RAW_LOYALTY bigint = 0,
		@BALANCE_RAW_LOYALTY bigint = 0,
		@TRANSACTION_ON_HOLD_ID bigint = null,
		@SSW_TRAN_ID bigint = null,
		@REFERENCE varchar(100) = null,
		@BRAND_ID int = null
	)
as
begin
	set nocount, xact_abort on
	DECLARE @PartyId int, @ID bigint
	if isnull(@BRAND_ID,0) = 0
	begin
		select @PartyID = (select PARTYID from admin_all.ACCOUNT where ID = @ACCOUNT_ID)
		select @BRAND_ID = [admin_all].[fn_GetBrandIdFromPartyId] (@PartyID)
	end
	begin transaction
	insert into admin_all.ACCOUNT_TRAN
	(
		ACCOUNT_ID,
		DATETIME,
		TRAN_TYPE,
		AMOUNT_REAL,
		BALANCE_REAL,
		PLATFORM_TRAN_ID,
		GAME_TRAN_ID,
		GAME_ID,
		PLATFORM_ID,
		PAYMENT_ID,
		ROLLED_BACK,
		ROLLBACK_TRAN_ID,
		AMOUNT_RELEASED_BONUS,
		AMOUNT_PLAYABLE_BONUS,
		BALANCE_RELEASED_BONUS,
		BALANCE_PLAYABLE_BONUS,
		AMOUNT_RAW_LOYALTY,
		BALANCE_RAW_LOYALTY,
		TRANSACTION_ON_HOLD_ID,
		SSW_TRAN_ID,
		REFERENCE,
		BRAND_ID
	)
	values
		(
			@ACCOUNT_ID,
			@DATETIME,
			@TRAN_TYPE,
			@AMOUNT_REAL,
			@BALANCE_REAL,
			@PLATFORM_TRAN_ID,
			@GAME_TRAN_ID ,
			@GAME_ID ,
			@PLATFORM_ID ,
			@PAYMENT_ID ,
			@ROLLED_BACK ,
			@ROLLBACK_TRAN_ID ,
			@AMOUNT_RELEASED_BONUS ,
			@AMOUNT_PLAYABLE_BONUS ,
			@BALANCE_RELEASED_BONUS,
			@BALANCE_PLAYABLE_BONUS ,
			@AMOUNT_RAW_LOYALTY,
			@BALANCE_RAW_LOYALTY,
			@TRANSACTION_ON_HOLD_ID ,
			@SSW_TRAN_ID ,
			@REFERENCE ,
			@BRAND_ID
		)
	select @ID = cast(session_context(N'AccountTranID') as bigint)
	-- We only insert when overall underflow is NOT zero. Most of transaction should not have underflow
	if @UNDERFLOW_REAL + @UNDERFLOW_RELEASED_BONUS + @UNDERFLOW_PLAYABLE_BONUS <> 0
	begin
		insert into admin_all.ACCOUNT_TRAN_UNDERFLOW(ID, ACCOUNT_ID, DATETIME, AMOUNT_REAL, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS)
			select @ID, @ACCOUNT_ID, @DATETIME, @UNDERFLOW_REAL, @UNDERFLOW_RELEASED_BONUS, @UNDERFLOW_PLAYABLE_BONUS
	end
	commit 
		select * from admin_all.ACCOUNT_TRAN where ID = @ID
end
