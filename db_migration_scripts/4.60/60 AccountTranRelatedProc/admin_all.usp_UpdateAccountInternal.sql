SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('[admin_all].[usp_UpdateAccountInternal]') is null
	exec('create procedure [admin_all].[usp_UpdateAccountInternal] as --')
go
ALTER procedure [admin_all].[usp_UpdateAccountInternal]
(
	@PartyID			int,
	@AmountReal			numeric(38,18),
	@ReleasedBonus		numeric(38,18) = null,
	@PlayableBonus		numeric(38,18) = null,
	@AmountSecondary	numeric(38,18) = null,
	@TranType			varchar(10),
	@PlatformID			int = null,
	@PlatformTranID		nvarchar(100) = null,
	@GameTranID			nvarchar(100) = null,
	@GameID				varchar(100) = null,
	@PaymentID			int = null,
	@AmountRawLoyalty	bigint = 0,
	@Reference			varchar(100) = null,
	@Cit				numeric(38,18) = 0,
	@DateTime			datetime2 = null,
	@UpdateBonuses		bit = 0,
	@MachineID			int = null,
	@AccountTranID		bigint = null output
)
as
begin
	set nocount on
	set xact_abort on
	declare @AccountReleasedBonus numeric(38,18), @AcountPlaybleBonus numeric(38,18),
			@AccountSecondary numeric(38,18), @AccountRawLoyalty bigint,
			@BonusReleasedBonus numeric(38,18), @BonusPlayableBonus numeric(38,18), @BonusID int,
			@BonusReleasedBonusWinning numeric(38,18), @BonusPlayableBonusWinning numeric(38,18),
			@IsChanged bit, @BalanceReal numeric(38,18), @UserType int, @Currency nchar(3),
			@BrandID int
	begin transaction
	select	@ReleasedBonus = isnull(@ReleasedBonus, 0),
			@PlayableBonus = isnull(@PlayableBonus, 0),
			@AmountReal = isnull(@AmountReal, 0),
			@Cit = isnull(@Cit, 0),
			@AmountSecondary = isnull(@AmountSecondary, 0)
	select @UserType = user_type, @Currency = CURRENCY from external_mpt.USER_CONF where PARTYID = @PartyID
	update admin_all.ACCOUNT
		set @BalanceReal = BALANCE_REAL        = BALANCE_REAL + @AmountReal,
			@AccountReleasedBonus = RELEASED_BONUS = RELEASED_BONUS + @ReleasedBonus,
			@AcountPlaybleBonus   = PLAYABLE_BONUS = PLAYABLE_BONUS + @PlayableBonus,
			@Cit                  = CIT = CIT + @Cit,
			@AccountSecondary     = SECONDARY_BALANCE = SECONDARY_BALANCE + @AmountSecondary,
			@AccountRawLoyalty    = RAW_LOYALTY_POINTS = RAW_LOYALTY_POINTS + @AmountRawLoyalty
	where PARTYID = @PartyID
	select @BrandID = BRANDID
	from external_mpt.USER_CONF
	where PARTYID = @PartyID
	if exists(
				select *
				from admin_all.ACCOUNT_TRAN 
				where BRAND_ID = @BrandID
					and PLATFORM_TRAN_ID = @PlatformTranID
					and PLATFORM_ID = @PlatformID
					and TRAN_TYPE = @TranType
					and AMOUNT_REAL = @AmountReal + @AmountSecondary
					and AMOUNT_RELEASED_BONUS = @ReleasedBonus
					and AMOUNT_PLAYABLE_BONUS = @PlayableBonus
			)
	begin
		raiserror('Duplicated transaction is found. Transaction is discarded.', 16, 1)
		rollback
		return
	end
	if isnull(@UserType, 0) not in (1, 2, 20)
	begin
		if @BalanceReal <0
			throw 50000, 'Insufficient fund', 1; 
		if @AccountReleasedBonus <0
			throw 50000, 'Insufficient released bonus', 1; 
		if @AcountPlaybleBonus <0
			throw 50000, 'Insufficient playable bonus', 1; 
	end
	if (@DateTime is null)
		set @DateTime = getdate()
	insert into admin_all.ACCOUNT_TRAN (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID, GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS, AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY, REFERENCE, BRAND_ID, MachineID )
		select
				a.ID,
				@DateTime                           DATETIME,
				@TranType                           TRAN_TYPE,
				(@AmountReal + @AmountSecondary) as AMOUNT_REAL,
				a.BALANCE_REAL,
				@PlatformTranID                     PLATFORM_TRAN_ID,
				@GameTranID                         GAME_TRAN_ID,
				@GameID                             GAME_ID,
				@PlatformID                         PLATFORM_ID,
				@PaymentID                          payment_id,
				0                                   ROLLED_BACK,
				null                                ROLLBACK_TRAN_ID,
				@ReleasedBonus                      AMOUNT_RELEASED_BONUS,
				@PlayableBonus                      AMOUNT_PLAYABLE_BONUS,
				a.RELEASED_BONUS                    BALANCE_RELEASED_BONUS,
				a.PLAYABLE_BONUS                    BALANCE_PLAYABLE_BONUS,
				@AmountRawLoyalty                   AMOUNT_RAW_LOYALTY,
				a.RAW_LOYALTY_POINTS                BALANCE_RAW_LOYALTY,
				@Reference                          REFERENCE,
				u.BRANDID                           BRAND_ID,
				@MachineID						  MachineID
		from admin_all.ACCOUNT a
			inner join external_mpt.USER_CONF u on a.PARTYID = u.PARTYID
		where u.PARTYID = @PartyID;

	select @AccountTranID = cast(session_context(N'AccountTranID') as bigint)
	if admin_all.fn_IsMultiCurrencyEnabled() = 1
	begin
		exec admin_all.usp_InsertAccountTranCurrencyExtension @AccountTranID = @AccountTranID,	@PartyID = @PartyID, @Datetime = @DateTime, @CurrencyFrom = @Currency, @AmountReal = @AmountReal, @ReleasedBonus = @ReleasedBonus, @PlayableBonus = @PlayableBonus
	end
	exec admin_all.usp_CheckUserLimit @PartyID = @PartyID, @TransactionType = @TranType, @Datetime = @DateTime, @AmountReal = @AmountReal
	if (@UpdateBonuses > 0) and (@ReleasedBonus <> 0 or @PlayableBonus <> 0)
	begin
		if (@ReleasedBonus <> 0)
        begin
			declare cBonus cursor local static for
				select ID, RELEASED_BONUS, RELEASED_BONUS_WINNINGS
				from admin_all.BONUS
				where PARTYID = @PartyID
				order by STATUS desc, TRIGGER_DATE
			open cBonus
			fetch next from cBonus into @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning
			while @@fetch_status = 0
			begin
				select @IsChanged = 0
				-- Add Fund to Bonus
				if @ReleasedBonus > 0
				begin
					select @BonusReleasedBonus = @BonusReleasedBonus + @ReleasedBonus
					select @ReleasedBonus = 0, @IsChanged = 1
                end
				--Take fund from Bonus
				--First working on Released Winning
				if @ReleasedBonus < 0
				begin
					if @BonusReleasedBonusWinning > 0
					begin
						if @BonusReleasedBonusWinning >= abs(@ReleasedBonus)
						begin
							select @BonusReleasedBonusWinning = @BonusReleasedBonusWinning + @ReleasedBonus
							select @ReleasedBonus = 0, @IsChanged = 1
						end
						else
						begin
							select @ReleasedBonus = @BonusReleasedBonusWinning + @ReleasedBonus, @IsChanged = 1
							select @BonusReleasedBonusWinning = 0
                        end
					end
                end
				--Then working on Released Bonus
				if @ReleasedBonus < 0
				begin
					if @BonusReleasedBonus > 0
					begin
						if @BonusReleasedBonus >= abs(@ReleasedBonus)
						begin
							select @BonusReleasedBonus = @BonusReleasedBonus + @ReleasedBonus
							select @ReleasedBonus = 0, @IsChanged = 1
                        end
						else
						begin
							select @ReleasedBonus = @BonusReleasedBonus + @ReleasedBonus, @IsChanged = 1
							select @BonusReleasedBonus = 0
                        end
					end
				end
				if @IsChanged = 1
				begin
					update admin_all.BONUS
						set RELEASED_BONUS = @BonusReleasedBonus, RELEASED_BONUS_WINNINGS = @BonusReleasedBonusWinning
					where ID = @BonusID
                  --print @BonusID
                end
				if @ReleasedBonus = 0
					break;
				fetch next from cBonus into @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning
			end
			close cBonus
			deallocate cBonus
			--print @PartyID
			if @ReleasedBonus <> 0
			begin
				raiserror ('Insufficient released bonus.', 16, 1)
				rollback
				return
			end
		end

		if (@PlayableBonus <> 0)
		begin
			--if @TranType not in ('MACHIN_EFT', 'SYSTEM_EFT')
			--begin
				declare cBonus cursor local static for
				select b.ID, b.RELEASED_BONUS, b.RELEASED_BONUS_WINNINGS,
					case when b.status in ('ACTIVE', 'QUEUED', 'SPENT', 'SPENT_ACTIVE', 'QUALIFIED')
					then b.PLAYABLE_BONUS
					else 0 end,
					case when b.status in ('ACTIVE', 'QUEUED', 'SPENT', 'SPENT_ACTIVE', 'QUALIFIED')
					then b.PLAYABLE_BONUS_WINNINGS
					else 0 end
				from admin_all.BONUS b
					--inner join admin_all.BONUS_PLAN bp on bp.ID =b.BONUS_PLAN_ID
				where b.PARTYID = @PartyID
					and b.STATUS  in ('ACTIVE', 'QUEUED', 'SPENT', 'SPENT_ACTIVE', 'QUALIFIED')
				order by b.status, b.TRIGGER_DATE
			--end
			--else
			--begin
			--	declare cBonus cursor local static for
			--	select b.ID, b.RELEASED_BONUS, b.RELEASED_BONUS_WINNINGS,
			--		case when b.status in ('ACTIVE', 'QUEUED', 'SPENT', 'SPENT_ACTIVE', 'QUALIFIED')
			--		then b.PLAYABLE_BONUS
			--		else 0 end,
			--		case when b.status in ('ACTIVE', 'QUEUED', 'SPENT', 'SPENT_ACTIVE', 'QUALIFIED')
			--		then b.PLAYABLE_BONUS_WINNINGS
			--		else 0 end
			--	from admin_all.BONUS b
			--		inner join admin_all.BONUS_PLAN bp on bp.ID =b.BONUS_PLAN_ID
			--	where b.PARTYID = @PartyID
			--		and b.STATUS  in ('ACTIVE', 'QUEUED', 'SPENT', 'SPENT_ACTIVE', 'QUALIFIED')
			--		--and bp.BONUS_CODE = 'BONUS_PLAN_EFT'
			--	order by b.status, b.TRIGGER_DATE
			--end
			open cBonus
			if  @@cursor_rows = 0 and @TranType in ('SYSTEM_EFT')
			begin
				insert into admin_all.BONUS (PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN, BONUS_INCREMENTAL_ID, EXTERNAL_BONUS_ID, RELEASED_BONUS, PLAYABLE_BONUS)
					select	@PartyID PARTYID, bp.ID BONUS_PLAN_ID, GETDATE() TRIGGER_DATE, bp.END_DATE EXPIRY_DATE, 'ACTIVE' STATUS,
							0 AMOUNT_WAGERED, 0 WAGER_REQUIREMENT, @AmountReal AMOUNT, null PAYMENT_ID, null RELEASE_DATE, @ReleasedBonus AMOUNT_WITHDRAWN,
							null BONUS_INCREMENTAL_ID, null EXTERNAL_BONUS_ID, @ReleasedBonus RELEASED_BONUS, @PlayableBonus PLAYABLE_BONUS
					from admin_all.BONUS_PLAN bp
					where (bp.BONUS_CODE='BONUS_PLAN_EFT' AND bp.TRIGGER_TYPE ='PLTFRM_BON')
				select @PlayableBonus = 0
			end
			fetch next from cBonus into @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning, @BonusPlayableBonus, @BonusPlayableBonusWinning
			while @@fetch_status = 0
			begin
				select @IsChanged = 0
				if @PlayableBonus > 0
				begin
					select @BonusPlayableBonus = @BonusPlayableBonus + @PlayableBonus
					--print 'Adding Playable bonus, amount=' + convert(varchar, cast(@BonusPlayableBonus as money))
					select @PlayableBonus = 0, @IsChanged = 1
				end
				-- working on Playable Winning
				if @PlayableBonus < 0
				begin
					if @BonusPlayableBonusWinning > 0
					begin
						if @BonusPlayableBonusWinning >= -@PlayableBonus
						begin
							select @BonusPlayableBonusWinning = @BonusPlayableBonusWinning + @PlayableBonus
							select @PlayableBonus = 0, @IsChanged = 1
						end
						else
						begin
							select @PlayableBonus = @BonusPlayableBonusWinning + @PlayableBonus, @IsChanged = 1
							select @BonusPlayableBonus = 0
						end
					end
				end
				--             Then working on Playable Bonus
				if @PlayableBonus < 0
				begin
					if @BonusPlayableBonus > 0
					begin
						if @BonusPlayableBonus >= -@PlayableBonus
						begin
							select @BonusPlayableBonus = @BonusPlayableBonus + @PlayableBonus
							select @PlayableBonus = 0, @IsChanged = 1
						end
						else
						begin
							select @PlayableBonus = @BonusPlayableBonus + @PlayableBonus, @IsChanged = 1
							select @BonusPlayableBonus = 0
						end
					end
				end
				if @IsChanged = 1
				begin
					--print 'new playable bonus value is '
					--print @BonusPlayableBonus
					update admin_all.BONUS
						set PLAYABLE_BONUS        = isnull(@BonusPlayableBonus, PLAYABLE_BONUS),
							RELEASED_BONUS          = @BonusReleasedBonus,
							PLAYABLE_BONUS_WINNINGS = isnull(@BonusPlayableBonusWinning, PLAYABLE_BONUS_WINNINGS),
							RELEASED_BONUS_WINNINGS = @BonusReleasedBonusWinning
					where ID = @BonusID
					--print @BonusID
				end
				if @PlayableBonus = 0
					break;
				fetch next from cBonus into @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning, @BonusPlayableBonus, @BonusPlayableBonusWinning
			end
			close cBonus
			deallocate cBonus
			if @PlayableBonus <> 0
			begin
				--throw 50000, 'Insufficient playable bonus.', 1;
				raiserror ('Insufficient playable bonus.', 16, 1)
				rollback
				return
            end
		end
	end
	commit
end
