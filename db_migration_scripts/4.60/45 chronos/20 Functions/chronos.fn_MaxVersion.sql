set ansi_nulls, quoted_identifier on
go
if object_id('chronos.fn_MaxVersion') is null
begin
	exec('create function chronos.fn_MaxVersion() returns int as begin return 1; end')
end
go
alter function chronos.fn_MaxVersion(@v1 binary(8), @v2 binary(8))
returns binary(8)
as
begin
	return case when isnull(@v1, 0x00) >= isnull(@v2, 0x00) then isnull(@v1, 0x00) else isnull(@v2, 0x00) end
end