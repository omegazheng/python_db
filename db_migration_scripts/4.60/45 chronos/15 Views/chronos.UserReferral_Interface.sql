SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
if object_id('[chronos].[UserReferral_Interface]') is null
	exec('create view [chronos].[UserReferral_Interface] as select 1 as one')
go
alter view [chronos].[UserReferral_Interface] 
as 
	select cast([UserReferralID] as int) as [UserReferralID],cast([PartyID] as int) as [PartyID],cast([ReferredPartyID] as int) as [ReferredPartyID],  ___IsDeleted___ 
	from [chronos].[UserReferral]
GO

--sp_help 'admin_all.user_referral'