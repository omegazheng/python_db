
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
if object_id('chronos.VIPStatus_Interface') is null
exec ('create view chronos.VIPStatus_Interface as select 1 as one')
go
alter view [chronos].[VIPStatus_Interface] as select cast([VIPStatusID] as int) as [VIPStatusID],cast([Code] as varchar(50)) as [Code],  ___IsDeleted___ from [chronos].[VIPStatus]
GO


