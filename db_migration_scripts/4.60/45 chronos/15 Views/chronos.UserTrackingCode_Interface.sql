SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
if object_id('[chronos].[UserTrackingCode_Interface]') is null
	exec('create view [chronos].[UserTrackingCode_Interface] as select 1 as one')
go
alter view [chronos].[UserTrackingCode_Interface] 
as 
	select 
		cast([UserTrackingCodeID] as int) as [UserTrackingCodeID],
		cast([PartyID] as int) as [PartyID],
		cast(null as varchar(20)) as CodeKey,
		cast(null as varchar(255)) as [Value],
		___IsDeleted___ 
		from  [chronos].[UserTrackingCode]
GO

--sp_help 'admin_all.user_tracking_code'

--select * from admin_all.user_tracking_code