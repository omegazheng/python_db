set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_GetUserTopProduct') is null
	exec('create procedure chronos.usp_GetUserTopProduct as --')
go
alter procedure chronos.usp_GetUserTopProduct @ConsumerID uniqueidentifier
as
begin
	set nocount on 
	set xact_abort on
	set lock_timeout 0
	if @@trancount = 0
		throw 50000, 'chronos.usp_GetUserTopProduct must be run within a transaction', 16;
	declare @FromVersion binary(8) = 0, @CurrentVersion binary(8) = min_active_rowversion()
	update a
		set 
				@FromVersion = FromVersion,
				ToVersion = @CurrentVersion
	from chronos.SynchronizationStatus a (rowlock)
	where Name = 'UserTopProduct'
		and ConsumerID = @ConsumerID
	if @@rowcount = 0
	begin
		insert into chronos.SynchronizationStatus(ConsumerID, Name, FromVersion, ToVersion)
			select @ConsumerID, 'UserTopProduct', @FromVersion, @CurrentVersion
	end
	if @FromVersion = 0
	begin
		select	 current_transaction_id() TransactionID, *
		from Chronos.UserTopProduct p
			
	end
	else
	begin
		select	 current_transaction_id() TransactionID, *
		from Chronos.UserTopProduct p
		where ___RowVersion___ >= @FromVersion
	end
end
go
--begin transaction
--exec chronos.usp_GetUserTopProduct
--rollback

