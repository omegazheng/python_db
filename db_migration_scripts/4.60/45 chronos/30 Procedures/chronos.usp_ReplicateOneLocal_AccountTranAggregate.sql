
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
if object_id('chronos.usp_ReplicateOneLocal_AccountTranAggregate') is null
	exec('create procedure chronos.usp_ReplicateOneLocal_AccountTranAggregate as--')
go
ALTER procedure chronos.usp_ReplicateOneLocal_AccountTranAggregate 
as
begin
	set nocount on
	declare @FriendlyName varchar(128) = 'Chronos-Dummy-AccountTranAggregate'
	declare @ConfigurationID int = maint.fn_GetReplicationConfigurationID(@FriendlyName)
	declare @Date datetime = getdate()
	exec maint.usp_SetReplicateOneContextInfo
	begin try
		declare @SourceConnectionID int, @SourceTableOrQuery varchar(max), @Columns varchar(max),
				@FilterType varchar(100), @TargetConnectionID int, @TargetSchemaName varchar(128),
				@TargetObjectName varchar(128), @BatchSize int, @IsActive bit, 
				@SourceConnectionString varchar(max), @FullTargetTableName nvarchar(256)
		select
				@SourceConnectionID = SourceConnectionID, @SourceTableOrQuery = SourceTableOrQuery, @Columns = Columns,
				@FilterType = FilterType, @TargetConnectionID = TargetConnectionID, @TargetSchemaName = TargetSchemaName,
				@TargetObjectName = TargetObjectName, @BatchSize = BatchSize, @IsActive = IsActive,
				@SourceConnectionString = maint.fn_GetSQLConnectionStringByID(SourceConnectionID),
				@FullTargetTableName = TargetSchemaName + '.' + TargetObjectName
		from maint.ReplicationConfiguration
		where ConfigurationID = @ConfigurationID
		if @@rowcount = 0 or @IsActive = 0
		begin
			print 'Could not find replication configuration'
			goto ___End___
		end
		if @FilterType <> 'ChangeTracking'
		begin
			print 'Only ChangeTracking is supported'
		end
		declare @FromValueQuery varchar(max), @FromValueQueryEvaluationType varchar(20), @ToValueQuery varchar(max), 
				@ToValueQueryEvaluationType varchar(20), @ToValueAdjustment varchar(max), @FromValueAdjustment varchar(max),
				@CurrentChangeTrackingVersion bigint, @CurrentRowVersion binary(8)
		select 
				@FromValueQuery = FromValueQuery, @FromValueQueryEvaluationType = FromValueQueryEvaluationType, @ToValueQuery = ToValueQuery, 
				@ToValueQueryEvaluationType = ToValueQueryEvaluationType, @ToValueAdjustment = ToValueAdjustment, @FromValueAdjustment = FromValueAdjustment
		from maint.FilterType
		where FilterType = @FilterType
		if @@rowcount = 0
		begin
			print 'Cound not find Filter Type.'
			goto ___End___
		end
		
		select	@CurrentChangeTrackingVersion = change_tracking_current_version()
		declare @FromValue sql_variant, @ToValue sql_variant
		select @FromValue = FromValue, @ToValue = ToValue
		from maint.ReplicationLastExecution
		where ConfigurationID = @ConfigurationID

		exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @StartDate = @Date

		declare @SQL nvarchar(max), @Rows int = 0, @Error varchar(max)

		--if @FilterType = 'Snapshot'
		--begin
		--	select @SQL = @SourceTableOrQuery
		--	exec @Rows = maint.usp_ConnectionBulkCopy @SourceConnectionID = @SourceConnectionID, @CommandText = @SQL, @TargetConnectionID = @TargetConnectionID, @TargetTable = @FullTargetTableName, @BatchSize = @BatchSize
		--	goto ___End___
		--end
		
		if @FilterType = 'ChangeTracking'
		begin
			if not exists(select * from sys.change_tracking_databases where database_id = db_id())
			begin
				select @SQL = 'alter database '+db_name()+' set change_tracking = on'
				exec(@SQL)
			end
			if not exists(select * from sys.change_tracking_tables where object_id = object_id(@SourceTableOrQuery))
			begin
				select @SQL = 'alter table '+@SourceTableOrQuery+' enable change_tracking'
				exec(@SQL)
			end
			select * into #SourceDefinition from maint.fn_GetLocalTableSchema(@SourceTableOrQuery)
			alter table #SourceDefinition add TargetColumn varchar(128)
			delete #SourceDefinition where ColumnName in ('___IsDeleted___')
			if @Columns is not null
			begin
				if charindex('<',@Columns) = 0
				begin
					select * from #SourceDefinition where PrimaryKeyOrder > 0 or ColumnName in (select Value from maint.SplitDelimitedString(@Columns, ','))
				end
				else
				begin
					declare @xColumn xml = cast(@Columns as xml);
					update d
						set d.TargetColumn = m.Target
					from #SourceDefinition d
						inner join (
									select col.value('@Source', 'varchar(128)') as Source, col.value('@Target', 'varchar(128)') Target
									from @xColumn.nodes('/Mapping/Column') n(col)
									) m on m.Source = d.ColumnName
					delete #SourceDefinition where TargetColumn is null
				end
			end;
	
		
			select @FromValue = @ToValue
			--select @FromValue = cast(@ToValue as bigint) - 1
			if @FromValue < 0
				select @FromValue = 0
			select @ToValue = change_tracking_current_version()
			
			if @FromValue is null
			begin
				select @SQL = '
								insert into '+@TargetSchemaName+'.'+@TargetObjectName+'(___IsDeleted___'+(select ','+quotename(TargetColumn) from #SourceDefinition order by ColumnID for xml path(''), type).value('.', 'varchar(max)')+')
								select 0 as ___IsDeleted___'+(select ','+quotename(ColumnName) from #SourceDefinition order by ColumnID for xml path(''), type).value('.', 'varchar(max)')+' 
								from ' + @SourceTableOrQuery+'
								where Period = ''Y''
								select @Rows= @@rowcount'
				select @FromValue = 0
				
			end
			else
			begin
			
			
			select @SQL = '	declare @v bigint = cast(@Version as bigint)
								insert into '+@TargetSchemaName+'.'+@TargetObjectName+'(___IsDeleted___'+(select ','+quotename(TargetColumn) from #SourceDefinition order by ColumnID for xml path(''), type).value('.', 'varchar(max)')+')
									select case when c.SYS_CHANGE_OPERATION = ''D'' then 1 else 0 end ___IsDeleted___'
									+(select ','+case when PrimaryKeyOrder>0 then 'c.'+quotename(ColumnName) else 'b.'+quotename(ColumnName) end+' as ' + quotename(ColumnName)  from #SourceDefinition order by ColumnID for xml path(''), type).value('.', 'varchar(max)')+'
									from changetable(changes '+@SourceTableOrQuery+', @v) c 
										left join '+@SourceTableOrQuery+' b on '+stuff((select ' and c.'+quotename(ColumnName)+' = b.' + quotename(ColumnName) from #SourceDefinition where PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'varchar(max)'), 1, 5, '')+'
									where c.SYS_CHANGE_CONTEXT is null 
										and c.Period = ''Y''
								select @Rows = @@rowcount'
			end
			--print @SQL
			exec sp_executesql @SQL, N'@Version sql_variant, @Rows int output', @FromValue, @Rows output
			exec maint.usp_SetReplicationHistory @ConfigurationID =@ConfigurationID, @FromValue= @FromValue, @ToValue = @ToValue
			goto ___End___
		end
	

___End___:
	end try
	begin catch
		select @Error = error_message()
		exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @Error = @Error
	end catch
	select @Date = getdate()
	exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @EndDate = @Date, @Rows = @Rows
	exec maint.usp_ClearReplicateOneContextInfo
	return @ConfigurationID
end
GO
