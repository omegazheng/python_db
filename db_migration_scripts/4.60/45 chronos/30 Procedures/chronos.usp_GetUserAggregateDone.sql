set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_GetUserAggregateDone') is null
	exec('create procedure chronos.usp_GetUserAggregateDone as --')
go
alter procedure chronos.usp_GetUserAggregateDone @ConsumerID uniqueidentifier, @TransactionID bigint
as
begin
	set nocount on 
	set xact_abort on
	set lock_timeout 0
	if current_transaction_id() <> isnull(@TransactionID, 0)
		throw 50000, 'chronos.usp_GetUserAggregateDone must be run within the same transaction', 16;
	update a
		set FromVersion = ToVersion 
	from chronos.SynchronizationStatus a (rowlock)
	where Name = 'UserAggregate'
		and ConsumerID = @ConsumerID
end
go
--select * from chronos.SynchronizationStatus
--begin transaction
--declare @TransactionID bigint
--select @TransactionID = current_transaction_id()
--exec chronos.usp_GetUserAggregate '00000000-0000-0000-0000-000000000001'
--exec chronos.usp_GetUserAggregateDone'00000000-0000-0000-0000-000000000001', @TransactionID
--select * from chronos.SynchronizationStatus
--commit
--rollback

--update Chronos.Party set ___IsDeleted___ = 0 where PartyID = 10000
--select * from Chronos.Party where PartyID = 10000
--select * from chronos.SynchronizationStatus
