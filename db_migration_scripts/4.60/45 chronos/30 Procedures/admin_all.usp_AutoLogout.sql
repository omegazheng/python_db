SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('admin_all.usp_AutoLogout') is null
	exec('create procedure admin_all.usp_AutoLogout as')
go
alter procedure admin_all.usp_AutoLogout
as
begin
	set nocount on
	set xact_abort on
	begin transaction

	declare @WebSessioniTimeout int, @GamePlayTimeout int --in minutes
	exec @WebSessioniTimeout = sp_getapplock 'admin_all.usp_AutoLogout','Exclusive','Transaction', 0
	if @WebSessioniTimeout < 0
		goto ___Exit___;

	select @WebSessioniTimeout = isnull(cast((select value from admin_all.REGISTRY_HASH where MAP_KEY = 'psWebSessionTimeout') as int), 30)
	select @GamePlayTimeout = isnull(cast((select value from admin_all.REGISTRY_HASH where MAP_KEY = 'psGamePlayTimeout') as int), 5)

	select s.PARTYID, s.SESSION_KEY, IP, SessionType
		into #1
	from admin_all.USER_WEB_SESSION s
	where s.LAST_ACCESS_TIME< dateadd(minute, -@WebSessioniTimeout, getdate())
		and s.SessionType = 'ONLINE'
		and not exists(
			select * 
			from admin_all.ACCOUNT_TRAN at
				inner join admin_all.ACCOUNT a on at.ACCOUNT_ID = a.id
			where a.PARTYID = s.PARTYID
				and at.DATETIME > dateadd(minute, -@GamePlayTimeout, getdate())

		)
	if @@rowcount = 0
		goto ___Exit___;
	--create unique clustered index idx0 on #1(SESSION_KEY)
	--create index idx1 on #1(PARTYID)
	
	delete admin_all.USER_WEB_SESSION where SESSION_KEY in (select x.SESSION_KEY from #1 x)
	delete admin_all.USER_PLATFORM where PARTYID in (select x.PARTYID from #1 x)
	insert into admin_all.USER_LOGIN_LOG(partyid, login_time, LOGIN_TYPE, EXECUTED_BY, SESSION_KEY, ip)
		select
			distinct PARTYID, getdate(), 'TIMEOUT' LOGIN_TYPE, 'SYSTEM' EXECUTED_BY, SESSION_KEY, IP
		from #1
___Exit___:
	commit
end
go
--exec admin_all.usp_AutoLogout
--select * from admin_all.USER_WEB_SESSION 
--select * from admin_all.USER_LOGIN_LOG where LOGIN_TYPE = 'TIMEOUT'
--sp_help 'admin_all.USER_LOGIN_LOG'
--sp_helpindex 'admin_all.USER_WEB_SESSION'