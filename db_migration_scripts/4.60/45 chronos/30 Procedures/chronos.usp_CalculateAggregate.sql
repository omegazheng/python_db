set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_CalculateAggregate') is null
	exec('create procedure chronos.usp_CalculateAggregate as --')
go
alter procedure chronos.usp_CalculateAggregate 
as
begin
	
	set nocount on 
	set xact_abort on
	set lock_timeout 0
	declare @ConsumerID uniqueidentifier = 0x00
	begin transaction
	declare @FromVersion binary(8) = 0, @CurrentVersion binary(8) = min_active_rowversion(), @Date date = datefromparts(year(getdate()),1,1)
	update a
		set 
				@FromVersion = FromVersion,
				ToVersion = @CurrentVersion
	from chronos.SynchronizationStatus a (rowlock)
	where Name = 'usp_CalculateAggregate'
		and ConsumerID = @ConsumerID
	if @@rowcount = 0
	begin
		insert into chronos.SynchronizationStatus(ConsumerID, Name, FromVersion, ToVersion)
			select @ConsumerID, 'usp_CalculateAggregate', @FromVersion, @CurrentVersion
	end
	if @FromVersion = 0
	begin
		;with x0 as
		(
			select PartyID, 
				ProductID,
				GameID, 
				Sum(AmountReal) Handle
			from Chronos.AccountTranAggregate a
			where a.TranType in ('GAME_BET', 'GAME_WIN')
				and ___IsDeleted___ = 0
				and ProductID <>-1
				and GameID <> '-UNKNOWN-'
			group by PartyID, ProductID, GameID
		),
		x1 as
		(
			select	PartyID,
					ProductID,
					GameID,
					Handle,
					row_number() over(partition by PartyID order by Handle desc) Rank
			from x0
		),
		x2 as (
				select x1.*, p.CODE ProductCode, gi.NAME GameName
				from x1 
					inner join admin_all.PLATFORM p on p.id = x1.ProductID 
					inner join admin_all.GAME_INFO gi on gi.GAME_ID = x1.GameID and gi.PLATFORM_ID = x1.ProductID
				where Rank <=3
			)
		merge chronos.UserTopProduct t
		using x2 as s on t.Rank = s.Rank and t.PartyID = s.PartyID
		when not matched then
			insert(PartyID, Rank, ProductCode, GameID, GameName, Handle)
				values(s.PartyID, s.Rank, s.ProductCode, s.GameID, s.GameName, Handle)
		when matched and (s.ProductCode <> t.ProductCode or s.GameName <> t.GameName or s.GameID <> t.GameID or s.Handle <> t.Handle) then
			update set t.ProductCode = s.ProductCode, t.GameName = s.GameName, t.GameID = s.GameID, t.Handle = s.Handle
		when not matched by source then
			delete
		;
		insert into chronos.UserAggregate_Interface(PartyID, Currency,LTDDepositAmountReal,LTDDepositAmountReleasedBonus,LTDDepositAmountPlayableBonus,LTDWithdrawAmountReal,LTDWithdrawAmountReleasedBonus,LTDWithdrawAmountPlayableBonus,LTDTransferOutAmountReal,LTDTransferOutAmountReleasedBonus,LTDTransferOutAmountPlayableBonus,YTDWithdrawAmountReal,YTDWithdrawAmountReleasedBonus,YTDWithdrawAmountPlayableBonus,YTDDepositAmountReal,YTDDepositAmountReleasedBonus,YTDDepositAmountPlayableBonus,YTDTransferOutAmountReal,YTDTransferOutAmountReleasedBonus,YTDTransferOutAmountPlayableBonus,___IsDeleted___)
			select PartyID, Currency,
					sum(case when ___IsDeleted___ = 0 and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReal else 0 end) LTDDepositAmountReal,
					sum(case when ___IsDeleted___ = 0 and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReleasedBonus else 0 end) LTDDepositAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountPlayableBonus else 0 end) LTDDepositAmountPlayableBonus,
					
					sum(case when ___IsDeleted___ = 0 and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReal else 0 end) LTDWithdrawAmountReal,
					sum(case when ___IsDeleted___ = 0 and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReleasedBonus else 0 end) LTDWithdrawAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountPlayableBonus else 0 end) LTDWithdrawAmountPlayableBonus,
					
					sum(case when ___IsDeleted___ = 0 and TranType in ('TRANSF_OUT') then AmountReal else 0 end) LTDTransferOutAmountReal,
					sum(case when ___IsDeleted___ = 0 and TranType in ('TRANSF_OUT') then AmountReleasedBonus else 0 end) LTDTransferOutAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and TranType in ('TRANSF_OUT') then AmountPlayableBonus else 0 end) LTDTransferOutAmountPlayableBonus,

					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReal else 0 end) YTDWithdrawAmountReal,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReleasedBonus else 0 end) YTDWithdrawAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountPlayableBonus else 0 end) YTDWithdrawAmountPlayableBonus,

					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReal else 0 end) YTDDepositAmountReal,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReleasedBonus else 0 end) YTDDepositAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountPlayableBonus else 0 end) YTDDepositAmountPlayableBonus,
					
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('TRANSF_OUT') then AmountReal else 0 end) YTDTransferOutAmountReal,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('TRANSF_OUT') then AmountReal else 0 end) YTDTransferOutAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('TRANSF_OUT') then AmountReal else 0 end) YTDTransferOutAmountPlayableBonus,
					sum(case when ___IsDeleted___  = 0 then 1 else -1 end) ___IsDeleted___
			from Chronos.AccountTranAggregate
			where TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL',
								'DEPOSIT', 'DP_RB', 'DP_RBACK',
								'TRANSF_OUT'
								)
				and Period = 'Y'
				and AggregateType = 0
			group by PartyID, Currency
	end
	else 
	begin
		create table #ChangedParty(PartyID int primary key)
			select distinct PartyID
			from Chronos.AccountTranAggregate
			where ___RowVersion___ >= @FromVersion


		;with x0 as
		(
			select PartyID, 
				ProductID,
				GameID, 
				Sum(AmountReal) Handle
			from Chronos.AccountTranAggregate a
			where a.TranType in ('GAME_BET', 'GAME_WIN')
				and ___IsDeleted___ = 0
				and ProductID <>-1
				and GameID <> '-UNKNOWN-'
				and exists(select * from #ChangedParty b where a.PartyID = b.PartyID)
			group by PartyID, ProductID, GameID
		),
		x1 as
		(
			select	PartyID,
					ProductID,
					GameID,
					Handle,
					row_number() over(partition by PartyID order by Handle desc) Rank
			from x0
		),
		x2 as (
				select x1.*, p.CODE ProductCode, gi.NAME GameName
				from x1 
					inner join admin_all.PLATFORM p on p.id = x1.ProductID 
					inner join admin_all.GAME_INFO gi on gi.GAME_ID = x1.GameID and gi.PLATFORM_ID = x1.ProductID
				where Rank <=3
			)
		merge chronos.UserTopProduct t
		using x2 as s on t.Rank = s.Rank and t.PartyID = s.PartyID
		when not matched then
			insert(PartyID, Rank, ProductCode, GameID, GameName, Handle)
				values(s.PartyID, s.Rank, s.ProductCode, s.GameID, s.GameName, Handle)
		when matched and (s.ProductCode <> t.ProductCode or s.GameName <> t.GameName or s.GameID <> t.GameID or s.Handle <> t.Handle) then
			update set t.ProductCode = s.ProductCode, t.GameName = s.GameName, t.GameID = s.GameID, t.Handle = s.Handle
		when not matched by source then
			delete
		;
		insert into chronos.UserAggregate_Interface(PartyID, Currency,LTDDepositAmountReal,LTDDepositAmountReleasedBonus,LTDDepositAmountPlayableBonus,LTDWithdrawAmountReal,LTDWithdrawAmountReleasedBonus,LTDWithdrawAmountPlayableBonus,LTDTransferOutAmountReal,LTDTransferOutAmountReleasedBonus,LTDTransferOutAmountPlayableBonus,YTDWithdrawAmountReal,YTDWithdrawAmountReleasedBonus,YTDWithdrawAmountPlayableBonus,YTDDepositAmountReal,YTDDepositAmountReleasedBonus,YTDDepositAmountPlayableBonus,YTDTransferOutAmountReal,YTDTransferOutAmountReleasedBonus,YTDTransferOutAmountPlayableBonus,___IsDeleted___)
			select PartyID, Currency,
					sum(case when ___IsDeleted___ = 0 and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReal else 0 end) LTDDepositAmountReal,
					sum(case when ___IsDeleted___ = 0 and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReleasedBonus else 0 end) LTDDepositAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountPlayableBonus else 0 end) LTDDepositAmountPlayableBonus,
					
					sum(case when ___IsDeleted___ = 0 and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReal else 0 end) LTDWithdrawAmountReal,
					sum(case when ___IsDeleted___ = 0 and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReleasedBonus else 0 end) LTDWithdrawAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountPlayableBonus else 0 end) LTDWithdrawAmountPlayableBonus,
					
					sum(case when ___IsDeleted___ = 0 and TranType in ('TRANSF_OUT') then AmountReal else 0 end) LTDTransferOutAmountReal,
					sum(case when ___IsDeleted___ = 0 and TranType in ('TRANSF_OUT') then AmountReleasedBonus else 0 end) LTDTransferOutAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and TranType in ('TRANSF_OUT') then AmountPlayableBonus else 0 end) LTDTransferOutAmountPlayableBonus,

					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReal else 0 end) YTDWithdrawAmountReal,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountReleasedBonus else 0 end) YTDWithdrawAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL') then AmountPlayableBonus else 0 end) YTDWithdrawAmountPlayableBonus,

					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReal else 0 end) YTDDepositAmountReal,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountReleasedBonus else 0 end) YTDDepositAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('DEPOSIT', 'DP_RB', 'DP_RBACK') then AmountPlayableBonus else 0 end) YTDDepositAmountPlayableBonus,
					
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('TRANSF_OUT') then AmountReal else 0 end) YTDTransferOutAmountReal,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('TRANSF_OUT') then AmountReal else 0 end) YTDTransferOutAmountReleasedBonus,
					sum(case when ___IsDeleted___ = 0 and Date = @Date and TranType in ('TRANSF_OUT') then AmountReal else 0 end) YTDTransferOutAmountPlayableBonus,
					sum(case when ___IsDeleted___  = 0 then 1 else -1 end) ___IsDeleted___
			from Chronos.AccountTranAggregate a
			where TranType in ('WD_CANCEL', 'WD_RB', 'WD_RBACK', 'WD_REJECT', 'WITHDRAW', 'WITHDRAWAL',
								'DEPOSIT', 'DP_RB', 'DP_RBACK',
								'TRANSF_OUT'
								)
				and Period = 'Y'
				and AggregateType = 0
				and exists(select * from #ChangedParty b where a.PartyID = b.PartyID)
			group by PartyID, Currency
	end
	commit
end

--select distinct TranType  from Chronos.AccountTranAggregate order by 1
--select * from chronos.UserAggregate
--select * from chronos.SynchronizationStatus
--select * from chronos.UserTopProduct