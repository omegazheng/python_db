set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_GetEventDone') is null
	exec('create procedure chronos.usp_GetEventDone as --')
go
alter procedure chronos.usp_GetEventDone @ConsumerID uniqueidentifier, @TransactionID bigint, @EventCategoryID int = -1
as
begin
	set nocount on 
	set xact_abort on
	set lock_timeout 0
	if current_transaction_id() <> isnull(@TransactionID, 0)
		throw 50000, 'chronos.usp_GetEventDone must be run within the same transaction', 16;
	update a
		set FromVersion = ToVersion 
	from chronos.SynchronizationStatus a (rowlock)
	where Name = 'Event'
		and ConsumerID = @ConsumerID
		and EventCategoryID = @EventCategoryID
end
go
--select * from chronos.SynchronizationStatus
--begin transaction
--declare @TransactionID bigint
--select @TransactionID = current_transaction_id()
--exec chronos.usp_GetEvent '00000000-0000-0000-0000-000000000001'
--exec chronos.usp_GetEventDone'00000000-0000-0000-0000-000000000001', @TransactionID
--select * from chronos.SynchronizationStatus
--commit
--rollback


