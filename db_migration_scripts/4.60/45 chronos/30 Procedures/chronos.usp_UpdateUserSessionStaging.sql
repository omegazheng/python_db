set ansi_nulls, quoted_identifier on
if object_id('chronos.usp_UpdateUserSessionStaging') is null
begin
	exec('create procedure chronos.usp_UpdateUserSessionStaging as --')
end
go
alter procedure chronos.usp_UpdateUserSessionStaging
(
	@Multiplier numeric(38, 18),
	@PartyID int,
	@Name varchar(50),
	@Value numeric(38, 18),
	@Date datetime = null,
	@Value1 numeric(38, 18) = null,
	@Value2 numeric(38, 18) = null,
	@Value3 numeric(38, 18) = null
)
as
begin
	set nocount on
	if isnull(@Value,0) = 0
		return
	select @Date = isnull(@Date, getdate()), @Value = (isnull(@Value, 0) + isnull(@Value1, 0) + isnull(@Value2, 0) + isnull(@Value3, 0))* @Multiplier
	update t with(rowlock)
		set t.Value = t.Value + @Value , t.Date = @Date
	from chronos.UserSessionStaging t 
	where t.PartyID = @PartyID 
		and t.Name = @Name
	if @@rowcount = 0
		insert into chronos.UserSessionStaging(PartyID, Name, Value, Date)
			values(@PartyID, @Name, @Value, @Date)
	;
end