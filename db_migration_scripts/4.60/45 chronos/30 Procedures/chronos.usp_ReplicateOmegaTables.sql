set ansi_nulls, quoted_identifier on
if object_id('chronos.usp_ReplicateOmegaTables') is null
	exec('create procedure chronos.usp_ReplicateOmegaTables as --')
go
alter procedure chronos.usp_ReplicateOmegaTables
as
begin
	if chronos.fn_IsEnabled() = 0
		return
	exec maint.usp_ReplicateOneLocal 'Chronos-Dummy-Party'
	exec maint.usp_ReplicateOneLocal 'Chronos-Dummy-VIPStatus'
	exec maint.usp_ReplicateOneLocal 'Chronos-Dummy-UserTrackingCode'
	exec maint.usp_ReplicateOneLocal 'Chronos-Dummy-UserReferral'
	exec maint.usp_ReplicateOneLocal 'Chronos-Dummy-Staff'
	exec maint.usp_ReplicateOneLocal 'Chronos-Dummy-UserTag'
	exec chronos.usp_ReplicateOneLocal_AccountTranAggregate
	exec chronos.usp_CalculateAggregate 
end
go


--select count(*) from admin_all.AccountTranAggregate where Period = 'Y'
--select count(*) from admin_all.AccountTranhourlyAggregate

--select count(*) from chronos.AccountTranAggregate where Period = 'Y'


--select top 10 * from maint.ReplicationLastExecution order by 1 desc