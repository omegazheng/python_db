set ansi_nulls, quoted_identifier on
go
if object_id('[chronos].[TRI_AccountTranAggregate_Interface]') is null
	exec('create trigger [chronos].[TRI_AccountTranAggregate_Interface] on [chronos].[AccountTranAggregate_Interface]
instead of insert
as
begin
	return
end')
go 
ALTER trigger [chronos].[TRI_AccountTranAggregate_Interface] on [chronos].[AccountTranAggregate_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [chronos].[AccountTranAggregate] t
	using (select * from inserted where Period = 'Y')s on s.[Period] = t.[Period] and s.[Date] = t.[Date] and s.[AggregateType] = t.[AggregateType] and s.[TranType] = t.[TranType] and s.[PartyID] = t.[PartyID] and s.[ProductID] = t.[ProductID] and s.[GameID] = t.[GameID]
	when not matched then
		insert ([Period],[AggregateType],[Date],[BrandID],[PartyID],[Currency],[TranType],[ProductID],[GameID],[AmountReal],[AmountReleasedBonus],[AmountPlayableBonus],[GameCount],[TranCount], ___IsDeleted___)
			values (s.[Period],s.[AggregateType],s.[Date],s.[BrandID],s.[PartyID],s.[Currency],s.[TranType],s.[ProductID],s.[GameID],s.[AmountReal],s.[AmountReleasedBonus],s.[AmountPlayableBonus],s.[GameCount],s.[TranCount], s.___IsDeleted___)
	when matched and (isnull(s.___IsDeleted___, 0) <> isnull(t.___IsDeleted___, 0) or s.[BrandID] is null and t.[BrandID] is not null or s.[BrandID] is not null and t.[BrandID] is null or s.[BrandID] <> t.[BrandID] or s.[Currency] is null and t.[Currency] is not null or s.[Currency] is not null and t.[Currency] is null or s.[Currency] <> t.[Currency] or s.[AmountReal] is null and t.[AmountReal] is not null or s.[AmountReal] is not null and t.[AmountReal] is null or s.[AmountReal] <> t.[AmountReal] or s.[AmountReleasedBonus] is null and t.[AmountReleasedBonus] is not null or s.[AmountReleasedBonus] is not null and t.[AmountReleasedBonus] is null or s.[AmountReleasedBonus] <> t.[AmountReleasedBonus] or s.[AmountPlayableBonus] is null and t.[AmountPlayableBonus] is not null or s.[AmountPlayableBonus] is not null and t.[AmountPlayableBonus] is null or s.[AmountPlayableBonus] <> t.[AmountPlayableBonus] or s.[GameCount] is null and t.[GameCount] is not null or s.[GameCount] is not null and t.[GameCount] is null or s.[GameCount] <> t.[GameCount] or s.[TranCount] is null and t.[TranCount] is not null or s.[TranCount] is not null and t.[TranCount] is null or s.[TranCount] <> t.[TranCount]) then
		update set t.[BrandID] = case isnull(s.___IsDeleted___, 0) when 0 then s.[BrandID] else t.[BrandID] end,
					t.[Currency] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Currency] else t.[Currency] end,
					t.[AmountReal] = case isnull(s.___IsDeleted___, 0) when 0 then s.[AmountReal] else t.[AmountReal] end,
					t.[AmountReleasedBonus] = case isnull(s.___IsDeleted___, 0) when 0 then s.[AmountReleasedBonus] else t.[AmountReleasedBonus] end,
					t.[AmountPlayableBonus] = case isnull(s.___IsDeleted___, 0) when 0 then s.[AmountPlayableBonus] else t.[AmountPlayableBonus] end,
					t.[GameCount] = case isnull(s.___IsDeleted___, 0) when 0 then s.[GameCount] else t.[GameCount] end,
					t.[TranCount] = case isnull(s.___IsDeleted___, 0) when 0 then s.[TranCount] else t.[TranCount] end,
					t.[___IsDeleted___] = isnull(s.___IsDeleted___, 0);

	;
	
end

