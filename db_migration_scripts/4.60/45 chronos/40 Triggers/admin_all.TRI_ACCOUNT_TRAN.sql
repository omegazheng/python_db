SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.TRI_ACCOUNT_TRAN') is null
begin
	exec('create trigger [admin_all].[TRI_ACCOUNT_TRAN] on [admin_all].[ACCOUNT_TRAN] 
instead of insert, update, delete
as 
return
')
end
GO
ALTER trigger [admin_all].[TRI_ACCOUNT_TRAN] on [admin_all].[ACCOUNT_TRAN] 
instead of insert, update, delete
as 
begin 
	if @@rowcount = 0
		return
	set nocount on
	declare @ID bigint
	if not exists(select * from inserted) -- delete
	begin
		delete t from admin_all.ACCOUNT_TRAN_REALTIME t inner join deleted s on s.ID = t.ID
		delete t from admin_all.ACCOUNT_TRAN_ALL t inner join deleted s on s.ID = t.ID
	end
	if not exists(select * from deleted) 
	begin
		if exists(
					select * 
					from admin_all.ACCOUNT_TRAN a with(forceseek)
						inner join inserted i on a.ACCOUNT_ID = i.ACCOUNT_ID and a.TRAN_TYPE = i.TRAN_TYPE and a.PLATFORM_TRAN_ID = i.PLATFORM_TRAN_ID and a.PLATFORM_ID = i.PLATFORM_ID
				)
		begin
			raiserror('Duplicated transaction found', 16, 1)
			return
		end
		insert into admin_all.ACCOUNT_TRAN_REALTIME([ACCOUNT_ID],[DATETIME],[TRAN_TYPE],[AMOUNT_REAL],[BALANCE_REAL],[PLATFORM_TRAN_ID],[GAME_TRAN_ID],[GAME_ID],[PLATFORM_ID],[payment_id],[ROLLED_BACK],[ROLLBACK_TRAN_ID],[AMOUNT_RELEASED_BONUS],[AMOUNT_PLAYABLE_BONUS],[BALANCE_RELEASED_BONUS],[BALANCE_PLAYABLE_BONUS],[AMOUNT_UNDERFLOW],[AMOUNT_RAW_LOYALTY],[BALANCE_RAW_LOYALTY],[TRANSACTION_ON_HOLD_ID],[SSW_TRAN_ID],[REFERENCE], BRAND_ID, MachineID)
			select i.[ACCOUNT_ID],i.[DATETIME],i.[TRAN_TYPE],i.[AMOUNT_REAL],i.[BALANCE_REAL],i.[PLATFORM_TRAN_ID],i.[GAME_TRAN_ID],i.[GAME_ID],i.[PLATFORM_ID],i.[payment_id],i.[ROLLED_BACK],i.[ROLLBACK_TRAN_ID],i.[AMOUNT_RELEASED_BONUS],i.[AMOUNT_PLAYABLE_BONUS],i.[BALANCE_RELEASED_BONUS],i.[BALANCE_PLAYABLE_BONUS],i.[AMOUNT_UNDERFLOW],i.[AMOUNT_RAW_LOYALTY],i.[BALANCE_RAW_LOYALTY],i.[TRANSACTION_ON_HOLD_ID],i.[SSW_TRAN_ID],i.[REFERENCE], i.BRAND_ID, i.MachineID
			from inserted i
				--external_mpt.USER_CONF users
				--inner  join admin_all.account on users.PARTYID = account.PARTYID
				--inner  join inserted i on i.ACCOUNT_ID = account.id			
			select @ID = scope_identity()
			exec sp_set_session_context  'AccountTranID', @ID
		goto ___Exit___
	end
	-- update
	update a
		set a.[ACCOUNT_ID]=i.[ACCOUNT_ID],a.[DATETIME]=i.[DATETIME],a.[TRAN_TYPE]=i.[TRAN_TYPE],a.[AMOUNT_REAL]=i.[AMOUNT_REAL],a.[BALANCE_REAL]=i.[BALANCE_REAL],a.[PLATFORM_TRAN_ID]=i.[PLATFORM_TRAN_ID],a.[GAME_TRAN_ID]=i.[GAME_TRAN_ID],a.[GAME_ID]=i.[GAME_ID],a.[PLATFORM_ID]=i.[PLATFORM_ID],a.[payment_id]=i.[payment_id],a.[ROLLED_BACK]=i.[ROLLED_BACK],a.[ROLLBACK_TRAN_ID]=i.[ROLLBACK_TRAN_ID],a.[AMOUNT_RELEASED_BONUS]=i.[AMOUNT_RELEASED_BONUS],a.[AMOUNT_PLAYABLE_BONUS]=i.[AMOUNT_PLAYABLE_BONUS],a.[BALANCE_RELEASED_BONUS]=i.[BALANCE_RELEASED_BONUS],a.[BALANCE_PLAYABLE_BONUS]=i.[BALANCE_PLAYABLE_BONUS],a.[AMOUNT_UNDERFLOW]=i.[AMOUNT_UNDERFLOW],a.[AMOUNT_RAW_LOYALTY]=i.[AMOUNT_RAW_LOYALTY],a.[BALANCE_RAW_LOYALTY]=i.[BALANCE_RAW_LOYALTY],a.[TRANSACTION_ON_HOLD_ID]=i.[TRANSACTION_ON_HOLD_ID],a.[SSW_TRAN_ID]=i.[SSW_TRAN_ID],a.[REFERENCE]=i.[REFERENCE],a.[BRAND_ID]=i.[BRAND_ID],a.[MachineID]=i.[MachineID]
	from admin_all.ACCOUNT_TRAN_REALTIME a
		inner join inserted i on a.ID = i.ID
		
	if (select PARSENAME ( base_object_name , 3) from sys.synonyms where object_id = object_id('admin_all.Synonym_ACCOUNT_TRAN_ALL')) is null
		or exists(select * from sys.databases where database_id = db_id((select PARSENAME ( base_object_name , 3) from sys.synonyms where object_id = object_id('admin_all.Synonym_ACCOUNT_TRAN_ALL'))) and is_read_only = 0)
	begin
		update a
			set a.[ACCOUNT_ID]=i.[ACCOUNT_ID],a.[DATETIME]=i.[DATETIME],a.[TRAN_TYPE]=i.[TRAN_TYPE],a.[AMOUNT_REAL]=i.[AMOUNT_REAL],a.[BALANCE_REAL]=i.[BALANCE_REAL],a.[PLATFORM_TRAN_ID]=i.[PLATFORM_TRAN_ID],a.[GAME_TRAN_ID]=i.[GAME_TRAN_ID],a.[GAME_ID]=i.[GAME_ID],a.[PLATFORM_ID]=i.[PLATFORM_ID],a.[payment_id]=i.[payment_id],a.[ROLLED_BACK]=i.[ROLLED_BACK],a.[ROLLBACK_TRAN_ID]=i.[ROLLBACK_TRAN_ID],a.[AMOUNT_RELEASED_BONUS]=i.[AMOUNT_RELEASED_BONUS],a.[AMOUNT_PLAYABLE_BONUS]=i.[AMOUNT_PLAYABLE_BONUS],a.[BALANCE_RELEASED_BONUS]=i.[BALANCE_RELEASED_BONUS],a.[BALANCE_PLAYABLE_BONUS]=i.[BALANCE_PLAYABLE_BONUS],a.[AMOUNT_UNDERFLOW]=i.[AMOUNT_UNDERFLOW],a.[AMOUNT_RAW_LOYALTY]=i.[AMOUNT_RAW_LOYALTY],a.[BALANCE_RAW_LOYALTY]=i.[BALANCE_RAW_LOYALTY],a.[TRANSACTION_ON_HOLD_ID]=i.[TRANSACTION_ON_HOLD_ID],a.[SSW_TRAN_ID]=i.[SSW_TRAN_ID],a.[REFERENCE]=i.[REFERENCE],a.[BRAND_ID]=i.[BRAND_ID],a.[MachineID]=i.[MachineID]
		from admin_all.Synonym_ACCOUNT_TRAN_ALL a
			inner join inserted i on a.ID = i.ID
		goto ___Exit___
	end
	set identity_insert admin_all.ACCOUNT_TRAN_REALTIME on
	insert into admin_all.ACCOUNT_TRAN_REALTIME([ID],[ACCOUNT_ID],[DATETIME],[TRAN_TYPE],[AMOUNT_REAL],[BALANCE_REAL],[PLATFORM_TRAN_ID],[GAME_TRAN_ID],[GAME_ID],[PLATFORM_ID],[payment_id],[ROLLED_BACK],[ROLLBACK_TRAN_ID],[AMOUNT_RELEASED_BONUS],[AMOUNT_PLAYABLE_BONUS],[BALANCE_RELEASED_BONUS],[BALANCE_PLAYABLE_BONUS],[AMOUNT_UNDERFLOW],[AMOUNT_RAW_LOYALTY],[BALANCE_RAW_LOYALTY],[TRANSACTION_ON_HOLD_ID],[SSW_TRAN_ID],[REFERENCE],[BRAND_ID],[MachineID])
			select i.[ID],i.[ACCOUNT_ID],i.[DATETIME],i.[TRAN_TYPE],i.[AMOUNT_REAL],i.[BALANCE_REAL],i.[PLATFORM_TRAN_ID],i.[GAME_TRAN_ID],i.[GAME_ID],i.[PLATFORM_ID],i.[payment_id],i.[ROLLED_BACK],i.[ROLLBACK_TRAN_ID],i.[AMOUNT_RELEASED_BONUS],i.[AMOUNT_PLAYABLE_BONUS],i.[BALANCE_RELEASED_BONUS],i.[BALANCE_PLAYABLE_BONUS],i.[AMOUNT_UNDERFLOW],i.[AMOUNT_RAW_LOYALTY],i.[BALANCE_RAW_LOYALTY],i.[TRANSACTION_ON_HOLD_ID],i.[SSW_TRAN_ID],i.[REFERENCE],i.[BRAND_ID], i.[MachineID]
			from inserted i 
				inner join admin_all.Synonym_ACCOUNT_TRAN_ALL a on a.ID = i.ID
			where not exists(select * from admin_all.ACCOUNT_TRAN_REALTIME r where r.id = i.id)
	set identity_insert admin_all.ACCOUNT_TRAN_REALTIME off
___Exit___:
	declare @ret int
	exec @ret = sp_getapplock @Resource = N'AccountTranHourlyAggregateCalculation', @LockMode = N'Shared', @LockOwner = 'Transaction', @LockTimeout = 0
	;with s as
	(
		select dateadd(hour, datediff(hour, '1990-01-01', DATETIME), '1990-01-01') Datetime, case when @ret >= 0 then 0 else 1 end CalculationInProgress from inserted
		union
		select dateadd(hour, datediff(hour, '1990-01-01', DATETIME), '1990-01-01') Datetime, case when @ret >= 0 then 0 else 1 end CalculationInProgress from deleted
	)
	insert into admin_all.AccountTranHourlyAggregateRecalculate(DateTime, CalculationInProgress)
		select DateTime, CalculationInProgress
		from s
		where not exists(
						select * 
						from admin_all.AccountTranHourlyAggregateRecalculate t 
						where t.Datetime = s.Datetime 
							and t.CalculationInProgress = s.CalculationInProgress
						);
--Chronos:
	if chronos.fn_IsEnabled() = 0
		return
	declare @AmountReal numeric(38, 18), @ReleasedBonus numeric(38,18), @PlayableBonus numeric(38,18),@RawLoyalty numeric(38,18), @PartyID int, @Name varchar(50), @TranType varchar(50), @Date datetime,
			@BalanceRR numeric(38, 18), @BalanceBonus numeric(38, 18), @Operation char(1), @EventID bigint, @ProductCode nvarchar(20),
			@GameID nvarchar(100), @GameName nvarchar(100), @GameCategory nvarchar(100), @ProductType varchar(100), @AmountRR numeric(38, 18),
			@AccountTranID bigint
	declare c cursor local fast_forward for
		with x0 as
		(
			select 
				isnull(i.ID, d.ID) AccountTranID,
				isnull(i.ACCOUNT_ID, d.ACCOUNT_ID) AccountID,
				isnull(i.AMOUNT_REAL,0) -isnull(d.AMOUNT_REAL, 0) AmountReal,
				isnull(i.AMOUNT_RELEASED_BONUS,0) -isnull(d.AMOUNT_RELEASED_BONUS, 0) AmountReleasedBonus,
				isnull(i.AMOUNT_PLAYABLE_BONUS,0) -isnull(d.AMOUNT_PLAYABLE_BONUS, 0) AmountPlayableBonus,
				isnull(i.AMOUNT_RAW_LOYALTY, 0) - isnull(d.AMOUNT_RAW_LOYALTY, 0)   RawLoyalty,
				isnull(i.TRAN_TYPE, d.TRAN_TYPE) TranType,
				isnull(i.DATETIME, d.DATETIME) Date,
				case 
					when i.ACCOUNT_ID is null and d.ACCOUNT_ID is not null then 'D'
					when i.ACCOUNT_ID is not null and d.ACCOUNT_ID is null then 'I'
					else 'U'
				end Operation,
				isnull(i.BALANCE_REAL, 0) + isnull(i.BALANCE_RELEASED_BONUS, 0) BalanceRR,
				i.BALANCE_PLAYABLE_BONUS BalanceBonus,
				i.PLATFORM_ID,
				i.GAME_ID,
				isnull(i.BRAND_ID, d.BRAND_ID) BrandID
			from inserted i
				full outer join deleted d on i.ID = d.ID
		)
		select a.PARTYID, t.AmountReal, t.AmountReleasedBonus, t.AmountPlayableBonus, t.RawLoyalty/cast(isnull(bl.LOYALTY_TO_RAW, 1) as numeric(38,18)) ,t.TranType, t.Date, t.Operation, t.BalanceRR, t.BalanceBonus, p.CODE, t.GAME_ID, gi.NAME, c.NAME, p.PLATFORM_TYPE, t.AmountReal + t.AmountReleasedBonus, AccountTranID
		from  x0 t
			inner join admin_all.ACCOUNT a on a.id = t.AccountID
			left join admin_all.PLATFORM p on p.ID = t.PLATFORM_ID
			left join admin_all.GAME_INFO gi on gi.GAME_ID = t.GAME_ID and gi.PLATFORM_ID = t.PLATFORM_ID
			left join admin_all.GAME_CATEGORY c on c.ID = gi.GAME_CATEGORY_ID
			left join admin_all.BRAND_LOYALTY bl on bl.BRANDID = t.BrandID
		where TranType in ('GAME_WIN', 'CASH_OUT', 'GAME_BET', 'BONUS_REL', 'CRE_BONUS', 'PLTFRM_BON', 'TOURN_WIN')
	open c
	fetch next from c into @PARTYID, @AmountReal, @ReleasedBonus, @PlayableBonus, @RawLoyalty, @TranType, @Date, @Operation, @BalanceRR, @BalanceBonus, @ProductCode, @GameID, @GameName, @GameCategory, @ProductType, @AmountRR, @AccountTranID
	while @@fetch_status = 0
	begin
		select @EventID = null
		if @TranType in ('GAME_WIN', 'CASH_OUT')
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID, 'SessionWinRR', @AmountReal, @Date, @ReleasedBonus
		if @TranType in ('GAME_BET')
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID, 'SessionTurnoverRR', @AmountReal, @Date, @ReleasedBonus
		if @TranType in ('GAME_WIN', 'CASH_OUT')
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID, 'SessionWinBonus', @PlayableBonus, @Date
		if @TranType in ('GAME_BET')
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID, 'SessionTurnoverBonus', @PlayableBonus, @Date
		if @TranType in ('BONUS_REL')
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID, 'SessionBonusRelease', @ReleasedBonus, @Date
		if @TranType in ('CRE_BONUS')
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID, 'SessionBonusAward', @PlayableBonus, @Date
		if @TranType in ('GAME_WIN', 'CASH_OUT', 'GAME_BET', 'BONUS_REL', 'CRE_BONUS')
		begin
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID, 'SessionLoyalty', @RawLoyalty, @Date
		end
		if @ProductType = 'SPORTSBOOK'
		begin
			if @TranType in ('GAME_WIN') and @AmountReal + @ReleasedBonus + @PlayableBonus = 0  
				exec chronos.usp_UpdatePartyWinLoss @PartyID, 0, @AmountRR
			else
				exec chronos.usp_UpdatePartyWinLoss @PartyID, 1, @AmountRR
		end
		else
		begin
			if @TranType in ('GAME_BET') and @AmountReal + @ReleasedBonus + @PlayableBonus <> 0 
				exec chronos.usp_UpdatePartyWinLoss @PartyID, 0, @AmountRR
			if @TranType in ('GAME_WIN') and @AmountReal + @ReleasedBonus + @PlayableBonus <> 0 
				exec chronos.usp_UpdatePartyWinLoss @PartyID, 1, @AmountRR
		end

		
		if @TranType in ('GAME_WIN', 'CASH_OUT', 'PLTFRM_BON',
-- 		                 'CRE_BONUS', 'BONUS_REL',
		                 'TOURN_WIN', 'GAME_BET') and @Operation = 'I'
		begin
			insert into chronos.Event(PartyID, Name) 
				select @PartyID, 
						case @TranType 
							when 'CASH_OUT' then 'CashOut' 
							when 'PLTFRM_BON' then 'FreespinWin' 
							when 'CRE_BONUS' then 'BonusAward'
							when 'BONUS_REL' then 'BonusRelease'
							when 'TOURN_WIN' then 'TournamentWin'
							when 'GAME_BET' then 'GameBet'
						end
				where @TranType in ('CASH_OUT', 'PLTFRM_BON', 'CRE_BONUS', 'BONUS_REL', 'TOURN_WIN', 'GAME_BET') 
							
			select @EventID = scope_identity()
			if @TranType in ('GAME_WIN') and @AmountReal + @ReleasedBonus + @PlayableBonus <> 0 
			begin
				insert into chronos.Event(PartyID, Name) values(@PartyID, 'GameWin' )
				select @EventID = scope_identity()
			end
			if @ProductType = 'SPORTSBOOK'
			begin
				if @TranType in ('GAME_WIN') and @AmountReal + @ReleasedBonus + @PlayableBonus = 0  
				begin
					insert into chronos.Event(PartyID, Name) values(@PartyID, 'SportsLoss')
					select @EventID = scope_identity()
					exec chronos.usp_UpdatePartyWinLoss @PartyID, 0, @AmountRR
				end
			end
		end
		if @EventID is not null
		begin
			insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'AccountTranId', isnull(@AccountTranID, @ID))
			if @BalanceRR <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'BalanceRR', @BalanceRR)
			if @BalanceBonus <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'BalanceBonus', @BalanceBonus)
			if @AmountReal <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'AmountReal', @AmountReal)
			if @ReleasedBonus <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'AmountReleasedBonus', @ReleasedBonus)
			if @PlayableBonus <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'AmountPlayableBonus', @PlayableBonus)
			if @AmountReal + @ReleasedBonus + @PlayableBonus <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'Amount', @AmountReal + @ReleasedBonus + @PlayableBonus)

			if nullif(rtrim(@ProductCode), '') is not null insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'Provider', @ProductCode)
			if nullif(rtrim(@GameID), '') is not null insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'GameID', @GameID)
			if nullif(rtrim(@GameName), '') is not null insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'GameName', @GameName)
			if nullif(rtrim(@GameCategory), '') is not null insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'GameCategory', @GameCategory)
			exec chronos.usp_UpdateWinLossEventData @EventID, @PartyID
		end
		fetch next from c into @PARTYID, @AmountReal, @ReleasedBonus, @PlayableBonus, @RawLoyalty, @TranType, @Date, @Operation, @BalanceRR, @BalanceBonus, @ProductCode, @GameID, @GameName, @GameCategory, @ProductType,  @AmountRR, @AccountTranID
	end
	close c
	deallocate c
	declare @PaymentID int, @i int=1, @TotalEvent int
--Chronos bank activity
	if cast(session_context(N'TransactionID') as bigint) = current_transaction_id()
	begin
		select @TotalEvent = cast(session_context(N'TotalEvent') as int)
		while @i<= @TotalEvent
		begin
			select	@EventID = cast(session_context(N'EventID' + cast(@i as varchar(20))) as bigint),
					@PaymentID = cast(session_context(N'PaymentID' + cast(@i as varchar(20))) as int)

			insert into chronos.EventData(EventID, Name, Value1) 
			select @EventID, 'BalanceRR', isnull(BALANCE_REAL, 0) + isnull(BALANCE_RELEASED_BONUS, 0)
			from inserted i
			where i.payment_id = @PaymentID
				and isnull(BALANCE_REAL, 0) + isnull(BALANCE_RELEASED_BONUS, 0) <> 0
			insert into chronos.EventData(EventID, Name, Value1) 
			select @EventID, 'BalanceBonus', isnull(BALANCE_PLAYABLE_BONUS, 0)
			from inserted i
			where i.payment_id = @PaymentID
				and isnull(BALANCE_PLAYABLE_BONUS, 0) <> 0
			select @i = @i + 1
		end

	end
end