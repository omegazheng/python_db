set ansi_nulls, quoted_identifier on
go
create or alter trigger chronos.TRI_UserTag_Interface on chronos.UserTag_Interface
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [chronos].[UserTag] t
	using inserted s on s.Code = t.Code and s.PartyID = t.PartyID
	when not matched then
		insert (Code,PartyID, Name, ___IsDeleted___)
			values (s.Code, s.PartyID, s.Name, isnull(s.___IsDeleted___, 0))
	when matched and (isnull(s.___IsDeleted___, 0) <> t.___IsDeleted___ or  s.[Name] is null and t.[Name] is not null or s.[Name] is not null and t.[Name] is null or s.[Name] <> t.[Name]) then
		update set t.[Code] = case isnull(s.___IsDeleted___, 0) when 0 then s.Name else t.Name end,
					t.___IsDeleted___ = isnull(s.___IsDeleted___, 0)
	;
	
end