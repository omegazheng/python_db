set ansi_nulls, quoted_identifier on
if object_id('admin_all.TRI_USER_ACTION_LOG_ForChronos') is null
	exec('create trigger admin_all.TRI_USER_ACTION_LOG_ForChronos on admin_all.USER_ACTION_LOG for insert as return')
go
alter trigger admin_all.TRI_USER_ACTION_LOG_ForChronos on admin_all.USER_ACTION_LOG
for insert
as 
begin
	if @@rowcount = 0
		return;
	if chronos.fn_IsEnabled() = 0
		return
	set nocount on
	declare @EventID bigint, @Value varchar(8000), @PartyID int, @ActionType varchar(50)
	declare c cursor local fast_forward for
		select i.ACTION_TYPE, i.PARTYID, rtrim(isnull(i.COMMENT, i.NEW_VALUE))
		from inserted i 
		where ACTION_TYPE in('DEPOSIT_LIMIT_REACHED', 'APPLY_FREESPIN')
			and nullif(rtrim(isnull(i.COMMENT, i.NEW_VALUE)), '')  is not null
	open c
	fetch next from c into @ActionType, @PartyID, @Value
	while @@fetch_status = 0
	begin
		insert into chronos.Event(PartyID, Name) values(@PartyID, case @ActionType when 'DEPOSIT_LIMIT_REACHED' then 'LimitReached' else 'FreeSpinAward' end)
		select @EventID = scope_identity()
		insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'Information', @Value)
		fetch next from c into @ActionType, @PartyID, @Value
	end
	close c
	deallocate c
end

