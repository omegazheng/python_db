set ansi_nulls, quoted_identifier on
go
if object_id('[chronos].[TRI_VIPStatus_Interface]') is null
	exec('create trigger [chronos].[TRI_VIPStatus_Interface] on [chronos].[VIPStatus_Interface]
instead of insert
as
begin
	return
end')
go 
ALTER trigger [chronos].[TRI_VIPStatus_Interface] on [chronos].[VIPStatus_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [chronos].[VIPStatus] t
	using inserted s on s.[VIPStatusID] = t.[VIPStatusID]
	when not matched then
		insert ([VIPStatusID],[Code], ___IsDeleted___)
			values (s.[VIPStatusID],s.[Code], isnull(s.___IsDeleted___, 0))
	when matched and (isnull(s.___IsDeleted___, 0) <> t.___IsDeleted___ or  s.[Code] is null and t.[Code] is not null or s.[Code] is not null and t.[Code] is null or s.[Code] <> t.[Code]) then
		update set t.[Code] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Code] else t.[Code] end,
					t.___IsDeleted___ = isnull(s.___IsDeleted___, 0)
	;
	
end