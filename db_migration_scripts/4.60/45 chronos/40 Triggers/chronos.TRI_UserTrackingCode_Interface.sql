SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
if object_id('chronos.TRI_UserTrackingCode_Interface') is null
exec('create trigger [chronos].[TRI_UserTrackingCode_Interface] on [chronos].[UserTrackingCode_Interface]
instead of insert
as
begin
return
end')
go
alter trigger [chronos].[TRI_UserTrackingCode_Interface] on [chronos].[UserTrackingCode_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [chronos].[UserTrackingCode] t
	using (
			select 
					UserTrackingCodeID,
					PartyID,
					case when isnull(CodeKey, '') = 'affId' and isnull(___IsDeleted___, 0) = 0 and Value is not null then 0 else 1 end ___IsDeleted___
			from inserted
			) s on s.[UserTrackingCodeID] = t.[UserTrackingCodeID]
	when not matched then
		insert ([UserTrackingCodeID],[PartyID], ___IsDeleted___)
			values (s.[UserTrackingCodeID],s.[PartyID], s.___IsDeleted___)
	when matched and (isnull(s.___IsDeleted___, 0) <> t.___IsDeleted___ or s.[PartyID] is null and t.[PartyID] is not null or s.[PartyID] is not null and t.[PartyID] is null or s.[PartyID] <> t.[PartyID]) then
		update set t.[PartyID] = case isnull(s.___IsDeleted___, 0) when 0 then s.[PartyID] else t.[PartyID] end,
					t.___IsDeleted___ = isnull(s.___IsDeleted___, 0)
	;
	
end
GO
--exec sp_help 'admin_all.user_tracking_code'

