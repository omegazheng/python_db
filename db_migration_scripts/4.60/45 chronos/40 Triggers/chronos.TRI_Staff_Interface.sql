set ansi_nulls, quoted_identifier on
go
if object_id('[chronos].[TRI_Staff_Interface]') is null
	exec('create trigger [chronos].[TRI_Staff_Interface] on [chronos].[Staff_Interface]
instead of insert
as
begin
	return
end')
go 
ALTER trigger [chronos].[TRI_Staff_Interface] on [chronos].[Staff_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [chronos].[Party] t
	using ( select 
					-PartyID as PartyID, 
					-1 UserType, 
					UserID, 
					FirstName as Nickname, 
					Email, FirstName, LastName, Mobile, Language, RegistrationDate, VIPStatus, 
					Hierarchy, 
					case when isnull(Active, 0) = 0 then 1 else  ___IsDeleted___ end ___IsDeleted___
			from inserted
		)s on s.[PartyID] = t.[PartyID]
	when not matched then
		insert ([PartyID],[UserID],[Nickname],[Email],[FirstName],[LastName],[Mobile],[Language],[RegistrationDate],[VIPStatus],[Hierarchy], ___IsDeleted___)
			values (s.[PartyID],s.[UserID],s.[Nickname],s.[Email],s.[FirstName],s.[LastName],s.[Mobile],s.[Language],s.[RegistrationDate],s.[VIPStatus],s.[Hierarchy], isnull(___IsDeleted___, 0))
	when matched and (isnull(s.___IsDeleted___, 0) <> t.___IsDeleted___ or s.[UserID] is null and t.[UserID] is not null or s.[UserID] is not null and t.[UserID] is null or s.[UserID] <> t.[UserID] or s.[Nickname] is null and t.[Nickname] is not null or s.[Nickname] is not null and t.[Nickname] is null or s.[Nickname] <> t.[Nickname] or s.[Email] is null and t.[Email] is not null or s.[Email] is not null and t.[Email] is null or s.[Email] <> t.[Email] or s.[FirstName] is null and t.[FirstName] is not null or s.[FirstName] is not null and t.[FirstName] is null or s.[FirstName] <> t.[FirstName] or s.[LastName] is null and t.[LastName] is not null or s.[LastName] is not null and t.[LastName] is null or s.[LastName] <> t.[LastName] or s.[Mobile] is null and t.[Mobile] is not null or s.[Mobile] is not null and t.[Mobile] is null or s.[Mobile] <> t.[Mobile] or s.[Language] is null and t.[Language] is not null or s.[Language] is not null and t.[Language] is null or s.[Language] <> t.[Language] or s.[RegistrationDate] is null and t.[RegistrationDate] is not null or s.[RegistrationDate] is not null and t.[RegistrationDate] is null or s.[RegistrationDate] <> t.[RegistrationDate] or s.[VIPStatus] is null and t.[VIPStatus] is not null or s.[VIPStatus] is not null and t.[VIPStatus] is null or s.[VIPStatus] <> t.[VIPStatus] or s.[Hierarchy] is null and t.[Hierarchy] is not null or s.[Hierarchy] is not null and t.[Hierarchy] is null or s.[Hierarchy] <> t.[Hierarchy] or s.[UserType] is null and t.[UserType] is not null or s.[UserType] is not null and t.[UserType] is null or s.[UserType] <> t.[UserType]) then
		update set	t.[UserID] = case isnull(s.___IsDeleted___, 0) when 0 then s.[UserID] else t.[UserID] end,
					t.[Nickname] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Nickname] else t.[Nickname] end,
					t.[Email] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Email] else t.[Email] end,
					t.[FirstName] = case isnull(s.___IsDeleted___, 0) when 0 then s.[FirstName] else t.[FirstName] end,
					t.[LastName] = case isnull(s.___IsDeleted___, 0) when 0 then s.[LastName] else t.[LastName] end,
					t.[Mobile] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Mobile] else t.[Mobile] end,
					t.[Language] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Language] else t.[Language] end,
					t.[RegistrationDate] = case isnull(s.___IsDeleted___, 0) when 0 then s.[RegistrationDate] else t.[RegistrationDate] end,
					t.[VIPStatus] = case isnull(s.___IsDeleted___, 0) when 0 then s.[VIPStatus] else t.[VIPStatus] end,
					t.UserType = -1,
					t.[Hierarchy] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Hierarchy] else t.[Hierarchy] end,
					t.___IsDeleted___ = isnull(s.___IsDeleted___, 0)
	;
end
GO

