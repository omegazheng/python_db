set ansi_nulls, quoted_identifier on
if object_id('admin_all.TRI_PAYMENT_ForChronos') is null
	exec('create trigger admin_all.TRI_PAYMENT_ForChronos on admin_all.PAYMENT for insert as return')
go
alter trigger admin_all.TRI_PAYMENT_ForChronos on admin_all.PAYMENT
for insert, update
as 
begin
	if @@rowcount = 0
		return
	if chronos.fn_IsEnabled() = 0
		return
	set nocount on
	declare @PaymentType varchar(25), @OldStatus varchar(25), @NewStatus varchar(25), @AmountReal numeric(38, 18), @Amount numeric(38, 18), @AmountReleasedBonus numeric(38,18), @PartyID int, @TransactionID bigint = current_transaction_id(), @PaymentID int, @EventID int, @Date datetime, @Method nvarchar(30), @i int = 0, @VarName nvarchar(50), @Fee numeric(38, 18), @BonusCode varchar(50)
	exec sp_set_session_context 'TransactionID', @TransactionID
	declare c cursor local fast_forward for 
		with x0 as
		(
			select i.TYPE, i.STATUS as NewStatus, d.STATUS as OldStatus, i.AMOUNT, i.AMOUNT_REAL, i.AMOUNT_RELEASED_BONUS, i.ACCOUNT_ID, i.id , i.METHOD, i.FEE, i.REQ_BONUS_PLAN_ID
			from inserted i
				full outer join deleted d on i.id = d.id
			where i.TYPE in ('DEPOSIT') and i.STATUS in ('COMPLETED')
				or i.TYPE in ('WITHDRAW', 'WITHDRAWAL') and i.STATUS in ('PENDING', 'COMPLETED', 'CANCELLED', 'REJECTED')
		)
	
		select a.PARTYID, x0.TYPE, x0.OldStatus, x0.NewStatus, isnull(x0.AMOUNT, 0), isnull(x0.AMOUNT_REAL, 0), isnull(x0.AMOUNT_RELEASED_BONUS, 0), x0.id, x0.METHOD, isnull(x0.FEE, 0), bp.BONUS_CODE
		from x0
			inner join admin_all.ACCOUNT a on a.id = x0.ACCOUNT_ID
			left join admin_all.BONUS_PLAN bp on bp.ID = try_cast(x0.REQ_BONUS_PLAN_ID as int)
		where isnull(x0.OldStatus,  '') <> isnull(x0.NewStatus, '')
	open c
	fetch next from c into @PartyID, @PaymentType, @OldStatus, @NewStatus, @Amount, @AmountReal, @AmountReleasedBonus, @PaymentID, @Method, @Fee, @BonusCode
	while @@fetch_status = 0
	begin
		select @EventID = null
		if @PaymentType = 'DEPOSIT' and @NewStatus = 'COMPLETED'
		begin
			insert into chronos.Event(PartyID, Name) values(@PartyID, 'Deposit')
			select @EventID = scope_identity()
		end
		else if @PaymentType = 'WITHDRAWAL' and @NewStatus in ('PENDING', 'COMPLETED', 'CANCELLED', 'REJECTED')
		begin
			insert into chronos.Event(PartyID, Name) 
				values(@PartyID, case @NewStatus 
									when 'PENDING' then 'WithdrawalRequest'
									when 'COMPLETED' then 'WithdrawalProcess'
									when 'CANCELLED' then 'WithdrawalCancel'
									when 'REJECTED' then 'WithdrawalReject'
								end)
			select @EventID = scope_identity()
		end
		if @EventID is not null
		begin
			select @i = @i + 1
			select @VarName = 'EventID' + cast(@i as varchar(20)); exec sp_set_session_context @VarName, @EventID
			select @VarName = 'PaymentID' + cast(@i as varchar(20)); exec sp_set_session_context @VarName, @PaymentID
			
			if @BonusCode is not null  insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'BonusCode', @Amount)
			if @Amount<> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'Amount', @Amount)
			if @AmountReal <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'AmountReal', @AmountReal)
			if @AmountReleasedBonus <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'AmountReleasedBonus', @AmountReleasedBonus)
			if @Fee<> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'TransactionFee', @Fee)
			if @Method is not null  insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'PaymentMethod', @Method)
			if @PaymentID is not null  insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'PaymentId', @PaymentID)
		end
		fetch next from c into @PartyID, @PaymentType, @OldStatus, @NewStatus, @Amount, @AmountReal, @AmountReleasedBonus, @PaymentID, @Method, @Fee, @BonusCode
	end
	close c
	deallocate c
	exec sp_set_session_context 'TotalEvent', @i
end
--select * from chronos.EventData