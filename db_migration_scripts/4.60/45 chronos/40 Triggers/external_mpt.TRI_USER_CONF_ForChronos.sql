set ansi_nulls, quoted_identifier on
if object_id('external_mpt.TRI_USER_CONF_ForChronos') is null
	exec('create trigger external_mpt.TRI_USER_CONF_ForChronos on external_mpt.USER_CONF for insert, delete, update as return')
go
alter trigger external_mpt.TRI_USER_CONF_ForChronos on external_mpt.USER_CONF 
for insert, delete, update 
as 
begin
	if @@rowcount = 0
		return
		
	if chronos.fn_IsEnabled() = 0
		return
	set nocount on
	declare @OldRegistrationStatus varchar(10), @NewRegistrationStatus varchar(10),
			@OldMadeDeposit tinyint, @NewMadeDeposit tinyint,
			@OldVIPStatus int, @NewVIPStatus int,
			@PartyID int, @EventID bigint
		

	declare c cursor fast_forward local for
		select isnull(i.PARTYID, d.PARTYID), i.REGISTRATION_STATUS, d.REGISTRATION_STATUS, i.MADE_DEPOSIT, d.MADE_DEPOSIT, i.VIP_STATUS, d.VIP_STATUS
		from inserted i
			full outer join deleted d on i.PARTYID = d.PARTYID
		where (i.REGISTRATION_STATUS is null or d.REGISTRATION_STATUS is null or i.REGISTRATION_STATUS<> d.REGISTRATION_STATUS)
			or (i.MADE_DEPOSIT is null or d.MADE_DEPOSIT is null or i.MADE_DEPOSIT <> d.MADE_DEPOSIT)
			or isnull(i.VIP_STATUS, -123) <> isnull(d.VIP_STATUS, -123)
	open c
	fetch next from c into @PartyID, @NewRegistrationStatus, @OldRegistrationStatus, @NewMadeDeposit, @OldMadeDeposit, @NewVIPStatus, @OldVIPStatus
	while @@fetch_status = 0
	begin

		if @NewRegistrationStatus in('OPEN', 'QUICK_OPEN')
		begin
			insert into chronos.Event(Name, PartyID) values('Registration', @PartyID);
			select @EventID = scope_identity()

			insert into chronos.EventData(EventID, Name, Value1, Value2)
			select @EventID, 'Confirmation', isnull(@NewRegistrationStatus, 'Unknown'), @OldRegistrationStatus

			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'UserID',  UserID from inserted where UserID is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Nickname',  NICKNAME from inserted where NICKNAME is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Email',  EMAIL from inserted where EMAIL is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Title',  TITLE from inserted where TITLE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'FirstName',  FIRST_NAME from inserted where FIRST_NAME is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'LastName',  LAST_NAME from inserted where LAST_NAME is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Gender',  GENDER from inserted where GENDER is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'BirthDate',  BIRTHDATE from inserted where BIRTHDATE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Address',  ADDRESS from inserted where ADDRESS is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'City',  CITY from inserted where CITY is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Province',  PROVINCE from inserted where PROVINCE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Country',  COUNTRY from inserted where COUNTRY is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Mobile',  MOBILE_PHONE from inserted where MOBILE_PHONE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Language',  LANGUAGE from inserted where LANGUAGE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Currency',  CURRENCY from inserted where CURRENCY is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'RegistrationDate',  REG_DATE from inserted where REG_DATE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'VIPStatus',  VIP_STATUS from inserted where VIP_STATUS is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'UserType',  user_type from inserted where user_type is not null and PartyID = @PartyID

		end
		if		@NewRegistrationStatus in('PLAYER', 'REGISTERED', 'QUICK_REG') and
		    (@OldRegistrationStatus IS NULL or   @OldRegistrationStatus NOT IN ('REGISTERED', 'QUICK_REG'))
		begin
			insert into chronos.Event(Name, PartyID) values('Confirmation', @PartyID);
			select @EventID = scope_identity()
			
			insert into chronos.EventData(EventID, Name, Value1, Value2)
				select @EventID, 'Confirmation', isnull(@NewRegistrationStatus, 'Unknown'), @OldRegistrationStatus
			

			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'UserID',  UserID from inserted where UserID is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Nickname',  NICKNAME from inserted where NICKNAME is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Email',  EMAIL from inserted where EMAIL is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Title',  TITLE from inserted where TITLE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'FirstName',  FIRST_NAME from inserted where FIRST_NAME is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'LastName',  LAST_NAME from inserted where LAST_NAME is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Gender',  GENDER from inserted where GENDER is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'BirthDate',  BIRTHDATE from inserted where BIRTHDATE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Address',  ADDRESS from inserted where ADDRESS is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'City',  CITY from inserted where CITY is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Province',  PROVINCE from inserted where PROVINCE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Country',  COUNTRY from inserted where COUNTRY is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Mobile',  MOBILE_PHONE from inserted where MOBILE_PHONE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Language',  LANGUAGE from inserted where LANGUAGE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'Currency',  CURRENCY from inserted where CURRENCY is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'RegistrationDate',  REG_DATE from inserted where REG_DATE is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'VIPStatus',  VIP_STATUS from inserted where VIP_STATUS is not null and PartyID = @PartyID
			insert into chronos.EventData(EventID, Name, Value1)
			select @EventID, 'UserType',  user_type from inserted where user_type is not null and PartyID = @PartyID


		end
		if isnull(@NewVIPStatus, -123) <> isnull(@OldVIPStatus, -123)
		begin
			insert into chronos.Event(Name, PartyID) values('ChangeInVIPStatus', @PartyID);
			insert into chronos.EventData(EventID, Name, Value1, Value2)
				select scope_identity(), 'ChangeInVIPStatus', isnull((select code from admin_all.VIP_STATUS where id = @NewVIPStatus), 'Unknown'), (select code from admin_all.VIP_STATUS where id = @OldVIPStatus)
		end
		if isnull(@OldMadeDeposit, 0) = 0 and isnull(@NewMadeDeposit, 0) = 1
		begin
			insert into chronos.Event(Name, PartyID) values('FirstDeposit', @PartyID);
			insert into chronos.EventData(EventID, Name, Value1, Date)
				select scope_identity(), 'amount', p.AMOUNT , REQUEST_DATE
				from admin_all.PAYMENT p 
					inner join admin_all.ACCOUNT a on a.id = p.ACCOUNT_ID
				where p.STATUS = 'COMPLETED' 
					and TYPE = 'DEPOSIT' 
					and a.PARTYID = @PartyID
				order by PROCESS_DATE asc
		end
		fetch next from c into @PartyID, @NewRegistrationStatus, @OldRegistrationStatus, @NewMadeDeposit, @OldMadeDeposit, @NewVIPStatus, @OldVIPStatus
	end
	close c
	deallocate c
end