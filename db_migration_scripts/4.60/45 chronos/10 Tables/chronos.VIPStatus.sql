set ansi_nulls, quoted_identifier on
go
if object_id('chronos.VIPStatus') is null
begin
	create table chronos.VIPStatus
	(
		VIPStatusID int NOT NULL,
		Code varchar(50) NOT NULL, 
		___IsDeleted___ bit not NULL constraint DF_chronos_VIPStatus____IsDeleted___ default(0),
		___RowVersion___ rowversion not null
		constraint PK_chronos_VIPStatus primary key (VIPStatusID),
		constraint UQ_chronos_VIPStatus____RowVersion___ unique(___RowVersion___)
	);
end