set ansi_nulls, quoted_identifier on
go
if object_id('chronos.SynchronizationStatus') is null
begin
	create table chronos.SynchronizationStatus
	(
		ConsumerID uniqueidentifier not null,
		Name varchar(128) not null,
		FromVersion binary(8) not null,
		ToVersion binary(8) not null,
		EventCategoryID int not null constraint DF_admin_all_SynchronizationStatus_EventCategoryID default(-1)
		constraint PK_chronos_SynchronizationStatus primary key (ConsumerID, Name, EventCategoryID)
	)
end



