set ansi_nulls, quoted_identifier on
go
if object_id('Chronos.UserAggregate') is null
begin
	create table chronos.UserAggregate
	(
		PartyID int NOT NULL,
		Currency nchar(3) not NULL,
		
		LTDDepositAmountReal numeric(38, 18) NOT NULL,
		LTDDepositAmountReleasedBonus numeric(38, 18) NOT NULL,
		LTDDepositAmountPlayableBonus numeric(38, 18) NOT NULL,

		LTDWithdrawAmountReal numeric(38, 18) NOT NULL,
		LTDWithdrawAmountReleasedBonus numeric(38, 18) NOT NULL,
		LTDWithdrawAmountPlayableBonus numeric(38, 18) NOT NULL,


		LTDTransferOutAmountReal numeric(38, 18) NOT NULL,
		LTDTransferOutAmountReleasedBonus numeric(38, 18) NOT NULL,
		LTDTransferOutAmountPlayableBonus numeric(38, 18) NOT NULL,

		YTDWithdrawAmountReal numeric(38, 18) NOT NULL,
		YTDWithdrawAmountReleasedBonus numeric(38, 18) NOT NULL,
		YTDWithdrawAmountPlayableBonus numeric(38, 18) NOT NULL,

		YTDDepositAmountReal numeric(38, 18) NOT NULL,
		YTDDepositAmountReleasedBonus numeric(38, 18) NOT NULL,
		YTDDepositAmountPlayableBonus numeric(38, 18) NOT NULL,
				
		YTDTransferOutAmountReal numeric(38, 18) NOT NULL,
		YTDTransferOutAmountReleasedBonus numeric(38, 18) NOT NULL,
		YTDTransferOutAmountPlayableBonus numeric(38, 18) NOT NULL,
		___IsDeleted___ bit not NULL constraint DF_chronos_UserAggregate____IsDeleted___ default(0),
		___RowVersion___ rowversion not null,
		constraint UQ_chronos_UserAggregate____RowVersion___ unique(___RowVersion___),
		constraint PK_Chronos_UserAggregate primary key(PartyID, Currency)
	)
end
