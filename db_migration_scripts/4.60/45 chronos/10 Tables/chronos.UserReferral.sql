set ansi_nulls, quoted_identifier on
go
if object_id('chronos.UserReferral') is null
begin
	create table chronos.UserReferral
	(
		UserReferralID int not null,
		PartyID int NOT NULL,
		ReferredPartyID int NULL, 
		___IsDeleted___ bit not NULL constraint DF_chronos_UserReferral____IsDeleted___ default(0),
		___RowVersion___ rowversion not null
		constraint PK_chronos_UserReferral primary key (UserReferralID),
		constraint UQ_chronos_UserReferral____RowVersion___ unique(___RowVersion___)
		
	);
end
go 
if not exists(select * from sys.indexes where object_id = object_id('chronos.UserReferral') and name = 'IDX_chronos_UserReferral_ReferredPartyID_PartyID')
begin
	create index IDX_chronos_UserReferral_ReferredPartyID_PartyID on chronos.UserReferral(ReferredPartyID, PartyID, ___RowVersion___ desc)
end