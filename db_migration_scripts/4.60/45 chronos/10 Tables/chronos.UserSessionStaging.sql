set ansi_nulls, quoted_identifier on
if object_id('chronos.UserSessionStaging') is null
begin
	create table chronos.UserSessionStaging
	(
		PartyID int not null,
		Name varchar(50) not null,
		Value numeric(38, 18) not null,
		Date datetime not null constraint DF_chronos_UserSessionStaging_Date default(getdate()),
		constraint PK_chronos_UserSessionStaging primary key (PartyID, Name)
	)
end
go

