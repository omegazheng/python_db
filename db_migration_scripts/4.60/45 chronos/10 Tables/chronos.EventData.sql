set ansi_nulls, quoted_identifier on
if object_id('chronos.EventData') is null
begin
	create table chronos.EventData
	(
		EventID bigint not null,
		Name nvarchar(200) not null,
		Value1 sql_variant not null,
		Value2 sql_variant null,
		Date datetime not null constraint DF_chronos_EventData_Date default(getdate()),
		constraint PK_chronos_EventPlayerActivity primary key (EventID,Name)with(ignore_dup_key=on)
	)
end
--begin transaction
--alter table chronos.EventData drop constraint PK_chronos_EventPlayerActivity
--alter table chronos.EventData alter column Name nvarchar(200) not null
--alter table chronos.EventData add  constraint PK_chronos_EventPlayerActivity primary key (EventID,Name)with(ignore_dup_key=on)
--commit
