set ansi_nulls, quoted_identifier on
go
if object_id('chronos.PartyWinLoss') is null
begin
	create table chronos.PartyWinLoss
	(
		PartyID int NOT NULL,
		WinsInARow int not null,
		LossInARow int not null,
		WinsInARowAmountRR numeric(38,18) not null,
		LossInARowAmountRR numeric(38,18) not null,
		LastUpdateDate datetime not null,
		constraint PK_chronos_PartyWinLoss primary key ( PartyID)
	);
end
