SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_PLAN_FREEPLAY]') AND type in (N'U'))
  BEGIN
    CREATE TABLE [admin_all].[BONUS_PLAN_FREEPLAY](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [BONUS_PLAN_ID] [int] NOT NULL,
      [PRODUCT_ID] [int] NOT NULL,
      [START_DATE] [datetime] NULL,
      [EXPIRY_DATE] [datetime] NULL,
      [GAME_INFO_IDS] nvarchar(3000) NULL,
      [ROUNDS] [int] NULL,
      [PROMO_NAME] nvarchar(255) NULL,
      [COIN_VALUE] [int] NULL,
      [FREEPLAN_ID] [int] NULL,
      [EXPIRATION_BONUS_TIME] [datetime] NULL,
      [WAGER_REQUIREMENT] [int] NULL,
      [LINES] [int] NULL,
      [DENOMINATION] nvarchar(255) NULL,
      [PROMO_CODE] nvarchar(255) NULL,
      [EXPIRE_IN_HOURS] [int] NULL,
      [BET_AMOUNT] double precision NULL,
      [CURRENCY_CODE] nchar(3) NULL,
      [ALLOW_MULTIPLE_ASSIGN] binary NULL,
      [MIN_BET] binary NULL,
      [IS_ROUND_BET] binary NULL,
      [COST] numeric(38,18) NULL,
      [NUM_LINES] [int] NULL,
      [NUM_COINS] [int] NULL,
      [COIN_SIZE] numeric(38,18) NULL,
      [SUBPLATFORM_NAME] nvarchar(255) NULL,
      [COINS] [int] NULL,
      [BET_LEVEL] [int] NULL,
      [COIN_VAL] [int] NULL,
      [AMOUNT] [int] NULL,
      [CANCEL_AMOUNT] [int] NULL,
      [FREEBET_ID] [int] NULL
        CONSTRAINT [PK_admin_all_BONUS_PLAN_FREEPLAY] PRIMARY KEY CLUSTERED
          (
            [ID] ASC
          )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
  END
GO