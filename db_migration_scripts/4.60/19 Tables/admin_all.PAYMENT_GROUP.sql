set quoted_identifier, ansi_nulls on
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[PAYMENT_GROUP]') AND type in (N'U'))
begin
	create table ADMIN_ALL.PAYMENT_GROUP
	(
        ID  int identity,
        ORGPAYMENTID int,
        PAYMENT_REF  int,
        constraint PK_admin_all_PAYMENT_GROUP primary key(ID)
	)
end