set ansi_nulls, quoted_identifier on
go
set xact_abort on
go

if not exists(
				select * 
				from sys.indexes i 
					inner join sys.index_columns ic on i.object_id = ic.object_id and i.index_id = ic.index_id
				where i.name = 'IDX_CS_admin_all_AccountTranAggregate' 
					and  i.object_id = object_id('admin_all.AccountTranAggregate')
					and col_name(ic.object_id, ic.column_id) = 'BonusPlanTran_AmountPlayableBonus'
				)
begin
	begin transaction
	create nonclustered columnstore index IDX_CS_admin_all_AccountTranAggregate ON admin_all.AccountTranAggregate(Period, AggregateType,Date,BrandID,PartyID,Currency,TranType,ProductID,GameID,AmountReal,AmountReleasedBonus,AmountPlayableBonus,GameCount,TranCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)with (drop_existing = on) 
	insert into admin_all.AccountTranAggregatePendingCalculation(Period, Date)
		select distinct 'D', cast(datetime as date) from admin_all.AccountTranHourlyAggregate
		union all
		select distinct 'M', datefromparts (year(Datetime), month(Datetime), 1) from admin_all.AccountTranHourlyAggregate
		union all
		select distinct 'Y', datefromparts (year(Datetime), 1, 1) from admin_all.AccountTranHourlyAggregate
	commit
end

GO


