if exists(select * from sys.tables where object_id = object_id('admin_all.DW_ACCOUNT_TRAN_DAILY'))
	drop table admin_all.DW_ACCOUNT_TRAN_DAILY
go
set ansi_nulls, quoted_identifier on
go
create or alter view admin_all.DW_ACCOUNT_TRAN_DAILY
as
	select 
		g.Date SUMMARY_DATE,
		g.PartyID PARTY_ID,
		a.ID ACCOUNT_ID,
		g.TranType TRAN_TYPE,
		b.BRANDNAME BRAND,
		g.Currency CURRENCY,
		g.BrandID BRAND_ID,
		cast(dateadd(month, datediff(month, '2000-01-01', Date), '2000-01-01') as datetime) MONTH,
		cast(dateadd(year, datediff(year, '2000-01-01', Date), '2000-01-01') as datetime) YEAR,
		sum(g.AmountReal) AMOUNT_REAL,
		sum(g.AmountReleasedBonus) AMOUNT_RELEASED_BONUS,
		sum(g.AmountPlayableBonus) AMOUNT_PLAYABLE_BONUS,
		sum(isnull(g.RawLoyalty, 0)) AMOUNT_RAW_LOYALTY
	from admin_all.AccountTranAggregate g
		inner join admin_all.ACCOUNT a on a.PARTYID = g.PartyID
		inner join admin.CASINO_BRAND_DEF b on b.BRANDID = g.BrandID
	where g.AggregateType = 0
		and g.Period = 'D'
	group by g.Date, g.PartyID, a.id, g.TranType, b.BRANDNAME, g.Currency, g.BrandID
go
----set statistics io, time on
--select PARTY_ID,sum(AMOUNT_REAL), sum(AMOUNT_RELEASED_BONUS), sum(AMOUNT_PLAYABLE_BONUS), sum(AMOUNT_RAW_LOYALTY), count(*)
--from admin_all.DW_ACCOUNT_TRAN_DAILY1 a
--where a.SUMMARY_DATE between '2018-01-01' and '2019-01-01' --and PARTY_ID in (1435681,1601188,1330882,151053,1806282,1796782,1605870,965,1588409,1692152,167648,1657272,1678989,1668745)
--group by PARTY_ID
----order by 1
------except
--select PARTY_ID, sum(AMOUNT_REAL), sum(AMOUNT_RELEASED_BONUS), sum(AMOUNT_PLAYABLE_BONUS), sum(AMOUNT_RAW_LOYALTY), count(*)
--from admin_all.DW_ACCOUNT_TRAN_DAILY a
--where a.SUMMARY_DATE between '2018-01-01' and '2019-01-01' --and PARTY_ID in (1435681,1601188,1330882,151053,1806282,1796782,1605870,965,1588409,1692152,167648,1657272,1678989,1668745)
--group by PARTY_ID
----order by 1





