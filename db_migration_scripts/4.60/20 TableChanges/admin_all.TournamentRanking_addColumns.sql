SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[TOURNAMENT_RANKING]') and name = 'AMOUNT_BET')
ALTER TABLE [admin_all].[TOURNAMENT_RANKING]
    ADD [AMOUNT_BET] numeric(38,18) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[TOURNAMENT_RANKING]') and name = 'AMOUNT_WIN')
ALTER TABLE [admin_all].[TOURNAMENT_RANKING]
    ADD [AMOUNT_WIN] numeric(38,18) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[TOURNAMENT_RANKING]') and name = 'BET_COUNT')
ALTER TABLE [admin_all].[TOURNAMENT_RANKING]
    ADD [BET_COUNT] int null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[TOURNAMENT_RANKING]') and name = 'WIN_COUNT')
ALTER TABLE [admin_all].[TOURNAMENT_RANKING]
    ADD [WIN_COUNT] int null
GO