SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[external_mpt].[USER_CONF]') = object_id and name = 'PROFESSION')
  BEGIN
    ALTER TABLE [external_mpt].[USER_CONF]
      ADD [PROFESSION] NVARCHAR(50) NULL
  END
GO