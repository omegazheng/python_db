SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[TOURNAMENT_CURRENCY]') and name = 'RATIO')
ALTER TABLE [admin_all].[TOURNAMENT_CURRENCY]
  ADD [RATIO] numeric(38,18) null
GO
