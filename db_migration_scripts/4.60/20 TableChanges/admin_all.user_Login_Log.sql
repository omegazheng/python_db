if not exists(select * from sys.columns where object_id = object_id('admin_all.user_login_log') and name = 'login_attempts')
	alter table admin_all.user_login_log add login_attempts integer not null
	    constraint DF_admin_all_User_Login_Log_login_attempts default 0