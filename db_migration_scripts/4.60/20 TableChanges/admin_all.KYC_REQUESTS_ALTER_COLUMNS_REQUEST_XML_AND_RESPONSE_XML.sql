
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[KYC_REQUESTS]') = object_id and name = 'REQUEST_XML')
  BEGIN
    ALTER TABLE [admin_all].[KYC_REQUESTS]
      ALTER COLUMN [REQUEST_XML] nvarchar(max)
  END
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[KYC_REQUESTS]') = object_id and name = 'RESPONSE_XML')
  BEGIN
    ALTER TABLE [admin_all].[KYC_REQUESTS]
    ALTER COLUMN [RESPONSE_XML] nvarchar(max)
  END
GO

