update x 
set BONUS_CODE = BONUS_CODE + '-' + cast(rn as varchar(10))
from (
		select 
				ID, BONUS_CODE, row_number() over(partition by BONUS_CODE order by ID asc) rn
		from admin_all.BONUS_PLAN
		where BONUS_CODE is not null
	) x
where x.rn > 1
go
if not exists(select * from  sys.indexes i where i.name = 'IDX_admin_all_BONUS_PLAN_BONUS_CODE_Filtered'and object_id = object_id('admin_all.BONUS_PLAN'))
begin
	create unique index IDX_admin_all_BONUS_PLAN_BONUS_CODE_Filtered on admin_all.BONUS_PLAN(BONUS_CODE) where BONUS_CODE is not null
end

go