SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'COUNT')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [COUNT] int null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'COUNT_CONDITION')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [COUNT_CONDITION] numeric(1) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'CUSTOM_FIELD_1_NAME')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [CUSTOM_FIELD_1_NAME] nvarchar(256) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'CUSTOM_FIELD_1')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [CUSTOM_FIELD_1] nvarchar(256) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'CUSTOM_FIELD_1_CONDITION')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [CUSTOM_FIELD_1_CONDITION] numeric(1) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'CUSTOM_FIELD_2_NAME')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [CUSTOM_FIELD_2_NAME] nvarchar(256) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'CUSTOM_FIELD_2')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [CUSTOM_FIELD_2] nvarchar(256) null
GO

if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BONUS_PLAN_SPORTSBOOK]') and name = 'CUSTOM_FIELD_2_CONDITION')
ALTER TABLE [admin_all].[BONUS_PLAN_SPORTSBOOK]
    ADD [CUSTOM_FIELD_2_CONDITION] numeric(1) null
GO


