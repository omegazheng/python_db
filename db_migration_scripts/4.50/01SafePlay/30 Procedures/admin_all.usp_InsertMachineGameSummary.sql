set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.usp_InsertMachineGameSummary') is null
	exec('create procedure admin_all.usp_InsertMachineGameSummary as --')
go
alter procedure admin_all.usp_InsertMachineGameSummary
(
	@PartyID int,
	@DateTime datetime,
	@MachineID int,
	@GameID varchar(100),
-- 	@ProductID int,
-- 	@GameInfoID int,
	@EFTCashWagered numeric(38,18),
	@EFTPromoWagered numeric(38,18),
	@GamesPlayed numeric(38,18),
	@Win numeric(38,18),
	@GameWon int,
	@EFTCashIn numeric(38,18),
	@EFTCashOut numeric(38,18),
	@EFTPromoIn numeric(38,18),
	@EFTPromoOut numeric(38,18),
	@TotalCashWagered numeric(38,18),
	@TotalPromoWagered numeric(38,18),
	@CoinsIn numeric(38,18),
	@CoinsOut numeric(38,18),
	@NotesIn numeric(38,18),
	@NotesOut numeric(38,18),
	@TicketIn numeric(38,18),
	@TicketOut numeric(38,18),
	@PromoTicketIn numeric(38,18),
	@PromoTicketOut numeric(38,18),
	@WinLoss numeric(38,18),
	@LoyaltyPoint numeric(38,18),
	@CurrencyCode varchar(10),
    @MachineTranID bigint = null,
    @EFTID bigint = null
)
as
begin
    set nocount, xact_abort on
    declare @ProductID int
    select @DateTime = isnull(@Datetime, getdate())
    begin transaction
        if @EFTID is not null
            begin
                select @MachineTranID = MachineTranID
                from admin_all.MachineTranEFT
                where EFTID = @EFTID
            end

        insert into admin_all.MachineGameSummary(PartyID, DateTime, MachineID, GameID, ProductID, GameInfoID, EFTCashWagered, EFTPromoWagered, GamesPlayed, Win, GameWon, EFTCashIn, EFTCashOut, EFTPromoIn, EFTPromoOut, TotalCashWagered, TotalPromoWagered, CoinsIn, CoinsOut, NotesIn, NotesOut, TicketIn, TicketOut, PromoTicketIn, PromoTicketOut, WinLoss, LoyaltyPoint, CurrencyCode, MachineTranID, EFTID)
            select @PartyID, @DateTime, @MachineID, @GameID, m.ProductID, gi.ID, isnull(@EFTCashWagered, 0), isnull(@EFTPromoWagered, 0), isnull(@GamesPlayed, 0), isnull(@Win, 0), isnull(@GameWon, 0), isnull(@EFTCashIn, 0), isnull(@EFTCashOut, 0), isnull(@EFTPromoIn, 0), isnull(@EFTPromoOut, 0), isnull(@TotalCashWagered, 0), isnull(@TotalPromoWagered, 0), isnull(@CoinsIn, 0), isnull(@CoinsOut, 0), isnull(@NotesIn, 0), isnull(@NotesOut, 0), isnull(@TicketIn, 0), isnull(@TicketOut, 0), isnull(@PromoTicketIn, 0), isnull(@PromoTicketOut, 0), isnull(@WinLoss, 0), isnull(@LoyaltyPoint, 0), isnull(@CurrencyCode, ''), @MachineTranID, @EFTID
            from admin_all.Machine m
                left outer join admin_all.GAME_INFO gi on gi.PLATFORM_ID = m.ProductID and gi.GAME_ID = @GameID
         where m.MachineID = @MachineID
        if isnull(@LoyaltyPoint, 0) <> 0
        begin
              select @ProductID = ProductID
              from admin_all.Machine
              where MachineID = @MachineID
              if @@rowcount = 0
               begin
                 raiserror('Could not find Machine', 16, 1)
                 rollback
                 return
               end
            exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = 0, @ReleasedBonus = 0,  @PlayableBonus = 0, @TranType = 'MACHIN_LOY',
                                                        @PlatformID = @ProductID, @MachineID = @MachineID,
                                                        @AmountRawLoyalty= @LoyaltyPoint
            end
    commit
end
go
--begin transaction
--exec admin_all.usp_InsertMachineGameSummary 1, '2019-01-01',312, 'STORM147',1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'usd', 2,2
--select * from admin_all.MachineGameSummary
--rollback
