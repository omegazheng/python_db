
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('[admin_all].[usp_GetUserReviewReport]') is null
	exec('create procedure [admin_all].[usp_GetUserReviewReport] as --')
go
alter procedure [admin_all].[usp_GetUserReviewReport]
(
	@PartyID int = null,
	@startDate DATETIME = null,
	@endDate DATETIME = null,
	@brandIds nvarchar(3000) = null,
	@statuses nvarchar(1000) = null,
	@Tags varchar(max)= null
)
as
begin

	declare @filterByCompleteDay Bit;

	if exists(select RTRIM(LTRIM(Item)) STATUS from admin_all.fc_splitDelimiterString(@statuses, ',')  where Item = 'Completed')
		begin
			SET @filterByCompleteDay = 1;
		end
	else
		begin
			SET @filterByCompleteDay = 0;
		end

	print 'Is Complete Day used as filter?';
	print @filterByCompleteDay;

	create table #BRANDS (ID int primary key);
    create table #STATUSES (STATUS nvarchar(15) primary key with (ignore_dup_key=on));
	create table #tags (ID int  primary key with (ignore_dup_key=on))

    insert into #BRANDS select cast(Item as int) ID from admin_all.fc_splitDelimiterString(@brandIds, ',');
    insert into #STATUSES(STATUS) select RTRIM(LTRIM(Item)) STATUS from admin_all.fc_splitDelimiterString(@statuses, ',');
	insert into #tags(ID)
		select id 
		from admin_all.COMMENT_TAG 
		where CODE in (select item from admin_all.fc_splitDelimiterString(@Tags, ','))

		declare @sql nvarchar(max)
		select @SQL = '
		;with users as
		(
			select u.partyid, u.userid, u.ACTIVE_FLAG activeFlag, u.KYC_STATUS kycStatus
			from external_mpt.USER_CONF u 
			'+ case when exists(select * from #BRANDS) then '	inner join #BRANDS b on u.BRANDID = b.ID ' else '' end +'
			'+ case when @PartyID is not null then ' where u.PARTYID = @PartyID ' else '' end +'
		),
		reviews as
		(
			select 
					r.id, r.partyid, r.status reviewStatus,
					uc1.DATE requestDate, r.SCHEDULE_DATE scheduleDate, uc2.DATE completeDate,
					uc1.COMMENT requestComment, uc2.COMMENT reviewComment,
					reqStaff.LOGINNAME requestStaff, revStaff.LOGINNAME reviewStaff,
					admin_all.fn_GetUserCommentTags(uc1.ID, uc2.ID) tags
			from admin_all.USER_REVIEW r
				left join admin_all.USER_COMMENTS uc1 on r.REQUEST_COMMENT_ID = uc1.ID
				left join admin_all.USER_COMMENTS uc2 on r.REVIEW_COMMENT_ID = uc2.ID
				'+case when exists(select * from #STATUSES) then ' inner join #STATUSES s on r.STATUS = s.STATUS' else '' end +'
				left join admin_all.STAFF_AUTH_TBL reqStaff on uc1.STAFFID = reqStaff.STAFFID
				left join admin_all.STAFF_AUTH_TBL revStaff on uc2.STAFFID = revStaff.STAFFID
			where (1=1)
				'+case when @statuses is not null and @filterByCompleteDay=0 and @startDate is not null and @endDate is not null then ' and r.SCHEDULE_DATE between @startDate and @endDate ' else '' end + '
		        '+case when @statuses is not null and @filterByCompleteDay=1 and @startDate is not null and @endDate is not null then ' and uc2.DATE between @startDate and @endDate ' else '' end + '
				'+case when @startDate is not null and @endDate is not null then ' and (uc1.DATE between @startDate and @endDate) or (uc2.DATE between @startDate and @endDate)' else '' end + '
				'+ case when exists(select * from #tags) then 'and exists(select *
							from #tags tt
								inner join admin_all.USER_COMMENT_TAG ucg on ucg.COMMENT_TAG_ID = tt.ID
							where ucg.USER_COMMENTS_ID = isnull(uc1.ID, -100)
								or ucg.USER_COMMENTS_ID = isnull(uc2.ID, -100)
						)'  else '' end + '
		)
		select r.id, u.partyid, u.userid, u.activeFlag, u.kycStatus, r.reviewStatus,
		CAST(r.requestDate AS date) requestDate, CAST(r.scheduleDate AS date) scheduleDate, CAST(r.completeDate AS date) completeDate,
		r.requestComment, r.reviewComment,
		r.requestStaff, r.reviewStaff, r.tags
		from reviews r inner join users u on r.partyid = u.partyid' + ''+ case when @PartyID is not null then ' where u.PARTYID = @PartyID ' else '' end +'

		order by r.requestDate desc;
		'
	    print @SQL
	exec sp_executesql @SQL, N'@PartyID int, @startDate DATETIME, @endDate DATETIME', @PartyID, @startDate, @endDate
end

-- When status is Due or Scheduled, uc2.DATE is null,  both use schedule date to filter
-- When status is Completed == uc2.DATE is not null,  use complete date to filter
-- when status is null, on user review pop out table, use request date as