
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('admin_all.fn_GetUserCommentTags') is null
	exec('create function [admin_all].fn_GetUserCommentTags() returns varchar(max) as begin return  1 end')
go
alter function admin_all.fn_GetUserCommentTags(@UserCommentID1 int, @UserCommentID2 int)
returns varchar(max)
as
begin
	if @UserCommentID1 is null and @UserCommentID2 is null
		return null
	declare @s varchar(max) = ''
	select @s= @s+','+ ct.CODE
	from admin_all.COMMENT_TAG ct
	where exists(select *
				from  admin_all.USER_COMMENT_TAG uct
			    where uct.USER_COMMENTS_ID in(isnull(@UserCommentID2, @UserCommentID1))
				    -- When there is reviewComment, use the tags from review Comment
				 and ct.ID = uct.COMMENT_TAG_ID)
	order by ct.CODE
	return stuff(@s, 1, 1, '')
end