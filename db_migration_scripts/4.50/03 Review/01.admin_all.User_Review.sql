/*select * from admin_all.USER_COMMENT_TAG
select * from admin_all.USER_REVIEW
select * from admin_all.USER_COMMENTS
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists(select * from sys.columns where object_id = object_id('admin_all.USER_REVIEW') and name = 'REQUEST_COMMENT_ID')
	alter table admin_all.USER_REVIEW add REQUEST_COMMENT_ID int
if not exists(select * from sys.columns where object_id = object_id('admin_all.USER_REVIEW') and name = 'REVIEW_COMMENT_ID')
	alter table admin_all.USER_REVIEW add REVIEW_COMMENT_ID int
go
declare @s nvarchar(max)=''

select @s = @s + 'alter table [admin_all].[USER_REVIEW] drop constraint ' + quotename(name)+';' from sys.foreign_keys where parent_object_id = object_id('[admin_all].[USER_REVIEW]') and referenced_object_id = object_id('admin_all.STAFF_AUTH_TBL')
exec(@s)
go

if exists(select * from sys.indexes where name ='IDX_admin_all_USER_REVIEW_REVIEW_STAFFID' and object_id=object_id('admin_all.USER_REVIEW'))
    drop index IDX_admin_all_USER_REVIEW_REVIEW_STAFFID on admin_all.USER_REVIEW
go

if exists(select * from sys.indexes where name ='IDX_admin_all_USER_REVIEW_REQUEST_STAFFID' and object_id=object_id('admin_all.USER_REVIEW'))
    drop index IDX_admin_all_USER_REVIEW_REQUEST_STAFFID on admin_all.USER_REVIEW
go

if not exists(select * from sys.columns where object_id = object_id('admin_all.USER_REVIEW') and name = 'COMPLETE_DATE')
	return

begin
    --declare @sql nvarchar(1000)
    declare @sql nvarchar(max)=''
--SET @sql = N'
    SET @sql =@sql +'
set xact_abort, nocount on
begin transaction
    print ''transaction''
    declare @rc int, @r int, @i int
    declare c cursor local for
        select ID from admin_all.USER_REVIEW with(tablockx) order by ID asc
    open c
    fetch next from c into @i
    while @@fetch_status = 0
    begin
        insert into admin_all.USER_COMMENTS with(tablockx)(DATE, STAFFID, COMMENT, PARTYID)
        select REQUEST_DATE, REQUEST_STAFFID, REQUEST_COMMENT, PARTYID
        from admin_all.USER_REVIEW
        where ID = @i
          and REQUEST_DATE is not null
          and REQUEST_STAFFID is not null
          and REQUEST_COMMENT is not null
          and PARTYID is not null
        select @rc = @@rowcount, @r = scope_identity()
        if @rc > 0
            update admin_all.USER_REVIEW  set REQUEST_COMMENT_ID = @r where id = @i


        insert into admin_all.USER_COMMENTS with(tablockx)(DATE, STAFFID, COMMENT, PARTYID)
        select COMPLETE_DATE, REVIEW_STAFFID, REVIEW_COMMENT, PARTYID
        from admin_all.USER_REVIEW
        where ID = @i
          and COMPLETE_DATE is not null
          and REVIEW_STAFFID is not null
          and REVIEW_COMMENT is not null
          and PARTYID is not null
        select @rc = @@rowcount, @r = scope_identity()
        if @rc > 0
            update admin_all.USER_REVIEW  set REVIEW_COMMENT_ID = @r where id = @i

        fetch next from c into @i
    end
    close c
    deallocate c

    alter table [admin_all].[USER_REVIEW]  drop column COMPLETE_DATE
    alter table [admin_all].[USER_REVIEW]  drop column REVIEW_STAFFID
    alter table [admin_all].[USER_REVIEW]  drop column REVIEW_COMMENT
    alter table [admin_all].[USER_REVIEW]  drop column REQUEST_DATE
    alter table [admin_all].[USER_REVIEW]  drop column REQUEST_STAFFID
    alter table [admin_all].[USER_REVIEW]  drop column REQUEST_COMMENT

    ALTER TABLE [admin_all].[USER_REVIEW]  ADD  CONSTRAINT [FK_admin_all_USER_REVIEW_USER_COMMENTS_REQUEST_COMMENT_ID] FOREIGN KEY(REQUEST_COMMENT_ID) REFERENCES admin_all.USER_COMMENTS (ID)
    ALTER TABLE [admin_all].[USER_REVIEW]  ADD  CONSTRAINT [FK_admin_all_USER_REVIEW_USER_COMMENTS_REQUEST_REVIEW_COMMENT_ID] FOREIGN KEY(REVIEW_COMMENT_ID) REFERENCES admin_all.USER_COMMENTS (ID)
    create index IDX_admin_all_USER_REVIEW_REQUEST_COMMENT_ID on [admin_all].[USER_REVIEW](REQUEST_COMMENT_ID)
    create index IDX_admin_all_USER_REVIEW_REVIEW_COMMENT_ID on [admin_all].[USER_REVIEW](REVIEW_COMMENT_ID)

commit'
EXEC sys.sp_executesql @sql


--select * from [admin_all].[USER_REVIEW]
end
go


