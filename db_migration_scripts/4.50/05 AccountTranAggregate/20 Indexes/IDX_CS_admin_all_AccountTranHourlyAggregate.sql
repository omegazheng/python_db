if not exists(select * from sys.indexes where object_id = object_id('admin_all.AccountTranHourlyAggregate') and name = 'IDX_CS_admin_all_AccountTranHourlyAggregate')
begin
	create nonclustered columnstore index IDX_CS_admin_all_AccountTranHourlyAggregate on admin_all.AccountTranHourlyAggregate(AggregateType, Datetime, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, GameCount, TranCount) on PS_AccountTranHourlyAggregate(AggregateType)
end