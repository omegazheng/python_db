set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.AccountTranAggregatePendingCalculation') is null
begin
	create table admin_all.AccountTranAggregatePendingCalculation
	(
		Period char(1) not null, -- Day, Month, Year
		Date date not null,
		constraint PK_admin_all_AccountTranAggregatePendingCalculation primary key(Period, Date)WITH (ignore_dup_key = on)
	)
end

if not exists(select * from admin_all.AccountTranAggregate)
begin
	insert into admin_all.AccountTranAggregatePendingCalculation(Period, Date)
		select distinct 'D', cast(datetime as date) from admin_all.AccountTranHourlyAggregate
		union all
		select distinct 'M', datefromparts (year(Datetime), month(Datetime), 1) from admin_all.AccountTranHourlyAggregate
		union all
		select distinct 'Y', datefromparts (year(Datetime), 1, 1) from admin_all.AccountTranHourlyAggregate
end