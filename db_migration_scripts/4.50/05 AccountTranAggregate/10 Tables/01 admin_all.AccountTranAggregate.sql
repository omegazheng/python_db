set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.AccountTranAggregate') is null
begin
	create table admin_all.AccountTranAggregate
	(
		Period char(1) not null, -- Day, Month, Year
		AggregateType tinyint NOT NULL,
		Date date NOT NULL,
		BrandID int NOT NULL,
		PartyID int NOT NULL,
		Currency nchar(3) NULL,
		TranType varchar(10) NOT NULL,
		ProductID int NOT NULL,
		GameID varchar(100) NOT NULL,
		AmountReal numeric(38, 18) NOT NULL,
		AmountReleasedBonus numeric(38, 18) NOT NULL,
		AmountPlayableBonus numeric(38, 18) NOT NULL,
		GameCount int NOT NULL,
		TranCount int NOT NULL,
		constraint PK_admin_all_AccountTranAggregate primary key(Period, Date, AggregateType, TranType, PartyID, ProductID,GameID ASC)WITH (Data_compression = Page)
	)
end

if not exists(select * from sys.indexes where object_id = object_id('admin_all.AccountTranAggregate') and name = 'IDX_CS_admin_all_AccountTranAggregate')
begin
	create nonclustered columnstore index IDX_CS_admin_all_AccountTranAggregate on admin_all.AccountTranAggregate(Period,AggregateType, Date, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, GameCount, TranCount)
end