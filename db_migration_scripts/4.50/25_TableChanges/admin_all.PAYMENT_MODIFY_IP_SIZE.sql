SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[PAYMENT]') = object_id and name = 'IP_ADDRESS')
  BEGIN
    ALTER TABLE [admin_all].[PAYMENT]
    ALTER COLUMN [IP_ADDRESS] VARCHAR(100)
  END
GO
