if exists (select * from dbo.CUSTOMER where name in ('YYY', 'DEMO'))
begin

  if not exists(select *
                from admin_all.REGISTRY_HASH
                where MAP_KEY = 'digitain_sportbook.gameMobileBaseLink')
    BEGIN
      insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
      VALUES ('digitain_sportbook.gameMobileBaseLink', 'TBD');
    END

  if not exists(select *
                from admin_all.REGISTRY_HASH
                where MAP_KEY = 'digitain.sportbook.container.id')
    BEGIN
      insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
      VALUES ('digitain.sportbook.container.id', 'TBD');
    END

  if not exists(select *
                from admin_all.REGISTRY_HASH
                where MAP_KEY = 'digitain.sportbook.hash.router.disabled')
    BEGIN
      insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
      VALUES ('digitain.sportbook.hash.router.disabled', 'TBD');
    END

end
