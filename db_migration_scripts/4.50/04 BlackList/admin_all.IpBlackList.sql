SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(SELECT *
              FROM sys.objects
              WHERE object_id = OBJECT_ID(N'[admin_all].[IP_BLACKLIST]')
                AND type in (N'U'))
  BEGIN
    CREATE TABLE [admin_all].[IP_BLACKLIST] (
      [ID]       [int] IDENTITY (1, 1) NOT NULL,
      [IP]       [varchar](48)         NOT NULL,
      [IP_END]   [varchar](48),
      [BRAND_ID] [int],
      [COUNTRY]  [char](2),
      [REFERENCE] [nvarchar](1000)
      CONSTRAINT [PK_admin_all_IP_BLACKLIST] PRIMARY KEY CLUSTERED
        (
          [ID] ASC
        )
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY],
      CONSTRAINT [UQ_admin_all_IP_BLACKLIST_IP_BRAND_ID] UNIQUE NONCLUSTERED
        (
          [IP] ASC,
          [BRAND_ID] ASC
        )
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY]
    ) ON [PRIMARY]
  END
GO
IF NOT EXISTS(SELECT *
              FROM sys.foreign_keys
              WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_IP_BLACKLIST_BRAND_ID]')
                AND parent_object_id = OBJECT_ID(N'[admin_all].[IP_BLACKLIST]'))
  ALTER TABLE [admin_all].[IP_BLACKLIST]
    WITH CHECK ADD CONSTRAINT [FK_admin_all_IP_BLACKLIST_BRAND_ID] FOREIGN KEY ([BRAND_ID])
  REFERENCES [admin].[CASINO_BRAND_DEF] ([BRANDID])
GO

IF NOT EXISTS(SELECT *
              FROM sys.foreign_keys
              WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_IP_BLACKLIST_COUNTRY]')
                AND parent_object_id = OBJECT_ID(N'[admin_all].[IP_BLACKLIST]'))
  ALTER TABLE [admin_all].[IP_BLACKLIST]
    WITH CHECK ADD CONSTRAINT [FK_admin_all_IP_BLACKLIST_COUNTRY] FOREIGN KEY ([COUNTRY])
  REFERENCES [admin_all].[COUNTRY] ([ISO2_CODE])
GO

if not exists(select * from sys.indexes where name ='IDX_admin_all_IP_BLACKLIST_BRAND_ID' and object_id=object_id('admin_all.IP_BLACKLIST'))
	create index IDX_admin_all_IP_BLACKLIST_BRAND_ID on admin_all.IP_BLACKLIST(BRAND_ID)
go

if not exists(select * from sys.indexes where name ='IDX_admin_all_IP_BLACKLIST_COUNTRY' and object_id=object_id('admin_all.IP_BLACKLIST'))
	create index IDX_admin_all_IP_BLACKLIST_COUNTRY on admin_all.IP_BLACKLIST(COUNTRY)
go