set ansi_nulls on
go
set quoted_identifier on
go
if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'croatia.usp_GetTransactionSummary')
      AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure croatia.usp_GetTransactionSummary
go

if exists(select * from sys.schemas where name = 'croatia')
begin
    exec('CREATE procedure croatia.usp_GetTransactionSummary
    (
        @DateFrom date,
        @DateTo date
    )
    as
    begin
        set nocount on
        select
            sum(case when AmountReal > 0 then AmountReal else 0 end) PayOutAmountReal,
            sum(case when AmountReal < 0 then -AmountReal else 0 end) PayInAmountReal,
            sum(case when AmountPlayableBonus > 0 then AmountPlayableBonus else 0 end) PayOutAmountPlayableBonus,
            sum(case when AmountPlayableBonus < 0 then -AmountPlayableBonus else 0 end) PayInAmountPlayableBonus,
            sum(case when AmountReleasedBonus > 0 then AmountReleasedBonus else 0 end) PayOutAmountReleasedBonus,
            sum(case when AmountReleasedBonus < 0 then -AmountReleasedBonus else 0 end) PayInAmountReleasedBonus,
            sum(case when TranType = ''TRANSF_IN'' then AmountReal else 0 end) TransferInAmountReal,
            sum(case when TranType = ''TRANSF_OUT'' then -AmountReal else 0 end) TransferOutAmountReal,
            sum(case when TranType = ''TRANSF_IN'' then AmountPlayableBonus else 0 end) TransferInAmountPlayableBonus,
            sum(case when TranType = ''TRANSF_OUT'' then -AmountPlayableBonus else 0 end) TransferOutAmountPlayableBonus,
            sum(case when TranType = ''TRANSF_IN'' then AmountReleasedBonus else 0 end) TransferInAmountReleasedBonus,
            sum(case when TranType = ''TRANSF_OUT'' then -AmountReleasedBonus else 0 end) TransferOutAmountReleasedBonus,
            sum(TranCount) TranCount,
            Currency
        from admin_all.AccountTranHourlyAggregate
        where Datetime >= cast(@DateFrom as datetime)
            and Datetime < cast(@DateTo as datetime)
        group by Currency
        order by Currency
    end')
end
go