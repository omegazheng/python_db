SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
        SELECT *
        FROM DBO.SYSOBJECTS
        WHERE ID =
              OBJECT_ID(N'[admin_all].[usp_UpdateSignupConfirmationCode]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
    DROP PROCEDURE [admin_all].[usp_UpdateSignupConfirmationCode]
GO

CREATE PROCEDURE admin_all.usp_UpdateSignupConfirmationCode
(
    @PartyID INT,
    @ConfirmationCode NVARCHAR(255),
    @result INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT, xact_abort ON
    UPDATE external_mpt.USER_CONF
    SET CONFIRMATION_CODE= CONCAT('CONFIRMED-', CONFIRMATION_CODE)
    where PARTYID = @PartyID and CONFIRMATION_CODE = @ConfirmationCode
    select @result = @@rowcount

END
