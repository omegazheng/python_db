set ansi_nulls on
go
set quoted_identifier on
go

if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'croatia.usp_GetFilteredTransactionSummary')
      AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure croatia.usp_GetFilteredTransactionSummary
go

if exists(select * from sys.schemas where name = 'croatia')
begin
exec('CREATE procedure croatia.usp_GetFilteredTransactionSummary
    (
        @DateFrom date,
        @DateTo date,
        @TransactionTypes varchar(8000),
        @PartyId int,
        @PayOutThreshold numeric(38,18),
        @PayInThreshold numeric(38,18),
        @OrderBy varchar(255)
    )
    as
    begin
        set nocount on
        select top 10 *
        from
            (select
                 PartyID,
                 Currency,
                 sum(case when TranType in(''GAME_WIN'', ''TRANSF_IN'') then AmountReal else 0 end) PayInAmountReal,
                 sum(case when TranType in(''GAME_BET'', ''TRANSF_OUT'') then -AmountReal else 0 end) PayOutAmountReal,
                 sum(case when TranType in(''GAME_WIN'', ''TRANSF_IN'') then AmountPlayableBonus else 0 end) PayInAmountPlayableBonus,
                 sum(case when TranType in(''GAME_BET'', ''TRANSF_OUT'') then -AmountPlayableBonus else 0 end) PayOutAmountPlayableBonus,
                 sum(case when TranType in(''GAME_WIN'', ''TRANSF_IN'') then AmountReleasedBonus else 0 end) PayInAmountReleasedBonus,
                 sum(case when TranType in(''GAME_BET'', ''TRANSF_OUT'') then -AmountReleasedBonus else 0 end) PayOutAmountReleasedBonus,
                 sum(TranCount) as TranCount
             from admin_all.AccountTranHourlyAggregate
             where Datetime >= cast(@DateFrom as datetime)
               and Datetime < cast(@DateTo as datetime)
               and TranType in (select Item from admin_all.fc_splitDelimiterString(@TransactionTypes, '',''))
               and ((@PartyId is null) or (PartyID = @PartyId))
             group by PartyID, Currency) allPayInOuts
        where PayOutAmountPlayableBonus + PayOutAmountReleasedBonus + PayOutAmountReal >= @PayOutThreshold
          and PayInAmountPlayableBonus + PayInAmountReleasedBonus + PayInAmountReal >= @PayInThreshold
        order by case when @OrderBy=''PayIn'' then PayInAmountReal end desc,
                 case when @OrderBy=''PayOut'' then PayOutAmountReal end desc
    end')
end
go