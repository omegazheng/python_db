set ansi_nulls, quoted_identifier on
if object_id('admin_all.usp_RemoveUser') is null
	exec('create procedure admin_all.usp_RemoveUser as --')
go
alter procedure admin_all.usp_RemoveUser @PartyID int = null
as
begin
	set xact_abort, nocount on

	--select u.PARTYID, u.ParentID, Hierarchy, Hierarchy.GetLevel()
	--from external_mpt.USER_CONF u
	--	where u.partyid   in (1654733, 1650262, 1648450)
	begin transaction
	--select object_schema_name(object_id)+'.'+object_name(object_id) TableName, sum(rows) RowsBeforeDelete
	--	into #1
	--from sys.partitions
	--where object_schema_name(object_id) not in ('sys','dbo')
	--	and index_id in(0,1)
	--group by object_schema_name(object_id)+'.'+object_name(object_id)


	declare @AccountID int
	declare c cursor local static for
		with a as(
			select b.PARTYID, b.id, 1 as l, b.BALANCE_REAL+b.RELEASED_BONUS Balance, u.user_type
			from external_mpt.USER_CONF u
				inner join admin_all.ACCOUNT b  on b.PARTYID = u.PARTYID
			where u.ParentID is null --and u.PARTYID <> 1648450
			union all
			select b.PARTYID, b.id, a.l+ 1,b.BALANCE_REAL+b.RELEASED_BONUS Balance, u.user_type
			from a
				inner join external_mpt.USER_CONF u on u.ParentID = a.PARTYID
				inner join admin_all.ACCOUNT b  on b.PARTYID = u.PARTYID

		)

		select a.PARTYID, a.id--, user_type
		from a
		where --partyid  not in (1654733, 1650262, 1648450)
			--and
			Balance = 0 --and (isnull(user_type,0) = 0)
			and
			(@PartyID is null or PARTYID = @PartyID)
		order by l desc


	--declare c cursor local static for
	--	select a.PARTYID, a.id
	--	from external_mpt.USER_CONF u
	--		inner join admin_all.ACCOUNT a  on a.PARTYID = u.PARTYID
	--	where u.partyid  not in (1654733, 1650262, 1648450)
	--	order by u.Level desc
	open c
	fetch next from c into @PartyID, @AccountID
	while @@fetch_status = 0
	begin
	--print @PartyID
		--select top 10 * from admin_all.BONUS_PLAN_INCLUDED_USER
		--if @PartyID = 1648452
		--begin
		--	select level, ParentID,Hierarchy,* from external_mpt.USER_CONF where partyid = @PartyID
		--	select level, ParentID,Hierarchy, * from external_mpt.USER_CONF where ParentID = @PartyID
		--end
		delete a
		from admin_all.ACCOUNT_TRAN_REALTIME a
		where ACCOUNT_ID = @AccountID

		delete a
		from admin_all.ACCOUNT_TRAN_ALL a
		where ACCOUNT_ID = @AccountID

		delete a
		from admin_all.WITHDRAWALS a
			inner join admin_all.PAYMENT b on a.PAYMENT_ID = b.id
		where b.ACCOUNT_ID = @AccountID

		delete a
		from admin_all.PaymentCredentialPayment a
			inner join admin_all.PAYMENT b on a.PaymentID = b.id
		where b.ACCOUNT_ID = @AccountID

		delete a
		from admin_all.PaymentCredential a
		where a.AccountID = @AccountID

		delete a
		from admin_all.WITHDRAWAL_REQUEST a
			inner join admin_all.PAYMENT b on a.PAYMENT_ID = b.id
		where b.ACCOUNT_ID = @AccountID

		delete a
		from admin_all.PAYMENT_ROLLBACK a
			inner join admin_all.PAYMENT b on a.PAYMENT_ID = b.id
		where b.ACCOUNT_ID = @AccountID

		delete c
		from admin_all.BONUS b
			--inner join admin_all.BONUS_TRAN a on a.BONUS_ID = b.ID
			inner join admin_all.BONUS_TRAN_UPDATER_STATUS c on c.BONUS_ID = b.ID
		where b.PARTYID = @PartyID

		delete a
		from admin_all.BONUS b
			inner join admin_all.BONUS_HISTORY a on a.BONUS_ID = b.ID
		where b.PARTYID = @PartyID

		delete a
		from admin_all.BONUS b
			inner join admin_all.BONUS_TRAN a on a.BONUS_ID = b.ID
		where b.PARTYID = @PartyID

		delete a
		from admin_all.BONUS b
			inner join admin_all.GAME_BONUS_BUCKET a on a.BONUS_ID = b.ID
		where b.PARTYID = @PartyID

		delete a
		from admin_all.BONUS a
		where PARTYID = @PartyID

		delete a
		from admin_all.PAYMENT a
		where ACCOUNT_ID = @AccountID

		delete a
		from admin_all.PAYMENT_TOKEN a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.RECENT_USERS a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_COMMISSION a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_SESSION a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_COMMENTS a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_PLATFORM a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_FAVORITE_GAME a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_WEB_SESSION a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_REGISTRY a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_ACTION_LOG a
		where a.PARTYID = @PartyID


		delete a
		from admin_all.DEPOSIT_LIMIT_PER_ACCOUNT a
		where a.ACCOUNT_ID = @AccountID

		delete a
		from admin_all.ACCOUNT a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.BONUS_PLAN_INCLUDED_USER a
		where a.PARTY_ID = @PartyID

		delete a
		from admin_all.USER_DOCUMENTS a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.USER_LOGIN_LOG a
		where a.partyid = @PartyID;

		delete a
		from admin_all.USER_LIMIT a
		where a.partyid = @PartyID;

		delete a
		from admin_all.MachineTranEFT a
			inner join admin_all.MachineTran b on a.MachineTranID = b.MachineTranID
		where b.PARTYID = @PartyID

		delete a
		from admin_all.MachineTran a
		where a.partyid = @PartyID;

		delete a
		from admin_all.KYC_HISTORY a
		where a.PARTYID = @PartyID

		delete a
		from admin_all.KYC_REQUESTS a
		where a.PARTYID = @PartyID

		delete u
		from external_mpt.user_conf u
		where PARTYID = @PartyID;

		delete a
		from admin_all.DAILY_BALANCE_HISTORY a
		where a.ACCOUNT_ID = @AccountID

		delete a
		from admin_all.DataFeed a
		where a.PartyID = @PartyID

		delete a
		from admin_all.AccountTranHourlyAggregate a
		where a.PartyID = @PartyID

		delete a
		from admin_all.GameWinnerList a
		where a.PartyID = @PartyID

		--delete a
		--from admin_all.DW_ACCOUNT_TRAN_DAILY a
		--where a.Party_ID = @PartyID

		delete a
		from admin_all.DW_GAME_PLAYER_DAILY a
		where a.Party_ID = @PartyID

		delete a
		from external_mpt.UserBrandCurrency a
		where a.PartyID = @PartyID

		delete a
		from external_mpt.UserBrandCurrency a
		where a.PartyID = @PartyID

		delete a
		from BonusPlan.BonusAccountTran a
		where a.PartyID = @PartyID


		delete a
		from agency.direct_ggr  a
		where a.agent_id = @PartyID

		delete a
		from agency.network_ggr  a
		where a.agent_id = @PartyID

		delete a
		from agency.player_ggr  a
		where a.agent_id = @PartyID

		delete a
		from agency.commission_payment  a
		where a.agent_id = @PartyID

		delete a
		from agency.potential_commission  a
		where a.agent_id = @PartyID

		delete a
		from agency.potential_commission  a
		where a.agent_id = @PartyID
	___NEXT___:
		fetch next from c into @PartyID, @AccountID
	end
	close c
	deallocate c


	--select a.*, b.RowsAfterDelete, a.RowsBeforeDelete - b.RowsAfterDelete Diff
	--from #1 a
	--	inner join (
	--					select object_schema_name(object_id)+'.'+object_name(object_id) TableName, sum(rows) RowsAfterDelete
	--					from sys.partitions p
	--					where object_schema_name(object_id) not in ('sys','dbo')
	--						and index_id in(0,1)
	--					group by object_schema_name(object_id)+'.'+object_name(object_id)
	--	) b on a.TableName = b.TableName
	--order by 1
	--? select * from  admin_all.DAILY_BIGGESTWIN
	exec  admin_all.usp_fixBonusInconsistency
	commit



--select 'delete '+object_schema_name(object_id)+'.'+object_name(object_id) + '  ','select * from '+object_schema_name(object_id)+'.'+object_name(object_id) , rows
--from sys.partitions where object_schema_name(object_id) not in ('sys','dbo') and index_id in(0,1) and rows > 0 order by rows desc


end
go

--exec admin_all.usp_RemoveUser @PartyID =  1000032
--select * from external_mpt.USER_CONF where PARTYID = 1648681
--select * from admin_all.ACCOUNT where PARTYID = 1648681
