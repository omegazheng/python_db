if object_id('[admin_all].[FK_admin_all_Machine_PreviousMachineID_Machine]') is null
	alter table [admin_all].[Machine] add constraint [FK_admin_all_Machine_PreviousMachineID_Machine] foreign key ([PreviousMachineID]) references [admin_all].[Machine]([MachineID]);