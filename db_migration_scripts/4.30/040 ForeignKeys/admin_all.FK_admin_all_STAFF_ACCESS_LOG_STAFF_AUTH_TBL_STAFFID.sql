if object_id('[admin_all].[FK_admin_all_STAFF_ACCESS_LOG_STAFF_AUTH_TBL_STAFFID]') is null
	alter table [admin_all].[STAFF_ACCESS_LOG] add constraint [FK_admin_all_STAFF_ACCESS_LOG_STAFF_AUTH_TBL_STAFFID] foreign key ([STAFFID]) references [admin_all].[STAFF_AUTH_TBL]([STAFFID]) on delete cascade;

--	alter table [admin_all].[STAFF_ACCESS_LOG] drop constraint [FK_admin_all_STAFF_ACCESS_LOG_STAFF_AUTH_TBL_STAFFID] 