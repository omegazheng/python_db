SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[POKER_MTT_LIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[POKER_MTT_LIST](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mttId] [bigint] NOT NULL,
	[brandid] [int] NULL,
	[name] [varchar](100) NULL,
	[isRealMoney] [smallint] NULL,
	[betLimit] [varchar](20) NULL,
	[buyInAmount] [numeric](38, 18) NULL,
	[buyInType] [smallint] NULL,
	[feeAmount] [numeric](38, 18) NULL,
	[maxPlayers] [int] NULL,
	[registeredPlayers] [int] NULL,
	[status] [varchar](20) NULL,
	[mttLevel] [int] NULL,
	[gameType] [varchar](50) NULL,
	[startDate] [datetime] NULL,
	[isPrivate] [smallint] NULL,
	[currencyId] [int] NULL,
 CONSTRAINT [PK_admin_all_POKER_MTT_LIST] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
