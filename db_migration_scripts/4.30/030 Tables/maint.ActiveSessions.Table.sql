SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[ActiveSessions]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[ActiveSessions](
	[SnapshotDate] [datetime] NOT NULL,
	[SessionID] [smallint] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[BlockingSessionID] [smallint] NULL,
	[DurationInSecond] [int] NULL,
	[WaitType] [nvarchar](60) NULL,
	[WaitTime] [int] NOT NULL,
	[WaitResource] [nvarchar](256) NOT NULL,
	[LastWaitType] [nvarchar](60) NOT NULL,
	[CPU] [int] NOT NULL,
	[Reads] [bigint] NOT NULL,
	[Writes] [bigint] NOT NULL,
	[LogicalReads] [bigint] NOT NULL,
	[TotalElapsedTime] [int] NOT NULL,
	[SQLText] [nvarchar](max) NULL,
	[QueryPlan] [xml] NULL,
	[GrantedMemory] [int] NOT NULL,
	[NestedLevel] [int] NOT NULL,
	[RowCount] [bigint] NOT NULL,
	[TransactionIsolationLevel] [smallint] NOT NULL,
	[ExecutingManagedCode] [bit] NOT NULL,
 CONSTRAINT [PK_maint_ActiveSessions] PRIMARY KEY CLUSTERED 
(
	[SnapshotDate] ASC,
	[SessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
