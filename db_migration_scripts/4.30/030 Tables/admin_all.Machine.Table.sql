SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[Machine]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[Machine](
	[MachineID] [int] NOT NULL,
	[PreviousMachineID] [int] NULL,
	[LocationID] [int] NOT NULL,
	[CategoryID] [int] NULL,
	[ProductID] [int] NULL,
	[ManufacturerID] [int] NULL,
	[Datetime] [datetime] NOT NULL,
	[HostNumber] [nvarchar](50) NULL,
	[SerialNumber] [nvarchar](50) NULL,
	[PromoCreditSupported] [int] NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_admin_all_Machine] PRIMARY KEY CLUSTERED 
(
	[MachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[Machine]') AND name = N'IDX_admin_all_Machine_CategoryID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_Machine_CategoryID] ON [admin_all].[Machine]
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[Machine]') AND name = N'IDX_admin_all_Machine_LocationID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_Machine_LocationID] ON [admin_all].[Machine]
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[Machine]') AND name = N'IDX_admin_all_Machine_ManufacturerID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_Machine_ManufacturerID] ON [admin_all].[Machine]
(
	[ManufacturerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[Machine]') AND name = N'IDX_admin_all_Machine_PreviousMachineID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_Machine_PreviousMachineID] ON [admin_all].[Machine]
(
	[PreviousMachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[Machine]') AND name = N'IDX_admin_all_Machine_ProductID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_Machine_ProductID] ON [admin_all].[Machine]
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_Machine_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[Machine] ADD  CONSTRAINT [DF_admin_all_Machine_IsActive]  DEFAULT ((1)) FOR [IsActive]
END
GO
