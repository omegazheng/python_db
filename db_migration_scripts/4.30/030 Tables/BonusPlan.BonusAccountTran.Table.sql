SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BonusPlan].[BonusAccountTran]') AND type in (N'U'))
BEGIN
CREATE TABLE [BonusPlan].[BonusAccountTran](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[BonusID] [int] NOT NULL,
	[PartyID] [int] NOT NULL,
	[BrandID] [int] NOT NULL,
	[DateTime] [datetime2](3) NOT NULL,
	[TranType] [nvarchar](10) NULL,
	[AmountPlayableBonus] [numeric](38, 18) NULL,
	[AmountPlayableBonusWinnings] [numeric](38, 18) NULL,
	[AmountReleasedBonus] [numeric](38, 18) NULL,
	[AmountReleasedBonusWinnings] [numeric](38, 18) NULL,
	[AmountWageredContribution] [numeric](38, 18) NULL,
	[BalancePlayableBonus] [numeric](38, 18) NULL,
	[BalancePlayableBonusWinnings] [numeric](38, 18) NULL,
	[BalanceReleasedBonus] [numeric](38, 18) NULL,
	[BalanceReleasedBonusWinnings] [numeric](38, 18) NULL,
	[AccountTranId] [bigint] NULL,
 CONSTRAINT [PK_BonusPlan_BonusAccountTran] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[BonusPlan].[BonusAccountTran]') AND name = N'IDX_BonusPlan_BonusAccountTran_AccountTranId')
CREATE NONCLUSTERED INDEX [IDX_BonusPlan_BonusAccountTran_AccountTranId] ON [BonusPlan].[BonusAccountTran]
(
	[AccountTranId] ASC
)
INCLUDE ( 	[AmountPlayableBonus],
	[TranType]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[BonusPlan].[BonusAccountTran]') AND name = N'IDX_BonusPlan_BonusAccountTran_BonusID')
CREATE NONCLUSTERED INDEX [IDX_BonusPlan_BonusAccountTran_BonusID] ON [BonusPlan].[BonusAccountTran]
(
	[BonusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[BonusPlan].[BonusAccountTran]') AND name = N'IDX_BonusPlan_BonusAccountTran_BrandID_TranType_DateTime')
CREATE NONCLUSTERED INDEX [IDX_BonusPlan_BonusAccountTran_BrandID_TranType_DateTime] ON [BonusPlan].[BonusAccountTran]
(
	[BrandID] ASC,
	[TranType] ASC,
	[DateTime] ASC
)
INCLUDE ( 	[AmountReleasedBonusWinnings],
	[AmountReleasedBonus],
	[AmountPlayableBonus],
	[AmountPlayableBonusWinnings],
	[BonusID],
	[PartyID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[BonusPlan].[BonusAccountTran]') AND name = N'IDX_BonusPlan_BonusAccountTran_PartyID')
CREATE NONCLUSTERED INDEX [IDX_BonusPlan_BonusAccountTran_PartyID] ON [BonusPlan].[BonusAccountTran]
(
	[PartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[BonusPlan].[BonusAccountTran]') AND name = N'IDX_BonusPlan_BonusAccountTran_TranType_DateTime')
CREATE NONCLUSTERED INDEX [IDX_BonusPlan_BonusAccountTran_TranType_DateTime] ON [BonusPlan].[BonusAccountTran]
(
	[TranType] ASC,
	[DateTime] ASC
)
INCLUDE ( 	[PartyID],
	[AmountPlayableBonus],
	[AccountTranId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
