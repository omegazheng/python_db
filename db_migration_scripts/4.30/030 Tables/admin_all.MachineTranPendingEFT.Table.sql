SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTranPendingEFT]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MachineTranPendingEFT](
	[EFTID] [bigint] NOT NULL,
	[PartyID] [int] NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[AmountReal] [numeric](38, 18) NOT NULL,
	[ReleasedBonus] [numeric](38, 18) NOT NULL,
	[PlayableBonUs] [numeric](38, 18) NOT NULL,
	[AccountTranID] [bigint] NOT NULL,
 CONSTRAINT [PK_admin_all_MachineTranPendingEFT] PRIMARY KEY CLUSTERED 
(
	[EFTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_MachineTranPendingEFT_PartyID] UNIQUE NONCLUSTERED 
(
	[PartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachineTranPendingEFT_Date]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachineTranPendingEFT] ADD  CONSTRAINT [DF_admin_all_MachineTranPendingEFT_Date]  DEFAULT (getdate()) FOR [Datetime]
END
GO
