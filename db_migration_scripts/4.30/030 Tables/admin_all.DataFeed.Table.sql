SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DataFeed]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[DataFeed](
	[BrandID] [int] NOT NULL,
	[AccountTranID] [bigint] NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[PartyID] [int] NOT NULL,
	[UserID] [nvarchar](100) NOT NULL,
	[Currency] [nchar](3) NOT NULL,
	[ProductID] [int] NULL,
	[ProductCode] [nvarchar](20) NULL,
	[ProductTranID] [nvarchar](100) NULL,
	[GameInfoID] [int] NULL,
	[GameID] [varchar](100) NULL,
	[GameTranID] [nvarchar](100) NULL,
	[TranType] [varchar](10) NOT NULL,
	[AmountReal] [numeric](38, 18) NOT NULL,
	[AmountPlayableBonus] [numeric](38, 18) NOT NULL,
	[AmountReleasedBonus] [numeric](38, 18) NOT NULL,
	[BalanceReal] [numeric](38, 18) NOT NULL,
	[BalancePlayableBonus] [numeric](38, 18) NOT NULL,
	[BalanceReleasedBonus] [numeric](38, 18) NOT NULL,
	[RollbackTranID] [bigint] NULL,
	[RollbackTranType] [varchar](10) NULL,
 CONSTRAINT [PK_admin_all_DataFeed] PRIMARY KEY CLUSTERED 
(
	[BrandID] ASC,
	[AccountTranID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE) ON [PS_DataFeed]([BrandID])
) ON [PS_DataFeed]([BrandID])
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[DataFeed]') AND name = N'IDX_admin_all_DataFeed_BrandID_Datetime')
CREATE NONCLUSTERED INDEX [IDX_admin_all_DataFeed_BrandID_Datetime] ON [admin_all].[DataFeed]
(
	[BrandID] ASC,
	[Datetime] ASC
)
INCLUDE ( 	[AccountTranID],
	[PartyID],
	[UserID],
	[Currency],
	[ProductID],
	[ProductCode],
	[ProductTranID],
	[GameInfoID],
	[GameID],
	[GameTranID],
	[TranType],
	[AmountReal],
	[AmountPlayableBonus],
	[BalanceReal],
	[AmountReleasedBonus],
	[BalancePlayableBonus],
	[BalanceReleasedBonus],
	[RollbackTranID],
	[RollbackTranType]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE) ON [PS_DataFeed]([BrandID])
GO
