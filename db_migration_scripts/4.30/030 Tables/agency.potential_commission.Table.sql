SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[agency].[potential_commission]') AND type in (N'U'))
BEGIN
CREATE TABLE [agency].[potential_commission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[agent_id] [int] NULL,
	[plan_id] [int] NULL,
	[product_id] [int] NULL,
	[date] [datetime] NULL,
	[value] [numeric](38, 18) NULL,
	[TYPE] [varchar](20) NULL,
	[start_date] [datetime] NULL,
	[end_date] [datetime] NULL,
 CONSTRAINT [PK_agency_potential_commission] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
