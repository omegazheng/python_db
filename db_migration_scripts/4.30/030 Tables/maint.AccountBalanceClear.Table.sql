SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[AccountBalanceClear]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[AccountBalanceClear](
	[PartyID] [int] NOT NULL,
	[Updated] [bit] NOT NULL,
	[Datetime] [datetime] NOT NULL,
 CONSTRAINT [PK_maint_AccountBalanceClear] PRIMARY KEY CLUSTERED 
(
	[PartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_AccountBalanceClear_Updated]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[AccountBalanceClear] ADD  CONSTRAINT [DF_maint_AccountBalanceClear_Updated]  DEFAULT ((0)) FOR [Updated]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_AccountBalanceClear_Datetime]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[AccountBalanceClear] ADD  CONSTRAINT [DF_maint_AccountBalanceClear_Datetime]  DEFAULT (getdate()) FOR [Datetime]
END
GO
