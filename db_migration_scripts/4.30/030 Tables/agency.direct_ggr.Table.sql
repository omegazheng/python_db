SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[agency].[direct_ggr]') AND type in (N'U'))
BEGIN
CREATE TABLE [agency].[direct_ggr](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[agent_id] [int] NULL,
	[product_id] [int] NULL,
	[date] [datetime] NULL,
	[value] [numeric](38, 18) NULL,
	[wins] [numeric](38, 18) NULL,
	[bets] [numeric](38, 18) NULL,
	[released_bonuses] [numeric](38, 18) NULL,
 CONSTRAINT [PK_agency_direct_ggr] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[agency].[direct_ggr]') AND name = N'IDX_agency_direct_ggr_agent_id_date_product_id')
CREATE NONCLUSTERED INDEX [IDX_agency_direct_ggr_agent_id_date_product_id] ON [agency].[direct_ggr]
(
	[agent_id] ASC,
	[date] ASC,
	[product_id] ASC
)
INCLUDE ( 	[value],
	[released_bonuses]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
