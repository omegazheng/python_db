SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[CurrencyConversionChangeLogInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[CurrencyConversionChangeLogInfo](
	[InfoID] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](4000) NOT NULL,
	[ApplicationName] [nvarchar](128) NOT NULL,
	[LoginName] [nvarchar](128) NOT NULL,
	[HostName] [nvarchar](128) NOT NULL,
	[HostIPAddress] [nvarchar](128) NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_admin_all_CurrencyConversionChangeLogInfo] PRIMARY KEY CLUSTERED 
(
	[InfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CurrencyConversionChangeLogInfo_Date]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CurrencyConversionChangeLogInfo] ADD  CONSTRAINT [DF_admin_all_CurrencyConversionChangeLogInfo_Date]  DEFAULT (getdate()) FOR [Date]
END
GO
