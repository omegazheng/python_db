SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Logs].[GameRequestHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [Logs].[GameRequestHistory](
	[PartyID] [int] NULL,
	[SessionKey] [nvarchar](40) NULL,
	[RequestTime] [datetime2](3) NOT NULL,
	[ResponseTime] [datetime2](3) NOT NULL,
	[RequestIP] [varchar](50) NULL,
	[ServerName] [varchar](50) NULL,
	[ProductID] [int] NOT NULL,
	[Method] [varchar](30) NULL,
	[Request] [nvarchar](500) NULL,
	[Response] [nvarchar](500) NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Logs_GameRequestHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Logs].[GameRequestHistory]') AND name = N'IDX_Logs_GameRequestHistory_RequestTime_ResponseTime_PartyID_ProductID_Method')
CREATE NONCLUSTERED INDEX [IDX_Logs_GameRequestHistory_RequestTime_ResponseTime_PartyID_ProductID_Method] ON [Logs].[GameRequestHistory]
(
	[RequestTime] ASC,
	[ResponseTime] ASC,
	[PartyID] ASC,
	[ProductID] ASC,
	[Method] ASC
)
INCLUDE ( 	[SessionKey]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
