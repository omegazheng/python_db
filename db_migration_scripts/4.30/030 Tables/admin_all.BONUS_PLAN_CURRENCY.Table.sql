SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_PLAN_CURRENCY]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[BONUS_PLAN_CURRENCY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BONUS_PLAN_ID] [int] NOT NULL,
	[CURRENCY_CODE] [nchar](3) NOT NULL,
 CONSTRAINT [PK_admin_all_BONUS_PLAN_CURRENCY] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_PLAN_CURRENCY]') AND name = N'IDX_admin_all_Bonus_Plan_Currency_Bonus_Plan_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_Bonus_Plan_Currency_Bonus_Plan_ID] ON [admin_all].[BONUS_PLAN_CURRENCY]
(
	[BONUS_PLAN_ID] ASC
)
INCLUDE ( 	[CURRENCY_CODE]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_PLAN_CURRENCY]') AND name = N'IDX_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CODE')
CREATE NONCLUSTERED INDEX [IDX_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CODE] ON [admin_all].[BONUS_PLAN_CURRENCY]
(
	[CURRENCY_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
