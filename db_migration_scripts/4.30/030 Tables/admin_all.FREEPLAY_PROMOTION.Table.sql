SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[FREEPLAY_PROMOTION]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[FREEPLAY_PROMOTION](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FREEPLAY_PLAN_ID] [int] NULL,
	[CODE] [nvarchar](40) NOT NULL,
	[STATUS] [nvarchar](30) NOT NULL,
	[TRIGGER_DATE] [datetime] NULL,
	[LAST_UPDATED_TIME] [datetime] NULL,
	[IS_COMPLETED] [bit] NOT NULL,
	[PARTY_ID] [int] NOT NULL,
	[TOTAL_BALANCE] [numeric](38, 18) NOT NULL,
	[REMAINING_BALANCE] [numeric](38, 18) NULL,
	[AMOUNT_WON] [numeric](38, 18) NULL,
	[DEFAULT_STAKE_LEVEL] [numeric](38, 18) NULL,
	[GAME_INFO_ID] [int] NULL,
	[LINE] [int] NULL,
	[COIN] [numeric](38, 18) NULL,
	[DENOMINATION] [numeric](38, 18) NULL,
	[BET_PER_ROUND] [numeric](38, 18) NOT NULL,
	[ROUNDS] [int] NULL,
	[GAME_INFO_IDS] [nvarchar](3000) NULL,
 CONSTRAINT [PK_admin_all_FREEPLAY_PROMOTION] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[FREEPLAY_PROMOTION]') AND name = N'IDX_admin_all_FREEPLAY_PROMOTION_FREEPLAY_PLAN_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_FREEPLAY_PROMOTION_FREEPLAY_PLAN_ID] ON [admin_all].[FREEPLAY_PROMOTION]
(
	[FREEPLAY_PLAN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[FREEPLAY_PROMOTION]') AND name = N'IDX_admin_all_FREEPLAY_PROMOTION_PARTY_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_FREEPLAY_PROMOTION_PARTY_ID] ON [admin_all].[FREEPLAY_PROMOTION]
(
	[PARTY_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_FREEPLAY_PROMOTION_IS_COMPLETED]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[FREEPLAY_PROMOTION] ADD  CONSTRAINT [DF_admin_all_FREEPLAY_PROMOTION_IS_COMPLETED]  DEFAULT ((0)) FOR [IS_COMPLETED]
END
GO
