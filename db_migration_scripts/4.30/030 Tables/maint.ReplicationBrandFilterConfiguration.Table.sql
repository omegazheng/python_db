SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[ReplicationBrandFilterConfiguration]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[ReplicationBrandFilterConfiguration](
	[BrandID] [int] NOT NULL,
	[EnablePrevious] [bit] NOT NULL,
	[EnableCurrent] [bit] NOT NULL,
	[RetentionPeriodPrevious] [int] NOT NULL,
	[RetentionPeriodCurrent] [int] NOT NULL,
 CONSTRAINT [PK_maint_ReplicationBrandFilterConfiguration] PRIMARY KEY CLUSTERED 
(
	[BrandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
