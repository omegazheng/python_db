SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MachinePlayGameSummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MachinePlayGameSummary](
	[SummaryID] [bigint] IDENTITY(1,1) NOT NULL,
	[PartyID] [int] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[MachineID] [int] NOT NULL,
	[GameID] [varchar](100) NULL,
	[ProductID] [int] NULL,
	[GameInfoID] [int] NULL,
	[AmountReal] [numeric](38, 18) NOT NULL,
	[PlayableBonus] [numeric](38, 18) NOT NULL,
	[GamesPlayed] [int] NOT NULL,
	[TotalCreditWin] [numeric](38, 18) NOT NULL,
	[GameWin] [int] NOT NULL,
	[AmountTransferIn] [numeric](38, 18) NOT NULL,
	[AmountTransferOut] [numeric](38, 18) NOT NULL,
	[BonusTransferIn] [numeric](38, 18) NOT NULL,
	[BonusTransferOut] [numeric](38, 18) NOT NULL,
	[LoyaltyPoint] [bigint] NOT NULL,
	[TotalPromoAmount] [numeric](38, 18) NOT NULL,
	[TotalWinLossAmount] [numeric](38, 18) NOT NULL,
	[TicketIn] [numeric](38, 18) NOT NULL,
	[TicketOut] [numeric](38, 18) NOT NULL,
	[TicketPromoIn] [numeric](38, 18) NOT NULL,
	[TicketPromoOut] [numeric](38, 18) NOT NULL
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachinePlayGameSummary_TotalPromoAmount]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachinePlayGameSummary] ADD constraint DF_admin_all_MachinePlayGameSummary_TotalPromoAmount DEFAULT ((0.000000000000000000)) FOR [TotalPromoAmount]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachinePlayGameSummary_TotalWinLossAmount]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachinePlayGameSummary] ADD constraint DF_admin_all_MachinePlayGameSummary_TotalWinLossAmount DEFAULT ((0.000000000000000000)) FOR [TotalWinLossAmount]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachinePlayGameSummary_TicketIn]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachinePlayGameSummary] ADD constraint DF_admin_all_MachinePlayGameSummary_TicketIn DEFAULT ((0.000000000000000000)) FOR [TicketIn]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachinePlayGameSummary_TicketOut]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachinePlayGameSummary] ADD constraint DF_admin_all_MachinePlayGameSummary_TicketOut DEFAULT ((0.000000000000000000)) FOR [TicketOut]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachinePlayGameSummary_TicketPromoIn]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachinePlayGameSummary] ADD constraint DF_admin_all_MachinePlayGameSummary_TicketPromoIn DEFAULT ((0.000000000000000000)) FOR [TicketPromoIn]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachinePlayGameSummary_TicketPromoOut]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachinePlayGameSummary] ADD constraint DF_admin_all_MachinePlayGameSummary_TicketPromoOut DEFAULT ((0.000000000000000000)) FOR [TicketPromoOut]
END
GO
