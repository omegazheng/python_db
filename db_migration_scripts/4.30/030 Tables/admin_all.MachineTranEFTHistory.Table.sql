SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTranEFTHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MachineTranEFTHistory](
	[MachineTranEFTHistoryID] [bigint] NOT NULL,
	[EFTID] [bigint] NULL,
	[MachineTranID] [bigint] NULL,
	[AccountTranID] [bigint] NULL,
	[RollbackTranID] [bigint] NULL,
	[TransferType] [varchar](50) NOT NULL,
	[Status] [varchar](30) NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[AmountReal] [numeric](38, 18) NOT NULL,
	[ReleasedBonus] [numeric](38, 18) NOT NULL,
	[PlayableBonus] [numeric](38, 18) NOT NULL,
	[Reference] [varchar](max) NULL,
	[ProductTranID] [varchar](100) NULL,
	[RecordDate] [datetime] NOT NULL,
 CONSTRAINT [PK_admin_all_MachineTranEFTHistory] PRIMARY KEY CLUSTERED 
(
	[MachineTranEFTHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTranEFTHistory]') AND name = N'IDX_admin_all_MachineTranEFTHistory_EFTID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_MachineTranEFTHistory_EFTID] ON [admin_all].[MachineTranEFTHistory]
(
	[EFTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTranEFTHistory]') AND name = N'IDX_admin_all_MachineTranEFTHistory_MachineTranID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_MachineTranEFTHistory_MachineTranID] ON [admin_all].[MachineTranEFTHistory]
(
	[MachineTranID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachineTranEFTHistory_RecordDate]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachineTranEFTHistory] ADD  CONSTRAINT [DF_admin_all_MachineTranEFTHistory_RecordDate]  DEFAULT (getdate()) FOR [RecordDate]
END
GO
