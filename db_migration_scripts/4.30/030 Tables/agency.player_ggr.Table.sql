SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[agency].[player_ggr]') AND type in (N'U'))
BEGIN
CREATE TABLE [agency].[player_ggr](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[agent_id] [int] NULL,
	[player_id] [int] NULL,
	[product_id] [int] NULL,
	[date] [datetime] NULL,
	[value] [numeric](38, 18) NULL,
	[wins] [numeric](38, 18) NULL,
	[bets] [numeric](38, 18) NULL,
	[released_bonuses] [numeric](38, 18) NULL,
 CONSTRAINT [PK_agency_player_ggr] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[agency].[player_ggr]') AND name = N'IDX_agency_player_ggr_date')
CREATE NONCLUSTERED INDEX [IDX_agency_player_ggr_date] ON [agency].[player_ggr]
(
	[date] ASC
)
INCLUDE ( 	[player_id],
	[product_id],
	[value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[agency].[player_ggr]') AND name = N'IDX_agency_player_ggr_product_id_date')
CREATE NONCLUSTERED INDEX [IDX_agency_player_ggr_product_id_date] ON [agency].[player_ggr]
(
	[product_id] ASC,
	[date] ASC
)
INCLUDE ( 	[player_id],
	[value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
