SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[AlertCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[AlertCode](
	[AlertCode] [varchar](20) NOT NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_admin_all_AlertCode] PRIMARY KEY CLUSTERED 
(
	[AlertCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
