SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatabaseUpdateBatch]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatabaseUpdateBatch](
	[BatchID] [bigint] IDENTITY(1,1) NOT NULL,
	[HasError] [bit] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[LoginName] [varchar](128) NOT NULL,
 CONSTRAINT [PK_dbo_DatabaseUpdateBatch] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_dbo_DatabaseUpdateBatch_HasError]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DatabaseUpdateBatch] ADD  CONSTRAINT [DF_dbo_DatabaseUpdateBatch_HasError]  DEFAULT ((0)) FOR [HasError]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_dbo_DatabaseUpdateBatch_Date]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DatabaseUpdateBatch] ADD  CONSTRAINT [DF_dbo_DatabaseUpdateBatch_Date]  DEFAULT (getutcdate()) FOR [StartDate]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_dbo_DatabaseUpdateBatch_LoginName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DatabaseUpdateBatch] ADD  CONSTRAINT [DF_dbo_DatabaseUpdateBatch_LoginName]  DEFAULT (suser_sname()) FOR [LoginName]
END
GO
