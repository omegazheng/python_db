SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_SPORTSBOOK]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[BONUS_SPORTSBOOK](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BONUS_ID] [int] NOT NULL,
	[TYPE] [varchar](20) NULL,
	[SELECTION] [varchar](256) NULL,
	[SELECTION_CONDITION] [numeric](1, 0) NULL,
	[ODDS] [numeric](38, 18) NULL,
	[ODDS_CONDITION] [numeric](1, 0) NULL,
	[MARKET] [varchar](20) NULL,
	[MARKET_CONDITION] [numeric](1, 0) NULL,
	[PERIOD] [varchar](12) NULL,
	[PERIOD_CONDITION] [numeric](1, 0) NULL,
	[EVENT] [varchar](256) NULL,
	[EVENT_CONDITION] [numeric](1, 0) NULL,
	[SPORTSBOOK_PLATFORM_ID] [int] NOT NULL,
 CONSTRAINT [PK_admin_all_BONUS_SPORTSBOOK] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_SPORTSBOOK]') AND name = N'IDX_admin_all_BONUS_SPORTSBOOK_BONUS_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_BONUS_SPORTSBOOK_BONUS_ID] ON [admin_all].[BONUS_SPORTSBOOK]
(
	[BONUS_ID] ASC
)
INCLUDE ( 	[TYPE],
	[SELECTION],
	[SELECTION_CONDITION],
	[ODDS],
	[ODDS_CONDITION],
	[MARKET],
	[MARKET_CONDITION],
	[PERIOD],
	[PERIOD_CONDITION],
	[EVENT],
	[EVENT_CONDITION],
	[SPORTSBOOK_PLATFORM_ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
