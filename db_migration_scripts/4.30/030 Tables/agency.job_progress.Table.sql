SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[agency].[job_progress]') AND type in (N'U'))
BEGIN
CREATE TABLE [agency].[job_progress](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[job_name] [varchar](255) NOT NULL,
	[message] [varchar](255) NOT NULL,
 CONSTRAINT [PK_agency_job_progress] PRIMARY KEY CLUSTERED 
(
	[job_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
