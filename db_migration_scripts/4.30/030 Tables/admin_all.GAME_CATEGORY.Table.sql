SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[GAME_CATEGORY]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[GAME_CATEGORY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](100) NOT NULL,
	[IS_SLOTS] [tinyint] NOT NULL,
	[IS_CARDGAME] [tinyint] NOT NULL,
	[IS_TABLEGAME] [tinyint] NOT NULL,
	[IS_BINGO] [tinyint] NOT NULL,
	[IS_BLACKJACK_GAME] [tinyint] NOT NULL,
	[IS_ROULETTE_GAME] [tinyint] NOT NULL,
 CONSTRAINT [PK_admin_all_GAME_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_GAME_CATEGORY_NAME] UNIQUE NONCLUSTERED 
(
	[NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_GAME_CATEGORY_IS_SLOTS]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[GAME_CATEGORY] ADD  CONSTRAINT [DF_admin_all_GAME_CATEGORY_IS_SLOTS]  DEFAULT ((0)) FOR [IS_SLOTS]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_GAME_CATEGORY_IS_CARDGAME]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[GAME_CATEGORY] ADD  CONSTRAINT [DF_admin_all_GAME_CATEGORY_IS_CARDGAME]  DEFAULT ((0)) FOR [IS_CARDGAME]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_GAME_CATEGORY_IS_TABLEGAME]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[GAME_CATEGORY] ADD  CONSTRAINT [DF_admin_all_GAME_CATEGORY_IS_TABLEGAME]  DEFAULT ((0)) FOR [IS_TABLEGAME]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_GAME_CATEGORY_IS_BINGO]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[GAME_CATEGORY] ADD  CONSTRAINT [DF_admin_all_GAME_CATEGORY_IS_BINGO]  DEFAULT ((0)) FOR [IS_BINGO]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_GAME_CATEGORY_IS_BLACKJACK_GAME]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[GAME_CATEGORY] ADD constraint DF_admin_all_GAME_CATEGORY_IS_BLACKJACK_GAME DEFAULT ((0)) FOR [IS_BLACKJACK_GAME]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_GAME_CATEGORY_IS_ROULETTE_GAME]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[GAME_CATEGORY] ADD constraint DF_admin_all_GAME_CATEGORY_IS_ROULETTE_GAME DEFAULT ((0)) FOR [IS_ROULETTE_GAME]
END
GO
