SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[AUTH_OPERATION]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[AUTH_OPERATION](
	[NAME] [nvarchar](255) NOT NULL,
	[DESCRIPTION] [nvarchar](255) NOT NULL,
	[URL] [nvarchar](255) NULL,
 CONSTRAINT [PK_ADMIN_ALL_AUTH_OPERATION] PRIMARY KEY CLUSTERED 
(
	[NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
