SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_CONTRIB_RATIO]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[BONUS_CONTRIB_RATIO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BRANDID] [int] NOT NULL,
	[GAME_INFO_ID] [int] NOT NULL,
	[CONTRIB_RATIO] [int] NOT NULL,
 CONSTRAINT [PK_admin_all_BONUS_CONTRIB_RATIO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_CONTRIB_RATIO]') AND name = N'IDX_admin_all_BONUS_CONTRIB_RATIO_BRANDID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_BONUS_CONTRIB_RATIO_BRANDID] ON [admin_all].[BONUS_CONTRIB_RATIO]
(
	[BRANDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[BONUS_CONTRIB_RATIO]') AND name = N'IDX_admin_all_BONUS_CONTRIB_RATIO_GAME_INFO_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_BONUS_CONTRIB_RATIO_GAME_INFO_ID] ON [admin_all].[BONUS_CONTRIB_RATIO]
(
	[GAME_INFO_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_BONUS_CONTRIB_RATIO_CONTRIB_RATIO]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[BONUS_CONTRIB_RATIO] ADD  CONSTRAINT [DF_admin_all_BONUS_CONTRIB_RATIO_CONTRIB_RATIO]  DEFAULT ((0)) FOR [CONTRIB_RATIO]
END
GO
