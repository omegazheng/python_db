SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[COMMISSION_STRUCTURE]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[COMMISSION_STRUCTURE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[COMMISSION_ID] [int] NOT NULL,
	[PERIOD] [varchar](30) NOT NULL,
	[HAS_BONUS] [bit] NOT NULL,
	[HAS_RANGE] [bit] NOT NULL,
	[HAS_GROUP] [bit] NOT NULL,
	[HAS_PARAMETER] [bit] NOT NULL,
	[PROVIDER_PERCENTAGE] [numeric](10, 2) NULL,
	[PROFIT_PERCENTAGE] [numeric](10, 2) NULL,
 CONSTRAINT [PK_admin_all_COMMISSION_STRUCTURE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[COMMISSION_STRUCTURE]') AND name = N'IDX_admin_all_COMMISSION_STRUCTURE_COMMISSION_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_COMMISSION_STRUCTURE_COMMISSION_ID] ON [admin_all].[COMMISSION_STRUCTURE]
(
	[COMMISSION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_COMMISSION_STRUCTURE_HAS_BONUS]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[COMMISSION_STRUCTURE] ADD  CONSTRAINT [DF_admin_all_COMMISSION_STRUCTURE_HAS_BONUS]  DEFAULT ((0)) FOR [HAS_BONUS]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_COMMISSION_STRUCTURE_HAS_RANGE]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[COMMISSION_STRUCTURE] ADD  CONSTRAINT [DF_admin_all_COMMISSION_STRUCTURE_HAS_RANGE]  DEFAULT ((0)) FOR [HAS_RANGE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_COMMISSION_STRUCTURE_HAS_GROUP]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[COMMISSION_STRUCTURE] ADD  CONSTRAINT [DF_admin_all_COMMISSION_STRUCTURE_HAS_GROUP]  DEFAULT ((0)) FOR [HAS_GROUP]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_COMMISSION_STRUCTURE_HAS_PARAMETER]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[COMMISSION_STRUCTURE] ADD  CONSTRAINT [DF_admin_all_COMMISSION_STRUCTURE_HAS_PARAMETER]  DEFAULT ((0)) FOR [HAS_PARAMETER]
END
GO
