SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[PaymentCredentialPayment]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[PaymentCredentialPayment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PaymentCredentialID] [int] NOT NULL,
	[PaymentID] [int] NOT NULL,
 CONSTRAINT [PK_admin_all_PAYMENTCREDENTIALPAYMENT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_PaymentCredentialPayment_PaymentCredentialID_PaymentID] UNIQUE NONCLUSTERED 
(
	[PaymentCredentialID] ASC,
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[PaymentCredentialPayment]') AND name = N'IDX_admin_all_PaymentCredentialPayment_PaymentID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_PaymentCredentialPayment_PaymentID] ON [admin_all].[PaymentCredentialPayment]
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
