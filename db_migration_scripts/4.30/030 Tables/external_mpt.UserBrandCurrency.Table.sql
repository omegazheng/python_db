SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[UserBrandCurrency]') AND type in (N'U'))
BEGIN
CREATE TABLE [external_mpt].[UserBrandCurrency](
	[PartyID] [int] NOT NULL,
	[BrandID] [int] NOT NULL,
	[Currency] [nchar](3) NOT NULL,
 CONSTRAINT [PK_external_mpt_UserBrandCurrency] PRIMARY KEY CLUSTERED 
(
	[PartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
