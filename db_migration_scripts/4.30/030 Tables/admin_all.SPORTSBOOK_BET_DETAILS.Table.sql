SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[SPORTSBOOK_BET_DETAILS]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[SPORTSBOOK_BET_DETAILS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CURRENCY] [varchar](3) NOT NULL,
	[AMOUNT] [numeric](38, 18) NOT NULL,
	[TYPE] [varchar](20) NOT NULL,
	[WAGER_ID] [nvarchar](50) NOT NULL,
	[PLATFORM_CODE] [nvarchar](20) NOT NULL,
	[STATUS] [nvarchar](100) NULL,
	[CREATED_TIMESTAMP] [datetime] NULL,
	[SETTLED_TIMESTAMP] [datetime] NULL,
	[POTENTIAL_WIN] [numeric](38, 18) NULL,
	[TOTAL_ODDS] [numeric](38, 18) NULL,
	[CASH_OUT] [bit] NULL,
	[MSG] [nvarchar](3072) NULL,
 CONSTRAINT [PK_admin_all_SPORTSBOOK_BET_DETAILS] PRIMARY KEY CLUSTERED 
(
	[PLATFORM_CODE] ASC,
	[WAGER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[SPORTSBOOK_BET_DETAILS]') AND name = N'IDX_admin_all_SPORTSBOOK_BET_DETAILS_ID')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_admin_all_SPORTSBOOK_BET_DETAILS_ID] ON [admin_all].[SPORTSBOOK_BET_DETAILS]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[SPORTSBOOK_BET_DETAILS]') AND name = N'IDX_admin_all_SPORTSBOOK_BET_DETAILS_PLATFORM_CODE')
CREATE NONCLUSTERED INDEX [IDX_admin_all_SPORTSBOOK_BET_DETAILS_PLATFORM_CODE] ON [admin_all].[SPORTSBOOK_BET_DETAILS]
(
	[PLATFORM_CODE] ASC
)
INCLUDE ( 	[ID],
	[CURRENCY],
	[AMOUNT],
	[TYPE],
	[WAGER_ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[SPORTSBOOK_BET_DETAILS]') AND name = N'IDX_admin_all_SPORTSBOOK_BET_DETAILS_WAGER_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_SPORTSBOOK_BET_DETAILS_WAGER_ID] ON [admin_all].[SPORTSBOOK_BET_DETAILS]
(
	[WAGER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
