SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[Connection]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[Connection](
	[ConnectionID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[Comment] [varchar](128) NULL,
	[ServerName] [varchar](128) NOT NULL,
	[DatabaseName] [varchar](128) NULL,
	[UserName] [varchar](128) NULL,
	[Password] [varbinary](256) NULL,
	[IsContextConnection] [bit] NOT NULL,
	[CurrentChangeTrackingVersion] [bigint] NOT NULL,
	[CurrentRowVersion] [binary](8) NOT NULL,
	[AutoUpdateVersion] [bit] NOT NULL,
 CONSTRAINT [PK_maint_Connection] PRIMARY KEY CLUSTERED 
(
	[ConnectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_maint_Connection] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_Connection_IsContextConnection]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[Connection] ADD  CONSTRAINT [DF_maint_Connection_IsContextConnection]  DEFAULT ((0)) FOR [IsContextConnection]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_Connection_ChangeTrackingVersion]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[Connection] ADD  CONSTRAINT [DF_maint_Connection_ChangeTrackingVersion]  DEFAULT ((0)) FOR [CurrentChangeTrackingVersion]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_Connection_CurrentRowVersion]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[Connection] ADD  CONSTRAINT [DF_maint_Connection_CurrentRowVersion]  DEFAULT ((0)) FOR [CurrentRowVersion]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_Connection_AutoUpdateVersion]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[Connection] ADD  CONSTRAINT [DF_maint_Connection_AutoUpdateVersion]  DEFAULT ((0)) FOR [AutoUpdateVersion]
END
GO
