SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[POKER_HAND]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[POKER_HAND](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HAND_NUMBER] [int] NOT NULL,
	[PARTYID] [int] NOT NULL,
	[START_DATE] [varchar](255) NULL,
	[END_DATE] [varchar](255) NULL,
	[is_real_money] [bit] NULL,
	[is_multi_table_tournament] [bit] NULL,
	[is_single_table_tournament] [bit] NULL,
	[game_type] [varchar](100) NULL,
	[bet_limit] [varchar](100) NULL,
	[opening_balance] [numeric](38, 18) NOT NULL,
	[closing_balance] [numeric](38, 18) NOT NULL,
	[rake_amount] [numeric](38, 18) NOT NULL,
	[bet_amount] [numeric](38, 18) NOT NULL,
	[won_amount] [numeric](38, 18) NOT NULL,
 CONSTRAINT [PK_admin_all_POKER_HAND] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_POKER_HAND_HAND_NUMBER_PARTYID] UNIQUE NONCLUSTERED 
(
	[HAND_NUMBER] ASC,
	[PARTYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[POKER_HAND]') AND name = N'IDX_admin_all_POKER_HAND_PARTYID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_POKER_HAND_PARTYID] ON [admin_all].[POKER_HAND]
(
	[PARTYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_is_real_money]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_is_real_money]  DEFAULT ((0)) FOR [is_real_money]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_is_multi_table_tournament]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_is_multi_table_tournament]  DEFAULT ((0)) FOR [is_multi_table_tournament]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_is_single_table_tournament]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_is_single_table_tournament]  DEFAULT ((0)) FOR [is_single_table_tournament]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_opening_balance]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_opening_balance]  DEFAULT ((0)) FOR [opening_balance]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_closing_balance]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_closing_balance]  DEFAULT ((0)) FOR [closing_balance]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_rake_amount]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_rake_amount]  DEFAULT ((0)) FOR [rake_amount]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_bet_amount]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_bet_amount]  DEFAULT ((0)) FOR [bet_amount]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_POKER_HAND_won_amount]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[POKER_HAND] ADD  CONSTRAINT [DF_admin_all_POKER_HAND_won_amount]  DEFAULT ((0)) FOR [won_amount]
END
GO
