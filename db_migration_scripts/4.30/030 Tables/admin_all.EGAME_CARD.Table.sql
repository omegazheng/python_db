SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[EGAME_CARD]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[EGAME_CARD](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PARTYID] [int] NOT NULL,
	[LOSS_CREDIT] [numeric](38, 18) NOT NULL,
	[CREDIT_PERIOD] [int] NOT NULL,
	[DEFAULT_LOSS_CREDIT] [numeric](38, 18) NOT NULL,
	[DEFAULT_CREDIT_PERIOD] [int] NOT NULL,
	[RENEWAL_DATE] [datetime] NOT NULL,
	[BETS] [numeric](38, 18) NOT NULL,
	[WINS] [numeric](38, 18) NOT NULL,
	[RESULTS] [numeric](38, 18) NOT NULL,
	[CHECKOUT_DATE] [datetime] NOT NULL,
	[GAME_CARD_ID] [int] NOT NULL,
	[IS_CHECKED_OUT] [int] NOT NULL,
	[HIT_LIMIT] [int] NOT NULL,
 CONSTRAINT [PK_admin_all_EGAME_CARD] PRIMARY KEY CLUSTERED 
(
	[PARTYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_EGAME_CARD_HIT_LIMIT]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[EGAME_CARD] ADD  CONSTRAINT [DF_admin_all_EGAME_CARD_HIT_LIMIT]  DEFAULT ((0)) FOR [HIT_LIMIT]
END
GO
