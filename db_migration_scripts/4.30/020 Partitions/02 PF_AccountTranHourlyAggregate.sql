if not exists(select * from sys.partition_functions where name = 'PF_AccountTranHourlyAggregate')
	create partition function [PF_AccountTranHourlyAggregate](tinyint) as range left for values (0, 1, 2, 3, 4, 5)
GO

