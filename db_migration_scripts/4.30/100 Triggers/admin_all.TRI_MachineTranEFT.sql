set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_MachineTranEFT on admin_all.MachineTranEFT
for delete, update
as
begin 
	if @@rowcount = 0
		return
	set nocount on

	insert into admin_all.MachineTranEFTHistory(MachineTranEFTHistoryID, EFTID, MachineTranID, AccountTranID, RollbackTranID, TransferType, Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, Reference, ProductTranID, RecordDate)
		select next value for admin_all.SeqMachineTranEFTHistoryID MachineTranEFTHistoryID, EFTID, MachineTranID, AccountTranID, RollbackTranID, TransferType, Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, Reference, ProductTranID, getdate() RecordDate
		from deleted

end
go
