set ansi_nulls, quoted_identifier on
go
create or alter trigger [admin_all].[TRI_StaffProduct] on [admin_all].[StaffProduct] 
instead of insert, update
as 
begin
	if @@rowcount = 0
		return
	if exists(
				select * 
				from admin_all.StaffCompany sc
					inner join inserted i on i.StaffID = sc.StaffID
				)
	begin
		raiserror('A company associated staff can not be set as a product associated staff.', 16, 1)
		rollback
	end
end
go
