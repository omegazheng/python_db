set ansi_nulls, quoted_identifier on
go

create or alter trigger [admin_all].[TRI_admin_all_AUTH_ROLE_INS] on [admin_all].[AUTH_ROLE]
for insert as
begin
	if @@rowcount = 0 return;
	set nocount on
	update u
		set Hierarchy = hierarchyid::Parse(
			case when i.ParentID is null then '/' + cast(i.ID as varchar(20))+'/'
				else u1.Hierarchy.ToString() + cast(i.ID as varchar(20))+'/'
			end)
	from admin_all.AUTH_ROLE  as u
		inner join inserted i on i.ID = u.ID
		left outer join admin_all.AUTH_ROLE u1 on i.ParentID = u1.ID
end
go
