set ansi_nulls, quoted_identifier on
go

create or alter trigger [admin_all].[TRI_admin_all_AUTH_ROLE_UPD] on [admin_all].[AUTH_ROLE]
for update as
begin
	if @@rowcount = 0 return;
	set nocount on
	if UPDATE(ParentID)
	begin
		declare @HIDs table(HID HierarchyId)
		;with map as
		(
			select d.Hierarchy.GetAncestor(1) OldParent,
				case when i.ParentID is null  then hierarchyid::GetRoot () else p.Hierarchy end NewParent,
				i.ID,
				d.Hierarchy OldHierarchy
			from inserted i
				left outer join admin_all.AUTH_ROLE p on p.ID = i.ParentID
				inner join deleted d on d.ID = i.ID
			where isnull(i.ParentID, -1) <> isnull(d.ParentID, -1)
		)
		update u
			set u.Hierarchy = u.Hierarchy.GetReparentedValue(map.OldParent, map.NewParent)
			output inserted.Hierarchy into @HIDs
		from admin_all.AUTH_ROLE u
			inner join map on u.Hierarchy.IsDescendantOf(map.OldHierarchy) = 1

		if exists(
					select*
					from (select cast('<R>'+replace(HID.GetAncestor(1).ToString(),'/', '</R><R>')+'</R>' as xml) P from @HIDs)x0
					where exists(
											select *
											from  (
														select n.value('.', 'varchar(20)') v
														from  x0.P.nodes('R') c(n)
													) x2
											where v <> ''
											group by v
											having count(*) > 1
									)
					)
			begin
					raiserror('Items are referenced multiple times in the hierarchy.', 16,1)
					rollback
					return
			end
	end
end
go
