set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_JobStatus on admin_all.JobStatus 
for update 
as
begin
	if @@rowcount = 0
		return
	set nocount on
	delete t
	from admin_all.JobStatus t
		inner join inserted s on s.JobName = t.JobName
	where s.Status = 'Removed'
	insert into admin_all.JobExecutionHistory(JobName, Status, StartDate, EndDate, Message)
		select JobName, Status, StartDate, EndDate, Message
		from inserted
		where EndDate is not null
	if @@rowcount >0
	begin
		;with x0 as
		(
			select h.JobName, h.JobExecutionHistoryID, row_number() over(partition by h.JobName order by h.JobExecutionHistoryID desc) RowNumber
			from inserted i
				inner join admin_all.JobExecutionHistory h on h.JobName = i.JobName
			where i.EndDate is not null
		)
		delete h
		from admin_all.JobExecutionHistory h
			inner join x0 on x0.JobName = h.JobName and x0.JobExecutionHistoryID = h.JobExecutionHistoryID
		where x0.RowNumber > 1000
	end
end



go
