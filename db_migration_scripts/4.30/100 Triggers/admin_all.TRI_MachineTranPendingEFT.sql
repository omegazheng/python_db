set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_MachineTranPendingEFT on admin_all.MachineTranPendingEFT 
for insert, delete, update
as
begin 
	if @@rowcount = 0
		return
	set nocount on
	declare @ContextInfo binary(128) = cast(cast('admin_all.usp_MachineEFTSetMachineID' as varchar(128)) as varbinary(128))
	if isnull(context_info(), 0x0) <> @ContextInfo
	begin
		insert into admin_all.MachineTranEFTHistory(MachineTranEFTHistoryID, EFTID, MachineTranID, AccountTranID, RollbackTranID, TransferType, Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, Reference, ProductTranID, RecordDate)
			select EFTID MachineTranEFTHistoryID, EFTID, null MachineTranID, AccountTranID, null RollbackTranID, 'MACHINE_EFT' TransferType, 'Pending' Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, null Reference, null ProductTranID, getdate() RecordDate
			from inserted
	end
	else
	begin
		update a
			set MachineTranID = eft.MachineTranID
		from admin_all.MachineTranEFTHistory a
			inner join deleted b on a.MachineTranEFTHistoryID = b.EFTID
			inner join admin_all.MachineTranEFT eft on eft.EFTID = b.EFTID
	end
end
go
