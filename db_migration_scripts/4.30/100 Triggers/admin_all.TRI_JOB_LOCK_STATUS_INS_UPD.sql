set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_JOB_LOCK_STATUS_INS_UPD on [admin_all].[JOB_LOCK_STATUS]
for insert, update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	;with x0 as
	(
		select isnull(i.JOB_NAME, d.JOB_NAME) JOB_NAME, i.LOCKED CurrentStatus, isnull(d.LOCKED, 0) PreviousStatus
		from inserted i
			left join deleted d on i.JOB_NAME = d.JOB_NAME
	)
	update t
		set 
			t.JOB_STARTED_TIME = case when x0.PreviousStatus = 0 and x0.CurrentStatus = 1 then getdate() else t.JOB_STARTED_TIME end,
			t.JOB_END_TIME = case when x0.PreviousStatus = 0 and x0.CurrentStatus = 1 then null else getdate() end
	from admin_all.JOB_LOCK_STATUS t
		inner join x0 on x0.JOB_NAME = t.JOB_NAME
	where x0.PreviousStatus <> x0.CurrentStatus
end

go
