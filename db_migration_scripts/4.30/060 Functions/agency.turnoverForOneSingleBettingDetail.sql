set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[turnoverForOneSingleBettingDetail]
    (@bet_id int, @bet_amount numeric(38,18), @plan_id int) returns numeric(38,18)
AS
  BEGIN
    declare @overall_odds numeric(38,18)
    declare @contribution numeric(38,18)
    declare @turnover numeric(38,18)
    declare @settled_event_count int

    set @turnover = 0

    select @overall_odds=d.overall_odds,
           @settled_event_count=d.settled_event_count
    from admin_all.betting_detail as d
    where d.id = @bet_id

    declare @percentage numeric(38,18)
    declare @has_exception int
    declare @operator varchar(255)
    declare @exceptional_percentage numeric(38,18)
    declare @criteria numeric(38,18)

    if @settled_event_count is not null and @settled_event_count <> 0
    begin
        select top 1 @percentage = percentage,
                @has_exception = g.has_exception,
                    @operator = g.operator,
                    @criteria = g.min_odds,
                    @exceptional_percentage = g.user_percentage
            from admin_all.commission_group as g
                join admin_all.commission_structure as s on g.commission_structure_id = s.id
            where g.grouping <= @settled_event_count
                and s.commission_id = @plan_id
            order by g.grouping desc

        if @has_exception = 1
        begin
            if @operator = '<' and @overall_odds < @criteria
                select @percentage = @exceptional_percentage
            if @operator = '<=' and @overall_odds <= @criteria
                select @percentage = @exceptional_percentage
            if @operator = '>=' and @overall_odds >= @criteria
                select @percentage = @exceptional_percentage
            if @operator = '>' and @overall_odds > @criteria
                select @percentage = @exceptional_percentage
            if @operator = '=' and @overall_odds = @criteria
                select @percentage = @exceptional_percentage
        end

        select @turnover = @bet_amount * (@percentage/100.00)
    end

    return @turnover
  END

go
