set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetSQLConnectionStringByName(@ConnectionName varchar(128))
returns varchar(8000)
as
begin
	return (
				select 
						maint.fn_GetSQLConnectionStringByID(ConnectionID)
				from maint.Connection
				where Name = @ConnectionName
			)
end

go
