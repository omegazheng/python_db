set ansi_nulls, quoted_identifier on
go
create or alter function external_mpt.GetPartySubordinate(@PartyID int, @RelativeLevel int = 10) 
returns table 
as 
	return(
			With x0 as 
			(
				select PartyID, Hierarchy, Level
				from external_mpt.USER_CONF
				where PartyID = @PartyID
			)
			select u.PartyID, u.Hierarchy, u.Level 
			from external_mpt.USER_CONF u
				cross join x0
			where u.Hierarchy.IsDescendantOf(x0.Hierarchy) = 1
				and u.Level > x0.Level 
				and u.Level <= x0.Level + @RelativeLevel
		)
go
