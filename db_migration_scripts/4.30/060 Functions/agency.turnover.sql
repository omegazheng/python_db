set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[turnover]
    (@agent_id int, @plan_id int, @start_date DATETIME, @end_date DATETIME) returns numeric(38,18)
AS
  BEGIN
    declare @turnover numeric(38,18)

    set @turnover = (
        select
        	sum([agency].[turnoverForOneSingleBettingDetail](d.id, x.amount, @plan_id))
        from admin_all.betting_detail as d
        	join (
        		select d.id,
        				sum(
        					case
        				       when t.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then -(t.amount_real + t.amount_released_bonus)
        				       when t.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then -(t.amount_real + t.amount_released_bonus)
        				       else 0
        			       end
        				) amount

        		from admin_all.betting_detail as d
        		    join admin_all.account_tran as t on d.bet_reference_num = t.game_tran_id and d.platform_id = t.platform_id
        		        join admin_all.account as a on t.account_id = a.id
        		            join external_mpt.user_conf as u on a.partyid = u.partyid
        				left join admin_all.account_tran as r on t.rollback_tran_id = r.id
        		where d.settlement_date >= @start_date
        		    and d.settlement_date < @end_date
        		    and d.platform_id in (select platform_id from admin_all.commission_platform where commission_id = @plan_id)
        		    and u.parentid = @agent_id
        		group by d.id
        	) x on x.id = d.id
    )

    if @turnover is null
        set @turnover = 0

    return @turnover
  END

go
