set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_IsChangeTrackingEnabledOnTable(@ConnectionString varchar(max), @TableName sysname)
returns bit
as
begin
	declare @SQL nvarchar(max)
	select @SQL = 'select case when exists(select * from sys.change_tracking_tables where object_id = object_id(@TableName)) then 1 else 0 end'
	select @SQL = 'exec sp_executesql N''' + replace(@SQL  , '''', '''''') + ''', N''@TableName sysname'', N''' + replace(@TableName, '''', '''''') + ''''
	return maint.fn_ExecuteSQLScalarString(@ConnectionString, @SQL)
end 

go
