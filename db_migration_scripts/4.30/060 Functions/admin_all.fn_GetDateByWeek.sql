set ansi_nulls, quoted_identifier on
go

-- NOTE: better to set datefirst before use this function
-- e.g., SET DATEFIRST 1
create or alter function [admin_all].[fn_GetDateByWeek]
  (@week INT, @year INT)
  RETURNS DATETIME
AS
  BEGIN
    DECLARE @value DATETIME
    SET @value =
    (SELECT dateadd(WEEK, @week - 1, dateadd(YEAR, @year - 1900, 0)) - 4 -
                                               datepart(DW, dateadd(
                                                                WEEK, @week - 1,
                                                                dateadd(YEAR, @year - 1900, 0)) - 4) + 1)

    RETURN @value
  END

go
