set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetSQLConnectionStringByID(@ConnectionID int)
RETURNS varchar(8000)
as
begin
	return (
			case 
					when @ConnectionID = -1 then 'context connection=true'
					when @ConnectionID = 0 then maint.fn_BuildSQLConnectionString(@@ServerName, db_name(), null, null)
					else
						(
							select 
									maint.fn_BuildSQLConnectionString(ServerName, DatabaseName, UserName, maint.fn_GetConnectionPassword(ConnectionID))
							from maint.Connection
							where ConnectionID = @ConnectionID
						)
				end
			)
end

go
