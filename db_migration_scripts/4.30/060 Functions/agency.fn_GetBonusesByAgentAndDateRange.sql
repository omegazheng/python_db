set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[fn_GetBonusesByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME) returns numeric(19, 2)
AS
  BEGIN
    declare @value numeric(19, 2)
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)

    if @user_type = 0
        set @value = (
            select sum(released_bonuses)
            from agency.player_ggr
            where player_id = @agent_id
                and date >= @start_date and date < @end_date
        )
    else
        set @value = (
            select sum(released_bonuses)
            from (
                select 'direct' type, id, released_bonuses
                from agency.direct_ggr
                where agent_id = @agent_id
                    and date >= @start_date and date < @end_date

                union

                select 'network' type, id, released_bonuses
                from agency.network_ggr
                where agent_id = @agent_id
                    and date >= @start_date and date < @end_date
            ) x
        )

    if @value is null
        set @value = 0

    return @value
  END

go
