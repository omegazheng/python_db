set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.GetBrandIDByStaffID(@StaffID int)
returns table
as
return(
with x0 as
(
    select BRANDID
    from admin_all.STAFF_BRAND_TBL
    where STAFFID = @StaffID
), x1 as
(
  select BRANDID
  from admin.CASINO_BRAND_DEF b
  where exists(select * from x0 where x0.BRANDID = b.BRANDID)
  union all
  select b.BRANDID
  from admin.CASINO_BRAND_DEF b
    inner join x1 on x1.BRANDID = b.Parent_BrandID
)

select * from x1
)
go
