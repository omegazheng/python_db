set ansi_nulls, quoted_identifier on
go

create or alter function admin_all.fn_getAlphabet
    (@Value nvarchar(max)) returns nvarchar(max)

as
  begin
    if @Value is null set @Value = ''
    Declare @KeepValues as nvarchar(max)
    Set @KeepValues = '%[^a-zA-Z]%'
    While PatIndex(@KeepValues, @Value) > 0
      Set @Value = Lower(Stuff(@Value, PatIndex(@KeepValues, @Value), 1, ''))
    Return rtrim(ltrim(Lower(@Value)))
  end

go
