set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[fn_GetGgrByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME) returns numeric(38,18)
AS
  BEGIN
    declare @value numeric(38,18)
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)

    if @user_type = 0
        set @value = (
            select sum(value - released_bonuses)
            from agency.player_ggr
            where player_id = @agent_id
                and date >= @start_date and date < @end_date
        )
    else
        set @value = (
            select sum(ggr)
            from (
                select 'direct' type, id, value - released_bonuses as ggr
                from agency.direct_ggr
                where agent_id = @agent_id
                    and date >= @start_date and date < @end_date

                union

                select 'network' type, id, value - released_bonuses as ggr
                from agency.network_ggr
                where agent_id = @agent_id
                    and date >= @start_date and date < @end_date
            ) x
        )

    if @value is null
        set @value = 0

    return @value
  END

go
