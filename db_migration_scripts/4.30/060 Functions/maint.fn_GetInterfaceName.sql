set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetInterfaceName(@SchemaName sysname, @TableName sysname)
returns sysname
as
begin
	return quotename(@SchemaName) + '.' + quotename(@TableName+'_Interface')
end
go
