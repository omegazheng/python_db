set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[fn_GetSignupsByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME) returns int
AS
  BEGIN
    declare @value int
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)

    if @user_type = 0
        set @value = (
            select count(*)
            from external_mpt.user_conf as player
            where
                player.reg_date >= @start_date and player.reg_date < @end_date
                and player.partyid = @agent_id
        )
    else
    begin
        declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);
        set @value = (
            select count(*)
            from external_mpt.user_conf as player
            where
                player.hierarchy.IsDescendantOf(@agentHierarchy)=1
                and player.reg_date >= @start_date and player.reg_date < @end_date
                and player.user_type = 0
        )
    end
    if @value is null
        set @value = 0

    return @value
  END

go
