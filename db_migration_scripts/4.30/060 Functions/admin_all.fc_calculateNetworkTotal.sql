set ansi_nulls, quoted_identifier on
go

create or alter function [admin_all].[fc_calculateNetworkTotal](@Parent_Hierarchy hierarchyid, @Parent_PartyID int)
    returns table
    as
    return (
    with x0 as
    (
        select u.PARTYID, u.ParentID, u.Hierarchy, u.Level, u.user_type, a.BALANCE_REAL, a.CIT
            , a.PLAYABLE_BONUS, a.RELEASED_BONUS
        from external_mpt.USER_CONF u
            left join admin_all.ACCOUNT a on u.PARTYID = a.PARTYID
    )
    select
        isnull(sum(case when not(s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID)) then s.CIT end ),0) Network_CIT,
        isnull(Sum(case when s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID) then CIT end),0) User_CIT,

        isnull(sum(case when not(s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID)) then s.BALANCE_REAL end ), 0) Network_BALANCE_REAL,
        isnull(Sum(case when s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID) then BALANCE_REAL end), 0) User_BALANCE_REAL,

        isnull(sum(case when not(s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID)) then s.PLAYABLE_BONUS end ), 0) Network_PLAYABLE_BONUS,
        isnull(Sum(case when s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID) then PLAYABLE_BONUS end), 0) User_PLAYABLE_BONUS,

        isnull(sum(case when not(s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID)) then s.RELEASED_BONUS end), 0) Network_RELEASED_BONUS,
        isnull(Sum(case when s.user_type = 0 and s.ParentID = isnull(@Parent_PartyID, s.ParentID) then RELEASED_BONUS end), 0) User_RELEASED_BONUS
    from x0 s
    where s.Hierarchy.IsDescendantOf(@Parent_Hierarchy) = 1
          and s.Hierarchy <> @Parent_Hierarchy
    )


go
