set ansi_nulls, quoted_identifier on
go


create or alter function [admin_all].[fn_SanityCheckDWWithAccountTran] (@startDate DateTime, @endDate DateTime)
  RETURNS nvarchar
AS
  BEGIN
    declare @diff money = 0;
    declare @result nvarchar(5) = 'FAILED'

    SELECT @diff =
           sum (HANDLE_REAL_DIFF + HANDLE_RB_DIFF + HANDLE_PB_DIFF + WIN_REAL_DIFF + WIN_RB_DIFF + WIN_PB_DIFF)

    FROM
      (
        SELECT
          summary.CURRENCY,
          (transactions.HANDLE_REAL - summary.HANDLE_REAL) HANDLE_REAL_DIFF,
          (transactions.HANDLE_RELEASED_BONUS - summary.HANDLE_RELEASED_BONUS) HANDLE_RB_DIFF,
          (transactions.HANDLE_PLAYABLE_BONUS- summary.HANDLE_PLAYABLE_BONUS) HANDLE_PB_DIFF,
          (transactions.WIN_REAL- summary.WIN_REAL) WIN_REAL_DIFF,
          (transactions.WIN_RELEASED_BONUS- summary.WIN_RELEASED_BONUS) WIN_RB_DIFF,
          (transactions.WIN_PLAYABLE_BONUS- summary.WIN_PLAYABLE_BONUS) WIN_PB_DIFF
        FROM (
               SELECT
                 CURRENCY,
                 SUM(HANDLE_REAL)           HANDLE_REAL,
                 SUM(HANDLE_RELEASED_BONUS) HANDLE_RELEASED_BONUS,
                 SUM(HANDLE_PLAYABLE_BONUS) HANDLE_PLAYABLE_BONUS,
                 SUM(WIN_REAL)              WIN_REAL,
                 SUM(WIN_RELEASED_BONUS)    WIN_RELEASED_BONUS,
                 SUM(WIN_PLAYABLE_BONUS)    WIN_PLAYABLE_BONUS
               FROM (
                      SELECT
                        CURRENCY,
                        TRAN_TYPE,
                        CASE WHEN TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND')
                          THEN SUM(-1 * AMOUNT_REAL)
                        ELSE 0 END HANDLE_REAL,
                        CASE WHEN TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND')
                          THEN SUM(-1 * AMOUNT_RELEASED_BONUS)
                        ELSE 0 END HANDLE_RELEASED_BONUS,
                        CASE WHEN TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND')
                          THEN SUM(-1 * AMOUNT_PLAYABLE_BONUS)
                        ELSE 0 END HANDLE_PLAYABLE_BONUS,
                        CASE WHEN TRAN_TYPE IN ('GAME_WIN', 'CASH_OUT')
                          THEN SUM(AMOUNT_REAL)
                        ELSE 0 END WIN_REAL,
                        CASE WHEN TRAN_TYPE IN ('GAME_WIN', 'CASH_OUT')
                          THEN SUM(AMOUNT_RELEASED_BONUS)
                        ELSE 0 END WIN_RELEASED_BONUS,
                        CASE WHEN TRAN_TYPE IN ('GAME_WIN', 'CASH_OUT')
                          THEN SUM(AMOUNT_PLAYABLE_BONUS)
                        ELSE 0 END WIN_PLAYABLE_BONUS
                      FROM ACCOUNT_TRAN
                        JOIN ACCOUNT ON ACCOUNT_TRAN.ACCOUNT_ID = ACCOUNT.id
                        JOIN EXTERNAL_MPT.USER_CONF USERS ON USERS.PARTYID = ACCOUNT.PARTYID
                      WHERE ROLLED_BACK = 0 AND
                            DATETIME >= @startDate
                            AND DATETIME < @endDate
                            AND TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND', 'GAME_WIN', 'CASH_OUT', 'STAKE_DEC')
                      GROUP BY USERS.CURRENCY, TRAN_TYPE
                    ) result
               GROUP BY CURRENCY
             ) transactions
          left join
          (

            SELECT CURRENCY,
              SUM (HANDLE_REAL) HANDLE_REAL,
              SUM (HANDLE_RELEASED_BONUS) HANDLE_RELEASED_BONUS ,
              SUM (HANDLE_PLAYABLE_BONUS) HANDLE_PLAYABLE_BONUS,
              SUM (HANDLE_REAL - PNL_REAL) WIN_REAL,
              SUM (DW_GAME_PLAYER_DAILY.HANDLE_RELEASED_BONUS - DW_GAME_PLAYER_DAILY.PNL_RELEASED_BONUS) WIN_RELEASED_BONUS,
              SUM (DW_GAME_PLAYER_DAILY.HANDLE_PLAYABLE_BONUS - DW_GAME_PLAYER_DAILY.PNL_PLAYABLE_BONUS) WIN_PLAYABLE_BONUS
            FROM ADMIN_ALL.DW_GAME_PLAYER_DAILY
            WHERE SUMMARY_DATE >= @startDate AND SUMMARY_DATE < @endDate
            GROUP BY CURRENCY
          ) summary on transactions.CURRENCY = summary.CURRENCY
      ) output

    if @diff  = 0
      SET @result = 'PASSED'
    return  @result
  END


go
