set ansi_nulls, quoted_identifier on
go
create or alter function maint.usp_ConnectionExecuteScalarObject(@ConnectionID int, @CommandText nvarchar(max))
returns sql_variant
as
begin
	return maint.fn_ExecuteSQLScalarObject(maint.fn_GetSQLConnectionStringByID(@ConnectionID),@CommandText)
end
go
