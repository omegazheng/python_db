set ansi_nulls, quoted_identifier on
go



create or alter function [agency].[fn_GetTransfersByTypeAndAgentAndDateRange]
    (@type varchar(25), @agent_id int, @start_date DATETIME, @end_date DATETIME) returns numeric(38,18)
AS
  BEGIN
    declare @value numeric(38,18);
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id);
    declare @transferType varchar(10);

    if @user_type = 0
    begin
        set @transferType = @type;
        if @type = 'WITHDRAW'
        begin
            set @transferType = 'WITHDRAWAL'
        end
        set @value = (
            select sum(amount_real + amount_released_bonus)
            from external_mpt.user_conf as player
                join admin_all.account as account on account.partyid = player.partyid
                    join admin_all.account_tran on account_tran.account_id = account.id
            where
                player.partyid = @agent_id

                and datetime >= @start_date and datetime < @end_date
                and tran_type = @transferType
                and reference is not null
        )
    end
    else
    begin
        declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);
        set @transferType = 'P_' + @type

        declare @subagents TABLE(partyid int, parentid int)
        insert into @subagents
            select
                partyid,
                parentid
            from external_mpt.user_conf as player
            where
                player.user_type = 0
                and player.parentid is not null
                and player.hierarchy.IsDescendantOf(@agentHierarchy) = 1

        declare @accounts table (account_id int)
        insert into @accounts(account_id)
            select account.id
            from admin_all.account
            where
                account.partyid in (select parentid from @subagents)

        set @value = (
            select sum(-amount_real)
            from admin_all.account_tran
            where
                datetime >= @start_date and datetime < @end_date
                and tran_type = @transferType
                and account_id in (select account_id from @accounts)
        )
    end

    if @value is null
        set @value = 0

    return @value
  END

go
