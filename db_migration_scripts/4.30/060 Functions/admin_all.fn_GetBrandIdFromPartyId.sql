set ansi_nulls, quoted_identifier on
go


create or alter function [admin_all].[fn_GetBrandIdFromPartyId] (@partyId int)
  RETURNS int
AS
  BEGIN
    DECLARE @brandId int;
    Set @brandId = (SELECT brandId from external_mpt.USER_CONF where partyid = @partyId)
    if @brandId is null
      set @brandId = 0
    RETURN @brandId;
  END


go
