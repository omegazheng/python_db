set ansi_nulls, quoted_identifier on
go

create or alter function admin_all.fn_getMD5
    (@Value varchar(max)) returns varchar(32)
as
  begin
    return LOWER(CONVERT(VARCHAR(32), HashBytes('MD5', @Value), 2));
  end

go
