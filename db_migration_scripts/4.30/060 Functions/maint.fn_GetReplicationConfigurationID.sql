set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetReplicationConfigurationID(@FriendlyName varchar(128))
returns int
as
begin
	declare @ConfigurationID int
	if isnumeric(@FriendlyName) = 1
	begin
		select @ConfigurationID = ConfigurationID
		from maint.ReplicationConfiguration
		where ConfigurationID = cast(@FriendlyName as int)
	end
	if @ConfigurationID is not null
		return @ConfigurationID
	select @ConfigurationID = ConfigurationID
	from maint.ReplicationConfiguration
	where Name = @FriendlyName
	return @ConfigurationID
end

go
