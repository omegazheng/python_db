set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetRegistry (@Key nvarchar(255))
returns nvarchar(max)
as
begin
	return (select VALUE from admin_all.REGISTRY_HASH where MAP_KEY = @Key)
end


go
