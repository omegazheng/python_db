set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetUserAssociatedAccount(@PartyID int)
returns table
as
return 
(
	select b.*
	from external_mpt.UserAssociatedAccount a
		inner join external_mpt.UserAssociatedAccount b on b.PartyID = a.PartyID
	where a.AssociatedPartyID = @PartyID
)

go
