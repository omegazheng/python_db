set ansi_nulls, quoted_identifier on
go
create or alter view [admin_all].[v_accounting_player_daily]
AS
  SELECT
    PARTYID,
    BRANDID,
    CURRENCY,
    METHOD,
    SUMMARY_DATE,
    PROCESS_DATE,
    sum(deposit)          AS DEPOSIT,
    sum(fee)              AS FEE,
    sum(deposit_count)    AS DEPOSIT_COUNT,
    sum(withdrawal)       AS WITHDRAWAL,
    sum(withdrawal_count) AS WITHDRAWAL_COUNT,
    MONTH,
    YEAR
  FROM
    (SELECT
       account.PARTYID,
       METHOD,
       brandid,
       currency,
       CAST(CONVERT(VARCHAR(8), REQUEST_DATE, 112) AS DATETIME)        AS SUMMARY_DATE,
       CAST(CONVERT(VARCHAR(8), PROCESS_DATE, 112) AS DATETIME)        AS PROCESS_DATE,
       (CASE WHEN (PAYMENT.type = 'DEPOSIT' AND status = 'COMPLETED')
         THEN amount
        ELSE 0 END)                                                    AS DEPOSIT,
       (CASE WHEN (PAYMENT.type = 'DEPOSIT' AND status = 'COMPLETED')
         THEN fee
        ELSE 0 END)                                                    AS FEE,
       (CASE WHEN (PAYMENT.type = 'DEPOSIT' AND status = 'COMPLETED')
         THEN 1
        ELSE 0 END)                                                    AS DEPOSIT_COUNT,
       (CASE WHEN (PAYMENT.type = 'WITHDRAWAL' AND status = 'COMPLETED')
         THEN amount
        ELSE 0 END)                                                    AS WITHDRAWAL,
       (CASE WHEN (PAYMENT.type = 'WITHDRAWAL' AND status = 'COMPLETED')
         THEN 1
        ELSE 0 END)                                                    AS WITHDRAWAL_COUNT,
       CAST(CONVERT(VARCHAR(6), REQUEST_DATE, 112) + '01' AS DATETIME) AS MONTH,
       CAST(CONVERT(VARCHAR(4), REQUEST_DATE, 112) AS DATETIME)        AS YEAR
     FROM
       PAYMENT
       LEFT JOIN account ON account.id = payment.account_id
       LEFT JOIN external_mpt.user_conf ON user_conf.partyid = account.partyid
    ) a
  GROUP BY
    partyid,
    brandid,
    currency,
    method,
    summary_date,
    process_date,
    month,
    year
go
