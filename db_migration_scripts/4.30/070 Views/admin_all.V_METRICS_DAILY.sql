set ansi_nulls, quoted_identifier on
go
create or alter view [admin_all].[V_METRICS_DAILY] as select     SUMMARY_DATE,     count(a.partyid) as SIGNUP,     sum(funded) as FUNDED,     CAST(CONVERT(VARCHAR(6),summary_date,112) + '01' as DATETIME) as MONTH,     CAST(CONVERT(VARCHAR(4),summary_date,112) as DATETIME) as YEAR     from     (         select             user_conf.partyid,             account.id,             CAST(CONVERT(VARCHAR(8),REG_DATE,112) as DATETIME) as SUMMARY_DATE,             (case when account.id in (select account_id from payment where type='DEPOSIT' and STATUS='COMPLETED') then 1 else 0 end) as funded         from             external_mpt.user_conf             left join account on account.PARTYID = user_conf.PARTYID     ) a group by summary_date
go
