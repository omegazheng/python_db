set ansi_nulls, quoted_identifier on
go
create or alter view [admin_all].[v_banking_activity_report]
as
  select
    p.id as PAYMENT_ID,
    u.partyid as PARTYID,
    u.userid as USERID,
    u.first_name as FIRST_NAME,
    u.last_name as LAST_NAME,
    b.brandname,
    b.brandid,
    p.TYPE as TRANSACTION_TYPE,
    p.METHOD as TRANSACTION_METHOD,
    p.INFO as ACCOUNT_NUMBER,
    p.AMOUNT as REQUESTED_AMOUNT,
    p.AMOUNT as AMOUNT,
    p.fee as FEE,
    p.STATUS as STATUS,
    p.REQUEST_DATE as REQUEST_DATE,
    p.PROCESS_DATE as PROCESS_DATE,
    u.CURRENCY as CURRENCY,
    p.MISC as MISC,
    p.CONV_RATE_AFTER_MARGIN as CONV_RATE_AFTER_MARGIN,
    p.CONV_AMOUNT as CONV_AMOUNT,
    p.CONV_CURRENCY as CONV_CURRENCY
  from
    PAYMENT p,
    external_mpt.USER_CONF u,
    ACCOUNT a,
    [admin].CASINO_BRAND_DEF b
  where
    p.account_id = a.id
    and u.BRANDID = b.brandid
    and a.partyid = u.partyid
    and p.REQUEST_AMOUNT is NULL
    and p.TYPE='DEPOSIT'
    and p.STATUS='COMPLETED'
  union
  select
    p.id as PAYMENT_ID,
    u.partyid as PARTYID,
    u.userid as USERID,
    u.first_name as FIRST_NAME,
    u.last_name as LAST_NAME,
    b.brandname,
    b.brandid,
    p.TYPE as TRANSACTION_TYPE,
    p.METHOD as TRANSACTION_METHOD,
    p.INFO as ACCOUNT_NUMBER,
    p.REQUEST_AMOUNT as REQUESTED_AMOUNT,
    p.AMOUNT as AMOUNT,
    p.fee as FEE,
    p.STATUS as STATUS,
    p.REQUEST_DATE as REQUEST_DATE,
    p.PROCESS_DATE as PROCESS_DATE,
    u.CURRENCY as CURRENCY,
    p.MISC as MISC,
    p.CONV_RATE_AFTER_MARGIN as CONV_RATE_AFTER_MARGIN,
    p.CONV_AMOUNT as CONV_AMOUNT,
    p.CONV_CURRENCY as CONV_CURRENCY
  from
    PAYMENT p,
    external_mpt.USER_CONF u,
    ACCOUNT a,
    [admin].CASINO_BRAND_DEF b
  where
    p.account_id = a.id
    and u.BRANDID = b.brandid
    and a.partyid = u.partyid
    and p.REQUEST_AMOUNT IS NOT NULL
    and p.TYPE='DEPOSIT'
    and p.STATUS='COMPLETED'
  union
  select
    p.id as PAYMENT_ID,
    u.partyid as PARTYID,
    u.userid as USERID,
    u.first_name as FIRST_NAME,
    u.last_name as LAST_NAME,
    b.brandname,
    b.BRANDID,
    p.TYPE as TRANSACTION_TYPE,
    p.METHOD as TRANSACTION_METHOD,
    p.INFO as ACCOUNT_NUMBER,
    p.AMOUNT as REQUESTED_AMOUNT,
    0 as AMOUNT,
    0 as FEE,
    p.STATUS as STATUS,
    p.REQUEST_DATE as REQUEST_DATE,
    p.PROCESS_DATE as PROCESS_DATE,
    u.CURRENCY as CURRENCY,
    p.MISC as MISC,
    p.CONV_RATE_AFTER_MARGIN as CONV_RATE_AFTER_MARGIN,
    p.CONV_AMOUNT as CONV_AMOUNT,
    p.CONV_CURRENCY as CONV_CURRENCY
  from
    PAYMENT p,
    external_mpt.USER_CONF u,
    ACCOUNT a,
    [admin].CASINO_BRAND_DEF b
  where
    p.account_id = a.id
    and u.BRANDID = b.brandid
    and a.partyid = u.partyid
    and p.TYPE='DEPOSIT'
    and p.STATUS <> 'COMPLETED'
  union
  select
    p.id as PAYMENT_ID,
    u.partyid as PARTYID,
    u.userid as USERID,
    u.first_name as FIRST_NAME,
    u.last_name as LAST_NAME,
    b.brandname,
    b.BRANDID,
    p.TYPE as TRANSACTION_TYPE,
    p.METHOD as TRANSACTION_METHOD,
    p.INFO as ACCOUNT_NUMBER,
    p.AMOUNT as REQUESTED_AMOUNT,
    w.PROCESSED_AMOUNT as AMOUNT,
    0 as FEE,
    w.STATUS as STATUS,
    p.REQUEST_DATE as REQUEST_DATE,
    p.PROCESS_DATE as PROCESS_DATE,
    u.CURRENCY as CURRENCY,
    p.MISC as MISC,
    p.CONV_RATE_AFTER_MARGIN as CONV_RATE_AFTER_MARGIN,
    p.CONV_AMOUNT as CONV_AMOUNT,
    p.CONV_CURRENCY as CONV_CURRENCY
  from
    PAYMENT p,
    external_mpt.USER_CONF u,
    ACCOUNT a,
    [admin].CASINO_BRAND_DEF b,
    (
      select
        account_id,
        sum(case when status='COMPLETED' then (amount_real+amount_bonus_winnings+amount_bonus) else 0 end) as PROCESSED_AMOUNT,
        payment_id,
        status
      from
        WITHDRAWALS
      where
        status <> 'FAILED'
      group by
        account_id, payment_id, status
    ) w
  where
    p.account_id = a.id
    and u.BRANDID = b.brandid
    and a.partyid = u.partyid
    and w.payment_id=p.id
  union
  select
    p.id as PAYMENT_ID,
    u.partyid as PARTYID,
    u.userid as USERID,
    u.first_name as FIRST_NAME,
    u.last_name as LAST_NAME,
    b.brandname,
    b.BRANDID,
    p.TYPE as TRANSACTION_TYPE,
    p.METHOD as TRANSACTION_METHOD,
    p.INFO as ACCOUNT_NUMBER,
    p.AMOUNT as REQUESTED_AMOUNT,
    0 as AMOUNT,
    0 as FEE,
    p.STATUS as STATUS,
    p.REQUEST_DATE as REQUEST_DATE,
    p.PROCESS_DATE as PROCESS_DATE,
    u.CURRENCY as CURRENCY,
    p.MISC as MISC,
    p.CONV_RATE_AFTER_MARGIN as CONV_RATE_AFTER_MARGIN,
    p.CONV_AMOUNT as CONV_AMOUNT,
    p.CONV_CURRENCY as CONV_CURRENCY
  from
    PAYMENT p,
    external_mpt.USER_CONF u,
    ACCOUNT a,
    [admin].CASINO_BRAND_DEF b
  where
    p.account_id = a.id
    and u.BRANDID = b.brandid
    and a.partyid = u.partyid
    and p.STATUS='PENDING'
    and p.TYPE='WITHDRAWAL'
go
