set ansi_nulls, quoted_identifier on
go
  create or alter view [admin_all].[v_accounting_monthly] as select METHOD, MONTH, COUNT(*) as count, SUM(deposit) as deposit, SUM(WITHDRAWAL) as withdrawal from ( select METHOD, CAST(CONVERT(VARCHAR(6),REQUEST_DATE,112)  + '01' as DATETIME) as MONTH, (case when (type='DEPOSIT' and status='COMPLETED') then amount else 0 end) as DEPOSIT, (case when (type='WITHDRAWAL' and status='COMPLETED') then amount else 0 end) as WITHDRAWAL from PAYMENT ) a group by METHOD,MONTH
go
