set ansi_nulls, quoted_identifier on
go

create or alter view [admin_all].[v_cash_liability]
as
  select
    a.SUMMARY_DATE,
    a.DEPOSIT,
    a.FEE,
    a.WITHDRAWAL_REQUESTED,
    a.WITHDRAWAL_REQUESTED_RELEASED_BONUS,
    a.WITHDRAWAL_APPROVED,
    a.WITHDRAWAL_APPROVED_RELEASED_BONUS,
    a.ADJUSTMENT,
    a.ADJUSTMENT_BONUS,
    a.HOUSE_HOLD,
    a.HOUSE_HOLD_RELEASED_BONUS,
    a.WITHDRAWAL_PENDING,
    a.WITHDRAWAL_PENDING_RELEASED_BONUS,
    a.TRANSFER,
    a.TRANSFER_RELEASED_BONUS,
    a.TIPS,
    a.TIPS_RELEASED_BONUS,
    b.OPENING_BALANCE as OPENING_BALANCE,
    b.OPENING_BALANCE_RELEASED_BONUS as OPENING_BALANCE_RELEASED_BONUS,
    a.CLOSING_BALANCE as CLOSING_BALANCE,
    a.CLOSING_RELEASED_BONUS as CLOSING_BALANCE_RELEASED_BONUS,
    a.CURRENCY,
    a.BRANDID,
    a.AMOUNT_UNDERFLOW as UNDERFLOW
  from
      [admin_all].[dw_cash_liability_summary] a
      left join
      (
        select
          c.DATETIME as SUMMARY_DATE,
          sum(c.BALANCE_REAL) as OPENING_BALANCE,
          sum(c.RELEASED_BONUS) as OPENING_BALANCE_RELEASED_BONUS,
          u.CURRENCY as CURRENCY,
          u.BRANDID as BRANDID
        from
            [admin_all].[DAILY_BALANCE_HISTORY] c,
            [external_mpt].[USER_CONF] u,
            [admin_all].[ACCOUNT] d
        where
          d.PARTYID = u.PARTYID
          and d.id = c.account_id
        group by c.DATETIME, u.CURRENCY, u.BRANDID
      ) b
        on
          a.summary_date = b.summary_date
          and a.currency = b.currency
          and a.BRANDID = b.brandid
go
