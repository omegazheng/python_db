set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.v_PlayerAggregateBase') is null
begin
exec('
create view admin_all.v_PlayerAggregateBase with schemabinding
as
select ath.AggregateType, ath.TranType, cast(ath.Datetime as date) Datetime, ath.ProductID, ath.PartyID, ath.Currency, ath.BrandID, ath.GameID,
		sum(
				CASE
				WHEN ath.TranType in (''GAME_PLAY'', ''GAME_WIN'', ''GAME_BET'', ''CASH_OUT'', ''STAKE_DEC'', ''REFUND'')
				THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
				ELSE 0
				END) as PNL,
		sum(
				CASE
				WHEN ((ath.TranType = ''GAME_PLAY'' AND (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) < 0) OR
					ath.TranType in (''GAME_BET'',''STAKE_DEC'', ''REFUND''))
				THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
				ELSE 0
				END) as Handle,
		sum(
				CASE
				WHEN (ath.TranType = ''GAME_PLAY'' AND ath.AmountReal < 0) OR ath.TranType in (''GAME_BET'',''STAKE_DEC'', ''REFUND'')
				THEN ath.AmountReal * -1
				ELSE 0
				END) HandleReal,
		sum(
			CASE
			WHEN ath.TranType in (''GAME_PLAY'', ''GAME_WIN'', ''GAME_BET'', ''CASH_OUT'', ''STAKE_DEC'', ''REFUND'')
			THEN (ath.AmountReal) * -1
			ELSE 0
			END) PNLReal,
		sum(
				CASE
				WHEN (ath.TranType = ''GAME_PLAY'' AND ath.AmountReleasedBonus < 0) OR ath.TranType in (''GAME_BET'',''STAKE_DEC'', ''REFUND'')
				THEN ath.AmountReleasedBonus * -1
				ELSE 0
				END) HandleReleasedBonus,
		sum(
				CASE
				WHEN ath.TranType in (''GAME_PLAY'', ''GAME_WIN'', ''GAME_BET'', ''CASH_OUT'', ''STAKE_DEC'', ''REFUND'')
				THEN ath.AmountReleasedBonus * -1
				ELSE 0
				END) PNLReleasedBonus,
		sum(
				CASE
				WHEN (ath.TranType = ''GAME_PLAY'' AND ath.AmountPlayableBonus < 0) OR ath.TranType in (''GAME_BET'',''STAKE_DEC'', ''REFUND'')
				THEN ath.AmountPlayableBonus * -1
				ELSE 0
				END) HandlePlayableBonus,
		sum(
				CASE
				WHEN ath.TranType in (''GAME_PLAY'', ''GAME_WIN'', ''GAME_BET'', ''CASH_OUT'', ''STAKE_DEC'', ''REFUND'')
				THEN ath.AmountPlayableBonus * -1
				ELSE 0
				END) PNLPlayableBonus,
		sum(
				CASE
				WHEN (ath.TranType = ''BONUS_REL'' AND ath.ProductID <> -1 AND ath.GameID <> ''-UNKNOWN-'')
				THEN ath.AmountPlayableBonus * -1
				ELSE 0
				END
			) InGamePlayableBonusRelease,
		sum(
				CASE
				WHEN (ath.TranType = ''BONUS_REL'' AND ath.ProductID <> -1 AND ath.GameID <> ''-UNKNOWN-'')
				THEN ath.AmountReleasedBonus * -1
				ELSE 0
				END
			) InGameReleasedBonusRelease,
		sum(
			CASE
			WHEN ath.TranType = ''TIPS''
			THEN (ath.AmountReal) * -1
			ELSE 0
			END) TipsReal,
		sum(
			CASE
			WHEN ath.TranType = ''TIPS''
			THEN (ath.AmountPlayableBonus) * -1
			ELSE 0
			END)  TipsPlayableBonus,
		sum(
			CASE
			WHEN ath.TranType = ''TIPS''
			THEN (ath.AmountReleasedBonus) * -1
			ELSE 0
			END) TipsReleasedBonus,
		sum(ath.GameCount) GameCount,
		sum(CASE WHEN ath.TranType = ''GAME_BET'' THEN ath.TranCount else 0 end) BetCount,
		sum(TranCount) TranCount,
		sum(CASE WHEN ath.TranType = ''TIPS'' THEN ath.TranCount else 0 END) TipsCount,
		count_big(*) Count
	from admin_all.AccountTranHourlyAggregate ath
where (
				ath.TranType in (''GAME_PLAY'', ''GAME_WIN'', ''GAME_BET'', ''CASH_OUT'', ''STAKE_DEC'', ''REFUND'', ''TIPS'')  OR
				(ath.TranType = ''BONUS_REL'' AND ath.ProductID <> -1 AND ath.GameID <> ''-UNKNOWN-'')
		)
group by ath.AggregateType, ath.TranType, cast(ath.Datetime as date) , ath.ProductID, ath.PartyID, ath.Currency, ath.BrandID, ath.GameID')
end
go
if not exists(select * from sys.indexes where object_id = object_id('admin_all.v_PlayerAggregateBase') and name = 'PK_amin_all_v_PlayerAggregateBase')
begin
	create unique clustered index PK_amin_all_v_PlayerAggregateBase ON admin_all.v_PlayerAggregateBase(AggregateType, Datetime, TranType, PartyID, ProductID, GameID)
end
GO

