set ansi_nulls, quoted_identifier on
go
create or alter view admin_all.vJobExecutionHistory
as
	select	'SQL' as JobLocation,
			j.name collate database_default JobName,
			case h.run_status
				when 0 then 'Failed'
				when 1 then 'Completed'
				when 2 then 'Retrying'
				when 3 then 'Terminated'
				else 'In Progress'
			end collate database_default Status,
			maint.fn_SQLServerAgentDateTime(h.run_date, h.run_time) StartDate,
			dateadd(second, h.run_duration % 100 + (h.run_duration/100%100)*60+ (h.run_duration/10000%100)*60*60+ (h.run_duration/1000000%100) *60*60*24, maint.fn_SQLServerAgentDateTime(h.run_date, h.run_time)) EndDate,
			h.message collate database_default Message

	from msdb.dbo.sysjobs j
		inner join msdb.dbo.sysjobhistory h on h.job_id = j.job_id
	where h.step_id = 0
	union all
	select	'Omega' as JobLocation,
			JobName,
			Status,
			StartDate,
			EndDate,
			Message
	from admin_all.JobStatus

go
