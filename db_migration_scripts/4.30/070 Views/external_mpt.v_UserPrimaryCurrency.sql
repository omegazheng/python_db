set ansi_nulls, quoted_identifier on
go
if object_id ('external_mpt.v_UserPrimaryCurrency') is null
exec('
create view external_mpt.v_UserPrimaryCurrency with schemabinding
as
	select a.ID as AssociatedAccountID, aa.AssociatedPartyID, aa.PartyID, u.Currency, u.BrandID, ubc.Currency AssociatedCurrency
	from external_mpt.UserAssociatedAccount aa
		inner join external_mpt.USER_CONF u on u.PARTYID = aa.PartyID
		inner join admin_all.ACCOUNT a on a.PARTYID = aa.AssociatedPartyID
		inner join external_mpt.UserBrandCurrency ubc on ubc.PartyID = aa.AssociatedPartyID
')
go
if not exists(select * from sys.indexes where name = 'PK_external_mpt_v_UserPrimaryCurrency' and object_id = object_id('external_mpt.v_UserPrimaryCurrency'))
	create unique clustered index PK_external_mpt_v_UserPrimaryCurrency on external_mpt.v_UserPrimaryCurrency(AssociatedPartyID)
go
if not exists(select * from sys.indexes where name = 'IDX_external_mpt_v_UserPrimaryCurrency_PartyID' and object_id = object_id('external_mpt.v_UserPrimaryCurrency'))
	create index IDX_external_mpt_v_UserPrimaryCurrency_PartyID on external_mpt.v_UserPrimaryCurrency(PartyID)
go
if not exists(select * from sys.indexes where name = 'IDX_external_mpt_v_UserPrimaryCurrency_Currency' and object_id = object_id('external_mpt.v_UserPrimaryCurrency'))
	create index IDX_external_mpt_v_UserPrimaryCurrency_Currency on external_mpt.v_UserPrimaryCurrency(Currency)include (PartyID, AssociatedAccountID, BrandID)
go
if not exists(select * from sys.indexes where name = 'IDX_external_mpt_v_UserPrimaryCurrency_AssociatedAccountID' and object_id = object_id('external_mpt.v_UserPrimaryCurrency'))
	create index IDX_external_mpt_v_UserPrimaryCurrency_AssociatedAccountID on external_mpt.v_UserPrimaryCurrency(AssociatedAccountID)include (Currency, PartyID, BrandID)
GO



