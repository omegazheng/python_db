set ansi_nulls, quoted_identifier on
go
 create or alter view [admin_all].[v_accounting_daily] as select SUMMARY_DATE, METHOD, COUNT(*) as COUNT, SUM(deposit) as DEPOSIT, SUM(WITHDRAWAL) as WITHDRAWAL, MONTH, YEAR from ( select method, CAST(CONVERT(VARCHAR(8),REQUEST_DATE,112) as DATETIME) as SUMMARY_DATE, (case when (type='DEPOSIT' and status='COMPLETED') then amount else 0 end) as DEPOSIT, (case when (type='WITHDRAWAL' and status='COMPLETED') then amount else 0 end) as WITHDRAWAL, CAST(CONVERT(VARCHAR(6),REQUEST_DATE,112) + '01' as DATETIME) as MONTH, CAST(CONVERT(VARCHAR(4),REQUEST_DATE,112) as DATETIME) as YEAR from PAYMENT ) a group by METHOD,SUMMARY_DATE, MONTH, YEAR
go
