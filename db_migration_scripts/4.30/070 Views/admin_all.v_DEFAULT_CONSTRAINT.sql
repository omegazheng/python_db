set ansi_nulls, quoted_identifier on
go
create or alter view [admin_all].[v_DEFAULT_CONSTRAINT] AS
  SELECT
    db_name()            AS CONSTRAINT_CATALOG,
    t_obj.name           AS TABLE_NAME,
    user_name(c_obj.uid) AS CONSTRAINT_SCHEMA,
    c_obj.name           AS CONSTRAINT_NAME,
    col.name             AS COLUMN_NAME,
    col.colid            AS ORDINAL_POSITION,
    com.text             AS DEFAULT_CLAUSE
  FROM sysobjects c_obj JOIN syscomments com ON c_obj.id = com.id
    JOIN sysobjects t_obj ON c_obj.parent_obj = t_obj.id
    JOIN sysconstraints con ON c_obj.id = con.constid
    JOIN syscolumns col ON t_obj.id = col.id AND con.colid = col.colid
  WHERE c_obj.xtype = 'D'
go
