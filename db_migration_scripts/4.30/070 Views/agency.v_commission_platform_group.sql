set ansi_nulls, quoted_identifier on
go

create or alter view [agency].[v_commission_platform_group]
AS
    select * from
      (SELECT COMMISSION_ID,
         (SELECT CONVERT(varchar(100),list.PLATFORM_ID) + '|' FROM admin_all.COMMISSION_PLATFORM list
         WHERE list.COMMISSION_ID = cp.COMMISSION_ID
          Order by list.PLATFORM_ID FOR XML PATH('')) as PlatformGroup
       from admin_all.COMMISSION_PLATFORM cp Group By cp.COMMISSION_ID) cp_view

go
