set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_InsertFreePlayPlanGameInfo
  (
    @FREEPLAY_PLAN_ID INT,
    @GAMEINFO_ID INT
  )
as
  begin
    DECLARE @ID bigint

    insert into admin_all.FREEPLAY_PLAN_GAMEINFO
    (
      FREEPLAY_PLAN_ID,
      GAMEINFO_ID
    )
    values
      (
        @FREEPLAY_PLAN_ID,
        @GAMEINFO_ID
      )

    select @ID = @@IDENTITY
    select  id as id,
            freeplay_plan_id as freePlayPlanId,
            gameInfo_id as gameInfoId
    from admin_all.FREEPLAY_PLAN_GAMEINFO where ID = @ID

  end

go
