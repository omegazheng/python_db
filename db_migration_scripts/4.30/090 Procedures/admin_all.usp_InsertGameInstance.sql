set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_InsertGameInstance
(
	@GameTranID [nvarchar](100),
	@ProductID [int],
	@PartyID [int],
	@GameID nvarchar(100) = null
)
as
begin
	set nocount on
	INSERT INTO admin_all.GAME_INSTANCE (PARTY_ID, GAME_TRAN_ID, PRODUCT_ID, GAME_ID)
		VALUES (@PartyID, @GameTranID, @ProductID, @GameID)
	
	--exec [dbo].[usp_EnqueueGameInstance] @GameTranID, @ProductID, @PartyID, @GameID
end

go
