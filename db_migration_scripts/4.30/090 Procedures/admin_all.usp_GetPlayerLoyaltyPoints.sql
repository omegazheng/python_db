set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerLoyaltyPoints]
    (@partyid int)
AS
  BEGIN
    declare @brandid int = (select brandid from external_mpt.user_conf where partyid=@partyid)
    declare @brandloyalty int = (select loyalty_to_raw from admin_all.brand_loyalty where brandid=@brandid and is_enabled=1)

    if @brandloyalty is null or @brandloyalty = 0
    begin
        select -1 as loyaltyPoints
    end
    else
    begin
    	declare @accountLoyalty bigint = (select raw_loyalty_points from admin_all.account where partyid=@partyid)
        select @accountLoyalty / @brandloyalty as loyaltyPoints
    end
  END

go
