set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getRegistrationAccountStatus]
(@startDate DATETIME, @endDate DATETIME)
AS
BEGIN
  DECLARE @startDateLocal DATETIME
  DECLARE @endDateLocal DATETIME
  DECLARE @oneYearBack DATETIME
  DECLARE @twoYearBack DATETIME

  SET @startDateLocal = @startDate
  SET @endDateLocal = DATEADD(DD, 1, @endDate);
  SET @oneYearBack = DATEADD(YEAR, -1, @endDate)
  set @twoYearBack = DATEADD(YEAR, -2, @endDate)

  SELECT
    'TOTAL_ACCOUNTS' AS TYPE,
    GBTOTAL.total GB,
    NonGBTotal.total NonGB
  from
    (SELECT
       COUNT(PARTYID) total
     FROM external_mpt.user_conf users WHERE COUNTRY = 'GB'
    ) GBTotal,
    (
      SELECT
        COUNT(PARTYID) total
      FROM external_mpt.user_conf users WHERE COUNTRY != 'GB'
    ) NonGBTotal

  UNION

  SELECT
    'SELF_EXCLUSION' AS TYPE,
    SUM(CASE WHEN users.country = 'GB'
      THEN 1
        ELSE 0 END)  AS GB,
    SUM(CASE WHEN users.country != 'GB'
      THEN 1
        ELSE 0 END)  AS NonGB
  FROM external_mpt.user_conf users
    JOIN
    (
      SELECT DISTINCT PARTYID
      FROM [admin_all].[USER_ACTION_LOG] actionLog
      WHERE
        actionLog.ACTION_TYPE = 'LOCK_STATUS_CHANGE'
        AND actionLog.NEW_VALUE IN ('LOCK_BY_STAFF', 'LOCK_BY_PLYR', 'EXCLUDE_BY_PLYR')
        --and users.BRANDID in (:brandIds)
        AND actionLog.DATE >= @startDateLocal
        AND actionLog.DATE < @endDateLocal) selfExclusion
      ON users.partyId = selfExclusion.PARTYID

  UNION

  SELECT
    'KYC_SUSPENDED' AS TYPE,
    SUM(CASE WHEN users.country = 'GB'
      THEN 1
        ELSE 0 END)  AS GB,
    SUM(CASE WHEN users.country != 'GB'
      THEN 1
        ELSE 0 END)  AS NonGB
  FROM external_mpt.user_conf users
    JOIN
    (
      SELECT DISTINCT PARTYID
      FROM USER_ACTION_LOG actionLog
      WHERE
        FIELD_NAME = 'KycStatus'
      AND OLD_VALUE = 'REQUESTED'
      AND actionLog.NEW_VALUE = 'FAILED'
      --and users.BRANDID in (:brandIds)
      AND actionLog.DATE >= @startDateLocal
      AND actionLog.DATE < @endDateLocal) kycSuspended
      ON users.partyId = kycSuspended.PARTYID

  UNION

  SELECT
    'TIMEOUT'       AS TYPE,
    SUM(CASE WHEN users.country = 'GB'
      THEN 1
        ELSE 0 END) AS GB,
    SUM(CASE WHEN users.country != 'GB'
      THEN 1
        ELSE 0 END) AS NonGB
  FROM external_mpt.user_conf users
    JOIN
    (
      SELECT DISTINCT PARTYID
      FROM [admin_all].[USER_ACTION_LOG] actionLog
      WHERE
        actionLog.ACTION_TYPE = 'LOCK_STATUS_CHANGE'
        AND actionLog.NEW_VALUE IN ('TIMEOUT_BY_PLYR')
        --and users.BRANDID in (:brandIds)
        AND actionLog.DATE >= @startDateLocal
        AND actionLog.DATE < @endDateLocal) timedout
      ON users.partyId = timedout.PARTYID

  UNION

  SELECT
    'ACTIVE_OVER_YEAR' AS TYPE,
    SUM(CASE WHEN country = 'GB'
      THEN 1
        ELSE 0 END)    AS GB,
    SUM(CASE WHEN country != 'GB'
      THEN 1
        ELSE 0 END)    AS NonGB
  FROM
    (
      SELECT
        DISTINCT
        PARTY_ID,
        COUNTRY
      FROM
        [admin_all].[DW_GAME_PLAYER_DAILY] dw
      WHERE
        --dw.BRANDID in (:brandIds) and
        dw.SUMMARY_DATE >= @oneYearBack
        AND dw.SUMMARY_DATE < @endDateLocal
    ) ACTIVE_YEAR

  UNION

  SELECT
    'ACTIVE_BINGO'  AS TYPE,
    SUM(CASE WHEN country = 'GB'
      THEN 1
        ELSE 0 END) AS GB,
    SUM(CASE WHEN country != 'GB'
      THEN 1
        ELSE 0 END) AS NonGB
  FROM
    (
      SELECT
        DISTINCT
        PARTY_ID,
        COUNTRY
      FROM
        [admin_all].[DW_GAME_PLAYER_DAILY] dw
        JOIN admin_all.GAME_INFO g ON g.GAME_ID = dw.GAME_ID
        JOIN admin_all.PLATFORM pl ON g.PLATFORM_ID = pl.ID
      WHERE
        dw.SUMMARY_DATE >= @startDateLocal
        AND dw.SUMMARY_DATE < @endDateLocal
        AND pl.PLATFORM_TYPE = 'BINGO'
    ) ACTIVE_BINGO

  UNION

  SELECT
    'ACTIVE_CASINO' AS TYPE,
    SUM(CASE WHEN country = 'GB'
      THEN 1
        ELSE 0 END) AS GB,
    SUM(CASE WHEN country != 'GB'
      THEN 1
        ELSE 0 END) AS NonGB
  FROM
    (
      SELECT
        DISTINCT
        PARTY_ID,
        COUNTRY
      FROM
        [admin_all].[DW_GAME_PLAYER_DAILY] dw
        JOIN admin_all.GAME_INFO g ON g.GAME_ID = dw.GAME_ID
        JOIN admin_all.PLATFORM pl ON g.PLATFORM_ID = pl.ID
      WHERE
        dw.SUMMARY_DATE >= @startDateLocal
        AND dw.SUMMARY_DATE < @endDateLocal
        AND pl.PLATFORM_TYPE = 'CASINO'
    ) ACTIVE_CASINO

  -- Active Account Sports

  UNION

    SELECT
    'ACTIVE_SPORTS' AS TYPE,
    SUM(CASE WHEN country = 'GB'
      THEN 1
        ELSE 0 END) AS GB,
    SUM(CASE WHEN country != 'GB'
      THEN 1
        ELSE 0 END) AS NonGB
  FROM
    (
      SELECT
        DISTINCT
        PARTY_ID,
        COUNTRY
      FROM
        [admin_all].[DW_GAME_PLAYER_DAILY] dw
          JOIN admin_all.GAME_INFO g ON g.GAME_ID = dw.GAME_ID
          JOIN admin_all.PLATFORM pl ON g.PLATFORM_ID = pl.ID
      WHERE
        --dw.BRANDID in (:brandIds) and
        dw.SUMMARY_DATE >= @startDateLocal
        AND dw.SUMMARY_DATE < @endDateLocal
        AND pl.PLATFORM_TYPE = 'SPORTSBOOK'
    ) ACTIVE_SPORTS


  UNION

  SELECT
    'NEW_REGISTRATION' AS TYPE,
    SUM(CASE WHEN country = 'GB'
      THEN 1
        ELSE 0 END)    AS GB,
    SUM(CASE WHEN country != 'GB'
      THEN 1
        ELSE 0 END)    AS NonGB
  FROM
    (
      SELECT
        PARTYID,
        COUNTRY
      FROM
        external_mpt.user_conf
      WHERE
        REG_DATE >= @startDateLocal
        AND REG_DATE < @endDateLocal
      --and BRANDID in (:brandIds)
    ) NEW_REGISTRATION

  UNION

  SELECT
    'NEW_ACTIVE' AS TYPE,
    SUM(CASE WHEN country = 'GB'
      THEN 1
        ELSE 0 END)    AS GB,
    SUM(CASE WHEN country != 'GB'
      THEN 1
        ELSE 0 END)    AS NonGB
  FROM
    (
      SELECT
        u.PARTYID,
        COUNTRY
        FROM
      external_mpt.user_conf u
      JOIN admin_all.ACCOUNT a ON a.PARTYID = u.PARTYID
      JOIN admin_all.PAYMENT p ON p.ACCOUNT_ID = a.ID
    WHERE
      u.REG_DATE >= @startDateLocal
      AND u.REG_DATE < @endDateLocal
      AND p.TYPE = 'DEPOSIT'
      AND p.STATUS = 'COMPLETED'
    ) NEW_ACTIVE

  UNION

  SELECT
    'LOST_ACCOUNTS' AS TYPE,
    SUM(CASE WHEN country = 'GB'
      THEN 1
        ELSE 0 END)    AS GB,
    SUM(CASE WHEN country != 'GB'
      THEN 1
        ELSE 0 END)    AS NonGB
  FROM
    (
      SELECT
        DISTINCT
        PARTY_ID,
        COUNTRY
      FROM
        [admin_all].[DW_GAME_PLAYER_DAILY] dw
      WHERE
        --dw.BRANDID in (:brandIds) and
        dw.SUMMARY_DATE >= @twoYearBack
        AND dw.SUMMARY_DATE < @oneYearBack
        AND dw.PARTY_ID NOT IN
            (SELECT dw1.PARTY_ID FROM [admin_all].[DW_GAME_PLAYER_DAILY] dw1
            WHERE
              --dw.BRANDID in (:brandIds) and
              dw1.SUMMARY_DATE >= @oneYearBack
              AND dw1.SUMMARY_DATE < @endDateLocal )
    ) LOST_ACCOUNTS


END

go
