set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ComposeAlertTitle @Title varchar(max) output
as
begin
	set nocount on
	if object_id('dbo.customer') is not null
	begin
		exec sp_executesql N'select @Title = @Title + ''<''+isnull((select top 1 rtrim(name) from dbo.CUSTOMER),'''')+''-''+rtrim(@@servername)+''>''', N'@Title varchar(max) output', @Title output
		return
	end
	select @Title = @Title + '<'+rtrim(@@servername)+'>'
end

go
