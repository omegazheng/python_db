set ansi_nulls, quoted_identifier on
go

-- if id != null, just get that FreePlayPromotion (id get do not care about the status)
-- else all for ACTIVE, by code & party_id & gameId, get the active FreePlayPromotion by code & gameId & party_id, (if code == null, find the most reasonable FreePlayPromotion)
-- now promotionStatus=PENDING same as QUEUE (PENDING is for future usage)

create or alter procedure admin_all.usp_GetFreePlayPromotion
  (
    @ID bigint,
    @CODE NVARCHAR(40),
    @GAME_INFO_ID INT,
    @PARTY_ID INT
  )
as
  begin

    IF @ID IS NOT NULL
      BEGIN
        select  id as id,
          freeplay_plan_id as freePlayPlanId,
          code as code,
          status as status,
          trigger_date as triggerDate,
          last_updated_time as lastUpdatedTime,
          is_completed as isCompleted,
          party_id as partyId,
          isnull(total_balance,0) as totalBalance,
          isnull(remaining_balance,0) as remainingBalance,
          isnull(amount_won,0) as amountWon,
          default_stake_level as defaultStakeLevel,
          game_info_id as gameInfoId,
          line as line,
          isnull(coin,0) as coin,
          isnull(denomination,0) as denomination,
          isnull(bet_per_round,0) as betPerRound
        from
          admin_all.FREEPLAY_PROMOTION
        where
          ID = @ID
      END
    ELSE
      IF ((@CODE IS NOT NULL) AND (@PARTY_ID IS NOT NULL) AND (@GAME_INFO_ID IS NOT NULL))
        BEGIN
          select  id as id,
            freeplay_plan_id as freePlayPlanId,
            code as code,
            status as status,
            trigger_date as triggerDate,
            last_updated_time as lastUpdatedTime,
            is_completed as isCompleted,
            party_id as partyId,
            isnull(total_balance,0) as totalBalance,
            isnull(remaining_balance,0) as remainingBalance,
            isnull(amount_won,0) as amountWon,
            default_stake_level as defaultStakeLevel,
            game_info_id as gameInfoId,
            line as line,
            isnull(coin,0) as coin,
            isnull(denomination,0) as denomination,
            isnull(bet_per_round,0) as betPerRound
          from
            admin_all.FREEPLAY_PROMOTION
          where
            code = @CODE
            and party_id = @PARTY_ID
            and game_info_id = @GAME_INFO_ID
            and status = 'ACTIVE'
        END
      ELSE
        -- find the most reasonable. if QUEUED, set to ACTIVE. This only be called in API:initialize. (The first step of freePlay)
        BEGIN
          if @PARTY_ID is null
            begin
              raiserror('PARTY_ID should not be null ', 16,1)
              return
            end

          if @GAME_INFO_ID is null
            begin
              raiserror('GAME_INFO_ID should not be null ', 16,1)
              return
            end

          SET NOCOUNT ON
          SET XACT_ABORT ON
          DECLARE @PromotionStatus NVARCHAR(30), @FreePlayPromotionId bigint

          -- expire old record status. FreePlayPromotion and FreePlay_Plan.
          -- should care PARTY_ID=@PARTY_ID / gameInfoId.
          -- because this player may has other free game running.
          update admin_all.FREEPLAY_PROMOTION
          set STATUS='EXPIRED'
          where
            FREEPLAY_PROMOTION.STATUS in ('ACTIVE', 'QUEUE', 'PENDING')
            and PARTY_ID = @PARTY_ID and GAME_INFO_ID=@GAME_INFO_ID
            and FREEPLAY_PLAN_ID in (select ID from FREEPLAY_PLAN where FREEPLAY_PLAN.STATUS='ACTIVE' and EXPIRY_DATE < getdate() or END_DATE < getdate())

          update admin_all.FREEPLAY_PLAN set STATUS='EXPIRED' where STATUS in ('ACTIVE', 'INACTIVE') and EXPIRY_DATE < getdate()

          -- find the most reasonable. if QUEUED, set to ACTIVE
          BEGIN TRANSACTION

            SELECT TOP 1
              @FreePlayPromotionId = ID,
              @PromotionStatus = STATUS
            FROM admin_all.FREEPLAY_PROMOTION
            WHERE PARTY_ID = @PARTY_ID
                  AND status IN ('QUEUED', 'ACTIVE', 'PENDING')
                  AND game_info_id = @GAME_INFO_ID
            ORDER BY ID

            IF @@rowcount > 0
              BEGIN
                PRINT 'Found 1 FreePlayPromotion =' + cast(@FreePlayPromotionId AS VARCHAR)
                IF @PromotionStatus IN ('QUEUED', 'PENDING')
                  BEGIN
                    UPDATE admin_all.FREEPLAY_PROMOTION
                    SET STATUS = 'ACTIVE'
                    WHERE ID = @FreePlayPromotionId
                  END
                ELSE
                  PRINT 'It is active already ' + cast(@FreePlayPromotionId AS VARCHAR)
              END

            -- return result
              select  id as id,
                freeplay_plan_id as freePlayPlanId,
                code as code,
                status as status,
                trigger_date as triggerDate,
                last_updated_time as lastUpdatedTime,
                is_completed as isCompleted,
                party_id as partyId,
                isnull(total_balance,0) as totalBalance,
                isnull(remaining_balance,0) as remainingBalance,
                isnull(amount_won,0) as amountWon,
                default_stake_level as defaultStakeLevel,
                game_info_id as gameInfoId,
                line as line,
                isnull(coin,0) as coin,
                isnull(denomination,0) as denomination,
                isnull(bet_per_round,0) as betPerRound
              from
                admin_all.FREEPLAY_PROMOTION
              where
                ID = @FreePlayPromotionId

          COMMIT
        END

  end

go
