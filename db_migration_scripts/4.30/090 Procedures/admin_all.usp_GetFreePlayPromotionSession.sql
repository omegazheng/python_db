set ansi_nulls, quoted_identifier on
go


create or alter procedure admin_all.usp_GetFreePlayPromotionSession
  (
    @ID INT
  )
as
  begin

    select  id as id,
            freeplay_promotion_id as freePlayPromotionId,
            session as session
    from admin_all.FREEPLAY_PROMOTION_SESSION where ID = @ID

  end

go
