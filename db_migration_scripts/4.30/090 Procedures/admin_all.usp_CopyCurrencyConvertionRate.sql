set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_CopyCurrencyConvertionRate] @FromDate date, @ToDate date
as
	begin
		set nocount on
		set xact_abort on
		begin transaction
		delete b
		from admin_all.CURRENCY_CONV a
			inner join admin_all.CURRENCY_CONV_RATE b on a.ID = b.CURRENCY_CONV_ID
		where Date >= cast(@ToDate AS datetime) and Date < cast(dateadd(day, 1, @ToDate) AS datetime)
		delete a
		from admin_all.CURRENCY_CONV a
		where Date >= cast(@ToDate AS datetime) and Date < cast(dateadd(day, 1, @ToDate) AS datetime)

		select  a.BASE_CURRENCY, b.TO_CURRENCY, b.RATE into #1
		from admin_all.CURRENCY_CONV a
			inner join admin_all.CURRENCY_CONV_RATE b on a.ID = b.CURRENCY_CONV_ID
		where Date >= cast(@FromDate AS datetime) and Date < cast(dateadd(day, 1, @FromDate) AS datetime)
		create table #2(ID int, BaseCurrency nchar(3) primary key)
		insert into admin_all.CURRENCY_CONV(BASE_CURRENCY, NAME, CODE, IS_SYSTEM, DATE)
		output inserted.ID, inserted.BASE_CURRENCY into #2
			select distinct BASE_CURRENCY, 'Daily Rate ' + CONVERT(varchar(100), @ToDate, 13), 'Copied', 1, @ToDate
			from #1
		insert into admin_all.CURRENCY_CONV_RATE(CURRENCY_CONV_ID, TO_CURRENCY, RATE)
			select a.ID, b.TO_CURRENCY, b.RATE
			from #2 a
				inner join #1 b on a.BaseCurrency = b.BASE_CURRENCY collate database_default
		commit
	end

go
