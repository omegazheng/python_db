set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerLoginDeviceSummary]
  (@partyid INT)
AS
  BEGIN
    DECLARE @localPartyId INT
    SET @localPartyId = @partyid
    SELECT
      count(DEVICE) AS COUNT,
      DEVICE,
      "CATEGORY" =
                 CASE WHEN device = 'Personal computer'
                   THEN 'desktop'
                 WHEN device = 'Tablet' OR device = 'Smartphone'
                   THEN 'mobile'
                 ELSE 'others'
                 END
    FROM admin_all.user_login_log AS logs
    WHERE partyid = @localPartyId AND login_type = 'LOGIN'
    GROUP BY DEVICE
  END

go
