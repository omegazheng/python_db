set ansi_nulls, quoted_identifier on
go

-- get logins by period, [today, yesterday, week to date, month to date]
create or alter procedure [admin_all].[omegasp_getLoginByPeriod]
  (
    @staffid INT
  )
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(DAY, -32, CAST(@endDateLocal AS DATE))

    SELECT
      CAST(LOGIN_TIME AS DATE) AS DATE,
      BRANDID                  AS BRAND_ID,
      COUNT(*)                 AS LOGINS
    INTO #Temp
    FROM ADMIN_ALL.USER_LOGIN_LOG
      LEFT JOIN EXTERNAL_MPT.USER_CONF
        ON USER_LOGIN_LOG.PARTYID = USER_CONF.PARTYID
    WHERE LOGIN_TIME >= @startDateLocal
          AND LOGIN_TIME < @endDateLocal
          AND LOGIN_TYPE = 'LOGIN'
          AND BRANDID IN (SELECT BRANDID
                          FROM [admin_all].[STAFF_BRAND_TBL]
                          WHERE STAFFID = @staffid)
    GROUP BY CAST(LOGIN_TIME AS DATE), BRANDID

    SELECT
      tt.PERIOD      AS PERIOD,
      SUM(tt.LOGINS) AS LOGINS
    FROM (
           SELECT
             'TODAY'  AS PERIOD,
             BRAND_ID AS BRAND_ID,
             LOGINS   AS LOGINS
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION

           SELECT
             'YESTERDAY' AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             LOGINS      AS LOGINS
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION

           SELECT
             'WTD'       AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             SUM(LOGINS) AS LOGINS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID

           UNION

           SELECT
             'MTD'       AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             SUM(LOGINS) AS LOGINS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID
         ) tt
    GROUP BY tt.PERIOD

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END
  END

go
