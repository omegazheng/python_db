set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_MigrateROLLBACKRecords
as
begin
	set xact_abort, nocount on
	declare @i int, @BatchSize int = 150, @j int = 0
	declare @AccountID int, @DATETIME datetime, @TRAN_TYPE varchar(10), @AMOUNT_REAL numeric(38,18), @BALANCE_REAL numeric(38,18), @PLATFORM_TRAN_ID nvarchar(100), @GAME_TRAN_ID nvarchar(100), @GAME_ID varchar(100), @PLATFORM_ID int, @payment_id int, @ROLLED_BACK int, @AMOUNT_RELEASED_BONUS numeric(38,18), @AMOUNT_PLAYABLE_BONUS numeric(38,18), @BALANCE_RELEASED_BONUS numeric(38,18), @BALANCE_PLAYABLE_BONUS numeric(38,18), @AMOUNT_UNDERFLOW numeric(38,18), @AMOUNT_RAW_LOYALTY numeric(38,18), @BALANCE_RAW_LOYALTY numeric(38,18), @TRANSACTION_ON_HOLD_ID bigint, @SSW_TRAN_ID bigint, @REFERENCE varchar(100)
	declare @TransactionID bigint, @RollbackTransactionID bigint, @ProductID int, @BrandID int, @PreviousTransaction bigint
	declare c cursor static for
		select ID, ROLLBACK_TRAN_ID from admin_all.ACCOUNT_TRAN where TRAN_TYPE = 'ROLLBACK' order by DATETIME asc
	open c
	fetch next from c into @RollbackTransactionID, @TransactionID
	while @@fetch_status = 0
	begin
		if @@TRANCOUNT = 0
		begin 
			begin transaction
			select @i = 0
			--print @j
		end
	
		select	@TRAN_TYPE = TRAN_TYPE, @AMOUNT_REAL =  AMOUNT_REAL, @BALANCE_REAL = BALANCE_REAL, @PLATFORM_TRAN_ID = PLATFORM_TRAN_ID, 
				@GAME_TRAN_ID = GAME_TRAN_ID, @GAME_ID = GAME_ID, @ProductID = PLATFORM_ID, @payment_id = payment_id, 
				@ROLLED_BACK = ROLLED_BACK, /*@RollbackTransactionID = ROLLBACK_TRAN_ID,*/ @AMOUNT_RELEASED_BONUS = AMOUNT_RELEASED_BONUS, 
				@AMOUNT_PLAYABLE_BONUS = AMOUNT_PLAYABLE_BONUS, @BALANCE_PLAYABLE_BONUS = @BALANCE_RELEASED_BONUS, @BALANCE_PLAYABLE_BONUS = BALANCE_PLAYABLE_BONUS, 
				@AMOUNT_UNDERFLOW = AMOUNT_UNDERFLOW, @AMOUNT_RAW_LOYALTY = AMOUNT_RAW_LOYALTY, @BALANCE_RAW_LOYALTY = BALANCE_RAW_LOYALTY, 
				@TRANSACTION_ON_HOLD_ID = TRANSACTION_ON_HOLD_ID, @SSW_TRAN_ID = SSW_TRAN_ID, @REFERENCE = REFERENCE, @BrandID = BRAND_ID,
				@AccountID = ACCOUNT_ID, @DATETIME = DATETIME
		from admin_all.ACCOUNT_TRAN
		where ID = @TransactionID
		
		select	@AMOUNT_REAL = -@AMOUNT_REAL, @AMOUNT_PLAYABLE_BONUS = -@AMOUNT_PLAYABLE_BONUS, @AMOUNT_PLAYABLE_BONUS = -@AMOUNT_PLAYABLE_BONUS,
				@AMOUNT_RAW_LOYALTY = -@AMOUNT_RAW_LOYALTY, @AMOUNT_UNDERFLOW = -@AMOUNT_UNDERFLOW

		select	top 1
				@BALANCE_PLAYABLE_BONUS = isnull(BALANCE_PLAYABLE_BONUS,0) + @AMOUNT_PLAYABLE_BONUS,
				@BALANCE_RAW_LOYALTY = isnull(BALANCE_RAW_LOYALTY, 0) + @AMOUNT_RAW_LOYALTY,
				@BALANCE_REAL = isnull(BALANCE_REAL, 0) + @AMOUNT_REAL,
				@BALANCE_RELEASED_BONUS = isnull(BALANCE_RELEASED_BONUS, 0) + AMOUNT_RELEASED_BONUS
		from admin_all.ACCOUNT_TRAN
		where ACCOUNT_ID = @AccountID
			and DATETIME <= @DATETIME
			and TRAN_TYPE <> 'ROLLBACK'
		order by DATETIME desc

		update r	
			set
				r.TRAN_TYPE = @TRAN_TYPE,
				r.PLATFORM_TRAN_ID = @PLATFORM_TRAN_ID,
				r.GAME_TRAN_ID = @GAME_TRAN_ID, 
				r.PLATFORM_ID = @ProductID, 
				r.payment_id = @payment_id,
				r.TRANSACTION_ON_HOLD_ID = @TRANSACTION_ON_HOLD_ID,
				r.SSW_TRAN_ID = @SSW_TRAN_ID,
				r.REFERENCE = @REFERENCE,
				r.BRAND_ID = @BrandID,

				BALANCE_PLAYABLE_BONUS = @BALANCE_PLAYABLE_BONUS,
				BALANCE_RAW_LOYALTY = @BALANCE_RAW_LOYALTY,
				BALANCE_REAL = @BALANCE_REAL,
				BALANCE_RELEASED_BONUS = @BALANCE_RELEASED_BONUS

		from admin_all.ACCOUNT_TRAN r
		where  id = @RollbackTransactionID
		select @i = @i +1, @j = @j+1
		if @i = @BatchSize
		begin
			commit
		end
		fetch next from c into @RollbackTransactionID, @TransactionID
	end
	close c
	deallocate c
	if @@trancount > 0
		commit
end

go
