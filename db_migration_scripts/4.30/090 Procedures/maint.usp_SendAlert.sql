set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SendAlert 
(
	@Subject nvarchar(255) = 'mail test',
	@Body nvarchar(max) = null,
	@TestMode bit = 0
)
as
begin
	set nocount on
	declare @Recipients varchar(max)
	select @Recipients = Recipients
	from maint.AlertMailList
	where Customer = 'OMEGA'
	select @Recipients = Recipients + @Recipients
	from maint.AlertMailList
	where Customer collate database_default = (select rtrim(name) from dbo.CUSTOMER)
	--print @Recipients
	if @TestMode = 1
		select @Recipients = 'mak@omegasys.eu;zheng@omegasys.eu; robbie@omegasys.eu; leo@omegasys.eu; john.huang@sqlnotes.info;'
	exec maint.usp_ComposeAlertTitle @Subject output
	exec msdb.. sp_send_dbmail @profile_name = 'OmegaAlert', @recipients=@Recipients, @subject = @Subject, @body = @Body
end

go
