set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetRolePermissionByStaffAndSubtype]
    (@staffid int, @subtype varchar(25))
AS
  BEGIN
    SELECT
    	role_permission.*
    FROM admin_all.STAFF_ROLE
    	join admin_all.ROLE_PERMISSION on role_permission.role_id = staff_role.role_id

    where staff_role.staff_id = @staffid
    	and type='HIDE_DATA' and scope='PAGE' and SUBTYPE=@subtype
  END

go
