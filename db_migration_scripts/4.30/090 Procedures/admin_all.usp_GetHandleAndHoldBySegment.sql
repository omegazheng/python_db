set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetHandleAndHoldBySegment]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

SELECT
      PLATFORM_TYPE AS SEGMENT,
      ROUND(SUM(CONV_HANDLE),2) AS HANDLE,
      CASE WHEN SUM(CONV_HANDLE) = 0 THEN 0
      ELSE ROUND(SUM(CONV_PNL) / SUM(CONV_HANDLE) * 100, 2)
      END AS HOLD
    FROM

      (

        SELECT admin_all.fn_GetCurrencyConversionRate(dw.SUMMARY_DATE, dw.CURRENCY,'EUR')* HANDLE AS CONV_HANDLE,
          admin_all.fn_GetCurrencyConversionRate(dw.SUMMARY_DATE, dw.CURRENCY,'EUR')* PNL AS CONV_PNL,
          pl.PLATFORM_TYPE
        FROM [admin_all].[DW_GAME_PLAYER_DAILY] dw
        JOIN admin_all.GAME_INFO g ON g.GAME_ID = dw.GAME_ID
        JOIN admin_all.PLATFORM pl ON g.PLATFORM_ID = pl.ID
        WHERE dw.SUMMARY_DATE >= @startDateLocal
              AND dw.SUMMARY_DATE < @endDateLocal
              AND pl.PLATFORM_TYPE IN ('CASINO', 'BINGO', 'SPORTSBOOK')
      ) DATA
      GROUP BY PLATFORM_TYPE

   END

go
