set ansi_nulls, quoted_identifier on
go

-- For Leander to save the replayUrl
create or alter procedure admin_all.usp_GameInstanceDetail
  (
    @GameTranId nvarchar(100),
    @GameId nvarchar(100),
    @ProductId  int,
    @PartyId  int,
    @Reference  nvarchar(max)
  )
AS
  BEGIN

    set nocount on
    INSERT INTO admin_all.GAME_INSTANCE_DETAIL (PRODUCT_ID, REFERENCE, GAME_TRAN_ID, PARTY_ID, GAME_ID)
    VALUES (@ProductID, @Reference, @GameTranID, @PartyID, @GameID)

  END

go
