set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPaymentCredential]
  (@staffid int, @startDate DATETIME, @endDate DATETIME)
  AS
  BEGIN
    set nocount on

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT s.*
    FROM [admin_all].[PaymentCredential] s WITH (NOLOCK)
           INNER JOIN [admin_all].[account] a WITH (NOLOCK) ON a.id = s.accountid
           INNER JOIN [external_mpt].[user_conf] u WITH (NOLOCK) ON a.partyid = u.partyid
           INNER JOIN [admin].[casino_brand_def] b WITH (NOLOCK) ON b.brandid = u.brandid
    WHERE s.lastUpdateTime >= @startDateLocal
      AND s.lastUpdateTime < @endDateLocal
      AND u.brandId IN (SELECT distinct BRANDID
                        FROM [admin_all].[STAFF_BRAND_TBL]
                        WHERE STAFFID = @staffid)
    ORDER BY s.lastUpdateTime DESC
  END

go
