set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_MachineCloseTransaction
(
	@MachineTranID bigint = null,
	@EFTID bigint = null
)
as
begin
	set nocount on
	if @MachineTranID is null
	begin
		select @MachineTranID = MachineTranID
		from admin_all.MachineTranEFT
		where EFTID = @EFTID
	end
	if @MachineTranID is null
	begin
		raiserror('Cold not find machine transaction', 16, 1)
		return
	end
	update admin_all.MachineTran set EndDate = getdate() where MachineTranID = @MachineTranID
end

go
