set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_reporting_getAgentPlayerBetList]
(
--declare 
    @agentid   INT, --=48933,
    @startDate DATETIME, --= '2017-03-01',
    @endDate   DATETIME, --= '2018-04-01',
    @userid    VARCHAR(255) = NULL,
    @betslip   VARCHAR(255) = NULL,
    @outcome   VARCHAR(10) = NULL,
    @betStatus VARCHAR(30) = NULL
)
AS
BEGIN
	set nocount on
    DECLARE @agentidLocal INT
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @useridLocal VARCHAR(255)
    DECLARE @betslipLocal VARCHAR(255)
    DECLARE @outcomeLocal VARCHAR(10)
    DECLARE @betStatusLocal VARCHAR(30)
    SET @agentidLocal = @agentid
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate)
    SET @useridLocal = @userid
    SET @betslipLocal = @betslip
    SET @outcomeLocal = @outcome
    SET @betStatusLocal = @betStatus
	if object_id('tempdb..#BetStatus') is not null
		drop table #BetStatus
	create table #BetStatus(BetStatus varchar(30) primary key with(ignore_dup_key = on))
	insert into #BetStatus
		SELECT Item
		FROM [admin_all].fc_splitDelimiterString(@betStatusLocal, ',')
	if object_id('tempdb..#Ret') is not null
		drop table #Ret
	--set statistics xml on
    SELECT	u.PARTYID, u.USERID, u.FIRST_NAME + ' ' + u.LAST_NAME AS name, u.CURRENCY, t.DATETIME, p.CODE AS platformCode, 
			bd.id, /*aa.bets as BET_AMOUNT,  aa.wins as WINNINGS, aa.released_bonuses as BONUS,  aa.ngr,*/ bd.ACCOUNT_TRAN_ID, bd.BET_REFERENCE_NUM, bd.GAME_REFERENCE,  
			bd.BET_MODE, bd.DESCRIPTION, bd.FRONTEND_TYPE, bd.BET_STATUS, bd.SPORT_IDS, bd.TRAN_ID, bd.STATE, bd.overall_odds, bd.SETTLEMENT_DATE, 
			bd.SETTLED_EVENT_COUNT, bd.PLATFORM_ID, bd.BET_TYPE_STR, bd.IS_PAID, bd.CREATED_DATE, bd.POTENTIAL_COMMISSION, bd.AGENT_ID
			into  #Ret
    FROM external_mpt.USER_CONF u
		inner join admin_all.ACCOUNT a on a.PARTYID = u.PARTYID
		inner join admin_all.ACCOUNT_TRAN t on t.ACCOUNT_ID = a.id
		inner join admin_all.BETTING_DETAIL bd on bd.ACCOUNT_TRAN_ID = t.ID
		inner JOIN admin_all.PLATFORM p ON t.PLATFORM_ID = p.ID
		--cross apply (
  --               select
  --                 -sum(case
  --                      when a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
  --                      when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
  --                      end) ngr,
  --                 sum(case
  --                     when a.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
  --                     when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
  --                     else 0
  --                     end) wins,
  --                 -sum(case
  --                      when a.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
  --                      when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
  --                      else 0
  --                      end) bets,
  --                 sum(case
  --                     when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
  --                     else 0
  --                     end) released_bonuses
  --               from admin_all.account_tran as a 
  --                 left loop join admin_all.account_tran as r on a.rollback_tran_id = r.id
  --               where bd.bet_reference_num = a.game_tran_id
		--			and bd.platform_id = a.platform_id
		--			and a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
  --             ) aa 
      
    WHERE u.ParentID = @agentidLocal
          AND u.user_type = 0
          AND p.PLATFORM_TYPE = 'SPORTSBOOK'
          AND (@useridLocal IS NULL OR u.USERID LIKE '%' + @useridLocal + '%')
          AND (@betslipLocal IS NULL OR bd.BET_REFERENCE_NUM LIKE '%' + @betslipLocal + '%')
          AND (@outcomeLocal IS NULL OR
               (@outcomeLocal = 'CLOSED' AND bd.SETTLEMENT_DATE >= @startDateLocal AND bd.SETTLEMENT_DATE < @endDateLocal)
               OR
               (@outcomeLocal = 'OPEN' AND t.DATETIME >= @startDateLocal AND t.DATETIME < @endDateLocal AND bd.SETTLEMENT_DATE IS NULL))
          AND (@betStatusLocal IS NULL OR bd.BET_STATUS IN (select BetStatus from #BetStatus))

    --ORDER BY t.DATETIME DESC;
	SELECT	r.PARTYID, r.USERID, r.name, r.CURRENCY, r.DATETIME, r.platformCode, 
			r.id, aa.bets as BET_AMOUNT,  aa.wins as WINNINGS, aa.released_bonuses as BONUS,  aa.ngr, r.ACCOUNT_TRAN_ID, r.BET_REFERENCE_NUM, r.GAME_REFERENCE,  
			r.BET_MODE, r.DESCRIPTION, r.FRONTEND_TYPE, r.BET_STATUS, r.SPORT_IDS, r.TRAN_ID, r.STATE, r.overall_odds, r.SETTLEMENT_DATE, 
			r.SETTLED_EVENT_COUNT, r.PLATFORM_ID, r.BET_TYPE_STR, r.IS_PAID, r.CREATED_DATE, r.POTENTIAL_COMMISSION, r.AGENT_ID
	from #Ret r
	cross apply (
                 select
                   -sum(case
                        when a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                        when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                        end) ngr,
                   sum(case
                       when a.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                       when a.tran_type in ('ROLLBACK') and rb.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                       else 0
                       end) wins,
                   -sum(case
                        when a.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                        when a.tran_type in ('ROLLBACK') and rb.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                        else 0
                        end) bets,
                   sum(case
                       when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                       else 0
                       end) released_bonuses
                 from admin_all.account_tran as a 
                   left loop join admin_all.account_tran as rb on a.rollback_tran_id = rb.id
                 where r.BET_REFERENCE_NUM = a.game_tran_id
					and r.PLATFORM_ID = a.platform_id
					and a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
               ) aa 
	order by r.DATETIME desc
END


go
