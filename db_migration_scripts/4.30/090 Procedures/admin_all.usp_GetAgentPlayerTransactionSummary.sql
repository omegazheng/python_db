set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetAgentPlayerTransactionSummary]
  (
    @agentid INT,
    @startDate DATETIME,
    @endDate DATETIME,
    @userid    VARCHAR(255) = NULL,
    @platformIds VARCHAR(255) = NULL
  )
AS
  BEGIN
    DECLARE @agentidLocal INT
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @useridLocal VARCHAR(255)
    DECLARE @platformIdsLocal VARCHAR(255)
    SET @agentidLocal = @agentid
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate)
    SET @useridLocal = @userid
    SET @platformIdsLocal = @platformIds

    SELECT
      SUM(t.AMOUNT_REAL + t.AMOUNT_RELEASED_BONUS) as totalAmount,
      COUNT(*) as totalRecord
    FROM admin_all.ACCOUNT_TRAN t
      JOIN admin_all.ACCOUNT a ON t.ACCOUNT_ID = a.id
      JOIN external_mpt.USER_CONF u ON a.PARTYID = u.PARTYID
      JOIN admin_all.PLATFORM p ON t.PLATFORM_ID = p.ID
    WHERE u.ParentID = @agentidLocal
          AND u.user_type = 0
          AND p.id not in
              (select
                 platform.id
               from PLATFORM platform
                 join platform provider on platform.provider_id = provider.id and provider.code = 'BIA_SPORTBOOK'
              )
          AND t.DATETIME >= @startDateLocal AND t.DATETIME < @endDateLocal
          AND (@useridLocal IS NULL OR u.USERID LIKE '%' + @useridLocal + '%')
          AND (@platformIdsLocal IS NULL OR p.ID IN (SELECT * FROM
                                                    [admin_all].fc_splitDelimiterString(@platformIdsLocal, ',')))
  END
go
