set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicationRaiseError
as
begin
	set nocount on
	declare @Error nvarchar(4000) = ''
	select @Error= @Error + Error + '
' from maint.fn_GetReplicationErrors(@@spid)
	where Error is not null
	if @@rowcount > 0
		throw 50000, @Error, 1
end

go
