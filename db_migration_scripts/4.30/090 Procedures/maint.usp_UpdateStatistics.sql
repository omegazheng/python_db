set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_UpdateStatistics
as
begin
	set nocount on
	declare @SQL nvarchar(max)
	declare c cursor local static for
		select 'update statistics '+quotename(object_schema_name(object_id)) +'.'+ quotename(name)+' with fullscan;'
		from sys.objects o
		where exists(

		select object_schema_name(s.object_id), object_name(s.object_id), s.name, p.last_updated, p.modification_counter, p.rows
		from sys.stats s
			cross apply sys.dm_db_stats_properties(s.object_id, s.stats_id) p
		where s.object_id = o.object_id
			and p.rows < 30000000
			and(
					p.last_updated < dateadd(day, -3, getdate())
					and
					isnull(cast(p.modification_counter as float)/nullif(p.rows,0), 0) > 0.01
				)
			)

		and object_schema_name(o.object_id) not in('sys')
		and (
				o.name not like 'QRTZ_%'
			)
	open c 
	fetch next from c into @SQL
	while @@fetch_status = 0
	begin
		exec(@SQL)
		fetch next from c into @SQL
	end
	close c
	deallocate c
end

go
