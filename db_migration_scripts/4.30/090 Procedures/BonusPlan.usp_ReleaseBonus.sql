set ansi_nulls, quoted_identifier on
go

-- This will release the bonus.
-- For Playable, it will move fund from BONUS and account playableBonus to releaseBonus, status = RELEASED and update GAME BUCKET
-- For non-Playable, it will update bonus status, release amount, date, etc
-- create account transaction.
-- It will applock account-AccountId-99999 to lock the account to make sure no race condition.
create or alter procedure [BonusPlan].[usp_ReleaseBonus]
(
  @PartyID                    INT,
  @BonusID                    INT,
  @BonusPlanId                INT,
  @BonusWagerReq              numeric(38,18),
  @BonusPlayableBonus         numeric(38,18),
  @BonusPlayableBonusWinnings numeric(38,18),
  @BonusAmount                numeric(38,18),
  @PlatformId                 INT
)
AS
BEGIN
  SET NOCOUNT ON
  SET XACT_ABORT ON
  BEGIN TRANSACTION

  DECLARE @IsPlayable BIT, @AccountId INT
  DECLARE @ReleaseIncrement numeric(38,18), @AccountPlayableBonus numeric(38,18), @PlayableIncrement numeric(38,18)
  DECLARE @BalanceReal numeric(38,18), @ReleasedBonus numeric(38,18), @PlayableBonus numeric(38,18)
  DECLARE @NOW DATETIME
  DECLARE @result INT
  DECLARE @AccountTran TABLE(ID BIGINT)

  SET @IsPlayable = (SELECT IS_PLAYABLE
                     FROM admin_all.BONUS_PLAN
                     WHERE id = @BonusPlanId)

  SET @AccountId = (SELECT ID
                    FROM admin_all.ACCOUNT
                    WHERE PARTYID = @PartyID)

  EXEC @result = admin_all.usp_LockAccount @AccountID=@AccountId, @PartyID=NULL

  IF @result < 0
    BEGIN
      ROLLBACK
      RAISERROR ('Mutex for user %i is being acquired by another session.', 16, 1, @PartyID)
      RETURN
    END

  IF @IsPlayable = 1
    BEGIN

      UPDATE admin_all.bonus
      SET
        amount_wagered                 = @BonusWagerReq,
        status                         = 'RELEASED',
        release_date                   = GETDATE(),
        released_bonus                 = released_bonus + @BonusPlayableBonus,
        released_bonus_amount          = released_bonus_amount + @BonusPlayableBonus,
        released_bonus_winnings        = released_bonus_winnings + @BonusPlayableBonusWinnings,
        released_bonus_winnings_amount = released_bonus_winnings_amount + @BonusPlayableBonusWinnings,
        playable_bonus                 = 0, playable_bonus_winnings = 0
      WHERE id = @BonusID

      UPDATE admin_all.game_bonus_bucket
      SET
        RELEASED_BONUS_WINNINGS = RELEASED_BONUS_WINNINGS + PLAYABLE_BONUS_WINNINGS,
        RELEASED_BONUS          = RELEASED_BONUS + PLAYABLE_BONUS,
        MAX_RELEASED_BONUS      = MAX_RELEASED_BONUS + MAX_PLAYABLE_BONUS,
        PLAYABLE_BONUS_WINNINGS = 0,
        PLAYABLE_BONUS          = 0,
        MAX_PLAYABLE_BONUS      = 0
      WHERE bonus_id = @BonusID

      -- Now update Account
      SET @ReleaseIncrement = @BonusPlayableBonus + @BonusPlayableBonusWinnings

      SET @ReleaseIncrement = CAST(@BonusAmount AS numeric(38,18))
      SET @AccountPlayableBonus = (SELECT PLAYABLE_BONUS
                                   FROM admin_all.ACCOUNT
                                   WHERE ID = @AccountId)

      -- Do we have min function here ?
      SET @PlayableIncrement = [admin_all].[fn_MinimumNumeric] (@ReleaseIncrement, @AccountPlayableBonus)
      SET @PlayableIncrement = @PlayableIncrement * -1


      -- Make sure 2 decimal point
      SET @PlayableIncrement = Round(@PlayableIncrement, 2)
      -- Round up
      UPDATE ADMIN_ALL.ACCOUNT WITH ( ROWLOCK )
      SET
        RELEASED_BONUS = RELEASED_BONUS + @ReleaseIncrement,
        PLAYABLE_BONUS = PLAYABLE_BONUS + @PlayableIncrement
      WHERE
        ID = @AccountId

      SELECT
          @BalanceReal = BALANCE_REAL,
          @ReleasedBonus = ACCOUNT.RELEASED_BONUS,
          @PlayableBonus = ACCOUNT.PLAYABLE_BONUS
      FROM ADMIN_ALL.ACCOUNT
      WHERE
        ID = @AccountId

      -- Create Account Tran
      SET @NOW = GETDATE()
      INSERT @AccountTran EXEC admin_all.usp_InsertAccountTran @ACCOUNT_ID = @AccountId, @DATETIME = @NOW, @TRAN_TYPE = 'BONUS_REL',
                                           @AMOUNT_REAL = 0, @BALANCE_REAL = @BalanceReal,
                                           @AMOUNT_RELEASED_BONUS = @ReleaseIncrement,
                                           @BALANCE_RELEASED_BONUS = @ReleasedBonus,
                                           @AMOUNT_PLAYABLE_BONUS = @PlayableIncrement,
                                           @BALANCE_PLAYABLE_BONUS = @PlayableBonus


    END
  ELSE
    BEGIN

      -- Make sure 2 decimal point
      SET @BonusAmount = CAST(@BonusAmount AS numeric(38,18))

      UPDATE admin_all.bonus
      SET
        amount_wagered        = @BonusWagerReq,
        status                = 'RELEASED',
        release_date          = GETDATE(),
        released_bonus        = @BonusAmount,
        released_bonus_amount = @BonusAmount
      WHERE id = @BonusID

      UPDATE ADMIN_ALL.ACCOUNT WITH ( ROWLOCK )
      SET
        RELEASED_BONUS = RELEASED_BONUS + @ReleaseIncrement,
        PLAYABLE_BONUS = PLAYABLE_BONUS + @PlayableIncrement
      WHERE
        ID = @AccountId

      SELECT
          @BalanceReal = BALANCE_REAL,
          @ReleasedBonus = ACCOUNT.RELEASED_BONUS,
          @PlayableBonus = ACCOUNT.PLAYABLE_BONUS
      FROM ADMIN_ALL.ACCOUNT
      WHERE
        ID = @AccountId

      -- Create Account Tran
      SET @NOW = GETDATE()
      Insert @AccountTran EXEC admin_all.usp_InsertAccountTran @ACCOUNT_ID = @AccountId, @DATETIME = @NOW, @TRAN_TYPE = 'BONUS_REL',
                                           @AMOUNT_REAL = 0, @BALANCE_REAL = @BalanceReal,
                                           @AMOUNT_RELEASED_BONUS = @ReleaseIncrement,
                                           @BALANCE_RELEASED_BONUS = @ReleasedBonus,
                                           @BALANCE_PLAYABLE_BONUS = @PlayableBonus

    END
    SELECT ID FROM @AccountTran

  COMMIT

END

go
