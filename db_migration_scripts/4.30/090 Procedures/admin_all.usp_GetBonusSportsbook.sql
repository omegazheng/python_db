set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_GetBonusSportsbook
  (
    @BonusID int = null,
    @BonusPlanID int = null
  )
as
  begin
     if @BonusPlanID is null
       BEGIN
         select @BonusPlanID = bonus_plan_id from admin_all.bonus where id = @BonusID
       END

    IF (SELECT COUNT(*)
        from admin_all.bonus_sportsbook where bonus_id = @BonusID) >0
      BEGIN
        select
          type,
          selection,
          selection_condition,
          odds,
          odds_condition,
          market,
          market_condition,
          period,
          period_condition,
          event,
          event_condition--,
          --0 as count,
          --0 as count_condition,
          --0 as custom_field_1_name,
          --0 as custom_field_1,
          --0 as custom_field_1_condition,
          --0 as custom_field_2_name,
          --0 as custom_field_2,
          --0 as custom_field_2_condition
        from admin_all.bonus_sportsbook where bonus_id = @BonusID
      END
    ELSE
      BEGIN
        select
            type,
            selection,
            selection_condition,
            odds,
            odds_condition,
            market,
            market_condition,
            period,
            period_condition,
            event,
            event_condition--,
            --count,
            --count_condition,
            --custom_field_1_name,
            --custom_field_1,
            --custom_field_1_condition,
            --custom_field_2_name,
            --custom_field_2,
            --custom_field_2_condition
        from admin_all.bonus_plan_sportsbook where bonus_plan_id = @BonusPlanID
      END
  end

go
