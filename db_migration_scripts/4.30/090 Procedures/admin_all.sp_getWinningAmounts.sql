set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getWinningAmounts]
  (
    @startDate DATETIME,
    @endDate   DATETIME,
    @brands    VARCHAR(255)
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @brandsLocal VARCHAR(255)
    SET @startDateLocal = @startDate
    SET @endDateLocal = @endDate
    SET @brandsLocal = @brands
    SELECT
      USERID,
      PARTYID,
      SUM((GAMES.PNL_REAL + GAMES.PNL_RELEASED_BONUS) * -1) AS NET_WIN,
      SUM(GAMES.PNL_PLAYABLE_BONUS * -1)                    AS NET_WIN_BONUS,
      USERS.CURRENCY
    FROM
      admin_all.DW_GAME_PLAYER_DAILY AS GAMES
      JOIN
      external_mpt.USER_CONF AS USERS
        ON
          GAMES.PARTY_ID = USERS.PARTYID
    WHERE
      SUMMARY_DATE >= @startDateLocal
      AND SUMMARY_DATE <= @endDateLocal
      AND (@brandsLocal IS NULL OR Games.BRAND_ID IN (@brandsLocal))
    GROUP BY USERID, PARTYID, USERS.CURRENCY
  END

go
