set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationBonusPlan
  as
  begin
    set nocount on
    --exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.BONUS_PLAN', @ForceIdentityColumnInsert = 0, @ForceMatchedDataUpdate = 0, @PrimaryKeys = 'PLAN_NAME', @ExcludedColumns = null
    --admin_all.BONUS_PLAN
    begin transaction
      ;
      with s as
        (
        select [PLAN_NAME],
               [BONUS_CODE],
               [START_DATE],
               [END_DATE],
               [MAX_TRIGGER_ALL],
               [MAX_TRIGGER_PLAYER],
               [PRIORITY],
               [DEPOSIT_FIRST],
               [DEPOSIT_MIN_AMOUNT],
               [DEPOSIT_MAX_AMOUNT],
               [AMOUNT],
               [AWARD_TYPE],
               [MULTIPLIER],
               [EXPIRY_DAYS],
               [TRIGGER_TYPE],
               [WAGER_REQ],
               [CREATED_DATE],
               [STATUS],
               [IS_PLAYABLE],
               [PLAYABLE_BONUS_QUALIFIES],
               [IS_AFFILIATE_SPECIFIC],
               [IS_FREE_BET],
               [IS_FREE_BET_RETURN],
               [IS_PLAYABLE_WINNINGS_RELEASED],
               [WAGER_REQUIREMENT_ACTION],
               [IS_INCREMENTAL],
               [INCREMENTAL_COUNT],
               [IS_CANCEL_ON_WITHDRAWAL],
               [LINKED_PARENT_BONUS_PLAN_ID],
               [RECURRING_FREQUENCY],
               [RECURRING_START_HOUR],
               [RECURRING_START_MIN],
               [RECURRING_END_HOUR],
               [RECURRING_END_MIN],
               [RECURRING_DAYS],
               [RECURRING_REPEAT],
               [RECURRING_REPEAT_UNTIL],
               [MAX_TRIGGER_PER_RECURRING],
               [MAX_TRIGGER_PER_RECURRING_DAY],
               [TYPE],
               [EXTERNAL_BONUS_PLAN_ID],
               [SPORTSBOOK_PLATFORM_ID],
               [EXTERNAL_BONUS_CODE],
               [IS_SPORTSBOOK],
               [SEQ_BP_MAP_ID]
        from (
               values (N'BONUS_PLAN_EFT', N'BONUS_PLAN_EFT', N'2013-12-04 00:00:00', N'2020-12-31 00:00:00', null, null,
                       1, 0, null, null, 0.00, N'FLAT', N'BONUS', 1, N'PLTFRM_BON', 10.00, N'2013-12-04 12:48:02',
                       N'ACTIVE', 1, 0, 0, 0, 0, 0, N'RELEASE', 0, 0, 1, null, null, null, null, null, null, null,
                       null, null, null, null, N'INTERNAL', null, null, null, 0, null)
             ) v([PLAN_NAME], [BONUS_CODE], [START_DATE], [END_DATE], [MAX_TRIGGER_ALL], [MAX_TRIGGER_PLAYER],
                 [PRIORITY], [DEPOSIT_FIRST], [DEPOSIT_MIN_AMOUNT], [DEPOSIT_MAX_AMOUNT], [AMOUNT], [AWARD_TYPE],
                 [MULTIPLIER], [EXPIRY_DAYS], [TRIGGER_TYPE], [WAGER_REQ], [CREATED_DATE], [STATUS], [IS_PLAYABLE],
                 [PLAYABLE_BONUS_QUALIFIES], [IS_AFFILIATE_SPECIFIC], [IS_FREE_BET], [IS_FREE_BET_RETURN],
                 [IS_PLAYABLE_WINNINGS_RELEASED], [WAGER_REQUIREMENT_ACTION], [IS_INCREMENTAL], [INCREMENTAL_COUNT],
                 [IS_CANCEL_ON_WITHDRAWAL], [LINKED_PARENT_BONUS_PLAN_ID],
                 [RECURRING_FREQUENCY], [RECURRING_START_HOUR], [RECURRING_START_MIN], [RECURRING_END_HOUR],
                 [RECURRING_END_MIN], [RECURRING_DAYS], [RECURRING_REPEAT], [RECURRING_REPEAT_UNTIL],
                 [MAX_TRIGGER_PER_RECURRING], [MAX_TRIGGER_PER_RECURRING_DAY], [TYPE], [EXTERNAL_BONUS_PLAN_ID],
                 [SPORTSBOOK_PLATFORM_ID], [EXTERNAL_BONUS_CODE], [IS_SPORTSBOOK], [SEQ_BP_MAP_ID])
        )
        merge admin_all.BONUS_PLAN t
      using s
      on s.[PLAN_NAME] = t.[PLAN_NAME]
      when not matched then
        insert ([PLAN_NAME], [BONUS_CODE], [START_DATE], [END_DATE], [MAX_TRIGGER_ALL], [MAX_TRIGGER_PLAYER],
                [PRIORITY], [DEPOSIT_FIRST], [DEPOSIT_MIN_AMOUNT], [DEPOSIT_MAX_AMOUNT], [AMOUNT], [AWARD_TYPE],
                [MULTIPLIER], [EXPIRY_DAYS], [TRIGGER_TYPE], [WAGER_REQ], [CREATED_DATE], [STATUS], [IS_PLAYABLE],
                [PLAYABLE_BONUS_QUALIFIES], [IS_AFFILIATE_SPECIFIC], [IS_FREE_BET], [IS_FREE_BET_RETURN],
                [IS_PLAYABLE_WINNINGS_RELEASED], [WAGER_REQUIREMENT_ACTION], [IS_INCREMENTAL], [INCREMENTAL_COUNT],
                [IS_CANCEL_ON_WITHDRAWAL], [LINKED_PARENT_BONUS_PLAN_ID],
                [RECURRING_FREQUENCY], [RECURRING_START_HOUR], [RECURRING_START_MIN], [RECURRING_END_HOUR],
                [RECURRING_END_MIN], [RECURRING_DAYS], [RECURRING_REPEAT], [RECURRING_REPEAT_UNTIL],
                [MAX_TRIGGER_PER_RECURRING], [MAX_TRIGGER_PER_RECURRING_DAY], [TYPE], [EXTERNAL_BONUS_PLAN_ID],
                [SPORTSBOOK_PLATFORM_ID], [EXTERNAL_BONUS_CODE], [IS_SPORTSBOOK], [SEQ_BP_MAP_ID])
        values (s.[PLAN_NAME], s.[BONUS_CODE], s.[START_DATE], s.[END_DATE], s.[MAX_TRIGGER_ALL],
                s.[MAX_TRIGGER_PLAYER], s.[PRIORITY], s.[DEPOSIT_FIRST], s.[DEPOSIT_MIN_AMOUNT], s.[DEPOSIT_MAX_AMOUNT],
                s.[AMOUNT], s.[AWARD_TYPE], s.[MULTIPLIER], s.[EXPIRY_DAYS], s.[TRIGGER_TYPE], s.[WAGER_REQ],
                s.[CREATED_DATE], s.[STATUS], s.[IS_PLAYABLE], s.[PLAYABLE_BONUS_QUALIFIES], s.[IS_AFFILIATE_SPECIFIC],
                s.[IS_FREE_BET], s.[IS_FREE_BET_RETURN], s.[IS_PLAYABLE_WINNINGS_RELEASED],
                s.[WAGER_REQUIREMENT_ACTION], s.[IS_INCREMENTAL], s.[INCREMENTAL_COUNT], s.[IS_CANCEL_ON_WITHDRAWAL],
                s.[LINKED_PARENT_BONUS_PLAN_ID], s.[RECURRING_FREQUENCY],
                s.[RECURRING_START_HOUR], s.[RECURRING_START_MIN], s.[RECURRING_END_HOUR], s.[RECURRING_END_MIN],
                s.[RECURRING_DAYS], s.[RECURRING_REPEAT], s.[RECURRING_REPEAT_UNTIL], s.[MAX_TRIGGER_PER_RECURRING],
                s.[MAX_TRIGGER_PER_RECURRING_DAY], s.[TYPE], s.[EXTERNAL_BONUS_PLAN_ID], s.[SPORTSBOOK_PLATFORM_ID],
                s.[EXTERNAL_BONUS_CODE], s.[IS_SPORTSBOOK], s.[SEQ_BP_MAP_ID]);
    commit;
  end
go
