set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerActiveBonuses]
    (@partyid int)
AS
  BEGIN
		select b.id,
					 bp.id                              as plan_id,
					 bp.plan_name,
					 bp.trigger_type,
					 bp.IS_PLAYABLE                     as playable,
					 b.AMOUNT_WAGERED,
					 b.wager_requirement,
					 b.amount,
					 b.playable_bonus,
					 b.PLAYABLE_BONUS_WINNINGS,
					 b.released_bonus_amount,
					 b.STATUS,
					 uc.USERID,
					 uc.FIRST_NAME + ' ' + uc.LAST_NAME as name,
					 uc.CURRENCY,
					 uc.PARTYID
		FROM external_mpt.USER_CONF uc
					 JOIN admin_all.BONUS b on b.PARTYID = uc.PartyID
					 JOIN admin_all.BONUS_PLAN bp ON b.BONUS_PLAN_ID = bp.ID
		WHERE uc.PARTYID = @partyid
			AND b.STATUS IN ('ACTIVE', 'QUEUED')
		UNION
		select b.id,
					 bp.id                              as plan_id,
					 bp.plan_name,
					 bp.trigger_type,
					 bp.IS_PLAYABLE                     as playable,
					 b.AMOUNT_WAGERED,
					 b.wager_requirement,
					 b.amount,
					 b.playable_bonus,
					 b.PLAYABLE_BONUS_WINNINGS,
					 b.released_bonus_amount,
					 b.STATUS,
					 uc.USERID,
					 uc.FIRST_NAME + ' ' + uc.LAST_NAME as name,
					 uc.CURRENCY,
					 uc.PARTYID
		FROM external_mpt.UserAssociatedAccount ua
					 JOIN  external_mpt.USER_CONF uc on ua.AssociatedPartyID = uc.PARTYID
					 JOIN admin_all.BONUS b on b.PARTYID = uc.PartyID
					 JOIN admin_all.BONUS_PLAN bp ON b.BONUS_PLAN_ID = bp.ID
		WHERE ua.PARTYID = @partyid
			AND b.STATUS IN ('ACTIVE', 'QUEUED')
		ORDER BY status asc
  END

go
