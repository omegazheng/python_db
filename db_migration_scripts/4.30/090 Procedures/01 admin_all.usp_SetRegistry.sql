set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_SetRegistry @Key nvarchar(255), @Value nvarchar(max), @BrandID int = null
as
begin
	set nocount on
	if @BrandID is null
	begin
		merge admin_all.REGISTRY_HASH as t
		using (select @Key Map_Key) as s on t.MAP_KEY = s.Map_Key
		when not matched then 
			insert(MAP_KEY, VALUE)
				values(@Key, @Value)
		when matched and @Value is null then
			delete
		when matched and t.Value <> @Value then
			update set t.Value = @Value
		;
	end
	else
	begin
		;with t as
		(
			select * 
			from admin_all.BRAND_REGISTRY 
			where BRANDID = @BrandID
		)
		merge t
		using (select @Key Map_Key) as s on t.MAP_KEY = s.Map_Key
		when not matched then 
			insert(MAP_KEY, VALUE, BrandID)
				values(@Key, @Value, @BrandID)
		when matched and @Value is null then
			delete
		when matched and t.Value <> @Value then
			update set t.Value = @Value
		;
	end
end
go
