set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getFundInCustomerAccount]
  (@endDate DATETIME, @brands VARCHAR(255))
AS
  BEGIN
    DECLARE @brandsLocal VARCHAR(255)
    DECLARE @endDateLocal DATETIME
    SET @brandsLocal = @brands
    SET @endDateLocal = DATEADD(DD, 1, @endDate)
    SELECT
      sum(gb_balanceReal)    AS balanceReal,
      sum(noGBbalanceReal)   AS no_GBbalanceReal,
      sum(gb_releaseBonus)   AS releaseBonus,
      sum(noGBreleaseBonus)  AS no_GBreleaseBonus,
      sum(gb_playableBonus)  AS playableBonus,
      sum(noGBplayableBonus) AS no_GBplayableBonus,
      a.currency
    FROM
      (
        SELECT
          userTable.CURRENCY AS currency,
          (
            CASE
            WHEN userTable.country = 'GB'
              THEN accountHistory.BALANCE_REAL
            ELSE 0
            END
          )                  AS gb_balanceReal,
          (
            CASE
            WHEN userTable.country <> 'GB'
              THEN accountHistory.BALANCE_REAL
            ELSE 0
            END
          )                  AS noGBbalanceReal,
          (
            CASE
            WHEN userTable.country = 'GB'
              THEN accountHistory.RELEASED_BONUS
            ELSE 0
            END
          )                  AS gb_releaseBonus,
          (
            CASE
            WHEN userTable.country <> 'GB'
              THEN accountHistory.RELEASED_BONUS
            ELSE 0
            END
          )                  AS noGBreleaseBonus,
          (
            CASE
            WHEN userTable.country = 'GB'
              THEN accountHistory.PLAYABLE_BONUS
            ELSE 0
            END
          )                  AS gb_playableBonus,
          (
            CASE
            WHEN userTable.country <> 'GB'
              THEN accountHistory.PLAYABLE_BONUS
            ELSE 0
            END
          )                  AS noGBplayableBonus
        FROM
          [external_mpt].[USER_CONF] userTable, [admin_all].[ACCOUNT] account
          JOIN [admin_all].[DAILY_BALANCE_HISTORY] accountHistory ON accountHistory.ACCOUNT_ID = account.id
        WHERE
          account.PARTYID = userTable.PARTYID
          AND (@brandsLocal IS NULL OR userTable.BRANDID IN (@brandsLocal))
          AND accountHistory.DATETIME = @endDateLocal
      ) a
    GROUP BY a.currency;
  END

go
