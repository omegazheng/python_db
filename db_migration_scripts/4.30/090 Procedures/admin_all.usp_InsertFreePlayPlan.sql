set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_InsertFreePlayPlan]
  (
    @CODE varchar(30),
    @NAME varchar(255),
    @STATUS varchar(30),
    @DESCRIPTION varchar(300),
    @BRAND_ID INT,
    @START_DATE datetime,
    @END_DATE datetime,
    @EXPIRY_DATE datetime,
    @PRODUCT_ID INT,
    @CHANNEL varchar(30),
    @BET_PER_ROUND numeric(38,18),
    @ROUNDS INT
  )
as
  begin
    DECLARE @ID bigint

    insert into admin_all.FREEPLAY_PLAN
    (
      CODE,
      NAME,
      STATUS,
      DESCRIPTION,
      BRAND_ID,
      START_DATE,
      END_DATE,
      EXPIRY_DATE,
      PRODUCT_ID,
      CHANNEL,
      BET_PER_ROUND,
      ROUNDS
    )
    values
      (
        @CODE,
        @NAME,
        @STATUS,
        @DESCRIPTION,
        @BRAND_ID,
        @START_DATE,
        @END_DATE,
        @EXPIRY_DATE,
        @PRODUCT_ID,
        @CHANNEL,
        @BET_PER_ROUND,
        @ROUNDS
      )

    select @ID = @@IDENTITY
    select  id as id,
            code as code,
            name as name,
            status as status,
            description as description,
            brand_id as brandId,
            start_date as startDate,
            end_date as endDate,
            expiry_date as expiryDate,
            product_id as productId,
            channel as channel,
            isnull(bet_per_round,0) as betPerRound,
            rounds as rounds
    from admin_all.FREEPLAY_PLAN where ID = @ID

  end

go
