set ansi_nulls, quoted_identifier on
go


create or alter procedure [admin_all].[usp_PlayerTrackingCardStatusEquiryChecking]
(@UUID NVARCHAR(100))
AS
BEGIN
  if @UUID is null return 0

  select ub.session_key, ub.partyID, ub.MachineID, ub.last_access_time, ub.SessionType, ub.SessionState
  from admin_all.USER_WEB_SESSION ub
         join external_mpt.USER_CONF u on u.PRIMARY_WALLET_UUID = @UUID
  where ub.PARTYID = u.PARTYID
    and ub.SessionType='OFFLINE'
    and ub.SessionState in ('PENDING', 'OPEN')

END

go
