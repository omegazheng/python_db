set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_TakeSnapshotActiveSessions @SQL nvarchar(max) = null output
as
begin
	set nocount, xact_abort on
	declare @Date datetime, @rc int
	select @Date = getdate(),@SQL = null
	begin transaction
	exec sp_getapplock 'maint.usp_TakeSnapshotActiveSessions', 'Exclusive'
	insert into maint.ActiveSessions(SnapshotDate, SessionID, StartDate, BlockingSessionID, DurationInSecond, WaitType, WaitTime, WaitResource, LastWaitType, CPU, Reads, Writes, LogicalReads, TotalElapsedTime, SQLText, QueryPlan, GrantedMemory, NestedLevel, [RowCount], TransactionIsolationLevel, ExecutingManagedCode)
		select  @Date SnapshotDate, r.Session_ID SessionID, r.start_time StartDate,r.blocking_session_id BlockingSessionID, datediff(second,r.start_time, getdate()) DurationInSecond, 
			r.wait_type WaitType, r.wait_time WaitTime, r.wait_resource WaitResource, r.last_wait_type LastWaitType,
			r.cpu_time CPU, r.reads Reads, r.writes Writes, r.logical_reads LogicalReads, r.total_elapsed_time TotalElapsedTime,
			t.text SQLText, p.query_plan QueryPlan, 
			r.granted_query_memory GrantedMemory, r.nest_level NestedLevel, r.row_count [RowCount], r.transaction_isolation_level TransactionIsolationLevel, 
			r.executing_managed_code ExecutingManagedCode
		from sys.dm_exec_requests r
			cross apply sys.dm_exec_sql_text(r.sql_handle) t
			cross apply sys.dm_exec_query_plan(r.plan_handle) p
		where r.session_id not in (@@SPID)
	select @rc = @@rowcount
	commit
	waitfor delay '00:00:00.010'
	if @rc >0
		select @SQL= 'select * from maint.ActiveSessions where SnapshotDate = '+quotename(convert(varchar(max), @Date, 121), '''')
end

go
