set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddMachineCategory
(
	@CategoryID int = null output ,
	@Code nvarchar(50),
	@Limit numeric(38,18) = -1
)
as
begin
	set nocount on 
	select @CategoryID = CategoryID
	from admin_all.MachineCategory
	where Code = @Code
	if @@rowcount > 0
		return
	insert into admin_all.MachineCategory(Code, Limit)
		values(@Code, @Limit)
	select @CategoryID = scope_identity()
end
go
