set ansi_nulls, quoted_identifier on
go

-- if id != null, just get that FreePlayTran
-- else by provider_tran_is & product_id

create or alter procedure admin_all.usp_GetFreePlayTran
  (
    @ID bigint,
    @PROVIDER_TRAN_ID NVARCHAR(100),
    @PODUCT_ID INT
  )
as
  begin

    IF @ID IS NOT NULL
      BEGIN
        select  id as id,
                party_id as partyId,
                date_time as dateTime,
                tran_type as tranType,
                isnull(amount,0) as amount,
                provider_id as providerId,
                PROVIDER_TRAN_ID as providerTranId,
                game_tran_id as gameTranId,
                GAME_INFO_ID as gameInfoId,
                isnull(REMAINING_BALANCE, 0) as remainingBalance
        from admin_all.FREEPLAY_TRAN where ID = @ID
      END
    ELSE
      BEGIN
        select  id as id,
                party_id as partyId,
                date_time as dateTime,
                tran_type as tranType,
                isnull(amount,0) as amount,
                provider_id as providerId,
                PROVIDER_TRAN_ID as providerTranId,
                game_tran_id as gameTranId,
                GAME_INFO_ID as gameInfoId,
                isnull(REMAINING_BALANCE, 0) as remainingBalance
        from admin_all.FREEPLAY_TRAN
        where PROVIDER_TRAN_ID = @PROVIDER_TRAN_ID
        and provider_id=@PODUCT_ID
      END

  end

go
