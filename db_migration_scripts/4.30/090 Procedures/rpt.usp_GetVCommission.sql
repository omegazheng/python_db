set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetVCommission
(
	@AgentID int,
	@Year int
)
as
begin
	set nocount on

		--GGR(REAL)
		--Declare @AgentID Integer =   91429996

		--Declare @AgentID Integer = 91429996
-- 		set @AgentID = 91429996;
-- 		declare @Year int;
	declare @First_Date date = datefromparts(@Year, 1, 1), @Last_Date date = datefromparts(@year+1, 1, 1);
	create table #Months(Month date not null)
	insert into #Months 
		values (datefromparts(@Year, 1, 1)), (datefromparts(@Year, 2, 1)), (datefromparts(@Year, 3, 1)), (datefromparts(@Year, 4, 1)),
			(datefromparts(@Year, 5, 1)), (datefromparts(@Year, 6, 1)), (datefromparts(@Year, 7, 1)), (datefromparts(@Year, 8, 1)),
			(datefromparts(@Year, 9, 1)), (datefromparts(@Year, 10, 1)), (datefromparts(@Year, 11, 1)), (datefromparts(@Year, 12, 1))
	declare @h hierarchyid
	select @h = Hierarchy
	from external_mpt.USER_CONF
	where PARTYID = @AgentID
	;with x0 as
	(
		select u.PARTYID, a.ID as AccountID
		from external_mpt.USER_CONF u
			inner join admin_all.ACCOUNT a on a.PARTYID = u.PARTYID
		where u.PARTYID <> @AgentID
			and u.Hierarchy.IsDescendantOf(@h) = 1
	),
	x1 as
	(
		select 
				dateadd(month, datediff(month, '2000-01-01', p.PROCESS_DATE), '2000-01-01') Month,
				PROCESS_DATE,
				row_number() over(partition by p.ACCOUNT_ID order by p.PROCESS_DATE asc) rownum
		from admin_all.PAYMENT p 
			inner join x0 on x0.AccountID = p.ACCOUNT_ID
		where p.TYPE = 'DEPOSIT' and p.STATUS = 'COMPLETED'
			and p.AMOUNT_REAL > 0
	),
	x2 as
	(
		select month, Count(*) FirstDepositCount
		from x1
		where x1.rownum = 1
			and PROCESS_DATE >=@First_Date and PROCESS_DATE < @Last_Date
		group by month
	),
	x3 as
	(
		select 
				dateadd(month, datediff(month, '2000-01-01', p.PROCESS_DATE), '2000-01-01') Month,
				sum(p.AMOUNT_REAL) AMOUNT_REAL,  sum(p.AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
		from admin_all.PAYMENT p 
		where p.TYPE = 'WITHDRAWAL' and p.STATUS = 'COMPLETED'
			and exists(select * from x0 where x0.AccountID = p.ACCOUNT_ID)
			and PROCESS_DATE >=@First_Date and PROCESS_DATE < @Last_Date
		group by dateadd(month, datediff(month, '2000-01-01', p.PROCESS_DATE), '2000-01-01')
	),
	x4 as
	(
		-- NGRAR2 need to + Carry Forward NGRAR afterwards on java end;
		SELECT  DATEADD(MONTH, DATEDIFF(MONTH, 0, at.DATETIME), 0) as month,
				sum(at.AmountReal) AmountReal
				--SUM(at.AMOUNT_REAL) as GGR_REAL, sum(p.AMOUNT_RELEASED_BONUS) as WithdrawlR8, SUM(at.AMOUNT_REAL) + sum(p.AMOUNT_REAL) + sum(p.AMOUNT_RELEASED_BONUS) as NGRAR2
		FROM admin_all.AccountTranHourlyAggregate at
		WHERE at.TranType IN ('GAME_BET', 'GAME_WIN')
			and at.DATETIME >= @First_Date and at.DATETIME < @Last_Date
-- traverse all children under a given agent excluding agent himself
			and exists(select * from x0 where x0.PARTYID = at.PartyID)
		group by  DATEADD(MONTH, DATEDIFF(MONTH, 0, at.DATETIME), 0)
	),
	x5 as
	(
		select isnull(x2.month, x3.month) month, x2.FirstDepositCount, x3.AMOUNT_REAL, x3.AMOUNT_RELEASED_BONUS
		from x2
			full outer join x3 on x2.Month = x3.Month
	)
	select m.Month Month,
			isnull(x5.FirstDepositCount, 0) FirstDepositCount, 
			isnull(x4.AmountReal, 0) as GGR_REAL, isnull(x5.AMOUNT_RELEASED_BONUS, 0) WithdrawlR8,
			isnull(x4.AmountReal, 0) + isnull(x5.AMOUNT_REAL, 0) + isnull(x5.AMOUNT_RELEASED_BONUS, 0) NGRAR2
	from #Months  m
		left join x4
			full outer join x5 on x4.month = x5.month
				on m.Month = isnull(x4.month, x5.month)
	order by 1
end

go
