set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetPaymentByPlayers
(
	@AgentID int,
	@DateFrom datetime,
	@DateTo datetime
)
as
begin
	DECLARE @endDateLocal DATETIME
	SET @endDateLocal = DATEADD(DD, 1, @DateTo);
	set nocount on
	;with x0 as 
	(
		select u.PARTYID, u.ParentID, u.UserID, u.Currency, u.user_type
		from external_mpt.USER_CONF u
		where ParentID = @AgentID
		union all
		select u1.PARTYID, u1.ParentID, u1.UserID, u1.Currency, u1.user_type
		from external_mpt.USER_CONF u1
			inner join x0 on x0.PARTYID = u1.ParentID
	)
	select	p.PROCESS_DATE ProcessDate, cast(cast(datediff(minute, p.PROCESS_DATE, getdate()) as int)/60 as varchar(20)) HoursAgo, p.TYPE MethodName, p.METHOD PaymentGateway, isnull(p.AMOUNT,0) Amount, p.STATUS Status, p.MISC Message,
			u.USERID as Agent,x0.PartyID, x0.UserID, x0.ParentID, x0.Currency
	from x0
		inner join admin_all.ACCOUNT a on x0.PartyID = a.PartyID
		inner join admin_all.PAYMENT p on p.ACCOUNT_ID = a.id and p.PROCESS_DATE >=@DateFrom and p.PROCESS_DATE < @endDateLocal
		left join external_mpt.USER_CONF u on u.PARTYID = x0.ParentID
	where isnull(x0.user_type, 0) = 0
	order by u.USERID, x0.USERID
end

go
