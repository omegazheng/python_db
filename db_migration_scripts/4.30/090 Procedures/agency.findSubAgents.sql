set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findSubAgents]
    (@parent_id int)
AS
  BEGIN
    select partyid as id from external_mpt.user_conf
    where parentid = @parent_id
       and
          user_type <> 0
  END

go
