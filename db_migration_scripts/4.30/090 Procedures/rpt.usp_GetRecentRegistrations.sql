set ansi_nulls, quoted_identifier on
go
create or alter procedure [rpt].[usp_GetRecentRegistrations]
  (@startDate DATETIME, @endDate DATETIME, @AgentID int)
AS
  BEGIN

    set nocount on
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    ;with x0 as
            (
              select
                u.PARTYID,
                u.ParentID,
                u.UserID,
                u.Currency,
                u.COUNTRY,
                u.NICKNAME,
                u.FIRST_NAME,
                u.LAST_NAME,
                u.REG_DATE,
                u.ACTIVE_FLAG,
                u.user_type
              from external_mpt.USER_CONF u
              where ParentID = @AgentID
              union all
              select u1.PARTYID,
                     u1.ParentID,
                     u1.UserID,
                     u1.Currency,
                     u1.COUNTRY,
                     u1.NICKNAME,
                     u1.FIRST_NAME,
                     u1.LAST_NAME,
                     u1.REG_DATE,
                     u1.ACTIVE_FLAG,
                     u1.user_type
              from external_mpt.USER_CONF u1
                     inner join x0 on x0.PARTYID = u1.ParentID
            )
     SELECT *
     FROM x0
     WHERE x0.REG_DATE >= @startDate
       AND x0.REG_DATE < @endDateLocal
     ORDER BY x0.REG_DATE DESC
  END

go
