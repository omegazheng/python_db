set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_SetPrimaryCurrencyAccount
(
	@AssociatedPartyID int
)
as
begin
	set xact_abort, nocount on
	begin transaction
	update ua1
		set IsPrimary = 0
	from external_mpt.UserAssociatedAccount ua
		inner join external_mpt.UserAssociatedAccount ua1 on ua.PartyID = ua1.PartyID
	where ua.AssociatedPartyID = @AssociatedPartyID
		and ua1.IsPrimary = 1
	update ua1
		set IsPrimary = 1
	from external_mpt.UserAssociatedAccount ua1 
	where ua1.AssociatedPartyID = @AssociatedPartyID
	commit
end
go
