set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getDashboardOverview]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = @endDate
    SELECT
      tranTypeCurrency.CURRENCY,
      tranTypeCurrency.TRAN_TYPE,
      isnull(summary.AMOUNT_REAL, 0) AMOUNT_REAL,
      isnull(summary.AMOUNT_PLAYABLE_BONUS, 0) AMOUNT_PLAYABLE_BONUS,
      isnull(summary.AMOUNT_RELEASED_BONUS, 0) AMOUNT_RELEASED_BONUS
    FROM
      (
        SELECT
          tranTypes.TRAN_TYPE,
          ISO_CODE as CURRENCY
        FROM
          (
            SELECT 'GAME_BET' AS TRAN_TYPE
            UNION SELECT 'GAME_WIN' AS TRAN_TYPE
            UNION SELECT 'GAME_WIN_IN_GAME_RELEASE' AS TRAN_TYPE
            UNION SELECT 'BONUS_REL' AS TRAN_TYPE
            UNION SELECT 'DEPOSIT' AS TRAN_TYPE
            UNION SELECT 'WITHDRAWAL' AS TRAN_TYPE
            UNION SELECT 'IN_GAME_RELEASE_WITHDRAWAL' AS TRAN_TYPE
            UNION SELECT 'CASINO_BONUS_WITHDRAWAL' AS TRAN_TYPE
          ) tranTypes, CURRENCY
      ) tranTypeCurrency
      left join

      (
        SELECT
          ACCOUNT_TRAN.TRAN_TYPE,
          USER_CONF.CURRENCY,
          SUM(AMOUNT_REAL) AMOUNT_REAL,
          SUM(AMOUNT_PLAYABLE_BONUS) AMOUNT_PLAYABLE_BONUS,
          --       SUM(AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
          SUM(
              CASE
              WHEN (tran_type = 'BONUS_REL' and platform_id IS null and game_id is null) THEN AMOUNT_RELEASED_BONUS
              WHEN (TRAN_TYPE = 'GAME_BET' OR TRAN_TYPE = 'GAME_WIN') THEN AMOUNT_RELEASED_BONUS
              ELSE 0 END
          ) AMOUNT_RELEASED_BONUS
        FROM ACCOUNT_TRAN WITH(NOLOCK)
          JOIN ACCOUNT ON ACCOUNT_TRAN.ACCOUNT_ID = ACCOUNT.id
          JOIN EXTERNAL_MPT.USER_CONF ON ACCOUNT.PARTYID = USER_CONF.PARTYID
        WHERE DATETIME >= @startDateLocal AND ACCOUNT_TRAN.DATETIME < @endDateLocal
              AND ACCOUNT_TRAN.TRAN_TYPE IN ('GAME_BET', 'GAME_WIN', 'BONUS_REL')
              AND ACCOUNT_TRAN.ROLLED_BACK = 0
        GROUP BY
          ACCOUNT_TRAN.TRAN_TYPE,
          USER_CONF.CURRENCY

        UNION

        SELECT
          'GAME_WIN_IN_GAME_RELEASE' AS TRAN_TYPE,
          USER_CONF.CURRENCY,
          0 AMOUNT_REAL,
          0 AMOUNT_PLAYABLE_BONUS,
          SUM(
              CASE WHEN (tran_type = 'BONUS_REL' and platform_id IS NOT null and game_id is NOT null) THEN AMOUNT_RELEASED_BONUS * -1
              ELSE 0
              END
          ) AMOUNT_RELEASED_BONUS
        FROM ACCOUNT_TRAN WITH(NOLOCK)
          JOIN ACCOUNT ON ACCOUNT_TRAN.ACCOUNT_ID = ACCOUNT.id
          JOIN EXTERNAL_MPT.USER_CONF ON ACCOUNT.PARTYID = USER_CONF.PARTYID
        WHERE DATETIME >= @startDateLocal AND ACCOUNT_TRAN.DATETIME < @endDateLocal
              AND ACCOUNT_TRAN.TRAN_TYPE IN ('BONUS_REL')
              AND ACCOUNT_TRAN.ROLLED_BACK = 0
        GROUP BY
          ACCOUNT_TRAN.TRAN_TYPE,
          USER_CONF.CURRENCY

        UNION

        SELECT
          'IN_GAME_RELEASE_WITHDRAWAL' AS TRAN_TYPE,
          users.CURRENCY,
          0 AMOUNT_REAL,
          0 AMOUNT_PLAYABLE_BONUS,
          sum(WITHDRAWN_AMOUNT) as IN_GAME_RELEASE_WITHDRAWAL from BONUS_TRAN
          join bonus on bonus.id = bonus_tran.BONUS_ID
          join BONUS_PLAN on bonus.BONUS_PLAN_ID = bonus_plan.id
          join external_mpt.user_conf users on users.partyId = bonus.PARTYID
        where SUMMARY_DATE >= @startDateLocal and SUMMARY_DATE < @endDateLocal and BONUS_PLAN.IS_PLAYABLE_WINNINGS_RELEASED = 1
        group by users.CURRENCY

        UNION

        SELECT
          'CASINO_BONUS_WITHDRAWAL' AS TRAN_TYPE,
          users.CURRENCY,
          0 AMOUNT_REAL,
          0 AMOUNT_PLAYABLE_BONUS,
          sum(WITHDRAWN_AMOUNT) as IN_GAME_RELEASE_WITHDRAWAL from BONUS_TRAN
          join bonus on bonus.id = bonus_tran.BONUS_ID
          join BONUS_PLAN on bonus.BONUS_PLAN_ID = bonus_plan.id
          join external_mpt.user_conf users on users.partyId = bonus.PARTYID
        where SUMMARY_DATE >= @startDateLocal and SUMMARY_DATE < @endDateLocal and BONUS_PLAN.IS_PLAYABLE_WINNINGS_RELEASED = 0
        group by users.CURRENCY


        UNION

        SELECT
          'WITHDRAWAL' AS TRAN_TYPE,
          CURRENCY,
          AMOUNT_REAL,
          AMOUNT_PLAYABLE_BONUS,
          AMOUNT_RELEASED_BONUS
        FROM
          (
            SELECT
              USER_CONF.CURRENCY,
              SUM(AMOUNT_REAL)           AMOUNT_REAL,
              0           AMOUNT_PLAYABLE_BONUS,
              SUM(AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
            FROM ADMIN_ALL.PAYMENT WITH (NOLOCK )
              JOIN ADMIN_ALL.ACCOUNT ON PAYMENT.ACCOUNT_ID = ACCOUNT.id
              JOIN EXTERNAL_MPT.USER_CONF ON USER_CONF.PARTYID = ACCOUNT.PARTYID
            WHERE
              PAYMENT.TYPE = 'WITHDRAWAL' AND STATUS = 'COMPLETED'
              AND PROCESS_DATE >= @startDateLocal
              AND PROCESS_DATE < @endDateLocal
            GROUP BY USER_CONF.CURRENCY
          ) WITHDRAWAL

        UNION

        SELECT
          'DEPOSIT' AS TRAN_TYPE,
          CURRENCY,
          AMOUNT_REAL,
          AMOUNT_PLAYABLE_BONUS,
          AMOUNT_RELEASED_BONUS
        FROM
          (
            SELECT
              USER_CONF.CURRENCY,
              SUM(AMOUNT_REAL)           AMOUNT_REAL,
              0           AMOUNT_PLAYABLE_BONUS,
              SUM(AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
            FROM ADMIN_ALL.PAYMENT WITH (NOLOCK )
              JOIN ADMIN_ALL.ACCOUNT ON PAYMENT.ACCOUNT_ID = ACCOUNT.id
              JOIN EXTERNAL_MPT.USER_CONF ON USER_CONF.PARTYID = ACCOUNT.PARTYID
            WHERE
              PAYMENT.TYPE = 'DEPOSIT' AND STATUS = 'COMPLETED'
              AND PROCESS_DATE >= @startDateLocal
              AND PROCESS_DATE < @endDateLocal
            GROUP BY USER_CONF.CURRENCY
          ) DEPOSIT
      )
      summary on summary.TRAN_TYPE = tranTypeCurrency.TRAN_TYPE and tranTypeCurrency.CURRENCY = summary.CURRENCY
    ORDER BY tranTypeCurrency.CURRENCY, tranTypeCurrency.TRAN_TYPE
  END

go
