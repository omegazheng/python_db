set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_TurnAccountTranIntoMaintenanceMode 
as
begin
	set nocount, xact_abort on
	
	declare @SQL nvarchar(max)
/*
EditionID
1804890536 = Enterprise
1872460670 = Enterprise Edition: Core-based Licensing
610778273= Enterprise Evaluation
-2117995310 = Developer
-1534726760 = Standard
*/
	if not(
			parsename(cast(serverproperty('ProductVersion') as varchar(50)), 4) >= 9 and cast(serverproperty('EditionID') as int) in (1804890536, 1872460670, 610778273) -- 2005+, enterprise or dev edition
			or 
			parsename(cast(serverproperty('ProductVersion') as varchar(50)), 4) >= 13
			and (cast(parsename(cast(serverproperty('ProductVersion') as varchar(50)), 1) as int) > 0 or parsename(cast(serverproperty('ProductVersion') as varchar(50)), 2) >= 4457)
			and cast(serverproperty('EditionID') as int)= -1534726760 --SQL Server 2016 sp1 standard 
		)
	begin
		raiserror('SQL Server does not support database snapshot, cannot enter maintenance mode', 16, 1)
		return
	end



	exec admin_all.usp_CopyAccountTranRealtimeToAccountTranAll 0
	exec admin_all.usp_CreateDatabaseSnapshot
	
	begin transaction
	
	select @SQL = 'drop synonym admin_all.Synonym_ACCOUNT_TRAN_ALL'
	exec(@SQL)
	select @SQL = 'create synonym admin_all.Synonym_ACCOUNT_TRAN_ALL for ' + quotename(db_name()+'_snapshot')+'.admin_all.ACCOUNT_TRAN_ALL'
	exec(@SQL)

select @SQL = 'alter view admin_all.ACCOUNT_TRAN
as
select ' +stuff((select ','+quotename(name) from sys.columns where object_id = object_id('admin_all.ACCOUNT_TRAN_ALL') order by column_id for xml path(''), type).value('.','nvarchar(max)'), 1, 1, '') +'
from admin_all.ACCOUNT_TRAN_REALTIME
union all
select ' +stuff((select ','+quotename(name) from sys.columns where object_id = object_id('admin_all.ACCOUNT_TRAN_ALL') order by column_id for xml path(''), type).value('.','nvarchar(max)'), 1, 1, '') +'
from admin_all.Synonym_ACCOUNT_TRAN_ALL a
where not exists(select * from admin_all.ACCOUNT_TRAN_REALTIME b where a.ID = b.ID)
'
	exec(@SQL)
	commit
end

go
