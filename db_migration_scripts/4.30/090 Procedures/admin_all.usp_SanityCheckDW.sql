set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_SanityCheckDW @StartDate datetime = null, @EndDate datetime = null
as
begin
	set nocount, xact_abort on

  -- NOTE: EndDate should be inclusive. It will be the beginning day of the follow day
	if @EndDate is not null
		 select @EndDate = DATEADD(DAY, DATEDIFF(DAY, 0, @EndDate), 1)

	if @StartDate is null
		Select @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) -1, 0)
	-- Initialize EndDate to be First Day of current month if not been set
	if @EndDate is null
		Select @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)

	SELECT
				 isnull(summary.CURRENCY, transactions.CURRENCY) Currency,
				 (isnull(transactions.HANDLE_REAL, 0) - isnull(summary.HANDLE_REAL, 0))                     HANDLE_REAL_DIFF,
				 (isnull(transactions.HANDLE_RELEASED_BONUS, 0) - isnull(summary.HANDLE_RELEASED_BONUS, 0)) HANDLE_RB_DIFF,
				 (isnull(transactions.HANDLE_PLAYABLE_BONUS, 0) - isnull(summary.HANDLE_PLAYABLE_BONUS, 0)) HANDLE_PB_DIFF,
				 (isnull(transactions.WIN_REAL, 0) - isnull(summary.WIN_REAL, 0))                           WIN_REAL_DIFF,
				 (isnull(transactions.WIN_RELEASED_BONUS, 0) - isnull(summary.WIN_RELEASED_BONUS, 0))       WIN_RB_DIFF,
				 (isnull(transactions.WIN_PLAYABLE_BONUS, 0) - isnull(summary.WIN_PLAYABLE_BONUS, 0))       WIN_PB_DIFF
	FROM (
			 SELECT
							CURRENCY,
							SUM(HANDLE_REAL)           HANDLE_REAL,
							SUM(HANDLE_RELEASED_BONUS) HANDLE_RELEASED_BONUS,
							SUM(HANDLE_PLAYABLE_BONUS) HANDLE_PLAYABLE_BONUS,
							SUM(WIN_REAL)              WIN_REAL,
							SUM(WIN_RELEASED_BONUS)    WIN_RELEASED_BONUS,
							SUM(WIN_PLAYABLE_BONUS)    WIN_PLAYABLE_BONUS
			 FROM (
						SELECT
									 CURRENCY,
									 TRAN_TYPE,
									 CASE WHEN TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND')
														 THEN SUM(-1 * AMOUNT_REAL)
												ELSE 0 END HANDLE_REAL,
									 CASE WHEN TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND')
														 THEN SUM(-1 * AMOUNT_RELEASED_BONUS)
												ELSE 0 END HANDLE_RELEASED_BONUS,
									 CASE WHEN TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND')
														 THEN SUM(-1 * AMOUNT_PLAYABLE_BONUS)
												ELSE 0 END HANDLE_PLAYABLE_BONUS,
									 CASE WHEN TRAN_TYPE IN ('GAME_WIN', 'CASH_OUT')
														 THEN SUM(AMOUNT_REAL)
												ELSE 0 END WIN_REAL,
									 CASE WHEN TRAN_TYPE IN ('GAME_WIN', 'CASH_OUT')
														 THEN SUM(AMOUNT_RELEASED_BONUS)
												ELSE 0 END WIN_RELEASED_BONUS,
									 CASE WHEN TRAN_TYPE IN ('GAME_WIN', 'CASH_OUT')
														 THEN SUM(AMOUNT_PLAYABLE_BONUS)
												ELSE 0 END WIN_PLAYABLE_BONUS
						FROM admin_all.ACCOUNT_TRAN
									 JOIN admin_all.ACCOUNT ON ACCOUNT_TRAN.ACCOUNT_ID = ACCOUNT.id
									 JOIN EXTERNAL_MPT.USER_CONF USERS ON USERS.PARTYID = ACCOUNT.PARTYID
						WHERE ROLLED_BACK = 0 AND
										DATETIME >= @startDate
								AND DATETIME < @endDate
								AND TRAN_TYPE IN ('GAME_BET', 'STAKE_DEC', 'REFUND', 'GAME_WIN', 'CASH_OUT')
						GROUP BY USERS.CURRENCY, TRAN_TYPE
						) result
			 GROUP BY CURRENCY
			 ) transactions
				 full outer JOIN
					 (

					 SELECT
									CURRENCY,
									SUM(HANDLE_REAL)                                                                          HANDLE_REAL,
									SUM(HANDLE_RELEASED_BONUS)                                                                HANDLE_RELEASED_BONUS,
									SUM(HANDLE_PLAYABLE_BONUS)                                                                HANDLE_PLAYABLE_BONUS,
									SUM(HANDLE_REAL - PNL_REAL)                                                               WIN_REAL,
									SUM(DW_GAME_PLAYER_DAILY.HANDLE_RELEASED_BONUS - DW_GAME_PLAYER_DAILY.PNL_RELEASED_BONUS) WIN_RELEASED_BONUS,
									SUM(DW_GAME_PLAYER_DAILY.HANDLE_PLAYABLE_BONUS - DW_GAME_PLAYER_DAILY.PNL_PLAYABLE_BONUS) WIN_PLAYABLE_BONUS
					 FROM ADMIN_ALL.DW_GAME_PLAYER_DAILY
					 WHERE SUMMARY_DATE >= @startDate AND SUMMARY_DATE < @endDate
					 GROUP BY CURRENCY
					 ) summary ON transactions.CURRENCY = summary.CURRENCY
end

go
