set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetGgyAndReleasedBonus]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      'TOTAL_GGY' AS                          TYPE,
      SUMMARY.CURRENCY,
      SUMMARY.COUNTRY,
      SUM(SUMMARY.HOLD_REAL)               REAL,
      SUM(SUMMARY.HOLD_RELEASED_BONUS)     RELEASED_BONUS,
      ABS(ISNULL(BONUS_ACCOUNT_TRAN_SUMMARY.AMOUNT_PLAYABLE_BONUS, 0))+abs(isnull(sum(SUMMARY.HOLD_PLAYABLE_BONUS), 0))     PLAYABLE_BONUS
    FROM
      (
        SELECT
          DAILY.CURRENCY AS CURRENCY,
          UPPER(DAILY.COUNTRY)  AS COUNTRY,
          DAILY.HANDLE_REAL    AS HANDLE_REAL,
          DAILY.HANDLE_RELEASED_BONUS     AS HANDLE_RELEASED_BONUS,
          DAILY.HANDLE_PLAYABLE_BONUS     AS HANDLE_PLAYABLE_BONUS,
          DAILY.PNL_REAL     AS HOLD_REAL,
          DAILY.PNL_RELEASED_BONUS     AS HOLD_RELEASED_BONUS,
		  DAILY.PNL_PLAYABLE_BONUS     AS HOLD_PLAYABLE_BONUS,
          DAILY.IN_GAME_RELEASED_BONUS_RELEASE     AS IN_GAME_RELEASED_BONUS_RELEASE,
          DAILY.IN_GAME_PLAYABLE_BONUS_RELEASE     AS IN_GAME_PLAYABLE_BONUS_RELEASE

        FROM
          DW_GAME_PLAYER_DAILY DAILY
        WHERE
          DAILY.SUMMARY_DATE >= @startDateLocal
          AND DAILY.SUMMARY_DATE < @endDateLocal
      ) SUMMARY
      LEFT JOIN (
                  select
                    SUM(AmountPlayableBonus) as AMOUNT_PLAYABLE_BONUS,
                    USERTABLE.COUNTRY,
                    USERTABLE.CURRENCY
                 from bonusplan.BonusAccountTran bonusAccountTran
                    JOIN EXTERNAL_MPT.USER_CONF USERTABLE ON bonusAccountTran.PARTYID = USERTABLE.PARTYID
                  where
                    bonusAccountTran.TranType in ('GAME_BET', 'ROLLBACK', 'TIPS', 'REFUND')
                    and bonusAccountTran.DateTime >= @startDateLocal AND bonusAccountTran.DateTime < @endDateLocal
                  GROUP BY USERTABLE.COUNTRY,
                    USERTABLE.CURRENCY
                ) BONUS_ACCOUNT_TRAN_SUMMARY
        on
          SUMMARY.COUNTRY = BONUS_ACCOUNT_TRAN_SUMMARY.COUNTRY
          AND SUMMARY.CURRENCY = BONUS_ACCOUNT_TRAN_SUMMARY.CURRENCY

    GROUP BY SUMMARY.COUNTRY, SUMMARY.CURRENCY, BONUS_ACCOUNT_TRAN_SUMMARY.AMOUNT_PLAYABLE_BONUS

    UNION ALL

    SELECT
      'RELEASED_BONUS'                AS TYPE,
      CURRENCY                        AS CURRENCY,
      COUNTRY                         AS COUNTRY,
      0                               AS REAL,
      SUM(AMOUNT_RELEASED_BONUS)      AS RELEASED_BONUS,
      0                               AS PLAYABLE_BONUS
    FROM
      (
        SELECT
          CASE WHEN (tran_type = 'BONUS_REL' and platform_id IS null or game_id is null)
            THEN AMOUNT_RELEASED_BONUS
          ELSE 0
          END AS AMOUNT_RELEASED_BONUS,
          USERS.CURRENCY,
          UPPER(USERS.COUNTRY) as COUNTRY
        FROM
          ACCOUNT_TRAN
          INNER JOIN ACCOUNT ON ACCOUNT.ID = ACCOUNT_TRAN.ACCOUNT_ID
          INNER JOIN [EXTERNAL_MPT].[USER_CONF] USERS ON USERS.PARTYID = ACCOUNT.PARTYID
        WHERE
          ACCOUNT_TRAN.TRAN_TYPE = 'BONUS_REL'
          AND ACCOUNT_TRAN.ROLLED_BACK = 0
          AND ACCOUNT_TRAN.DATETIME >= @startDateLocal
          AND ACCOUNT_TRAN.DATETIME < @endDateLocal
      ) TOTAL
    GROUP BY TOTAL.COUNTRY, TOTAL.CURRENCY

    UNION ALL

    SELECT
      'IN_GAME_RELEASED_BONUS'                AS TYPE,
      CURRENCY                        AS CURRENCY,
      COUNTRY                         AS COUNTRY,
      0                               AS REAL,
      SUM(AMOUNT_RELEASED_BONUS)      AS RELEASED_BONUS,
      0                               AS PLAYABLE_BONUS
    FROM
      (
        SELECT
          CASE WHEN (tran_type = 'BONUS_REL' and platform_id IS NOT null AND game_id is NOT null)
            THEN AMOUNT_RELEASED_BONUS
          ELSE 0
          END AS AMOUNT_RELEASED_BONUS,
          USERS.CURRENCY,
          UPPER(USERS.COUNTRY) as COUNTRY
        FROM
          ACCOUNT_TRAN
          INNER JOIN ACCOUNT ON ACCOUNT.ID = ACCOUNT_TRAN.ACCOUNT_ID
          INNER JOIN [EXTERNAL_MPT].[USER_CONF] USERS ON USERS.PARTYID = ACCOUNT.PARTYID
        WHERE
          ACCOUNT_TRAN.TRAN_TYPE = 'BONUS_REL'
          AND ACCOUNT_TRAN.ROLLED_BACK = 0
          AND ACCOUNT_TRAN.DATETIME >= @startDateLocal
          AND ACCOUNT_TRAN.DATETIME < @endDateLocal
      ) TOTAL
    GROUP BY TOTAL.COUNTRY, TOTAL.CURRENCY
  END

go
