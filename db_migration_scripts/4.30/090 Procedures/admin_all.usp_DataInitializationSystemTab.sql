set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationSystemTab
as
begin
	set nocount on
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.SYSTEM_TAB', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--admin_all.SYSTEM_TAB
	begin transaction
	set identity_insert [admin_all].[SYSTEM_TAB] on;
	;with s as 
	(
		select [ID],[NAME],[COMPONENT],[MENU],[PARAMETER],[AUTO_OPEN],[URL]
		from (
				values (1,N'Home',1,N'home',null,1,N'/app/core/home/default')
					,(2,N'Player',2,N'player',null,1,N'/app/core/players/search')
			) v([ID],[NAME],[COMPONENT],[MENU],[PARAMETER],[AUTO_OPEN],[URL])
	)
	merge admin_all.SYSTEM_TAB t
	using s on s.[ID]= t.[ID]
	when not matched then
		insert ([ID],[NAME],[COMPONENT],[MENU],[PARAMETER],[AUTO_OPEN],[URL])
		values(s.[ID],s.[NAME],s.[COMPONENT],s.[MENU],s.[PARAMETER],s.[AUTO_OPEN],s.[URL])
	when matched and (s.[NAME] is null and t.[NAME] is not null or s.[NAME] is not null and t.[NAME] is null or s.[NAME] <> t.[NAME] or s.[COMPONENT] is null and t.[COMPONENT] is not null or s.[COMPONENT] is not null and t.[COMPONENT] is null or s.[COMPONENT] <> t.[COMPONENT] or s.[MENU] is null and t.[MENU] is not null or s.[MENU] is not null and t.[MENU] is null or s.[MENU] <> t.[MENU] or s.[PARAMETER] is null and t.[PARAMETER] is not null or s.[PARAMETER] is not null and t.[PARAMETER] is null or s.[PARAMETER] <> t.[PARAMETER] or s.[AUTO_OPEN] is null and t.[AUTO_OPEN] is not null or s.[AUTO_OPEN] is not null and t.[AUTO_OPEN] is null or s.[AUTO_OPEN] <> t.[AUTO_OPEN] or s.[URL] is null and t.[URL] is not null or s.[URL] is not null and t.[URL] is null or s.[URL] <> t.[URL]) then 
		update set  t.[NAME]= s.[NAME], t.[COMPONENT]= s.[COMPONENT], t.[MENU]= s.[MENU], t.[PARAMETER]= s.[PARAMETER], t.[AUTO_OPEN]= s.[AUTO_OPEN], t.[URL]= s.[URL]
	;
	set identity_insert [admin_all].[SYSTEM_TAB] off;
	commit;
end
go
