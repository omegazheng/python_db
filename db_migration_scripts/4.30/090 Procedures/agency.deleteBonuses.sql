set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[deleteBonuses]
    (@agent_id int, @plan_id int, @start_date datetime, @end_date datetime)
AS
BEGIN

    delete from agency.potential_commission
    where   agent_id = @agent_id
        and plan_id = @plan_id
        and type = 'BONUS'
        and start_date = @start_date
        and end_date = @end_date

    delete from agency.commission_payment
    where   agent_id = @agent_id
        and plan_id = @plan_id
        and type = 'BONUS'
        and start_date = @start_date
        and end_date = @end_date
        and state not like 'COMPLETED%'

END

go
