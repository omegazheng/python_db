set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.brand_CopyBrandLanguage
(
	@SourceBrandId int,
	@TargetBrandId int
)
as
begin
	INSERT INTO BRAND_LANGUAGE (BRANDID, LANGUAGE, IS_DEFAULT, LANGUAGE_COUNTRY)
		SELECT @targetBrandId, LANGUAGE, IS_DEFAULT, LANGUAGE_COUNTRY
		from BRAND_LANGUAGE
		WHERE BRANDID = @sourceBrandId
					and LANGUAGE not in (SELECT LANGUAGE from BRAND_LANGUAGE where BRANDID = @targetBrandId)
end

go
