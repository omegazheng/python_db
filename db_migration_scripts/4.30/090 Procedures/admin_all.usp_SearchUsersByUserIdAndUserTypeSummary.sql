set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_SearchUsersByUserIdAndUserTypeSummary]
  (
    @userType VARCHAR(20),
    @name VARCHAR(50)
  )
AS
  BEGIN
    DECLARE @TYPE VARCHAR(20)
    SET @TYPE = @userType;

  IF @TYPE = 'staff'
    BEGIN
      SELECT
        COUNT(*) AS totalRecord
      FROM [admin_all].[STAFF_AUTH_TBL]
      WHERE LOGINNAME like '%' + @name + '%'
    END
  IF @TYPE = 'agency'
    BEGIN
      SELECT
        COUNT(*) AS totalRecord
      FROM external_mpt.USER_CONF
      WHERE USERID like '%' + @name + '%'
        AND USER_TYPE IS NOT NULL AND user_type <> 0
    END
END

go
