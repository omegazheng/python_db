set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_SystemEFTStart
(
	
	@MachineID int,
	@PartyID int,
	@ProductTranID nvarchar(100),
	@AmountReal numeric(38, 18),
	@ReleasedBonus numeric(38, 18),
	@PlayableBonus numeric(38,18),
	@Reference varchar(max) = null
)
as
begin
	set nocount, xact_abort  on
	begin transaction
	exec admin_all.usp_LockTwoCheeryTransaction @PartyID = @PartyID, @MachineID = @MachineID


	declare @EFTID bigint, @MachineTranID bigint , @Date datetime = getdate()
	select @EFTID = next value for admin_all.SeqMachineTranEFTHistoryID

	select @MachineTranID = MachineTranID
	from admin_all.MachineTran
	where PartyID = @PartyID
		and MachineID = @MachineID
		and EndDate is null
	if @@rowcount = 0
	begin
		select @MachineTranID = @EFTID
		insert into admin_all.MachineTran(MachineTranID, PartyID, MachineID, StartDate)
			values(@MachineTranID, @PartyID, @MachineID, @Date)
	end
	
	insert into admin_all.MachineTranEFT (EFTID, MachineTranID, AccountTranID, RollbackTranID, TransferType, Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, Reference, ProductTranID)
		select @EFTID, @MachineTranID, null AccountTranID, null RollbackTranID, 'SYSTEM_EFT' TransferType, 'Pending' Status, @Date Datetime, @AmountReal, @ReleasedBonus, @PlayableBonus, @Reference, @ProductTranID
	commit
	select @EFTID as EFTID
end

go
