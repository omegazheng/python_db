set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_InsertMachineEvent
(
	@MachineID int,
	@EventKey int,
	@Datetime datetime = null,
	@Description nvarchar(max) = null,
	@SessionKey nvarchar(100) = null
)
as
begin
	set nocount on 
	insert into admin_all.MachineEvent(MachineID, EventKey, Datetime, Description, SessionKey)
		values(@MachineID, @EventKey, isnull(@Datetime, getdate()), nullif(@Description, ''), nullif(@SessionKey, ''))
end
go
