set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetGamePlayInfoByStaffAndBrands
(
	@staffid int, 
	@brands nvarchar(2000), 
	@segments nvarchar(2000),
	@AggregateType tinyint = 0
)
AS
BEGIN
	set nocount on

	declare @SourceFrom varchar(100)
	select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
	if @SourceFrom is null
	begin
		exec admin_all.usp_SetRegistry 'core.dashboard.source', 'dw'
		select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
	end

    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(DAY, -64, CAST(GETDATE() AS DATE))

	create table #Brands (BrandID int primary key)
	insert into #Brands(BrandID)
		SELECT distinct BRANDID
        FROM admin_all.STAFF_BRAND_TBL
        WHERE STAFFID = @staffid
            AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands,','))
	create table #Segments(SegmentID int primary key)
	insert into #Segments(SegmentID)
		select cast(Item as int) segmentId from admin_all.fc_splitDelimiterString(@segments,',')
	create table #Temp(
						DATE date, BRAND_ID int, GAME_PLAY int, CURRENCY nvarchar(3),
						HANDLE_REAL numeric(38,18), HANDLE_RELEASED_BONUS numeric(38,18), HANDLE_PLAYABLE_BONUS numeric(38,18),
						PNL_REAL numeric(38,18), PNL_RELEASED_BONUS numeric(38,18), PNL_PLAYABLE_BONUS numeric(38,18), 
						IN_GAME_RELEASED_BONUS_RELEASE numeric(38,18), IN_GAME_PLAYABLE_BONUS_RELEASE numeric(38,18)
					)

	if @SourceFrom = 'dw'
    begin
      insert into #Temp(DATE , BRAND_ID, GAME_PLAY, CURRENCY ,
              HANDLE_REAL, HANDLE_RELEASED_BONUS, HANDLE_PLAYABLE_BONUS,
              PNL_REAL, PNL_RELEASED_BONUS, PNL_PLAYABLE_BONUS,
              IN_GAME_RELEASED_BONUS_RELEASE, IN_GAME_PLAYABLE_BONUS_RELEASE)
        SELECT
          SUMMARY_DATE                        AS DATE,
          dw.BRAND_ID                            AS BRAND_ID,
          SUM(GAME_COUNT)                     AS GAME_PLAY,
          CURRENCY,
          SUM(HANDLE_REAL)                    AS HANDLE_REAL,
          SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
          SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
          SUM(PNL_REAL)                       AS PNL_REAL,
          SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
          SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
          SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
          SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
        FROM
          admin_all.DW_GAME_PLAYER_DAILY dw
          left join admin_all.PLATFORM pl on dw.PLATFORM_ID = pl.ID
        WHERE
          dw.SUMMARY_DATE >= @startDateLocal
          AND dw.SUMMARY_DATE < @endDateLocal
          AND DW.BRAND_ID IN (select x.BrandID from #Brands x)
          AND pl.segment_id in (select x.SegmentID from #Segments x)
        GROUP BY dw.SUMMARY_DATE, dw.BRAND_ID, dw.CURRENCY
    end

	else
    begin
      insert into #Temp(DATE , BRAND_ID, GAME_PLAY, CURRENCY ,
              HANDLE_REAL, HANDLE_RELEASED_BONUS, HANDLE_PLAYABLE_BONUS,
              PNL_REAL, PNL_RELEASED_BONUS, PNL_PLAYABLE_BONUS,
              IN_GAME_RELEASED_BONUS_RELEASE, IN_GAME_PLAYABLE_BONUS_RELEASE)
        SELECT
          Date                        AS DATE,
          dw.BrandID                            AS BRAND_ID,
          SUM(dw.GameCount)                     AS GAME_PLAY,
          CURRENCY,
          SUM(dw.HandleReal)                    AS HANDLE_REAL,
          SUM(dw.HandleReleasedBonus)          AS HANDLE_RELEASED_BONUS,
          SUM(dw.HandlePlayableBonus)          AS HANDLE_PLAYABLE_BONUS,
          SUM(dw.PNLReal)                       AS PNL_REAL,
          SUM(dw.PNLReleasedBonus)             AS PNL_RELEASED_BONUS,
          SUM(dw.PNLPlayableBonus)             AS PNL_PLAYABLE_BONUS,
          SUM(dw.InGamePlayableBonusRelease) AS IN_GAME_RELEASED_BONUS_RELEASE,
          SUM(dw.InGameReleasedBonusRelease) AS IN_GAME_PLAYABLE_BONUS_RELEASE
        FROM admin_all.fn_GetPlayerDailyAggregate(@AggregateType, @startDateLocal, dateadd(day, 1,@endDateLocal)) dw
          left join admin_all.PLATFORM pl on dw.ProductID = pl.ID
        WHERE  DW.BrandID IN (select x.BrandID from #Brands x)
          AND pl.segment_id in (select x.SegmentID from #Segments x)
        GROUP BY dw.Date, dw.BrandID, dw.Currency
    end


    SELECT
      tt.PERIOD                              AS PERIOD,
      tt.CURRENCY                            AS CURRENCY,
      SUM(tt.GAME_PLAY)                      AS GAME_PLAY,
      SUM(tt.HANDLE_REAL)                    AS HANDLE_REAL,
      SUM(tt.HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
      SUM(tt.HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
      SUM(tt.PNL_REAL)                       AS PNL_REAL,
      SUM(tt.PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
      SUM(tt.PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
      SUM(tt.IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
      SUM(tt.IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
    FROM (

           SELECT
             'TODAY'                        AS PERIOD,
             BRAND_ID                       AS BRAND_ID,
             CURRENCY,
             GAME_PLAY                      AS GAME_PLAY,
             HANDLE_REAL                    AS HANDLE_REAL,
             HANDLE_RELEASED_BONUS          AS HANDLE_RELEASED_BONUS,
             HANDLE_PLAYABLE_BONUS          AS HANDLE_PLAYABLE_BONUS,
             PNL_REAL                       AS PNL_REAL,
             PNL_RELEASED_BONUS             AS PNL_RELEASED_BONUS,
             PNL_PLAYABLE_BONUS             AS PNL_PLAYABLE_BONUS,
             IN_GAME_RELEASED_BONUS_RELEASE AS IN_GAME_RELEASED_BONUS_RELEASE,
             IN_GAME_PLAYABLE_BONUS_RELEASE AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION ALL

           SELECT
             'YESTERDAY'                    AS PERIOD,
             BRAND_ID                       AS BRAND_ID,
             CURRENCY,
             GAME_PLAY                      AS GAME_PLAY,
             HANDLE_REAL                    AS HANDLE_REAL,
             HANDLE_RELEASED_BONUS          AS HANDLE_RELEASED_BONUS,
             HANDLE_PLAYABLE_BONUS          AS HANDLE_PLAYABLE_BONUS,
             PNL_REAL                       AS PNL_REAL,
             PNL_RELEASED_BONUS             AS PNL_RELEASED_BONUS,
             PNL_PLAYABLE_BONUS             AS PNL_PLAYABLE_BONUS,
             IN_GAME_RELEASED_BONUS_RELEASE AS IN_GAME_RELEASED_BONUS_RELEASE,
             IN_GAME_PLAYABLE_BONUS_RELEASE AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION ALL

           SELECT
             'WTD'                               AS PERIOD,
             BRAND_ID                            AS BRAND_ID,
             CURRENCY,
             SUM(GAME_PLAY)                      AS GAME_PLAY,
             SUM(HANDLE_REAL)                    AS HANDLE_REAL,
             SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
             SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
             SUM(PNL_REAL)                       AS PNL_REAL,
             SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
             SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
             SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
             SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID, CURRENCY

           UNION ALL

           SELECT
             'MTD'                               AS PERIOD,
             BRAND_ID                            AS BRAND_ID,
             CURRENCY,
             SUM(GAME_PLAY)                      AS GAME_PLAY,
             SUM(HANDLE_REAL)                    AS HANDLE_REAL,
             SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
             SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
             SUM(PNL_REAL)                       AS PNL_REAL,
             SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
             SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
             SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
             SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID, CURRENCY

           UNION ALL

           SELECT
             'SPLM'                               AS PERIOD,
             BRAND_ID                            AS BRAND_ID,
             CURRENCY,
             SUM(GAME_PLAY)                      AS GAME_PLAY,
             SUM(HANDLE_REAL)                    AS HANDLE_REAL,
             SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
             SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
             SUM(PNL_REAL)                       AS PNL_REAL,
             SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
             SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
             SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
             SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal)-1, 0) AS DATE)
                 AND DATE <= cast(DATEADD(MONTH, -1, @endDateLocal) AS DATE)
           GROUP BY BRAND_ID, CURRENCY
         ) tt
    GROUP BY tt.PERIOD, tt.CURRENCY

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
    END
  END

go
