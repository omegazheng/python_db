set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CheckDailyRollback @StartDate datetime = null, @EndDate datetime = null
as
begin
	set nocount, xact_abort on

  -- NOTE: EndDate should be inclusive. It will be the beginning day of the follow day
	if @EndDate is not null
		 select @EndDate = DATEADD(DAY, DATEDIFF(DAY, 0, @EndDate), 1)

	if @StartDate is null
		Select @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) -1, 0)
	-- Initialize EndDate to be First Day of current month if not been set
	if @EndDate is null
		Select @EndDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)

	select originalTran.datetime orig, rollbacktran.datetime rollbacktran, DATEDIFF(day , originalTran.datetime, rollbacktran.DATETIME) diff,
				 u.CURRENCY,
				 PLATFORM.CODE,
				 originalTran.TRAN_TYPE, originalTran.amount_real, originalTran.AMOUNT_PLAYABLE_BONUS,
				 originalTran.AMOUNT_RELEASED_BONUS
	from admin_all.ACCOUNT_TRAN_All originalTran with(forceseek)
				 inner  join admin_all.ACCOUNT_TRAN_All rollbackTran on rollbAckTran.ROLLBACK_TRAN_ID = originalTran.id
				 join admin_all.PLATFORM on PLATFORM.id = originalTran.PLATFORM_ID
				 join admin_all.account on account.id = originalTran.account_id
				 join external_mpt.USER_CONF u on u.PARTYID = account.PARTYID
	where originalTran.DATETIME >= @StartDate AND originalTran.DATETIME < @EndDate
			and DATEDIFF(day , originalTran.datetime, rollbacktran.DATETIME) <> 0
			and originalTran.ROLLED_BACK = 1
	union all

	select originalTran.datetime orig, rollbacktran.datetime rollbacktran, DATEDIFF(day , originalTran.datetime, rollbacktran.DATETIME) diff,
				 u.CURRENCY,
				 PLATFORM.CODE,
				 originalTran.TRAN_TYPE, originalTran.amount_real, originalTran.AMOUNT_PLAYABLE_BONUS,
				 originalTran.AMOUNT_RELEASED_BONUS
	from admin_all.ACCOUNT_TRAN_REALTIME originalTran with(forceseek)
				 inner  join admin_all.ACCOUNT_TRAN_REALTIME rollbackTran on rollbAckTran.ROLLBACK_TRAN_ID = originalTran.id
				 join admin_all.PLATFORM on PLATFORM.id = originalTran.PLATFORM_ID
				 join admin_all.account on account.id = originalTran.account_id
				 join external_mpt.USER_CONF u on u.PARTYID = account.PARTYID
	where originalTran.DATETIME >= @StartDate AND originalTran.DATETIME < @EndDate
			and DATEDIFF(day , originalTran.datetime, rollbacktran.DATETIME) <> 0
			and originalTran.ROLLED_BACK = 1
	union all

	select originalTran.datetime orig, rollbacktran.datetime rollbacktran, DATEDIFF(day , originalTran.datetime, rollbacktran.DATETIME) diff,
				 u.CURRENCY,
				 PLATFORM.CODE,
				 originalTran.TRAN_TYPE, originalTran.amount_real, originalTran.AMOUNT_PLAYABLE_BONUS,
				 originalTran.AMOUNT_RELEASED_BONUS
	from admin_all.ACCOUNT_TRAN_All originalTran with(forceseek)
				 inner  join admin_all.ACCOUNT_TRAN_REALTIME rollbackTran on rollbAckTran.ROLLBACK_TRAN_ID = originalTran.id
				 join admin_all.PLATFORM on PLATFORM.id = originalTran.PLATFORM_ID
				 join admin_all.account on account.id = originalTran.account_id
				 join external_mpt.USER_CONF u on u.PARTYID = account.PARTYID
	where originalTran.DATETIME >= @StartDate AND originalTran.DATETIME < @EndDate
			and DATEDIFF(day , originalTran.datetime, rollbacktran.DATETIME) <> 0
			and originalTran.ROLLED_BACK = 1

end

go
