set ansi_nulls, quoted_identifier on
go
/*
Commit the reserve transaction:
Insert two dummy and opposite records and Do not update account balance.
*/
create or alter procedure admin_all.usp_CommitTransaction
(
  @RsvTranID bigint = null,
  @RsvPlatformTranID nvarchar(100) = null,
  @PlatformID int = null,
  @RsvCommitPlatformTranID nvarchar(100) = null,
  @GameBetPlatformTranID nvarchar(100) = null,
  @RsvCommitTranID bigint = null output,
  @Status int = null output
)
as
begin
  set nocount, xact_abort on
  declare @ACCOUNT_ID int, @DATETIME datetime, @AMOUNT_REAL numeric(38,18), @PLATFORM_TRAN_ID nvarchar(100), @GAME_TRAN_ID nvarchar(100), @GAME_ID varchar(100), @PLATFORM_ID int, @PAYMENT_ID int, @AMOUNT_RELEASED_BONUS numeric(38,18), @AMOUNT_PLAYABLE_BONUS numeric(38,18), @AMOUNT_UNDERFLOW numeric(38,18), @AMOUNT_RAW_LOYALTY numeric(38,18), @REFERENCE varchar(100), @BRAND_ID int;
  declare @A_BALANCE_REAL numeric(38,18), @A_RELEASED_BONUS numeric(38,18), @A_PLAYABLE_BONUS numeric(38,18), @A_RAW_LOYALTY_POINTS numeric(38,18);
  declare @ROLLED_BACK int

  if @RsvTranID is not null
      begin
          select @RsvTranID = ID, @PLATFORM_ID = PLATFORM_ID from admin_all.ACCOUNT_TRAN where ID = @RsvTranID
      end
  else
      begin
          select @RsvTranID = ID, @PLATFORM_ID = PLATFORM_ID from admin_all.ACCOUNT_TRAN
          where PLATFORM_TRAN_ID = @RsvPlatformTranID
            and PLATFORM_ID = @PlatformID
            and TRAN_TYPE = 'RESERVE'
      end
  if @@rowcount = 0
      begin
--       		raiserror('Reserve_Tran_Not_Found', 16, 1);
          select @Status = -1; --  'Reserve_Tran_Not_Found'
		      return
      end

  select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where ROLLBACK_TRAN_ID = @RsvTranID and PLATFORM_ID = @PLATFORM_ID and TRAN_TYPE <> 'RSV_COMMIT'
  if @@rowcount > 0
		  begin
--           raiserror('Reserve_Tran_Already_Canceled', 16, 2);
          select @Status = -2; -- Reserve_Tran_Already_Canceled
		      return
		  end

  select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where ROLLBACK_TRAN_ID = @RsvTranID and PLATFORM_ID = @PLATFORM_ID and TRAN_TYPE = 'RSV_COMMIT'
	if @@rowcount > 0
      begin
        select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where PLATFORM_TRAN_ID = @RsvCommitPlatformTranID AND PLATFORM_ID = @PLATFORM_ID AND TRAN_TYPE = 'RSV_COMMIT';
        select @Status = 1;  --   success, already processed
        return
      end

	begin transaction
      select @ACCOUNT_ID = ACCOUNT_ID, @AMOUNT_REAL =  AMOUNT_REAL,
          @PLATFORM_TRAN_ID = PLATFORM_TRAN_ID, @GAME_TRAN_ID = GAME_TRAN_ID, @GAME_ID = GAME_ID, @PLATFORM_ID = PLATFORM_ID, @PAYMENT_ID = payment_id,
          @AMOUNT_RELEASED_BONUS = AMOUNT_RELEASED_BONUS, @AMOUNT_PLAYABLE_BONUS = AMOUNT_PLAYABLE_BONUS,
          @AMOUNT_UNDERFLOW = AMOUNT_UNDERFLOW, @AMOUNT_RAW_LOYALTY = AMOUNT_RAW_LOYALTY,
          @REFERENCE = REFERENCE, @BRAND_ID = BRAND_ID
      from admin_all.ACCOUNT_TRAN where ID = @RsvTranID;

      select @A_BALANCE_REAL = BALANCE_REAL, @A_RELEASED_BONUS = RELEASED_BONUS, @A_PLAYABLE_BONUS = PLAYABLE_BONUS,
          @A_RAW_LOYALTY_POINTS = RAW_LOYALTY_POINTS
      from admin_all.ACCOUNT with(Rowlock, xlock) where id = @ACCOUNT_ID;

      select @DATETIME = getdate(), @AMOUNT_REAL = isnull(@AMOUNT_REAL, 0), @AMOUNT_RELEASED_BONUS = isnull(@AMOUNT_RELEASED_BONUS, 0),
          @AMOUNT_PLAYABLE_BONUS = isnull(@AMOUNT_PLAYABLE_BONUS, 0), @AMOUNT_RAW_LOYALTY = isnull(@AMOUNT_RAW_LOYALTY, 0),
          @RsvCommitPlatformTranID = isnull(@RsvCommitPlatformTranID, @PLATFORM_TRAN_ID),
          @GameBetPlatformTranID = isnull(@GameBetPlatformTranID, @PLATFORM_TRAN_ID),
          @ROLLED_BACK = 0;
      insert into admin_all.ACCOUNT_TRAN(
            ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL,
            PLATFORM_TRAN_ID, GAME_TRAN_ID, GAME_ID, PLATFORM_ID, PAYMENT_ID,
            ROLLED_BACK, ROLLBACK_TRAN_ID, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS,
            BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS,
            AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY,
            REFERENCE, BRAND_ID
          )
		  values(
            @ACCOUNT_ID, @DATETIME, 'RSV_COMMIT', -@AMOUNT_REAL, @A_BALANCE_REAL - @AMOUNT_REAL,
            @RsvCommitPlatformTranID, @GAME_TRAN_ID, @GAME_ID, @PLATFORM_ID, @PAYMENT_ID,
            @ROLLED_BACK, @RsvTranID, -@AMOUNT_RELEASED_BONUS, -@AMOUNT_PLAYABLE_BONUS,
            @A_RELEASED_BONUS - @AMOUNT_RELEASED_BONUS, @A_PLAYABLE_BONUS - @AMOUNT_PLAYABLE_BONUS,
            -@AMOUNT_RAW_LOYALTY, @A_RAW_LOYALTY_POINTS - @AMOUNT_RAW_LOYALTY,
            @REFERENCE, @BRAND_ID
          ),
          (
            @ACCOUNT_ID, @DATETIME, 'GAME_BET', @AMOUNT_REAL, @A_BALANCE_REAL,
            @GameBetPlatformTranID, @GAME_TRAN_ID, @GAME_ID, @PLATFORM_ID, @PAYMENT_ID,
            @ROLLED_BACK, null, @AMOUNT_RELEASED_BONUS, @AMOUNT_PLAYABLE_BONUS,
            @A_RELEASED_BONUS, @A_PLAYABLE_BONUS, @AMOUNT_RAW_LOYALTY, @A_RAW_LOYALTY_POINTS,
            @REFERENCE, @BRAND_ID
          );
  commit
  select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where PLATFORM_TRAN_ID = @RsvCommitPlatformTranID AND PLATFORM_ID = @PLATFORM_ID AND TRAN_TYPE = 'RSV_COMMIT';
  select @Status = 0;  --   success
  return
end
go
