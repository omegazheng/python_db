set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicationTaskInitialize
as
begin
	set nocount on
	set xact_abort on
	declare @Name nvarchar(1000)

	while(1=1)
	begin
		update top (1) r	
			set r.SessionID = @@spid,
				@Name = rc.Name
		from maint.ReplicationTask r with(readcommittedlock, readpast, rowlock)
			left join maint.ReplicationLastExecution rl on r.ConfigurationID = rl.ConfigurationID
			inner join maint.ReplicationConfiguration rc on rc.ConfigurationID = r.ConfigurationID
		where rl.ToValue is null and r.SessionID is null
		if @@rowcount = 0
			goto ___Exit___
		exec maint.usp_ReplicateOne @Name
	end
___Exit___:
end

go
