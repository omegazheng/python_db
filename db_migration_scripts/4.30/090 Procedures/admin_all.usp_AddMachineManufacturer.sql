set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddMachineManufacturer
(
	@ManufacturerID int = null output ,
	@Code nvarchar(50),
	@Name nvarchar(200)
)
as
begin
	set nocount on 
	select @ManufacturerID = ManufacturerID
	from admin_all.MachineManufacturer
	where Code = @Code
	if @@rowcount > 0
		return
	insert into admin_all.MachineManufacturer(Code, Name)
		values(@Code, @Name)
	select @ManufacturerID = scope_identity()
end
go
