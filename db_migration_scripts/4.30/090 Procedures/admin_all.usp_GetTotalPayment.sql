set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_GetTotalPayment
 (
	@partyid     INT,
	@startDate   DATETIME,
	@endDate     DATETIME,
	@paymentType   VARCHAR(255),
	@paymentStatus VARCHAR(255),
	@paymentMethod VARCHAR(255)
)
AS
  BEGIN
    select Isnull(sum(payment.amount),0.00) as total  from admin_all.payment
      join admin_all.account on  payment.ACCOUNT_ID = ACCOUNT.id
      join external_mpt.USER_CONF u on  u.PARTYID  = ACCOUNT.PARTYID
    where
        payment.request_date >= @startDate and
        payment.request_date < @endDate and
        u.PARTYID = @partyid and
        payment.type = @paymentType and
        payment.status = @paymentStatus  and
        payment.method = @paymentMethod  and
        payment.id not in (select PAYMENT_ID from admin_all.payment_rollback)

  END


go
