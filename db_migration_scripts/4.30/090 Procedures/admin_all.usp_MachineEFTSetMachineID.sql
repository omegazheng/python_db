set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_MachineEFTSetMachineID
(
	@EFTID bigint,
	@MachineID int,
	@SessionKey nvarchar(100),
	@ProductTranID varchar(100) = null,
	@Reference varchar(max) = null
)
as
begin
	set nocount on
	set xact_abort off
	begin try
		begin transaction
		exec admin_all.usp_LockTwoCheeryTransaction @MachineID = @MachineID, @EFTID = @EFTID

		declare @ProductID int, @Date datetime = getdate(), @MachineTranID bigint, @StartDate datetime, 
				@PartyID int, @AccountTranID bigint, @AmountReal numeric(38, 18), @ReleasedBonus numeric(38, 18), @PlayableBonus numeric(38, 18),
				@ContextInfo binary(128) = cast(cast('admin_all.usp_MachineEFTSetMachineID' as varchar(128)) as varbinary(128))
	
		set context_info @ContextInfo

		select	
				@MachineTranID = EFTID, @StartDate = Datetime, @AccountTranID = AccountTranID, 
				@PartyID = PartyID, @AmountReal = AmountReal, @ReleasedBonus = ReleasedBonus,
				@PlayableBonus = PlayableBonus
		from admin_all.MachineTranPendingEFT
		where EFTID = @EFTID
		if @@rowcount = 0
		begin
			raiserror('Couldn not find pending MATCHINE EFT transaction. MachineID is set already.', 16, 1)
		end
	

		select @ProductID = ProductID
		from admin_all.Machine
		where MachineID = @MachineID

		select @MachineTranID = MachineTranID
		from admin_all.MachineTran
		where PartyID = @PartyID
			and MachineID = @MachineID
			and EndDate is null
		if @@rowcount = 0
		begin
			insert into admin_all.MachineTran(MachineTranID, PartyID, MachineID, StartDate, EndDate)
				select @MachineTranID, @PartyID, @MachineID, @StartDate, null
		end	
		
		update a
			set MachineID = @MachineID, PLATFORM_TRAN_ID = @ProductTranID, PLATFORM_ID = @ProductID
		from admin_all.ACCOUNT_TRAN a
		where ID = @AccountTranID
	
		update a
			set MachineID = @MachineID, a.SessionState='OPEN'
		from admin_all.USER_WEB_SESSION a
		where --a.PARTYID = @PartyID 
				SESSION_KEY = @SessionKey
			and a.sessionType='OFFLINE' 
			and a.SessionState in ('OPEN', 'PENDING')

		
		--update admin_all.MachineTranEFTHistory
		--	set MachineTranID = @MachineTranID, Reference = @Reference, ProductTranID = @ProductTranID
		--where MachineTranEFTHistoryID = @EFTID

		insert into admin_all.MachineTranEFT (EFTID, MachineTranID, AccountTranID, RollbackTranID, TransferType, Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, Reference, ProductTranID)
			select @EFTID, @MachineTranID, @AccountTranID, null RollbackTranID, 'MACHINE_EFT' TransferType, 'Pending' Status, @Date Datetime, @AmountReal, @ReleasedBonus, @PlayableBonus, @Reference, @ProductTranID
		delete admin_all.MachineTranPendingEFT where EFTID = @EFTID
		commit

		set context_info 0x0
	end try
	begin catch
		set context_info 0x0
		rollback;
		throw;
	end catch
end
go
