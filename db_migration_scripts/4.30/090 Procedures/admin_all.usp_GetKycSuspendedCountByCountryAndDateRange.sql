set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetKycSuspendedCountByCountryAndDateRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
      select
          count(*) as counts,
          UPPER(country) as COUNTRY
      from external_mpt.USER_CONF
      where
          LOCKED_STATUS = 'KYC_FAILED_LOCK'
          and LOCKED_SINCE BETWEEN @startDate and @endDate
      group by country
      order by counts desc
  END

go
