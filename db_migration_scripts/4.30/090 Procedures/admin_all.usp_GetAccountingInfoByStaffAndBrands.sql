set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetAccountingInfoByStaffAndBrands]
    (@staffid int, @brandsXml xml)
AS
  BEGIN
    create table  #brands (brandid int primary key)
    insert into #brands(brandid)
		SELECT BRANDID
		FROM [admin_all].[STAFF_BRAND_TBL]
		WHERE STAFFID = @staffid and brandid in (SELECT C.value('.', 'int') as value FROM @brandsXml.nodes('brand') as X(C))

    	

    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = cast(DATEADD(YEAR, -10, @endDateLocal) AS DATE) -- DATEADD(DAY, -64, CAST(GETDATE() AS DATE))

    SELECT *
    INTO #Temp
    FROM (
           SELECT
             case when p.TYPE = 'WITHDRAWAL' AND p.STATUS = 'COMPLETED' then 'WITHDRAWAL' else 'DEPOSIT' end AS TRAN_TYPE,
             CAST(p.PROCESS_DATE AS DATE) DATE,
             u.BRANDID                    BRAND_ID,
             u.CURRENCY,
             SUM(p.AMOUNT_REAL)           AMOUNT_REAL,
             0                            AMOUNT_PLAYABLE_BONUS,
             SUM(p.AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
           FROM admin_all.PAYMENT p
             WITH ( NOLOCK )
             JOIN admin_all.ACCOUNT
               ON p.ACCOUNT_ID = ACCOUNT.id
             JOIN EXTERNAL_MPT.USER_CONF
                  u ON u.PARTYID = ACCOUNT.PARTYID
           WHERE
             (
				p.TYPE = 'WITHDRAWAL' AND p.STATUS = 'COMPLETED'
				or
				 p.TYPE = 'DEPOSIT' AND p.STATUS = 'COMPLETED'
			)
             AND p.PROCESS_DATE >= @startDateLocal
             AND p.PROCESS_DATE < @endDateLocal
             AND u.BRANDID IN (select brandid from #brands)
           GROUP BY CAST(p.PROCESS_DATE AS DATE), u.CURRENCY, u.BRANDID, case when p.TYPE = 'WITHDRAWAL' AND p.STATUS = 'COMPLETED' then 'WITHDRAWAL' else 'DEPOSIT' end

           --UNION ALL

           --SELECT
           --  'DEPOSIT'                    TRAN_TYPE,
           --  CAST(p.REQUEST_DATE AS DATE) DATE,
           --  u.BRANDID                    BRAND_ID,
           --  u.CURRENCY,
           --  SUM(p.AMOUNT_REAL)           AMOUNT_REAL,
           --  0                            AMOUNT_PLAYABLE_BONUS,
           --  SUM(p.AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
           --FROM admin_all.PAYMENT p
           --  WITH ( NOLOCK )
           --  JOIN admin_all.ACCOUNT
           --    ON p.ACCOUNT_ID = ACCOUNT.id
           --  JOIN EXTERNAL_MPT.USER_CONF u
           --    ON u.PARTYID = ACCOUNT.PARTYID
           --WHERE
           --  p.TYPE = 'DEPOSIT' AND p.STATUS = 'COMPLETED'
           --  AND p.REQUEST_DATE >= @startDateLocal
           --  AND p.REQUEST_DATE < @endDateLocal
           --  AND u.BRANDID IN (select brandid from #brands)
           --GROUP BY CAST(p.REQUEST_DATE AS DATE), u.CURRENCY, u.BRANDID
         ) ACCOUNTING

    SELECT
      tt.PERIOD                     AS PERIOD,
      tt.CURRENCY                   AS CURRENCY,
      tt.TRAN_TYPE                  AS TRAN_TYPE,
      SUM(tt.AMOUNT_REAL)           AS AMOUNT_REAL,
      SUM(tt.AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
      SUM(tt.AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
    FROM (
           SELECT
             'TODAY'               AS PERIOD,
             BRAND_ID              AS BRAND_ID,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT_REAL           AS AMOUNT_REAL,
             AMOUNT_RELEASED_BONUS AS AMOUNT_RELEASED_BONUS,
             AMOUNT_PLAYABLE_BONUS AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION

           SELECT
             'YESTERDAY'           AS PERIOD,
             BRAND_ID              AS BRAND_ID,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT_REAL           AS AMOUNT_REAL,
             AMOUNT_RELEASED_BONUS AS AMOUNT_RELEASED_BONUS,
             AMOUNT_PLAYABLE_BONUS AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION

           SELECT
             'WTD'                      AS PERIOD,
             BRAND_ID                   AS BRAND_ID,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID, CURRENCY, TRAN_TYPE

           UNION

           SELECT
             'MTD'                      AS PERIOD,
             BRAND_ID                   AS BRAND_ID,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID, CURRENCY, TRAN_TYPE

           UNION

           SELECT
             'SPLM'                      AS PERIOD,
             BRAND_ID                   AS BRAND_ID,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp

           WHERE DATE >= CAST (DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) AS DATE)
                         AND
             DATE <= cast(DATEADD(MONTH, -1, @endDateLocal) AS DATE)
           GROUP BY BRAND_ID, CURRENCY, TRAN_TYPE

         ) tt
    GROUP BY tt.PERIOD, tt.CURRENCY, TRAN_TYPE

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END
  END

go
