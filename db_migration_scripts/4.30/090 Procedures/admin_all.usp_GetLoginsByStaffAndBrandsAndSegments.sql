set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetLoginsByStaffAndBrandsAndSegments]
    (@staffid int, @brands nvarchar(2000))
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(DAY, -64, CAST(@endDateLocal AS DATE))

	create table #Brands (BrandID int primary key)
	insert into #Brands(BrandID)
		SELECT distinct BRANDID
        FROM [admin_all].[STAFF_BRAND_TBL]
        WHERE STAFFID = @staffid
            AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands,','))

    SELECT
      CAST(LOGIN_TIME AS DATE) AS DATE,
      BRANDID                  AS BRAND_ID,
      COUNT(*)                 AS LOGINS
    INTO #Temp
    FROM ADMIN_ALL.USER_LOGIN_LOG
      LEFT JOIN EXTERNAL_MPT.USER_CONF
        ON USER_LOGIN_LOG.PARTYID = USER_CONF.PARTYID
    WHERE LOGIN_TIME >= @startDateLocal
          AND LOGIN_TIME < @endDateLocal
          AND LOGIN_TYPE = 'LOGIN'
          AND BRANDID IN (SELECT x.BRANDID FROM #Brands x)
    GROUP BY CAST(LOGIN_TIME AS DATE), BRANDID

    SELECT
      tt.PERIOD      AS PERIOD,
      SUM(tt.COUNT_FOR_PERIOD) AS COUNT_FOR_PERIOD
    FROM (
           SELECT
             'TODAY'  AS PERIOD,
             BRAND_ID AS BRAND_ID,
             LOGINS   AS COUNT_FOR_PERIOD
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION ALL

           SELECT
             'YESTERDAY' AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             LOGINS      AS COUNT_FOR_PERIOD
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION ALL

           SELECT
             'WTD'       AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             SUM(LOGINS) AS COUNT_FOR_PERIOD
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID

           UNION ALL

           SELECT
             'MTD'       AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             SUM(LOGINS) AS COUNT_FOR_PERIOD
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID

           UNION ALL

           SELECT
             'SPLM'       AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             SUM(LOGINS) AS COUNT_FOR_PERIOD
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal)-1, 0) AS DATE)
                 AND DATE <= cast(DATEADD(MONTH, -1, @endDateLocal) AS DATE)
           GROUP BY BRAND_ID
         ) tt
    GROUP BY tt.PERIOD

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END

  END

go
