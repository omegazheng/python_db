set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetDashboardCashHeld]
  (@staffid INT, @brands NVARCHAR(2000))
AS
  BEGIN
    set nocount on
    create table #Brands (
      BrandID int primary key
    )
    insert into #Brands (BrandID)
      SELECT distinct BRANDID
      FROM [admin_all].[STAFF_BRAND_TBL]
      WHERE STAFFID = @staffid
            AND BRANDID IN (select cast(Item as int) brandId
                            from admin_all.fc_splitDelimiterString(@brands, ','))

    SELECT
      SUM(BALANCE_REAL + RELEASED_BONUS) AS CASH_HELD,
      CURRENCY
    FROM
      ADMIN_ALL.ACCOUNT AS ACCOUNT
      JOIN
      external_mpt.USER_CONF AS USERS
        ON
          ACCOUNT.PARTYID = USERS.PARTYID
    where
      USERS.BRANDID in (select b.BrandID
                        from #Brands b)
    GROUP BY CURRENCY
  END

go
