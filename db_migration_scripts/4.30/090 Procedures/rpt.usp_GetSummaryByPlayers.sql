set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetSummaryByPlayers
(
	@AgentID int,
	@DateFrom datetime,
	@DateTo datetime
)
as
begin
	set nocount on
	DECLARE @endDateLocal DATETIME
	SET @endDateLocal = DATEADD(DD, 1, @DateTo);
	;with x0 as 
	(
		select u.PARTYID, u.ParentID, u.UserID, u.Currency, u.user_type
		from external_mpt.USER_CONF u
		where ParentID = isnull(@AgentID,ParentID) --and u.user_type <> 0
		union all
		select u1.PARTYID, u1.ParentID, u1.UserID, u1.Currency, u1.user_type
		from external_mpt.USER_CONF u1
			inner join x0 on x0.PARTYID = u1.ParentID
	),
	x1 as
	(	
		select	
				PartyID, 
				sum(AmountReal) NGRReal, 
				sum(AmountReleasedBonus)NGRReleasedBonus, 
				sum(AmountPlayableBonus)NGRPlayableBonus
		from admin_all.AccountTranHourlyAggregate
		where AggregateType = cast(0 as tinyint)
			and Datetime >= @DateFrom
			and Datetime < @endDateLocal
			and TranType in ('GAME_BET', 'GAME_WIN')
		group by PartyID
	),
	x2 as (
		select	ACCOUNT_ID, 
				sum(case when TYPE = 'WITHDRAWAL' then AMOUNT end) Withdrawal,
				sum(case when TYPE = 'DEPOSIT' then AMOUNT end) Deposit
		from admin_all.PAYMENT p
		where PROCESS_DATE >= @DateFrom 
			and PROCESS_DATE < @endDateLocal
			and STATUS = 'COMPLETED'
		group by ACCOUNT_ID
	),
	x3 as
	(
		select isnull(t.PartyID, p.PARTYID) as PartyID, t.NGRReal, t.NGRReleasedBonus, NGRPlayableBonus, p.Deposit, p.Withdrawal
		from x1 t
			full outer join (
								select a.PARTYID, x2.Withdrawal, x2.Deposit
								from x2 
									inner join admin_all.ACCOUNT a on x2.ACCOUNT_ID = a.id
								) p on p.PARTYID = t.PartyID
	)
	select	isnull(x3.NGRReal, 0) NGRReal, isnull(x3.NGRReleasedBonus, 0) NGRReleasedBonus, isnull(x3.NGRPlayableBonus, 0) NGRPlayableBonus, isnull(x3.Deposit, 0)Deposit, isnull(x3.Withdrawal, 0)Withdrawal,
			u.USERID as Agent,x0.PartyID, x0.UserID, x0.ParentID, x0.Currency
	from x0
		left join x3 on x0.PartyID = x3.PartyID
		left join external_mpt.USER_CONF u on u.PARTYID = x0.ParentID
	where isnull(x0.user_type, 0) = 0
	order by u.USERID, x0.USERID
end

go
