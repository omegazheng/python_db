set ansi_nulls, quoted_identifier on
go

-- This will try to take away fund from ReleasedBonus or PlayableBonus if it is not 0.
create or alter procedure [BonusPlan].[usp_RemoveFundFromBonusByPartyID]
	(
		@PartyID int,
		@ReleasedBonus numeric(38,18) = 0 ,
		@PlayableBonus numeric(38,18) = 0
	)
as
	begin
		set nocount on
		set xact_abort on
		declare @AccountReleasedBonus numeric(38,18), @AcountPlaybleBonus numeric(38,18),
			@BonusReleasedBonus numeric(38,18), @BonusPlayableBonus numeric(38,18), @BonusID int,
			@BonusReleasedBonusWinning numeric(38,18),@BonusPlayableBonusWinning numeric(38,18),
			@IsChanged bit , @BonusStatus nvarchar(25)

		begin transaction


		if @ReleasedBonus = 0 and @PlayableBonus = 0
			goto ___End___
		declare cBonus cursor local static for
			select	ID,
				RELEASED_BONUS, RELEASED_BONUS_WINNINGS, STATUS,
				PLAYABLE_BONUS, PLAYABLE_BONUS_WINNINGS
			from admin_all.BONUS
			where PARTYID = @PartyID
			order by TRIGGER_DATE


		open cBonus
		fetch next from cBonus into @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning, @BonusStatus, @BonusPlayableBonus, @BonusPlayableBonusWinning
		while @@fetch_status = 0
			begin
				select @IsChanged = 0

				if @ReleasedBonus > 0
				begin
					if @BonusReleasedBonusWinning > 0
					begin
						if @BonusReleasedBonusWinning >= @ReleasedBonus
							begin
								select @BonusReleasedBonusWinning = @BonusReleasedBonusWinning - @ReleasedBonus
								select @ReleasedBonus = 0, @IsChanged = 1
							end
						else
							begin
								select @ReleasedBonus = @ReleasedBonus - @BonusReleasedBonusWinning , @IsChanged = 1
								select @BonusReleasedBonusWinning = 0
							end
					end
				end

				if @ReleasedBonus > 0
					begin
						if @BonusReleasedBonus > 0
							begin
								if @BonusReleasedBonus >= @ReleasedBonus
									begin
										select @BonusReleasedBonus = @BonusReleasedBonus - @ReleasedBonus
										select @ReleasedBonus = 0, @IsChanged = 1
									end
								else
									begin
										select @ReleasedBonus = @ReleasedBonus - @BonusReleasedBonus , @IsChanged = 1
										select @BonusReleasedBonus = 0
									end
							end
					end

				-- Playable
				if @BonusStatus NOT IN ('CANCELED', 'EXPIRED')
					begin
						if @PlayableBonus > 0
							begin
								if @BonusPlayableBonusWinning > 0
									begin
										if @BonusPlayableBonusWinning >= @PlayableBonus
											begin
												select @BonusPlayableBonusWinning = @BonusPlayableBonusWinning - @PlayableBonus
												select @PlayableBonus = 0, @IsChanged = 1
											end
										else
											begin
												select @PlayableBonus = @PlayableBonus - @BonusPlayableBonusWinning , @IsChanged = 1
												select @BonusPlayableBonusWinning = 0
											end
									end
							end

						if @PlayableBonus > 0
							begin
								if @BonusPlayableBonus > 0
									begin
										if @BonusPlayableBonus >= @PlayableBonus
											begin
												select @BonusPlayableBonus = @BonusPlayableBonus - @PlayableBonus
												select @PlayableBonus = 0, @IsChanged = 1
											end
										else
											begin
												select @PlayableBonus = @PlayableBonus - @BonusPlayableBonus , @IsChanged = 1
												select @BonusPlayableBonus = 0
											end
									end
							end
					end

				if @IsChanged = 1
					begin
						update admin_all.BONUS
						set PLAYABLE_BONUS = isnull(@BonusPlayableBonus, PLAYABLE_BONUS), RELEASED_BONUS = @BonusReleasedBonus,
							PLAYABLE_BONUS_WINNINGS = isnull(@BonusPlayableBonusWinning, PLAYABLE_BONUS_WINNINGS),
							RELEASED_BONUS_WINNINGS = @BonusReleasedBonusWinning
						where ID = @BonusID
						--print @BonusID
					end
				if @ReleasedBonus = 0 and @PlayableBonus = 0
					break;
				fetch next from cBonus into @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning, @BonusStatus, @BonusPlayableBonus, @BonusPlayableBonusWinning
			end
		close cBonus
		deallocate cBonus
___End___:

	select @ReleasedBonus as ReleasedBonus, @PlayableBonus as PlayableBonus
	commit
end

go
