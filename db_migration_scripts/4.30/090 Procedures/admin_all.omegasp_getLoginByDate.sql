set ansi_nulls, quoted_identifier on
go

-- count logins by date, brand
-- trace back to 32 days
create or alter procedure [admin_all].[omegasp_getLoginByDate]
  (
    @date   DATETIME,
    @staffid INT
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = DATEADD(DD, 1, @date)
    SET @startDateLocal = DATEADD(DAY, -32, CAST(GETDATE() AS DATE))
    SELECT
      CAST(LOGIN_TIME AS DATE) AS DATE,
      BRANDID                  AS BRAND_ID,
      COUNT(*)                 AS LOGINS
    FROM ADMIN_ALL.USER_LOGIN_LOG
      LEFT JOIN EXTERNAL_MPT.USER_CONF
        ON USER_LOGIN_LOG.PARTYID = USER_CONF.PARTYID
    WHERE LOGIN_TIME >= @startDateLocal
          AND LOGIN_TIME < @endDateLocal
          AND LOGIN_TYPE = 'LOGIN'
          AND BRANDID IN (SELECT BRANDID
                          FROM [admin_all].[STAFF_BRAND_TBL]
                          WHERE STAFFID = @staffid)
    GROUP BY CAST(LOGIN_TIME AS DATE), BRANDID
  END

go
