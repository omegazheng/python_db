set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ConnectionExecute
(
	@ConnectionID int, 
	@CommandText nvarchar(max)
)
as
begin
	declare @ret int, @ConnectionString nvarchar(max)
	select @ConnectionString = maint.fn_GetSQLConnectionStringByID(@ConnectionID)
	exec @ret = maint.usp_ExecuteSQL @ConnectionString = @ConnectionString, @CommandText = @CommandText
	return @ret
end

go
