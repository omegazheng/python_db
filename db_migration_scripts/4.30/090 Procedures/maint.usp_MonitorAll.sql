set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_MonitorAll
as
begin
	set nocount on
	exec maint.usp_MonitorCPUUsage
	exec maint.usp_MonitorLongRunningQuery
end
go
