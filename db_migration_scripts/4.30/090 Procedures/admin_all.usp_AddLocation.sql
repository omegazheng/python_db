set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddLocation
(
	@LocationID int = null output ,
	@Code nvarchar(50),
	@Name nvarchar(200),
	@Town nvarchar(100),
	@PostalCode nvarchar(50),
	@CompanyID int
)
as
begin
	set nocount on 
	select @LocationID = LocationID
	from admin_all.Location
	where Code = @Code
	if @@rowcount > 0
		return
	insert into admin_all.Location(Code, Name, Town, PostalCode, CompanyID)
		values(@Code, @Name, @Town, @PostalCode, @CompanyID)
	select @LocationID = scope_identity()
end
go
