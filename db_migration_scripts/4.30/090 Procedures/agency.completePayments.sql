set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[completePayments]
AS
BEGIN
	set nocount on
	set xact_abort off
	begin try
		declare @ErrorMessage nvarchar(4000)
		exec sp_getapplock 'agency.completePayments', 'Exclusive', 'Session', -1

        update agency.commission_payment set state = 'COMPLETED_REJECTED' where state = 'REJECTED'

        declare approved_payments cursor for
            select commission_payment.id,
                    commission_payment.agent_id,
            		commission_payment.plan_id,
            		commission_payment.value
                        from agency.commission_payment as commission_payment
                            left join admin_all.commission as p on commission_payment.plan_id = p.id
                            left join external_mpt.user_conf as a on commission_payment.agent_id = a.partyid
                        where commission_payment.state = 'APPROVED'
                             or (
                                 commission_payment.state = 'PENDING'
                                 and p.is_autopay = 1
                                 and a.is_autopay = 1
                             )

        declare @payment_id int
        declare @agent_id int
        declare @plan_id int
        declare @payment_value numeric(38,18)
        declare @account_balance_real numeric(38,18)
        declare @account_released_bonus numeric(38,18)
        declare @account_playable_bonus numeric(38,18)
        declare @account_balance_raw_loyalty bigint
        declare @account_id int
        declare @brand_id int
        declare @system_staff_id int
        declare @commissionReference nvarchar(max)

        select @system_staff_id = staffid
            from admin_all.staff_auth_tbl
            where loginname = 'SYSTEM'

        open approved_payments
        fetch next from approved_payments into @payment_id, @agent_id, @plan_id, @payment_value

        WHILE @@FETCH_STATUS = 0
        BEGIN
			begin transaction
            
			select  @account_id = id,
                    @account_balance_real = balance_real,
                    @account_released_bonus = released_bonus,
                    @account_playable_bonus = playable_bonus,
                    @account_balance_raw_loyalty = raw_loyalty_points
                from admin_all.account with(rowlock, updlock)
                where partyid = @agent_id
			
			update commission_payment set state = 'COMPLETED', processed_date = GETDATE()
            where id = @payment_id

            

            select @brand_id = brandid
                from external_mpt.user_conf
                where partyid = @agent_id

          select  @commissionReference  = commission.NAME + ' ' + CONVERT(VARCHAR(11),agency.commission_payment.end_date,106)
          from agency.commission_payment
            join admin_all.commission on agency.commission_payment.plan_id = admin_all.commission.id
            join admin_all.user_commission on commission.ID = USER_COMMISSION.COMMISSION_ID
          where
            commission.id = @plan_id and user_commission.PARTYID = @agent_id


            insert into admin_all.account_tran
                (
                    account_id, brand_id, datetime, tran_type,
                    amount_real, amount_released_bonus, amount_playable_bonus,
                    balance_real, balance_released_bonus, balance_playable_bonus,
                    amount_underflow, amount_raw_loyalty, BALANCE_RAW_LOYALTY, reference
                  -- Here we need to add reference
                )
                values
                (
                    @account_id, @brand_id, GETDATE(), 'COMMISSION',
                    @payment_value, 0, 0,
                    @account_balance_real + @payment_value, @account_released_bonus, @account_playable_bonus,
                    0, 0, @account_balance_raw_loyalty, LEFT(@commissionReference, 100)
                )

            update admin_all.account
                set balance_real = balance_real + @payment_value
                where partyid = @agent_id

            insert into admin_all.user_action_log
                (date, staffid, partyid, action_type)
                values
                (GETDATE(), @system_staff_id, @agent_id, 'COMMISSION')
			commit
            fetch next from approved_payments into @payment_id, @agent_id, @plan_id, @payment_value
         END
     close approved_payments
     DEALLOCATE approved_payments
	 exec sp_releaseapplock 'agency.completePayments', 'session'
	 end try
	 begin catch
		select @ErrorMessage = error_message()
		if @@trancount > 0
			rollback
		begin try
			exec sp_releaseapplock 'agency.completePayments', 'session'
		end try
		begin catch
		end catch
		raiserror(@ErrorMessage, 16, 1)
	 end catch
  END

go
