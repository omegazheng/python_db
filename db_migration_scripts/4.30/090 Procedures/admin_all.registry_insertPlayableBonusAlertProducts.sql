set ansi_nulls, quoted_identifier on
go


create or alter procedure admin_all.registry_insertPlayableBonusAlertProducts
as

begin

-- brand_registry
-- BRANDID: brandID
-- MAP_KEY: 'PLAYABLE_BONUS_ALERT_PRODUCTS'
-- VALUE: productCode list string, e.g.,, 'OMEGA_CAS, FAZI, NOLIMIT_CITY', default: empty string ''

-- registry_hash
-- MAP_KEY: 'PLAYABLE_BONUS_ALERT_PRODUCTS'
-- VALUE: productCode list string, e.g., 'OMEGA_CAS, FAZI, NOLIMIT_CITY', default: empty string ''

--   -- brand_registry
--   -- default  brand: OMEGA
--   declare @brandID int
--   if EXISTS (select * from admin.CASINO_BRAND_DEF where BRANDNAME = 'OMEGA')
--       begin
--           select @brandID = BRANDID from admin.CASINO_BRAND_DEF where BRANDNAME = 'OMEGA'
--           if NOT EXISTS (select * from admin_all.BRAND_REGISTRY where BRANDID = @brandID and MAP_KEY = 'playableBonusAlert.products')
--             insert into admin_all.BRAND_REGISTRY (BRANDID, MAP_KEY, VALUE) values (1, 'playableBonusAlert.products', '')
--       end

  -- registry_hash
   if NOT EXISTS (select * from admin_all.REGISTRY_HASH where MAP_KEY = 'playableBonusAlert.products')
       insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE) values ( 'playableBonusAlert.products', '')

  -- update
  -- update admin_all.BRAND_REGISTRY set value = 'OMEGA_CAS, FAZI, NOLIMIT_CITY' where MAP_KEY = 'playableBonusAlert.products'
  -- update admin_all.REGISTRY_HASH set value = 'OMEGA_CAS, FAZI, NOLIMIT_CITY' where MAP_KEY = 'playableBonusAlert.products'

end


go
