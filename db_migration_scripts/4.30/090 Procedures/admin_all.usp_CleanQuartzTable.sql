set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_CleanQuartzTable]
AS
  BEGIN
    DELETE FROM admin_all.QRTZ_BLOB_TRIGGERS;
    DELETE FROM admin_all.QRTZ_CALENDARS;
    DELETE FROM admin_all.QRTZ_CRON_TRIGGERS;
    DELETE FROM admin_all.QRTZ_FIRED_TRIGGERS;
    DELETE FROM admin_all.QRTZ_LOCKS;
    DELETE FROM admin_all.QRTZ_PAUSED_TRIGGER_GRPS;
    DELETE FROM admin_all.QRTZ_SCHEDULER_STATE;
    DELETE FROM admin_all.QRTZ_SIMPLE_TRIGGERS;
    DELETE FROM admin_all.QRTZ_SIMPROP_TRIGGERS;
    DELETE FROM admin_all.QRTZ_TRIGGERS;
    DELETE FROM admin_all.QRTZ_JOB_DETAILS;
  END

go
