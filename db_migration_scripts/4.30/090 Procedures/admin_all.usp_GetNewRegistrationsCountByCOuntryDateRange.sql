set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetNewRegistrationsCountByCOuntryDateRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

    SELECT
      count(*) as counts,
      COUNTRY as COUNTRY
    FROM
      (
        SELECT
          PARTYID,
          COUNTRY
        FROM
          external_mpt.user_conf
        WHERE
          REG_DATE >= @startDateLocal
          AND REG_DATE < @endDateLocal
        --and BRANDID in (:brandIds)
      ) NEW_REGISTRATION
    group by country
    order by counts desc

  END

go
