set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findRelevantAgents]
  (
    @partyid   INT
  )
AS
  BEGIN
    declare @tmp nvarchar(MAX)
    select @tmp = Hierarchy.ToString() from  external_mpt.user_conf where PARTYID = @partyid

    select * from external_mpt.USER_CONF where PARTYID in (
      select Item as agentId from admin_all.fc_splitDelimiterString(@tmp, '/')
    )
    UNION
    select *
    from external_mpt.user_conf
    where
      (user_type=0 and parentid is not null
       or
       user_type<>0)
      AND Hierarchy.ToString() like '%/'+ cast(@partyid as nvarchar(100))+'/%';
  END

go
