set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_PurgeBonusHistory] @Retention int = 500, @BatchSize int = 150, @ArchiveDatabase nvarchar(128)= null
as
begin
	set nocount, xact_abort on
	declare @i bigint, @ID int, @db nvarchar(128), @SQL nvarchar(max), @Proc sysname, @FullTargetTableName sysname
	if @ArchiveDatabase is not null
		begin
			select @db = db_name(db_id(@ArchiveDatabase))
			if @db is null
				begin
					raiserror('Archive database is not valid %s', 16,1, @ArchiveDatabase)
					return
				end
			if @db is not null
				begin
					select @Proc = quotename(@db) + '..sp_executesql'
					select @SQL = 'select @ret = case when exists(select * from sys.schemas where name = ''admin_all'') then 1 else 0 end'
					exec @Proc @SQL, N'@ret int output', @i output
					if @i = 0
						begin
							select @SQL = 'create schema admin_all'
							begin try
							exec @proc @SQL
							end try
							begin catch
							end catch
						end
					select @FullTargetTableName = quotename(@db)+'.admin_all.bonus_history'
					if object_id(@FullTargetTableName) is null
						begin
							select @SQL = 'create table ' + @FullTargetTableName +' ([ID] int,[TIMESTAMP] datetime,[BONUS_ID] int,[PARTYID] int,[BONUS_PLAN_ID] int,[TRIGGER_DATE] datetime,[EXPIRY_DATE] datetime,[STATUS] varchar(25),[AMOUNT_WAGERED] numeric(38,18),[WAGER_REQUIREMENT] numeric(38,18),[AMOUNT] numeric(38,18),[PAYMENT_ID] int,[RELEASE_DATE] datetime,[AMOUNT_WITHDRAWN] numeric(38,18),[RELEASED_BONUS_WINNINGS] numeric(38,18),[RELEASED_BONUS] numeric(38,18),[PLAYABLE_BONUS_WINNINGS] numeric(38,18),[PLAYABLE_BONUS] numeric(38,18), constraint PK_admin_all_bonus_history primary key (ID))'
							exec(@SQL)
						end
					select @SQL = '	insert into ' + @FullTargetTableName + '(ID, TIMESTAMP, BONUS_ID, PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS)
	select ID, TIMESTAMP, BONUS_ID, PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS
	from (
			delete a
				output deleted.ID,deleted.TIMESTAMP,deleted.BONUS_ID,deleted.PARTYID,deleted.BONUS_PLAN_ID,deleted.TRIGGER_DATE,deleted.EXPIRY_DATE,deleted.STATUS,deleted.AMOUNT_WAGERED,deleted.WAGER_REQUIREMENT,deleted.AMOUNT,deleted.PAYMENT_ID,deleted.RELEASE_DATE,deleted.AMOUNT_WITHDRAWN,deleted.RELEASED_BONUS_WINNINGS,deleted.RELEASED_BONUS,deleted.PLAYABLE_BONUS_WINNINGS,deleted.PLAYABLE_BONUS
			from admin_all.bonus_history a
			where a.ID = @ID
				and not exists(select * from admin_all.BONUS_TRAN_UPDATER_STATUS b where b.LAST_BONUS_HISTORY_ID = a.ID)
		) t1
		'
				end
		end
	--print @SQl
	--return
	select @i = 0
	declare c cursor local static for
		select a.ID
		from admin_all.bonus_history a
		where a.TIMESTAMP < dateadd(day, -@Retention, getdate()) --or a.TIMESTAMP < '2016-01-01'
																																and not exists(select * from admin_all.BONUS_TRAN_UPDATER_STATUS b where b.LAST_BONUS_HISTORY_ID = a.ID)
	open c
	fetch next from c into @ID
	while @@fetch_status = 0
		begin
			select @i = @i +1
			if @@trancount = 0
				begin
					begin transaction
				end
			if @db is null
				begin
					delete a from admin_all.bonus_history a where a.ID = @ID and not exists(select * from admin_all.BONUS_TRAN_UPDATER_STATUS b where b.LAST_BONUS_HISTORY_ID = a.ID)
				end
			else
				begin
					exec sp_executesql @SQL, N'@ID bigint', @ID = @ID
				end
			if @i % @BatchSize = 0
				begin
					while @@trancount > 0
						commit
				end
			fetch next from c into @ID
		end
	close c
	deallocate c
	while @@trancount > 0
		commit
end

go
