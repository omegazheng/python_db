set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationRegistryHash
as
begin
	set nocount on
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.REGISTRY_HASH', @ForceIdentityColumnInsert = 0, @ForceMatchedDataUpdate = 0, @PrimaryKeys = 'MAP_KEY', @ExcludedColumns = null
	--admin_all.REGISTRY_HASH
	begin transaction
	;with s as 
	(
		select [MAP_KEY],[VALUE],[ENCRYPTED],[KEY_IDX]
		from (
				values (N'ukgc.duplicate.user.match.count',N'4',0,0)
					,(N'account.suspension.job.enabled',N'false',0,0)
					,(N'account.suspension.condition.nodepositperiod',N'30',0,0)
					,(N'displayDailyAccountStatusUpdaterJob',N'false',0,0)
					,(N'core.dashboard.source',N'dw',0,0)
					,(N'GameWinnerList.FundType',N'REAL',0,0)
					,(N'GameWinnerList.TopN',N'100',0,0)
					,(N'GameWinnerList.RetentionPeriod',N'31',0,0)
					,(N'AccountTranHourlyAggregate.LastExecutionTime',N'2018-12-19 10:21:00',0,0)
					,(N'AccountTranHourlyAggregate.CurrentVersion',N'0x00000000000007D0',0,0)
					,(N'GameWinnerList.LastLoadedAccountTranID',N'0',0,0)
					,(N'AccountTranHourlyAggregate.JobInterval(sec)',N'30',0,0)
					,(N'twofactor.auth.enabled',N'false',0,0)
					,(N'currencyConversion.job.interval',N'0',0,0)
					,(N'currencyConversion.updateInterval',N'1440',0,0)
					,(N'currencyConversion.updateInterval.virtual',N'525600',0,0)
					,(N'currencyConversion.updateInterval.crypto',N'60',0,0)
					,(N'ipstack.api.url',N'http://api.ipstack.com',0,0)
					,(N'ipstack.access.key',N'',0,0)
					,(N'geoip.provider',N'',0,0)
					,(N'updateCurrencyConversionJob.display',N'true',0,0)
					,(N'verification.code.digit',N'4',0,0)
					,(N'verification.system.email',N'info@omegasys.eu',0,0)
					,(N'PopulateDWGamePlayerDaily.LastExecutionTime',N'2018-12-19 10:20:00',0,0)
					,(N'database.replication.enable',N'0',0,0)
					,(N'database.replication.retention.period(days)',N'180',0,0)
					,(N'multi.currency.primary',N'',0,0)
					,(N'multi.currency.secondary',N'',0,0)
					,(N'Croatia.Regulation.Enabled',N'0',0,0)
					,(N'Croatia.Regulation.PersonalID',N'Test',0,0)
					,(N'Croatia.Regulation.PersonalName',N'Test',0,0)
					,(N'netRefer.releasedBonusToGrossRevenue.isEnabled','1', 0,0)--	Default= true	Allow released bonus to be added to gross revenue
					,(N'netRefer.bonus_Calculation.isEnabled', '0', 0,0)--Default = false	Allow withdrawn released bonus value to be calculated in bonus column
					,(N'netReferUpload.jackpotContribution.isEnabled','0',0,0) --Default=false	If ture, allow jackpot contribution to be included as fee adjustment.If false, Fee adjustment will be zero
					,(N'netRefer.manualBonusAdjustmentPercentage','100',0,0)--	Default=100	
					,(N'netRefer.targetCurrency', '', 0, 0)		--If specify target currency, all the amount will be converted to this currency in activity report. If not defined, will show the raw currecy
					,(N'netReferUpload.isEnabled', '1', 0, 0)

			) v([MAP_KEY],[VALUE],[ENCRYPTED],[KEY_IDX])
	)
	merge admin_all.REGISTRY_HASH t
	using s on s.[MAP_KEY]= t.[MAP_KEY]
	when not matched then
		insert ([MAP_KEY],[VALUE],[ENCRYPTED],[KEY_IDX])
		values(s.[MAP_KEY],s.[VALUE],s.[ENCRYPTED],s.[KEY_IDX])
	
	;
	commit;
end
go
