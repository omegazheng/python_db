set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_CreateBonusPlaform]
AS
BEGIN

  DECLARE @count int = 0
  DECLARE @ID int = 0
  select @count = count(*) from admin_all.PLATFORM where PLATFORM.CODE = 'BONUS'
  IF @COUNT = 0
  BEGIN
     SELECT @ID = MIN(ID) FROM admin_all.PLATFORM
     INSERT INTO admin_all.PLATFORM (CODE, NAME, IS_ENABLED, USERNAME, PASSWORD, WALLET_TYPE, PLATFORM_TYPE, CONVERTJACKPOTS, SESSION_LIMIT_WEB, SESSION_LIMIT_MOBILE, REALITY_CHECK_WEB, REALITY_CHECK_MOBILE, EXCLUDED_COUNTRIES, PROVIDER_ID) VALUES ('BONUS', 'BONUS', 1, null, null, 'BONUS', 'BONUS', 1, 'iframe', 'iframe', 'iframe', 'iframe', null, @ID)
     SELECT @ID = SCOPE_IDENTITY()
     Update admin_all.PLATFORM set PROVIDER_ID = @ID WHERE ID = @ID
  END
END


go
