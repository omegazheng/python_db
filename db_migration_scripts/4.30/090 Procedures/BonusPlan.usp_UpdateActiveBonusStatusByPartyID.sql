set ansi_nulls, quoted_identifier on
go

-- This will try to expire bonus that expiryDate is reached and set the next QUEUED to ACTIVE
-- apparent this does not consider the distinction of casino and sportsbook bonus.
-- TODO separate the cursor into casino and SB
create or alter procedure BonusPlan.usp_UpdateActiveBonusStatusByPartyID
(
  @PartyID INT
)
AS
BEGIN
  SET NOCOUNT ON
  SET XACT_ABORT ON
  DECLARE @IsActiveFound BIT, @BonusStatus NVARCHAR(25), @BonusID INT
  DECLARE @NoOfRec INT
  DECLARE @ExpiredPlayableBonusAmount numeric(38, 18) = 0;

  -- expire old record
  -- I left this outside the transaction because we need to do that anyway.
  select @ExpiredPlayableBonusAmount = -1 * sum(PLAYABLE_BONUS + PLAYABLE_BONUS_WINNINGS) from admin_all.bonus
  WHERE partyId = @PartyID AND status IN ('QUEUED', 'ACTIVE', 'SPENT_ACTIVE') AND EXPIRY_DATE < getdate();

  UPDATE admin_all.bonus
  SET status = 'EXPIRED'
  WHERE partyId = @PartyID AND status IN ('QUEUED', 'ACTIVE', 'SPENT_ACTIVE') AND EXPIRY_DATE < getdate()

  -- test
  if @ExpiredPlayableBonusAmount <> 0
    update admin_all.account set PLAYABLE_BONUS = (PLAYABLE_BONUS + @ExpiredPlayableBonusAmount)
    where ACCOUNT.PARTYID =  @PartyID


  BEGIN TRANSACTION

  SET @NoOfRec = (SELECT count(*)
                  FROM admin_all.bonus
                  WHERE partyId = @PartyID AND status IN ('ACTIVE', 'SPENT_ACTIVE'))
  --PRINT 'noofrec =' + cast(@NoOfRec AS VARCHAR)
  SELECT @IsActiveFound = 0
  IF @NoOfRec > 0
    SELECT @IsActiveFound = 1;

  -- Found the first oldest QUEUED bonus and set it to ACTIVE
  SELECT @IsActiveFound = 0
  SELECT TOP 1
      @BonusID = ID,
      @BonusStatus = STATUS
  FROM admin_all.BONUS
  WHERE PARTYID = @PartyID
        AND status IN ('QUEUED', 'ACTIVE', 'SPENT_ACTIVE')
  ORDER BY TRIGGER_DATE
  IF @@rowcount > 0
    BEGIN
      PRINT 'Found 1 bonus =' + cast(@BonusID AS VARCHAR)
      SELECT @IsActiveFound = 1
      IF @BonusStatus IN ('QUEUED')
        BEGIN
          UPDATE admin_all.BONUS
          SET STATUS = 'ACTIVE'
          WHERE ID = @BonusID
        END
      ELSE
        PRINT 'It is active already ' + cast(@BonusID AS VARCHAR)
    END

  SELECT @IsActiveFound AS IsActiveFound
  COMMIT
END

go
