set ansi_nulls, quoted_identifier on
go

-- get game play, handle_real, pnl_real, ..., by date, brand, currency
-- trace back to 32 days
create or alter procedure [admin_all].[omegasp_getGamePlayByDate]
  (
    @date   DATETIME,
    @staffid INT
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = DATEADD(DD, 1, @date)
    SET @startDateLocal = DATEADD(DAY, -32, CAST(GETDATE() AS DATE))

    SELECT
      SUMMARY_DATE                        AS DATE,
      BRAND_ID                            AS BRAND_ID,
      CURRENCY,
      SUM(GAME_COUNT)                     AS GAME_PLAY,
      SUM(HANDLE_REAL)                    AS HANDLE_REAL,
      SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
      SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
      SUM(PNL_REAL)                       AS PNL_REAL,
      SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
      SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
      SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
      SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
    FROM
      ADMIN_ALL.DW_GAME_PLAYER_DAILY dw
    WHERE
      dw.SUMMARY_DATE >= @startDateLocal
      AND
      dw.SUMMARY_DATE < @endDateLocal
      AND BRAND_ID IN (SELECT BRANDID
                       FROM [admin_all].[STAFF_BRAND_TBL]
                       WHERE STAFFID = @staffid)
    GROUP BY
      dw.SUMMARY_DATE,
      dw.BRAND_ID,
      dw.CURRENCY
  END

go
