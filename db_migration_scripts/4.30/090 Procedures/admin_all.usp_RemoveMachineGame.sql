set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_RemoveMachineGame
(
	@MachineID int,
	@GameInfoID int = null,
	@ProductID int=null, 
	@GameName nvarchar(50)= null
)
as
begin
	set nocount on 
	if @GameInfoID is null
	begin
		select @GameInfoID = ID 
		from admin_all.GAME_INFO 
		where GAME_ID = @GameName 
			and PLATFORM_ID = @ProductID
	end
	delete admin_all.MachineGame 
	where MachineID = @MachineID
		and GameInfoID = @GameInfoID
end
go
