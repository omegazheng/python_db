set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CreateDatabaseSnapshot 
as
begin
	set nocount, xact_abort on
	declare @SourceDatabaseName sysname = db_name()
	declare @SnapshotName sysname = db_name()+'_snapshot'

	declare @SQL nvarchar(max), @Proc sysname = quotename(@SourceDatabaseName)+'..sp_executesql'
	select @SQL =  'drop database ' + @SnapshotName
	if db_id(@SnapshotName) is not null
		exec(@SQL)
	
	declare @t table(name sysname, physical_name sysname, i int identity(1,1))
	insert into @t
		exec @Proc N'select name, physical_name from sys.database_files where type = 0'
	
	select @SQL = 'create database '+ QUOTENAME(@SnapshotName) + ' on '
	select @SQL = @SQL + '(name = '+QUOTENAME(name)+', filename = '''+physical_name+'.'+@SnapshotName+'''),'
	from @t
	where i>1
	select @SQL = @SQL + '(name = '+QUOTENAME(name)+', filename = '''+physical_name+'.'+@SnapshotName+''')'
	from @t
	where i=1
	select @SQL = @SQL + ' AS SNAPSHOT OF '+QUOTENAME(@SourceDatabaseName)+' ;'
	exec(@SQL)

end

go
