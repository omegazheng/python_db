set ansi_nulls, quoted_identifier on
go

-- get game play, handle_real, pnl_real, ..., by period, currency
create or alter procedure [admin_all].[omegasp_getGamePlayByPeriod]
  (
    @staffid INT
  )
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(DAY, -32, CAST(GETDATE() AS DATE))

    SELECT
      SUMMARY_DATE                        AS DATE,
      BRAND_ID                            AS BRAND_ID,
      CURRENCY,
      SUM(GAME_COUNT)                     AS GAME_PLAY,
      SUM(HANDLE_REAL)                    AS HANDLE_REAL,
      SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
      SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
      SUM(PNL_REAL)                       AS PNL_REAL,
      SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
      SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
      SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
      SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
    INTO #Temp
    FROM
      ADMIN_ALL.DW_GAME_PLAYER_DAILY dw
    WHERE
      dw.SUMMARY_DATE >= @startDateLocal
      AND
      dw.SUMMARY_DATE < @endDateLocal
      AND BRAND_ID IN (SELECT BRANDID
                       FROM [admin_all].[STAFF_BRAND_TBL]
                       WHERE STAFFID = @staffid)
    GROUP BY
      dw.SUMMARY_DATE,
      dw.BRAND_ID,
      dw.CURRENCY

    SELECT
      tt.PERIOD                              AS PERIOD,
      tt.CURRENCY                            AS CURRENCY,
      SUM(tt.GAME_PLAY)                      AS GAME_PLAY,
      SUM(tt.HANDLE_REAL)                    AS HANDLE_REAL,
      SUM(tt.HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
      SUM(tt.HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
      SUM(tt.PNL_REAL)                       AS PNL_REAL,
      SUM(tt.PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
      SUM(tt.PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
      SUM(tt.IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
      SUM(tt.IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
    FROM (

           SELECT
             'TODAY'                        AS PERIOD,
             BRAND_ID                       AS BRAND_ID,
             CURRENCY,
             GAME_PLAY                      AS GAME_PLAY,
             HANDLE_REAL                    AS HANDLE_REAL,
             HANDLE_RELEASED_BONUS          AS HANDLE_RELEASED_BONUS,
             HANDLE_PLAYABLE_BONUS          AS HANDLE_PLAYABLE_BONUS,
             PNL_REAL                       AS PNL_REAL,
             PNL_RELEASED_BONUS             AS PNL_RELEASED_BONUS,
             PNL_PLAYABLE_BONUS             AS PNL_PLAYABLE_BONUS,
             IN_GAME_RELEASED_BONUS_RELEASE AS IN_GAME_RELEASED_BONUS_RELEASE,
             IN_GAME_PLAYABLE_BONUS_RELEASE AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION

           SELECT
             'YESTERDAY'                    AS PERIOD,
             BRAND_ID                       AS BRAND_ID,
             CURRENCY,
             GAME_PLAY                      AS GAME_PLAY,
             HANDLE_REAL                    AS HANDLE_REAL,
             HANDLE_RELEASED_BONUS          AS HANDLE_RELEASED_BONUS,
             HANDLE_PLAYABLE_BONUS          AS HANDLE_PLAYABLE_BONUS,
             PNL_REAL                       AS PNL_REAL,
             PNL_RELEASED_BONUS             AS PNL_RELEASED_BONUS,
             PNL_PLAYABLE_BONUS             AS PNL_PLAYABLE_BONUS,
             IN_GAME_RELEASED_BONUS_RELEASE AS IN_GAME_RELEASED_BONUS_RELEASE,
             IN_GAME_PLAYABLE_BONUS_RELEASE AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION

           SELECT
             'WTD'                               AS PERIOD,
             BRAND_ID                            AS BRAND_ID,
             CURRENCY,
             SUM(GAME_PLAY)                      AS GAME_PLAY,
             SUM(HANDLE_REAL)                    AS HANDLE_REAL,
             SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
             SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
             SUM(PNL_REAL)                       AS PNL_REAL,
             SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
             SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
             SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
             SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID, CURRENCY

           UNION

           SELECT
             'MTD'                               AS PERIOD,
             BRAND_ID                            AS BRAND_ID,
             CURRENCY,
             SUM(GAME_PLAY)                      AS GAME_PLAY,
             SUM(HANDLE_REAL)                    AS HANDLE_REAL,
             SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
             SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
             SUM(PNL_REAL)                       AS PNL_REAL,
             SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
             SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
             SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
             SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID, CURRENCY
         ) tt
    GROUP BY tt.PERIOD, tt.CURRENCY

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END
  END

go
