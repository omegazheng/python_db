set ansi_nulls, quoted_identifier on
go


create or alter procedure [agency].[getAgentPlayerSummary]
	(@sp_name VARCHAR(20), @agent_id INT)
AS
	BEGIN
		select
			users.partyId,
			users.brandId,
			users.currency,
			account.id as accountId,
			handleReal,
			winReal,
			handleReleasedBonus,
			winReleasedBonus,
			handlePlayableBonus,
			winPlayableBonus,
			numberOfBets,
			totalDeposit,
			totalWithdrawal
		from external_mpt.user_conf users
			inner join admin_all.account on users.partyId = account.PARTYID
			full outer join
			(
				SELECT
					PARTY_ID,
					sum(HANDLE_REAL) handleReal,
					sum(HANDLE_REAL - PNL_REAL) winReal,
					sum(HANDLE_RELEASED_BONUS) handleReleasedBonus,
					sum(HANDLE_RELEASED_BONUS - PNL_RELEASED_BONUS) winReleasedBonus,
					sum(HANDLE_PLAYABLE_BONUS) handlePlayableBonus,
					sum(HANDLE_PLAYABLE_BONUS - PNL_PLAYABLE_BONUS) winPlayableBonus,
					sum(GAME_COUNT) numberOfBets
				FROM
					admin_all.DW_GAME_PLAYER_DAILY
				group by PARTY_ID
			) dw on dw.PARTY_ID = users.PARTYID
			full outer join (
												select
													payment.ACCOUNT_ID,
													SUM (CASE WHEN  TYPE = 'DEPOSIT' THEN AMOUNT ELSE 0 END) totalDeposit,
													SUM(CASE WHEN TYPE = 'WITHDRAWAL' THEN AMOUNT ELSE 0 END) totalWithdrawal
												from admin_all.PAYMENT
												WHERE PAYMENT.STATUS = 'COMPLETED'
												group by payment.ACCOUNT_ID
											) p on p.ACCOUNT_ID = account.id
		where
			users.ParentID = @agent_id

	END

go
