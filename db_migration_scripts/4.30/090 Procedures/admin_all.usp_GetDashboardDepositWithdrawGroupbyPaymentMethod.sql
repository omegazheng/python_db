set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetDashboardDepositWithdrawGroupbyPaymentMethod]
  (@staffid INT, @brands NVARCHAR(2000), @period NVARCHAR(200))
AS
  BEGIN
    set nocount on
    SET DATEFIRST 1
    DECLARE @endDate DATE = cast(getdate() AS DATE)
    DECLARE @startDate DATE = cast(getdate() AS DATE)

    create table #Brands (
      BrandID int primary key
    )
    insert into #Brands (BrandID)
      SELECT distinct BRANDID
      FROM [admin_all].[STAFF_BRAND_TBL]
      WHERE STAFFID = @staffid
            AND BRANDID IN (select cast(Item as int) brandId
                            from admin_all.fc_splitDelimiterString(@brands, ','))

    if @period = 'TODAY'
      begin
        set @EndDate = CAST(getdate() AS DATE)
        set @StartDate = CAST(getdate() AS DATE)
      end
    else if @period = 'YESTERDAY'
      begin
        SET @EndDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
        SET @StartDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
      end
    else if @period = 'WTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = CAST(DATEADD(DD, 1 - DATEPART(DW, GETDATE()), GETDATE()) AS DATE)
      end
    else if @period = 'MTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = cast(dateadd(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AS DATE)
      end
    else if @period = 'LTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = cast(DATEADD(YEAR, -100, getdate()) AS DATE)
      end

    SELECT
      'WITHDRAWAL'                 AS TRAN_TYPE,
      @period                      AS PERIOD,
      CAST(p.REQUEST_DATE AS DATE) AS DATE,
      u.CURRENCY                   AS CURRENCY,
      p.METHOD                     AS METHOD,
      pm.NAME                      AS NAME,
      SUM(p.AMOUNT_REAL)              AMOUNT_REAL,
      0                               AMOUNT_PLAYABLE_BONUS,
      SUM(p.AMOUNT_RELEASED_BONUS)    AMOUNT_RELEASED_BONUS
    FROM admin_all.PAYMENT p
         WITH ( NOLOCK )
      LEFT JOIN admin_all.PAYMENT_METHOD pm
        ON p.METHOD = pm.CODE
      LEFT JOIN admin_all.ACCOUNT
        ON p.ACCOUNT_ID = ACCOUNT.id
      LEFT JOIN EXTERNAL_MPT.USER_CONF
                u ON u.PARTYID = ACCOUNT.PARTYID And u.BRANDID IN (SELECT x.BRANDID
                                                                   from #Brands x)
    WHERE
      p.TYPE = 'WITHDRAWAL' AND p.STATUS = 'COMPLETED'
      AND p.REQUEST_DATE >= @startDate
      AND p.REQUEST_DATE < DATEADD(DAY,1,@endDate)
    GROUP BY CAST(p.REQUEST_DATE AS DATE), u.CURRENCY, p.METHOD, pm.NAME

    UNION

    SELECT
      'DEPOSIT'                    AS TRAN_TYPE,
      @period                      AS PERIOD,
      CAST(p.REQUEST_DATE AS DATE) AS DATE,
      u.CURRENCY                   AS CURRENCY,
      p.METHOD                     AS METHOD,
      pm.NAME                      AS NAME,
      SUM(p.AMOUNT_REAL)              AMOUNT_REAL,
      0                               AMOUNT_PLAYABLE_BONUS,
      SUM(p.AMOUNT_RELEASED_BONUS)    AMOUNT_RELEASED_BONUS
    FROM admin_all.PAYMENT p
         WITH ( NOLOCK )
      LEFT JOIN admin_all.PAYMENT_METHOD pm
        ON p.METHOD = pm.CODE
      LEFT JOIN admin_all.ACCOUNT
        ON p.ACCOUNT_ID = ACCOUNT.id
      LEFT JOIN EXTERNAL_MPT.USER_CONF u
        ON u.PARTYID = ACCOUNT.PARTYID And u.BRANDID IN (SELECT x.BRANDID
                                                         from #Brands x)
    WHERE
      p.TYPE = 'DEPOSIT' AND p.STATUS = 'COMPLETED'
      AND p.REQUEST_DATE >= @startDate
      AND p.REQUEST_DATE < DATEADD(DAY, 1, @endDate)
    GROUP BY CAST(p.REQUEST_DATE AS DATE), u.CURRENCY, p.METHOD, pm.NAME

  END

go
