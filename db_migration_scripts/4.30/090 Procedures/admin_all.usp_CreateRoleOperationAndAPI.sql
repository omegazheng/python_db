set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_CreateRoleOperationAndAPI
  (
    @OperationName nvarchar(50),
    @RoleName nvarchar (50),
    @RoleID int,
    @ParentRoleName nvarchar (50) = null,
    @DisplayOrder integer = 100,
    @API varchar(350) = null,
    @Method varchar(10) = null
  )
as
  begin

    DECLARE @ParentRoleID int = 0,
    @DBRoleID int = 0,
    @DBRoleName nvarchar(50)
    begin
      if (@RoleName is null)
        raiserror('RoleName %s is invalid', 16, 1, @RoleName)

      select @ParentRoleID = ID from admin_all.AUTH_ROLE where NAME = @ParentRoleName
      --print  @ParentRoleID
      if (@ParentRoleID = 0)
        raiserror('Unable to find ParentRoleName %s', 16, 1, @ParentRoleName)


      select @DBRoleID = ID from admin_all.AUTH_ROLE where NAME = @RoleName
      --print  'ROLE ID =' +  CONVERT(varchar(300),@RoleID)

      if @DBRoleID > 0 AND @DBRoleID <> @RoleID
        raiserror('RoleID and RoleName %s doesn''t match', 16, 1, @RoleName)

      select @DBRoleName = name from admin_all.AUTH_ROLE where id = @RoleID
      if @DBRoleName is not null AND @DBRoleName<> @RoleName
        raiserror('RoleName already exists with different ID', 16, 1)


      -- Insert Role if not already exists
      if (@DBRoleID = 0)
        begin
          set identity_insert admin_all.auth_role on
          insert into admin_all.AUTH_ROLE(ID, NAME, DESCRIPTION, ParentID, DisplayOrder)
          values (@RoleID, @RoleName, @RoleName, @ParentRoleID, @DisplayOrder)
          set identity_insert admin_all.auth_role off
        end


      if (@OperationName is null)
        raiserror('OperationName %s is invalid', 16, 1, @OperationName)

      Declare @Counts int
      select  @Counts = count(*) from admin_all.AUTH_OPERATION where NAME = @OperationName
      if @Counts  = 0
        insert into admin_all.AUTH_OPERATION (NAME, DESCRIPTION, URL)
        values (@OperationName, @OperationName, null)


      if not exists (select * from admin_all.AUTH_ROLE_OPERATION
      where ROLE_ID = @RoleID and OPERATION_NAME = @OperationName)
        insert into admin_all.AUTH_ROLE_OPERATION values (@RoleID, @OperationName)

      if (@API is not null) and (@Method is not null)
        begin
          if not exists (select * from admin_all.AUTH_OPERATION_API
                         where OPERATION = @OperationName and
                                 API = @API and METHOD = @Method)
            insert into admin_all.AUTH_OPERATION_API (OPERATION, METHOD, API)
            values (@OperationName, @Method, @API)
        end
    end
  end


go
