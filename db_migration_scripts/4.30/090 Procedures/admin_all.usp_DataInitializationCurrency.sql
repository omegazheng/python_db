set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationCurrency
as
begin
	set nocount on
	----exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.Currency', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 0, @PrimaryKeys = null, @ExcludedColumns = null
	----admin_all.Currency
	--begin transaction
	--;with s as 
	--(
	--	select [iso_code],[name],[numeric_code],[symbol],[is_default],[symbol_code],[NUMBER_DECIMAL_POINT],[is_virtual],[TYPE],[REFRESH_INTERVAL],[AlertThreshold],[StopThreshold]
	--	from (
	--			values (N'---',N'Only used in CurrencyConversionRate tables',N'-1        ',N'---       ',0,N'-',0,1,N'-',0,0,0)
	--		) v([iso_code],[name],[numeric_code],[symbol],[is_default],[symbol_code],[NUMBER_DECIMAL_POINT],[is_virtual],[TYPE],[REFRESH_INTERVAL],[AlertThreshold],[StopThreshold])
	--)
	--merge admin_all.Currency t
	--using s on s.[iso_code]= t.[iso_code]
	--when not matched then
	--	insert ([iso_code],[name],[numeric_code],[symbol],[is_default],[symbol_code],[NUMBER_DECIMAL_POINT],[is_virtual],[TYPE],[REFRESH_INTERVAL],[AlertThreshold],[StopThreshold])
	--	values(s.[iso_code],s.[name],s.[numeric_code],s.[symbol],s.[is_default],s.[symbol_code],s.[NUMBER_DECIMAL_POINT],s.[is_virtual],s.[TYPE],s.[REFRESH_INTERVAL],s.[AlertThreshold],s.[StopThreshold])
	
	--;
	--commit;
	----exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.CurrencyConversionRate', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	----admin_all.CurrencyConversionRate
	--begin transaction
	--set identity_insert [admin_all].[CurrencyConversionRate] on;
	--;with s as 
	--(
	--	select [RateID],[CurrencyFrom],[CurrencyTo],[Rate],[EffectiveDate],[ExpiryDate]
	--	from (
	--			values (-1,N'---',N'---',1,N'1900-01-01',N'9999-12-31')
	--		) v([RateID],[CurrencyFrom],[CurrencyTo],[Rate],[EffectiveDate],[ExpiryDate])
	--)
	--merge admin_all.CurrencyConversionRate t
	--using s on s.[RateID]= t.[RateID]
	--when not matched then
	--	insert ([RateID],[CurrencyFrom],[CurrencyTo],[Rate],[EffectiveDate],[ExpiryDate])
	--	values(s.[RateID],s.[CurrencyFrom],s.[CurrencyTo],s.[Rate],s.[EffectiveDate],s.[ExpiryDate])
	--when matched and (s.[CurrencyFrom] is null and t.[CurrencyFrom] is not null or s.[CurrencyFrom] is not null and t.[CurrencyFrom] is null or s.[CurrencyFrom] <> t.[CurrencyFrom] or s.[CurrencyTo] is null and t.[CurrencyTo] is not null or s.[CurrencyTo] is not null and t.[CurrencyTo] is null or s.[CurrencyTo] <> t.[CurrencyTo] or s.[Rate] is null and t.[Rate] is not null or s.[Rate] is not null and t.[Rate] is null or s.[Rate] <> t.[Rate] or s.[EffectiveDate] is null and t.[EffectiveDate] is not null or s.[EffectiveDate] is not null and t.[EffectiveDate] is null or s.[EffectiveDate] <> t.[EffectiveDate] or s.[ExpiryDate] is null and t.[ExpiryDate] is not null or s.[ExpiryDate] is not null and t.[ExpiryDate] is null or s.[ExpiryDate] <> t.[ExpiryDate]) then 
	--	update set  t.[CurrencyFrom]= s.[CurrencyFrom], t.[CurrencyTo]= s.[CurrencyTo], t.[Rate]= s.[Rate], t.[EffectiveDate]= s.[EffectiveDate], t.[ExpiryDate]= s.[ExpiryDate]
	--;
	--set identity_insert [admin_all].[CurrencyConversionRate] off;
	--commit;
end
go
