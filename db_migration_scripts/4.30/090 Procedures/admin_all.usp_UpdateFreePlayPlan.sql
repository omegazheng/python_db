set ansi_nulls, quoted_identifier on
go
-- update FreePlayPlan
-- can not change: id, cide, name

create or alter procedure [admin_all].[usp_UpdateFreePlayPlan]
  (
    @ID INT,
    @STATUS varchar(30),
    @DESCRIPTION varchar(300),
    @START_DATE DATETIME,
    @END_DATE DATETIME,
    @EXPIRY_DATE DATETIME,
    @CHANNEL varchar(30),
    @BET_PER_ROUND numeric(38,18),
    @ROUNDS INT
  )
as
  BEGIN
    set nocount on
 --   begin transaction

      update admin_all.FREEPLAY_PLAN
      set STATUS = @STATUS, DESCRIPTION = @DESCRIPTION, START_DATE = @START_DATE, END_DATE = @END_DATE, EXPIRY_DATE = @EXPIRY_DATE,
          CHANNEL = @CHANNEL, BET_PER_ROUND = @BET_PER_ROUND, ROUNDS = @ROUNDS
      where ID = @ID

 --   commit

  END

go
