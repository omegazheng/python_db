set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetDeviceTypeByStaffAndDateRange]
  (@brandIds VARCHAR(2000) = NULL, @period nvarchar(200))
AS
 BEGIN
	set nocount on
	SET DATEFIRST 1
    DECLARE @brandIdsLocal VARCHAR(255)
    --DECLARE @results TABLE(name VARCHAR(100), occurences INT)
    SET @brandIdsLocal = @brandIds;

	create table #Brands (BrandID int primary key)
	insert into #Brands(BrandID)
		select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brandIds,',')

    declare @EndDate date = cast (getdate() as  date)
    declare @StartDate date = cast (getdate() as  date)

    if @period = 'TODAY'
      begin
        set @EndDate = CAST(getdate() AS DATE)
        set @StartDate = CAST(getdate() AS DATE)
      end
    else if @period = 'YESTERDAY'
      begin
        SET @EndDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
        SET @StartDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
      end
    else if @period = 'WTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = CAST(DATEADD(DD, 1 - DATEPART(DW, GETDATE()), GETDATE()) AS DATE)
      end
    else if @period = 'MTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = cast(dateadd(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AS DATE)
      end
    else if @period = 'LTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = cast(DATEADD(YEAR, -100, getdate()) AS DATE)
      end

      SELECT
        case when ull.is_mobile = 0 then 'Desktop' else ull.device_os end AS name,
        count(*)  AS occurences
      FROM admin_all.user_login_log ull
        JOIN external_mpt.USER_CONF uc ON uc.PARTYID = ull.partyid
      WHERE
        (uc.BRANDID IN (SELECT x.BrandID FROM #Brands x))
        AND ull.executed_by = 'PLAYER'
        AND ull.login_type = 'LOGIN'
        AND ull.login_time >= @StartDate AND ull.login_time < DATEADD(DAY, 1, @EndDate)
	group by case when ull.is_mobile = 0 then 'Desktop' else ull.device_os end

  END

go
