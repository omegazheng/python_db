set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_setJobLockStatus
(
	@Job_Name varchar(500), 
	@IsLocked bit,
	@Job_Started_Time DATETIME = null,
	@Job_End_Time DATETIME = null,
	@Job_Message varchar(500) = null
)
as
begin
		set nocount, xact_abort on
		if @IsLocked = 1
			exec admin_all.usp_UpdateJob @Job_Name, 'Start', @Job_Message
		else 
			exec admin_all.usp_UpdateJob @Job_Name, 'Complete', @Job_Message

		/*
		begin transaction
		exec sp_getapplock N'admin_all.usp_setJobLockStatus', N'Exclusive', 'Transaction', -1

		declare @Job_Time_out_Hours int = 3
		-- if the job lasts longer than 3 hours, we will rename the job
		if exists (select * from admin_all.JOB_LOCK_STATUS where JOB_NAME = @Job_Name and LOCKED = 1
																														 and job_started_time < dateadd(hour, -@Job_Time_out_Hours, getdate()))
			begin
				update admin_all.JOB_LOCK_STATUS
				set JOB_NAME = @Job_Name + '_TIMEOUT_' + convert(varchar(25), JOB_STARTED_TIME, 120),
					job_message = @Job_Name + ' took longer than 3 hours. Cancel the job.'
				WHERE JOB_NAME = @Job_Name and LOCKED = 1
							and job_started_time < dateadd(hour, -@Job_Time_out_Hours, getdate())
				if @@rowcount > 0 and @Job_Name = 'ngr-update'
					begin
						delete from agency.job_progress
					end
			end


		if not exists(select * from admin_all.JOB_LOCK_STATUS where JOB_NAME = @Job_Name )
			insert into  admin_all.JOB_LOCK_STATUS (JOB_NAME, LOCKED,JOB_STARTED_TIME)
			values(@Job_Name, 0,@Job_Started_Time)
		if @IsLocked = 1
			begin
				if exists(select * from admin_all.JOB_LOCK_STATUS where JOB_NAME = @Job_Name and LOCKED = 1)
					begin
						raiserror('Job %s is already locked', 16, 1, @Job_Name)
						goto ___End___
					end
				update admin_all.JOB_LOCK_STATUS set LOCKED = 1 , JOB_STARTED_TIME = @Job_Started_Time ,
					JOB_END_TIME = @Job_End_Time, JOB_MESSAGE = @Job_Message
				where JOB_NAME = @Job_Name
			end
		else
			begin
				if exists(select * from admin_all.JOB_LOCK_STATUS where JOB_NAME = @Job_Name and LOCKED = 1)
					begin
						update admin_all.JOB_LOCK_STATUS set LOCKED = 0, JOB_END_TIME = @Job_End_Time,
							JOB_MESSAGE = @Job_Message
						where JOB_NAME = @Job_Name
					end
			end
		___End___:
		commit
	*/
end

go
