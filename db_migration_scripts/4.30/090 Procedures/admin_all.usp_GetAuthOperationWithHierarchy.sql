set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetAuthOperationWithHierarchy]
  (
    @operationName nvarchar(255) = null
  )
AS
  BEGIN
    DECLARE @OPERATION NVARCHAR(255)
    SET @OPERATION = @operationName;

  IF @OPERATION IS NULL
  BEGIN
    SELECT
      ao.name,
      ao.DESCRIPTION,
      ao.URL,
      r.Hierarchy.ToString() AS Hierarchy
    FROM AUTH_OPERATION ao
      JOIN AUTH_ROLE_OPERATION aro ON ao.NAME = aro.OPERATION_NAME
      JOIN AUTH_ROLE R ON aro.ROLE_ID = R.ID
  END

  IF @OPERATION IS NOT NULL
  BEGIN
    SELECT
      ao.name,
      ao.DESCRIPTION,
      ao.URL,
      r.Hierarchy.ToString() AS Hierarchy
    FROM AUTH_OPERATION ao
      JOIN AUTH_ROLE_OPERATION aro ON ao.NAME = aro.OPERATION_NAME
      JOIN AUTH_ROLE R ON aro.ROLE_ID = R.ID
    WHERE ao.NAME = @OPERATION
  END
END

go
