set ansi_nulls, quoted_identifier on
go


create or alter procedure admin_all.usp_InsertEGTGame
(
    @gameID VARCHAR (100),
    @gameName VARCHAR (100),
    @isTouch Bit
)
as

begin

    declare @gameCategoryID int

    declare @gameLaunchID VARCHAR (100)
    set @gameLaunchID = @gameID

    declare @isFreespinEnable int
    set @isFreespinEnable = 0

    declare @subPlatformID int
    set @subPlatformID = null

    declare @segmentId int
    set @segmentId = 1


    if EXISTS (select * from admin_all.PLATFORM where CODE = 'EGT_CAS')
        begin
            declare @platformID int
            select @platformID = ID from admin_all.PLATFORM where CODE = 'EGT_CAS'

            if not exists (Select ID from admin_all.GAME_CATEGORY where Name = 'Unknown')
                insert into admin_all.GAME_CATEGORY (NAME) values ('Unknown')
            select @GameCategoryID =  ID from admin_all.GAME_CATEGORY where Name = 'Unknown'

            if not exists (select * from admin_all.GAME_INFO where GAME_ID = @gameID and PLATFORM_ID = @platformID)
                begin
                    insert into admin_all.GAME_INFO (PLATFORM_ID, GAME_ID, NAME, GAME_CATEGORY_ID, GAME_LAUNCH_ID, IS_TOUCH, IS_FREESPIN_ENABLE, SUB_PLATFORM_ID, SEGMENT_ID)
                    values (@platformID, @gameID , @gameName, @gameCategoryID, @gameLaunchID, @isTouch, @isFreespinEnable, @subPlatformID, @segmentId)
                end
        end

end


go
