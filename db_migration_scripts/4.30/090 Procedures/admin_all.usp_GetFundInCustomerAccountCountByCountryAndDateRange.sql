set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetFundInCustomerAccountCountByCountryAndDateRange]
  (@endDate DATETIME, @brands VARCHAR(255))
AS
  BEGIN
    DECLARE @brandsLocal VARCHAR(255)
    DECLARE @endDateLocal DATETIME
    SET @brandsLocal = @brands
    SET @endDateLocal = @endDate;
    SELECT
      sum(country_balanceReal)    AS balanceReal,
      sum(country_releaseBonus)   AS releaseBonus,
      sum(country_playableBonus)  AS playableBonus,
      a.currency,
      a.country
    FROM
      (
        SELECT
          userTable.CURRENCY AS currency,
          accountHistory.BALANCE_REAL AS country_balanceReal,
          accountHistory.RELEASED_BONUS AS country_releaseBonus,
          accountHistory.PLAYABLE_BONUS AS country_playableBonus,
          UPPER(userTable.country) as country
        FROM
          [external_mpt].[USER_CONF] userTable, [admin_all].[ACCOUNT] account
          JOIN [admin_all].[DAILY_BALANCE_HISTORY] accountHistory ON accountHistory.ACCOUNT_ID = account.id
        WHERE
          account.PARTYID = userTable.PARTYID
          AND (@brandsLocal IS NULL
            OR userTable.BRANDID IN (SELECT * FROM [admin_all].fc_splitDelimiterString(@brandsLocal, ',')))
          AND accountHistory.DATETIME = @endDateLocal
      ) a
    GROUP BY a.currency, a.country;
  END

go
