set ansi_nulls, quoted_identifier on
go

-- Only use by CMS, when create Promotion, should check if this player has active or not

create or alter procedure admin_all.usp_GetFreePlayPromotionByPartyId
  (
    @PartyId INT,
    @StartDate datetime = null,
    @ToDate datetime = null
  )
as
  begin

    if @ToDate is not null
      select @ToDate = DATEADD(DAY, DATEDIFF(DAY, 0, @ToDate), 1)

    if @PartyId is null
      begin
        raiserror('PARTY_ID should not be null ', 16,1)
        return
      end

    select  p.id as id,
            p.freeplay_plan_id as freePlayPlanId,
            p.code as code,
            p.status as status,
            p.trigger_date as triggerDate,
            p.last_updated_time as lastUpdatedTime,
            p.is_completed as isCompleted,
            p.party_id as partyId,
            isnull(p.total_balance,0) as totalBalance,
            isnull(p.remaining_balance,0) as remainingBalance,
            isnull(p.amount_won,0) as amountWon,
            p.default_stake_level as defaultStakeLevel,
            g.GAME_ID as gameId,
            p.GAME_INFO_ID as gameInfoId,
            p.line as line,
            isnull(p.coin,0) as coin,
            isnull(p.denomination,0) as denomination,
            isnull(p.bet_per_round,0) as betPerRound
    from
      admin_all.FREEPLAY_PROMOTION p
      join admin_all.GAME_INFO g on g.ID = p.GAME_INFO_ID
    where
      party_id = @PartyId
      and (trigger_date >= @StartDate or @StartDate is null)
      and (trigger_date < @ToDate or @ToDate is null)
    ORDER BY
      id DESC
  end

go
