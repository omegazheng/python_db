set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetRegistrationsReport]
  (
    @startDate DATETIME,
    @endDate DATETIME,
    @brandids VARCHAR(255) = NULL
  )

AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @brandidsLocal VARCHAR(255)
    SET @startDateLocal = @startDate
    SET @endDateLocal = @endDate
    SET @brandidsLocal = @brandids

    SELECT
      uc.PARTYID,
      uc.USERID,
      uc.NICKNAME,
      uc.FIRST_NAME,
      uc.LAST_NAME,
      uc.REG_DATE,
      uc.MOBILE_PHONE,
      uc.EMAIL,
      uc.ACTIVE_FLAG,
      uc.LOCKED_STATUS,
      cbd.BRANDNAME,
      cty.name as COUNTRY_NAME
    FROM
      [external_mpt].[USER_CONF] uc
      JOIN [admin].[CASINO_BRAND_DEF] cbd on cbd.brandid = uc.brandid
      LEFT JOIN [admin_all].[COUNTRY] cty on uc.country = cty.iso2_Code
    WHERE
      uc.reg_date >= @startDateLocal
      and uc.reg_date <= @endDateLocal
      and (@brandidsLocal IS NULL or uc.brandid IN (SELECT * FROM
        [admin_all].fc_splitDelimiterString(@brandidsLocal, ',')))
      ORDER BY uc.reg_date desc
  END

go
