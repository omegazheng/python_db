set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerRevenues]
    (@partyid int)
AS
BEGIN
    SET DATEFIRST 1
    declare @currency varchar(3) = (select currency from external_mpt.user_conf where partyid=@partyid)
    DECLARE @startDateLocal DATETIME = DATEADD(YEAR, 100, 0);
    DECLARE @endDateLocal DATETIME = GETDATE()

	  SELECT
        'TODAY' AS PERIOD,
        SUMMARY_DATE , MONTH, YEAR,
        SUM(PNL) PNL,
        SUM(HANDLE) HANDLE,

        CASE WHEN SUM(HANDLE) = 0 THEN 0
        ELSE ROUND(SUM(PNL) / SUM(HANDLE) * 100, 2)
        END AS HOLD
    FROM
        admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE
        party_Id = @partyid
        AND CURRENCY = @currency
		 AND SUMMARY_DATE = CAST(@endDateLocal AS DATE)
    GROUP BY
        SUMMARY_DATE, MONTH, YEAR

    UNION

	  SELECT
        'YESTERDAY' AS PERIOD,
        SUMMARY_DATE , MONTH, YEAR,
        SUM(PNL) PNL,
        SUM(HANDLE) HANDLE,

        CASE WHEN SUM(HANDLE) = 0 THEN 0
        ELSE ROUND(SUM(PNL) / SUM(HANDLE) * 100, 2)
        END AS HOLD
    FROM
        admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE
        party_Id = @partyid
        AND CURRENCY = @currency
		 AND SUMMARY_DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))
    GROUP BY
        SUMMARY_DATE, MONTH, YEAR

    UNION

    SELECT
        'WTD' AS PERIOD,
        SUMMARY_DATE , MONTH, YEAR,
        SUM(PNL) PNL,
        SUM(HANDLE) HANDLE,

        CASE WHEN SUM(HANDLE) = 0 THEN 0
        ELSE ROUND(SUM(PNL) / SUM(HANDLE) * 100, 2)
        END AS HOLD
    FROM
        admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE
        party_Id = @partyid
        AND CURRENCY = @currency
		 AND SUMMARY_DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
        AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
        SUMMARY_DATE, MONTH, YEAR

    UNION

	  SELECT
        'MTD' AS PERIOD,
        SUMMARY_DATE , MONTH, YEAR,
        SUM(PNL) PNL,
        SUM(HANDLE) HANDLE,

        CASE WHEN SUM(HANDLE) = 0 THEN 0
        ELSE ROUND(SUM(PNL) / SUM(HANDLE) * 100, 2)
        END AS HOLD
    FROM
        admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE
        party_Id = @partyid
        AND CURRENCY = @currency
		 AND SUMMARY_DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
        AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
        SUMMARY_DATE, MONTH, YEAR

    UNION

	  SELECT
        'LTD' AS PERIOD,
        SUMMARY_DATE , MONTH, YEAR,
        SUM(PNL) PNL,
        SUM(HANDLE) HANDLE,

        CASE WHEN SUM(HANDLE) = 0 THEN 0
        ELSE ROUND(SUM(PNL) / SUM(HANDLE) * 100, 2)
        END AS HOLD
    FROM
        admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE
        party_Id = @partyid
        AND CURRENCY = @currency
		 AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
        SUMMARY_DATE, MONTH, YEAR

END

go
