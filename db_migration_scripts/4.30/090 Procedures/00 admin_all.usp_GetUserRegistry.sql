set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetUserRegistry]
    (@partyid INT,@mapKey VARCHAR)
AS
  BEGIN
    select
    	partyid,
    	@mapKey,
    	value
    from admin_all.USER_REGISTRY
    	WHERE partyid = @partyid and MAP_KEY=@mapKey
  END

go
