set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetPlatformsPerformanceByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
    declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);

    select
        platform.name as name,
        sum(value) as ngr
    from agency.player_ggr
        join admin_all.platform on platform.id = player_ggr.product_id
        join external_mpt.user_conf as player on player.partyid = player_ggr.player_id
    where
        player.hierarchy.IsDescendantOf(@agentHierarchy)=1
        and player_ggr.date >= @start_date and player_ggr.date < @end_date
    group by platform.name

  END

go
