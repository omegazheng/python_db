set ansi_nulls, quoted_identifier on
go

create or alter procedure BonusPlan.usp_UpdateBonusAccountTran
	(
		@bonusAccountTranIds varchar(max),
		@accountTranId bigint
	)
as
	begin
		if @bonusAccountTranIds is null return;
		update BonusPlan.BonusAccountTran set AccountTranId = @accountTranId
		where id in (SELECT *
								 FROM [admin_all].fc_splitDelimiterString(
										 @bonusAccountTranIds,
										 ','))
		end

go
