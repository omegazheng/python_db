set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationAgentPermission
as
begin
set nocount on

begin transaction

merge admin_all.AUTH_USER_ROLE aur
using (
  SELECT PARTYID, 'agency' as USER_TYPE, r.ROLE_ID
  FROM external_mpt.USER_CONF
    cross join (
    select 1001 as ROLE_ID
    union all
    select 1002 as ROLE_ID
    union all
    select 1003 as ROLE_ID
    union all
    select 1004 as ROLE_ID
    union all
    select 1005 as ROLE_ID
    union all
    select 1006 as ROLE_ID
  ) r
  where user_type in (1, 2, 20)
  and (PARTYID not in (SELECT USER_ID FROM admin_all.AUTH_USER_ROLE WHERE USER_TYPE = 'agency'))
) temp
on aur.USER_ID = temp.PARTYID
when not matched THEN
  insert (USER_ID, USER_TYPE, ROLE_ID)
  values (temp.PARTYID, temp.USER_TYPE, temp.ROLE_ID);
commit;
end

go
