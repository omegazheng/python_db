set ansi_nulls, quoted_identifier on
go

-- if freePlay_plan_id != null, get the active FreePlayPlanGameInfoList by freePlayPlanId
-- id id != null, just get that FreePlayPlanGameInfo
-- id freePlay_plan_id != null & gameInfoId != null, just get that FreePlayPlanGameIndo

create or alter procedure admin_all.usp_GetFreePlayPlanGameInfo
  (
    @FREEPLAY_PLAN_ID INT,
    @ID INT,
    @GAMEINFO_ID INT
  )
as
  begin

    IF @ID IS NOT NULL
      BEGIN
        select  id as id,
                freeplay_plan_id as freePlayPlanId,
                gameInfo_id as gameInfoId
        from
          admin_all.FREEPLAY_PLAN_GAMEINFO
        where
          ID = @ID
      END
    ELSE
      IF (@FREEPLAY_PLAN_ID IS NOT NULL) and (@GAMEINFO_ID is not null)
        BEGIN
          select  id as id,
            freeplay_plan_id as freePlayPlanId,
            gameInfo_id as gameInfoId
          from
            admin_all.FREEPLAY_PLAN_GAMEINFO
          where
            freeplay_plan_id = @FREEPLAY_PLAN_ID
            and gameInfo_id = @GAMEINFO_ID
        END

      ELSE IF @FREEPLAY_PLAN_ID IS NOT NULL
        BEGIN
          select  id as id,
            freeplay_plan_id as freePlayPlanId,
            gameInfo_id as gameInfoId
          from
            admin_all.FREEPLAY_PLAN_GAMEINFO
          where
            freeplay_plan_id = @FREEPLAY_PLAN_ID
        END

  end

go
