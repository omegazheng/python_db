set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_FixImportBonus
as
begin
  set nocount, xact_abort on
   --list all the Players having bonus inconsistency issues for review
  declare @PartyID int, @AccountID int, @UserID nvarchar(255),
  @ReleasedBonus numeric(38, 18),  @BonusReleasedBonus numeric(38, 18),
  @PlayableBonus numeric(38, 18), @BonusPlayableBonus numeric(38, 18),
    @BonusID int

  declare c cursor local fast_forward for
    SELECT
      PARTY_ID, ACCOUNT_ID, USERID,
      RELEASED_BONUS,  BONUS_RELEASED_BONUS,
      PLAYABLE_BONUS, BONUS_PLAYABLE_BONUS
    FROM
      (SELECT
         account.PARTYID AS                 PARTY_ID,
         account.id      AS                 ACCOUNT_ID,
         account.RELEASED_BONUS,
         ISNULL(bd.BONUS_RELEASED_BONUS, 0) BONUS_RELEASED_BONUS,
         ACCOUNT.PLAYABLE_BONUS,
         ISNULL(bd.BONUS_PLAYABLE_BONUS, 0) BONUS_PLAYABLE_BONUS,
         user_conf.USERID
       FROM account
         LEFT JOIN
         (
           SELECT
             bonuses.PARTYID,
             sum(bonuses.BONUS_RELEASED_BONUS) AS BONUS_RELEASED_BONUS,
             sum(bonuses.BONUS_PLAYABLE_BONUS) AS BONUS_PLAYABLE_BONUS
           FROM
             (
               SELECT
                 bonus.PARTYID,
                 CASE WHEN (bonus.STATUS = 'ACTIVE' OR bonus.STATUS = 'QUEUED' OR bonus.STATUS = 'SPENT' OR
                            bonus.STATUS = 'SPENT_ACTIVE' OR bonus.STATUS = 'QUALIFIED' OR bonus.STATUS = 'PENDING')
                   THEN (bonus.PLAYABLE_BONUS + bonus.PLAYABLE_BONUS_WINNINGS) ELSE 0 END AS BONUS_PLAYABLE_BONUS,
                 bonus.RELEASED_BONUS + bonus.RELEASED_BONUS_WINNINGS                   AS BONUS_RELEASED_BONUS
               FROM BONUS) bonuses
           GROUP BY bonuses.PARTYID
         ) BD
           ON account.PARTYID = BD.PARTYID
         LEFT JOIN external_mpt.user_conf ON user_conf.partyid = account.partyid
      ) r
    WHERE
      r.RELEASED_BONUS != r.BONUS_RELEASED_BONUS OR
      r.PLAYABLE_BONUS != r.BONUS_PLAYABLE_BONUS

  open c
  fetch next from c into @PartyID, @AccountID, @UserID, @ReleasedBonus, @BonusReleasedBonus, @PlayableBonus, @BonusPlayableBonus
  while @@fetch_status = 0
    begin
      -- match account with bonus if account is less than the bonuses
      if (@ReleasedBonus <> @BonusReleasedBonus or @PlayableBonus <> @BonusPlayableBonus)
        begin
          print 'INSERT BONUS';
          insert into admin_all.Bonus (PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT,
                                       Playable_Bonus, Released_Bonus)
            select @PartyID, bp.ID, getdate(), dateadd(day, bp.EXPIRY_DAYS, getdate()) as EXPIRY_DATE, 'QUEUED' STATUS, 0,
              --bp.WAGER_REQ * bp.AMOUNT as WAGER_REQUIREMENT,
              40 * bp.AMOUNT as WAGER_REQUIREMENT,  (@PlayableBonus - @BonusPlayableBonus) as Amount, (@PlayableBonus - @BonusPlayableBonus) As Playable_Bonus,
              (@ReleasedBonus - @BonusReleasedBonus) As Released_Bonus
            from admin_all.Bonus_Plan bp
              join BONUS_PLAN_BRAND bpb on bp.ID = bpb.BONUS_PLAN_ID
              join external_mpt.USER_CONF u on u.BRANDID = bpb.BRAND_ID and u.PARTYID = @PartyID
            WHERE PLAN_NAME LIKE '% - Generic%'
        end

      print 'fetching next item in cursor'
      fetch next from c into @PartyID, @AccountID, @UserID, @ReleasedBonus, @BonusReleasedBonus, @PlayableBonus, @BonusPlayableBonus
    end


  --  create table #BonusInconsistencyPartyIDs(PartyID integer primary key)

  close c
  deallocate c
end
go
