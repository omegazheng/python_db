set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerPlaying]
  (@staffid int, @brands nvarchar(2000), @startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    set nocount on
    create table #Brands (
      BrandID int primary key
    )
    insert into #Brands (BrandID)
    SELECT distinct BRANDID
    FROM [admin_all].[STAFF_BRAND_TBL]
    WHERE STAFFID = @staffid
      AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands, ','))

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT s.PARTYID,
           u.USERID,
           u.FIRST_NAME,
           u.LAST_NAME,
           u.CURRENCY,
           u.BRANDID,
           b.BRANDNAME,
           s.SESSION_START,
           s.LAST_GAME_ACTIVITY,
           s.LAST_REALITY_CHECK,
           s.WIN_LOSS,
           s.LAST_SESSION_LIMIT_CHECK,
           s.IS_MOBILE,
           s.GAME_INFO_ID

    FROM [admin_all].[USER_SESSION] s WITH (NOLOCK)
           INNER JOIN [external_mpt].[user_conf] u WITH (NOLOCK) ON s.partyid = u.partyid
           INNER JOIN [admin].[casino_brand_def] b WITH (NOLOCK) ON b.brandid = u.brandid
    WHERE s.last_game_activity >= @startDateLocal
      AND s.last_game_activity < @endDateLocal
      AND u.brandId IN (SELECT x.BrandID FROM #Brands x)
    ORDER BY s.last_game_activity DESC
  END

go
