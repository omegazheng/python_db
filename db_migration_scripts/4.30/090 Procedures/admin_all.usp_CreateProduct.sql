set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CreateProduct
(
  @Code nvarchar(50),
  @ProductType nvarchar (50) = null,
  @WalletType nvarchar(50) = null,
  @ProductID int = null output,
  @ReturnRecord bit = 1
)
as
begin
	set nocount, xact_abort on
	select @ProductID = ID from admin_all.platform where platform.code = @Code
	if @@rowcount > 0
		goto ___End___
	declare @SegmentID int 
	select	@ProductType = isnull(@ProductType, 'CASINO'),
			@WalletType = isnull(@WalletType, 'GAME_PLAY')
	select @SegmentID = id from admin_all.segment where name = @ProductType
--	select * from admin_all.segment

	begin transaction
	insert into admin_all.platform (code, name, is_enabled, username, password, wallet_type,
									platform_type, convertjackpots, session_limit_web, session_limit_mobile,
									reality_check_web, reality_check_mobile, excluded_countries,
									provider_id, segment_id)
     values (	@Code, @Code, 1, @Code, admin_all.fn_getMD5(@Code), @WalletType,
				@ProductType, 1, 'iframe',
				'iframe', 'iframe', 'iframe', null,
				null, @SegmentID)
     select @ProductID = scope_identity()
     Update admin_all.platform set provider_id = @ProductID where id = @ProductID
	 commit
___End___:
	if @ReturnRecord = 1
		select * from admin_all.PLATFORM where id = @ProductID
end


go
--exec admin_all.usp_CreateProduct 'BONUS', 'BONUS', 'BONUS', null, 0


--sp_help 'admin_all.platform'