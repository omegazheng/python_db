set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[sportsBetsCommissionReport]
  (@agent_id INT, @start_date DATETIME, @end_date DATETIME, @comm_plan_ids VARCHAR(50))
AS
  BEGIN
    SELECT
      RESULT.partyid           partyid,
      RESULT.userid            userid,
      RESULT.usertype          usertype,
      RESULT.product_id        product_id,
      RESULT.platform_code     platform_code,
      sum(RESULT.bets)              bets,
      sum(RESULT.wins)              wins,
      sum(RESULT.released_bonus)    released_bonus,
      sum(RESULT.ngr)               ngr,
      sum(RESULT.directcommission)  directcommission,
      sum(RESULT.entitycommission)  entitycommission,
      sum(RESULT.networkcommission) networkcommission,
      COMM_PLATFORM.ID   AS    COMMISSION_ID,
      COMM_PLATFORM.NAME AS    COMMISSION_NAME
    FROM (

           SELECT
             users.partyid          partyid,
             users.userid           userid,
             users.user_type        usertype,
             product_id             product_id,
             platform.code          platform_code,
             sum(bets)              bets,
             sum(wins)              wins,
             sum(released_bonuses)  released_bonus,
             sum(ngr)               ngr,
             sum(directcommission)  directcommission,
             sum(entitycommission)  entitycommission,
             sum(networkcommission) networkcommission
           FROM
             (
               SELECT
                 agent_id,
                 product_id,
                 bets,
                 wins,
                 released_bonuses,
                 ngr,
                 0 AS directcommission,
                 0 AS entitycommission,
                 0 AS networkcommission
               FROM (
                      SELECT
                        sum(isnull(bets, 0))                  bets,
                        -1 * sum(isnull(wins, 0))             wins,
                        -1 * sum(isnull(released_bonuses, 0)) released_bonuses,
                        sum(ngr)                              ngr,
                        platform.id                           product_id,
                        agent_id
                      FROM
                        (
                          SELECT
                            sum(isnull(bets, 0))             bets,
                            sum(isnull(wins, 0))             wins,
                            sum(isnull(released_bonuses, 0)) released_bonuses,
                            sum(value)                       ngr,
                            product_id,
                            agent_id
                          FROM
                            agency.direct_ggr
                          WHERE date >= @start_date AND date <= @end_date
                          GROUP BY agent_id, product_id

                          UNION

                          SELECT
                            sum(isnull(bets, 0))             bets,
                            sum(isnull(wins, 0))             wins,
                            sum(isnull(released_bonuses, 0)) released_bonuses,
                            sum(value)                       ngr,
                            product_id,
                            agent_id
                          FROM
                            agency.network_ggr
                          WHERE date >= @start_date AND date <= @end_date
                          GROUP BY agent_id, product_id
                        ) all_ngr
                        -- keep only sports betting ggr
                        JOIN admin_all.platform ON platform.id = all_ngr.product_id
                        -- keep only @agent_id's direct children in context
                        JOIN external_mpt.user_conf agent ON agent.partyid = all_ngr.agent_id
                      WHERE
                        --                  platform.platform_type = @platform_type AND
                        agent.parentid = @agent_id
                      GROUP BY
                        platform.id,
                        agent_id
                    ) agent_ngr_by_product

               UNION

               SELECT
                 agent_id,
                 product_id,
                 0 AS bets,
                 0 AS wins,
                 0 AS released_bonuses,
                 0 AS ngr,
                 directcommission,
                 entitycommission,
                 networkcommission
               FROM
                 (
                   SELECT
                     sum(CASE WHEN c.category = 'direct'
                       THEN value
                         ELSE 0 END) AS directcommission,
                     sum(CASE WHEN c.category = 'network'
                       THEN value
                         ELSE 0 END) AS entitycommission,
                     0               AS networkcommission,
                     cp.product_id,
                     --       cp.plan_id,
                     cp.agent_id
                   FROM
                     agency.commission_payment cp
                     JOIN admin_all.commission c ON c.id = cp.plan_id
                     JOIN admin_all.platform ON platform.id = cp.product_id
                     JOIN external_mpt.user_conf agent ON agent.partyid = cp.agent_id
                   WHERE
                     --               platform.platform_type = @platform_type AND
                     agent.parentid = @agent_id
                     AND cp.start_date >= @start_date AND cp.end_date <= dateadd(DAY, 1, @end_date)
                     AND cp.state <> 'COMPLETED_REJECTED'
                   GROUP BY
                     cp.agent_id, cp.product_id


                   UNION
                   -- network commission of children of the agent for all levels
                   SELECT
                     0          AS directcommission,
                     0          AS entitycommission,
                     sum(value) AS networkcommission,
                     cp.product_id,
                     network.agentId
                   FROM
                     agency.commission_payment cp
                     JOIN admin_all.commission c ON c.id = cp.plan_id
                     JOIN admin_all.platform ON platform.id = cp.product_id
                     JOIN
                     (
                       SELECT
                         directAgent.PARTYID   agentId,
                         networkAgents.PARTYID networkAgentPartyId
                       FROM external_mpt.USER_CONF directAgent
                         JOIN external_mpt.USER_CONF networkAgents ON networkAgents.Hierarchy.ToString()
                                                                      LIKE
                                                                      '%/' + convert(VARCHAR(30), directAgent.PARTYID) +
                                                                      '/%'
                                                                      AND networkAgents.user_type <> 0 AND
                                                                      networkAgents.PARTYID <> directAgent.PARTYID
                       WHERE directAgent.ParentID = @agent_id
                     ) network ON network.networkAgentPartyId = cp.agent_id
                   WHERE
                     --               platform.platform_type = @platform_type AND
                     cp.start_date >= @start_date AND cp.end_date <= dateadd(DAY, 1, @end_date)
                     AND cp.state <> 'COMPLETED_REJECTED'
                   GROUP BY
                     network.agentId, cp.product_id

                 ) agent_commission_by_product
             )
             agent_summary
             JOIN external_mpt.user_conf users ON users.partyid = agent_summary.agent_id
             JOIN admin_all.platform ON agent_summary.product_id = platform.id
           GROUP BY users.partyid, users.userid, users.user_type, product_id, platform.code

           UNION

           SELECT
             users.partyId,
             users.userId,
             users.user_type usertype,
             product_id,
             platform.code   platform_code,
             bets,
             wins,
             released_bonuses,
             ngr,
             0 AS            directCommission,
             0 AS            entityCommission,
             0 AS            networkCommission
           FROM (
                  SELECT
                    sum(ISNULL(bets, 0))                  bets,
                    -1 * sum(ISNULL(wins, 0))             wins,
                    -1 * sum(ISNULL(released_bonuses, 0)) released_bonuses,
                    sum(ngr)                              ngr,
                    platform.id                           product_id,
                    player_id
                  FROM
                    (
                      SELECT
                        sum(ISNULL(bets, 0))             bets,
                        sum(ISNULL(wins, 0))             wins,
                        sum(ISNULL(released_bonuses, 0)) released_bonuses,
                        sum(value)                       ngr,
                        product_id,
                        player_id
                      FROM
                        agency.player_ggr
                      WHERE date >= @start_date AND date <= @end_date AND agent_id = @agent_id
                      GROUP BY player_id, product_id
                    ) player_ngr
                    -- keep only sports betting ggr
                    JOIN admin_all.platform ON platform.id = player_ngr.product_id
                  --            WHERE platform.platform_type = @platform_type
                  GROUP BY
                    platform.id,
                    player_id
                ) player_ngr_by_product
             JOIN external_mpt.user_conf users ON users.partyId = player_ngr_by_product.player_id
             JOIN admin_all.platform ON platform.id = player_ngr_by_product.product_id

         ) RESULT
      JOIN (SELECT
              c.ID,
              c.NAME,
              cp.PLATFORM_ID
            FROM admin_all.COMMISSION c
              JOIN admin_all.COMMISSION_PLATFORM cp ON c.ID = cp.COMMISSION_ID
              JOIN admin_all.USER_COMMISSION uc ON uc.COMMISSION_ID = c.ID
            WHERE uc.PARTYID = @agent_id AND c.STATE = 'ACTIVE' AND c.ID IN (SELECT *
                                                                             FROM [admin_all].fc_splitDelimiterString(
                                                                                 @comm_plan_ids,
                                                                                 ','))) COMM_PLATFORM
        ON RESULT.product_id = COMM_PLATFORM.PLATFORM_ID
    GROUP BY RESULT.PARTYID, RESULT.userid, RESULT.usertype, RESULT.product_id, RESULT.platform_code, COMM_PLATFORM.ID, COMM_PLATFORM.NAME
  END

go
