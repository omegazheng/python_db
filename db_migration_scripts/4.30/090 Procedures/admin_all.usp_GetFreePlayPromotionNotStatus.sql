set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetFreePlayPromotionNotStatus]
  (
    @CODE         NVARCHAR(40),
    @GAME_INFO_ID INT,
    @PARTY_ID     INT
  )
AS
  DECLARE @promoCodeLocal NVARCHAR(40)
  DECLARE @gameInfoIdLocal INT
  DECLARE @partyIdLocal INT

  SET @promoCodeLocal = @CODE
  SET @gameInfoIdLocal = @GAME_INFO_ID
  SET @partyIdLocal = @PARTY_ID

  BEGIN
    SELECT
      id                           AS id,
      freeplay_plan_id             AS freePlayPlanId,
      code                         AS code,
      status                       AS status,
      trigger_date                 AS triggerDate,
      last_updated_time            AS lastUpdatedTime,
      is_completed                 AS isCompleted,
      party_id                     AS partyId,
      isnull(total_balance, 0)     AS totalBalance,
      isnull(remaining_balance, 0) AS remainingBalance,
      isnull(amount_won, 0)        AS amountWon,
      default_stake_level          AS defaultStakeLevel,
      game_info_id                 AS gameInfoId,
      line                         AS line,
      isnull(coin, 0)              AS coin,
      isnull(denomination, 0)      AS denomination,
      isnull(bet_per_round, 0)     AS betPerRound
    FROM
      admin_all.FREEPLAY_PROMOTION
    WHERE
      PARTY_ID = @partyIdLocal
      AND GAME_INFO_ID = @gameInfoIdLocal
      AND CODE = @promoCodeLocal;
  END
go
