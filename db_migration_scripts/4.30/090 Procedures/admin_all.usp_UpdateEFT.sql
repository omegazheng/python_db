set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_UpdateEFT
(
	@EFTID bigint,
	@Action varchar(30), --'Review', 'Complete', 'Cancel', 'Adjust'
	@Reference varchar(max) = null,
	@AmountReal numeric(38, 18) = null,
	@ReleasedBonus numeric(38, 18) = null,
	@PlayableBonus numeric(38, 18) = null
)
as
begin
	set nocount, xact_abort on 
	begin transaction
	exec admin_all.usp_LockTwoCheeryTransaction @EFTID = @EFTID
	declare @TransferType varchar(50), @OriginalStatus varchar(30), @Date datetime = getdate(), 
			@AccountTranID bigint, @RollbackTranID bigint, @Status varchar(30),
			@OriginalAmountReal numeric(38, 18), @OriginalReleasedBonus numeric(38, 18), @OriginalPlayableBonus numeric(38, 18),
			@PartyID int, @MachineID int, @ProductTranID varchar(100), @ProductID int, @TranType varchar(10)

	if isnull(@Action, '') not in ('Review', 'Complete', 'Cancel', 'Adjust')
	begin
		raiserror('@Action must be one of those values, Review, Complete, Fail, Cancel, and Adjust.', 16, 1)
		rollback
		return
	end
	if @Action <> 'Adjust'
	begin
		select @AmountReal = null, @ReleasedBonus = null, @PlayableBonus = null
	end
	select 
			@TransferType = e.TransferType, @OriginalStatus =e. Status,
			@AccountTranID = e.AccountTranID, @RollbackTranID = e.RollbackTranID,
			@OriginalAmountReal = e.AmountReal, @OriginalReleasedBonus = e.ReleasedBonus, @OriginalPlayableBonus = e.PlayableBonus,
			@PartyID = mt.PartyID, @MachineID = mt.MachineID, @ProductTranID = e.ProductTranID, @ProductID = m.ProductID
	from admin_all.MachineTranEFT e
		inner join admin_all.MachineTran mt on mt.MachineTranID = e.MachineTranID
		inner join admin_all.Machine m on m.MachineID = mt.MachineID
	where e.EFTID = @EFTID
	if @@rowcount = 0
	begin
		raiserror('Could not find EFT.', 16, 1)
		rollback
		return
	end;
	
	select @TranType = case when @TransferType = 'MACHINE_EFT' then 'MACHIN_EFT' else 'SYSTEM_EFT' end

	if @Action = 'Review' and @OriginalStatus in ('Pending')
	begin
		select @Status = 'InReview'
	end
	else if @Action = 'Complete' and @OriginalStatus in ('Pending', 'InReview')
	begin
		select @Status = 'Success'
		if @AccountTranID is null
		begin
			exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = @OriginalAmountReal, @ReleasedBonus = @OriginalReleasedBonus,  @PlayableBonus = @OriginalPlayableBonus, 
														@TranType = @TranType, @PlatformID = @ProductID, @PlatformTranID = @ProductTranID, @MachineID = @MachineID,
														@UpdateBonuses = 1, @AccountTranID= @AccountTranID output
		end
	end
	else if @Action = 'Adjust' and @OriginalStatus in ('InReview')
	begin
		select @Status = 'Pending'
		if @AccountTranID is not null
		begin
			exec admin_all.usp_RollbackTransactionInternal @TransactionID = @AccountTranID, @RollbackTransactionID = @RollbackTranID output
			exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = @AmountReal, @ReleasedBonus = @ReleasedBonus,  @PlayableBonus = @PlayableBonus, 
														@TranType = @TranType, @PlatformID = @ProductID, @PlatformTranID = @ProductTranID, @MachineID = @MachineID,
														@UpdateBonuses = 1, @AccountTranID= @AccountTranID output
		end
	end
	else if @Action = 'Cancel' and @OriginalStatus in ('Pending', 'InReview', 'Complete')
	begin
		if @AccountTranID is not null
		begin
			exec admin_all.usp_RollbackTransactionInternal @TransactionID = @AccountTranID, @RollbackTransactionID = @RollbackTranID output
		end
		select @Status = 'Canceled'
	end
	else
	begin
		raiserror('Cannot apply %s to status %s.', 16, 1, @Action, @OriginalStatus)
		rollback
		return
	end
	update admin_all.MachineTranEFT
		set AccountTranID = @AccountTranID,
			RollbackTranID = isnull(@RollbackTranID, RollbackTranID),
			Status = @Status,
			Datetime = @Date,
			AmountReal = isnull(@AmountReal, AmountReal),
			ReleasedBonus = isnull(@ReleasedBonus, ReleasedBonus),
			PlayableBonus = isnull(@PlayableBonus, PlayableBonus),
			Reference = @Reference
	where EFTID = @EFTID
	commit
end 
go
