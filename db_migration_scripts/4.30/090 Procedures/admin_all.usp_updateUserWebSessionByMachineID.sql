set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_updateUserWebSessionByMachineID
 (
	@PartyID int, 
	@LastAccessTime datetime = null,
	@IP varchar(25) = null,
	@IsMobile bit = 0,
	@SessionType varchar(20) = null,
	@SessionState varchar(20) = null,
	@MachineID int
	
)
as
begin
	set nocount on
	select @LastAccessTime = isnull(@LastAccessTime, getdate())

	declare @SessionKey nvarchar(50)
	

	if @MachineID is not null
	begin
		update  w 
			set 
				PARTYID = @PartyID,
				IP = @IP,
				IS_MOBILE = @IsMobile,
				SessionType = @SessionType,
				SessionState = @SessionState, 
				@LastAccessTime = LAST_ACCESS_TIME = case when @LastAccessTime < LAST_ACCESS_TIME then getdate() else @LastAccessTime end
		from admin_all.USER_WEB_SESSION w with (forceseek)
		where MachineID = @MachineID
		if @@rowcount >0
			goto ___Exit___
	end
	
	declare @Parties table (PartyID int primary key)
	insert into @Parties(PartyID)
		select AssociatedPartyID
		from admin_all.fn_GetUserAssociatedAccount(@PartyID) a
	if @@rowcount = 0
		insert into @Parties values(@PartyID)

	update  w 
		set 
			MachineID = @MachineID,
			IP = @IP,
			IS_MOBILE = @IsMobile,
			SessionType = @SessionType,
			SessionState = @SessionState, 
			@LastAccessTime = LAST_ACCESS_TIME = case when @LastAccessTime < LAST_ACCESS_TIME then getdate() else @LastAccessTime end
	from admin_all.USER_WEB_SESSION w with (forceseek)
	where PARTYID in (select a.PartyID from @Parties a)
	if @@rowcount >0
		goto ___Exit___

	
	insert into admin_all.USER_WEB_SESSION(PARTYID, SESSION_KEY, LAST_ACCESS_TIME, IP, IS_MOBILE, SessionType, SessionState, MachineID)
		select PartyID, replace(cast(newid() as nvarchar(50)), '-', ''), @LastAccessTime, @IP, @IsMobile, @SessionType, @SessionState, @MachineID
		from @Parties

___Exit___:
    select SESSION_KEY, PARTYID, PARLAY_JSESSIONID, LAST_ACCESS_TIME, SessionType, SessionState, MachineID
	from admin_all.USER_WEB_SESSION  w
	where exists(select * from @Parties p where p.PartyID = w.PARTYID)
  END

go
