set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetWithdrawnBonusInfo]
  (@partyid INT)
AS
  BEGIN

    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = CAST(DATEADD(YEAR, 100, 0) AS DATE)

    SELECT *
    INTO #Temp
    FROM (
           SELECT
             bt.SUMMARY_DATE          AS DATE,
             bt.WITHDRAWN_AMOUNT      AS AMOUNT
           FROM admin_all.BONUS_TRAN bt
             JOIN admin_all.BONUS b ON bt.BONUS_ID = b.ID
           WHERE
             b.PARTYID = @partyid
             AND bt.WITHDRAWN_AMOUNT > 0
         ) BONUSES


    SELECT
      'TODAY'                         AS PERIOD,
      AMOUNT                          AS AMOUNT
    FROM #Temp
    WHERE DATE = CAST(@endDateLocal AS DATE)

    UNION

    SELECT
      'YESTERDAY'                     AS PERIOD,
      AMOUNT                          AS AMOUNT
    FROM #Temp
    WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

    UNION

    SELECT
      'WTD'                           AS PERIOD,
      ISNULL(SUM(AMOUNT), 0)          AS AMOUNT
    FROM #Temp
    WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
          AND DATE <= CAST(@endDateLocal AS DATE)

    UNION

    SELECT
      'MTD'                           AS PERIOD,
      ISNULL(SUM(AMOUNT), 0)          AS AMOUNT
    FROM #Temp
    WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
          AND DATE <= CAST(@endDateLocal AS DATE)

    UNION

    SELECT
      'LTD'                           AS PERIOD,
      ISNULL(SUM(AMOUNT), 0)          AS AMOUNT
    FROM #Temp
    WHERE DATE <= CAST(@endDateLocal AS DATE)

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END


  END

go
