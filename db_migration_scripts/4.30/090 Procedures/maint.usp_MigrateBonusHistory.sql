set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_MigrateBonusHistory
as
begin
	set nocount on
	set xact_abort on
	if object_id('admin_all.BONUS_HISTORY_BeforeMigration') is null
		return
	declare @IDs table (ID int primary key)
	while exists(select * from admin_all.BONUS_HISTORY_BeforeMigration)
	begin
		delete @IDs;
		begin transaction
		set identity_insert admin_all.BONUS_HISTORY on
		insert into admin_all.BONUS_HISTORY([ID], [TIMESTAMP], [BONUS_ID], [PARTYID], [BONUS_PLAN_ID], [TRIGGER_DATE], [EXPIRY_DATE], [STATUS], [AMOUNT_WAGERED], [WAGER_REQUIREMENT], [AMOUNT], [PAYMENT_ID], [RELEASE_DATE], [AMOUNT_WITHDRAWN], [RELEASED_BONUS_WINNINGS], [RELEASED_BONUS], [PLAYABLE_BONUS_WINNINGS], [PLAYABLE_BONUS])
			output inserted.ID into @IDs
			select top 1000 [ID], [TIMESTAMP], [BONUS_ID], [PARTYID], [BONUS_PLAN_ID], [TRIGGER_DATE], [EXPIRY_DATE], [STATUS], [AMOUNT_WAGERED], [WAGER_REQUIREMENT], [AMOUNT], [PAYMENT_ID], [RELEASE_DATE], [AMOUNT_WITHDRAWN], [RELEASED_BONUS_WINNINGS], [RELEASED_BONUS], [PLAYABLE_BONUS_WINNINGS], [PLAYABLE_BONUS]
			from admin_all.BONUS_HISTORY_BeforeMigration
			order by ID desc
		set identity_insert admin_all.BONUS_HISTORY off
		delete admin_all.BONUS_HISTORY_BeforeMigration where ID in (select a.ID from @IDs a)
		commit
	end
	drop table admin_all.BONUS_HISTORY_BeforeMigration
	if not exists(select * from sys.foreign_keys where referenced_object_id = object_id('[admin_all].[BONUS_HISTORY]') and name = 'FK_admin_all_BONUS_TRAN_UPDATER_STATUS_BONUS_HISTORY_LAST_BONUS_HISTORY_ID')
	begin
		alter table [admin_all].[BONUS_TRAN_UPDATER_STATUS]  ADD  CONSTRAINT [FK_admin_all_BONUS_TRAN_UPDATER_STATUS_BONUS_HISTORY_LAST_BONUS_HISTORY_ID] FOREIGN KEY([LAST_BONUS_HISTORY_ID]) REFERENCES [admin_all].[BONUS_HISTORY_Temp] ([ID])
	end
	exec maint.usp_ForceNamingConvention 'admin_all.BONUS_HISTORY'
end

go
