set ansi_nulls, quoted_identifier on
go

create or alter procedure [BonusPlan].[usp_UpdateBonus]
	(
		@BonusID int,
		@AmountPlayableBonus numeric(38,18) = 0,
		@AmountPlayableBonusWinnings numeric(38,18) = 0,
		@AmountReleasedBonus numeric(38,18) = 0,
		@AmountReleasedBonusWinnings numeric(38,18) = 0
	)
as
	begin
		set nocount on
		set xact_abort on

		begin transaction

		UPDATE
			admin_all.bonus
		SET
			released_bonus_winnings = released_bonus_winnings + @AmountReleasedBonusWinnings,
			released_bonus = released_bonus + @AmountReleasedBonus,
			playable_bonus_winnings = playable_bonus_winnings + @AmountPlayableBonusWinnings,
			playable_bonus = playable_bonus + @AmountPlayableBonus
		WHERE id = @BonusID
		commit

		select
			id,
			playable_bonus,
			playable_bonus_winnings,
			released_bonus,
			released_bonus_winnings
		from
			admin_all.bonus
		where
			id = @BonusID
	end
-- exec BonusPlan.usp_InsertBonusAccountTran @BonusID = 1,
-- 																					-- @party_id = 91429638,
-- 																					-- @brand_id = 1,
-- 																					@datetime = '2017-01-01 23:59:59',
-- 																					@tran_type = 'MAN_BONUS',
-- 																					@AmountPlayableBonus = 1,
-- 																					@AmountPlayableBonusWinnings = 2,
-- 																					@AmountReleasedBonus = 3,
-- 																					@AmountReleasedBonusWinnings = 4,
-- 																					@amount_wagered_contribution = 5,
-- 																					@balance_playable_bonus = 6,
-- 																					@balance_playable_bonus_winning = 7,
-- 																					@balance_released_bonus = 8,
-- 																					@balance_released_bonus_winning = 9,
-- 																					@AccountTranId = 10001



go
