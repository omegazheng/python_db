set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetGBAgeRangeDetail]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      'M'                        AS GENDER,
      ISNULL(sum(CASE WHEN AGE >= 18 AND AGE < 25
        THEN 1
                 ELSE 0 END), 0) AS AGE1824,
      ISNULL(sum(CASE WHEN AGE >= 25 AND AGE < 35
        THEN 1
                 ELSE 0 END), 0) AS AGE2534,
      ISNULL(sum(CASE WHEN AGE >= 35 AND AGE < 45
        THEN 1
                 ELSE 0 END), 0) AS AGE3544,
      ISNULL(sum(CASE WHEN AGE >= 45 AND AGE < 55
        THEN 1
                 ELSE 0 END), 0) AS AGE4554,
      ISNULL(sum(CASE WHEN AGE >= 55 AND AGE < 65
        THEN 1
                 ELSE 0 END), 0) AS AGE5465,
      ISNULL(sum(CASE WHEN AGE >= 65
        THEN 1
                 ELSE 0 END), 0) AS AGE65AndAbove
    FROM (
           SELECT datediff(YEAR, BIRTHDATE, getdate()) age
           FROM
             external_mpt.USER_CONF
           WHERE
             COUNTRY = 'GB' AND REG_DATE >= @startDateLocal AND REG_DATE < @endDateLocal AND GENDER = 'M'
         ) result

    UNION

    SELECT
      'F'                        AS GENDER,
      ISNULL(sum(CASE WHEN AGE >= 18 AND AGE < 25
        THEN 1
                 ELSE 0 END), 0) AS AGE1824,
      ISNULL(sum(CASE WHEN AGE >= 25 AND AGE < 35
        THEN 1
                 ELSE 0 END), 0) AS AGE2534,
      ISNULL(sum(CASE WHEN AGE >= 35 AND AGE < 45
        THEN 1
                 ELSE 0 END), 0) AS AGE3544,
      ISNULL(sum(CASE WHEN AGE >= 45 AND AGE < 55
        THEN 1
                 ELSE 0 END), 0) AS AGE4554,
      ISNULL(sum(CASE WHEN AGE >= 55 AND AGE < 65
        THEN 1
                 ELSE 0 END), 0) AS AGE5465,
      ISNULL(sum(CASE WHEN AGE >= 65
        THEN 1
                 ELSE 0 END), 0) AS AGE65AndAbove
    FROM (
           SELECT datediff(YEAR, BIRTHDATE, getdate()) age
           FROM
             external_mpt.USER_CONF
           WHERE
             COUNTRY = 'GB' AND REG_DATE >= @startDateLocal AND REG_DATE < @endDateLocal AND GENDER = 'F'
         ) result

    UNION

    SELECT
      'UNKNOWN'                  AS GENDER,
      ISNULL(sum(CASE WHEN AGE >= 18 AND AGE < 25
        THEN 1
                 ELSE 0 END), 0) AS AGE1824,
      ISNULL(sum(CASE WHEN AGE >= 25 AND AGE < 35
        THEN 1
                 ELSE 0 END), 0) AS AGE2534,
      ISNULL(sum(CASE WHEN AGE >= 35 AND AGE < 45
        THEN 1
                 ELSE 0 END), 0) AS AGE3544,
      ISNULL(sum(CASE WHEN AGE >= 45 AND AGE < 55
        THEN 1
                 ELSE 0 END), 0) AS AGE4554,
      ISNULL(sum(CASE WHEN AGE >= 55 AND AGE < 65
        THEN 1
                 ELSE 0 END), 0) AS AGE5465,
      ISNULL(sum(CASE WHEN AGE >= 65
        THEN 1
                 ELSE 0 END), 0) AS AGE65AndAbove
    FROM (
           SELECT datediff(YEAR, BIRTHDATE, getdate()) age
           FROM
             external_mpt.USER_CONF
           WHERE
             COUNTRY = 'GB' AND REG_DATE >= @startDateLocal AND REG_DATE < @endDateLocal AND
             (GENDER IS NULL OR GENDER NOT IN ('F', 'M'))
         ) result


  END

go
