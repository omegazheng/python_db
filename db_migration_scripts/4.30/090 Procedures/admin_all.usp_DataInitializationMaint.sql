set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationMaint
as
begin
	set nocount on
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'maint.AlertMailList', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--maint.AlertMailList
	begin transaction
	;with s as 
	(
		select [Customer],[Recipients]
		from (
				values (N'OMEGA',N'zheng@omegasys.eu; mak@omegasys.eu; leo@omegasys.eu; robbie@omegasys.eu; john.huang@sqlnotes.info;')
			) v([Customer],[Recipients])
	)
	merge maint.AlertMailList t
	using s on s.[Customer]= t.[Customer]
	when not matched then
		insert ([Customer],[Recipients])
		values(s.[Customer],s.[Recipients])
	when matched and (s.[Recipients] is null and t.[Recipients] is not null or s.[Recipients] is not null and t.[Recipients] is null or s.[Recipients] <> t.[Recipients]) then 
		update set  t.[Recipients]= s.[Recipients]
	;
	commit;
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'maint.QueryEvaluationType', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--maint.QueryEvaluationType
	begin transaction
	;with s as 
	(
		select [EvaluationType],[Comment]
		from (
				values (N'Local',N'Query will be evaluated locally')
					,(N'None',N'Query will not be evaluated')
					,(N'Source',N'The result of query evaluated locally will be evaluated at the source connection')
					,(N'Target',N'The result of query evaluated locally will be evaluated at the target connection')
			) v([EvaluationType],[Comment])
	)
	merge maint.QueryEvaluationType t
	using s on s.[EvaluationType]= t.[EvaluationType]
	when not matched then
		insert ([EvaluationType],[Comment])
		values(s.[EvaluationType],s.[Comment])
	when matched and (s.[Comment] is null and t.[Comment] is not null or s.[Comment] is not null and t.[Comment] is null or s.[Comment] <> t.[Comment]) then 
		update set  t.[Comment]= s.[Comment]
	;
	commit;
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'maint.FilterType', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--maint.FilterType
	begin transaction
	;with s as 
	(
		select [FilterType],[Comment],[FromValueQuery],[FromValueQueryEvaluationType],[FromValueAdjustment],[ToValueQuery],[ToValueQueryEvaluationType],[ToValueAdjustment]
		from (
				values (N'ChangeTracking',null,N'cast(@LastLoadedValue as bigint)',N'Local',null,N'cast(@CurrentChangeTrackingVersion as bigint)',N'Local',null)
					,(N'DateTime-Back20min-Forward24hr',null,N'convert(varchar(16), dateadd(minute, -20, case when isnull(cast(@ToValue as datetime),   getdate()) > isnull(cast(@ToValue as datetime),   getdate()) then getdate() else isnull(cast(@ToValue as datetime),   getdate()) end  ), 120)',N'Local',N'convert(datetime, @NewFromValue, 120)',N'convert(varchar(16), dateadd(hour, 24, getdate()), 120)',N'Local',N'convert(datetime, dateadd(hour, -24,@NewToValue), 120)')
					,(N'RowVersion',null,N'convert(varchar(30),isnull(cast(@ToValue as binary(8)), 0x00), 1)',N'Local',N'convert(binary(8), @NewFromValue, 1)',N'convert(varchar(30),isnull(cast(@CurrentRowVersion as binary(8)), 0x00),1)',N'Local',N'convert(binary(8), @NewToValue, 1)')
					,(N'Snapshot',null,null,N'None',null,null,N'None',null)
			) v([FilterType],[Comment],[FromValueQuery],[FromValueQueryEvaluationType],[FromValueAdjustment],[ToValueQuery],[ToValueQueryEvaluationType],[ToValueAdjustment])
	)
	merge maint.FilterType t
	using s on s.[FilterType]= t.[FilterType]
	when not matched then
		insert ([FilterType],[Comment],[FromValueQuery],[FromValueQueryEvaluationType],[FromValueAdjustment],[ToValueQuery],[ToValueQueryEvaluationType],[ToValueAdjustment])
		values(s.[FilterType],s.[Comment],s.[FromValueQuery],s.[FromValueQueryEvaluationType],s.[FromValueAdjustment],s.[ToValueQuery],s.[ToValueQueryEvaluationType],s.[ToValueAdjustment])
	when matched and (s.[Comment] is null and t.[Comment] is not null or s.[Comment] is not null and t.[Comment] is null or s.[Comment] <> t.[Comment] or s.[FromValueQuery] is null and t.[FromValueQuery] is not null or s.[FromValueQuery] is not null and t.[FromValueQuery] is null or s.[FromValueQuery] <> t.[FromValueQuery] or s.[FromValueQueryEvaluationType] is null and t.[FromValueQueryEvaluationType] is not null or s.[FromValueQueryEvaluationType] is not null and t.[FromValueQueryEvaluationType] is null or s.[FromValueQueryEvaluationType] <> t.[FromValueQueryEvaluationType] or s.[FromValueAdjustment] is null and t.[FromValueAdjustment] is not null or s.[FromValueAdjustment] is not null and t.[FromValueAdjustment] is null or s.[FromValueAdjustment] <> t.[FromValueAdjustment] or s.[ToValueQuery] is null and t.[ToValueQuery] is not null or s.[ToValueQuery] is not null and t.[ToValueQuery] is null or s.[ToValueQuery] <> t.[ToValueQuery] or s.[ToValueQueryEvaluationType] is null and t.[ToValueQueryEvaluationType] is not null or s.[ToValueQueryEvaluationType] is not null and t.[ToValueQueryEvaluationType] is null or s.[ToValueQueryEvaluationType] <> t.[ToValueQueryEvaluationType] or s.[ToValueAdjustment] is null and t.[ToValueAdjustment] is not null or s.[ToValueAdjustment] is not null and t.[ToValueAdjustment] is null or s.[ToValueAdjustment] <> t.[ToValueAdjustment]) then 
		update set  t.[Comment]= s.[Comment], t.[FromValueQuery]= s.[FromValueQuery], t.[FromValueQueryEvaluationType]= s.[FromValueQueryEvaluationType], t.[FromValueAdjustment]= s.[FromValueAdjustment], t.[ToValueQuery]= s.[ToValueQuery], t.[ToValueQueryEvaluationType]= s.[ToValueQueryEvaluationType], t.[ToValueAdjustment]= s.[ToValueAdjustment]
	;
	commit;
	
end

go
