set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayersByStaffAndGivenField]
    (
        @staffid INT,
        @field   VARCHAR(20),
        @operatorId INT,
        @value NVARCHAR(50),
        @pageNum     INT,
        @pageSize    INT
        )
    AS
    BEGIN
        DECLARE @sql NVARCHAR(max);
        DECLARE @condition NVARCHAR(255);
        DECLARE @hideME bit;
        DECLARE @mobileColumn NVARCHAR(255);
        DECLARE @emailColumn NVARCHAR(255);


        SELECT @hideME = admin_all.fn_GetRegistry('hide_mobile_email')
        print @hideME
        IF @hideME = 1
            BEGIN
                select @mobileColumn = 'null'
                select @emailColumn = 'null'
            END
        ELSE
            BEGIN
                select @mobileColumn = 'mobile_phone'
                select @emailColumn = 'email'
            END

        IF @operatorId = 1
            SET @condition = ' like '''' + @value + N''%'''
        IF @operatorId = 2
            SET @condition = ' = '''' + @value + '''''
        IF @operatorId = 3
            SET @condition = ' like N''%'' + @value + N''%''';


        SET @sql = N'
    WITH TEMP AS (
      select
        ROW_NUMBER()
        OVER (
          ORDER BY u.partyid ASC )     AS RowNum,
        u.partyid,
        userid,
        nickname,
        first_name + '' '' + last_name as name,
        city + '', '' + country + '' '' as location,
        phone,'+
                   @mobileColumn +' as mobilePhone,
        ip as ip,'+
                   @emailColumn +' as email,
        brand.brandname as brand,
        vip_status as vipLevel
    from external_mpt.user_conf u
        join admin.casino_brand_def brand on brand.brandid = u.brandid
        left join external_mpt.UserAssociatedAccount ua on u.partyid = ua.AssociatedPartyID
    where
        u.brandid in (select brandid from [admin_all].[staff_brand_tbl] where staffid = @staffid)
        and u.' + @field + @condition + '
        and (ua.IsPrimary is NULL or ua.IsPrimary = 1)
        )
    SELECT *
    FROM TEMP
    WHERE
      (RowNum > @pageSize * (@pageNum - 1))
      AND
      (RowNum <= @pageSize * @pageNum)
    ORDER BY RowNum';

        print @sql
        EXEC sp_executesql @sql,
             N'@staffid int, @value nvarchar(500), @pageSize int, @pageNum int',
             @staffid = @staffid, @value = @value, @pageSize=@pageSize, @pageNum = @pageNum

    END

go
