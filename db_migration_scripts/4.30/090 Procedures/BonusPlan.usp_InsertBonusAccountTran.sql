set ansi_nulls, quoted_identifier on
go

create or alter procedure BonusPlan.usp_InsertBonusAccountTran
	(
		@BonusID int,
		@DateTime DateTime2(3),
		@TranType nvarchar(10),
		@AmountPlayableBonus money,
		@AmountPlayableBonusWinnings money,
		@AmountReleasedBonus money,
		@AmountReleasedBonusWinnings money,
		@AmountWageredContribution money,
		@BalancePlayableBonus money,
		@BalancePlayableBonusWinnings money,
		@BalanceReleasedBonus money,
		@BalanceReleasedBonusWinnings money,
		@AccountTranId bigint
	)
as
	begin
		declare @PartyID int, @BrandID int
			select @PartyID = (select PARTYID from admin_all.BONUS where id = @BonusID)
		select @BrandID = [admin_all].[fn_GetBrandIdFromPartyId] (@PartyID)
			insert into BonusPlan.BonusAccountTran
			(
				BonusID,
				PartyID,
				BrandID,
				DateTime,
				TranType,
				AmountPlayableBonus,
				AmountPlayableBonusWinnings,
				AmountReleasedBonus,
				AmountReleasedBonusWinnings,
				AmountWageredContribution,
				BalancePlayableBonus,
				BalancePlayableBonusWinnings,
				BalanceReleasedBonus,
				BalanceReleasedBonusWinnings,
				AccountTranId
			)
			OUTPUT
				INSERTED.ID,
				INSERTED.BonusID,
				INSERTED.BrandID,
				INSERTED.PartyID,
				INSERTED.DateTime,
				INSERTED.TranType,
				INSERTED.AmountPlayableBonus,
				INSERTED.AmountPlayableBonusWinnings,
				INSERTED.AmountReleasedBonus,
				INSERTED.AmountReleasedBonusWinnings,
				INSERTED.AmountWageredContribution,
				INSERTED.BalancePlayableBonus,
				INSERTED.BalancePlayableBonusWinnings,
				INSERTED.BalanceReleasedBonus,
				INSERTED.BalanceReleasedBonusWinnings,
				INSERTED.AccountTranId
			values
			(
				@BonusID,
				@PartyID,
				@BrandID,
				@DateTime,
				@TranType,
				@AmountPlayableBonus,
				@AmountPlayableBonusWinnings ,
				@AmountReleasedBonus ,
				@AmountReleasedBonusWinnings ,
				@AmountWageredContribution ,
				@BalancePlayableBonus ,
				@BalancePlayableBonusWinnings ,
				@BalanceReleasedBonus ,
				@BalanceReleasedBonusWinnings ,
				@AccountTranId
			)
		end

go
