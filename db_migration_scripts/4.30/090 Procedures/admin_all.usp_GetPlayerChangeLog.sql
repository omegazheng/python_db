set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerChangeLog]
  (@staffid   int, @brands nvarchar(2000), @startDate DATETIME, @endDate DATETIME, @logtype nvarchar(2000),
   @withStaff bit)
AS
  BEGIN
    set nocount on
    create table #Brands (
      BrandID int primary key
    )
    insert into #Brands (BrandID)
    SELECT distinct BRANDID
    FROM [admin_all].[STAFF_BRAND_TBL]
    WHERE STAFFID = @staffid
      AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands, ','))
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SELECT ID,
           DATE,
           STAFFID,
           COMMENT,
           l.PARTYID,
           u.USERID,
           c.BRANDNAME,
           ACTION_TYPE,
           ACTION_ID,
           FIELD_NAME,
           OLD_VALUE,
           NEW_VALUE
    FROM [admin_all].[USER_ACTION_LOG] l
           INNER JOIN [external_mpt].[USER_CONF] u WITH (NOLOCK) ON l.PARTYID = u.PARTYID
           INNER JOIN [admin].[CASINO_BRAND_DEF] c WITH (NOLOCK) ON u.BRANDID = c.BRANDID
    WHERE l.DATE >= @startDateLocal
            AND l.DATE < @endDateLocal
            AND u.BRANDID IN (SELECT x.BrandID FROM #Brands x)
            AND l.ACTION_TYPE in (SELECT distinct * FROM admin_all.fc_splitDelimiterString(@logtype, ','))
            AND (@withStaff = 0 AND l.STAFFID IS NULL)
       OR (@withStaff = 1 AND l.STAFFID = l.STAFFID)
    ORDER BY l.DATE DESC
  END

go
