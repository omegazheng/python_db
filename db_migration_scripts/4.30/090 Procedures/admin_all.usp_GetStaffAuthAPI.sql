set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetStaffAuthAPI]
  (
    @staffId INT
  )
AS
  BEGIN


    -- This should handle the duplicate API under different operation and duplicate role under different roll.
    ;with x0 as
  (
    select UR.ROLE_ID
    FROM admin_all.AUTH_USER_ROLE ur
    where ur.user_type = 'STAFF'
          and ur.USER_ID = @staffId
    union
    select id from admin_all.AUTH_ROLE ar where name = 'PUBLIC'
  ), x as
  (
    select r.Id, r.parentID
    from admin_all.auth_role r
      inner join x0 on x0.Role_id = r.id
    union all
    select r.ID, r.ParentID
    from admin_all.auth_role r
      inner join x on r.ParentID = x.ID
  ), x1 as (
      select  api , method, aro.ROLE_ID from AUTH_OPERATION_API api
        inner join admin_all.AUTH_OPERATION o on o.name = api.operation
        inner join admin_all.AUTH_ROLE_OPERATION aro on aro.OPERATION_NAME = o.name
  )
  select distinct  api, method from x1 inner join x on x.id = x1.ROLE_ID

  END

go
