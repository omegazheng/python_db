set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_MigrateCurrencyConversionRate
as
begin
	set xact_abort, nocount on
	--begin transaction
	select  a.Date, a.CurrencyFrom, a.CurrencyTo, a.Rate,Description into #1
	from (
	select a.BASE_CURRENCY CurrencyFrom , b.TO_CURRENCY CurrencyTo, a.Date Date, b.Rate Rate,
			row_number() over(partition by a.Date, a.BASE_CURRENCY, b.TO_CURRENCY order by a.Date) rn,
			max(a.NAME) over(partition by a.Date, a.BASE_CURRENCY, b.TO_CURRENCY order by a.Date) as Description 
	from admin_all.CURRENCY_CONV a with(tablockx)
		inner join admin_all.CURRENCY_CONV_RATE b on a.id = b.CURRENCY_CONV_ID
	) a
	where rn = 1
	create unique clustered index a on #1 (Date, CurrencyFrom, CurrencyTo)

	declare @Date datetime, @json nvarchar(max), @Description varchar(4000)
	declare c cursor local static for 
		select Date, max(Description)
		from #1
		group by Date
		order by 1  desc
	open c
	fetch next from c into @Date, @Description
	while @@fetch_status = 0
	begin
		select @json = (select * from #1 where Date = @Date for json path)
		exec admin_all.usp_SetCurrencyRate @Date, @Description, @json
		--select * from admin_all.CurrencyConversionRate
		--select * from admin_all.CurrencyConversionRateCurrent
		fetch next from c into @Date, @Description
	end
	close c
	deallocate c
	--commit transaction
	--rollback 
end

go
