set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_FixSynonym
as
  begin
    if exists(select * from sys.synonyms where object_id = object_id('admin_all.Synonym_ACCOUNT_TRAN_ALL') and base_object_name not like  '%[' + db_name() + ']%')
    begin
      drop synonym  admin_all.Synonym_ACCOUNT_TRAN_ALL
    end

    declare @SQL nvarchar(max)
    if not exists(select * from sys.synonyms where object_id = object_id('admin_all.Synonym_ACCOUNT_TRAN_ALL'))
      begin
        select @SQL = 'create synonym admin_all.Synonym_ACCOUNT_TRAN_ALL for ' + quotename(db_name())+'.admin_all.ACCOUNT_TRAN_ALL'
        exec(@SQL)
      end

  end

go
