set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CreateAlertMonitoringJob
as
begin
	set nocount on
	set nocount, xact_abort on 
	declare @schedule_uid uniqueidentifier, @schedule_id int, @job_name sysname, @command sysname, @job_id uniqueidentifier
	declare @CategoryName sysname = N'Omega Jobs'
	begin try
		if not exists (select * from msdb.dbo.syscategories where name=@CategoryName AND category_class=1)
			exec msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@CategoryName
	end try
	begin catch
		select @CategoryName = '[Uncategorized (Local)]'
	end catch

	create table #1( 
						schedule_id int, schedule_uid uniqueidentifier, schedule_name sysname, enabled int, freq_type int, freq_interval int, freq_subday_type int,
						freq_subday_interval int, freq_relative_interval int, freq_recurrence_factor int, active_start_date int, active_end_date int, active_start_time int, active_end_time int,
						date_created datetime, schedule_description nvarchar(4000) NULL, job_count int
					) 
	begin try
		insert into #1
			exec msdb..sp_help_schedule @schedule_name = 'Omega Jobs - Every 10 seconds'
	end try
	begin catch
		exec  msdb.dbo.sp_add_schedule @schedule_uid = @schedule_uid output, @schedule_id = @schedule_id output, @schedule_name=N'Omega Jobs - Every 10 seconds', @enabled=1, @freq_type=4, @freq_interval=1, @freq_subday_type=2, @freq_subday_interval=10, @freq_relative_interval=0, @freq_recurrence_factor=0, @active_start_date=20170720, @active_end_date=99991231, @active_start_time=0, @active_end_time=235959
		insert into #1
			exec msdb..sp_help_schedule @schedule_name = 'Omega Jobs - Every 10 seconds'
	end catch

	select @schedule_uid = schedule_uid, @schedule_id = schedule_id from #1 
	begin transaction
	
	select	@job_name = '(maint) Omega Alert',
			@command = 'exec '+quotename(db_name())+'.maint.usp_MonitorAll' 
	
	--select @schedule_uid = schedule_uid, @schedule_id = schedule_id from msdb.dbo.sysschedules where name = 'Omega Jobs - Every 5 minutes'
	--if @@rowcount = 0
	--	exec  msdb.dbo.sp_add_schedule @schedule_uid = @schedule_uid output, @schedule_id = @schedule_id output, @schedule_name=N'Omega Jobs - Every 5 minutes', @enabled=1, @freq_type=4, @freq_interval=1, @freq_subday_type=4, @freq_subday_interval=5, @freq_relative_interval=0, @freq_recurrence_factor=0, @active_start_date=20170720, @active_end_date=99991231, @active_start_time=0, @active_end_time=235959

	if not exists(select * from msdb.dbo.sysjobs where name = @job_name)
	begin
		exec msdb.dbo.sp_add_job @job_name= @job_name, @enabled=1, @notify_level_eventlog=0, @notify_level_email=0, @notify_level_netsend=0, @notify_level_page=0, @delete_level=0, @description='Monitor system and send alert', @category_name=@CategoryName, @job_id = @job_id output
		exec msdb.dbo.sp_add_jobstep @job_id=@job_id, @step_name=@command, @step_id=1, @cmdexec_success_code=0,  @on_success_action=1, @on_success_step_id=0, @on_fail_action=2, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=@command, @database_name=N'master', @flags=0
		exec msdb.dbo.sp_attach_schedule @job_id=@job_id,@schedule_id = @schedule_id
		exec msdb.dbo.sp_update_job @job_id = @job_id, @start_step_id = 1
		exec msdb.dbo.sp_add_jobserver @job_id = @job_id, @server_name = N'(local)'
	end
	commit
end

go
