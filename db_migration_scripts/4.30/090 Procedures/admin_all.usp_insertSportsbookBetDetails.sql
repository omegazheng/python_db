set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_insertSportsbookBetDetails
(
    @Currency VARCHAR (3),
    @Amount NUMERIC (38, 18),
    @Type VARCHAR (20),
    @WagerId VARCHAR (50),
    @PlatformCode VARCHAR (20),

    @Status VARCHAR (100),
    @CreatedTimestamp DATETIME,
    @SettledTimestamp DATETIME,
    @PotentialWin NUMERIC (38, 18),
    @TotalOdds NUMERIC (38, 18),
    @Cashout bit,
    @Msg nvarchar(1024),
    @BetDetailId	int = null output
)
AS

BEGIN

    insert into admin_all.SPORTSBOOK_BET_DETAILS
      (CURRENCY, AMOUNT, TYPE, WAGER_ID, PLATFORM_CODE,
       STATUS, CREATED_TIMESTAMP, SETTLED_TIMESTAMP, POTENTIAL_WIN, TOTAL_ODDS,
       CASH_OUT, MSG)
    values
      (@Currency, @Amount, @Type, @WagerId, @PlatformCode,
       @Status, @CreatedTimestamp, @SettledTimestamp, @PotentialWin, @TotalOdds,
       @Cashout, @Msg)

    select @BetDetailId = @@IDENTITY
END
go
