set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_SetCurrencyRate
(
	@Date datetime,
	@Description nvarchar(4000),
	@Rates nvarchar(max) -- [{"CurrencyFrom":"CAD","CurrencyTo":"USD","Rate":0.75},{"CurrencyFrom":"USD","CurrencyTo":"CAD","Rate":1.33}]
)
as
begin
	set nocount on
	set xact_abort off
	declare @Errormessage nvarchar(4000)
	create table #Rates(CurrencyFrom nchar(3) not null, CurrencyTo nchar(3) not null, Rate numeric(38,18) not null, Primary Key(CurrencyFrom, CurrencyTo))
	create table #Updated(CurrencyFrom nchar(3) not null, CurrencyTo nchar(3) not null, ExpiryDate datetime not null Primary Key(CurrencyFrom, CurrencyTo))
	insert into #Rates(CurrencyFrom, CurrencyTo, Rate)
	select json_value(value, '$.CurrencyFrom') CurrencyFrom, json_value(value, '$.CurrencyTo') CurrencyFrom, json_value(value, '$.Rate') Rate
	from openjson(@Rates)
	--insert into admin_all.CurrencyConversionRate(CurrencyFrom, CurrencyTo, rate, EffectiveDate, ExpiryDate)
	insert into #Rates(CurrencyFrom, CurrencyTo, Rate)
		select CurrencyFrom, CurrencyFrom, 1
		from #Rates r
		where not exists(select * from #Rates r1 where r1.CurrencyFrom = r.CurrencyFrom and r1.CurrencyTo = r.CurrencyTo)
	begin try
		begin transaction
		exec sp_getapplock 'admin_all.usp_SetCurrencyRate', 'Exclusive', 'Transaction', -1
		exec admin_all.usp_SetCurrencyRateImportSessionContext @Date, @Description
		
		update r
			set r.ExpiryDate = @Date
		output inserted.CurrencyFrom, inserted.CurrencyTo, deleted.ExpiryDate into #Updated
		from #Rates	s
			inner join admin_all.CurrencyConversionRate r on s.CurrencyFrom = r.CurrencyFrom and s.CurrencyTo = r.CurrencyTo and EffectiveDate<=@Date and ExpiryDate > @Date
		where s.Rate <> isnull(r.Rate, -1)
		
		insert into admin_all.CurrencyConversionRate(CurrencyFrom, CurrencyTo, Rate, EffectiveDate, ExpiryDate)
		select	s.CurrencyFrom, s.CurrencyTo, s.Rate, 
				case when u.CurrencyFrom is null then '1900-01-01' else @Date end EffectiveDate, 
				case when u.CurrencyFrom is null then '9999-12-31' else u.ExpiryDate end ExpiryDate
		from #Rates	s
			left outer join admin_all.CurrencyConversionRate r on s.CurrencyFrom = r.CurrencyFrom and s.CurrencyTo = r.CurrencyTo and EffectiveDate<=@Date and ExpiryDate > @Date
			left outer join #Updated u on u.CurrencyFrom = s.CurrencyFrom and u.CurrencyTo = s.CurrencyTo
		where s.Rate <> isnull(r.Rate, -1)
		commit
		exec admin_all.usp_SetCurrencyRateImportSessionContext
		
	end try
	begin catch
		select @Errormessage = error_message()
		rollback
		exec admin_all.usp_SetCurrencyRateImportSessionContext;
		throw 50000, @Errormessage, 1;
	end catch
end

go
