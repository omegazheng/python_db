set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CheckGameFinal
  (
    @GameTranID [nvarchar](100),
    @ProductID [int],
    @PartyID [int] = null
    --@GameID [nvarchar](100) = null
  )
as
  begin
    if @PartyID is not null
      begin
        if exists(SELECT GAME_TRAN_ID FROM admin_all.GAME_INSTANCE where PRODUCT_ID = @ProductID and GAME_TRAN_ID = @GameTranID and PARTY_ID = @PartyID)
          begin
            select 1
          end
        else
          begin
            select 0
          end
      end
    else
      begin
        if exists(SELECT GAME_TRAN_ID FROM admin_all.GAME_INSTANCE where PRODUCT_ID = @ProductID and GAME_TRAN_ID = @GameTranID)
          begin
            select 1
          end
        else
          begin
            select 0
          end
      end

  end

go
