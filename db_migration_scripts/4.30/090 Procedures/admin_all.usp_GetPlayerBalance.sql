set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerBalance](@PARTYID int, @IsGetDirectChildren bit)
as
begin 
	set nocount on
--declare @PartyID int = 108156, @IsGetDirectChildren bit = 1
--'/108156/27405/28683/40422/31969/29244/29641/'
	declare @SQL nvarchar(max)
	select @SQL = '
	declare @Hierarchy HierarchyID
	select  @Hierarchy = Hierarchy
	from external_mpt.USER_CONF
	where PARTYID = @PartyID
	
	;with x0 as
	(
	select 
			u.PARTYID, u.Level, u.user_type, isnull(a.BALANCE_REAL,0) BALANCE_REAL, isnull(a.CIT,0) CIT, 
			isnull(a.PLAYABLE_BONUS,0) PLAYABLE_BONUS, isnull(a.RELEASED_BONUS,0) RELEASED_BONUS, u.Hierarchy,
			case when @IsGetDirectChildren = 0 then 
					u.Hierarchy.GetAncestor(u.Level+1 - @Hierarchy.GetLevel()) 
				else 
					u.Hierarchy.GetAncestor(u.Level - @Hierarchy.GetLevel()) 
			end Root,
			ParentID
	from external_mpt.USER_CONF u 
	inner join admin_all.ACCOUNT a on u.PARTYID = a.PARTYID
	where u.Hierarchy.IsDescendantOf(@Hierarchy) = 1
		and (@IsGetDirectChildren = 0 or @IsGetDirectChildren = 1 and U.PartyID <> @PartyID)
		' + case 
			when  @IsGetDirectChildren = 0 then ''
			else ' and u.PartyID <> @PARTYID '
		end+'
	), x1 as
	(
		select *,
			case when Hierarchy <> Root and not(user_type = 0 and Root.GetLevel()= Level and Hierarchy.IsDescendantOf(Root) = 1) then 1 else 0 end IsNetwork,
			case when user_type = 0 and Root.GetLevel()= Level and Hierarchy.IsDescendantOf(Root) = 1 then 1 else 0 end IsUser
		from x0
	)
	select
			--Root.ToString(),
			max(case when Hierarchy = Root then PARTYID end) as PARTYID,
			--max(case when Hierarchy = Root then ParentID end) as ParentID, 
			max(case when Hierarchy = Root then Level end) as Level, 
			max(case when Hierarchy = Root then user_type end) as user_type, 
			max(case when Hierarchy = Root then BALANCE_REAL end) as BALANCE_REAL, 
			max(case when Hierarchy = Root then CIT end) as CIT, 
			max(case when Hierarchy = Root then PLAYABLE_BONUS end) as PLAYABLE_BONUS, 
			max(case when Hierarchy = Root then RELEASED_BONUS end) as RELEASED_BONUS,
			max(case when Hierarchy = Root then Hierarchy.ToString() end) as HierarchyString,
			isnull(sum(case when IsNetwork= 1 then CIT end ),0) Network_CIT,
			isnull(Sum(case when user_type = 0 and Root.GetLevel() = Level and Hierarchy.IsDescendantOf(Root) = 1 then CIT end),0) User_CIT,

			isnull(sum(case when IsNetwork= 1  then BALANCE_REAL end ), 0) Network_BALANCE_REAL,
			isnull(Sum(case when IsUser = 1 then BALANCE_REAL end), 0) User_BALANCE_REAL,

			isnull(sum(case when IsNetwork= 1  then PLAYABLE_BONUS end ), 0) Network_PLAYABLE_BONUS,
			isnull(Sum(case when IsUser = 1 then PLAYABLE_BONUS end), 0) User_PLAYABLE_BONUS,

			isnull(sum(case when IsNetwork= 1  then RELEASED_BONUS end), 0) Network_RELEASED_BONUS,
			isnull(Sum(case when IsUser = 1 then RELEASED_BONUS end), 0) User_RELEASED_BONUS
	from x1 s
	group by Root
	order by HierarchyString'
	--print @sql
	exec sp_executesql @SQL, N'@PARTYID int, @IsGetDirectChildren bit', @PARTYID, @IsGetDirectChildren
end

go
