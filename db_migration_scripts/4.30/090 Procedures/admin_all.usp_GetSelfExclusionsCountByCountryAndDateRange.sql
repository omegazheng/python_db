set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetSelfExclusionsCountByCountryAndDateRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

    SELECT
      count(*) as counts,
      UPPER(users.COUNTRY) as COUNTRY
    FROM external_mpt.user_conf users
      JOIN
      (
        SELECT DISTINCT PARTYID
        FROM [admin_all].[USER_ACTION_LOG] actionLog
        WHERE
          actionLog.ACTION_TYPE = 'LOCK_STATUS_CHANGE'
          AND actionLog.NEW_VALUE IN ('LOCK_BY_STAFF', 'LOCK_BY_PLYR', 'EXCLUDE_BY_PLYR')
          --and users.BRANDID in (:brandIds)
          AND actionLog.DATE >= @startDateLocal
          AND actionLog.DATE < @endDateLocal
        ) selfExclusion
        ON users.partyId = selfExclusion.PARTYID
    group by country
    order by counts desc
  END

go
