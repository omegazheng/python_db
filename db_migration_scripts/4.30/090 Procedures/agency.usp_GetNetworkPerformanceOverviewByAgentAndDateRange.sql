set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetNetworkPerformanceOverviewByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)

    select
        [agency].[fn_GetNgrByAgentAndDateRange](@agent_id, @start_date, @end_date) as ngr,
        [agency].[fn_GetBonusesByAgentAndDateRange](@agent_id, @start_date, @end_date) as bonuses,
        [agency].[fn_GetPaymentSumByTypeAndAgentAndDateRange]('DEPOSIT', @agent_id, @start_date, @end_date)  as deposits,
        [agency].[fn_GetPaymentSumByTypeAndAgentAndDateRange]('WITHDRAWAL', @agent_id, @start_date, @end_date)  as withdrawals,
        [agency].[fn_GetSignupsByAgentAndDateRange](@agent_id, @start_date, @end_date) as signups,
        [agency].[fn_GetActivesByAgentAndDateRange](@agent_id, @start_date, @end_date)  as actives,
        [agency].[fn_GetTransfersByTypeAndAgentAndDateRange]('DEPOSIT', @agent_id, @start_date, @end_date)  as transfersIn,
        [agency].[fn_GetTransfersByTypeAndAgentAndDateRange]('WITHDRAW', @agent_id, @start_date, @end_date)  as transfersOut,
        [agency].[fn_GetCommissionsByAgentAndDateRange](@agent_id, @start_date, @end_date)  as commissions

  END

go
