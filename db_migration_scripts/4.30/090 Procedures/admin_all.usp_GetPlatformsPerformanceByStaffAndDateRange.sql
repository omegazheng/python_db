set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlatformsPerformanceByStaffAndDateRange]
(
	@segmentIds VARCHAR(255) = NULL, 
	@brandIds VARCHAR(255) = NULL, 
	@period nvarchar(200),
	@AggregateType tinyint = 0
)
AS
  BEGIN
	set nocount on
	SET DATEFIRST 1
    DECLARE @brandIdsLocal VARCHAR(255)
    SET @brandIdsLocal = @brandIds
    DECLARE @segmentIdsLocal VARCHAR(255)
    DECLARE @results TABLE(name VARCHAR(255), real NUMERIC(16, 2), releasedBonus NUMERIC(16, 2), playableBonus NUMERIC(16, 2), currency VARCHAR(255))
    SET @segmentIdsLocal = @segmentIds;


	create table #Brands (BrandID int primary key)
	insert into #Brands(BrandID)
		select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brandIds,',')
	create table #Segments(SegmentID int primary key)
	insert into #Segments(SegmentID)
		select cast(Item as int) segmentId from admin_all.fc_splitDelimiterString(@segmentids,',')

    declare @EndDate date = cast(getdate() AS DATE)
    declare @StartDate date = cast(getdate() AS DATE)

    if @period = 'TODAY'
      begin
        set @EndDate = CAST(getdate() AS DATE)
        set @StartDate = CAST(getdate() AS DATE)
      end
    else if @period = 'YESTERDAY'
      begin
        SET @EndDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
        SET @StartDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
      end
    else if @period = 'WTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = CAST(DATEADD(DD, 1 - DATEPART(DW, GETDATE()), GETDATE()) AS DATE)
      end
    else if @period = 'MTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = cast(dateadd(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AS DATE)
      end
    else if @period = 'LTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
                SET @StartDate = cast(DATEADD(YEAR, -100, getdate()) AS DATE)
      end

    --INSERT INTO @results(name, real, releasedBonus, playableBonus, currency)
      SELECT
        p.NAME name,
        sum(dw.PNLReal)  real,
        sum(dw.PNLReleasedBonus)  releasedBonus,
        sum(dw.PNLPlayableBonus)  playableBonus,
        dw.currency AS currency
      FROM admin_all.fn_GetPlayerDailyAggregate(@AggregateType, @StartDate, dateadd(day, 1, @EndDate)) dw
        JOIN admin_all.platform p ON p.id = dw.ProductID
      WHERE
        dw.BrandID IN (SELECT x.BrandID from #Brands x)
        AND p.SEGMENT_ID IN (SELECT x.SegmentID from #Segments x)
        
      GROUP BY p.NAME, dw.currency
      ORDER BY p.NAME

    --SELECT *
    --FROM @results
    --ORDER BY name
  END

go
