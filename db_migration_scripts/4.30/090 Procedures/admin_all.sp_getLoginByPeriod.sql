set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getLoginByPeriod]
  (
    @brands VARCHAR(255)
  )
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @brandsLocal VARCHAR(255)
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(DAY, -32, CAST(@endDateLocal AS DATE))
    SET @brandsLocal = @brands

    SELECT
      CAST(LOGIN_TIME AS DATE) AS DATE,
      BRANDID                  AS BRAND_ID,
      COUNT(*)                 AS LOGINS
    INTO #Temp
    FROM ADMIN_ALL.USER_LOGIN_LOG
      LEFT JOIN EXTERNAL_MPT.USER_CONF
        ON USER_LOGIN_LOG.PARTYID = USER_CONF.PARTYID
    WHERE LOGIN_TIME >= @startDateLocal
          AND LOGIN_TIME < @endDateLocal
          AND LOGIN_TYPE = 'LOGIN'
          AND (@brandsLocal IS NULL
               OR BRANDID IN (SELECT *
                              FROM fc_splitDelimiterString(@brandsLocal, ',')))
    GROUP BY CAST(LOGIN_TIME AS DATE), BRANDID

    SELECT
      tt.PERIOD      AS PERIOD,
      SUM(tt.LOGINS) AS LOGINS
    FROM (
           SELECT
             'TODAY'  AS PERIOD,
             BRAND_ID AS BRAND_ID,
             LOGINS   AS LOGINS
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION

           SELECT
             'YESTERDAY' AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             LOGINS      AS LOGINS
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION

           SELECT
             'WTD'       AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             SUM(LOGINS) AS LOGINS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID

           UNION

           SELECT
             'MTD'       AS PERIOD,
             BRAND_ID    AS BRAND_ID,
             SUM(LOGINS) AS LOGINS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY BRAND_ID
         ) tt
    GROUP BY tt.PERIOD

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END
  END

go
