set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_playerDeposit]
  (@updatorid INT, @playerid INT, @depositAmount numeric(38,18), @misc NVARCHAR(MAX), @bonusplanId INT = NULL)
  AS
  BEGIN
    SET NOCOUNT ON
    SET XACT_ABORT ON
    BEGIN TRANSACTION
      DECLARE @uid INT
      DECLARE @pid INT
      DECLARE @amount numeric(38,18)
      DECLARE @msg NVARCHAR(MAX)
      DECLARE @bpId INT
      declare @uBrandID int, @pBrandID int

      SET @uid = @updatorid
      SET @pid = @playerid
      SET @amount = @depositAmount
      SET @bpId = @bonusplanId
      SET @msg = CAST(@uid AS VARCHAR(10)) + ' deposit amount: ' + CAST(CAST(@amount AS decimal (38,2)) AS VARCHAR(60)) + ' for ' +
                 CAST(@pid AS VARCHAR(10)) + '; ' + @misc
      DECLARE @pAccountID INT
      DECLARE @pAccountBalanceReal numeric(38,18)
      DECLARE @pAccountRelBonus numeric(38,18)
      DECLARE @pAccountPlaBonus numeric(38,18)
      DECLARE @uAccountId INT
      DECLARE @uAccountBalanceReal numeric(38,18)
      DECLARE @uAccountRelBonus numeric(38,18)
      DECLARE @uAccountPlaBonus numeric(38,18)

      SELECT
          @uAccountId = a.ID,
          @uAccountBalanceReal = a.BALANCE_REAL,
          @uAccountRelBonus = a.RELEASED_BONUS,
          @uAccountPlaBonus = a.PLAYABLE_BONUS,
          @uBrandID = u.BRANDID
      FROM admin_all.ACCOUNT a with(rowlock, updlock)
             inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
      WHERE u.PARTYID = @uid;

      SELECT
          @pAccountID = a.ID,
          @pAccountBalanceReal = a.BALANCE_REAL,
          @pAccountRelBonus = a.RELEASED_BONUS,
          @pAccountPlaBonus = a.PLAYABLE_BONUS,
          @pBrandID = u.BRANDID
      FROM admin_all.ACCOUNT a with(rowlock, updlock)
             inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
      WHERE u.PARTYID = @pid;

      DECLARE @releasedBonus numeric(38,18)
      DECLARE @playableBonus numeric(38,18)
      SET @releasedBonus = 0;
      SET @playableBonus = 0;
      DECLARE @insertBonusTran bit
      SET @insertBonusTran = 0;


      DECLARE @paymentId INT
      -- create payment
      INSERT INTO admin_all.PAYMENT (TYPE, METHOD, REF_NUMBER, IP_ADDRESS, REQUEST_DATE, PROCESS_DATE, STATUS, AMOUNT,
                                     ACCOUNT_ID, MISC, INFO, AMOUNT_REAL, AMOUNT_RELEASED_BONUS, REQ_BONUS_PLAN_ID,
                                     FEE, WORLD_PAY_CARD_ID, MANUAL_BANK_FIELDS, MANUAL_BANK_AGENT_ID, LAST_UPDATED_DATE,
                                     REJECTED_REASON, IS_CONVERTED, CONV_RATE, CONV_MARGIN_PERCENT, CONV_MARGIN_AMOUNT,
                                     CONV_AMOUNT, CONV_CURRENCY, CONV_RATE_AFTER_MARGIN, REQUEST_AMOUNT, REQUEST_METHOD)
      VALUES ('DEPOSIT', 'CASH', NULL, NULL, GETDATE(), GETDATE(), 'COMPLETED', @amount, @pAccountID, @msg, NULL, @amount,
              @releasedBonus,
              @bpId,
              0, NULL,
              NULL,
              NULL,
              GETDATE(),
              NULL,
              0,
              NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      SELECT @paymentId = SCOPE_IDENTITY()

      IF @bpId IS NOT NULL
        BEGIN
          -- apply bonus
          DECLARE @bonusAmount numeric(38,18)
          DECLARE @bonusAwardType VARCHAR(25)
          DECLARE @isFreeBet BIT
          DECLARE @isIncremental BIT
          DECLARE @incrementalCount INT
          DECLARE @expiryDays INT
          DECLARE @isPlayable BIT
          DECLARE @wagerReq numeric(38,18)
          DECLARE @multiplier VARCHAR(25)

          SELECT
              @bonusAmount = AMOUNT,
              @bonusAwardType = AWARD_TYPE,
              @isPlayable = IS_PLAYABLE,
              @isFreeBet = IS_FREE_BET,
              @isIncremental = IS_INCREMENTAL,
              @incrementalCount = INCREMENTAL_COUNT,
              @expiryDays = EXPIRY_DAYS,
              @wagerReq = WAGER_REQ,
              @multiplier = MULTIPLIER
          FROM admin_all.BONUS_PLAN
          WHERE ID = @bpId;

          IF (@bonusAwardType = 'FLAT' OR @isFreeBet = 1) AND @isIncremental = 1
            SET @bonusAmount = ROUND(@bonusAmount / @incrementalCount, 2)
          IF @bonusAwardType = 'PERCENT'
            SET @bonusAmount = ROUND(@depositAmount * @bonusAmount / 100, 2)

          -- create bonus
          DECLARE @bonusStatus VARCHAR(25)
          DECLARE @bonusWagerReq numeric(38,18)
          DECLARE @freeBetBalance numeric(38,18)

          IF (SELECT COUNT(*)
              FROM admin_all.BONUS
              WHERE PARTYID = @pid AND (STATUS = 'ACTIVE' OR STATUS = 'QUEUED')) > 0
            SET @bonusStatus = 'QUEUED'
          ELSE
            SET @bonusStatus = 'ACTIVE'

          IF @isPlayable = 1
            BEGIN
              SET @insertBonusTran = 1;
              SET @playableBonus = @bonusAmount;
            END
          ELSE
            SET @playableBonus = 0;

          IF @isFreeBet = 1
            SET @bonusWagerReq = @wagerReq
          ELSE
            IF @multiplier = 'BONUS'
              SET @bonusWagerReq = @bonusAmount * @wagerReq
            ELSE
              SET @bonusWagerReq = @depositAmount * @wagerReq

          IF @isIncremental = 1
            SET @bonusWagerReq = @bonusAmount * @wagerReq

          IF @isFreeBet = 1
            SET @freeBetBalance = @bonusAmount
          ELSE
            SET @freeBetBalance = 0

          INSERT INTO admin_all.BONUS (PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED,
                                       WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN,
                                       RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS,
                                       RELEASED_BONUS_AMOUNT, RELEASED_BONUS_WINNINGS_AMOUNT, FREE_BET_BALANCE,
                                       BONUS_INCREMENTAL_ID, EXTERNAL_BONUS_ID)
          VALUES
          (@pid, @bpId, GETDATE(), DATEADD(DD, @expiryDays, GETDATE()), @bonusStatus, 0, @bonusWagerReq, @bonusAmount,
           @paymentId,
           GETDATE(), 0, 0, @releasedBonus, 0, @playableBonus, 0, 0, @freeBetBalance, NULL, NULL);
        END

      -- update player account
      UPDATE admin_all.ACCOUNT
      SET BALANCE_REAL = @amount + BALANCE_REAL,
          PLAYABLE_BONUS = @playableBonus + PLAYABLE_BONUS
      WHERE PARTYID = @pid;


      DECLARE @reference VARCHAR(100)
      SET @reference = 'BEEVOUCHER ' + CAST(@uid AS VARCHAR(10)) + ' - ' + CAST(@pid AS VARCHAR(10))

      -- update player
      UPDATE external_mpt.USER_CONF
      SET
        LAST_DEPOSIT_DATE = GETDATE(),
        LAST_DEPOSIT_AMOUNT = @amount,
        LAST_DEPOSIT_TYPE = 'CASH'
      WHERE PARTYID = @pid

      -- create accountTran
      DECLARE @accountTranId INT
      INSERT INTO admin_all.ACCOUNT_TRAN (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID,
                                          GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID,
                                          AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS,
                                          BALANCE_PLAYABLE_BONUS, AMOUNT_RAW_LOYALTY,
                                          BALANCE_RAW_LOYALTY, TRANSACTION_ON_HOLD_ID,
                                          SSW_TRAN_ID, REFERENCE, BRAND_ID)
      VALUES
      (@pAccountID, GETDATE(), 'DEPOSIT', @amount, @pAccountBalanceReal + @amount, NULL, NULL, NULL, NULL, @paymentId,
       0, NULL,
       @releasedBonus,
       0,
       @pAccountRelBonus + @releasedBonus,
       @pAccountPlaBonus + 0,
       0, 0,
       NULL, NULL, @reference, @pBrandID);

      SELECT @accountTranId = @@identity

      -- create bonus accountTran
      IF @insertBonusTran = 1
        BEGIN
          INSERT INTO admin_all.ACCOUNT_TRAN (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID,
                                              GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID,
                                              AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS,
                                              BALANCE_PLAYABLE_BONUS, AMOUNT_RAW_LOYALTY,
                                              BALANCE_RAW_LOYALTY, TRANSACTION_ON_HOLD_ID,
                                              SSW_TRAN_ID, REFERENCE, BRAND_ID)
          VALUES
          (@pAccountID, GETDATE(), 'CRE_BONUS', 0, @pAccountBalanceReal + @amount, NULL, NULL, NULL, NULL, NULL, 0,
           NULL, @releasedBonus, @playableBonus, @pAccountRelBonus + @releasedBonus, @pAccountPlaBonus + @playableBonus,
           0, 0, NULL, NULL, NULL, @pBrandID);
        END

      -- create userActionLog
      INSERT INTO admin_all.USER_ACTION_LOG (DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME,
                                             OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
      VALUES (GETDATE(), NULL, @msg, @pid, 'DEPOSIT', @accountTranId, 'BALANCE_REAL', @pAccountBalanceReal,
              @pAccountBalanceReal + @amount, NULL);

      -- update agent account
      UPDATE admin_all.ACCOUNT
      SET BALANCE_REAL = -@amount + BALANCE_REAL
      WHERE PARTYID = @uid;

      -- create accountTran
      INSERT INTO admin_all.ACCOUNT_TRAN (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID,
                                          GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID,
                                          AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS,
                                          BALANCE_PLAYABLE_BONUS, AMOUNT_RAW_LOYALTY,
                                          BALANCE_RAW_LOYALTY, TRANSACTION_ON_HOLD_ID,
                                          SSW_TRAN_ID, REFERENCE, BRAND_ID)
      VALUES
      (@uAccountID, GETDATE(), 'P_DEPOSIT', -@amount, @uAccountBalanceReal - @amount, NULL, NULL, NULL, NULL, NULL,
       0, NULL,
       0,
       0,
       @uAccountRelBonus,
       @uAccountPlaBonus,
       0, 0,
       NULL, NULL, @reference, @uBrandID);

      set @accountTranId = @@identity

      -- create userActionLog
      INSERT INTO admin_all.USER_ACTION_LOG (DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME,
                                             OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
      VALUES (GETDATE(), NULL, @msg, @uid, 'P_DEPOSIT', @accountTranId, 'BALANCE_REAL', @uAccountBalanceReal,
              @uAccountBalanceReal - @amount, NULL);

      SELECT *
      FROM admin_all.ACCOUNT
      WHERE PARTYID = @pid;

    COMMIT
  END

go
