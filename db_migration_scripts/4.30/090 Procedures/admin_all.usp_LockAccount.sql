set ansi_nulls, quoted_identifier on
go

-- It takes either account id or party id and eventually based on the accountId to create applock
-- It will applock LockAccount-99999 to make sure no race condition.
create or alter procedure admin_all.usp_LockAccount
(
  @AccountID INT,
  @PartyID   INT
)
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @res NVARCHAR(128)
  IF @@trancount = 0
    BEGIN
      RAISERROR ('admin_all.usp_LockAccount must be run within a transaction', 16, 1)
      RETURN -1
    END
  IF @AccountID IS NULL
    BEGIN
      SELECT @AccountID = ID
      FROM admin_all.Account
      WHERE PartyID = @PartyID
    END
  IF @AccountID IS NULL
    BEGIN
      RAISERROR ('AccountID should not be null', 16, 1)
      RETURN
    END
  SELECT @res = 'LockAccount-' + cast(@AccountID AS NVARCHAR(20))
  EXEC sp_getapplock @res, 'Exclusive', 'Transaction', -1
END

go
