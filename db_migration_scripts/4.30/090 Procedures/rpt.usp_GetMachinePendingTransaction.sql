set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetMachinePendingTransaction
(
	@StaffID int	-- should be company staff? machines should be covered by staff company
)
as
begin
	set nocount on
	select	mt.MachineTranID, mt.MachineID, mt.PartyID, eft.EFTID,
			mt.StartDate, eft.AmountReal, eft.PlayableBonus, eft.ReleasedBonus ,eft.Status, eft.Reference, eft.TransferType,
			eft.Datetime EFTDate, 
			u.FIRST_NAME + ' ' + u.LAST_NAME as UserName, u.USERID, u.CURRENCY, pl.CODE as ProductCode, pl.name as ProductName
	from admin_all.MachineTran mt
		inner join admin_all.MachineTranEFT eft on mt.MachineTranID = eft.MachineTranID 
		inner join external_mpt. USER_CONF u on u.PARTYID = mt.PartyID
		inner join admin_all.Machine m on m.MachineID = mt.MachineID
		left join admin_all.PLATFORM pl on m.ProductID = pl.ID
	where eft.Status in ('Pending', 'InReview')
		and mt.EndDate is null
end

go
