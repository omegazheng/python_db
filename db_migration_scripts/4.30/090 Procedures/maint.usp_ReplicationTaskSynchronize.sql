set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicationTaskSynchronize
as
begin
	set nocount on
	set xact_abort on
	declare @Name varchar(128)
	declare c cursor local static for
		select rc.Name
		from maint.ReplicationTask r --with(readcommittedlock, readpast, rowlock)
			left join maint.ReplicationLastExecution rl on r.ConfigurationID = rl.ConfigurationID
			inner join maint.ReplicationConfiguration rc on rc.ConfigurationID = r.ConfigurationID
		where rl.ToValue is not null
	open c
	fetch next from c into @Name
	while @@fetch_status = 0
	begin
		exec maint.usp_ReplicateOne @Name
		fetch next from c into @Name
	end
	close c
	deallocate c

end

go
