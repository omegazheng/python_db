set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SetReplicateOneContextInfo
as
begin
	set context_info 0x6D61696E742E7573705F5265706C69636174654F6E65 -- select cast('maint.usp_ReplicateOne' as varbinary(128)), len(cast('maint.usp_ReplicateOne' as varbinary(128)))
end

go
