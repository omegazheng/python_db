set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddMachineProduct
(
	@ProductID int = null output ,
	@Code nvarchar(50),
	@Name nvarchar(200)
)
as
begin
	set nocount on 
	select @ProductID = ID
	from admin_all.PLATFORM
	where Code = @Code
	if @@rowcount > 0
		return
	insert into admin_all.PLATFORM (CODE, NAME, IS_ENABLED, USERNAME, PASSWORD, WALLET_TYPE, PLATFORM_TYPE, CONVERTJACKPOTS, SESSION_LIMIT_WEB, SESSION_LIMIT_MOBILE, REALITY_CHECK_WEB, REALITY_CHECK_MOBILE)
		values(@Code, @Name, 1, 'EFT', 'setlater', 'EFT', 'CASINO', 1, 'iframe', 'iframe', 'iframe', 'iframe');
	select @ProductID = scope_identity()
end

go
