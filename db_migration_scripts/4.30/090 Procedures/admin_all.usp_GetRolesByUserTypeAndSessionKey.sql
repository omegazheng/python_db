set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetRolesByUserTypeAndSessionKey]
  (
    @userType VARCHAR(20),
    @sessionKey VARCHAR(200)
  )
AS
 BEGIN
    DECLARE @TYPE VARCHAR(20)
    SET @TYPE = @userType;

  IF @TYPE = 'staff'
    BEGIN
      SELECT
          ar.ID,
          ar.name,
          ar.DESCRIPTION,
          ar.ParentID,
          ar.Level,
          ar.Hierarchy.ToString() as hierarchy,
          ar.DisplayOrder
      FROM AUTH_ROLE ar
        JOIN AUTH_USER_ROLE aur ON ar.ID = aur.ROLE_ID
        JOIN STAFF_AUTH_TBL sat ON aur.USER_ID = sat.STAFFID
      WHERE aur.USER_TYPE = @TYPE AND sat.COOKIE = @sessionKey
    END
  IF @TYPE = 'agency'
    BEGIN
      SELECT
        ar.ID,
        ar.name,
        ar.DESCRIPTION,
        ar.ParentID,
        ar.Level,
        ar.Hierarchy.ToString() as hierarchy,
        ar.DisplayOrder
      FROM AUTH_ROLE ar
        JOIN AUTH_USER_ROLE aur ON ar.ID = aur.ROLE_ID
        JOIN USER_WEB_SESSION uws ON aur.USER_ID = uws.PARTYID
      WHERE aur.USER_TYPE = @TYPE AND uws.SESSION_KEY = @sessionKey
    END
END

go
