set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetPlayerSessionFactor
(
	@StartDate datetime = null,
	@EndDate datetime = null,
	@BrandIDs nvarchar(2048) = null,
	@PartyID int = null,
  @Threshold decimal = 1000
)
as
begin
	set nocount on
	if object_id('tempdb..#BrandIDs') is not null
		drop table #BrandIDs
	create table #BrandIDs(BrandID int primary key)
	insert into #BrandIDs(BrandID)
		select distinct cast(Item as int)
		from admin_all.fc_splitDelimiterString(@BrandIDs,',')
		where rtrim(Item)<> ''
					and isnumeric(Item) = 1
	if not exists(select BrandID from #BrandIDs)
		insert into #BrandIDs (BrandID) select BrandID from admin.CASINO_BRAND_DEF


	if object_id('tempdb..#PartyIDs') is not null
		drop table #PartyIDs
	create table #PartyIDs(PartyID int primary key)

	if @PartyID is null
		begin
				insert into #PartyIDs (PartyID)
					select u.PARTYID from external_mpt.USER_CONF u
						join #BrandIDs b on u.BRANDID = b.BrandID
			end
	else
		begin
			insert into #PartyIDs (PartyID) select @PartyID
		end

	if @StartDate is null SELECT  @StartDate =  DATEADD(year, -20, GETDATE())
	if @EndDate is null SELECT  @EndDate =  DATEADD(day, 1, GETDATE())


		;with x0 as
(
		SELECT
			dw.BRAND_ID BrandID, dw.PARTY_ID as PartyID,
			lag(dw.SUMMARY_DATE) OVER (PARTITION BY dw.PARTY_ID ORDER BY dw.SUMMARY_DATE) PreviousDate,
			dw.SUMMARY_DATE CurrentDate
		FROM admin_all.DW_GAME_PLAYER_DAILY dw
			inner join #PartyIDs on dw.PARTY_ID = #PartyIDs.PartyID
		where dw.SUMMARY_DATE >= @StartDate and dw.Summary_Date < @EndDate
		group by dw.BRAND_ID, dw.PARTY_ID, dw.SUMMARY_DATE
)

	select
		BrandID,
		PartyID,
		AverageSession,
		DaysSinceLastSession,
		MostRecentSessionDate,
		(case when AverageSession = 0 then DaysSinceLastSession  else 1.0 * DaysSinceLastSession / AverageSession end) as ChurnFactor
	from (
select
	BrandID, PartyID,
	--PreviousDate, CurrentDate,
	avg(
			case when datediff(day, isnull(PreviousDate, CurrentDate), CurrentDate) <= @Threshold
				then datediff(day, isnull(PreviousDate, CurrentDate), CurrentDate)
		  	else 0
			end
	) AverageSession,
	max(CurrentDate) as MostRecentSessionDate,
	datediff(day, max(CurrentDate), getdate()) DaysSinceLastSession
from x0
group by x0.PartyID, x0.BrandID
	) result
	end

go
