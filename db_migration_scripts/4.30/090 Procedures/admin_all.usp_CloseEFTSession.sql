set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CloseEFTSession
(
	
	@MachineID int,
	@PartyID int
)
as
begin
	set nocount, xact_abort  on
	begin transaction
	exec admin_all.usp_LockTwoCheeryTransaction @PartyID = @PartyID, @MachineID = @MachineID

	update admin_all.MachineTran
		set EndDate = getdate()
	where PartyID = @PartyID
		and MachineID = @MachineID
		and EndDate is null
	
	commit
end

go
