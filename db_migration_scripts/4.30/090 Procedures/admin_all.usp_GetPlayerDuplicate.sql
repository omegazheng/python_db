set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerDuplicate]
  (@BrandID Integer = null)
AS
  BEGIN

	create table #Users (
    PartyID int primary key,
    UserID nvarchar(100),
    FirstName nvarchar(100),
    LastName nvarchar(100),
    BirthDate date,
    Email nvarchar(255),
    PostalCode nvarchar (100),
    LockedStaus nvarchar(100),
    ActivationStatus bit
    )

    create unique nonclustered index [idx_#Users_PartyID] on #Users (
      [PartyID] asc
    ) with (ignore_dup_key = on) on [primary]

    insert into #Users
      select
        PartyID,
        UserID,
        admin_all.fn_getAlphaNumeric(FIRST_NAME),
        admin_all.fn_getAlphaNumeric(LAST_NAME),
        isnull(BIRTHDATE, convert(datetime,'1900-01-01')) as BirthDate,
        admin_all.fn_getAlphaNumeric(EMAIL) Email,
        admin_all.fn_getAlphaNumeric(POSTAL_CODE) PostalCode,
        LOCKED_STATUS as LockedStaus,
        ACTIVE_FLAG as ActivationStatus
      from external_mpt.USER_CONF where BRANDID = @BrandID

    create table #User_Duplicates (
      PartyID int,
      DuplicateUserID nvarchar(100)
      CONSTRAINT PK_User_Duplicates PRIMARY KEY (PartyID,DuplicateUserID) with (ignore_dup_key = on)
    )

    create index IDX_UserDuplicates_PartyID on #User_Duplicates (PartyID)

    declare @UserCursor as cursor;

    declare @PartyID int, @FirstName nvarchar(100), @LastName nvarchar(100), @Email nvarchar(100), @PostalCode nvarchar(100),
      @BirthDate date;


    SET @UserCursor = CURSOR FOR
    SELECT PartyID, FirstName, LastName, Email, PostalCode, BirthDate FROM #Users;

    OPEN @UserCursor;
    FETCH NEXT FROM @UserCursor INTO @PartyID,@FirstName, @LastName, @Email, @PostalCode, @BirthDate;

    WHILE @@FETCH_STATUS = 0
      BEGIN
        --PRINT @PartyID;
        Insert into #User_Duplicates
          select @PartyID as PartyID, UserID as DuplicateUserID from #Users where PartyID <> @PartyID and
                                                                ((FirstName = @FirstName and LastName = @LastName and
                                                                  Email = @Email and PostalCode = @PostalCode)
                                                                 or (FirstName = @FirstName and LastName = @LastName and
                                                                     Email = @Email and BirthDate = @BirthDate)
                                                                 or (FirstName = @FirstName and LastName = @LastName and
                                                                     PostalCode = @PostalCode and BirthDate = @BirthDate)
                                                                 or (LastName = @LastName and BirthDate = @BirthDate and
                                                                     Email = @Email and PostalCode = @PostalCode)
                                                                 or (FirstName = @FirstName and BirthDate = @BirthDate and
                                                                     Email = @Email and PostalCode = @PostalCode)
                                                                )

        FETCH NEXT FROM @UserCursor INTO @PartyID,@FirstName, @LastName, @Email, @PostalCode, @BirthDate;
      END
    CLOSE @UserCursor;
    DEALLOCATE @UserCursor;
    select
      u.PARTYID, u.USERID, u.FIRST_NAME, u.LAST_NAME, u.EMAIL, u.BIRTHDATE, u.POSTAL_CODE,
      u.LOCKED_STATUS, u.ACTIVE_FLAG,
      result.Duplicates from (
      select d.PartyID,
        (select list.DuplicateUserID+ '|' from #User_Duplicates list
        where list.PartyID = d.PartyID Order by d.PartyID FOR xml path('')) as duplicates
      from #User_Duplicates d group by d.PartyID) result
    join external_mpt.user_conf u
      on result.PartyID = u.PARTYID


  END

go
