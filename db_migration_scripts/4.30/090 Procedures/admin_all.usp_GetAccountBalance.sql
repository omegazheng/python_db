set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetAccountBalance
(
	@PartyID int
)
as
begin
	set nocount on
	if admin_all.fn_IsMultiCurrencyEnabled() = 1
	begin
		select 
				a.BALANCE_REAL AmountReal, a.PLAYABLE_BONUS PlayableBonus, a.RELEASED_BONUS ReleasedBonus, uc.AssociatedCurrency,
				a.BALANCE_REAL * cr.Rate PrimaryAmountReal, a.PLAYABLE_BONUS * cr.Rate PrimaryPlayableBonus, a.RELEASED_BONUS * cr.Rate PrimaryReleasedBonus,
				uc.Currency PrimaryCurrency
		from admin_all.ACCOUNT a
			inner join external_mpt.v_UserPrimaryCurrency uc on uc.AssociatedAccountID = a.id
			cross apply admin_all.fn_GetCurrencyConversionRate1(null, uc.AssociatedCurrency, uc.Currency) cr
		where uc.PartyID = @PartyID
		return
	end
	select 
			a.BALANCE_REAL AmountReal, a.PLAYABLE_BONUS PlayableBonus, a.RELEASED_BONUS ReleasedBonus, u.Currency AssociatedCurrency,
			a.BALANCE_REAL PrimaryAmountReal, a.PLAYABLE_BONUS PrimaryPlayableBonus, a.RELEASED_BONUS PrimaryReleasedBonus,
			u.Currency PrimaryCurrency
	from admin_all.ACCOUNT a
		inner join external_mpt.v_UserPrimaryCurrency u on u.PartyID = a.PARTYID
	where u.PartyID = @PartyID
end
go
