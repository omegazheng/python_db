set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetTopTenNetworkByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)
    declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);

    if @user_type <> 20
    begin
        declare @topten TABLE(
        	partyid int, parentid int, userType int, userid varchar(100), name varchar(50), hierarchy hierarchyid,
        	ngr numeric(38,18), signups int, actives int
        )
        insert into @topten
        	select top 10 entry.*
        	from
        		[agency].[fn_GetTopTenEntryByAgentAndDateRange](@start_date, @end_date) as entry
        	where
        	    hierarchy.IsDescendantOf(@agentHierarchy)=1
        	    and partyid <> @agent_id
        	order by ngr desc
        select
        	entry.hierarchy.ToString() as hierarchy, entry.partyid, entry.parentid, entry.userType, entry.userid, entry.name,
        	entry.ngr, entry.signups, entry.actives
        from [agency].[fn_GetTopTenEntryByAgentAndDateRange](@start_date, @end_date) as entry
        where
        	(partyid in (select partyid from @topten) or partyid in (select parentid from @topten))
        	and partyid <> @agent_id
        order by hierarchy asc
    end
    else
        select top 10
            partyid, parentid, name, userType, userid, hierarchy,
            ngr, signups, actives
        from (
            select
                subagent.partyid as partyid,
                subagent.parentid as parentid,
                subagent.user_type as userType,
                subagent.userid as userid,
                subagent.first_name + ' ' + subagent.last_name as name,
                subagent.hierarchy.ToString() as hierarchy,
                [agency].[fn_GetNgrByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as ngr,
                [agency].[fn_GetSignupsByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as signups,
                [agency].[fn_GetActivesByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as actives

            from external_mpt.user_conf as subagent
            where
                subagent.hierarchy.IsDescendantOf(@agentHierarchy)=1
                and subagent.partyid <> @agent_id
        ) x
        order by ngr desc


  END

go
