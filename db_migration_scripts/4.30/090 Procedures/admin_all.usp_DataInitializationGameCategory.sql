set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationGameCategory
as
begin
	set nocount on
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.Game_Category', @ForceIdentityColumnInsert = 0, @ForceMatchedDataUpdate = 0, @PrimaryKeys = 'NAME', @ExcludedColumns = null
	--admin_all.Game_Category
	begin transaction
	;with s as 
	(
		select [NAME],[IS_SLOTS],[IS_CARDGAME],[IS_TABLEGAME],[IS_BINGO],[IS_BLACKJACK_GAME],[IS_ROULETTE_GAME]
		from (
				values (N'EFT',0,0,0,0,0,0)
			) v([NAME],[IS_SLOTS],[IS_CARDGAME],[IS_TABLEGAME],[IS_BINGO],[IS_BLACKJACK_GAME],[IS_ROULETTE_GAME])
	)
	merge admin_all.Game_Category t
	using s on s.[NAME]= t.[NAME]
	when not matched then
		insert ([NAME],[IS_SLOTS],[IS_CARDGAME],[IS_TABLEGAME],[IS_BINGO],[IS_BLACKJACK_GAME],[IS_ROULETTE_GAME])
		values(s.[NAME],s.[IS_SLOTS],s.[IS_CARDGAME],s.[IS_TABLEGAME],s.[IS_BINGO],s.[IS_BLACKJACK_GAME],s.[IS_ROULETTE_GAME])
	
	;
	commit;
end
go
