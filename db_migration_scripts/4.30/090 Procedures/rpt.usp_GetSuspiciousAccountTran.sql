set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetSuspiciousAccountTran
  (
    @DateFrom Date,
    @DateTo Date = null,
    @PartyID int = null
    )
  as
  begin
    set nocount, xact_abort on
    declare @SQL nvarchar(max)
    select @DateTo = dateadd(day, 1,isnull(@DateTo, getdate()))


    if isnull(@PartyID, 0) <> 0
      begin
          select mh.*, u.CURRENCY, u.USERID, p.CODE ProductCode
          from admin_all.MachineTranHeader mh
                 inner join external_mpt.USER_CONF u on u.PARTYID = mh.PARTYID
                 left join admin_all.PLATFORM p on p.id = mh.ProductID
          where mh.Status = 'InReview'
            and mh.DATETIME >= @DateFrom and mh.DATETIME < @DateTo
            and mh.PartyID=@PartyID
      end
    else
      begin
          select mh.*, u.CURRENCY, u.USERID, p.CODE ProductCode
          from admin_all.MachineTranHeader mh
                 inner join external_mpt.USER_CONF u on u.PARTYID = mh.PARTYID
                 left join admin_all.PLATFORM p on p.id = mh.ProductID
          where mh.Status = 'InReview'
            and mh.DATETIME >= @DateFrom and mh.DATETIME < @DateTo
      end

  end


go
