set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_MigrateStaffPermission @StaffID int = null
as
begin
	set nocount on
	select cast(STAFFID as int) STAFFID, cast(p.PERMISSION_TYPE as varchar(20)) as PERMISSION_TYPE
		into #StuffPermission
	from admin_all.STAFF_AUTH_TBL
		cross apply (values('SHOW_PHONE'), ('SHOW_MOBILE'), ('SHOW_EMAIL')) p(PERMISSION_TYPE)
	where @StaffID is null or STAFFID = @StaffID
	--select * 
	delete sp
	from #StuffPermission sp
	where exists(
					select * 
					from admin_all.STAFF_ROLE sr 
						inner join admin_all.ROLE_PERMISSION rp on sr.ROLE_ID = rp.ROLE_ID
					where sr.STAFF_ID = sp.STAFFID
						and (
								sp.PERMISSION_TYPE = 'SHOW_PHONE' and rp.TYPE = 'HIDE_DATA' and rp.SUBTYPE = 'PHONE'
							or	sp.PERMISSION_TYPE = 'SHOW_MOBILE' and rp.TYPE = 'HIDE_DATA' and rp.SUBTYPE = 'MOBILEPHONE'
							or	sp.PERMISSION_TYPE = 'SHOW_EMAIL' and rp.TYPE = 'HIDE_DATA' and rp.SUBTYPE = 'EMAIL'
						)
				)
	insert into #StuffPermission(STAFFID, PERMISSION_TYPE)
		select STAFF_ID, 'ENABLE_EXPORT'
		from admin_all.STAFF_ROLE sr 
			inner join admin_all.ROLE_PERMISSION rp on sr.ROLE_ID = rp.ROLE_ID
		where rp.TYPE = 'ENABLE_EXPORT'
			and (@StaffID is null or sr.STAFF_ID = @StaffID)
	--begin transaction
	;with t as
	(
		select * 
		from admin_all.AUTH_PERMISSION a
		where exists(select * from #StuffPermission sp where sp.STAFFID = a.USER_ID)
			and a.USER_TYPE = 'STAFF'
	)
	merge t
	using
		(select staffid, permission_type from #StuffPermission group by staffid, permission_type) s
on t.USER_ID = s.STAFFID and t.PERMISSION_TYPE = s.PERMISSION_TYPE
	when matched and t.ENABLE = 0 then
		update set ENABLE = 1
	when not matched then
		insert (USER_ID, USER_TYPE, PERMISSION_TYPE, ENABLE)
			values(s.STAFFID, 'STAFF', s.PERMISSION_TYPE, 1)
	--when not matched by source then
	--	delete 
	;
	--select * from admin_all.AUTH_PERMISSION where USER_ID = 7316
	--rollback
end

go
