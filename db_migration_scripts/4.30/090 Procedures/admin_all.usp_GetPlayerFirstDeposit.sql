set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerFirstDeposit]
  (@staffid int, @brands nvarchar(2000), @startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    set nocount on
    create table #Brands (
      BrandID int primary key
    )
    insert into #Brands (BrandID)
    SELECT distinct BRANDID
    FROM [admin_all].[STAFF_BRAND_TBL]
    WHERE STAFFID = @staffid
      AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands, ','))

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT id,
           method,
           process_date,
           amount,
           fee,
           account_id                         AS account_id,
           partyid,
           userid,
           country,
           reg_date,
           brandname,
           currency,
           brandid,
           total_withdrawal,
           total_deposit,
           (total_deposit - total_withdrawal) AS net_position
    FROM (SELECT p.id,
                 method,
                 p.process_date,
                 p.amount,
                 p.fee,
                 p.account_id,
                 u.partyid,
                 u.userid,
                 c.name                        AS country,
                 u.reg_date,
                 u.brandid,
                 b.brandname,
                 u.currency,
                 d.total_deposit,
                 isnull(w.total_withdrawal, 0) AS total_withdrawal
          FROM [admin_all].[PAYMENT] p WITH (NOLOCK)
                 INNER JOIN [admin_all].[account] a WITH (NOLOCK) ON p.account_id = a.id
                 INNER JOIN [external_mpt].[user_conf] u WITH (NOLOCK) ON a.partyid = u.partyid
                 INNER JOIN [admin_all].[COUNTRY] c WITH (NOLOCK) ON u.country = c.iso2_code
                 INNER JOIN [admin].[casino_brand_def] b WITH (NOLOCK) ON b.brandid = u.brandid
                 INNER JOIN (SELECT min(id) AS id, account_id, sum(amount) AS total_deposit
                             FROM [admin_all].[PAYMENT] WITH (NOLOCK)
                             WHERE type = 'DEPOSIT'
                               AND status = 'COMPLETED'
                             GROUP BY account_id) d ON d.id = p.id AND d.ACCOUNT_ID = p.ACCOUNT_ID
                 LEFT JOIN (SELECT account_id, sum(amount) AS total_withdrawal
                            FROM [admin_all].[PAYMENT] WITH (NOLOCK)
                            WHERE type = 'WITHDRAWAL'
                              AND status = 'COMPLETED'
                            GROUP BY account_id) w ON w.account_id = p.account_id) a
    WHERE process_date >= @startDateLocal
      AND process_date < @endDateLocal
      AND brandId IN (SELECT x.BrandID FROM #Brands x)
    ORDER BY process_date DESC
  END

go
