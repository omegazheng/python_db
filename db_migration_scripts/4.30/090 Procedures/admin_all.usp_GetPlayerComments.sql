set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerComments]
  (@partyid INT)
AS
  BEGIN
    SET NOCOUNT ON
    SELECT
      uc.ID,
      uc.DATE,
      uc.STAFFID,
      uc.COMMENT,
      uc.PARTYID,
      staff.LOGINNAME AS STAFF_NAME,
      result.Tags     AS TAGS
    FROM
      (SELECT
         USER_COMMENTS_ID,
         (SELECT CONVERT(varchar(100), list.tagCode) + ', '
          FROM
            (SELECT
               USER_COMMENTS_ID,
               CODE tagCode
             FROM USER_COMMENT_TAG uct
               JOIN comment_tag ct ON uct.COMMENT_TAG_ID = ct.ID) list
          WHERE list.USER_COMMENTS_ID = uct.USER_COMMENTS_ID
          ORDER BY list.USER_COMMENTS_ID
          FOR XML PATH ('')) AS Tags
       FROM (
              SELECT
                USER_COMMENTS_ID,
                CODE
              FROM USER_COMMENT_TAG uct
                JOIN comment_tag ct ON uct.COMMENT_TAG_ID = ct.ID
            ) uct
       GROUP BY uct.USER_COMMENTS_ID) result
      RIGHT JOIN user_comments uc ON result.USER_COMMENTS_ID = uc.ID
      RIGHT JOIN STAFF_AUTH_TBL staff ON staff.STAFFID = uc.STAFFID
    WHERE uc.PARTYID = @partyid
    ORDER BY DATE DESC
  END

go
