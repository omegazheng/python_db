set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_AppLock
(
  @Resource NVARCHAR(255), @LockTimeout INT
)
AS
BEGIN
  SET NOCOUNT, xact_abort ON
  DECLARE @ret int

  IF @Resource IS NULL
    BEGIN
      RAISERROR ('Resource can not be null', 16, 1)
      RETURN
    END

  BEGIN transaction
    EXEC @ret = sp_getapplock @Resource, 'Exclusive', 'Transaction', @LockTimeout
    if @ret < 0
      begin
        rollback
        raiserror('Resource %i is being acquired by another session.', 16, 1, @Resource)
        return
      end
  COMMIT
END

go
