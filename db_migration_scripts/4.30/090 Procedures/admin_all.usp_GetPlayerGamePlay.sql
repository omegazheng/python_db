set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerGamePlay]
  (@partyid INT)
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @endDateLocal DATETIME = GETDATE()
    DECLARE @startDateLocal DATETIME = CAST(DATEADD(YEAR, 100, 0) AS DATE)
    DECLARE @amountLocal INT = 5

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
      'TODAY'         AS PERIOD,
      (GAME_NAME)     AS GAME_NAME,
      GAME_ID,
      SUM(GAME_COUNT) AS GAME_COUNT
    FROM admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE = CAST(@endDateLocal AS DATE)
    GROUP BY
      PARTY_ID, PLATFORM, GAME_NAME, GAME_ID
    ORDER BY SUM(GAME_COUNT) DESC) today

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
      'YESTERDAY'         AS PERIOD,
      (GAME_NAME)     AS GAME_NAME,
      GAME_ID,
      SUM(GAME_COUNT) AS GAME_COUNT
    FROM admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))
    GROUP BY
      PARTY_ID, PLATFORM, GAME_NAME, GAME_ID
    ORDER BY SUM(GAME_COUNT) DESC) yesterday

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
      'WTD'         AS PERIOD,
      (GAME_NAME)     AS GAME_NAME,
      GAME_ID,
      SUM(GAME_COUNT) AS GAME_COUNT
    FROM admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
          AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
      PARTY_ID, PLATFORM, GAME_NAME, GAME_ID
    ORDER BY SUM(GAME_COUNT) DESC) wtd

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
      'MTD'         AS PERIOD,
      (GAME_NAME)     AS GAME_NAME,
      GAME_ID,
      SUM(GAME_COUNT) AS GAME_COUNT
    FROM admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
          AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
      PARTY_ID, PLATFORM, GAME_NAME, GAME_ID
    ORDER BY SUM(GAME_COUNT) DESC) mtd

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
      'LTD'         AS PERIOD,
      (GAME_NAME)     AS GAME_NAME,
      GAME_ID,
      SUM(GAME_COUNT) AS GAME_COUNT
    FROM admin_all.DW_GAME_PLAYER_DAILY WITH ( NOLOCK )
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
      PARTY_ID, PLATFORM, GAME_NAME, GAME_ID
    ORDER BY SUM(GAME_COUNT) DESC) ltd

  END

go
