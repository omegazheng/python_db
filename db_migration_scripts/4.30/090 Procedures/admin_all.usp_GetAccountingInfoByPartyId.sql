set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetAccountingInfoByPartyId]
    (@partyid int, @isAgentCashCount int = 0)
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(YEAR, 100, 0);

    SELECT *
    INTO #Temp
    FROM (
            SELECT
                'WITHDRAWAL' AS              TRAN_TYPE,
                CAST(w.DATETIME AS DATE) DATE,
                u.CURRENCY,
                SUM(w.AMOUNT_REAL)           AMOUNT_REAL,
                0                            AMOUNT_PLAYABLE_BONUS,
                SUM(w.AMOUNT_BONUS_WINNINGS + w.AMOUNT_BONUS)  AMOUNT_RELEASED_BONUS
            FROM admin_all.WITHDRAWALS w
                     WITH ( NOLOCK )
                     JOIN admin_all.PAYMENT p
                          ON p.id = w.payment_id
                     JOIN admin_all.ACCOUNT
                          ON w.ACCOUNT_ID = ACCOUNT.id
                     JOIN EXTERNAL_MPT.USER_CONF
                u ON u.PARTYID = ACCOUNT.PARTYID
            WHERE
                u.PARTYID = @partyid AND w.STATUS = 'COMPLETED'
              AND w.DATETIME >= @startDateLocal
              AND w.DATETIME < @endDateLocal
              AND (@isAgentCashCount = 1 OR u.ParentID IS NULL OR p.METHOD!='CASH')
            GROUP BY CAST(w.DATETIME AS DATE), u.CURRENCY

           UNION

           SELECT
             'DEPOSIT'                    TRAN_TYPE,
             CAST(p.REQUEST_DATE AS DATE) DATE,
             u.CURRENCY,
             SUM(p.AMOUNT_REAL)           AMOUNT_REAL,
             0                            AMOUNT_PLAYABLE_BONUS,
             SUM(p.AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
           FROM admin_all.PAYMENT p
             WITH ( NOLOCK )
             JOIN admin_all.ACCOUNT
               ON p.ACCOUNT_ID = ACCOUNT.id
             JOIN EXTERNAL_MPT.USER_CONF u
               ON u.PARTYID = ACCOUNT.PARTYID
           WHERE
             p.TYPE = 'DEPOSIT' AND p.STATUS = 'COMPLETED'
             AND p.REQUEST_DATE >= @startDateLocal
             AND p.REQUEST_DATE < @endDateLocal
             AND u.partyid = @partyid
             AND (@isAgentCashCount = 1 OR u.ParentID IS NULL OR p.METHOD!='CASH')
           GROUP BY CAST(p.REQUEST_DATE AS DATE), u.CURRENCY
         ) ACCOUNTING

    SELECT
      tt.PERIOD                     AS PERIOD,
      tt.CURRENCY                   AS CURRENCY,
      tt.TRAN_TYPE                  AS TRAN_TYPE,
      SUM(tt.AMOUNT_REAL)           AS AMOUNT_REAL,
      SUM(tt.AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
      SUM(tt.AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
    FROM (
           SELECT
             'TODAY'               AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT_REAL           AS AMOUNT_REAL,
             AMOUNT_RELEASED_BONUS AS AMOUNT_RELEASED_BONUS,
             AMOUNT_PLAYABLE_BONUS AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION

           SELECT
             'YESTERDAY'           AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT_REAL           AS AMOUNT_REAL,
             AMOUNT_RELEASED_BONUS AS AMOUNT_RELEASED_BONUS,
             AMOUNT_PLAYABLE_BONUS AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION

           SELECT
             'WTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

           UNION

           SELECT
             'MTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

           UNION

           SELECT
             'LTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

         ) tt
    GROUP BY tt.PERIOD, tt.CURRENCY, TRAN_TYPE

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END
  END

go
