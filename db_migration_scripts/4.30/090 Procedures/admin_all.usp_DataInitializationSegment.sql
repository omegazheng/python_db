set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationSegment
as
begin
	set nocount on
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.Segment', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--admin_all.Segment
	begin transaction
	set identity_insert [admin_all].[segment] on;
	;with s as 
	(
		select [id],[name]
		from (
				values (0,N'Others')
					,(1,N'Casino')
					,(2,N'Live Casino')
					,(3,N'Sports')
					--,(4,N'Bonus')
			) v([id],[name])
	)
	merge admin_all.Segment t
	using s on s.[id]= t.[id]
	when not matched then
		insert ([id],[name])
		values(s.[id],s.[name])
	when matched and (s.[name] is null and t.[name] is not null or s.[name] is not null and t.[name] is null or s.[name] <> t.[name]) then 
		update set  t.[name]= s.[name]
	;
	set identity_insert [admin_all].[segment] off;
	commit;
end
go
