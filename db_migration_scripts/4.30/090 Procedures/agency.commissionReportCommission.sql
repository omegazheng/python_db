set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[commissionReportCommission]
  (@agent_id INT, @start_date DATETIME, @end_date DATETIME, @comm_plan_ids VARCHAR(50))
AS
  BEGIN
    SELECT
      RR.*,
      (SELECT userid
       FROM external_mpt.USER_CONF
       WHERE PARTYID = RR.partyid) AS userid,
      (SELECT user_type
       FROM external_mpt.USER_CONF
       WHERE PARTYID = RR.partyid) AS userType,
      (SELECT name
       FROM admin_all.COMMISSION
       WHERE id = RR.commissionid) AS commissionName
    FROM (
           SELECT
             sum(RESULT.directcommission)  directcommission,
             sum(RESULT.entitycommission)  entitycommission,
             sum(RESULT.networkcommission) networkcommission,
             RESULT.agent_id               partyid,
             RESULT.PARENT_PLAN_ID         commissionid
           FROM (

                  SELECT
                    S_COMM.directcommission,
                    S_COMM.entitycommission,
                    S_COMM.networkcommission,
                    S_COMM.agent_id,
                    [agency].[findPlanWithSameProductSet](@agent_id, S_COMM.plan_id) AS PARENT_PLAN_ID
                  FROM (
                         SELECT
                           SUM(CASE WHEN c.category = 'direct'
                             THEN VALUE
                               ELSE 0 END) AS directcommission,
                           SUM(CASE WHEN c.category = 'network'
                             THEN VALUE
                               ELSE 0 END) AS entitycommission,
                           0               AS networkcommission,
                           cp.plan_id,
                           cp.agent_id
                         FROM
                           agency.commission_payment cp
                           JOIN admin_all.commission c ON c.id = cp.plan_id
                           JOIN external_mpt.user_conf agent ON agent.partyid = cp.agent_id
                         WHERE
                           agent.parentid = @agent_id
                           AND cp.start_date >= @start_date AND cp.end_date <= dateadd(DAY, 1, @end_date)
                           AND cp.state <> 'COMPLETED_REJECTED'
                         GROUP BY
                           cp.agent_id, cp.plan_id
                       ) S_COMM

                  UNION

                  SELECT
                    SUM(N_COMM_GROUP.directcommission),
                    SUM(N_COMM_GROUP.entitycommission),
                    SUM(N_COMM_GROUP.networkcommission),
                    N_COMM_GROUP.parentId AS agent_id,
                    N_COMM_GROUP.PARENT_PLAN_ID
                  FROM (
                         SELECT
                           N_COMM.*,
                           [agency].[findPlanWithSameProductSet](@agent_id, N_COMM.plan_id) AS PARENT_PLAN_ID
                         FROM (
                                SELECT
                                  0          AS directcommission,
                                  0          AS entitycommission,
                                  SUM(VALUE) AS networkcommission,
                                  cp.plan_id,
                                  cp.agent_id,
                                  network.parentId
                                FROM
                                  agency.commission_payment cp
                                  JOIN admin_all.commission c ON c.id = cp.plan_id
                                  JOIN
                                  (
                                    SELECT
                                      directAgent.PARTYID    agentId,
                                      networkAgents.ParentID parentId,
                                      networkAgents.PARTYID  networkAgentPartyId
                                    FROM external_mpt.USER_CONF directAgent
                                      JOIN external_mpt.USER_CONF networkAgents ON networkAgents.Hierarchy.ToString()
                                                                                   LIKE
                                                                                   '%/' +
                                                                                   CONVERT(VARCHAR(30),
                                                                                           directAgent.PARTYID)
                                                                                   +
                                                                                   '/%'
                                                                                   AND networkAgents.user_type <> 0 AND
                                                                                   networkAgents.PARTYID <>
                                                                                   directAgent.PARTYID
                                    WHERE directAgent.ParentID = @agent_id
                                  ) network ON network.networkAgentPartyId = cp.agent_id
                                WHERE
                                  cp.start_date >= @start_date AND cp.end_date <= dateadd(DAY, 1, @end_date)
                                  AND cp.state <> 'COMPLETED_REJECTED'
                                GROUP BY
                                  cp.agent_id, cp.plan_id, network.parentId
                              ) N_COMM
                       ) N_COMM_GROUP
                  GROUP BY N_COMM_GROUP.parentId, N_COMM_GROUP.PARENT_PLAN_ID) RESULT
           WHERE PARENT_PLAN_ID IN (SELECT *
                                    FROM [admin_all].fc_splitDelimiterString(
                                        @comm_plan_ids,
                                        ','))
           GROUP BY RESULT.agent_id, RESULT.PARENT_PLAN_ID
         ) RR
  END

go
