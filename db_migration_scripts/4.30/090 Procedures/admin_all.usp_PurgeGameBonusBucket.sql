set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_PurgeGameBonusBucket] @Retention int = 500, @BatchSize int = 150, @ArchiveDatabase nvarchar(128)= null
as
begin
  set nocount, xact_abort on
  declare @i bigint, @ID bigint, @db nvarchar(128), @SQL nvarchar(max), @Proc sysname, @FullTargetTableName sysname
  if @ArchiveDatabase is not null
    begin
      select @db = db_name(db_id(@ArchiveDatabase))
      if @db is null
        begin
          raiserror('Archive database is not valid %s', 16,1, @ArchiveDatabase)
          return
        end
      if @db is not null
        begin
          select @Proc = quotename(@db) + '..sp_executesql'
          select @SQL = 'select @ret = case when exists(select * from sys.schemas where name = ''admin_all'') then 1 else 0 end'
          exec @Proc @SQL, N'@ret int output', @i output
          if @i = 0
            begin
              select @SQL = 'create schema admin_all'
              begin try
              exec @proc @SQL
              end try
              begin catch
              end catch
            end
          select @FullTargetTableName = quotename(@db)+'.admin_all.game_bonus_bucket'
          if object_id(@FullTargetTableName) is null
            begin
              select @SQL = 'create table ' + @FullTargetTableName +' ([ID] bigint not null,[GAME_TRAN_ID] nvarchar(100),[PLATFORM_ID] int,[BONUS_ID] int,[CREATE_DATE] datetime,[LAST_UPDATE_DATE] datetime, [AMOUNT_REAL] numeric(38,18),[RELEASED_BONUS_WINNINGS] numeric(38,18),[RELEASED_BONUS] numeric(38,18),[PLAYABLE_BONUS_WINNINGS] numeric(38,18),[PLAYABLE_BONUS] numeric(38,18),[MAX_RELEASED_BONUS] numeric(38,18),[MAX_PLAYABLE_BONUS] numeric(38,18),[ACCOUNT_ID] int,[AMOUNT_FREE_BET] numeric(38,18),[GAME_ID] nvarchar(100), constraint PK_admin_all_game_bonus_bucket primary key (ID))'
              exec(@SQL)
            end
          select @SQL = '	insert into ' + @FullTargetTableName + '(ID, GAME_TRAN_ID, PLATFORM_ID, BONUS_ID, CREATE_DATE, LAST_UPDATE_DATE, AMOUNT_REAL, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS, MAX_RELEASED_BONUS, MAX_PLAYABLE_BONUS, ACCOUNT_ID, AMOUNT_FREE_BET, GAME_ID)
		select ID, GAME_TRAN_ID, PLATFORM_ID, BONUS_ID, CREATE_DATE, LAST_UPDATE_DATE, AMOUNT_REAL, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS, MAX_RELEASED_BONUS, MAX_PLAYABLE_BONUS, ACCOUNT_ID, AMOUNT_FREE_BET, GAME_ID
		from (
			delete t
				output deleted.ID, deleted.GAME_TRAN_ID, deleted.PLATFORM_ID, deleted.BONUS_ID, deleted.CREATE_DATE, deleted.LAST_UPDATE_DATE, deleted.AMOUNT_REAL, deleted.RELEASED_BONUS_WINNINGS, deleted.RELEASED_BONUS, deleted.PLAYABLE_BONUS_WINNINGS, deleted.PLAYABLE_BONUS, deleted.MAX_RELEASED_BONUS, deleted.MAX_PLAYABLE_BONUS, deleted.ACCOUNT_ID, deleted.AMOUNT_FREE_BET, deleted.GAME_ID
			from admin_all.game_bonus_bucket t
			where ID = @ID
		) t1
		'
        end
    end

  select @i = 0
  declare c cursor local static for
    select ID from admin_all.game_bonus_bucket where LAST_UPDATE_DATE < dateadd(day, -@Retention, getdate()) --or LAST_UPDATE_DATE < '2016-01-01'
  open c
  fetch next from c into @ID
  while @@fetch_status = 0
    begin
      select @i = @i +1
      if @@trancount = 0
        begin
          begin transaction
        end
      if @db is null
        begin
          delete admin_all.game_bonus_bucket where ID = @ID
        end
      else
        begin
          exec sp_executesql @SQL, N'@ID bigint', @ID = @ID
        end
      if @i % @BatchSize = 0
        begin
          while @@trancount > 0
            commit
        end
      fetch next from c into @ID
    end
  close c
  deallocate c
  while @@trancount > 0
    commit
end

go
