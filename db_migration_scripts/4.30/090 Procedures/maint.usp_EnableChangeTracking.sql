set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_EnableChangeTracking(@ConnectionString varchar(max))
as
begin
	set nocount on
	exec maint.usp_ExecuteSQL @ConnectionString, 'declare @SQL nvarchar(max);select @SQL = ''alter database ''+quotename(db_name())+'' set change_tracking = on'';if not exists(select * from sys.change_tracking_databases where database_id = db_id()) exec(@SQL);'
end

go
