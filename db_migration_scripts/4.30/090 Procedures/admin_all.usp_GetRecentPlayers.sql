set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetRecentPlayers]
    (@staffid int)
AS
BEGIN
    select top 7
        user_conf.partyid,
        user_conf.userid,
        user_conf.first_name + ' ' + user_conf.last_name as name,
        user_conf.city + ', ' + user_conf.country + ' ' as location,
        user_conf.country,
        casino_brand_def.brandname

    from admin_all.recent_users
        join external_mpt.user_conf on user_conf.partyid = recent_users.partyid
            join admin.casino_brand_def on casino_brand_def.brandid = user_conf.brandid
    where recent_users.staffid = @staffid
    order by last_access_time desc

END

go
