set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetActiveAccountsCasinoCountByCountryAndDataRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

    SELECT
      count(*) as counts,
      country as COUNTRY
    FROM
      (
        SELECT
          DISTINCT
          PARTY_ID,
          COUNTRY
        FROM
        [admin_all].[DW_GAME_PLAYER_DAILY] dw
        JOIN admin_all.GAME_INFO g ON g.GAME_ID = dw.GAME_ID
        JOIN admin_all.PLATFORM pl ON g.PLATFORM_ID = pl.ID
      WHERE
        dw.SUMMARY_DATE >= @startDateLocal
        AND dw.SUMMARY_DATE < @endDateLocal
        AND pl.PLATFORM_TYPE = 'CASINO'
  ) ACTIVE_CASINO
    group by country
    order by counts desc
  END

go
