set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getPlayableBonusWithOpeningBalance]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      CURRENCY,
      TRAN_TYPE,
      SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
      SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
      SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
    FROM DW_ACCOUNT_TRAN_DAILY DW
    WHERE DW.SUMMARY_DATE >= @startDateLocal
          AND
          DW.SUMMARY_DATE < @endDateLocal
          AND DW.TRAN_TYPE IN ('BONUS_REL', 'CRE_BONUS', 'EXP_BONUS', 'CANC_BONUS')
    GROUP BY CURRENCY, TRAN_TYPE

    UNION

    SELECT
      CURRENCY,
      'OPENING_BALANCE'      AS TRAN_TYPE,
      SUM(DA.BALANCE_REAL)   AS AMOUNT_REAL,
      SUM(DA.RELEASED_BONUS) AS RELEASED_BONUS,
      SUM(DA.PLAYABLE_BONUS) AS PLAYABLE_BONUS
    FROM ACCOUNT AC
      JOIN external_mpt.USER_CONF US
        ON AC.PARTYID = US.PARTYID
      JOIN DAILY_BALANCE_HISTORY DA
        ON AC.id = DA.ACCOUNT_ID
    WHERE DA.DATETIME = DATEADD(DD, -1, @startDateLocal)
    GROUP BY CURRENCY
END

go
