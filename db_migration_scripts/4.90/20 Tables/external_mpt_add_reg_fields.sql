
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Staff, User, Agent Enum
IF NOT EXISTS(select * from sys.columns where object_id('[external_mpt].[USER_CONF]') = object_id and name = 'CARD_SERIAL')
    begin
        Alter table external_mpt.USER_CONF
            ADD [CARD_SERIAL] nvarchar(50) NULL,
            [STREET_NUMBER] nvarchar(10) NULL,
            [FLOOR_NUMBER] nvarchar(10) NULL,
            [MUNICIPALITY] nvarchar(50) NULL;
    end