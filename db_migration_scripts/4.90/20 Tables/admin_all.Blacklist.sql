set ansi_nulls, quoted_identifier on
go
--drop table admin_all.Blacklist
go
if object_id('admin_all.Blacklist') is null
    begin
        create table admin_all.Blacklist
        (
            BlacklistId int identity (1,1)  not null,
            Type        nvarchar(20)        not null,
            Value       nvarchar(100)       not null,
            BrandId     int                 null,
            Country     char(2)             null,
            Reference   nvarchar(200)       null,
            Source      nvarchar(20)        null,
            Date        datetime            not null DEFAULT GETDATE()

            constraint PK_admin_all_Blacklist primary key(BlacklistId)
        )
    end
go