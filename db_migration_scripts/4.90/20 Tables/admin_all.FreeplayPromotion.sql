SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- FREEPLAY_PLAN
IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[FREEPLAY_PLAN]') = object_id and name = 'BET_PER_LINE')
    begin
        alter table [admin_all].[FREEPLAY_PLAN] add BET_PER_LINE numeric(38,18)
    end

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[FREEPLAY_PLAN]') = object_id and name = 'GAMBLE_ENABLED')
    begin
        alter table [admin_all].[FREEPLAY_PLAN] add GAMBLE_ENABLED bit default 0
    end


-- FREEPLAY_PROMOTION
IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[FREEPLAY_PROMOTION]') = object_id and name = 'BET_PER_LINE')
    begin
        alter table [admin_all].[FREEPLAY_PROMOTION] add BET_PER_LINE numeric(38,18)
    end

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[FREEPLAY_PROMOTION]') = object_id and name = 'GAMBLE_ENABLED')
    begin
        alter table [admin_all].[FREEPLAY_PROMOTION] add GAMBLE_ENABLED bit default 0
    end

-- IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[FREEPLAY_PROMOTION]') = object_id and name = 'DENOMINATION')
--     begin
--         alter table [admin_all].[FREEPLAY_PROMOTION] add DENOMINATION numeric(38,18)
--     end




