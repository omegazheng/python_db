SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Staff, User, Agent Enum
IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'DOCUMENT_TYPE_ID')
    begin
    	Alter table admin_all.USER_DOCUMENT_TYPE
    		add CONSTRAINT PK_USER_DOCUMENT_TYPE primary key (id);
    	alter table admin_all.USER_DOCUMENT_TYPE
            add constraint UNIQUE_DOCUMENT_TYPE unique(type);	
        ALTER TABLE [admin_all].[USER_DOCUMENTS]
            ADD [DOCUMENT_TYPE_ID] [int] NULL
        ALTER TABLE [admin_all].[USER_DOCUMENTS]
        	ADD CONSTRAINT FK_DOCUMENT_TYPE FOREIGN KEY (DOCUMENT_TYPE_ID)
      		REFERENCES [admin_all].[USER_DOCUMENT_TYPE] (ID)    
    end


IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'REVIEW_DATE')
    begin
    	Alter table admin_all.USER_DOCUMENTS
    		ADD [REVIEW_DATE] [datetime] NULL
    end

IF EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'FILE_DESCRIPTION')
    begin
        Alter table admin_all.USER_DOCUMENTS
            alter column [FILE_DESCRIPTION] varchar(150) null
    end
