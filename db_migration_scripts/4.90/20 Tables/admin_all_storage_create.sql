SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[Storage]') = object_id)
    begin
        CREATE TABLE admin_all.Storage (
        StorageId int IDENTITY(1,1) NOT NULL,
        BrandId int NULL,
        Country varchar(2)  NULL,
        StorageTypeKey nvarchar(20)  NOT NULL,
        Url text  NULL,
        Username text  NULL,
        Password text  NULL,
        StoragePath text  NULL,
        CONSTRAINT PK_admin_all_Storage PRIMARY KEY (StorageId),
        CONSTRAINT UQ_admin_all_Storage_BrandId_Country UNIQUE (BrandId,Country)
    )

    end


IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[Storage]') = object_id and name = 'StoragePath')
    begin
        alter table admin_all.Storage ADD [StoragePath] text NULL;
    end

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[USER_DOCUMENTS]') = object_id and name = 'StorageId')
    begin
        Alter table admin_all.USER_DOCUMENTS
            ADD [StorageId] [int] NULL
    end


