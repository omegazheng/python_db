if not exists(select * from sys.indexes where name = 'IDX_admin_all_BONUS_EXPIRY_DATE' and object_id = object_id('admin_all.BONUS'))
	create index IDX_admin_all_BONUS_EXPIRY_DATE on admin_all.BONUS(EXPIRY_DATE) include(Status, PartyID, PLAYABLE_BONUS, PLAYABLE_BONUS_WINNINGS)


--	drop index admin_all.BONUS.IDX_admin_all_BONUS_EXPIRY_DATE