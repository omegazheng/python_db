set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_ExpireBonus
as
begin
	set nocount, xact_abort on	
	declare @BonusID int
	declare cExpireBonus cursor fast_forward for
		select ID
		from admin_all.BONUS b
		where EXPIRY_DATE < getdate()
			and STATUS in ('ACTIVE', 'QUEUED', 'SPENT_ACTIVE', 'OPTED_IN')
			and exists(select * from admin_all.ACCOUNT a where a.PARTYID = b.PARTYID)
	open cExpireBonus
	fetch next from cExpireBonus into @BonusID
	while @@fetch_status = 0
	begin
		exec admin_all.usp_InvalidateBonus @BonusID
		fetch next from cExpireBonus into @BonusID
	end
	close cExpireBonus
	deallocate cExpireBonus
	
end
go
--begin transaction
--exec admin_all.usp_ExpireBonus
--rollback
