set ansi_nulls, quoted_identifier on
go

begin
    insert into admin_all.USER_REGISTRY (PARTYID, MAP_KEY, VALUE)
        select u.PARTYID, 'email.verification.status', 'UNVERIFIED'
        from external_mpt.USER_CONF u
            inner join external_mpt.UserAssociatedAccount ua on u.PARTYID = ua.AssociatedPartyID
        where u.REGISTRATION_STATUS in ('QUICK_OPEN', 'OPEN')
          and (ua.IsPrimary is NULL or ua.IsPrimary = 1)
          and not exists(select * from admin_all.USER_REGISTRY ur where ur.PARTYID = u.PARTYID and ur.MAP_KEY = 'email.verification.status')
end
go

begin
    insert into admin_all.USER_REGISTRY (PARTYID, MAP_KEY, VALUE)
        select u.PARTYID, 'email.verification.status', 'VERIFIED'
        from external_mpt.USER_CONF u
            inner join external_mpt.UserAssociatedAccount ua on u.PARTYID = ua.AssociatedPartyID
        where u.REGISTRATION_STATUS not in ('QUICK_OPEN', 'OPEN')
          and (ua.IsPrimary is NULL or ua.IsPrimary = 1)
          and not exists(select * from admin_all.USER_REGISTRY ur where ur.PARTYID = u.PARTYID and ur.MAP_KEY = 'email.verification.status')
end
go
