if not exists(select * from sys.columns where object_id = object_id('chronos.EventData') and name = 'Info')
begin
	alter table chronos.EventData add Info nvarchar(max)
end
go
