set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_SPORTSBOOK_BET_DETAILS_ForChronos on admin_all.SPORTSBOOK_BET_DETAILS
for insert, update
as 
begin
	if @@rowcount = 0
		return
	if chronos.fn_IsEnabled() = 0
		return
	declare @ID int
	declare c cursor local for
		select i.ID
		from inserted i
			left join deleted d on i.ID = d.ID
		where i.CURRENCY <> isnull(d.CURRENCY, '')
			or i.AMOUNT <> isnull(d.AMOUNT, 0)
			or i.TYPE <> isnull(d.TYPE, '') 
			or i.WAGER_ID <> isnull(d.WAGER_ID, '')
			or i.PLATFORM_CODE <> isnull(d.PLATFORM_CODE, '')
			or isnull(i.STATUS, '') <> isnull(d.STATUS, '')
			or isnull(i.POTENTIAL_WIN, 0) <> isnull(d.POTENTIAL_WIN, '')
			or isnull(i.TOTAL_ODDS, 0) <> isnull(d.TOTAL_ODDS, 0)
			or isnull(i.MSG, '') <> isnull(d.MSG, '')
	open c
	fetch next from c into @ID
	while @@fetch_status = 0
	begin
		exec chronos.usp_AddSportsBookMetadataEvent @ID
		fetch next from c into @ID
	end
	close c
	deallocate c
end