set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_USER_LOGIN_LOG_ForChronos on admin_all.USER_LOGIN_LOG
for insert
as 
begin
	if @@rowcount = 0
		return
	if chronos.fn_IsEnabled() = 0
		return
	declare @PartyID int, @SessionKey nvarchar(100), @LoginType varchar(20), @LoginTime datetime, @Count int, @Date datetime = getdate(), 
			@EventID bigint, @IP varchar(100), @Country char(2), @Attempts int
	declare c cursor local fast_forward for
		select partyid, SESSION_KEY, LOGIN_TYPE, login_time, ip, country, isnull(login_attempts, 0) from inserted i
	open c
	fetch next from c into @PartyID, @SessionKey, @LoginType, @LoginTime, @IP, @Country, @Attempts
	while @@fetch_status = 0
	begin
		select @Count = (
							select count(*) 
							from (
									select top 2 SESSION_KEY
									from admin_all.USER_WEB_SESSION 
									where PARTYID = @PartyID 
									and SessionType = 'ONLINE'
								) a 
							),
				@EventID = null

		if @LoginType = 'LOGIN' and @Count in(0, 1) and @Attempts = 0
		begin
			insert into chronos.Event(Name, PartyID, Date)
				values('Login', @PartyID, @Date)
			select @EventID = scope_identity()
			insert into chronos.EventData(EventID, Name, Value1, Value2, Date) 
					values(@EventID, 'LoginTime', @Date, null, @Date)
			
			insert into chronos.EventData(EventID, Name, Value1, Value2, Date) 
					select @EventID, 'DaysSinceLastLogin', datediff(day, Date, @Date), null, @Date
					from chronos.UserSessionStaging
					where PartyID = @PartyID
						and Name = 'LogoutDate'
						and datediff(day, Date, @Date) > 0

			insert into chronos.EventData(EventID, Name, Value1, Value2, Date) 
					select @EventID, 'LastLoginDate', Date, null, @Date
					from chronos.UserSessionStaging
					where PartyID = @PartyID
						and Name = 'LoginDateTimeForSessionDuration'
						and datediff(day, Date, @Date) > 0
			
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID,  'LoginDateTimeForSessionDuration', 1, @Date
		end
		else if @LoginType = 'LOGIN' and @Attempts > 0
		begin
			insert into chronos.Event(Name, PartyID, Date)
				values('LoginFailed', @PartyID, @Date)
			select @EventID = scope_identity()
		end
		else if @LoginType in('LOGOUT', 'TIMEOUT')
		begin
			if @Count = 0
			begin

				insert into chronos.Event(Name, PartyID, Date)
					values(case when @LoginType = 'LOGOUT' then 'Logout' else 'Timeout' end, @PartyID, @Date)
				select @EventID = scope_identity()

				insert into chronos.EventData(EventID, Name, Value1, Date)
					select @EventID, 'SessionDurationInSec', datediff(second, Date, @Date), @Date
					from chronos.UserSessionStaging
					where PartyID = @PartyID
						and Name = 'LoginDateTimeForSessionDuration'
						and datediff(second, Date, @Date) > 0

				insert into chronos.EventData(EventID, Name, Value1, Value2, Date)
					select @EventID, x.Name, x.Value, null, x.Date
					from (
							delete s
								output deleted.Name, deleted.Value, deleted.Date
							from chronos.UserSessionStaging s
							where s.PartyID = @PartyID
								and Name not in ('LoginDateTimeForSessionDuration')
						) x

				insert into chronos.EventData(EventID, Name, Value1, Value2, Date) 
					values(@EventID, 'LogoutTime', @Date, null, @Date)
				exec chronos.usp_UpdateUserSessionStaging 1, @PartyID,  'LogoutDate', 1, @Date
			end
		end
		if @EventID is not null
		begin
			if @IP is not null insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'IP', @IP, null, @Date)
			if @SessionKey is not null insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'SessionKey', @SessionKey, null, @Date)
			if @Attempts > 0 insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'LoginAttempts', @Attempts, null, @Date)
			if @Country is not null insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'Country', @Country, null, @Date)
		end
		fetch next from c into @PartyID, @SessionKey, @LoginType, @LoginTime, @IP, @Country, @Attempts
	end	
	close c
	deallocate c
end
go

--select * from admin_all.USER_LOGIN_LOG

--select * from chronos.UserSessionStaging where name = 'LoginDateTimeForSessionDuration'

--select top 100 * from chronos.EventData order by 1 desc

--select * from chronos.Event where EventID = 2309

--select * from  admin_all.USER_LOGIN_LOG where partyid = 100122409 order by 1 desc


--delete chronos.EventData  where name = 'logintime'


