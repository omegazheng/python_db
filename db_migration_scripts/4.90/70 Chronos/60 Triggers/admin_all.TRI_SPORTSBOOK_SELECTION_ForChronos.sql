set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_SPORTSBOOK_SELECTION_ForChronos on admin_all.SPORTSBOOK_SELECTION
for insert, update
as 
begin
	if @@rowcount = 0
		return
	if chronos.fn_IsEnabled() = 0
		return
	declare @ID int
	declare c cursor local for
		select distinct i.BET_DETAILS_ID
		from inserted i
			left join deleted d on i.ID = d.ID
		where isnull(i.MARKET, '')<> isnull(d.MARKET, '')
			or isnull(i.SELECTION, '')<> isnull(d.SELECTION, '')
			or isnull(i.ODDS, 0)<> isnull(d.ODDS, 0)
			or isnull(i.PERIOD, '')<> isnull(d.PERIOD, '')
			or isnull(i.EVENT, '')<> isnull(d.EVENT, '')
			or isnull(i.EVENT_PATHS, '')<> isnull(d.EVENT_PATHS, '')
			or isnull(i.OUTCOME_ID, '')<> isnull(d.OUTCOME_ID, '')
			or isnull(i.OUTCOME_DESCRIPTION, '')<> isnull(d.OUTCOME_DESCRIPTION, '')
			or isnull(i.MATCH, '')<> isnull(d.MATCH, '')
			or isnull(i.MATCH_INFO, '')<> isnull(d.MATCH_INFO, '')
			or isnull(i.SPORT_NAME, '')<> isnull(d.SPORT_NAME, '')
	open c
	fetch next from c into @ID
	while @@fetch_status = 0
	begin
		exec chronos.usp_AddSportsBookMetadataEvent @ID
		fetch next from c into @ID
	end
	close c
	deallocate c
end