set ansi_nulls, quoted_identifier on
go

create or alter function [admin_all].[fc_splitDelimiterStringToJackpotFields](
    @StringWithDelimiter VARCHAR(8000),
    @Delimiter VARCHAR(8))

    RETURNS @ItemJackpotTable TABLE(Name VARCHAR(100), Amount numeric(38,18), Currency varchar(10),  GameIDList VARCHAR(2000))

AS
BEGIN
    declare @ErrorString varchar(1000);
    declare @MaxLen int;
    set @MaxLen = LEN(@StringWithDelimiter)

    DECLARE @StartingPosition int;
    DECLARE @Item varchar(100);
    DECLARE @Name VARCHAR(100), @AmountStr VARCHAR(100), @Currency varchar(10), @GameIDList varchar(2000), @Amount numeric(38,18);

    IF LEN(@StringWithDelimiter) = 0 OR @StringWithDelimiter IS NULL
        RETURN;

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    if @StartingPosition = 1
        begin
            set @ErrorString = cast('Error:: @StringWithDelimiter should not begin with the delimiter' as int);
            return;
        end

    SET @Name = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @Name = ltrim(rtrim(@Name))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @AmountStr = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @AmountStr = ltrim(rtrim(@AmountStr))
    SET @Amount = CAST(@AmountStr as numeric(38,18))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @Currency = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @Currency = ltrim(rtrim(@Currency))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @GameIDList = ltrim(rtrim(@StringWithDelimiter))

    insert into @ItemJackpotTable(Name, Amount, Currency, GameIDList)
    values(@Name, @Amount, @Currency, @GameIDList)

    RETURN
END
go

-- select * into #TestJackpot
-- from admin_all.fc_splitDelimiterStringToJackpotFields('Testjackpot,12345.567,EUR,39206', ',')
-- select * from #TestJackpot
