IF object_id('[admin_all].[V_LatestConsent]') is null
exec('
	create view admin_all.V_LatestConsent as
	select c.*, lcv.version, cv.ConsentVersionId 
	from admin_all.Consent c 
	inner join 
	admin_all.ConsentVersion cv 
	on cv.ConsentId = c.ConsentId
	inner join 
	(select  ConsentId, max(Version ) version
	from admin_all.ConsentVersion 
	where Status = ''APPROVED''
		AND (ExpiryDate  IS NULL OR ExpiryDate > CURRENT_TIMESTAMP) 
		AND (StartDate  IS NULL OR StartDate  <  CURRENT_TIMESTAMP )
	group by ConsentId) lcv
	on cv.ConsentId  = lcv.ConsentId and cv.Version  = lcv.version;
');