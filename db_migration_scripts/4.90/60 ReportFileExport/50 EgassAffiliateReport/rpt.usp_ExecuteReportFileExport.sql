set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_ExecuteReportFileExport
(
	@ParametersInText nvarchar(max)
)
as
begin
	set nocount on 
	declare @Parameters xml, @ReportFileExportID uniqueidentifier, @Type nvarchar(128), @DateFrom datetime, @DateTo datetime, @Offset char(6), @BrandIDs varchar(max), @CodeKey varchar(20)
	select @Parameters = cast(@ParametersInText as xml)
	select	@ReportFileExportID = c.value('@ReportFileExportID', 'uniqueidentifier'), @Type = c.value('@Type', 'varchar(128)'), @DateFrom = c.value('@DateFrom', 'datetime'), 
			@DateTo = c.value('@DateTo', 'datetime'), @Offset = c.value('@Offset', 'char(6)'), @BrandIDs = c.value('@BrandIDs', 'varchar(max)'), @CodeKey = c.value('@CodeKey', 'varchar(20)')
	from @Parameters.nodes('/Parameters') n(c)
	if @ReportFileExportID is null
		return
	update rpt.ReportFileExport set ExecutionDate = getdate() where ReportFileExportID = @ReportFileExportID
	exec rpt.usp_SetReportFileExportID @ReportFileExportID
	if @Type = 'Egass Registration Report' 
	begin
		exec rpt.usp_DailyAffiliateRegistrationReport @DateFrom = @DateFrom, @DateTo = @DateTo, @Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @OutputTarget= 'File'
	end 
	else if @Type = 'Deposit Report'
	begin
		exec rpt.usp_AffiliateDepositTransactionDetail @DateFrom = @DateFrom, @DateTo = @DateTo, @Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @OutputTarget= 'File'
	end
	else if @Type = 'Net Revenue Report'
	begin
		exec rpt.usp_AffiliateNetRevenue @DateFrom = @DateFrom, @DateTo = @DateTo, @Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @OutputTarget= 'File'
	end
	else if @Type in('Map Sales Report', 'Map Registration Report' )
	begin
		exec rpt.usp_ExecuteMapAffiliateReport @ParametersInText
	end
	update rpt.ReportFileExport set CompletionDate = getdate() where ReportFileExportID = @ReportFileExportID
	exec rpt.usp_SetReportFileExportID

end
go
--declare @x nvarchar(max) = '<Parameters Type="Egass Registration Report" DateFrom="2001-01-01T00:00:00" DateTo="2020-01-01T00:00:00" Offset="+00:00" BrandIDs="1,2,3" CodeKey="btag" />'
--exec rpt.usp_ExecuteReportFileExport @x