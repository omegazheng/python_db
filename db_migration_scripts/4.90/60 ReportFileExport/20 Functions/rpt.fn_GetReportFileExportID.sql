set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetReportFileExportID()
returns uniqueidentifier
as
begin
	return cast(session_context(N'@ReportFileExportID') as uniqueidentifier)
end
go

--exec rpt.usp_SetReportFileExportID 'AE29A341-3B9A-4E1C-A485-549E3E783538'