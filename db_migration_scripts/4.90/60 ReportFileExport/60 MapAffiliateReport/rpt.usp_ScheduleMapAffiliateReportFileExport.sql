set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_ScheduleMapAffiliateReportFileExport
as
begin
	set nocount on
	declare @DateFrom datetime, @DateTo Datetime, @Offset char(6) = null, @BrandID int, @CodeKey varchar(20) = 'btag'

	if datepart(hour,Getdate()) < 3
		return

	--if cast(isnull(admin_all.fn_GetRegistry('affiliate.Map.Schedule.isEnabled'), '') as bit) <> 1
	--	return

	select @DateFrom = dateadd(day, -1, cast(Getdate() as date))
	select @DateTo = Dateadd(day, 1, @DateFrom)

	declare c cursor local for
		select BRANDID from admin.CASINO_BRAND_DEF
	open c
	fetch next from c into @BrandID
	while @@fetch_status = 0
	begin
		if isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.Schedule.isEnabled') as bit), 0) = 1
		begin
			exec rpt.usp_GetMapAffiliateReport @ReportFileExportID = null, @Type = 'Map Registration Report', @DateFrom = @DateFrom, @DateTo = @DateTo,	 @BrandID = @BrandID, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
			exec rpt.usp_GetMapAffiliateReport @ReportFileExportID = null, @Type = 'Map Sales Report', @DateFrom = @DateFrom, @DateTo = @DateTo, @BrandID = @BrandID, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
		end
		fetch next from c into @BrandID
	end 
	close c
	deallocate c

end
go



