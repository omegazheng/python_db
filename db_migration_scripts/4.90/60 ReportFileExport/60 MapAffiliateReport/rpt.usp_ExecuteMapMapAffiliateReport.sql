set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_ExecuteMapAffiliateReport
(
	@ParametersInText nvarchar(max) = null,
	@Type nvarchar(128) = null, 
	@DateFrom datetime = null, 
	@DateTo datetime = null, 
	@BrandID int = null, 
	@CodeKey varchar(20) = null
)
as
begin
	set nocount on 
	declare @Parameters xml, @ReportFileExportID uniqueidentifier, @ParameterHash uniqueidentifier
	if @ParametersInText is not null
	begin
		select @Parameters = cast(@ParametersInText as xml)
		select	@ReportFileExportID = c.value('@ReportFileExportID', 'uniqueidentifier'), @Type = c.value('@Type', 'varchar(128)'), @DateFrom = c.value('@DateFrom', 'datetime'), 
				@DateTo = c.value('@DateTo', 'datetime'), @BrandID = c.value('@BrandID', 'int'), @CodeKey = c.value('@CodeKey', 'varchar(20)')
		from @Parameters.nodes('/Parameters') n(c)
		
		update rpt.ReportFileExport set ExecutionDate = getdate() where ReportFileExportID = @ReportFileExportID
		exec rpt.usp_SetReportFileExportID @ReportFileExportID
		if @Type = 'Map Registration Report' 
		begin
			exec rpt.usp_MapAffiliateRegistrationReport @DateFrom = @DateFrom, @DateTo = @DateTo, @BrandID = @BrandID, @CodeKey = @CodeKey, @OutputTarget= 'File'
		end 
		else if @Type = 'Map Sales Report'
		begin
			exec rpt.usp_MapAffiliateSalesReport @DateFrom = @DateFrom, @DateTo = @DateTo, @BrandID = @BrandID, @CodeKey = @CodeKey, @OutputTarget= 'File'
		end
		update rpt.ReportFileExport set CompletionDate = getdate() where ReportFileExportID = @ReportFileExportID
		exec rpt.usp_UploadMapAffiliateReport @ReportFileExportID
	end
	else
	begin
		declare c cursor local static for
			select BRANDID 
			from admin.CASINO_BRAND_DEF 
			where BRANDID = isnull(@BrandID, BRANDID)
		open c
		fetch next from c into @BrandID
		while @@fetch_status = 0
		begin
			select @ReportFileExportID = null
			if isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.Schedule.isEnabled') as bit), 0) = 1
			begin
				select @Parameters = (select  @Type as [@Type], @DateFrom as [@DateFrom], @DateTo as [@DateTo], @BrandID as [@BrandID], @CodeKey as [@CodeKey] for xml path('Parameters'), type)
				select @ParameterHash = hashbytes('md5', cast(@Parameters as varbinary(max)))
				select @ReportFileExportID = ReportFileExportID from rpt.ReportFileExport where ParameterHash = @ParameterHash
				if @ReportFileExportID is null
				begin
					select @ReportFileExportID = newid()
					insert into rpt.ReportFileExport(ReportFileExportID, ParameterHash, Parameters)
						values(@ReportFileExportID, @ParameterHash, @Parameters)
				end
				update rpt.ReportFileExport set ExecutionDate = getdate() where ReportFileExportID = @ReportFileExportID
				exec rpt.usp_SetReportFileExportID @ReportFileExportID
				if @Type = 'Map Registration Report' 
				begin
					print @ReportFileExportID
					exec rpt.usp_MapAffiliateRegistrationReport @DateFrom = @DateFrom, @DateTo = @DateTo, @BrandID = @BrandID, @CodeKey = @CodeKey, @OutputTarget= 'File'
				end 
				else if @Type = 'Map Sales Report'
				begin
					exec rpt.usp_MapAffiliateSalesReport @DateFrom = @DateFrom, @DateTo = @DateTo, @BrandID = @BrandID, @CodeKey = @CodeKey, @OutputTarget= 'File'
				end
				update rpt.ReportFileExport set CompletionDate = getdate() where ReportFileExportID = @ReportFileExportID
				exec rpt.usp_UploadMapAffiliateReport @ReportFileExportID
			end
			fetch next from c into @BrandID
		end 
		close c
		deallocate c
	end
	exec rpt.usp_SetReportFileExportID
end
go
--declare @x nvarchar(max) = '<Parameters Type="Egass Registration Report" DateFrom="2001-01-01T00:00:00" DateTo="2020-01-01T00:00:00" Offset="+00:00" BrandID="1,2,3" CodeKey="btag" />'
--exec rpt.usp_ExecuteReportFileExport @x
--set xact_abort on
--begin transaction
--exec rpt.usp_ExecuteMapAffiliateReport @Type= 'Map Registration Report', @DateFrom = '2020-03-12', @DateTo = '2020-03-13', @BrandID = 1, @CodeKey = 'btag'
--rollback
--begin transaction
--select * from admin_all.BatchJob where Type = -3
--select *from admin_all.BatchJobTask where BatchJobID = 605
--exec rpt.usp_ExecuteMapAffiliateReport @Type= 'Map Registration Report', @DateFrom = '2020-03-12', @DateTo = '2020-03-13', @BrandID = null, @CodeKey = 'btag'
--select * from admin_all.BatchJob where Type = -3
--select *from admin_all.BatchJobTask where BatchJobID = 605
--rollback