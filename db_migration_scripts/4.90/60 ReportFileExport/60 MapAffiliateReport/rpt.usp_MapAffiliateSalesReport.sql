set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_MapAffiliateSalesReport
(
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@BrandID int = null,
	@CodeKey varchar(20) = 'btag',
	@OutputTarget varchar(20) = 'Table' -- File
)
as
begin

	set nocount on
    

	declare @AffiliateMapJackpotContributionIsEnabled bit = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.jackpotContribution.isEnabled') as bit), 0), --If true, allow jackpot contribution to be included as fee adjustment. If false, fee adjustment will be zero.
			@AffiliateMapManualBonusAdjustmentPercentage numeric(38, 18) = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.manualBonusAdjustmentPercentage') as numeric(38, 18)), 0), --the contribution percentage to calculate the manual bonus adjustment
			@AffiliateMapBonusCalculationIsEnabled bit = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.bonusCalculation.isEnabled') as bit), 0), --if true, allow released bonus to be included in net revenue calculation
			@AffiliateConvertToGBP bit = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.isConvertToGBP') as bit), 0)
			
	

	create table #BrandID(BrandID int, BrandName nvarchar(1000), primary key (BrandID))
	insert into #BrandID(BrandID, BrandName)
		select BRANDID, BRANDNAME
		from admin.CASINO_BRAND_DEF
		where BRANDID = @BrandID

	--select * from admin_all.GAME_INFO
	--select * from admin_all.PLATFORM
	--select * from omegasys_dev.rpt.TranType order by TranType
	create table #Ratio(BrandID int, ProductID int, GameID varchar(100), Ratio numeric(38, 18), primary key (BrandID, ProductID, GameID))
	insert into #Ratio(BrandID, ProductID, GameID, Ratio)
		select b.BrandID,gi.PLATFORM_ID ProductID, gi.GAME_ID GameID, Ratio/100.0
		from admin_all.GAME_INFO gi
			cross join #BrandID b  
			cross apply (
							select top 1 RATIO
							from admin_all.BRAND_GAME_JP_RATIO jp
							where jp.BRANDID = b.BrandID
								and gi.ID = jp.GAME_INFO_ID
							order by last_update desc
						) r 
		
	;with x0 as
	(
		select 
				h.BrandID, b.BrandName,h.PartyID, cast(h.Datetime as date) Date, h.ProductID, h.TranType, h.GameID,
				h.AmountReal, h.AmountReleasedBonus, h.AmountPlayableBonus, utc.VALUE AffiliateIdentifier
		from admin_all.AccountTranHourlyAggregate h 
			inner join #BrandID b on b.BrandID = h.BrandID
			inner join admin_all.USER_TRACKING_CODE utc on utc.CODE_KEY = @CodeKey and utc.PARTYID = h.PartyID
		where h.AggregateType = 0 
			and h.Datetime >= @DateFrom and h.Datetime < @DateTo
			and h.TranType in ('GAME_BET', 'GAME_WIN', 'STAKE_DEC', 'REFUND', 'CASH_OUT', 'BONUS_REL', 'CHARGE_BCK', 'MAN_BONUS')
	),
	x1 as
	(
		select x0.BrandID, x0.BrandName,x0.PartyID, x0.Date, x0.ProductID, x0.GameID,x0.TranType, sum(x0.AmountReal) AmountReal, sum(x0.AmountReleasedBonus) AmountReleasedBonus, sum(x0.AmountPlayableBonus) AmountPlayableBonus, x0.AffiliateIdentifier
		from x0
		group by x0.BrandID, x0.PartyID, x0.Date, x0.ProductID, x0.GameID,x0.TranType, x0.BrandName, x0.AffiliateIdentifier
	),
	x2 as
	(
		select	pc.PartyID, x1.Date, x1.BrandID, x1.BrandName, rate.Rate, x1.AffiliateIdentifier, 
 				sum(case when x1.TranType in ('GAME_BET') then -(x1.AmountReal + x1.AmountReleasedBonus) end) * rate.Rate Bets,
				sum(case when x1.TranType in ('GAME_WIN') then (x1.AmountReal + x1.AmountReleasedBonus) end) * rate.Rate Wins,
				sum(
					case 
						when isnull(p.PLATFORM_TYPE, '') = 'CASINO' and x1.TranType in ('GAME_BET', 'GAME_WIN') then -(x1.AmountReal + x1.AmountReleasedBonus)
						when isnull(p.PLATFORM_TYPE, '') = 'SPORTSBOOK' and x1.TranType in ('GAME_BET', 'GAME_WIN',  'STAKE_DEC', 'REFUND', 'CASH_OUT') then -(x1.AmountReal + x1.AmountReleasedBonus)
						else 0
					end 
				) * rate.Rate GrossRevenue,
						--
				sum(
					case 
						when @AffiliateMapBonusCalculationIsEnabled = 1 and x1.TranType in ('BONUS_REL') then x1.AmountReleasedBonus
						else 0
					end 
				) * rate.Rate Bonus,
				sum(
					case
						when @AffiliateMapJackpotContributionIsEnabled = 1 and x1.TranType in ('GAME_BET') and r.Ratio is not null then 
								- (x1.AmountReal + AmountReleasedBonus + AmountPlayableBonus) * r.Ratio
						else 0
					end 
					+ case
						when x1.TranType in ('MAN_BONUS') then @AffiliateMapManualBonusAdjustmentPercentage * (x1.AmountReal + AmountReleasedBonus + AmountPlayableBonus) / 100
						else 0
					end 
					- case
						when x1.TranType in ('CHARGE_BCK') then - (x1.AmountReal + AmountReleasedBonus + AmountPlayableBonus)
						else 0
					end 
				) * rate.Rate Adjustment,
				sum(
						case
							when x1.TranType in ('CHARGE_BCK') then - (x1.AmountReal + AmountReleasedBonus + AmountPlayableBonus)
							else 0
						end 
					) * rate.Rate ChargeBack

		from x1
			inner join external_mpt.v_UserPrimaryCurrency pc with(noexpand) on pc.AssociatedPartyID = x1.PartyID
			left join admin_all.PLATFORM p on p.ID = x1.ProductID
			left join #Ratio r on r.ProductID = x1.ProductID and r.GameID = x1.GameID and r.BrandID = x1.BrandID
			cross apply admin_all.fn_GetCurrencyConversionRate1(x1.Date, pc.AssociatedCurrency, case when @AffiliateConvertToGBP = 1 then 'GBP' else pc.AssociatedCurrency end) rate
		where (
					p.ID is not null and  p.PLATFORM_TYPE in ('CASINO', 'SPORTSBOOK') 
					or p.ID is null
				)
		group by pc.PartyID, x1.Date, x1.BrandID, x1.BrandName, rate.Rate, x1.AffiliateIdentifier
	)
	select PartyID, x2.BrandName, x2.GrossRevenue - x2.Bonus - x2.Adjustment NetRevenue, Bets, Wins, Date, ChargeBack, Bonus, Rate, AffiliateIdentifier
		into #Revenue
	from x2
	where GrossRevenue <> 0 or  x2.Bonus <> 0 or  x2.Adjustment <> 0

	 ;with x0 as
	(
		select pc.PartyID, cbd.BrandName,
			cast(p.PROCESS_DATE as date) PROCESS_DATE,
			pc.AssociatedCurrency,
			p.AMOUNT,
			(select min(cast(p1.PROCESS_DATE as date)) from admin_all.PAYMENT p1 where p1.ACCOUNT_ID = pc.AssociatedAccountID and p1.STATUS in( 'COMPLETED', 'DP_ROLLBACK') and p1.TYPE = 'DEPOSIT') FirstDepositDate,
			utc.VALUE AffiliateIdentifier
		from admin_all.USER_TRACKING_CODE utc
			inner join external_mpt.USER_CONF uc on uc.PARTYID = utc.PARTYID
			inner join #BrandID cbd on cbd.BrandID = uc.BRANDID
			inner join external_mpt.v_UserPrimaryCurrency pc with(noexpand) on pc.PartyID = uc.PARTYID
			inner join admin_all.PAYMENT p on p.ACCOUNT_ID = pc.AssociatedAccountID
		where CODE_KEY = @CodeKey and p.PROCESS_DATE >= @DateFrom and p.PROCESS_DATE < @DateTo
			and uc.REGISTRATION_STATUS = 'PLAYER'
			and p.STATUS in( 'COMPLETED', 'DP_ROLLBACK')
			and p.TYPE = 'DEPOSIT'
	)
	select  PartyID, PROCESS_DATE, BrandName,rate.Rate, AffiliateIdentifier,
			min(FirstDepositDate) FirstDepositDate,sum(AMOUNT)  * rate.Rate AMOUNT
			into #Payment
	from x0
		cross apply admin_all.fn_GetCurrencyConversionRate1(x0.PROCESS_DATE, x0.AssociatedCurrency,case when @AffiliateConvertToGBP = 1 then 'GBP' else x0.AssociatedCurrency end) rate
	group by PartyID, PROCESS_DATE, BrandName, rate.Rate, AffiliateIdentifier

	select 
			isnull(r.PartyID, p.PartyID) as PartyID,
			isnull(r.BrandName, p.BrandName) as BrandName,
			isnull(r.Date, p.PROCESS_DATE) as TransactionDate,
			nullif(p.AMOUNT, 0) as Deposits,
			nullif(r.Bets, 0) Bets,
			nullif(r.Wins, 0) Wins,
			nullif(r.ChargeBack, 0) ChargeBack,
			nullif(r.Bonus, 0) ReleasedBonuses,
			nullif(r.NetRevenue, 0) NetRevenue,
			case when @AffiliateConvertToGBP = 1 then isnull(r.Rate, p.Rate) else null end  CurrencyRateToGBP,
			isnull(r.AffiliateIdentifier, p.AffiliateIdentifier) as AffiliateIdentifier,
			p.FirstDepositDate
			into #temp
	from #Revenue r
		full outer join #Payment p on r.PartyID = p.PartyID and r.Date = p.PROCESS_DATE
	
	--select @OutputColumns = stuff((select ','+name as column_name from tempdb.sys.columns where object_id = object_id('tempdb..#temp') order by column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '');
	
	if @OutputTarget = 'File'
	begin
		declare @Query nvarchar(max) = 'select PartyID, BrandName, convert(varchar(100), TransactionDate, 120) TransactionDate, Deposits, Bets, Wins, ChargeBack, ReleasedBonuses, NetRevenue, CurrencyRateToGBP, AffiliateIdentifier, convert(varchar(100), FirstDepositDate, 120) FirstDepositDate
										from #temp'
		exec rpt.usp_GenerateReportFileExportContent @Query = @Query, 
				@Mapping = '<Mappings>
								<Mapping SourceColumn="PartyID" ReportColumn="PartyID"/>
								<Mapping SourceColumn="BrandName"/>
								<Mapping SourceColumn="TransactionDate"/>
								<Mapping SourceColumn="Deposits"/>
								<Mapping SourceColumn="Bets"/>
								<Mapping SourceColumn="Wins"/>
								<Mapping SourceColumn="ChargeBack"/>
								<Mapping SourceColumn="ReleasedBonuses"/>
								<Mapping SourceColumn="NetRevenue"/>
								<Mapping SourceColumn="CurrencyRateToGBP"/>
								<Mapping SourceColumn="AffiliateIdentifier"/>
								<Mapping SourceColumn="FirstDepositDate"/>
							</Mappings>',
				@Test = 0
		return
	end
    select PartyID, BrandName, TransactionDate, Deposits, Bets, Wins, ChargeBack, ReleasedBonuses, NetRevenue, CurrencyRateToGBP, AffiliateIdentifier, FirstDepositDate
	from #temp
	

end
go
--exec rpt.usp_MapAffiliateSalesReport @DateFrom = '2018-01-30', @DateTo = '2020-12-12', @OutputTarget = 'table'