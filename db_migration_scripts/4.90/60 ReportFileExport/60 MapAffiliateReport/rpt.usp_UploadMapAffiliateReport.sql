set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_UploadMapAffiliateReport
(
	@ReportFileExportID uniqueidentifier = 'CA01C842-8132-4414-9FC1-1EE285A299D3'
)
as
begin
	set nocount on
	declare @FileName nvarchar(128), @TextContent nvarchar(max), @BrandID int, @ReportType varchar(128)
	
	select	@BrandID = c.value('@BrandID', 'int'),
			@FileName = '_' + replace(convert(varchar(100), c.value('@DateFrom', 'date'), 120), '-', '') + '.csv',
			@ReportType = c.value('@Type', 'varchar(128)')
	from rpt.ReportFileExport r
		cross apply Parameters.nodes('/Parameters') n(c)
	where ReportFileExportID = @ReportFileExportID

	select 
			@FileName =  case 
							when  @ReportType = 'Map Registration Report' then BRANDNAME +'_reg'+ @FileName
							else BRANDNAME +'_sales'+ @FileName
						end
	from admin.CASINO_BRAND_DEF
	where BRANDID = @BrandID
		
	declare @Type nvarchar(20) = admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.FTP.Type'), 
			@Address nvarchar(128) = admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.FTP.Address'), 
			@Port int = cast(admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.FTP.Port') as int), 
			@UserName nvarchar(128) = admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.FTP.UserName'), 
			@Password nvarchar(128) = admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.FTP.Password'), 
			@RegFilePath nvarchar(1000) = admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.FTP.RegFilePath'),
			@SalesFilePath nvarchar(1000) = admin_all.fn_GetBrandRegistry(@BrandID, 'affiliate.Map.FTP.SalesFilePath')



	select @TextContent = decompress(Content) from rpt.ReportFileExportContent where ReportFileExportID = @ReportFileExportID

	if @ReportType = 'Map Registration Report'
		select @TextContent = admin_all.fn_BatchJobComposeFTPTaskContent(@Type, @Address, @Port, @UserName, @Password, @RegFilePath, @FileName, @TextContent, null)
	else
		select @TextContent = admin_all.fn_BatchJobComposeFTPTaskContent(@Type, @Address, @Port, @UserName, @Password, @SalesFilePath, @FileName, @TextContent, null)

	declare @BatchJobID int
	select @BatchJobID = BatchJobID from admin_all.BatchJob where Type = -3
	if @@rowcount  = 0
	begin
		exec admin_all.usp_CreateBatchJob @Name = 'FTP Upload', @Type = -3, @BatchJobID = @BatchJobID output
	end
	exec admin_all.usp_AddBatchJobTask @BatchJobID, @TextContent

	update t
		set t.Status = 'PENDING'
	from admin_all.BatchJobTask t
	where BatchJobID = @BatchJobID
		and ContentHash = cast(hashbytes('MD5', cast(@TextContent as varbinary(max))) as uniqueidentifier)
		and t.Status <> 'PENDING'

	if not exists(select * from admin_all.BatchJob where Status = 'RUNNING' and BatchJobID = @BatchJobID)
	begin
		exec admin_all.usp_UpdateBatchJob @BatchJobID = @BatchJobID, @BatchJobStatus = 'RUNNING'
	end
end
go
--exec rpt.usp_UploadMapAffiliateReport
--delete b
--from admin_all.BatchJob b
--	inner join admin_all.BatchJobTask t on b.batchJobID = t.BatchJobID
--where b.Type = -3


