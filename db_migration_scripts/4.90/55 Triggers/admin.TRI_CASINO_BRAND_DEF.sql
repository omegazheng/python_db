set ansi_nulls, quoted_identifier on
go
create or alter trigger [admin].[TRI_CASINO_BRAND_DEF] on [admin].[CASINO_BRAND_DEF]
for insert, update
as
begin
  if @@rowcount = 0
    return
  set nocount on
  insert into admin_all.STAFF_BRAND_TBL(BRANDID, STAFFID)
    select i.BRANDID, b.STAFFID
    from inserted i
      cross join admin_all.staff_auth_tbl b
    where b.SUPER_ADMIN = 1
          and not exists(select *
                         from admin_all.STAFF_BRAND_TBL sb
                         where sb.BRANDID = i.BRANDID
                               and sb.STAFFID = b.STAFFID
    )
	if not exists(select * from deleted)
	begin
		insert into admin_all.BRAND_COUNTRY(BRANDID, COUNTRY, MIN_SIGNUP_AGE)
		select i.BRANDID, c.iso2_code, 18
		from admin_all.COUNTRY c
			cross join inserted i
	end
end
go
--begin transaction
--declare @ID int
--insert into [admin].[CASINO_BRAND_DEF](BRANDNAME) values('dfsadasfd')
--select @id = scope_identity()
--select * from admin_all.BRAND_COUNTRY where BRANDID = @id
--rollback