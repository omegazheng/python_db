
IF EXISTS (SELECT 1 FROM sys.triggers  WHERE Name = 'TR_USER_CONSENT_CHANGE')
	begin    
    	drop trigger admin_all.TR_USER_CONSENT_CHANGE;
    end
    
IF NOT EXISTS (SELECT 1 FROM sys.triggers  WHERE Name = 'TR_USER_CONSENT_CHANGE')
exec('
	create trigger admin_all.TR_USER_CONSENT_CHANGE on admin_all.UserConsent
	
	after insert, update
	as
	begin
		if @@rowcount = 0
			return
		set nocount on 
		update admin_all.UserConsent set UpdateDate = current_timestamp where UserConsentId in (select distinct UserConsentId from inserted);		
		insert into admin_all.UserConsentHistory (UserConsentId, ConsentVersionId ,PartyId , Status, CreationDate, UpdateDate)		
			select  UserConsentId , ConsentVersionId,PartyId, Status, CreationDate, UpdateDate
			from inserted
 		
	
end')