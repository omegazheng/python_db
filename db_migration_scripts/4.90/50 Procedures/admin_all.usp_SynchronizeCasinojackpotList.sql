SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_SynchronizeCasinoJackpotList
(
    @ProductId int,
    @JackpotListString varchar(8000),
    @DelimiterBetweenJackpots varchar(5),
    @DelimiterBetweenJackpotFields varchar(5),
    @DelimiterBetweenGameIDs varchar(5),
    @UpdatedCount int output
)
as

BEGIN

    declare @SegmentID int
    select @SegmentID = id from admin_all.segment where name = 'Casino'
    if (@SegmentID is null)
        RAISERROR('Segment Casino does NOT exist', 16, 1);

    create table #Jackpots(Jackpot varchar(3000))
    create table #GameIDs(GameID varchar(100))

    insert into #Jackpots(Jackpot)
    select ltrim(rtrim(Item))
    from admin_all.fc_splitDelimiterString(@JackpotListString, @DelimiterBetweenJackpots)
    where ltrim(rtrim(Item)) <> ''

    begin transaction
        declare @CurrentJackpot varchar(500), @Name varchar(100), @Amount NUMERIC(38,18), @GameIDList varchar(2000),
            @Currency varchar(10), @BrandJackpotId int, @CurrentGameID varchar(100), @CurrentGameInfoID int
        set @UpdatedCount = 0

        while EXISTS(select Jackpot from #Jackpots)
            begin

                SELECT TOP 1 @CurrentJackpot = Jackpot FROM #Jackpots;
                select @Name = Name, @Amount = Amount, @Currency = Currency, @GameIDList = GameIDList
                from admin_all.fc_splitDelimiterStringToJackpotFields(@CurrentJackpot, @DelimiterBetweenJackpotFields)
                --                 print '@Name=' + @Name
--                     + ', @GameId=' + @GameId
--                     + ', @Amount=' + cast(@Amount as varchar(100))
--                     + ', @Currency=' + @Currency;

                -- BRAND_JACKPOTS
                if NOT exists (select ID from admin_all.BRAND_JACKPOTS where NAME = @Name and PLATFORM_ID = @ProductId and CURRENCY = @Currency)
                    begin
                        insert into admin_all.BRAND_JACKPOTS(NAME, PLATFORM_ID, AMOUNT, CURRENCY)
                        values(@Name, @ProductId, @Amount, @Currency);
                        set @UpdatedCount = @UpdatedCount + 1;
                        select @BrandJackpotId = ID from admin_all.BRAND_JACKPOTS where NAME = @Name and PLATFORM_ID = @ProductId and CURRENCY=@Currency
                    end
                else
                    begin
                        select @BrandJackpotId = ID from admin_all.BRAND_JACKPOTS where NAME = @Name and PLATFORM_ID = @ProductId and CURRENCY=@Currency
                        begin
                            update admin_all.BRAND_JACKPOTS set AMOUNT = @Amount
                            where NAME = @Name and PLATFORM_ID = @ProductId and CURRENCY=@Currency
                            set @UpdatedCount = @UpdatedCount + 1;
                        end
                    end

                -- GAME_JACKPOT_LINK
                print '@GameIDList=' + @GameIDList
                if  @GameIDList != 'null' AND LEN(@GameIDList) != 0
                    begin
                        --                         print '-----' + CAST(LEN(@GameId) as varchar(100))
                        insert into #GameIDs(GameID)
                        select ltrim(rtrim(Item))
                        from admin_all.fc_splitDelimiterString(@GameIDList, @DelimiterBetweenGameIDs)
                        where ltrim(rtrim(Item)) <> ''

                        while EXISTS(select GameID from #GameIDs)
                            begin
                                SELECT TOP 1 @CurrentGameID = GameID FROM #GameIDs;
                                if exists (select ID from admin_all.game_info where PLATFORM_ID = @ProductId and GAME_ID = @CurrentGameID)
                                     begin
                                         select @CurrentGameInfoID = ID from admin_all.game_info where PLATFORM_ID = @ProductId and GAME_ID = @CurrentGameID
                                         if not exists(select * from admin_all.GAME_JACKPOT_LINK where game_id = @CurrentGameInfoID and JACKPOT_ID = @BrandJackpotId)
                                             begin
                                                 insert into admin_all.GAME_JACKPOT_LINK(GAME_ID, JACKPOT_ID)
                                                 values(@CurrentGameInfoID, @BrandJackpotId);
                                             end
                                     end
                                else
                                     print 'Ignore non-existing GameID=' + @CurrentGameID + ' for jackpot=' + @Name;
                                delete from #GameIDs where GameID = @CurrentGameID;
                            end
                    end

                delete from #Jackpots where Jackpot = @CurrentJackpot;
            end
        print 'Update ' + cast(@UpdatedCount as varchar(100)) + ' records'
    commit

END
go

-- exec admin_all.usp_SynchronizeCasinoJackpotList
--      @ProductId = 233,
--      @JackpotListString = 'Testjackpot,12345.899,EUR,TestGameId&&TestGameId2;',
--      @DelimiterBetweenJackpots = ';',
--      @DelimiterBetweenJackpotFields = ',',
--      @DelimiterBetweenGameIDs = '&&',
--      @UpdatedCount = null
