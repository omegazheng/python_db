set ansi_nulls, quoted_identifier on
go
if object_id('[admin_all].[usp_UpdateFreePlayPromotion]') is null
    exec('create procedure [admin_all].[usp_UpdateFreePlayPromotion] as')
go

-- update FreePlayPromotion
-- update column: Status, TriggerDate, LastUpdatedTime, IsCompleted, TotalBalance, RemainingBalance, AmountWon, DefaultStakeLevel
-- where column: ID || Code & PartyID

alter procedure [admin_all].[usp_UpdateFreePlayPromotion]
    (
        @ID BIGINT,
        @CODE NVARCHAR(40),
        @STATUS NVARCHAR(30),
        @TRIGGER_DATE DATETIME,
        @LAST_UPDATED_TIME DATETIME,
        @IS_COMPLETED bit,
        @PARTY_ID INT,
        @TOTAL_BALANCE NUMERIC(38,18),
        @REMAINING_BALANCE NUMERIC(38,18),
        @AMOUNT_WON NUMERIC(38,18),
        @DEFAULT_STAKE_LEVEL NUMERIC(38,18),
        @BET_PER_ROUND NUMERIC(16,2),
        @ROUNDS INT,
        @BET_PER_LINE NUMERIC(38,18),
        @GAMBLE_ENABLED bit
        )
    as
    BEGIN
        DECLARE @SQL nvarchar(max);
        DECLARE @SQL_where nvarchar(max);
        DECLARE @SQL_set nvarchar(max);

        SET @SQL = 'update admin_all.FREEPLAY_PROMOTION '
        SET @SQL_where = ' where '
        SET @SQL_set = ' set '

        -- where
        IF @ID IS NOT NULL
            BEGIN
                set @SQL_where = @SQL_where + ' id=' + CAST(@ID AS NVARCHAR(50))
            END
        ELSE
            BEGIN
                set @SQL_where = @SQL_where + ' CODE=''' + @CODE + ''' and PARTY_ID=' + CAST(@PARTY_ID AS NVARCHAR(50))
            END

        -- update
        set @SQL_set = @SQL_set + ' LAST_UPDATED_TIME = ''' + CONVERT(nvarchar(25), @LAST_UPDATED_TIME, 121) + ''''
        set @SQL_set = @SQL_set + ', IS_COMPLETED =''' + CAST(@IS_COMPLETED AS CHAR(1)) + ''''

        IF @STATUS IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', STATUS=''' + @STATUS + ''''
            END

        IF @TRIGGER_DATE IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', TRIGGER_DATE = ''' + CONVERT(nvarchar(25), @TRIGGER_DATE, 121) + ''''
            END

        IF @TOTAL_BALANCE IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', TOTAL_BALANCE=' + CAST(@TOTAL_BALANCE AS VARCHAR(38))
            END

        IF @REMAINING_BALANCE IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', REMAINING_BALANCE=' + CAST(@REMAINING_BALANCE AS VARCHAR(38))
            END

        IF @AMOUNT_WON IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', AMOUNT_WON=' + CAST(@AMOUNT_WON AS VARCHAR(38))
            END

        IF @DEFAULT_STAKE_LEVEL IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', DEFAULT_STAKE_LEVEL=' + CAST(@DEFAULT_STAKE_LEVEL AS VARCHAR(38))
            END

        IF @BET_PER_ROUND IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', BET_PER_ROUND=' + CAST(@BET_PER_ROUND AS VARCHAR(38))
            END

        IF @BET_PER_LINE IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', BET_PER_LINE=' + CAST(@BET_PER_LINE AS VARCHAR(38))
            END

        IF @ROUNDS IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', ROUNDS=' + CAST(@ROUNDS AS VARCHAR(40))
            END

        IF @GAMBLE_ENABLED IS NOT NULL
            BEGIN
                set @SQL_set = @SQL_set + ', GAMBLE_ENABLED =''' + CAST(@GAMBLE_ENABLED AS CHAR(1)) + ''''
            END

        -- exec
        SET @SQL = @SQL + @SQL_set + @SQL_where
        PRINT @SQL
        exec(@SQL)

    END
GO


