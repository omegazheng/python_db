SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [admin_all].[usp_MigrateRegistryProperty]
    @PropertyA nvarchar(200),
    @PropertyB nvarchar(200),
    @IsToOverride bit = 0
AS
BEGIN
    DECLARE @ValuePropertyA nvarchar(200);
    DECLARE @valuePropertyB nvarchar(200);
    DECLARE @Error nvarchar(200);

    select @ValuePropertyA = value from admin_all.registry_hash where MAP_KEY=@PropertyA
    select @valuePropertyB = value from admin_all.registry_hash where MAP_KEY=@PropertyB

    IF (@ValuePropertyA IS NOT NULL)
        BEGIN
            IF (@valuePropertyB IS NULL)
                BEGIN
                    INSERT INTO admin_all.REGISTRY_HASH(MAP_KEY,value,[ENCRYPTED],KEY_IDX)
                        SELECT @PropertyB, value,ENCRYPTED,KEY_IDX from REGISTRY_HASH where MAP_KEY=@PropertyA;
                    delete from REGISTRY_HASH   where MAP_KEY=@PropertyA;
                END
            ELSE
                BEGIN
                    SET @Error = 'The Property ' + @PropertyB + ' already exists in registry_hash table.';
                    RAISERROR (@Error ,1,1);
                END
        END
    ELSE
        BEGIN
            SET @Error = 'The Property ' + @PropertyA + ' dont exists in registry_hash table.';
            RAISERROR (@Error ,1,1);
        END
    select @ValuePropertyA =value from admin_all.BRAND_REGISTRY where MAP_KEY=@PropertyA
    select @valuePropertyB =value from admin_all.BRAND_REGISTRY where MAP_KEY=@PropertyB
    IF (@ValuePropertyA IS NOT NULL)
        BEGIN
            IF(@valuePropertyB IS NULL)
                BEGIN
                    INSERT INTO admin_all.BRAND_REGISTRY(brandid,MAP_KEY,value,[ENCRYPTED],KEY_IDX,country)
                        SELECT brandid,@PropertyB, value,ENCRYPTED,KEY_IDX,Country from BRAND_REGISTRY where MAP_KEY=@PropertyA;

                    delete from BRAND_REGISTRY   where MAP_KEY=@PropertyA;
                END
            ELSE
                BEGIN
                    SET @Error = 'The Property ' + @PropertyB + ' already exists in registry_hash table.';
                    RAISERROR (@Error ,1,1);
                END
        END
    ELSE
        BEGIN
            SET @Error = 'The Property ' + @PropertyA + ' dont exists in Brand_registry table.';
            RAISERROR (@Error ,1,1);
        END
END

GO
