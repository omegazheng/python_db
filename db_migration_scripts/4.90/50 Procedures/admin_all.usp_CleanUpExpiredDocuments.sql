set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CleanupExpiredDocuments
as
begin 
	set nocount on
	select ID, STATUS into #1 from admin_all.USER_DOCUMENTS where EXPIRY_DATE < getdate()
	update t
		set t.STATUS = 'EXPIRED'
	from #1 s
		inner loop join admin_all.USER_DOCUMENTS t on s.ID = t.ID
    where s.STATUS <> 'DELETED'
end
