set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_DeleteFreePlayPlan
  (
    @ID INT
  )
as
begin
    set nocount, xact_abort on
    begin transaction
	delete admin_all.FREEPLAY_PLAN_GAMEINFO where FREEPLAY_PLAN_ID = @ID
	delete admin_all.FREEPLAY_PLAN_CURRENCY_STAKE where FREEPLAY_PLAN_ID = @ID

	delete admin_all.FREEPLAY_PROMOTION_SESSION where FREEPLAY_PROMOTION_ID in(select a.ID from admin_all.FREEPLAY_PROMOTION a where a.FREEPLAY_PLAN_ID = @ID)

	delete admin_all.FREEPLAY_PROMOTION where FREEPLAY_PLAN_ID = @ID
    delete from admin_all.FREEPLAY_PLAN where ID = @ID
    commit

end
go

--begin transaction
--exec admin_all.usp_DeleteFreePlayPlan 1
--rollback
--select * from admin_all.FREEPLAY_PLAN
--select top 100 * from admin_all.FREEPLAY_PROMOTION
--select 'delete admin_all.' + object_name(f.parent_object_id) + ' where '+  col_name(c.parent_object_id, c.parent_column_id ) + ' = @ID'
--from sys.foreign_keys f 
--	inner join sys.foreign_key_columns c on c.constraint_object_id = f.object_id
--where f.referenced_object_id = object_id('admin_all.FREEPLAY_PROMOTION_SESSION')
