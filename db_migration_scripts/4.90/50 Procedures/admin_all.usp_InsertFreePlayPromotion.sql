--copy from 4.30
set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_InsertFreePlayPromotion]
(
    @FREEPLAY_PLAN_ID INT = NULL,
    @CODE NVARCHAR(40),
    @STATUS NVARCHAR(30),
    @TRIGGER_DATE DATETIME,
    @LAST_UPDATED_TIME DATETIME,
    @IS_COMPLETED bit,
    @PARTY_ID INT,
    @TOTAL_BALANCE numeric(38,18) = null,
    @REMAINING_BALANCE numeric(38,18) = null,
    @AMOUNT_WON numeric(38,18) = null,
    @DEFAULT_STAKE_LEVEL numeric(38,18) = null,
    @GAME_INFO_ID INT = null,
    @LINE INT = null,
    @COIN numeric(38,18) = null,
    @DENOMINATION numeric(38,18) = null,
    @BET_PER_ROUND numeric(38,18) = null,
    @ROUNDS int = null,
    @GAME_INFO_IDS NVARCHAR(3000) = null,
    @BET_PER_LINE numeric(38,18) = null,
    @GAMBLE_ENABLED bit
)
as
BEGIN
    DECLARE @ID bigint

    insert into admin_all.FREEPLAY_PROMOTION
    (
        FREEPLAY_PLAN_ID,
        CODE,
        STATUS,
        TRIGGER_DATE,
        LAST_UPDATED_TIME,
        IS_COMPLETED,
        PARTY_ID,
        TOTAL_BALANCE,
        REMAINING_BALANCE,
        AMOUNT_WON,
        DEFAULT_STAKE_LEVEL,
        GAME_INFO_ID,
        LINE,
        COIN,
        DENOMINATION,
        BET_PER_ROUND,
        ROUNDS,
        GAME_INFO_IDS,
        BET_PER_LINE,
        GAMBLE_ENABLED
    )
    values
    (
        @FREEPLAY_PLAN_ID,
        @CODE,
        @STATUS,
        @TRIGGER_DATE,
        @LAST_UPDATED_TIME,
        @IS_COMPLETED,
        @PARTY_ID,
        @TOTAL_BALANCE,
        @REMAINING_BALANCE,
        @AMOUNT_WON,
        @DEFAULT_STAKE_LEVEL,
        @GAME_INFO_ID,
        @LINE,
        @COIN,
        @DENOMINATION,
        @BET_PER_ROUND,
        @ROUNDS,
        @GAME_INFO_IDS,
        @BET_PER_LINE,
        @GAMBLE_ENABLED
    )

    select @ID = @@IDENTITY
    select  id as id,
            freeplay_plan_id as freePlayPlanId,
            code as code,
            status as status,
            trigger_date as triggerDate,
            last_updated_time as lastUpdatedTime,
            is_completed as isCompleted,
            party_id as partyId,
            isnull(total_balance,0) as totalBalance,
            isnull(remaining_balance,0) as remainingBalance,
            isnull(amount_won,0) as amountWon,
            default_stake_level as defaultStakeLevel,
            game_info_id as gameInfoId,
            line as line,
            isnull(coin,0) as coin,
            isnull(denomination,0) as denomination,
            isnull(bet_per_round,0) as betPerRound,
            isnull(BET_PER_LINE,0) as BET_PER_LINE,
            GAMBLE_ENABLED as GAMBLE_ENABLED

    from admin_all.FREEPLAY_PROMOTION where ID = @ID

END

go
