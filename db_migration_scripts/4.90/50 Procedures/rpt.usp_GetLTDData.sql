set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetLTDData
(
	@PartyID int
)
as
begin
	set nocount on 
	declare @AccountID int, @Deposit numeric(38,18), @Withdrawal numeric(38, 18)

	select @AccountID = ID from admin_all.ACCOUNT where PARTYID = @PartyID
	select	@Deposit = sum(case when p.Type in ('DEPOSIT') then p.Amount else 0 end),
			@Withdrawal = sum(case when p.Type in ('WITHDRAWAL') then p.Amount else 0 end)
	from [admin_all].[v_PaymentLiveToDate] p 
	where p.AccountID = @AccountID
		and p.Type in ('DEPOSIT', 'WITHDRAWAL')
		and p.Status = 'COMPLETED'
	select @Deposit as Deposit, @Withdrawal Withdrawal,
			
			sum(case when a.TranType in ('GAME_BET') then -a.AmountReal else 0 end) GameBetAmountReal,
			sum(case when a.TranType in ('GAME_BET') then -a.AmountReleasedBonus else 0 end) GameBetAmountReleasedBonus,
			sum(case when a.TranType in ('GAME_BET') then -a.AmountPlayableBonus else 0 end) GameBetAmountPlayableBonus,

			sum(case when a.TranType in ('GAME_WIN') then a.AmountReal else 0 end) GameWinAmountReal,
			sum(case when a.TranType in ('GAME_WIN') then a.AmountReleasedBonus else 0 end) GameWinAmountReleasedBonus,
			sum(case when a.TranType in ('GAME_WIN') then a.AmountPlayableBonus else 0 end) GameWinAmountPlayableBonus,

			sum(case when a.TranType in ('BONUS_CRE') then a.AmountReleasedBonus else 0 end) BonusGrantAmountReleasedBonus,
			sum(case when a.TranType in ('BONUS_CRE') then a.AmountPlayableBonus else 0 end) BonusGrantAmountPlayableBonus,

			sum(case when a.TranType in ('BONUS_REL') then a.AmountReleasedBonus else 0 end) BonusReleaseAmountReleasedBonus,
			sum(case when a.TranType in ('WITHDRAWAL') then -a.AmountReleasedBonus else 0 end) BonusWithdrawnAmountReleasedBonus

	from admin_all.AccountTranAggregate a
	where a.AggregateType = 0
		and a.Period = 'Y'
		and PartyID = @PartyID
		and TranType in('GAME_BET', 'GAME_WIN', 'WITHDRAWAL', 'BONUS_CRE', 'BONUS_REL')
end
go
--exec rpt.usp_GetLTDData 91429636
