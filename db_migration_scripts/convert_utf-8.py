# import codecs
import glob
import tempfile
import os
import chardet

CONFIDENCE_THRESHOLD = 0.9
ALL_FILES = glob.glob('**/*.sql', recursive=True)
for filename in ALL_FILES:
    with open(filename, 'rb') as f:
        content_bytes = f.read()
    detected = chardet.detect(content_bytes)
    encoding = detected['encoding']
    confidence = detected['confidence']
    print(f"{filename}: detected as {encoding} with confidence {confidence}.")
    if confidence < CONFIDENCE_THRESHOLD:
        print(f"{filename} skipped.")
        continue
    content_text = content_bytes.decode(encoding)
    with tempfile.NamedTemporaryFile(mode='w', dir=os.path.dirname(filename),
                                     encoding='utf-8', delete=False) as f:
        f.write(content_text)
    os.replace(f.name, filename)
