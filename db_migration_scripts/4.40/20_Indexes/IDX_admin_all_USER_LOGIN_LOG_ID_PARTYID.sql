set xact_abort, nocount on

if not exists(select * from sys.indexes where name = 'IDX_admin_all_USER_LOGIN_LOG_IP_PARTYID' and object_id = object_id('[admin_all].[USER_LOGIN_LOG]'))
    create index IDX_admin_all_USER_LOGIN_LOG_IP_PARTYID on [admin_all].[USER_LOGIN_LOG] (IP, PARTYID)