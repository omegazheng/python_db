set xact_abort, nocount on
begin transaction
delete a 
from (
		select ID, row_number() over(partition by BONUS_PLAN_ID, Party_ID order by id) rn
		from admin_all.BONUS_PLAN_INCLUDED_USER
	) a
where rn > 1

if exists(select * from sys.indexes where name = 'IDX_admin_all_BONUS_PLAN_INCLUDED_USER_BONUS_PLAN_ID' and object_id = object_id('[admin_all].[BONUS_PLAN_INCLUDED_USER]'))
	drop index admin_all.BONUS_PLAN_INCLUDED_USER.IDX_admin_all_BONUS_PLAN_INCLUDED_USER_BONUS_PLAN_ID

if not exists(select * from sys.indexes where name = 'IDX_admin_all_BONUS_PLAN_INCLUDED_USER_BONUS_PLAN_ID_PARTY_ID' and object_id = object_id('[admin_all].[BONUS_PLAN_INCLUDED_USER]'))
	create unique index IDX_admin_all_BONUS_PLAN_INCLUDED_USER_BONUS_PLAN_ID_PARTY_ID on admin_all.BONUS_PLAN_INCLUDED_USER(BONUS_PLAN_ID, PARTY_ID) with (ignore_dup_key = on)

commit