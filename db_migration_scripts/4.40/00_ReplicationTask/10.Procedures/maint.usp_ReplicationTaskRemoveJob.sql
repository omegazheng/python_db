set ansi_nulls, quoted_identifier on
if object_id('maint.usp_ReplicationTaskRemoveJob') is null
	exec('create procedure maint.usp_ReplicationTaskRemoveJob as return')
go
alter procedure maint.usp_ReplicationTaskRemoveJob
as
begin
	set nocount on
	declare @JobID uniqueidentifier
	declare c cursor local for 
		select job_id 
		from msdb.dbo.sysjobs 
		where name like 'ReplicationTask%!['+rtrim(db_name())+'!]' escape '!'
	open c
	fetch next from c into @JobID
	while @@fetch_status = 0
	begin
		exec msdb..sp_delete_job @job_id = @JObID
		fetch next from c into @JobID
	end
	close c
	deallocate c
end