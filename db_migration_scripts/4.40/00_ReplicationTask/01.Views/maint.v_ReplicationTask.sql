set ansi_nulls, quoted_identifier on
if object_id('maint.v_ReplicationTask') is null
	exec('create view maint.v_ReplicationTask as select 1 as one')
go
alter view maint.v_ReplicationTask
as
select 
		rt.ConfigurationID, rt.SessionID,
		c.Name, 
		rl.FromValue, rl.ToValue, rl.Rows, rl.StartDate, rl.EndDate, rl.Error, rl.ReplicationHistoryID, rl.ApplicationName, rl.LoginName
from maint.ReplicationTask rt with(nolock)
	inner join maint.ReplicationConfiguration c on c.ConfigurationID = rt.ConfigurationID
	left join maint.ReplicationLastExecution rl on rl.ConfigurationID = rt.ConfigurationID
