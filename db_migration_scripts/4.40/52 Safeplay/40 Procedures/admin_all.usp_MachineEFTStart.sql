set ansi_nulls, quoted_identifier on
if object_id('admin_all.usp_MachineEFTStart') is null
begin
	exec ('create procedure admin_all.usp_MachineEFTStart as')
end
go
alter procedure admin_all.usp_MachineEFTStart
(
	@PartyID int,
	@AmountReal numeric(38, 18),
	@ReleasedBonus numeric(38, 18),
	@PlayableBonus numeric(38,18)
)
as
begin
	set nocount on
	set xact_abort off
	begin transaction
	begin try
	exec admin_all.usp_LockTwoCheeryTransaction @PartyID = @PartyID 

	if exists(select * from admin_all.MachineTranPendingEFT where PartyID = @PartyID)
	begin
		raiserror('Previous Machine EFT has not completed.', 16, 1)
		rollback
		return
	end

	declare @AccountTranID bigint, @Date datetime = getdate(), @EFTID bigint = next value for admin_all.SeqMachineTranEFTHistoryID

	select @AmountReal = isnull(@AmountReal, 0), @ReleasedBonus = isnull(@ReleasedBonus, 0), @PlayableBonus = isnull(@PlayableBonus, @PlayableBonus)

	exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = @AmountReal, @ReleasedBonus = @ReleasedBonus,  @PlayableBonus = @PlayableBonus, 
												@TranType = 'MACHIN_EFT', @PlatformID = null, @DateTime = @Date,
												@UpdateBonuses = 1, @AccountTranID= @AccountTranID output
	insert into admin_all.MachineTranPendingEFT(PartyID, Datetime, AmountReal, ReleasedBonus, PlayableBonus, EFTID, AccountTranID)
		select @PartyID PartyID, @Date Datetime, @AmountReal AmountReal, @ReleasedBonus ReleasedBonus, @PlayableBonus PlayableBonus, @EFTID EFTID, @AccountTranID AccountTranID
	end try
	begin catch
		if @@trancount>0
			rollback;
		throw;
	end catch
	commit
	select @EFTID as EFTID
end
go

