-- These stored proc are for migration only. After 4.40 , they can be removed
set ansi_nulls on
go
set quoted_identifier on
go

if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step1_AddBankNameSelectionField')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step1_AddBankNameSelectionField
go

CREATE PROCEDURE admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step1_AddBankNameSelectionField
(
    @paymentMethodCode nvarchar(30)
)
as

begin

if (exists (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode)
and NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='BankName' and payment_type='WITHDRAWAL')
and EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='BankName' and payment_type='DEPOSIT' and type = 'SELECT' ))
    begin
        declare @TYPE varchar(255), @MAX_LENGTH tinyint, @MIN_LENGTH tinyint, @WEIGHT tinyint, @IS_REQUIRED tinyint, @OPTIONS nvarchar(max);

        print CONCAT('type=',@TYPE);
        select @TYPE = type, @MAX_LENGTH=MAX_LENGTH, @MIN_LENGTH=MIN_LENGTH, @WEIGHT=WEIGHT, @IS_REQUIRED=IS_REQUIRED, @OPTIONS=OPTIONS
        from admin_all.MANUAL_BANK_FIELD where payment_method_code = @paymentMethodCode and name = 'BankName' and payment_type = 'DEPOSIT' and type = 'SELECT';

        print CONCAT('type=',@TYPE);
        insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
        values(@paymentMethodCode, 'BankName', 'WITHDRAWAL',  @TYPE, @MAX_LENGTH, @MIN_LENGTH, @WEIGHT, @IS_REQUIRED, @OPTIONS);
    end

end

-- exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step1_AddBankNameSelectionField @paymentMethodCode='MB_VIP_BANK_TSF'