
if exists (select *from dbo.CUSTOMER where name in ('REB', 'DEMO'))

begin

declare @TYPE varchar(255), @MAX_LENGTH tinyint, @MIN_LENGTH tinyint, @WEIGHT tinyint, @IS_REQUIRED tinyint, @OPTIONS nvarchar(max);

if exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK')
begin

if NOT exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'bankName' and payment_type = 'WITHDRAWAL')
  begin
    if exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'bankName' and payment_type = 'DEPSOIT')
      select @TYPE = type, @MAX_LENGTH=MAX_LENGTH, @MIN_LENGTH=MIN_LENGTH, @WEIGHT=WEIGHT, @IS_REQUIRED=IS_REQUIRED, @OPTIONS=OPTIONS
      from admin_all.MANUAL_BANK_FIELD where payment_method_code = 'MB_EUROBANK' and name = 'bankName' and payment_type = 'DEPSOIT';
    else
      select @TYPE = 'TEXT', @MAX_LENGTH=60, @MIN_LENGTH=1, @WEIGHT=1, @IS_REQUIRED=1, @OPTIONS=null; --/ips/getManualBankFields response before change

    insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
    values('MB_EUROBANK', 'bankName', 'WITHDRAWAL', @TYPE, @MAX_LENGTH, @MIN_LENGTH, @WEIGHT, @IS_REQUIRED, @OPTIONS);
  end

if NOT exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'iban' and payment_type = 'WITHDRAWAL')
  begin
    if exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'iban' and payment_type = 'DEPSOIT')
      select @TYPE = type, @MAX_LENGTH=MAX_LENGTH, @MIN_LENGTH=MIN_LENGTH, @WEIGHT=WEIGHT, @IS_REQUIRED=IS_REQUIRED, @OPTIONS=OPTIONS
      from admin_all.MANUAL_BANK_FIELD where payment_method_code = 'MB_EUROBANK' and name = 'iban' and payment_type = 'DEPSOIT';
    else
      select @TYPE = 'TEXT', @MAX_LENGTH=60, @MIN_LENGTH=1, @WEIGHT=1, @IS_REQUIRED=1, @OPTIONS=null; --/ips/getManualBankFields response before change

    insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
    values('MB_EUROBANK', 'iban', 'WITHDRAWAL', @TYPE, @MAX_LENGTH, @MIN_LENGTH, @WEIGHT, @IS_REQUIRED, @OPTIONS);
  end

if NOT exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'firstName' and payment_type = 'WITHDRAWAL')
  begin
    if exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'firstName' and payment_type = 'DEPSOIT')
      select @TYPE = type, @MAX_LENGTH=MAX_LENGTH, @MIN_LENGTH=MIN_LENGTH, @WEIGHT=WEIGHT, @IS_REQUIRED=IS_REQUIRED, @OPTIONS=OPTIONS
      from admin_all.MANUAL_BANK_FIELD where payment_method_code = 'MB_EUROBANK' and name = 'firstName' and payment_type = 'DEPSOIT';
    else
      select @TYPE = 'TEXT', @MAX_LENGTH=60, @MIN_LENGTH=1, @WEIGHT=1, @IS_REQUIRED=1, @OPTIONS=null; --/ips/getManualBankFields response before change

    insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
    values('MB_EUROBANK', 'firstName', 'WITHDRAWAL', @TYPE, @MAX_LENGTH, @MIN_LENGTH, @WEIGHT, @IS_REQUIRED, @OPTIONS);
  end

if NOT exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'lastName' and payment_type = 'WITHDRAWAL')
  begin
    if exists (select * from admin_all.manual_bank_field where payment_method_code = 'MB_EUROBANK' and name = 'lastName' and payment_type = 'DEPSOIT')
      select @TYPE = type, @MAX_LENGTH=MAX_LENGTH, @MIN_LENGTH=MIN_LENGTH, @WEIGHT=WEIGHT, @IS_REQUIRED=IS_REQUIRED, @OPTIONS=OPTIONS
      from admin_all.MANUAL_BANK_FIELD where payment_method_code = 'MB_EUROBANK' and name = 'lastName' and payment_type = 'DEPSOIT';
    else
      select @TYPE = 'TEXT', @MAX_LENGTH=60, @MIN_LENGTH=1, @WEIGHT=1, @IS_REQUIRED=1, @OPTIONS=null; --/ips/getManualBankFields response before change

    insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
    values('MB_EUROBANK', 'lastName', 'WITHDRAWAL', @TYPE, @MAX_LENGTH, @MIN_LENGTH, @WEIGHT, @IS_REQUIRED, @OPTIONS);
  end

end

end

