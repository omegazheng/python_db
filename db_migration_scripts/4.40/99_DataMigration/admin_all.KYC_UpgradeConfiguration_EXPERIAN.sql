

-- Turn on KYC EXPERIAN provider
-- via admin_all.BRAND_COUNTRY (KYC_SYSTEM), admin_all.BRAND_REGISTRY (kyc.check.enabled, experian.enabled)
-- instead of admin_all.BRAND_REGISTRY (kyc.experian)

if EXISTS (select * from admin_all.BRAND_REGISTRY where MAP_KEY = 'kyc.experian' and value = 'true')
  begin
      declare @CURRENT_BRANDID int;
      select @CURRENT_BRANDID = BRANDID from admin_all.BRAND_REGISTRY where MAP_KEY = 'kyc.experian' and value = 'true'

      if NOT EXISTS (select * from admin_all.BRAND_REGISTRY where BRANDID = @CURRENT_BRANDID and MAP_KEY = 'kyc.check.enabled')
          insert into admin_all.BRAND_REGISTRY(brandid, map_key, "value")
          values(@CURRENT_BRANDID, 'kyc.check.enabled', 'true')
      ELSE
          update admin_all.BRAND_REGISTRY set value = 'true' where BRANDID = @CURRENT_BRANDID and MAP_KEY = 'kyc.check.enabled';

--       if NOT EXISTS (select * from admin_all.BRAND_REGISTRY where BRANDID = @CURRENT_BRANDID and MAP_KEY = 'experian.enabled')
--           insert into admin_all.BRAND_REGISTRY(brandid, map_key, "value")
--           values(@CURRENT_BRANDID, 'experian.enabled', 'true')
--       ELSE
--           update admin_all.BRAND_REGISTRY set value = 'true' where BRANDID = @CURRENT_BRANDID and MAP_KEY = 'experian.enabled';

  end


if EXISTS (select * from admin_all.REGISTRY_HASH where MAP_KEY = 'kyc.experian' and value = 'true')
  begin
      if NOT EXISTS (select * from admin_all.REGISTRY_HASH where MAP_KEY = 'kyc.check.enabled')
          insert into admin_all.REGISTRY_HASH(map_key, "value")
          values('kyc.check.enabled', 'true')
      ELSE
          update admin_all.REGISTRY_HASH set value = 'true' where MAP_KEY = 'kyc.check.enabled';
--
--       if NOT EXISTS (select * from admin_all.REGISTRY_HASH where MAP_KEY = 'experian.enabled')
--           insert into admin_all.REGISTRY_HASH(map_key, "value")
--           values('experian.enabled', 'true')
--       ELSE
--           update admin_all.REGISTRY_HASH set value = 'true' where MAP_KEY = 'experian.enabled';

  end