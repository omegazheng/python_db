-- These stored proc are for migration only. After 4.40 , they can be removed
set ansi_nulls on
go
set quoted_identifier on
go

if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddSpecialFields_MB_VIP_BANK_TSF')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddSpecialFields_MB_VIP_BANK_TSF
go

CREATE PROCEDURE admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddSpecialFields_MB_VIP_BANK_TSF
(
    @paymentMethodCode nvarchar(30)
)
as

begin

if exists (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode)
    begin
        if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='dateOfIssue' and payment_type='WITHDRAWAL')
            insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
            values(@paymentMethodCode, 'dateOfIssue', 'WITHDRAWAL', 'DATE', 10, 8, 1, 1, null);

        if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='expiryDate' and payment_type='WITHDRAWAL')
          insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
          values(@paymentMethodCode, 'expiryDate', 'WITHDRAWAL', 'DATE', 10, 8, 1, 1, null);
    end
end

-- exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddSpecialFields_MB_VIP_BANK_TSF @paymentMethodCode='MB_VIP_BANK_TSF'