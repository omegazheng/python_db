-- These stored proc are for migration only. After 4.40 , they can be removed
set ansi_nulls on
go
set quoted_identifier on
go

if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddTextFields_MB_AKBANK_AND_MB_GRANTIBANK')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddTextFields_MB_AKBANK_AND_MB_GRANTIBANK
go

CREATE PROCEDURE admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddTextFields_MB_AKBANK_AND_MB_GRANTIBANK
(
    @paymentMethodCode nvarchar(30)
)
as

begin

if exists (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode)
begin
    declare @fields table (field nvarchar(255));
    insert into @fields(field) select name from admin_all.MANUAL_BANK_FIELD where payment_method_code = @paymentMethodCode and payment_type = 'DEPOSIT' and type = 'TEXT' order by weight desc;
     while EXISTS(select field from @fields)
       begin
       declare @field nvarchar(255);
       SELECT TOP 1 @field = field FROM @fields;

      if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name=@field and payment_type='WITHDRAWAL')
      and EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name=@field and payment_type='DEPOSIT' and type = 'TEXT')
         begin
           declare @TYPE varchar(255), @MAX_LENGTH tinyint, @MIN_LENGTH tinyint, @WEIGHT tinyint, @IS_REQUIRED tinyint, @OPTIONS nvarchar(max);

           select @TYPE = type, @MAX_LENGTH=MAX_LENGTH, @MIN_LENGTH=MIN_LENGTH, @WEIGHT=WEIGHT, @IS_REQUIRED=IS_REQUIRED, @OPTIONS=OPTIONS
           from admin_all.MANUAL_BANK_FIELD where payment_method_code = @paymentMethodCode and name = @field and payment_type = 'DEPOSIT' and type = 'TEXT';

            insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
            values(@paymentMethodCode, @field, 'WITHDRAWAL', @TYPE, @MAX_LENGTH, @MIN_LENGTH, @WEIGHT, @IS_REQUIRED, @OPTIONS);
          end

       delete from @fields where field = @field;
     end
end

end


-- exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddTextFields_MB_AKBANK_AND_MB_GRANTIBANK @paymentMethodCode='MB_AKBANK'