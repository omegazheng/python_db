set ansi_nulls on
GO
set quoted_identifier on
GO
if object_id('admin_all.usp_SendAlertLog') is null
	exec('create procedure admin_all.usp_SendAlertLog as --')
go
alter procedure admin_all.usp_SendAlertLog
as
begin
	set nocount on
	declare @AlertLogID bigint, @StartDate datetime, @EndDate datetime, @AlertCode varchar(20), @SeverityLevel int, @EventCount int, @StackTrace uniqueidentifier, @Message uniqueidentifier, 
			@StackTraceString varchar(max), @MessageString varchar(max), @DistributionListID int,
			@Recipients varchar(max), @Subject nvarchar(255), @Importance varchar(6),
			@IncludeStackTrace bit, @IncludeMessage bit, @Information nvarchar(max),
			@MessageBody nvarchar(max), @Customer varchar(100)

	select @Customer = name from dbo.CUSTOMER
	declare c1 cursor local fast_forward for
		select AlertLogID, StartDate, EndDate,AlertCode, SeverityLevel, EventCount, StackTrace, Message
		from admin_all.AlertLog
		where DistributionListID is null
		order by AlertLogID
	open c1
	fetch next from c1 into @AlertLogID, @StartDate, @EndDate, @AlertCode, @SeverityLevel, @EventCount, @StackTrace, @Message
	while @@fetch_status = 0
	begin
		declare c2 cursor local fast_forward for 
			select Recipients, Subject, Importance, IncludeStackTrace, IncludeMessage, Information, DistributionListID
			from admin_all.AlertDistributionList
			where AlertCode = @AlertCode
				and @SeverityLevel between SeverityLevelFrom and SeverityLevelTo
		open c2
		fetch next from c2 into @Recipients, @Subject, @Importance, @IncludeStackTrace, @IncludeMessage, @Information, @DistributionListID
		while @@fetch_status = 0
		begin
			select	@Subject		= replace(replace(replace(replace(replace(replace(replace(@Subject, '%ServerName%', @@serverName), '%Customer%', @Customer), '%EndDate%', convert(varchar(30), @EndDate, 120)), '%StartDate%', convert(varchar(30), @StartDate, 120)), '%AlertCode%', @AlertCode), '%SeverityLevel%', cast(@SeverityLevel as varchar(20))), '%EventCount%', cast(@EventCount as varchar(20))),
					@Information	= replace(replace(replace(replace(replace(replace(replace(@Information, '%ServerName%', @@serverName), '%Customer%', @Customer), '%EndDate%', convert(varchar(30), @EndDate, 120)), '%StartDate%', convert(varchar(30), @StartDate, 120)), '%AlertCode%', @AlertCode), '%SeverityLevel%', cast(@SeverityLevel as varchar(20))), '%EventCount%', cast(@EventCount as varchar(20)))
			select @MessageBody = ''
			if @Information is not null
			begin
				select @MessageBody = @MessageBody + @Information + char(0x0d) + char(0x0a)
			end

			if @IncludeMessage = 1
			begin
				select @MessageBody = @MessageBody + cast(Data as varchar(max)) + char(0x0d) + char(0x0a)
				from admin_all.fn_GetLargeValue(@Message)
			end
			
			if @IncludeMessage = 1
			begin
				select @MessageBody = @MessageBody + cast(Data as varchar(max)) + char(0x0d) + char(0x0a)
				from admin_all.fn_GetLargeValue(@StackTrace)
			end
			
			exec maint.usp_SendMail @profile_name = 'OmegaAlert', @recipients=@Recipients, @subject = @Subject, @body = @MessageBody
			update admin_all.AlertLog set DistributionListID = @DistributionListID where AlertLogID = @AlertLogID
			fetch next from c2 into @Recipients, @Subject, @Importance, @IncludeStackTrace, @IncludeMessage, @Information, @DistributionListID
		end
		close c2
		deallocate c2
		update admin_all.AlertLog set DistributionListID = -1 where AlertLogID = @AlertLogID and DistributionListID is null

		fetch next from c1 into @AlertLogID, @StartDate, @EndDate, @AlertCode, @SeverityLevel, @EventCount, @StackTrace, @Message
	end
	close c1
	deallocate c1
end
go
--exec admin_all.usp_SendAlertLog