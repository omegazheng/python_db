SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
if object_id('maint.usp_SendMail') is null
	exec('create procedure maint.usp_SendMail as --')
go
alter procedure maint.usp_SendMail
(
	@profile_name sysname = null,        
	@recipients varchar(max) = null, 
	@copy_recipients varchar(max) = null,
	@blind_copy_recipients varchar(max) = null,
	@subject nvarchar(255) = null,
	@body nvarchar(max) = null, 
	@body_format varchar(20) = 'TEXT',  -- HTML
	@importance varchar(6) = 'NORMAL', --'HIGH, LOW'
	@sensitivity varchar(12) = 'NORMAL',--Personal, Private, Confidential
	@reply_to varchar(max) = null,
	@from_address varchar(max) = null
)
as
begin
	set nocount on
	if isnull(admin_all.fn_GetRegistry('System.Mail.Enabled'),'1') <>'1'
		return
	if isnull(admin_all.fn_GetRegistry('System.Mail.UseMSDB'),'1') = 1
	begin
		exec msdb..sp_send_dbmail @profile_name = @profile_name, @recipients = @recipients,  @copy_recipients = @copy_recipients, @blind_copy_recipients = @blind_copy_recipients, @subject = @subject, @body = @body,  @body_format = @body_format, @importance = @importance, @sensitivity = @sensitivity, @reply_to = @reply_to, @from_address = @from_address
		return
	end
	insert into maint.MailItem(ProfileName, Recipients, CopyRecipients, BlindCopyRecipients, Subject, Body, BodyFormat, Importance, Sensitivity, ReplyTo, FromAddress)
		select @profile_name, @recipients, @copy_recipients, @blind_copy_recipients, @subject, @body, @body_format, @importance, @sensitivity, @reply_to, @from_address
end