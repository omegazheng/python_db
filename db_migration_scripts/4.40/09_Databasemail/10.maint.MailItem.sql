
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
if object_id('maint.MailItem') is null
begin
	create table maint.MailItem
	(
		MailItemID int not null identity(1,1),
		ProfileName varchar(128),
		Recipients varchar(max),
		CopyRecipients varchar(max),
		BlindCopyRecipients varchar(max),
		Subject nvarchar(255),
		Body nvarchar(max), 
		BodyFormat varchar(20) not null, --TEXT or HTML
		Importance varchar(6) not null,
		Sensitivity varchar(12) not null,
		ReplyTo varchar(max), 
		FromAddress varchar(max),


		RequestDate datetime not null constraint DF_maint_MailItem_RequestDate default(getdate()),
		Attempts int not null constraint DF_maint_MailItem_Attempts default(0),
		LastSentDate datetime null,
		NextSendingDate datetime not null constraint DF_maint_MailItem_nextSendingDate default(getdate()),
		Status varchar(30) constraint DF_maint_MailItem_Status default('Pending'),
		ErrorMessage varchar(max),
		constraint PK_maint_MailItem primary key clustered (MailItemID)
	)
end
go
if not exists(select * from sys.indexes where object_id = object_id('maint.MailItem') and name = 'IDX_maint_MailItem_Status_NextSendingDate')
	create index IDX_maint_MailItem_Status_NextSendingDate on maint.MailItem(Status, NextSendingDate) include(Attempts)