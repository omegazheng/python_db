SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[KYC_REQUESTS]') = object_id and name = 'KYC_SYSTEM')
  BEGIN
    ALTER TABLE [admin_all].[KYC_REQUESTS]
      ADD [KYC_SYSTEM] NVARCHAR(50) NULL
  END
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[KYC_REQUESTS]') = object_id and name = 'KYC_REQUEST')
  BEGIN
    ALTER TABLE [admin_all].[KYC_REQUESTS]
      ADD [KYC_REQUEST] NVARCHAR(2000) NULL
  END
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[KYC_REQUESTS]') = object_id and name = 'KYC_RESPONSE')
  BEGIN
    ALTER TABLE [admin_all].[KYC_REQUESTS]
      ADD [KYC_RESPONSE] NVARCHAR(max) NULL
  END
GO

