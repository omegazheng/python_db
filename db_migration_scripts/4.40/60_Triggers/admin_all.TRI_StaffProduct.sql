SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.TRI_StaffProduct') is null
begin
	exec('create trigger [admin_all].[TRI_StaffProduct] on [admin_all].[StaffProduct] 
instead of insert, update
as 
return
')
end
GO
ALTER trigger [admin_all].[TRI_StaffProduct] on [admin_all].[StaffProduct]
AFTER INSERT
as 
begin
	if @@rowcount = 0
		return
	if exists(
				select * 
				from admin_all.StaffCompany sc
					inner join inserted i on i.StaffID = sc.StaffID
				)
	begin
		raiserror('A company associated staff can not be set as a product associated staff.', 16, 1)
		rollback
	end
end