SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[USER_REVIEW]') AND type in (N'U'))
  BEGIN
    CREATE TABLE [admin_all].[USER_REVIEW](
      [ID] [int] IDENTITY(1,1) NOT NULL,
      [PARTYID] [int] NOT NULL,
      [STATUS] nvarchar(15) NOT NULL,
      [REQUEST_DATE] [datetime] NULL,
      [SCHEDULE_DATE] [datetime] NULL,
      [COMPLETE_DATE] [datetime] NULL,
      [REQUEST_COMMENT] nvarchar(max) NULL,
      [REVIEW_COMMENT] nvarchar(max) NULL,
      [REQUEST_STAFFID] [int] NULL,
      [REVIEW_STAFFID] [int] NULL,
     CONSTRAINT [PK_admin_all_USER_REVIEW] PRIMARY KEY CLUSTERED
    (
      [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
  END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_USER_REVIEW_external_mpt_USER_CONF_PARTYID]') AND parent_object_id = OBJECT_ID(N'[admin_all].[USER_REVIEW]'))
  ALTER TABLE [admin_all].[USER_REVIEW]  WITH CHECK ADD  CONSTRAINT [FK_admin_all_USER_REVIEW_external_mpt_USER_CONF_PARTYID] FOREIGN KEY([PARTYID])
  REFERENCES [external_mpt].[USER_CONF] ([PARTYID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_USER_REVIEW_REQUEST_STAFFID_STAFF_AUTH_TBL_STAFFID]') AND parent_object_id = OBJECT_ID(N'[admin_all].[USER_REVIEW]'))
  ALTER TABLE [admin_all].[USER_REVIEW]  WITH CHECK ADD  CONSTRAINT [FK_admin_all_USER_REVIEW_REQUEST_STAFFID_STAFF_AUTH_TBL_STAFFID] FOREIGN KEY([REQUEST_STAFFID])
  REFERENCES [admin_all].[STAFF_AUTH_TBL] ([STAFFID])
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_admin_all_USER_REVIEW_REVIEW_STAFFID_STAFF_AUTH_TBL_STAFFID]') AND parent_object_id = OBJECT_ID(N'[admin_all].[USER_REVIEW]'))
  ALTER TABLE [admin_all].[USER_REVIEW]  WITH CHECK ADD  CONSTRAINT [FK_admin_all_USER_REVIEW_REVIEW_STAFFID_STAFF_AUTH_TBL_STAFFID] FOREIGN KEY([REVIEW_STAFFID])
  REFERENCES [admin_all].[STAFF_AUTH_TBL] ([STAFFID])
GO

if not exists(select * from sys.indexes where name ='IDX_admin_all_USER_REVIEW_PARTYID' and object_id=object_id('admin_all.USER_REVIEW'))
create index IDX_admin_all_USER_REVIEW_PARTYID on admin_all.USER_REVIEW(PARTYID)
go

if not exists(select * from sys.indexes where name ='IDX_admin_all_USER_REVIEW_REQUEST_STAFFID' and object_id=object_id('admin_all.USER_REVIEW'))
create index IDX_admin_all_USER_REVIEW_REQUEST_STAFFID on admin_all.USER_REVIEW(REQUEST_STAFFID)
go

if not exists(select * from sys.indexes where name ='IDX_admin_all_USER_REVIEW_REVIEW_STAFFID' and object_id=object_id('admin_all.USER_REVIEW'))
create index IDX_admin_all_USER_REVIEW_REVIEW_STAFFID on admin_all.USER_REVIEW(REVIEW_STAFFID)
go