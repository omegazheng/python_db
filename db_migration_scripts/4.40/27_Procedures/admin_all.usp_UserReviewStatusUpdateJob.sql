set ansi_nulls on
go
set quoted_identifier on
go
if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'admin_all.usp_UserReviewStatusUpdateJob')
      AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure admin_all.usp_UserReviewStatusUpdateJob
go

/**
Update User Review Status from "SCHEDULED" to "DUE" when the schedule date is due.
*/
CREATE procedure admin_all.usp_UserReviewStatusUpdateJob
as
begin
    update admin_all.USER_REVIEW set
    STATUS = 'DUE'
    where STATUS = 'SCHEDULED'
    and SCHEDULE_DATE <= GETDATE();
end

-- exec admin_all.usp_UserReviewStatusUpdateJob

