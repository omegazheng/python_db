SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
    SELECT *
    FROM DBO.SYSOBJECTS
    WHERE ID =
          OBJECT_ID(N'[admin_all].[usp_insertSportsbookBetDetails]'))
  DROP procedure [admin_all].[usp_insertSportsbookBetDetails]
GO

CREATE PROCEDURE admin_all.usp_insertSportsbookBetDetails
(
    @Currency VARCHAR (3),
    @Amount NUMERIC (38, 18),
    @Type VARCHAR (20),
    @WagerId nvarchar(50),
    @PlatformCode nvarchar(20),

    @Status VARCHAR (100),
    @CreatedTimestamp DATETIME,
    @SettledTimestamp DATETIME,
    @PotentialWin NUMERIC (38, 18),
    @TotalOdds NUMERIC (38, 18),
    @Cashout bit,
    @Msg nvarchar(1024),
    @BetslipId nvarchar(50),
    @BetDetailId	int = null output
)
AS

BEGIN

    insert into admin_all.SPORTSBOOK_BET_DETAILS
      (CURRENCY, AMOUNT, TYPE, WAGER_ID, PLATFORM_CODE,
       STATUS, CREATED_TIMESTAMP, SETTLED_TIMESTAMP, POTENTIAL_WIN, TOTAL_ODDS,
       CASH_OUT, MSG, BETSLIP_ID)
    values
      (@Currency, @Amount, @Type, @WagerId, @PlatformCode,
       @Status, @CreatedTimestamp, @SettledTimestamp, @PotentialWin, @TotalOdds,
       @Cashout, @Msg, @BetslipId)

    select @BetDetailId = @@IDENTITY
END