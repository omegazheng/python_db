SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
    SELECT *
    FROM DBO.SYSOBJECTS
    WHERE ID =
          OBJECT_ID(N'[admin_all].[usp_GetPlayersByStaffAndGivenFieldSummary]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  DROP PROCEDURE [admin_all].[usp_GetPlayersByStaffAndGivenFieldSummary]
GO

CREATE PROCEDURE [admin_all].[usp_GetPlayersByStaffAndGivenFieldSummary]
(
    @staffid INT,
    @field   VARCHAR(20),
    @operatorId INT,
    @value NVARCHAR(50)
)
AS
BEGIN
    DECLARE @sql NVARCHAR(max);
    DECLARE @condition NVARCHAR(255);
    DECLARE @searchBy NVARCHAR(255);
    DECLARE @hideME bit;
    DECLARE @mobileColumn NVARCHAR(255);
    DECLARE @emailColumn NVARCHAR(255);

    SELECT @hideME = admin_all.fn_GetRegistry('hide_mobile_email')
    print @hideME
    IF @hideME = 1
        BEGIN
            select @mobileColumn = 'null'
            select @emailColumn = 'null'
        END
    ELSE
        BEGIN
            select @mobileColumn = 'mobile_phone'
            select @emailColumn = 'email'
        END

    IF @operatorId = 1
        SET @condition = ' like '''' + @value + N''%'''
    IF @operatorId = 2
        SET @condition = ' = '''' + @value + '''''
    IF @operatorId = 3
        SET @condition = ' like N''%'' + @value + N''%'''

    IF @field = 'ip'
        SET @searchBy = 'ull.'
    ELSE
        SET @searchBy = 'u.'

    SET @sql = N'
    select count(*) as totalRecord from (
    select
        u.partyid,
        userid,
        nickname,
        first_name + '' '' + last_name as name,
        city + '', '' + u.country + '' '' as location,
        phone,
        '+@mobileColumn +' as mobile_phone,
        '+@searchBy +'ip as ip,
        '+@emailColumn +' as email,
        brand.brandname as brand,
        vip_status as vipLevel
    from external_mpt.user_conf u
        join admin.casino_brand_def brand on brand.brandid = u.brandid
        left join admin_all.USER_LOGIN_LOG ull on u.PARTYID = ull.partyid
        left join external_mpt.UserAssociatedAccount ua on u.partyid = ua.AssociatedPartyID
    where
        u.brandid in (select brandid from [admin_all].[staff_brand_tbl] where staffid = @staffid)
        and '+@searchBy + @field + @condition + 'and (ua.IsPrimary is NULL or ua.IsPrimary = 1)
    group by
        u.partyid,
        userid,
        nickname,
        first_name + '' '' + last_name,
        city + '', '' + u.country + '' '',
        phone,
        mobile_phone,
        '+@searchBy +'ip,
        email,
        brand.brandname,
        vip_status) t';
    EXEC sp_executesql @sql,
         N'@staffid int, @value nvarchar(500)',
         @staffid = @staffid, @value = @value

END
go