
set ansi_nulls, quoted_identifier on
if object_id('admin_all.usp_GetStaffProduct') is null
    begin
        exec ('create procedure admin_all.usp_GetStaffProduct as')
    end
go
alter procedure admin_all.usp_GetStaffProduct
    (
        @StaffID int
    )
    as
    begin

        select StaffID, ProductID from admin_all.StaffProduct where StaffID = @StaffID
    END


--     exec usp_GetStaffProduct 7664
