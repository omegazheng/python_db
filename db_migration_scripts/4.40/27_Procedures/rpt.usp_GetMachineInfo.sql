set ansi_nulls, quoted_identifier on
if object_id('rpt.usp_GetMachineInfo') is null
  begin
    exec ('create procedure rpt.usp_GetMachineInfo as')
  end
go
alter procedure rpt.usp_GetMachineInfo
  (
    @MachineID int
  )
as
  begin
    select m.*, mm.Code as Manufacturer, c.CompanyID, c.Name as CompanyName, p.Name as ProductName, l.Name as LocationName
    from admin_all.Machine m
           join admin_all.MachineManufacturer mm on m.ManufacturerID = mm.ManufacturerID
           join admin_all.Location l on l.LocationID = m.LocationID
           join admin_all.PLATFORM p on p.id = m.ProductID
           join admin_all.Company c on c.CompanyID = l.CompanyID
    where m.MachineID = @MachineID
  end
