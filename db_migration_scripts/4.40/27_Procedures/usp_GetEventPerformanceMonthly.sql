set ansi_nulls, quoted_identifier on
go
if object_id('rpt.usp_GetEventPerformanceMonthly') is null
    begin
        exec('create procedure rpt.usp_GetEventPerformanceMonthly as --')
    end
go
alter procedure rpt.usp_GetEventPerformanceMonthly
    (
        @StaffID int,
        @DateFrom datetime,
        @DateTo datetime,
        @CompanyID int = null
        )
    as
    begin
        set nocount on
        select @DateFrom = cast(@DateFrom as date), @DateTo = dateadd(day, 1, cast(@DateTo as date))
        declare @StaffType varchar(10) = admin_all.fn_StaffType(@StaffID)
        declare @ComID int = (select sc.CompanyID
                              from admin_all.StaffCompany sc
                              where sc.StaffID = @StaffID)
        declare @SQL nvarchar(max)
        select @SQL = '
		select  c.CompanyID, c.Name as CompanyName, m.ProductID, p.Name as ProductName, DateTime = DATEADD(MONTH, DATEDIFF(MONTH, 0, me.DateTime), 0),
--         sum(me.AmountReal) AmountReal, sum(me.PlayableBonus) PlayableBonus, sum(me.GamesPlayed) GamesPlayed,
--         sum(me.TotalCreditWin) TotalCreditWin, sum(me.GameWin) GameWin, sum(me.AmountTransferIn) AmountTransferIn,
--         sum(me.AmountTransferOut) AmountTransferOut, sum(me.BonusTransferIn) BonusTransferIn,
--         sum(me.BonusTransferOut) BonusTransferOut, sum(me.LoyaltyPoint) LoyaltyPoint,
        Count(me.EventKey) as EventCount,
        Count(*) Count
		from admin_all.MachineEvent me
       inner join admin_all.Machine m on m.MachineID = me.MachineID
       left join admin_all.Location l on l.LocationID = m.LocationID
       left join admin_all.PLATFORM p on p.id = m.ProductID
       left join admin_all.Company c on c.CompanyID = l.CompanyID
		where me.Datetime >= @DateFrom and me.Datetime < @DateTo
 			'+case when @StaffType = 'Company' then ' and
 					c.CompanyID in (select f.CompanyID from admin_all.fn_ChildrenCompany(' + cast(@ComID as varchar(20)) + ') f
					)'
                    when @StaffType = 'Product' then '
 			and exists(
 					select *
 					from admin_all.StaffProduct sp
 					where sp.StaffID = @StaffID
 						and sp.ProductID = m.ProductID

 					)'
                    else ''
            end+'

		group by c.CompanyID, c.Name, m.ProductID, p.Name, me.EventKey, DATEADD(MONTH, DATEDIFF(MONTH, 0, me.DateTime), 0)'
        exec sp_executesql @SQL, N'@StaffID int, @DateFrom datetime, @DateTo datetime, @CompanyID int', @StaffID, @DateFrom, @DateTo, @CompanyID

    end
go
-- exec rpt.usp_GetEventPerformanceMonthly 1, '2018-01-01', '2018-02-01'
