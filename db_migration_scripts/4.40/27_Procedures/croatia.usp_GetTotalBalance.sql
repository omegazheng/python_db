set ansi_nulls on
go
set quoted_identifier on
go
if exists(
        select *
        from dbo.sysobjects
        where id =
              object_id(N'croatia.usp_GetTotalBalance')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
    drop procedure croatia.usp_GetTotalBalance
go

if exists(select * from sys.schemas where name = 'croatia')
    begin
        exec('CREATE procedure croatia.usp_GetTotalBalance
    as
    begin
        set nocount on
        select
            u.CURRENCY,
            sum(a.BALANCE_REAL) BalanceReal,
            sum(a.PLAYABLE_BONUS) PlayableBonus,
            sum(a.RELEASED_BONUS) ReleasedBonus,
            sum(a.RAW_LOYALTY_POINTS) RawLoyaltyPoints,
            sum(a.SECONDARY_BALANCE) SecondaryBalance,
            sum(a.UNPAID_CIT) UnpaidCIT
        from admin_all.ACCOUNT a
            inner join external_mpt.USER_CONF u on a.PARTYID = u.PARTYID
        group by u.CURRENCY
        order by 1
    end')
    end
go