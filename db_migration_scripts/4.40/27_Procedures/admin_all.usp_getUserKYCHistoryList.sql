IF EXISTS(
    SELECT *
    FROM DBO.SYSOBJECTS
    WHERE ID =
          OBJECT_ID(N'[admin_all].[usp_getUserKYCHistoryList]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  DROP PROCEDURE [admin_all].[usp_getUserKYCHistoryList]
GO

CREATE PROCEDURE [admin_all].[usp_getUserKYCHistoryList]
  (
    @partyId INT,
    @startDate DATETIME,
    @endDate DATETIME
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate)

    select KYC_HISTORY.PARTYID, KYC_HISTORY.date, RESULT, KYC_HISTORY.COMMENT, KYC_HISTORY.type, KYC_REQUESTS.KYC_SYSTEM,
       RESPONSE_XML, KYC_HISTORY.STAFFID, LOGINNAME from KYC_REQUESTS left join KYC_HISTORY on KYC_REQUEST_ID = KYC_REQUESTS.ID
           left join STAFF_AUTH_TBL on KYC_HISTORY.STAFFID=STAFF_AUTH_TBL.STAFFID
    where
            KYC_HISTORY.PARTYID=@partyId
      and KYC_HISTORY.date BETWEEN @startDateLocal and @endDateLocal

    ORDER BY KYC_HISTORY.date DESC
  END