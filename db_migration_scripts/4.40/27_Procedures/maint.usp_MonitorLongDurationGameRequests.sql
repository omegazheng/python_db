
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('maint.usp_MonitorLongDurationGameRequests') is null
	exec('create procedure maint.usp_MonitorLongDurationGameRequests as --')
go
alter procedure [maint].[usp_MonitorLongDurationGameRequests]
as
begin
	set nocount on 
	declare @DateFrom datetime , @DateTo datetime, @Count int, @Max int, @Avg int, @Over10 int
	declare @Body varchar(max)
	select @DateTo =max(RequestTime)
	from logs.GameRequestHistory 
	select @DateFrom = dateadd(second, -65, @DateTo)
	;with x0 as
	(
	select datediff(millisecond, RequestTime,  ResponseTime) Duration
	from logs.GameRequestHistory 
	where RequestTime > @DateFrom and RequestTime <=@DateTo
	)

	select @Count = count(*),  @Max = Max(Duration), @Avg = avg(Duration), @Over10 = count(case when Duration > 10000 then 1 end)
	from x0
	if @Over10 > 10
	begin
		select @Body = 'between ' + convert(varchar(100), @DateFrom, 120) + ' and ' + convert(varchar(100), @DateFrom, 120) + '  
	Total:' + cast(@Count as varchar(20)) + ' Requests; Max Duration:' + cast(@Count as varchar(20)) + ' millisecond; Average:' + cast(@Avg as varchar(20)) + '
	Over 10 second: ' + cast(@Over10 as int) + ' requests'
		exec maint.usp_SendAlert @Subject = '10 long duration game requests.', @Body = @Body, @TestMode = 0
	end
	
end
