import glob
import json
import re
import textwrap

import pyodbc
import pytest
from termcolor import colored
from db_utils import Context
from connection_utils import get_master_connection

from db_utils import (exec_create_database, exec_drop_database,
                      execute_sql_file_using_cnxn,
                      exists, exists_db, get_database_connection,
                      get_database_connection_for_log,
                      check_database_version, execute_sql_statement,
                      create_database_update_log, start_log, end_log,
                      start_deployment, batch_id)


def test_database_exists(execution_cnxn, database):
    cursor = execution_cnxn.cursor()
    exists = exists_db(cursor, database)
    assert exists


def test_create_and_drop_database():
    exec_create_database("Sample")
    with get_master_connection() as master:
        cursor = master.cursor()
        assert exists_db(cursor, "Sample") == True
        exec_drop_database("Sample")
        assert exists_db(cursor, "Sample") == False


def test_create_procedure_success(cursor):
    stored_proc = '''
    create or alter procedure dbo.usp_test
    as
    select 1
    '''
    cursor.execute(stored_proc)
    assert exists(cursor, 'dbo.usp_test')
    output = cursor.procedures(schema='dbo', procedure='usp_test').fetchone()
    assert output is not None


@pytest.mark.skip(reason="cursor.messages() under proposal ONLY https://www.python.org/dev/peps/pep-0249/#cursor-messages")
def test_sql_print_can_not_be_retrieved_from_cursor(cursor):
    stmt = 'print 1'
    result = cursor.execute(stmt).messages()
    assert result is not None


def test_sql_raise_error_been_reported_in_stored_proc(cursor):
    with pytest.raises(pyodbc.ProgrammingError):
        stored_proc = '''    
        create or alter procedure dbo.usp_RaiseError 
        as
        begin
            set nocount on 
            select getdate()
            select @@version
            raiserror ('custom error message', 16, 1)
        end     
        '''
        cursor.execute(stored_proc)

        if cursor.procedures(procedure='dbo.usp_RaiseError').fetchone():
            print('Procedures {} has been created successfully!'.format(
                'dbo.usp_RaiseError'))

        output = cursor.execute("{CALL dbo.usp_RaiseError}")
        print('Oputput from Select Statement is {}'.format(
            colored(output.fetchval(), 'green')))
        while cursor.nextset():
            rows = cursor.fetchall()
            for row in rows:
                print(colored(row, 'yellow'))


def test_execute_sql_file(context):
    files = glob.glob(
        '**/4.30/000 CreateSchema/02 CreateSchemas.sql', recursive=True)
    context.file_name = files[0]
    execute_sql_file_using_cnxn(context)


def test_database_connection_for_log_is_different_from_execution_connection(database):
    log_cnxn = get_database_connection_for_log(database)
    assert log_cnxn is not None
    execution_connection = get_database_connection(database)
    assert log_cnxn != execution_connection


def test_execute_sql_file_access_time_table(context):
    files = glob.glob(
        '**/admin_all.ACCESS_TIME.Table.sql', recursive=True)
    context.file_name = files[0]
    execute_sql_file_using_cnxn(context)


def test_execute_sql_file_column_contains_go(context):
    files = glob.glob(
        '**/4.30/030 Tables/SafePlay.SafePlayMachineInfo.Table.sql', recursive=True)
    context.file_name = files[0]
    execute_sql_file_using_cnxn(context)


def test_execute_sql_file_contains_curly_braces(context):
    files = glob.glob(
        '**/4.30/090 Procedures/admin_all.usp_DataInitializationAuthorization.sql', recursive=True)
    context.file_name = files[0]
    execute_sql_file_using_cnxn(context)


def test_execute_sql_file_CheckPreDeploymentJob(context):
    files = glob.glob(
        '**/PreScripts/050 PreDeploymentCheck/99 CheckPreDeploymentJob.sql', recursive=True)
    context.file_name = files[0]
    execute_sql_file_using_cnxn(context)


def test_check_database_version(context):
    result = check_database_version(context)
    assert result is not None


def test_start_deployment(log_cnxn, execution_cnxn):
    cursor = log_cnxn.cursor()
    id = start_deployment(cursor)
    assert id > 0


def test_create_database_update_log(log_cnxn):
    cursor = log_cnxn.cursor()

    cursor.execute("select count(*) from dbo.DatabaseUpdateLog")
    count = cursor.fetchval()

    batch_id = start_deployment(log_cnxn)
    path = 'path'
    version = '4.35'
    file_name = 'file_name'
    message = 'message'
    is_error_message = 0
    new_batch_id, batch_log_id = create_database_update_log(
        log_cnxn, path, version, file_name, batch_id, -1, message, is_error_message)

    assert new_batch_id == batch_id
    assert batch_log_id > 0
    cursor.execute("select count(*) from dbo.DatabaseUpdateLog")
    new_count = cursor.fetchval()
    assert new_count > count


def test_start_log_end_log(log_cnxn):
    batch_id = start_deployment(log_cnxn)
    cursor = log_cnxn.cursor()
    short_message_start = "hi"
    short_message_end = "over"
    batch_id, batch_log_id = start_log(log_cnxn, version=4.60, path='test',
                                       file_name='abc', batch_id=batch_id, batch_log_id=-1, short_message=short_message_start)
    cursor.execute(
        "select top 1 * from dbo.DatabaseUpdateLog order by BatchID desc, BatchLogID desc")
    result = cursor.fetchone()
    batch_log_id = result.BatchLogID
    assert result.Message == short_message_start

    end_log(log_cnxn, batch_id=batch_id, batch_log_id=batch_log_id,
            message=short_message_end, is_error_message=0)
    cursor.execute(
        "select top 1 * from dbo.DatabaseUpdateLog where BatchID = {BatchID} and BatchLogID = {BatchLogID}".
        format(BatchID=batch_id, BatchLogID=batch_log_id))
    result = cursor.fetchone()
    assert result.Message == (short_message_end + " | " + short_message_start)
    assert result.IsErrorMessage == 0
