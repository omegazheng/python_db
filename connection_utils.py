
from enum import Enum
from contextlib import contextmanager
from connection_settings import load_conn_settings
import pyodbc


class Context(object):
    path = 'N/A'
    file_name = 'N/A'
    log_enabled = False
    batch_id = -1
    batch_log_id = -1
    log_cnxn = None
    debug_enabled = False
    replace_global_variables = False

    def __init__(self, database, data_path=None, log_path=None, batch_id=-1, version="0"):
        self.database = database
        self.data_path = data_path
        self.log_path = log_path
        self.version = version
        self.batch_id = batch_id


class DBObjectType(Enum):
    DATABASE = 1,
    TABLE = 2,
    PROC = 3,
    FUNCTION = 4


class DBObject(object):
    def __init__(self, type, schema, name):
        self.type = type
        self.schema = schema
        self.name = name


TABLES = ["DatabaseVersion", "DatabaseUpdateBatch", "DatabaseUpdateLog"]
FUNCTIONS = ['GetVersion']
PROC = ['IsScriptDeployable', 'StartDeployment', 'CreateDatabaseUpdateLog']


DBO_OBJECTS = {
    "schema": "dbo",
    "objects": [
        {
            "type": DBObjectType.TABLE,
            "items": TABLES
        },
        {
            "type": DBObjectType.PROC,
            "items": PROC
        },
        {
            "type": DBObjectType.FUNCTION,
            "items": FUNCTIONS
        },

    ]
}


def get_master_connection():
    return get_database_connection('master')


@ contextmanager
def get_database_connection(database_name=None):
    connection_settings = load_conn_settings(database_name)
    cnxn = pyodbc.connect('DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'.format(
        connection_settings['driver'],
        connection_settings['server'],
        connection_settings['database'],
        connection_settings['username'],
        connection_settings['password']),
        autocommit=True)
    yield cnxn


@ contextmanager
def get_database_connection_for_log(database_name):
    connection_settings = load_conn_settings(database_name)
    cnxn = pyodbc.connect('DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'.format(
        connection_settings['driver'],
        connection_settings['server'],
        database_name,
        connection_settings['username'],
        connection_settings['password']),
        autocommit=True)
    yield cnxn
