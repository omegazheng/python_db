import textwrap
import sys
import pyodbc
import os
from termcolor import colored

import sql_file_utils
import string_utils
from connection_utils import (DBO_OBJECTS, Context, DBObject, DBObjectType,
                              get_database_connection,
                              get_database_connection_for_log,
                              get_master_connection)
from file_utils import get_sql_file, is_sql_file, list_dir_that_meets_version_requirements
from log_utils import STATUS_ERROR, STATUS_OK, print_status
from sql_file_utils import read_sql_statements

CREATE_DATABASE_SQL_FILE = 'CreateDatabase.sql'
DEPLOY_OBJECTS_SQL_FILE = 'DeployObjects.sql'
batch_id = 0
MAX_CHARS = 1000
STATUS_SUCCESS = "OK"
STATUS_FAILED = "FAILED"


class SafeDict(dict):
    def __missing__(self, key):
        return '{' + key + '}'


def default_path(cnxn):
    cursor = cnxn.cursor()
    stmt = textwrap.dedent('''
    SELECT
        CAST(SERVERPROPERTY('InstanceDefaultDataPath') AS nvarchar(2000)) AS DefaultDataPath,
        CAST(SERVERPROPERTY('InstanceDefaultLogPath') AS  nvarchar(2000)) AS DefaultLogPath
    ''')
    cursor.execute(stmt)
    path = cursor.fetchone()
    return path


def exists(cursor, name):
    stmt = ''' select object_name(object_id('{ObjectName}'))'''
    statement = stmt.format(ObjectName=name)
    cursor.execute(statement)
    name = cursor.fetchval()
    return name != None


def exists_db(cursor, database):
    stmt = '''SELECT name from sys.databases where name = '{DatabaseName}' '''
    statement = stmt.format(DatabaseName=database)
    cursor.execute(statement)
    db = cursor.fetchval()
    return database == db


def __drop_database(master, database):
    cursor = master.cursor()
    if exists_db(cursor, database):
        print('Dropping Database %s ...' % database)
        sql_drop = (
            "DECLARE @sql AS NVARCHAR(MAX);"
            "SET @sql = 'DROP DATABASE ' + QUOTENAME(?);"
            "EXEC sp_executesql @sql"
        )
        cursor.execute(sql_drop, database)
        cursor.commit()
    else:
        print('Database %s Not Found!' % database)


def exec_drop_database(database):
    with get_master_connection() as master_connection:
        # Update context using master connection
        __drop_database(master_connection, database)
        master_connection.commit()


def exec_drop_database_if_exists(database):
    with get_master_connection() as master_connection:
        # Update context using master connection
        context = Context(database=database)
        path = default_path(master_connection)
        context.data_path = path.DefaultDataPath
        context.log_path = path.DefaultLogPath
        context.cnxn = master_connection
        context.log_enabled = False
        context.replace_global_variables = True
        __drop_database(master_connection, context.database)
        context.replace_global_variables = False
        context.log_enabled = True


def exec_create_database(database):
    with get_master_connection() as master_connection:
        # Update context using master connection
        context = Context(database=database)
        path = default_path(master_connection)
        context.data_path = path.DefaultDataPath
        context.log_path = path.DefaultLogPath
        context.cnxn = master_connection
        context.log_enabled = False
        context.replace_global_variables = True
        __create_database(context)
        context.replace_global_variables = False
        context.log_enabled = True


def __create_database(context):
    cursor = context.cnxn.cursor()
    try:
        context.file_name = CREATE_DATABASE_SQL_FILE
        context.path = 'N/A'
        context.log_enabled = False
        execute_sql_file_using_cnxn(context)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    if (exists_db(cursor, context.database)):
        print('{} is available now! {}'.format(context.database, STATUS_OK))


def exec_deploy_objects(database):
    with get_database_connection(database) as cnxn:
        deploy_objects(cnxn)


def deploy_objects(context):
    context.log_enabled = False
    context.file_name = DEPLOY_OBJECTS_SQL_FILE
    execute_sql_file_using_cnxn(context)
    validate_deployed_objects(context)
    context.log_enabled = True


def validate_deployed_objects(context):
    for object_list in DBO_OBJECTS["objects"]:
        schema = DBO_OBJECTS['schema']
        type = object_list["type"]
        for item in object_list['items']:
            object = DBObject(type=type, schema=schema, name=item)
            cursor = context.cnxn.cursor()
            exist = exists(cursor, object.name)
            print_status(item, exist)


def execute_sql_file(context):
    with get_database_connection(context.database) as cnxn, get_database_connection(context.database) as log_cnxn:
        context.cnxn = cnxn
        context.log_cnxn = log_cnxn
        print(colored(context.file_name, 'yellow'))
        execute_sql_file_using_cnxn(context)


def execute_sql_file_using_cnxn(context):
    '''GO is a batch terminator in SQL Command
        It doesn't make any sense in pyodbc. Instead, issue separate commands from your script.'''
    cursor = context.cnxn.cursor()
    statements = read_sql_statements(context.file_name)
    try:
        execute_sql_statements(
            cursor, statements, context)
    except Exception as ex:
        print_status(ex, STATUS_ERROR)
        raise


def execute_sql_statements(cursor, statements, context):
    database = None if context is None else context.database
    data_path = None if context is None else context.data_path
    log_path = None if context is None else context.log_path
    log_cnxn = context.log_cnxn

    for stmt in statements:
        try:
            statement = stmt
            if (context.replace_global_variables):
                stmt = string_utils.convert_sql_variables(stmt)
                statement = stmt.format_map(SafeDict(DatabaseName=database,
                                                     DataPath=data_path,
                                                     LogPath=log_path))
            if (context.log_enabled):
                message = string_utils.escape_sql_string(statement[:MAX_CHARS])
                context.batch_id, context.batch_log_id = start_log(log_cnxn, context.current_version, context.path, context.file_name,
                                                                   context.batch_id, batch_log_id=-1, short_message=message)
            no_count_statement = '''SET ANSI_NULLS ON                
                            SET QUOTED_IDENTIFIER ON                
                            SET NOCOUNT ON '''
            cursor.execute(no_count_statement)
            # if not statement.startswith("create"):
            #     statement = no_count_statement + statement
            cursor.execute(statement)
            if (context.log_enabled):
                end_log(log_cnxn=log_cnxn, batch_id=context.batch_id,
                        batch_log_id=context.batch_log_id, message=STATUS_SUCCESS, is_error_message=0)

            if (context.debug_enabled):
                print('%s %s' % (statement, STATUS_OK))

            while cursor.nextset():
                rows = cursor.fetchall()
                for row in rows:
                    print(colored(row, 'yellow'))

        except Exception as ex:
            print_status(statement, STATUS_ERROR)
            print_status(ex, STATUS_ERROR)
            if (context.log_enabled):
                end_log(log_cnxn=log_cnxn, batch_id=context.batch_id,
                        batch_log_id=context.batch_log_id, message=STATUS_FAILED, is_error_message=1)
            raise


def check_database_version(context):
    with get_database_connection(context.database) as cnxn, get_database_connection(context.database) as log_cnxn:
        context.cnxn = cnxn
        context.log_cnxn = log_cnxn
        context.current_version = context.version
        execute_sql_file_using_cnxn(context)
        prepare_sql = '''
        declare @DatabaseVersion varchar(100)
        select @DatabaseVersion = '0'
        if object_id('dbo.DatabaseVersion') is null
        begin
            create table dbo.DatabaseVersion
            (
                Version varchar(100) not null,
                Date datetime not null constraint DF_dbo_DatabaseVersion_Date default(GetUTCDate())
            )
        end
        GO

        if not exists(select * from dbo.DatabaseVersion)
        begin
            insert into dbo.DatabaseVersion(Version)
                values(@DatabaseVersion)
        end
        GO

        if object_id('dbo.DatabaseUpdateBatch') is null
        begin
            create table dbo.DatabaseUpdateBatch
            (
                BatchID bigint not null identity(1,1) constraint PK_dbo_DatabaseUpdateBatch primary key,
                HasError bit not null constraint DF_dbo_DatabaseUpdateBatch_HasError default(0),
                StartDate datetime not null constraint DF_dbo_DatabaseUpdateBatch_Date default(GetUTCDate()),
                EndDate datetime null,
                LoginName varchar(128) not null constraint DF_dbo_DatabaseUpdateBatch_LoginName default(system_user)
            )
        end
        GO

        if object_id('dbo.DatabaseUpdateLog') is null
        begin
            create table dbo.DatabaseUpdateLog
            (

                BatchID bigint not null ,
                BatchLogID bigint not null identity(1,1),
                Version varchar(100) not null,
                Path varchar(max) not null,
                FileName varchar(max) not null,
                Message varchar(max),
                IsErrorMessage bit not null constraint DF_dbo_DatabaseUpdateLog_IsErrorMessage default(0),
                StartDate datetime not null constraint DF_dbo_DatabaseUpdateLog_Date default(GetUTCDate()),
                EndDate datetime null,
                constraint PK_dbo_DatabaseUpdateLog primary key(BatchID, BatchLogID)
            )
        end
        GO

        alter table dbo.DatabaseVersion alter column Version varchar(100)
        GO

        create or alter procedure dbo.CreateDatabaseUpdateLog (@BatchID bigint output, @BatchLogID bigint output, @Version varchar(100), @Path varchar(max), @FileName varchar(max), @Message varchar(max), @IsErrorMessage bit)
        as
        begin
            set nocount on
            update dbo.databaseVersion set Version = @Version, Date = GetUTCDate() where Version <= @Version
            if @BatchID = -1
            begin
                insert into dbo.DatabaseUpdateBatch default values
                    select @BatchID = scope_identity()
            end
            if @BatchLogID = -1
            begin
                insert into dbo.DatabaseUpdateLog(BatchID, Version, Path, FileName)
                    values(@BatchID, @Version, @Path, @FileName)
                select @BatchLogID = scope_identity()
                return
            end
            update dbo.DatabaseUpdateLog
                set Message = @Message + ' | ' + Message,
                    IsErrorMessage = @IsErrorMessage,
                    EndDate = GetUTCDate()
            where BatchID = @BatchID
                and BatchLogID = @BatchLogID

            update dbo.DatabaseUpdateBatch
                set EndDate = GetUTCDate(),
                    HasError =  case when HasError = 1 or @IsErrorMessage = 1 then 1 else 0 end
            where BatchID = @BatchID
            select @BatchLogID = -1
        end
        GO
        '''
        log_cnxn = context.log_cnxn
        cursor = log_cnxn.cursor()
        statements = read_sql_statements(prepare_sql)
        execute_sql_statements(cursor, statements, context)
        sql = "select Version from dbo.DatabaseVersion"
        version = cursor.execute(sql).fetchval()
        if version is None:
            version = "0.0.0"
        log_cnxn.commit()
        return str(version)


def start_log(log_cnxn, version, path, file_name, batch_id, batch_log_id, short_message):
    batch_id, batch_log_id = create_database_update_log(log_cnxn=log_cnxn, version=version, path=path,
                                                        file_name=file_name,
                                                        batch_id=batch_id, batch_log_id=batch_log_id,
                                                        message=short_message, is_error_message=0)
    return batch_id, batch_log_id


def end_log(log_cnxn, batch_id, batch_log_id, message, is_error_message):
    create_database_update_log(log_cnxn=log_cnxn, version=0, path="",
                               file_name="", batch_id=batch_id, batch_log_id=batch_log_id,
                               message=message, is_error_message=is_error_message)
    return batch_id, batch_log_id


def create_database_update_log(log_cnxn, path, version, file_name, batch_id, batch_log_id, message, is_error_message):
    cursor = log_cnxn.cursor()
    check_sql_function_CreateDatabaseUpdateLog(cursor)
    sql = '''exec dbo.CreateDatabaseUpdateLog @BatchID = {BatchID}, @BatchLogID = {BatchLogID}, @Version = '{Version}',
        @Path = '{Path}', @FileName = '{FileName}', @Message = '{Message}', @IsErrorMessage = {IsErrorMessage}'''.format(
        BatchID=batch_id,
        BatchLogID=batch_log_id,
        Path=path,
        Version=version,
        FileName=file_name,
        Message=message,
        IsErrorMessage=is_error_message)
    try:
        # print(sql)
        cursor.execute(sql)
        row = cursor.fetchone()
    except Exception as ex:
        print_status('Failed in executing SQL statement %s, exception \n %s' %
                     (sql, ex), STATUS_ERROR)
        raise
    finally:
        cursor.commit()
    return row.BatchID, row.BatchLogID


def check_sql_function_CreateDatabaseUpdateLog(cursor):
    prepare_sql = '''
    create or alter procedure dbo.CreateDatabaseUpdateLog
	(@BatchID bigint output,
	@BatchLogID bigint output,
	@Version varchar(100),
	@Path varchar(max),
	@FileName varchar(max),
	@Message varchar(max),
	@IsErrorMessage bit)
    as
    begin
        set nocount on
        update dbo.databaseVersion set Version = @Version, Date = GetUTCDate() where Version <= @Version
        if @BatchID = -1
            begin
            insert into dbo.DatabaseUpdateBatch
            default values
            select @BatchID = scope_identity()
        end
        if @BatchLogID = -1
            begin
            insert into dbo.DatabaseUpdateLog
                (BatchID, Version, Path, FileName, Message)
            values(@BatchID, @Version, @Path, @FileName, @Message)
            select @BatchLogID = scope_identity()
            select @BatchID as BatchID, @BatchLogID as BatchLogID
            return
        end
        update dbo.DatabaseUpdateLog
                set Message = @Message + ' | ' + Message ,
                    IsErrorMessage = @IsErrorMessage,
                    EndDate = GetUTCDate()
            where BatchID = @BatchID
            and BatchLogID = @BatchLogID

        update dbo.DatabaseUpdateBatch
                set EndDate = GetUTCDate(),
                    HasError =  case when HasError = 1 or @IsErrorMessage = 1 then 1 else 0 end
            where BatchID = @BatchID


        select @BatchID as BatchID, @BatchLogID as BatchLogID
    end
    '''
    try:
        cursor.execute(prepare_sql)
    except Exception as ex:
        print('Error check_sql_function_CreateDatabaseUpdateLog {}' % ex)


def execute_sql_statement(cursor, statement, debug=True):
    try:
        cursor.execute(statement)
        if (debug):
            print_status(statement, STATUS_OK)
        while cursor.nextset():
            rows = cursor.fetchall()
            for row in rows:
                print(colored(row, 'yellow'))
    except Exception as ex:
        print_status(ex, STATUS_ERROR)
        raise


def start_deployment(cursor):
    global batch_id
    try:
        cursor.execute("exec dbo.StartDeployment")
        batch_id = cursor.execute(
            "select coalesce(max(BatchID),0) from dbo.DatabaseUpdateBatch").fetchval()
        return batch_id
    except Exception as ex:
        print_status(ex, STATUS_ERROR)
        raise


def migrate_db(context):
    deploy_objects(context)
    set_batch_id(context)

    try:
        execute_script_with_variables_replacement(context, "./PreScripts/")
        execute_main_script(context, "./db_migration_scripts")
        # TO Test a Specific Script
        #execute_script(context, "/Users/omegazheng/GitLab/python_db/db_migration_scripts/4.50/90 EmailCleanup/maint.usp_CheckDatabaseMail.sql")
        execute_script_with_variables_replacement(context, "./PostScripts/")
    except Exception as ex:
        print_status(ex, STATUS_ERROR)
        raise


def set_batch_id(context):
    batch_id = start_deployment(context.cnxn)
    context.batch_id = batch_id
    print('BatchID={}'.format(context.batch_id))


def set_database_version(context):
    current_version = check_database_version(context)
    context.current_version = current_version
    print_status('current version {}'.format(
        context.current_version), STATUS_OK)


def execute_main_script(context, main_directory):
    '''
    The main script will check the version folders under the root, and determine the appropriate folders to run script against.
    '''
    set_database_version(context)
    context.log_enabled = True
    directories = list_dir_that_meets_version_requirements(
        root=main_directory, current_version=context.current_version, include_path=True)
    for directory in directories:
        try:
            execute_scripts_for_directory(context, directory)
        except Exception as ex:
            raise ex


def execute_script_with_variables_replacement(context, directory):
    context.replace_global_variables = True
    execute_scripts_for_directory(context, directory)
    context.replace_global_variables = False


def execute_scripts_for_directory(context, directory):
    context.log_enabled = True

    version_info = directory.split("/")[-1]
    context.current_version = version_info

    if not os.path.isdir:
        raise 'directory %s is not a directory' % directory

    files = get_sql_file(directory, context.version)
    for item in files:
        try:
            context.path = directory
            context.file_name = item
            execute_sql_file(context)
        except Exception as err:
            print(colored(err, 'red'))
            raise


def execute_script(context, script):
    context.log_enabled = True
    context.file_name = script
    try:
        path_elements = script.split("/")[:-1]
        context.path = "/".join(path_elements)
        execute_sql_file(context)
    except Exception as err:
        print(colored(err, 'red'))
        raise


if __name__ == '__main__':
    database = 'SampleDB'
    version = '4.10.0'
    # exec_drop_database_if_exists(database)
    with get_master_connection() as master:
        exist = exists_db(master.cursor(), database=database)
        if not exist:
            print('%s no longer exists' % database)

    # exec_create_database(database)
    with get_database_connection() as cnxn, get_database_connection_for_log(database) as log_cnxn:
        path = default_path(cnxn)
        context = Context(database=database,
                          data_path=path.DefaultDataPath, log_path=path.DefaultLogPath, version=version)
        context.cnxn = cnxn
        context.log_cnxn = log_cnxn
        migrate_db(context)
