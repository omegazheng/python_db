import re

REGEX_SQL_VARIABLES = r'\$\((\w+)\)'


def convert_sql_variables(input):
    output = re.sub(REGEX_SQL_VARIABLES, r'{\1}', input)
    return output


def escape_sql_string(sql):
    output = sql.replace("'", "''")
    return output
