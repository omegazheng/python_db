from termcolor import colored

STATUS_OK = colored(u'\u2714', 'green')  # ✔
STATUS_ERROR = colored(u'\u2718', 'red')  # ✘


def print_status(title, status):
    if status == STATUS_OK or status == True:
        print("%s %s" % (title, STATUS_OK))
    else:
        print("%s %s" % (colored(title, 'red'), STATUS_ERROR))
