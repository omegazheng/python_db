import textwrap
import pyodbc
import os
import sys
from termcolor import colored
from packaging import version as ver

data_path = ''
log_path = ''

server_name='.'
database_name='haha'
user_name = '' 
password = ''
database_version = ver.Version('0.0.0')
current_version = ver.Version('0.0.0')
path  = ''
batch_id = -1
batch_log_id = -1
log_connection = None
file = ''

STATUS_OK = colored(u'\u2714', 'green')  # ✔
STATUS_ERROR = colored(u'\u2718', 'red')  # ✘

def print_status(title, status):
    if status == STATUS_OK or status == True:
        print("%s %s" % (title, STATUS_OK))
    else:
        print("%s %s" % (colored(title, 'red'), STATUS_ERROR))

def print_info(str):
    #print_status(str, STATUS_OK)
    print(str)
def print_error(str):
    #print_status(str, STATUS_ERROR)
    print('================================================================================')
    print(str)
    print('================================================================================')




class Connection:
    def __init__(self, server_name, db, user_name, password):
        self.server_name = server_name
        self.user_name = user_name
        self.password = password
        self.ODBCDriver = "{ODBC Driver 17 for SQL Server}"
        if self.user_name:
            self.connection = pyodbc.connect("DRIVER={};SERVER={};UID={};PWD={}".format(self.ODBCDriver, self.server_name, self.user_name, self.password), autocommit=True)
        else:
            self.connection = pyodbc.connect("DRIVER={};SERVER={};Trusted_Connection=yes;".format(self.ODBCDriver, self.server_name), autocommit=True)
        self.database = db
        ##self.execute_one("SET ANSI_NULLS ON;SET QUOTED_IDENTIFIER ON;")
    @property
    def database(self):
        return self.fetch_val("select db_name()")
    @database.setter
    def database(self, db):
        self.connection.execute("use [{}]".format(db))
    def fetch_all(self, sql):
        cursor = self.connection.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        return rows
    def fetch_one(self, sql):
        cursor = self.connection.cursor()
        cursor.execute(sql)
        row = cursor.fetchone()
        return row
    def fetch_val(self, sql):
        cursor = self.connection.cursor()
        cursor.execute(sql)
        return cursor.fetchval()
    def fetch_none(self, sql):
        cursor = self.connection.cursor()
        cursor.execute(sql)
    def fetch_cursor(self, sql):
        cursor = self.connection.cursor()
        cursor.execute(sql)
        return cursor
    def get_cursor(self):
        return self.connection.cursor()
    def close(self):
        self.connection.close()


def get_sql_statements(lines):
    delimitor = 'GO'
    statement = ''
    global database_name, data_path,  log_path
    for line in lines:
        if(delimitor != line.strip().upper()):
            statement += line.replace('$(DatabaseName)', database_name).replace('$(DataPath)', data_path).replace('$(LogPath)', log_path)
        else:
            if statement.strip() != '':
                yield statement
            statement = ''
    if statement.strip() != '':
        yield statement

def initilization():
    global server_name, database_name, user_name, password, database_version, log_path, data_path, path, log_connection
    con = Connection(server_name, 'master', user_name, password)
    row = con.fetch_one(open('Initialization.sql').read().replace('$(DatabaseName)', database_name).replace('$(DataPath)', data_path).replace('$(LogPath)', log_path))
    data_path = row[0]
    log_path = row[1]
    database_version = ver.parse(row[2])
    con.close()
    path = os.path.dirname(os.path.realpath(__file__))
    log_connection = Connection(server_name, database_name, user_name, password)

def list_version():
    global path, database_version
    return sorted(
                    [v for v in 
                        [ver.parse(v) for v in 
                            [f for f in os.listdir(path) if(os.path.isdir(os.path.join(path, f)))]
                        ] if(isinstance(v, ver.Version) and v >= database_version)
                    ]
                )

def list_files(folder):
    for obj in os.walk(folder):
        for f in obj[2]:
            if(f.upper().endswith('.SQL')):
                yield os.path.join(obj[0],f)


def log(query, message=None):
    global log_connection, batch_id, batch_log_id, current_version, file
    cursor = log_connection.get_cursor()
    cursor.execute("exec dbo.CreateDatabaseUpdateLog @BatchID=?, @BatchLogID=?, @Version=?, @FileName=?, @Query=?, @Message=?", batch_id, batch_log_id, str(current_version), file, query, message)
    row = cursor.fetchone()
    batch_id = row[0]
    batch_log_id = row[1]

def begin_log(sql):
    log(sql)
    return

def end_log(msg=None):
    global batch_log_id
    log(None, msg)
    batch_log_id = -1
    return

def run_file(file_name):
    global path, current_version, server_name, database_name, user_name, password, file
    connection = Connection(server_name, database_name, user_name, password)
    file = file_name.replace(path, '.');
    print_info(file)
    for sql in get_sql_statements(open(file_name).readlines()):
        try:
            begin_log(sql)
            cursor = connection.fetch_cursor(sql)
            while cursor.nextset():
                rows = cursor.fetchall()
            end_log(None)
        except Exception as ex:
            end_log(str(ex));
            raise
    if(connection.fetch_val('select @@trancount') > 0):
        raise Exception('There are open transaction after file execution')
    connection.close()
    return


# program starts 
if (not (len(sys.argv) == 3 or len(sys.argv) == 5)):
    print('Please pass proper parameters:')
    print('python install.py server database')
    print('python install.py server database username password')
    sys.exit()

if (len(sys.argv) == 3):
    server_name = sys.argv[1]
    database_name = sys.argv[2]
    user_name = ''
    password = ''

if (len(sys.argv) == 5):
    server_name = sys.argv[1]
    database_name = sys.argv[2]
    user_name = sys.argv[3]
    password = sys.argv[4]

try:
    print_info('Deploying...')
    initilization()
    print_info('Initialization is done...')
    
    ##database_version = ver.Version('4.10.0')

    #pre-deployment
    print_info('Predeploying...')
    for f in sorted(list_files(os.path.join(path,'PreScripts')), key=str.casefold):
        run_file(f)
    print_info('Predeployment...done')
    print_info('Deploying...')
    for v in list_version():
        current_version = v
        for f in sorted(list_files(os.path.join(path,str(v))), key=str.casefold):
            run_file(f)
    print_info('Deployment...done')
    print_info('Post-deploying...')
    #post-deployment
    for f in sorted(list_files(os.path.join(path,'PostScripts')), key=str.casefold):
        run_file(f)
    print_info('Post-deployment...done')
    print_info('Done.')
except Exception as ex:
       print_error(ex)