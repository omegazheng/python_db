set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.fn_ChildrenCompany') is null
  begin
    exec ('create function admin_all.fn_ChildrenCompany () returns table as return SELECT TOP 0 * FROM admin_all.Company')
  end
go
alter function admin_all.fn_ChildrenCompany(@CompanyID int)
  returns table
  as
  return(
  WITH cte AS (SELECT c.CompanyID
               FROM admin_all.Company c
               WHERE c.CompanyID = @CompanyID
               UNION ALL
               SELECT a.CompanyID
               FROM admin_all.Company a
                      JOIN cte ON a.ParentCompanyID = cte.CompanyID)
  SELECT CompanyID
  FROM cte
  );
