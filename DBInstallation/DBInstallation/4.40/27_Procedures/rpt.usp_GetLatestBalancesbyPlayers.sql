set quoted_identifier, ansi_nulls on
if object_id('rpt.usp_GetLatestBalancesbyPlayers') is null
	exec('create procedure rpt.usp_GetLatestBalancesbyPlayers as --')
go
alter procedure rpt.usp_GetLatestBalancesbyPlayers
(
	@AgentID int,
	@DateFrom date,
	@DateTo date
)
as
begin
	DECLARE @endDateLocal DATETIME
	SET @endDateLocal = DATEADD(DD, 1, @DateTo);
	set nocount on
	;with x0 as 
	(
		select u.PARTYID, u.ParentID, u.UserID, u.Currency, u.user_type
		from external_mpt.USER_CONF u
		where ParentID = @AgentID
		union all
		select u1.PARTYID, u1.ParentID, u1.UserID, u1.Currency, u1.user_type
		from external_mpt.USER_CONF u1
			inner join x0 on x0.PARTYID = u1.ParentID
	)
	select	b.DATETIME,
			b.BALANCE_REAL + b.RELEASED_BONUS WithdrawableBalance, b.BALANCE_REAL, b.RELEASED_BONUS, b.PLAYABLE_BONUS,
			x0.USERID, x0.CURRENCY, u.USERID Agent, x0.PARTYID PartyId
	from x0	
		inner join admin_all.ACCOUNT a on a.PARTYID = x0.PARTYID
		left join admin_all.DAILY_BALANCE_HISTORY b on b.ACCOUNT_ID= a.id
		left join external_mpt.USER_CONF u on u.PARTYID = x0.ParentID
	where b.DATETIME >= @DateFrom 
		and b.DATETIME < @endDateLocal
		and isnull(x0.user_type, 0) = 0
end
go
--exec rpt.usp_GetLatestBalancesbyPlayers 91429977, '2018-11-01', '2018-11-30'

--select * from external_mpt.USER_CONF where PARTYID = 91429977
--select * from admin_all.WITHDRAWALS where id = 47
--select * from admin_all.PAYMENT where type = 'withdrawal' order by 1 desc