set ansi_nulls, quoted_identifier on
go
if object_id('maint.usp_SQLServerRequirementCheck') is null
	exec('create procedure maint.usp_SQLServerRequirementCheck as')
go
alter procedure maint.usp_SQLServerRequirementCheck
as
begin
	set nocount on
	declare @SQL nvarchar(max), @DatabaseName sysname, @Collation sysname = 'SQL_Latin1_General_CP1_CI_AS', @v sysname,
		@Major int, @Minor int, @Build int, @Revision int

	print '--Check server collation'
	select @v = cast(ServerProperty('Collation') as sysname)
	if @v <> 'SQL_Latin1_General_CP1_CI_AS'
	begin
		print '!!Server Collation must be ' + @Collation+'!! Current Collation is ' + @v
	end

	print '--Check server version'
	select	@Major = parsename(version, 4),
			@Minor = parsename(version, 3),
			@Build = parsename(version, 2),
			@Revision = parsename(version, 1)
	from (select cast(serverproperty('ProductVersion') as sysname) as version) v
	if cast(ServerProperty('ProductMajorVersion') as int) < 13
	begin 
		print '!!Server must be SQL Server 2016 SP2 and up'
	end
	else if cast(ServerProperty('ProductMajorVersion') as int) = 13
	begin
		if @Minor = 0
		begin
			if @Build < 5026
			begin
				print '!!SP2 is required!!'
			end
		end
	end
	print '--Check database name'
	if db_name() <> 'omega_prod'
	begin
		print '!!Database name must be omega_prod!!'
	end
	print '--Check database file location'
	if exists(select * from sys.database_files where physical_name like 'C:\%')
	begin
		print '!!!None of the database files should be on C: Drive!!'
	end
	print '--Check database file growth'
	if exists(select * from sys.database_files where growth < 25600)
	begin
		print '!!Minimum file growth for data and log files should be greater than 200MB!!'
	end
	print '--Check database properties'
	select @v = cast(databasepropertyex(db_name(), 'Collation') as sysname)
	if @v <> 'SQL_Latin1_General_CP1_CI_AS'
	begin
		print '!!Database collation must be ' + @Collation+'!! Current Collation is ' + @v
	end
	if not exists(select * from sys.databases where name = db_name() and is_read_committed_snapshot_on = 1)
	begin
		print '!!Read Committed Snapshot is not turned on!!'
	end
	if not exists(select * from sys.databases where name = db_name() and delayed_durability_desc = 'FORCED')
	begin
		print '!!Forced Delayed Durability is recommended!!'
	end
	print '--Check permission'
	if not exists(select * from sys.database_principals where name = 'admin_all')
	begin
		print '!!User admin_all count not be found!!'
	end
	
	if isnull(is_rolemember('db_owner', 'admin_all'),0) <>1
	begin
		print'!!User admin_all is not db_owner!!'
	end
	if not exists(
					select * 
					from sys.database_principals dp
						inner join sys.server_principals sp on dp.sid = sp.sid
					where dp.name = 'admin_all'
				)
	begin
		print '!!Could not find login admin_all!!'
	end
end
go
--exec maint.usp_SQLServerRequirementCheck





  