SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
    SELECT *
    FROM DBO.SYSOBJECTS
    WHERE ID =
          OBJECT_ID(N'[agency].[closeAgentGgrs]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  DROP PROCEDURE [agency].[closeAgentGgrs]
GO

CREATE PROCEDURE [agency].[closeAgentGgrs]
  (@agent_id int, @date DATETIME)
AS
  BEGIN
    declare @player_id int
    declare @player_account_id int

    delete from agency.player_ggr where agent_id = @agent_id and date = @date
    delete from agency.direct_ggr where agent_id = @agent_id and date = @date

    insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)
      select
        @agent_id,
        uc.partyid,
        a.platform_id,
        -sum(case
                when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
            end),
        @date,
        -sum(case
                when a.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                else 0
            end),
        -sum(case
                when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                else 0
            end),
        -sum(case
                when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                else 0
            end)
      from admin_all.account_tran as a
        join admin_all.platform as p on a.platform_id = p.id
        left join admin_all.account_tran as r on a.rollback_tran_id = r.id
        join admin_all.account as ac on ac.id = a.account_id
            join external_mpt.user_conf as uc on uc.partyid = ac.partyid
      where a.datetime >= @date and a.datetime < DATEADD(day, 1, @date)
            and a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
            and p.platform_type <> 'SPORTSBOOK'
            and uc.parentid = @agent_id
      group by a.account_id, a.platform_id, uc.partyid


    insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)
        select
            @agent_id,
            uc.partyid,
            a.platform_id,
            -sum(case
                    when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                    when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                end),
            @date,
             -sum(case
                     when a.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                     when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                     else 0
                 end),
             -sum(case
                     when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                     when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                     else 0
                 end),
             -sum(case
                     when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                     else 0
                 end)
        from admin_all.betting_detail as b
            join admin_all.account_tran as a on
                    b.bet_reference_num = a.game_tran_id
                and b.platform_id = a.platform_id
                left join admin_all.account_tran as r on a.rollback_tran_id = r.id
                join admin_all.account as ac on ac.id = a.account_id
                    join external_mpt.user_conf as uc on uc.partyid = ac.partyid
            join admin_all.platform as p on a.platform_id = p.id

        where b.settlement_date >= @date and b.settlement_date < DATEADD(day, 1, @date)
            and a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
            and p.platform_type = 'SPORTSBOOK'
            and uc.parentid = @agent_id
        group by a.account_id, a.platform_id, uc.partyid


    insert into agency.direct_ggr(agent_id, product_id, value, date, wins, bets, released_bonuses)
    select @agent_id, product_id, sum(value), @date, sum(wins), sum(bets), sum(released_bonuses)
    from agency.player_ggr
      where agency.player_ggr.agent_id = @agent_id
        and agency.player_ggr.date = @date
    group by product_id
  END
GO

