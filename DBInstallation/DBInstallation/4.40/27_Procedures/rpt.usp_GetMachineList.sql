set ansi_nulls, quoted_identifier on
go
if object_id('rpt.usp_GetMachineList') is null
  begin
    exec('create procedure rpt.usp_GetMachineList as --')
  end
go
alter procedure rpt.usp_GetMachineList
  (
    @StaffID int
  )
as
  begin
    set nocount on
    declare @StaffType varchar(10) = admin_all.fn_StaffType(@StaffID)
    declare @ComID int = (select sc.CompanyID
                          from admin_all.StaffCompany sc
                          where sc.StaffID = @StaffID)
    declare @SQL nvarchar(max)
    select @SQL = '
		select m.MachineId, m.Datetime, m.IsActive, c.CompanyID, c.Name as CompanyName, m.ProductID,
                  p.Name as ProductName, l.LocationID, l.Name as LocationName, u1.SessionState
    from admin_all.Machine m
        left join admin_all.Location l on l.LocationID = m.LocationID
        left join admin_all.PLATFORM p on p.id = m.ProductID
        left join admin_all.Company c on c.CompanyID = l.CompanyID
        left join admin_all.USER_WEB_SESSION u1 on m.MachineId = u1.MachineId
        left outer join admin_all.USER_WEB_SESSION u2 on (m.MachineId = u2.MachineId AND
    (u1.LAST_ACCESS_TIME < u2.LAST_ACCESS_TIME OR u1.LAST_ACCESS_TIME = u2.LAST_ACCESS_TIME AND u1.ID < u2.ID))
		where m.MachineId is not null AND u2.ID is null
 			'+case when @StaffType = 'Company' then ' and
 					c.CompanyID in (select f.CompanyID from admin_all.fn_ChildrenCompany(' + cast(@ComID as varchar(20)) + ') f
					)'
              when @StaffType = 'Product' then '
 			and exists(
 					select *
 					from admin_all.StaffProduct sp
 					where sp.StaffID = @StaffID
 						and sp.ProductID = m.ProductID

 					)'
              else ''
        end+ '
		'
    exec sp_executesql @SQL, N'@StaffID int', @StaffID

  end
go
-- exec rpt.usp_GetMachineList 1