
set ansi_nulls, quoted_identifier on
if object_id('admin_all.usp_UpdateStaffProduct') is null
    begin
        exec ('create procedure admin_all.usp_UpdateStaffProduct as')
    end
go
alter procedure admin_all.usp_UpdateStaffProduct
    (
        @StaffID int,
        @ProductID int,
        @CoreUserType nvarchar(100)
    )
    as
    begin
        set nocount, xact_abort on
        begin transaction

            delete from admin_all.StaffProduct where StaffID = @StaffID
            delete from admin_all.AUTH_USER_ROLE where USER_ID = @StaffID and USER_TYPE='STAFF' and ROLE_ID=2016

            if @CoreUserType <> 'STAFF'
            begin
                delete from admin_all.AUTH_USER_ROLE where USER_ID = @StaffID and USER_TYPE = 'STAFF'

                insert into admin_all.StaffProduct(StaffID, ProductID) values (@StaffID, @ProductID)

                insert into admin_all.AUTH_USER_ROLE(USER_ID, USER_TYPE, ROLE_ID) values (@StaffID, 'STAFF', 2016)
            end

        COMMIT

        select StaffID, ProductID from admin_all.StaffProduct where StaffID = @StaffID
    END


--     exec usp_UpdateStaffProduct 7664,186, 'PRODUCT_OWNER'
