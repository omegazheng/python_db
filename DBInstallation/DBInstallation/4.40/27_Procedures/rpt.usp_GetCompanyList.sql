set ansi_nulls, quoted_identifier on
go
if object_id('rpt.usp_GetCompanyList') is null
  begin
    exec ('create procedure rpt.usp_GetCompanyList as --')
  end
go
alter procedure rpt.usp_GetCompanyList
  (
    @StaffID int
  )
as
  begin
    set nocount on
    declare @StaffType varchar(10) = admin_all.fn_StaffType(@StaffID)
    if @StaffType = 'Company'
      begin
        declare @CompanyID int = (select sc.CompanyID from admin_all.StaffCompany sc where sc.StaffID = @StaffID)
        select *
        from admin_all.Company c
        where c.CompanyID in (select f.CompanyID from admin_all.fn_ChildrenCompany(@CompanyID) f)
        order by 2, 1
        return
      end
    if @StaffType = 'Product'
      begin
        WITH cteCompany as (select c.CompanyID, c.ParentCompanyID, c.Code, c.Name, c.Description, c.IsActive
                            from admin_all.Company c
                            where exists(
                                    select c.CompanyID, c.ParentCompanyID, c.Code, c.Name, c.Description, c.IsActive
                                    from admin_all.StaffProduct sp
                                           inner join admin_all.Machine m on m.ProductID = sp.ProductID
                                           inner join admin_all.Location l on l.LocationID = m.LocationID
                                    where sp.StaffID = @StaffID
                                      and c.CompanyID = l.CompanyID
                                      )
            UNION ALL

            select cc.CompanyID, cc.ParentCompanyID, cc.Code, cc.Name, cc.Description, cc.IsActive
            from cteCompany cte
                   inner join admin_all.Company cc on cte.ParentCompanyID = cc.CompanyID)

        select *
        from cteCompany
        order by 2, 1
        return
      end
    select * from admin_all.Company c order by 2, 1
  end