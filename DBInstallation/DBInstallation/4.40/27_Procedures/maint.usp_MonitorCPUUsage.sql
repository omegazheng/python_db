
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
go
if object_id('maint.usp_MonitorCPUUsage') is null
begin
	exec('create procedure maint.usp_MonitorCPUUsage as --')
end
GO
alter procedure maint.usp_MonitorCPUUsage
as
begin
	set nocount on 
	if @@version like '%Linux%'
		return
	declare @ts_now bigint = (SELECT cpu_ticks/(cpu_ticks/ms_ticks)FROM sys.dm_os_sys_info); 
	declare @c int, @Body varchar(max), @message varchar(512)
	;with x0 as
	(
		select SQLProcessUtilization, SystemIdle, 100 - SystemIdle - SQLProcessUtilization AS Other, dateadd(ms, -1 * (@ts_now - [timestamp]), getdate()) as Date
		from ( 
			  select record.value('(./Record/@id)[1]', 'int') AS record_id, 
					record.value('(./Record/SchedulerMonitorEvent/SystemHealth/SystemIdle)[1]', 'int') AS [SystemIdle], 
					record.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') AS [SQLProcessUtilization], 
					[timestamp] 
			  from ( 
					select [timestamp], CONVERT(xml, record) AS [record] 
					from sys.dm_os_ring_buffers 
					where ring_buffer_type = N'RING_BUFFER_SCHEDULER_MONITOR' 
					and record like '%<SystemHealth>%') AS x 
		  ) x
	)
	select @c = (
					select  count(*)
					from x0
					where Date >= dateadd(minute, -5, getdate())
						and SQLProcessUtilization + Other > 60
				)
		

	if @c < 5
		return
	exec maint.usp_TakeSnapshotActiveSessions @Body output
	
	exec maint.usp_SendAlert @Subject = 'PROD DB: CPU >= 60% in last 5 minutes', @Body = @Body
end
