SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
    SELECT *
    FROM DBO.SYSOBJECTS
    WHERE ID =
          OBJECT_ID(N'[agency].[closePlayerGgrs]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  DROP PROCEDURE [agency].[closePlayerGgrs]
GO

CREATE PROCEDURE [agency].[closePlayerGgrs]
    (@player_id int, @player_account_id int, @agent_id int, @date DATETIME)
AS
  BEGIN
    insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)
      select
        @agent_id,
        @player_id,
        a.platform_id,
        -sum(case
                when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
            end),
        @date,
        -sum(case
                when a.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                else 0
            end),
        -sum(case
                when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                else 0
            end),
        -sum(case
                when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                else 0
            end)
      from admin_all.account_tran as a
        join admin_all.platform as p on a.platform_id = p.id
        left join admin_all.account_tran as r on a.rollback_tran_id = r.id
      where a.account_id = @player_account_id
            and a.datetime >= @date
            and a.datetime < DATEADD(day, 1, @date)
            and a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
            and p.platform_type <> 'SPORTSBOOK'
      group by a.account_id, a.platform_id

    insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)

        select
            @agent_id,
            @player_id,
            a.platform_id,
            -sum(case
                    when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                    when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                end),
            @date,
             -sum(case
                     when a.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                     when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'SYSTEM_EFT', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                     else 0
                 end),
             -sum(case
                     when a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                     when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                     else 0
                 end),
             -sum(case
                     when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                     else 0
                 end)
        from admin_all.betting_detail as b
            join admin_all.account_tran as a on
                    b.bet_reference_num = a.game_tran_id
                and b.platform_id = a.platform_id
                left join admin_all.account_tran as r on a.rollback_tran_id = r.id
            join admin_all.platform as p on a.platform_id = p.id
        where b.settlement_date >= @date and b.settlement_date < DATEADD(day, 1, @date)
            and a.tran_type in ('GAME_BET', 'MACHIN_EFT', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
            and p.platform_type = 'SPORTSBOOK'
            and a.account_id = @player_account_id
        group by a.account_id, a.platform_id



  END
GO
