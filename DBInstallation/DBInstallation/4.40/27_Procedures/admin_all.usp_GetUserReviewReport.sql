set ansi_nulls on
go
set quoted_identifier on
go
if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'admin_all.usp_GetUserReviewReport')
      AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure admin_all.usp_GetUserReviewReport
go

CREATE procedure admin_all.usp_GetUserReviewReport
(
  @startDate DATETIME = null,
  @endDate DATETIME = null,
  @brandIds nvarchar(3000) = null,
  @statuses nvarchar(1000) = null
)
as
begin
    create table #BRANDS (
      ID int primary key
    );
    create table #STATUSES (
      ID int identity primary key,
      STATUS nvarchar(15)
    );
    insert into #BRANDS select cast(Item as int) ID from admin_all.fc_splitDelimiterString(@brandIds, ',');
    insert into #STATUSES(STATUS) select RTRIM(LTRIM(Item)) STATUS from admin_all.fc_splitDelimiterString(@statuses, ',');

    with users as
        (select u.partyid, u.userid, u.ACTIVE_FLAG activeFlag, u.KYC_STATUS kycStatus
        from external_mpt.USER_CONF u inner join #BRANDS b on u.BRANDID = b.ID),
    reviews as
        (select r.id, r.partyid, r.status reviewStatus,
        r.REQUEST_DATE requestDate, r.SCHEDULE_DATE scheduleDate, r.COMPLETE_DATE completeDate,
        r.REQUEST_COMMENT requestComment, r.REVIEW_COMMENT reviewComment,
        reqStaff.LOGINNAME requestStaff, revStaff.LOGINNAME reviewStaff
        from admin_all.USER_REVIEW r inner join #STATUSES s on r.STATUS = s.STATUS
        left join admin_all.STAFF_AUTH_TBL reqStaff on r.REQUEST_STAFFID = reqStaff.STAFFID
        left join admin_all.STAFF_AUTH_TBL revStaff on r.REVIEW_STAFFID = revStaff.STAFFID
        where r.REQUEST_DATE between @startDate and @endDate)
    select r.id, u.partyid, u.userid, u.activeFlag, u.kycStatus, r.reviewStatus,
    CAST(r.requestDate AS date) requestDate, CAST(r.scheduleDate AS date) scheduleDate, CAST(r.completeDate AS date) completeDate,
    r.requestComment, r.reviewComment,
    r.requestStaff, r.reviewStaff
    from reviews r inner join users u on r.partyid = u.partyid
    order by r.requestDate desc;
end

-- exec  admin_all.usp_GetUserReviewReport
--       @startDate = '2019-07-21',
--       @endDate = '2019-07-29',
--       @brandIds = '1,2,3',
--       @statuses = 'SCHEDULED, COMPLETED'