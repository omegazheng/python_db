set quoted_identifier, ansi_nulls on
if object_id('rpt.usp_GetUserLiveToDate') is null
	exec('create procedure rpt.usp_GetUserLiveToDate as --')
go
alter procedure rpt.usp_GetUserLiveToDate
(
	@DepositMininum numeric(38, 18) = null, 
	@DepositMaximum numeric(38, 18) = null, 
	@NGRMinimum numeric(38, 18) = null, 
	@NGRMaximum numeric(38,18) = null, 
	@LastVisitMinimum int = null, 
	@LastVisitMaximum int = null, 
	@SignupMinimum datetime = null, 
	@SignupMaximum datetime = null, 
	@Country nvarchar(max) = null, 
	@UserName varchar(1000) = null, 
	@Email varchar(1000) = null, 
	@Lastname varchar(1000) = null, 
	@OrderBy varchar(128) = null,
	@AgentID int = null
)
as
begin
	SET @SignupMaximum = DATEADD(DD, 1, @SignupMaximum);
	set nocount on
	select c.iso2_code as Country into #Country
	from admin_all.COUNTRY c
		inner join admin_all.fc_splitDelimiterString(@Country, ',') c1 on c.iso2_code = c1.Item
	select @UserName = '%'+@UserName+'%', @Email = '%'+@Email+'%'
	if @AgentID is not null
	begin
		select u.PARTYID into #a
		from external_mpt.USER_CONF u
		where u.Hierarchy.IsDescendantOf((select Hierarchy from external_mpt.USER_CONF u1 where u1.PARTYID = @AgentID)) = 1
			and u.PARTYID <> @AgentID
	end
	declare @SQL nvarchar(max)
	select @SQL = 'with x1 as
	(
		select u.UserID, u.PARTYID, u.ParentID, u.Currency, u.REG_DATE RegistrationDate, u.GENDER Gender, u.Country, u.FIRST_NAME FirstName, u.LAST_NAME LastName, u.BIRTHDATE DateOfBirth, u.Email, u.Phone, u.PHONE2 Phone2, u.MOBILE_PHONE Mobile
		from external_mpt.USER_CONF u '+case when @AgentID is not null then ' where u.PARTYID in (select a.PARTYID from #a a)' else '' end+'
	), 
	x2 as
	(
		select x1.*, u.USERID as Agent, isnull(l.login_time, RegistrationDate) as LastAccessDate, isnull(ngr.NGRAmountReal, 0) NGRAmountReal, isnull(ngr.NGRAmountPlayableBonus, 0) NGRAmountPlayableBonus, isnull(ngr.NGRAmountReleasedBonus, 0) NGRAmountReleasedBonus,
			isnull(ngr.NGRAmountReal, 0)/* + isnull(ngr.NGRAmountPlayableBonus, 0)*/ + isnull(ngr.NGRAmountReleasedBonus,0) TotalNGR,
			isnull(p.Deposit, 0) Deposit, isnull(p.Withdrawal,0) Withdrawal
		from x1
			left join external_mpt.USER_CONF u on u.PARTYID = x1.ParentID
			inner join admin_all.ACCOUNT a on a.PARTYID = x1.PARTYID
			outer apply(select -sum(at.AmountReal) as NGRAmountReal, -sum(at.AmountPlayableBonus) NGRAmountPlayableBonus, -sum(at.AmountReleasedBonus) as NGRAmountReleasedBonus from admin_all.v_AccountTranLiveToDate at with(noexpand) where at.AggregateType = 0 and at.TranType in(''GAME_WIN'', ''GAME_BET'') and at.PartyID = x1.PARTYID) ngr
			outer apply(select top 1 login_time  from admin_all.USER_LOGIN_LOG l where l.LOGIN_TYPE = ''LOGIN'' and l.partyid = x1.PARTYID order by login_time desc) l
			outer apply(select sum(case when p.Type = ''WITHDRAWAL'' then p.Amount end) Withdrawal, sum(case when p.Type = ''DEPOSIT'' then p.Amount end) Deposit from admin_all.v_PaymentLiveToDate p with(noexpand) where p.AccountID = a.id and p.Status = ''COMPLETED'' and p.Type in (''WITHDRAWAL'', ''DEPOSIT'')) p
	)
	select u.*, DATEDIFF(day,  LastAccessDate, GETDATE()) as LastVisitDays
	from x2 u
	where 1=1'
	+case when @DepositMininum is not null then ' and Deposit >= @DepositMininum' else '' end 
	+case when @DepositMaximum is not null then ' and Deposit <= @DepositMaximum' else '' end
	+case when @NGRMinimum is not null then ' and TotalNGR >= @NGRMinimum' else '' end
	+case when @NGRMaximum is not null then ' and TotalNGR <= @NGRMaximum' else '' end 
	+case when @LastVisitMinimum is not null then ' and LastAccessDate >= dateadd(day, -1*@LastVisitMinimum, getdate())' else '' end 
	+case when @LastVisitMaximum is not null then ' and LastAccessDate <= dateadd(day, -1*@LastVisitMaximum, getdate())' else '' end
	+case when @SignupMinimum is not null then ' and RegistrationDate >= @SignupMinimum' else '' end 
	+case when @SignupMaximum is not null then ' and RegistrationDate <= @SignupMaximum' else '' end 
	+case when @Country is not null then ' and exists(select * from #Country c where c.Country = u.Country)' else '' end 
	+case when @UserName is not null then ' and UserID like @UserName' else '' end 
	+case when @Email is not null then ' and Email like @Email' else '' end 
	+case when @Lastname is not null then ' and LastName like @Lastname' else '' end 
	+ '		
	'+ +case 
			when @OrderBy is not null then 
				' order by ' + @OrderBy
		else '' end 

	exec sp_executesql	@SQL, 
						N'@DepositMininum numeric(38, 18), @DepositMaximum numeric(38, 18), @NGRMinimum numeric(38, 18), @NGRMaximum numeric(38,18), @LastVisitMinimum int, @LastVisitMaximum int, @SignupMinimum datetime, @SignupMaximum datetime, @Country nvarchar(max), @UserName varchar(1000), @Email varchar(1000), @Lastname varchar(1000), @AgentID int',
						@DepositMininum, @DepositMaximum, @NGRMinimum, @NGRMaximum, @LastVisitMinimum, @LastVisitMaximum, @SignupMinimum, @SignupMaximum, @Country, @UserName, @Email, @Lastname, @AgentID
end
go

--exec rpt.usp_GetUserLiveToDate @Orderby = 'PartyId'