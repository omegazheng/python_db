SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.TRI_StaffCompany') is null
begin
	exec('create trigger [admin_all].[TRI_StaffCompany] on [admin_all].[StaffCompany] 
instead of insert, update
as 
return
')
end
GO
ALTER trigger [admin_all].[TRI_StaffCompany] on [admin_all].[StaffCompany] 
AFTER INSERT
as 
begin
	if @@rowcount = 0
		return
	if exists(
				select * 
				from admin_all.StaffProduct sp
					inner join inserted i on i.StaffID = sp.StaffID
				)
	begin
		raiserror('A product associated staff can not be set as a company associated staff.', 16, 1)
		rollback
	end
end