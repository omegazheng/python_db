set ansi_nulls, quoted_identifier on
if object_id('maint.usp_ReplicationTaskCreateJob') is null
	exec('create procedure maint.usp_ReplicationTaskCreateJob as return')
go
alter procedure maint.usp_ReplicationTaskCreateJob @MaxInitializationJobs int = 3
as
begin
	set nocount on
	exec maint.usp_CreateServiceJob 'ReplicationTask Synchronization', 'maint.usp_ReplicationTaskSynchronize', 'Synchronize initialized tables'
	declare @i int = 1, @JobName varchar(128)
	while @i <=3
	begin
		select @JobName = 'ReplicationTask Initialization ' + cast(@i as varchar(20))
		exec maint.usp_CreateServiceJob @JobName, 'maint.usp_ReplicationTaskInitialize', 'Initialize tables'
		select @i = @i + 1
	end
end