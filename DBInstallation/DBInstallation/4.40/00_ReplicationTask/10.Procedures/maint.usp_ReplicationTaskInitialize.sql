set ansi_nulls, quoted_identifier on
if object_id('maint.usp_ReplicationTaskInitialize') is null
	exec('create procedure maint.usp_ReplicationTaskInitialize as return')
go
alter procedure maint.usp_ReplicationTaskInitialize
as
begin
	set nocount on
	set xact_abort on
	declare @SQL nvarchar(max), @ConnectionString nvarchar(1000), @ConnectionID int

	while(1=1)
	begin
		begin transaction
		update top (1) r	
			set r.SessionID = @@spid,
				@ConnectionID = rc.SourceConnectionID,
				@SQL = 'exec maint.ReplicateOne ' + quotename(rc.Name, '''')
		from maint.ReplicationTask r with(readcommittedlock, readpast, rowlock)
			left join maint.ReplicationLastExecution rl on r.ConfigurationID = rl.ConfigurationID
			inner join maint.ReplicationConfiguration rc on rc.ConfigurationID = r.ConfigurationID
		where rl.ToValue is null
		if @@rowcount = 0
			goto ___Exit___

		select @ConnectionString = maint.fn_GetSQLConnectionStringByID(@ConnectionID) 
		exec maint.usp_ExecuteSQL @ConnectionString, @SQL
		rollback
	end
___Exit___:
	if @@trancount > 0
		rollback
end
go