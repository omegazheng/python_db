set ansi_nulls, quoted_identifier on
if object_id('maint.ReplicationTask') is null
begin
	create table maint.ReplicationTask
	(
		ConfigurationID int not null,
		SessionID int null,
		constraint PK_maint_ReplicationTask primary key clustered(ConfigurationID),
		constraint FK_maint_ReplicationTask_ReplicationConfiguration foreign key(ConfigurationID) references maint.ReplicationConfiguration(ConfigurationID)
	)
end