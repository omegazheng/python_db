SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[SPORTSBOOK_BET_DETAILS]') = object_id and name = 'BETSLIP_ID')
  BEGIN
    ALTER TABLE [admin_all].[SPORTSBOOK_BET_DETAILS]
      ADD [BETSLIP_ID] NVARCHAR(50) NULL
  END
GO

IF NOT EXISTS(select * from sys.indexes where object_id('[admin_all].[SPORTSBOOK_BET_DETAILS]') = object_id and name = 'IDX_admin_all_SPORTSBOOK_BET_DETAILS_BETSLIP_ID')
  BEGIN
      create index IDX_admin_all_SPORTSBOOK_BET_DETAILS_BETSLIP_ID ON admin_all.SPORTSBOOK_BET_DETAILS(BETSLIP_ID)
  END
GO

