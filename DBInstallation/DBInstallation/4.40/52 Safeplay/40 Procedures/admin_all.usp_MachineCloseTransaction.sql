SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.usp_MachineCloseTransaction') is null
	exec('create procedure admin_all.usp_MachineCloseTransaction as')
GO
ALTER procedure admin_all.usp_MachineCloseTransaction
(
	@MachineTranID bigint = null,
	@EFTID bigint = null,
	@PartyID int = null,
	@MachineID int = null
)
as
begin
	set nocount on
	if @MachineTranID is null and @EFTID is not null
	begin
		select @MachineTranID = MachineTranID
		from admin_all.MachineTranEFT
		where EFTID = @EFTID
	end
	if @MachineTranID is null
	begin
		select @MachineTranID = MachineTranID
		from admin_all.MachineTran
		where EndDate is null
			and PartyID = @PartyID
			and MachineID = @MachineID
	end
	if @MachineTranID is null
	begin
		raiserror('Cold not find machine transaction', 16, 1)
		return
	end
	update admin_all.MachineTran set EndDate = getdate() where MachineTranID = @MachineTranID
end
go
--select * from admin_all.MachineTran