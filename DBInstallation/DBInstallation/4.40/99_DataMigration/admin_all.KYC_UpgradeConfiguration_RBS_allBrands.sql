

-- Turn on KYC RBS provider
-- via admin_all.BRAND_COUNTRY (KYC_SYSTEM), admin_all.BRAND_REGISTRY (kyc.check.enabled, kyc.rbs.enabled)
-- instead of admin_all.BRAND_REGISTRY (kyc.rbs)

-- All Brands
if EXISTS (select * from admin_all.REGISTRY_HASH where MAP_KEY = 'kyc.rbs' and value = 'true')
begin
    if NOT EXISTS (select * from admin_all.REGISTRY_HASH where MAP_KEY = 'kyc.check.enabled')
        insert into admin_all.REGISTRY_HASH(map_key, "value")
        values('kyc.check.enabled', 'true')
    ELSE
        update admin_all.REGISTRY_HASH set value = 'true' where MAP_KEY = 'kyc.check.enabled';

--     if NOT EXISTS (select * from admin_all.REGISTRY_HASH where MAP_KEY = 'kyc.rbs.enabled')
--         insert into admin_all.REGISTRY_HASH(map_key, "value")
--         values('kyc.rbs.enabled', 'true')
--     ELSE
--         update admin_all.REGISTRY_HASH set value = 'true' where MAP_KEY = 'kyc.rbs.enabled';

    -- insert/update KYC_SYSTEM (all brands, all countries)
    declare @BRANDIDS table (BRANDID int);
    declare @CURRENT_BRANDID int;
    insert into @BRANDIDS(BRANDID) select distinct(BRANDID) from admin.CASINO_BRAND_DEF;
    while EXISTS(select BRANDID from @BRANDIDS)
        begin
            SELECT TOP 1 @CURRENT_BRANDID = BRANDID FROM @BRANDIDS ORDER BY BRANDID ASC;

            declare @COUNTRIES table (COUNTRY char(2));
            declare @CURRENT_COUNTRY char(2);
            INSERT INTO @COUNTRIES (COUNTRY) select distinct(COUNTRY) as countries from external_mpt.USER_CONF where BRANDID = @CURRENT_BRANDID ;
            delete from @COUNTRIES where COUNTRY is null;
            while EXISTS(select COUNTRY from @COUNTRIES)
            begin
                SELECT TOP 1 @CURRENT_COUNTRY = COUNTRY FROM @COUNTRIES ORDER BY COUNTRY ASC;
                if NOT exists (select * from admin_all.BRAND_COUNTRY where BRANDID = @CURRENT_BRANDID and COUNTRY = @CURRENT_COUNTRY)
                    insert into admin_all.BRAND_COUNTRY(BRANDID, COUNTRY, KYC_SYSTEM, MIN_SIGNUP_AGE)
                    values(@CURRENT_BRANDID, @CURRENT_COUNTRY, 'RBS', 18);
                ELSE
                    update admin_all.BRAND_COUNTRY set KYC_SYSTEM = 'RBS'
                    where BRANDID = @CURRENT_BRANDID and COUNTRY = @CURRENT_COUNTRY;
                delete from @COUNTRIES where COUNTRY = @CURRENT_COUNTRY;
            end

            delete from @BRANDIDS where BRANDID = @CURRENT_BRANDID;
        end
end