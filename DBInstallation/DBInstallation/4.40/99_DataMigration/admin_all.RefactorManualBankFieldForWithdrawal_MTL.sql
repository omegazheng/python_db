
if exists (select *from dbo.CUSTOMER where name in ('MTL', 'DEMO'))

begin

declare @paymentMethodCodes table (paymentMethodCode nvarchar(30));
insert into @paymentMethodCodes(paymentMethodCode) select distinct(PAYMENT_METHOD_CODE) from admin_all.MANUAL_BANK_FIELD;
while exists (select * from @paymentMethodCodes)
begin
    declare @paymentMethodCode nvarchar(30);
    select top 1 @paymentMethodCode=paymentMethodCode from @paymentMethodCodes;

    if @paymentMethodCode in (
        'MB_AKBANK', 'MB_DENZIBANK', 'MB_FINANSBANK_CEPBANK', 'MB_GARANTIBANK', 'MB_GARANTIONE_CEPBANK',
        'MB_ING_CEPBANK', 'MB_ISBANKASI', 'MB_PTT_CEPBANK', 'MB_SEKERBANK_CEPBANK', 'MB_TEB_CEPBANK',
        'MB_TEBBANKCEP', 'MB_VAKIFBANK_CEPBANK', 'MB_VIP_BANK_TSF', 'MB_YAPIKREDI', 'MB_ZIRAAT_CEPBANK'
        )
        begin
            -- Step 1: add bankName selection field if exists for 'DEPOSIT'
            exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step1_AddBankNameSelectionField @paymentMethodCode=@paymentMethodCode;

            -- Step 2: add special fields
            if @paymentMethodCode = 'MB_VIP_BANK_TSF'
                exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddSpecialFields_MB_VIP_BANK_TSF @paymentMethodCode=@paymentMethodCode;
            else if @paymentMethodCode in ('MB_AKBANK', 'MB_GARANTIBANK')
                exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step2_AddTextFields_MB_AKBANK_AND_MB_GRANTIBANK @paymentMethodCode=@paymentMethodCode
            update admin_all.MANUAL_BANK_FIELD set name = 'idNumber' where name = 'IDnumber' and PAYMENT_TYPE = 'WITHDRAWAL'

            -- Step 3: add 8 constant fields
            exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step3_Add8ConstantFields @paymentMethodCode=@paymentMethodCode;
        end
    delete from @paymentMethodCodes where paymentMethodCode = @paymentMethodCode;
end

end