IF  EXISTS (SELECT * FROM dbo.CUSTOMER WHERE  NAME in (N'BW', 'DEMO'))
BEGIN

declare @PLATFORM_ID int, @GAME_CATEGORY_ID int, @SEGAMENT_ID int;

    if not exists(select *
                  from admin_all.PLATFORM
                  where CODE = 'KAMBI')
    BEGIN
        exec admin_all.usp_CreateProduct @Code='KAMBI', @ProductType='SPORTSBOOK', @WalletType='GAME_PLAY'
        select @PLATFORM_ID = id from admin_all.PLATFORM where code = 'KAMBI'
        select @GAME_CATEGORY_ID = id from admin_all.GAME_CATEGORY where name = 'unknown'
        select @SEGAMENT_ID = id from admin_all.segment where name = 'Sports'

        if not exists(select * from admin_all.game_info where platform_id = @PLATFORM_ID and GAME_ID = 'KAMBI_GAME_ID')
            BEGIN
                INSERT INTO admin_all.GAME_INFO (PLATFORM_ID, GAME_ID, NAME, GAME_CATEGORY_ID, GAME_LAUNCH_ID, IS_TOUCH, IS_FREESPIN_ENABLE, SEGMENT_ID, SUB_PLATFORM_ID, REFERENCE)
                VALUES (@PLATFORM_ID, 'KAMBI_GAME_ID', 'KAMBI_GAME_ID', @GAME_CATEGORY_ID, 'KAMBI_GAME_ID', 0, 0, @SEGAMENT_ID, null, null);
            END
    END

    if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'kambi.regulationID')
    begin
        INSERT INTO ADMIN_ALL.REGISTRY_HASH (MAP_KEY, VALUE) VALUES ('kambi.regulationID', 'TBD');
    end

    if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'kambi.enableCancellationTest')
    begin
        INSERT INTO ADMIN_ALL.REGISTRY_HASH (MAP_KEY, VALUE) VALUES ('kambi.enableCancellationTest', 'false');
    end

    if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'kambi.bootstrapUrl')
    begin
        INSERT INTO ADMIN_ALL.REGISTRY_HASH (MAP_KEY, VALUE) VALUES ('kambi.bootstrapUrl', 'TBD');
    end

END
