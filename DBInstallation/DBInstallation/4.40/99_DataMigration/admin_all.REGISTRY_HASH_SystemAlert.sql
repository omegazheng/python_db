SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'systemAlert.emailList')
    begin
        insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
            values ('systemAlert.emailList', 'grant@omegasys.eu, zheng@omegasys.eu, jim@omegasys.eu, leo@omegasys.eu, mak@omegasys.eu, robbie@omegasys.eu, daniel@omegasys.eu, stanley@omegasys.eu')
    end

go
