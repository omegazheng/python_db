-- These stored proc are for migration only. After 4.40 , they can be removed
set ansi_nulls on
go
set quoted_identifier on
go

if exists(
    select *
    from dbo.sysobjects
    where id =
          object_id(N'admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step3_Add8ConstantFields')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  drop procedure admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step3_Add8ConstantFields
go

CREATE PROCEDURE admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step3_Add8ConstantFields
(
    @paymentMethodCode nvarchar(30)
)
as

begin

if exists (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode)
  begin

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='branchName' and payment_type='WITHDRAWAL')
        insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
        values(@paymentMethodCode, 'branchName', 'WITHDRAWAL', 'TEXT', 50, 1, 1, 1, null);

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='branchCode' and payment_type='WITHDRAWAL')
      insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
      values(@paymentMethodCode, 'branchCode', 'WITHDRAWAL', 'TEXT', 50, 1, 1, 1, null);

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='city' and payment_type='WITHDRAWAL')
      insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
      values(@paymentMethodCode, 'city', 'WITHDRAWAL', 'TEXT', 50, 1, 1, 1, null);

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='mobileNumber' and payment_type='WITHDRAWAL')
      insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
      values(@paymentMethodCode, 'mobileNumber', 'WITHDRAWAL', 'TEXT', 15, 8, 1, 1, null);

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='idNumber' and payment_type='WITHDRAWAL')
      insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
      values(@paymentMethodCode, 'idNumber', 'WITHDRAWAL', 'TEXT', 25, 1, 1, 1, null);

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='birthDate' and payment_type='WITHDRAWAL')
      insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
      values(@paymentMethodCode, 'birthDate', 'WITHDRAWAL', 'BIRTHDATE', 10, 8, 1, 1, null);

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='accountNumber' and payment_type='WITHDRAWAL')
      insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
      values(@paymentMethodCode, 'accountNumber', 'WITHDRAWAL', 'TEXT', 60, 1, 1, 1, null);

    if NOT EXISTS (select * from admin_all.manual_bank_field where payment_method_code = @paymentMethodCode and name='iban' and payment_type='WITHDRAWAL')
      insert into admin_all.MANUAL_BANK_FIELD(PAYMENT_METHOD_CODE, NAME, PAYMENT_TYPE, TYPE, MAX_LENGTH, MIN_LENGTH, WEIGHT, IS_REQUIRED, OPTIONS)
      values(@paymentMethodCode, 'iban', 'WITHDRAWAL', 'IBAN', 60, 1, 1, 1, null);

  end

end

-- exec admin_all.RefactorManualBankFieldForWithdrawal_MTL_Step3_Add8ConstantFields @paymentMethodCode='MB_VIP_BANK_TSF'