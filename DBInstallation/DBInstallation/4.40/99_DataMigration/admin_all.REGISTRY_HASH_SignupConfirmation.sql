SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Default signup/quickSignup process for all clients to email link
if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'signup.sendConfirmation.type')
    begin
        INSERT INTO ADMIN_ALL.REGISTRY_HASH (MAP_KEY, VALUE) VALUES ('signup.sendConfirmation.type', 'LINK');
    end

if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'signup.sendConfirmation.target')
    begin
        INSERT INTO ADMIN_ALL.REGISTRY_HASH (MAP_KEY, VALUE) VALUES ('signup.sendConfirmation.target', 'EMAIL');
    end

-- Delete deprecated confirmation type configurations
if EXISTS(select * from admin_all.REGISTRY_HASH where MAP_KEY IN ('quickSignup.sendConfirmationCode', 'quickSignup.sendConfirmationEmail'))
    begin
        delete from admin_all.REGISTRY_HASH where MAP_KEY IN ('quickSignup.sendConfirmationCode', 'quickSignup.sendConfirmationEmail');
    end

if EXISTS(select * from admin_all.BRAND_REGISTRY where MAP_KEY IN ('quickSignup.sendConfirmationCode', 'quickSignup.sendConfirmationEmail'))
    begin
        delete from admin_all.BRAND_REGISTRY where MAP_KEY IN ('quickSignup.sendConfirmationCode', 'quickSignup.sendConfirmationEmail');
    end

-- Create base Email Template for all brands. Not mandatory since we have a default to fallback to.
/*if not exists(select * from admin_all.EMAIL_TEMPLATE where type='CODE_CONFIRMATION' and language ='en' and subtype='ALL')
    begin
        declare @BrandIds table (BrandId int);
        declare @Current_BrandId int;
        insert into @BrandIds(BrandId) (select distinct(BrandId) from admin_all.EMAIL_TEMPLATE);
        while EXISTS(select BrandId from @BrandIds)
            begin
                select top 1 @Current_BrandId = BrandId from @BrandIds order by BrandId asc;
                INSERT INTO admin_all.EMAIL_TEMPLATE (brandid, type, subject, body, language, subtype) VALUES (@Current_BrandId, 'CODE_CONFIRMATION', 'Verification Code for ${user.userId}', 'Your verification code is ${code}', 'en', 'ALL');
                delete from @BrandIds where BrandId = @Current_BrandId;
            end
    end*/

-- Delete deprecated email information configurations
if EXISTS(select * from admin_all.REGISTRY_HASH where MAP_KEY IN ('quickSignup.confirmationCode.emailFrom', 'quickSignup.confirmationCode.emailSubject'))
    begin
        delete from admin_all.REGISTRY_HASH where MAP_KEY IN ('quickSignup.confirmationCode.emailFrom', 'quickSignup.confirmationCode.emailSubject');
    end

if EXISTS(select * from admin_all.BRAND_REGISTRY where MAP_KEY IN ('quickSignup.confirmationCode.emailFrom', 'quickSignup.confirmationCode.emailSubject'))
    begin
        delete from admin_all.BRAND_REGISTRY where MAP_KEY IN ('quickSignup.confirmationCode.emailFrom', 'quickSignup.confirmationCode.emailSubject');
    end

-- BETWARRIOR Specific changes to maintain their current behavior: Set signup/quickSignup process to mobile code // Allow full registration without link // disable confirmation email after code or link
-- Use Casino Brand Def since most clients do not have dbo.CUSTOMER populated
begin
    declare @BetWarriorBrandId int;
    set @BetWarriorBrandId = (select BRANDID from admin.CASINO_BRAND_DEF where BRANDNAME = N'BETWARRIOR');
    if @BetWarriorBrandId is not null
        begin
            if EXISTS(select * from admin_all.BRAND_REGISTRY where map_key = 'signup.sendConfirmation.target' and brandid = @BetWarriorBrandId)
                update admin_all.BRAND_REGISTRY set value = 'MOBILE' where map_key = 'signup.sendConfirmation.target' and brandid = @BetWarriorBrandId;
            else
                insert into admin_all.BRAND_REGISTRY(brandid, map_key, "value") VALUES (@BetWarriorBrandId, 'signup.sendConfirmation.target', 'MOBILE');
            if EXISTS(select * from admin_all.BRAND_REGISTRY where map_key = 'signup.sendConfirmation.type' and brandid = @BetWarriorBrandId)
                update admin_all.BRAND_REGISTRY set value = 'CODE' where map_key = 'signup.sendConfirmation.type' and brandid = @BetWarriorBrandId;
            else
                insert into admin_all.BRAND_REGISTRY(brandid, map_key, "value") VALUES (@BetWarriorBrandId, 'signup.sendConfirmation.type', 'CODE');
            if EXISTS(select * from admin_all.BRAND_REGISTRY where map_key = 'signup.sendConfirmation.bypassLink' and brandid = @BetWarriorBrandId)
                update admin_all.BRAND_REGISTRY set value = 'true' where map_key = 'signup.sendConfirmation.bypassLink' and brandid = @BetWarriorBrandId;
            else
                insert into admin_all.BRAND_REGISTRY(brandid, map_key, "value") VALUES (@BetWarriorBrandId, 'signup.sendConfirmation.bypassLink', 'true');
            if EXISTS(select * from admin_all.BRAND_REGISTRY where map_key = 'disabled.emailType.QUICK_SIGNUP_CONFIRMATION' and brandid = @BetWarriorBrandId)
                update admin_all.BRAND_REGISTRY set value = 'QUICK_SIGNUP_CONFIRMATION' where map_key = 'disabled.emailType.QUICK_SIGNUP_CONFIRMATION' and brandid = @BetWarriorBrandId;
            else
                insert into admin_all.BRAND_REGISTRY(brandid, map_key, "value") VALUES (@BetWarriorBrandId, 'disabled.emailType.QUICK_SIGNUP_CONFIRMATION', 'QUICK_SIGNUP_CONFIRMATION');
        end
end


go