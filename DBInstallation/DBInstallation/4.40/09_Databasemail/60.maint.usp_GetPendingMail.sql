GO
if object_id('maint.usp_GetPendingMail') is null
	exec('create procedure maint.usp_GetPendingMail as --')
go
alter procedure maint.usp_GetPendingMail
as
begin
	set nocount on
	if @@trancount = 0
	begin
		raiserror('maint.usp_GetPendingMail must be run within a transaction', 16, 1)
		return
	end
	if isnull(admin_all.fn_GetRegistry('System.Mail.Enabled'),'1') <>'1'
		return
	if isnull(admin_all.fn_GetRegistry('System.Mail.UseMSDB'),'1') = 1
		return
	declare @MaxAttempts int = cast(isnull(admin_all.fn_GetRegistry('System.Mail.MaxAttempts'),'0')  as int)
	update m
		set Status = 'Sending',
			LastSentDate = getdate()
		output current_transaction_id() ServerTransactionID, inserted.MailItemID, inserted.ProfileName, inserted.Recipients, inserted.CopyRecipients, inserted.BlindCopyRecipients, inserted.Subject, inserted.Body, inserted.BodyFormat, inserted.Importance, inserted.Sensitivity, inserted.ReplyTo, inserted.FromAddress
	from (
			select top 1 MailItemID,ProfileName, Recipients, CopyRecipients, BlindCopyRecipients, Subject, Body, BodyFormat, Importance, Sensitivity, ReplyTo, FromAddress, Status, LastSentDate
			from maint.MailItem with(forceseek, readpast, rowlock)
			where Status in ('Pending', 'Error')
				and NextSendingDate < getdate()
				and Attempts < @MaxAttempts
			order by NextSendingDate asc
		) m
end