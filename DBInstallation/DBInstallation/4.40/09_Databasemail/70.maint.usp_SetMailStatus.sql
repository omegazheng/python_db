GO
if object_id('maint.usp_SetMailStatus') is null
	exec('create procedure maint.usp_SetMailStatus as --')
go
alter procedure maint.usp_SetMailStatus
(
	@ServerTransactionID bigint, 
	@MailItemID int,
	@ErrorMessage varchar(max) = null
)
as
begin
	set nocount on
	if @@trancount = 0
	begin
		raiserror('maint.usp_SetMailStatus must be run within a transaction', 16, 1)
		return
	end
	if current_transaction_id() <> @ServerTransactionID
	begin
		raiserror('Transaction is not recognized', 16, 1)
		return
	end
	if isnull(admin_all.fn_GetRegistry('System.Mail.Enabled'),'1') <>'1'
		return
	if isnull(admin_all.fn_GetRegistry('System.Mail.UseMSDB'),'1') = 1
		return
	
	update m
		set Status = case when @ErrorMessage is null then 'Sent' else 'Error' end,
			LastSentDate = getdate(),
			NextSendingDate = dateadd(minute, cast(isnull(admin_all.fn_GetRegistry('System.Mail.AttemptInterval'),'5') as int), getdate()),
			Attempts = Attempts + 1,
			ErrorMessage = @ErrorMessage
		output current_transaction_id() ServerTransactionID, inserted.MailItemID, inserted.ProfileName, inserted.Recipients, inserted.CopyRecipients, inserted.BlindCopyRecipients, inserted.Subject, inserted.Body, inserted.BodyFormat, inserted.Importance, inserted.Sensitivity, inserted.ReplyTo, inserted.FromAddress
	from maint.MailItem m
	where m.MailItemID = @MailItemID

	declare @ret int
	exec @ret = sp_getapplock 'System.Mail.Retention', 'Exclusive', 'Transaction', 0
	if @ret < 0
		return
	select @ret = cast(isnull(admin_all.fn_GetRegistry('System.Mail.Retention'), '10') as int) * 24 * 60 + cast(isnull(admin_all.fn_GetRegistry('System.Mail.AttemptInterval'), '5') as int)
	delete m 
	from maint.MailItem m with(forceseek)
	where Status = 'Sent'
		and NextSendingDate < = dateadd(minute, -@ret, getdate())
end