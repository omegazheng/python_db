set ansi_nulls, quoted_identifier on
go
if object_id('maint.usp_CheckLogShippingStatus') is null
	exec('create procedure maint.usp_CheckLogShippingStatus as --')
go
alter procedure maint.usp_CheckLogShippingStatus
as
begin
	set nocount on
	declare @Date datetime, @Hours int, @Subject nvarchar(100), @Body nvarchar(100), @CustomerName sysname
	select top 1 @Date = convert(datetime, replace(substring([FileName],1,10), '_', '-')+' '+ substring([FileName],12,2)+':'+ substring([FileName],14,2) +':'+ substring([FileName],16,2), 120)
	from (
			select top 1 left(right(FileName, 29), 17) FileName
			from msdb.dbo.OmegaRestoreHistory
			where DatabaseName = db_name()
			order by FileName desc
	)a
	select @CustomerName = rtrim(isnull((select top 1 name from dbo.CUSTOMER), 'Unknown'))
	select @Hours = datediff(hour, @Date, getdate())
	if @hours is null 
	begin
		select @Subject = '['+@CustomerName+'] No log-shipping history' 
	end
	else if @Hours > 4
	begin
		select @Subject = '['+@CustomerName+'] Log-shipping delays '  + cast(@Hours as varchar(20)) + ' hours.'
	end
	if @Subject is not null
	begin
		exec maint.usp_SendMail  
					@recipients = 'john@omegasys.eu;zheng@omegasys.eu;peter@flowgaming.com;jim@omegasys.eu;grant@omegasys.eu',  
					@body = @Subject,  
					@subject = @Subject,
					@importance  ='High';  
	end
end
