set quoted_identifier, ansi_nulls on
go
if object_id('admin_all.usp_PopulateDWGamePlayerDaily') is null
begin
	exec('create procedure admin_all.usp_PopulateDWGamePlayerDaily as --')
end
go
alter procedure admin_all.usp_PopulateDWGamePlayerDaily @StartDate datetime = null, @EndDate datetime = null, @CallerProcedureID int = null
as
begin
	set nocount, xact_abort on
	
	declare @LastExecutionTime datetime, @Now datetime = getdate(), @StrLastExecutionTime varchar(100),
	        @Resource nvarchar(200), @Ret int, @LastSanityCheckDate date, @StrLastSanityCheckDate varchar(100),
			@ThisSanityCheckDate date, @MailBody nvarchar(max), @Recipients nvarchar(1000), @Subject nvarchar(1000)

	select @Recipients = admin_all.fn_GetRegistry('PopulateDWGamePlayerDaily.Recipients')
	if @Recipients is null
	begin
		exec admin_all.usp_SetRegistry 'PopulateDWGamePlayerDaily.Recipients',  'zheng@omegasys.eu'
		select @Recipients = admin_all.fn_GetRegistry('PopulateDWGamePlayerDaily.Recipients')
	end

	select	@LastSanityCheckDate = convert(date, admin_all.fn_GetRegistry('PopulateDWGamePlayerDaily.LastSanityCheckDate'), 120)
	if @LastSanityCheckDate is null
	begin
		select @StrLastSanityCheckDate = convert(varchar(100), dateadd(day, 1, eomonth(dateadd(month, -3, @Now))), 120) -- set to 2 month ago
		exec admin_all.usp_SetRegistry 'PopulateDWGamePlayerDaily.LastSanityCheckDate',  @StrLastSanityCheckDate
		select @LastSanityCheckDate = convert(datetime, admin_all.fn_GetRegistry('PopulateDWGamePlayerDaily.LastSanityCheckDate'), 120)
	end
	select @ThisSanityCheckDate = convert(varchar(100), dateadd(day, 1, eomonth(dateadd(month, -2, @Now))), 120) 

	select	@LastExecutionTime = convert(datetime, admin_all.fn_GetRegistry('PopulateDWGamePlayerDaily.LastExecutionTime'), 120)
	if @LastExecutionTime is null
	begin
		select @StrLastExecutionTime = convert(varchar(100), dateadd(second, -30, @Now), 120)
		exec admin_all.usp_SetRegistry 'PopulateDWGamePlayerDaily.LastExecutionTime',  @StrLastExecutionTime
		select @LastExecutionTime = convert(datetime, admin_all.fn_GetRegistry('PopulateDWGamePlayerDaily.LastExecutionTime'), 120)
	end


	select @StrLastExecutionTime = null
	if @StartDate is null
	begin
		select @StartDate = cast(getdate() as date)
		if datediff(day, @LastExecutionTime, @StartDate) > 0
			select @StartDate = @LastExecutionTime
		select @StrLastExecutionTime = convert(varchar(100), @Now, 120)
	end
	select @StartDate = cast(@StartDate as date)
	select @EndDate = dateadd(day, 1, isnull(@EndDate, @Now))
	

	while @StartDate < @EndDate
	begin
		--print convert(varchar(100), @StartDate, 120)
		begin transaction
		select @Resource = 'PopulateDWGamePlayerDaily-'+ convert(varchar(10), @StartDate, 120)
		exec @Ret = sp_getapplock @Resource = @Resource, @LockMode = N'Exclusive', @LockOwner = 'Transaction', @LockTimeout = 0
	  if @Ret <0
	    goto ___Next___
		if object_id('tempdb..#DW_GAME_PLAYER_DAILY') is not null
			drop table #DW_GAME_PLAYER_DAILY
		select top 0 * into #DW_GAME_PLAYER_DAILY from admin_all.DW_GAME_PLAYER_DAILY
		--create unique clustered index IDX on #DW_GAME_PLAYER_DAILY(SUMMARY_DATE, PARTY_ID,GAME_ID,PLATFORM)
		if object_id('tempdb..#Ratio') is not null
			drop table #Ratio
	

		SELECT
			 latestJPRatio.brandid AS BrandID,
			 latestJPRatio.ratio   AS Ratio,
			 latestJPRatio.GAME_INFO_ID GameInfoID
			 into #Ratio
		FROM [admin_all].[BRAND_GAME_JP_RATIO] latestJPRatio,
			(
			SELECT
				brandId,
				game_info_id,
				max(last_update) AS last_update
			FROM admin_all.brand_game_jp_ratio
			where last_update < dateadd(day, 1, @StartDate)
			GROUP BY brandId, game_info_id
			) latest
		WHERE latestJPRatio.BRANDID = latest.BRANDID AND
				latestJPRatio.GAME_INFO_ID = latest.GAME_INFO_ID AND
				latestJPRatio.last_update = latest.last_update
		create unique clustered index IDX on #Ratio(BrandID, GameInfoID)

		;with x0 as
		(
			select 
					@StartDate as SUMMARY_DATE, dateadd(year, datediff(year, '1900-01-01', @StartDate), '1900-01-01') Year, 
					dateadd(month, datediff(month, '1900-01-01', @StartDate), '1900-01-01') month,
					users.PARTYID PARTY_ID, users.BRANDID BRAND_ID, users.CURRENCY, users.REG_DATE,
					case when  convert(DATE, users.REG_DATE) >= CONVERT(DATE, @StartDate) THEN 1 ELSE 0 END IS_NEW,
					users.COUNTRY, brand.brandName BRAND, platform.CODE PLATFORM_CODE, platform.NAME PLATFORM,
					ISNULL(GAME_INFO.NAME, 'Unknown')  GAME_NAME,
					ISNULL(GAME_CATEGORY.NAME, 'Unknown') GAME_CATEGORY,
					dw_read.*,
					isnull(abs(PreJPC * ratio.Ratio /100), 0) JPC,
					isnull(abs(PreJPC_REAL * ratio.Ratio /100), 0) JPC_REAL,
					isnull(abs(PreJPC_RELEASED_BONUS * ratio.Ratio /100), 0) JPC_RELEASED_BONUS,
					isnull(abs(PreJPC_PLAYABLE_BONUS * ratio.Ratio /100), 0) JPC_PLAYABLE_BONUS
			from
			  (
				select	account_id,
				  platform_id,
				  game_id,
				  sum(
					  CASE
					  WHEN ((tran_type = 'GAME_PLAY' AND amount_real + AMOUNT_RELEASED_BONUS + AMOUNT_PLAYABLE_BONUS < 0) OR
							tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND'))
						THEN (amount_real + AMOUNT_RELEASED_BONUS + AMOUNT_PLAYABLE_BONUS) * -1
					  ELSE 0
					  END)
												 handle,
				  sum(
					  CASE
					  WHEN tran_type in ('GAME_PLAY', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'GAME_BET', 'MACHIN_EFT', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
						THEN (amount_real + AMOUNT_RELEASED_BONUS + AMOUNT_PLAYABLE_BONUS) * -1
					  ELSE 0
					  END)                       pnl,
				  sum(
					  CASE
					  WHEN (tran_type = 'GAME_PLAY' AND amount_real < 0) OR tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND')
						THEN amount_real * -1
					  ELSE 0
					  END)
												 handle_real,
				  sum(
					  CASE
					  WHEN tran_type in ('GAME_PLAY', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'GAME_BET', 'MACHIN_EFT', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
						THEN (amount_real) * -1
					  ELSE 0
					  END)                       pnl_real,
				  sum(
					  CASE
					  WHEN (tran_type = 'GAME_PLAY' AND AMOUNT_RELEASED_BONUS < 0) OR tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND')
						THEN AMOUNT_RELEASED_BONUS * -1
					  ELSE 0
					  END)
												 handle_released_bonus,
				  sum(
					  CASE
					  WHEN tran_type in ('GAME_PLAY', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'GAME_BET', 'MACHIN_EFT', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
						THEN AMOUNT_RELEASED_BONUS * -1
					  ELSE 0
					  END)
												 pnl_released_bonus,
				  sum(
					  CASE
					  WHEN (tran_type = 'GAME_PLAY' AND AMOUNT_PLAYABLE_BONUS < 0) OR tran_type in ('GAME_BET', 'MACHIN_EFT', 'STAKE_DEC', 'REFUND')
						THEN AMOUNT_PLAYABLE_BONUS * -1
					  ELSE 0
					  END)
												 handle_playable_bonus,
				  sum(
					  CASE
					  WHEN (tran_type = 'BONUS_REL' AND platform_id IS NOT NULL AND game_id IS NOT NULL)
						THEN AMOUNT_PLAYABLE_BONUS * -1
					  ELSE 0
					  END)
												 in_game_playable_bonus_release,
				  sum(
					  CASE
					  WHEN (tran_type = 'BONUS_REL' AND platform_id IS NOT NULL AND game_id IS NOT NULL)
						THEN AMOUNT_RELEASED_BONUS * -1
					  ELSE 0
					  END)
												 in_game_released_bonus_release,
				  sum(
					  CASE
					  WHEN tran_type in ('GAME_PLAY', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'GAME_BET', 'MACHIN_EFT', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
						THEN AMOUNT_PLAYABLE_BONUS * -1
					  ELSE 0
					  END)
												 pnl_playable_bonus,
				  sum(
					  CASE
					  WHEN tran_type = 'TIPS'
						THEN (AMOUNT_REAL) * -1
					  ELSE 0
					  END)                       tips_real,
				  sum(
					  CASE
					  WHEN tran_type = 'TIPS'
						THEN (AMOUNT_PLAYABLE_BONUS) * -1
					  ELSE 0
					  END)                       tips_playable_bonus,
				  sum(
					  CASE
					  WHEN tran_type = 'TIPS'
						THEN (AMOUNT_RELEASED_BONUS) * -1
					  ELSE 0
					  END)                       tips_released_bonus,
				  count(DISTINCT (game_tran_id)) game_count,
				  sum(CASE WHEN amount_real < 0 AND tran_type in ('GAME_BET', 'MACHIN_EFT')
					THEN 1
					  ELSE 0 END)                bet_count,
				  count(id)                      tran_count,
				  sum(CASE WHEN tran_type = 'TIPS'
					THEN 1
					  ELSE 0 END)                tips_count,
					sum(case when tran_type in ('GAME_BET', 'MACHIN_EFT') then amount_real + amount_released_bonus + amount_playable_bonus end) AS PreJPC,
					sum(case when tran_type in ('GAME_BET', 'MACHIN_EFT') then amount_real end) AS PreJPC_REAL,
					sum(case when tran_type in ('GAME_BET', 'MACHIN_EFT') then amount_released_bonus end) AS PreJPC_RELEASED_BONUS,
					sum(case when tran_type in ('GAME_BET', 'MACHIN_EFT') then amount_playable_bonus end) AS PreJPC_PLAYABLE_BONUS
				
				FROM admin_all.account_tran with(Forceseek)
				WHERE
				  (tran_type in ('GAME_PLAY', 'GAME_WIN', 'SYSTEM_EFT', 'CORRECTION', 'GAME_BET', 'MACHIN_EFT', 'CASH_OUT', 'STAKE_DEC', 'REFUND', 'TIPS')  OR
				   (tran_type = 'BONUS_REL' AND platform_id IS NOT NULL AND game_id IS NOT NULL))
				  AND DATETIME >= @StartDate
				  AND DATETIME < dateadd(day, 1, @StartDate)
				  AND rolled_back = 0
				GROUP BY account_id, platform_id, game_id
			  ) dw_read
			  join admin_all.account on account.id = dw_read.ACCOUNT_ID
			  join external_mpt.user_conf users on users.partyId = account.PARTYID
			  join admin.casino_brand_def brand on brand.brandId = users.brandId
			  left join admin_all.platform on dw_read.PLATFORM_ID = PLATFORM.ID
			  left join admin_all.game_info on dw_read.GAME_ID = game_info.GAME_ID AND DW_READ.PLATFORM_ID = GAME_INFO.PLATFORM_ID
			  left join admin_all.GAME_CATEGORY on game_info.GAME_CATEGORY_ID = GAME_CATEGORY.ID
			  outer apply (
								select *
								from #Ratio r
								where r.BrandID = users.BRANDID
									and r.GameInfoID = GAME_INFO.ID
						) ratio
		)
		insert into #DW_GAME_PLAYER_DAILY(SUMMARY_DATE, PARTY_ID, GAME_ID, PLATFORM, GAME_NAME, GAME_COUNT, BET_COUNT, TRAN_COUNT, HANDLE, PNL, MONTH, YEAR, GAME_CATEGORY, IS_NEW, BRAND, CURRENCY, PLATFORM_ID, BRAND_ID, HANDLE_REAL, PNL_REAL, HANDLE_RELEASED_BONUS, PNL_RELEASED_BONUS, HANDLE_PLAYABLE_BONUS, PNL_PLAYABLE_BONUS, COUNTRY, IN_GAME_PLAYABLE_BONUS_RELEASE, IN_GAME_RELEASED_BONUS_RELEASE, JPC_REAL, JPC_RELEASED_BONUS, JPC_PLAYABLE_BONUS, JPC, TIPS_REAL, TIPS_PLAYABLE_BONUS, TIPS_RELEASED_BONUS, TIPS_COUNT)
			select SUMMARY_DATE, PARTY_ID, GAME_ID, PLATFORM, GAME_NAME, GAME_COUNT, BET_COUNT, TRAN_COUNT, HANDLE, PNL, MONTH, YEAR, GAME_CATEGORY, IS_NEW, BRAND, CURRENCY, PLATFORM_ID, BRAND_ID, HANDLE_REAL, PNL_REAL, HANDLE_RELEASED_BONUS, PNL_RELEASED_BONUS, HANDLE_PLAYABLE_BONUS, PNL_PLAYABLE_BONUS, COUNTRY, IN_GAME_PLAYABLE_BONUS_RELEASE, IN_GAME_RELEASED_BONUS_RELEASE, JPC_REAL, JPC_RELEASED_BONUS, JPC_PLAYABLE_BONUS, JPC, TIPS_REAL, TIPS_PLAYABLE_BONUS, TIPS_RELEASED_BONUS, TIPS_COUNT
			from x0
	
		delete admin_all.DW_GAME_PLAYER_DAILY where SUMMARY_DATE = @StartDate
		insert into admin_all.DW_GAME_PLAYER_DAILY(SUMMARY_DATE, PARTY_ID, GAME_ID, PLATFORM, GAME_NAME, GAME_COUNT, BET_COUNT, TRAN_COUNT, HANDLE, PNL, MONTH, YEAR, GAME_CATEGORY, IS_NEW, BRAND, CURRENCY, PLATFORM_ID, BRAND_ID, HANDLE_REAL, PNL_REAL, HANDLE_RELEASED_BONUS, PNL_RELEASED_BONUS, HANDLE_PLAYABLE_BONUS, PNL_PLAYABLE_BONUS, COUNTRY, IN_GAME_PLAYABLE_BONUS_RELEASE, IN_GAME_RELEASED_BONUS_RELEASE, JPC_REAL, JPC_RELEASED_BONUS, JPC_PLAYABLE_BONUS, JPC, TIPS_REAL, TIPS_PLAYABLE_BONUS, TIPS_RELEASED_BONUS, TIPS_COUNT)
			select SUMMARY_DATE, PARTY_ID, GAME_ID, PLATFORM, GAME_NAME, GAME_COUNT, BET_COUNT, TRAN_COUNT, HANDLE, PNL, MONTH, YEAR, GAME_CATEGORY, IS_NEW, BRAND, CURRENCY, PLATFORM_ID, BRAND_ID, HANDLE_REAL, PNL_REAL, HANDLE_RELEASED_BONUS, PNL_RELEASED_BONUS, HANDLE_PLAYABLE_BONUS, PNL_PLAYABLE_BONUS, COUNTRY, IN_GAME_PLAYABLE_BONUS_RELEASE, IN_GAME_RELEASED_BONUS_RELEASE, JPC_REAL, JPC_RELEASED_BONUS, JPC_PLAYABLE_BONUS, JPC, TIPS_REAL, TIPS_PLAYABLE_BONUS, TIPS_RELEASED_BONUS, TIPS_COUNT
			from #DW_GAME_PLAYER_DAILY

___Next___:
		commit
		select @StartDate = dateadd(day, 1, @StartDate)
	end
	if @StrLastExecutionTime is not null
		exec admin_all.usp_SetRegistry 'PopulateDWGamePlayerDaily.LastExecutionTime',  @StrLastExecutionTime
	if @@procid = isnull(@CallerProcedureID, 0)
		return;
	if @ThisSanityCheckDate <= @LastSanityCheckDate
		return
	--Sanity Check
	select @StartDate = @ThisSanityCheckDate, @EndDate = dateadd(month, 1, @ThisSanityCheckDate)
	select @StrLastSanityCheckDate = convert(varchar(10), @ThisSanityCheckDate, 120)

	exec admin_all.usp_PopulateDWGamePlayerDaily @StartDate, @EndDate, @@procid
	create table #s (Currency char(3) not null, HANDLE_REAL_DIFF numeric(38, 18) not null, HANDLE_RB_DIFF numeric(38, 18) not null, HANDLE_PB_DIFF numeric(38, 18) not null, WIN_REAL_DIFF numeric(38, 18) not null, WIN_RB_DIFF numeric(38, 18) not null, WIN_PB_DIFF numeric(38, 18) not null)
	
	insert into #s
		exec admin_all.usp_SanityCheckDW @StartDate, @EndDate
	
	if not exists(select * from #s where not(HANDLE_REAL_DIFF = 0 and HANDLE_RB_DIFF = 0 and HANDLE_PB_DIFF = 0 and WIN_REAL_DIFF = 0 and WIN_RB_DIFF = 0 and WIN_PB_DIFF = 0))
	begin
		select @Subject = '(' + isnull((select Top 1 ltrim(rtrim(name)) from dbo.customer), 'Unknown') + ')DW Sanity Check SUCCEEDED for ' + @StrLastSanityCheckDate + ' and ' + convert(varchar(10), @EndDate, 120) + '.'
	end
	else
	begin
		select @Subject = '(' + isnull((select Top 1 ltrim(rtrim(name)) from dbo.customer), 'Unknown') + ')DW Sanity Check FAILED for ' + @StrLastSanityCheckDate + ' and ' + convert(varchar(10), @EndDate, 120) + '.'
	end

	select @MailBody =  N'<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<style type="text/css">
		th, td {border: 1px solid black; padding: 3px;}
		th {background-color: #ffe; font-weight: bold;}
		.right {text-align: right;}
	</style>
</head>
<body>
	<table  cellpadding="0" cellspacing="0"><thead>'+ 
			cast((select 
					(select 'Currency' as th for xml path(''), type), 
					(select 'HANDLE_REAL_DIFF' as th for xml path(''), type),
					(select 'HANDLE_RB_DIFF' as th for xml path(''), type),
					(select 'HANDLE_PB_DIFF' as th for xml path(''), type),
					(select 'WIN_REAL_DIFF' as th for xml path(''), type),
					(select 'WIN_RB_DIFF' as th for xml path(''), type),
					(select 'WIN_PB_DIFF' as th for xml path(''), type)
				for xml path('tr'), type) as varchar(max))
			+'</thead><tbody>' + cast( (select 
						(select Currency as td for xml path(''), type), 
						(select HANDLE_REAL_DIFF as td for xml path(''), type),
						(select HANDLE_RB_DIFF as td for xml path(''), type),
						(select HANDLE_PB_DIFF as td for xml path(''), type),
						(select WIN_REAL_DIFF as td for xml path(''), type),
						(select WIN_RB_DIFF as td for xml path(''), type),
						(select WIN_PB_DIFF as td for xml path(''), type)
				from #s
				for xml path('tr'), type) as varchar(max))
				
			+'
	</tbody></table>
</body>
</html>'
	
	

		
	exec maint.usp_SendMail @profile_name = 'OmegaAlert', @recipients = @Recipients, @body = @MailBody, @body_format = 'HTML', @subject = @Subject; 
	
	--select @StrLastSanityCheckDate , @StartDate, @EndDate
	exec admin_all.usp_SetRegistry 'PopulateDWGamePlayerDaily.LastSanityCheckDate',  @StrLastSanityCheckDate
end
go
--select object_name(906221749)
--exec admin_all.usp_PopulateDWGamePlayerDaily 
--select* from admin_all.REGISTRY_HASH where MAP_KEY like 'PopulateDWGamePlayerDaily%'
--delete admin_all.REGISTRY_HASH where MAP_KEY = 'PopulateDWGamePlayerDaily.Recipients'

--select convert(datetime, admin_all.fn_GetRegistry('PopulateDWGamePlayerDaily.LastExecutionTime'), 120)


--select * into tempdb.dbo. dw from admin_all.DW_GAME_PLAYER_DAILY where SUMMARY_DATE between '2018-06-01' and '2018-06-07'
--select * into tempdb.dbo. dw1 from admin_all.DW_GAME_PLAYER_DAILY where SUMMARY_DATE between '2018-06-01' and '2018-06-07'