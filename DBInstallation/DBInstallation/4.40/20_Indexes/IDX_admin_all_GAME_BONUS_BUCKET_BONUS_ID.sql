set xact_abort, nocount on


if not exists(select * from sys.indexes where name = 'IDX_admin_all_GAME_BONUS_BUCKET_BONUS_ID' and object_id = object_id('[admin_all].[GAME_BONUS_BUCKET]'))
	CREATE NONCLUSTERED INDEX IDX_admin_all_GAME_BONUS_BUCKET_BONUS_ID ON [admin_all].[GAME_BONUS_BUCKET] ([BONUS_ID])
INCLUDE ([ID],[RELEASED_BONUS_WINNINGS],[RELEASED_BONUS],[PLAYABLE_BONUS_WINNINGS],[PLAYABLE_BONUS],[MAX_RELEASED_BONUS],[MAX_PLAYABLE_BONUS])


