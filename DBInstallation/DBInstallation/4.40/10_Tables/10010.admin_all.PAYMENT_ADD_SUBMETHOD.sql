SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists(select * from sys.columns where object_id('[admin_all].[PAYMENT]') = object_id and name = 'SUBMETHOD')
  ALTER TABLE [admin_all].[PAYMENT]
  ADD [SUBMETHOD] NVARCHAR(100)
GO
