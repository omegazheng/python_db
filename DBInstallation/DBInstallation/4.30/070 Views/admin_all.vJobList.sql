set ansi_nulls, quoted_identifier on
go
create or alter view admin_all.vJobList
as
	select	'SQL' collate database_default as JobLocation,
			j.name collate database_default JobName,
			case
				when ja.run_requested_source is not null and ja.run_requested_source is not null and ja.job_history_id is null then 'Running'
				else
					case h.run_status
						when 0 then 'Failed'
						when 1 then 'Completed'
						when 2 then 'Retrying'
						when 3 then 'Terminated'
						else
							case h1.run_status
								when 0 then 'Failed'
								when 1 then 'Completed'
								when 2 then 'Retrying'
								when 3 then 'Terminated'
								else 'Idle'
							end
					end
			end collate database_default Status,
			isnull(ja.run_requested_date, maint.fn_SQLServerAgentDateTime(h1.run_date, h1.run_time)) StartDate,
			ja.stop_execution_date EndDate,
			h.message collate database_default Message,
			ja.next_scheduled_run_date NextScheduledRundate,
			j.enabled IsEnabled
	from (select top 1 session_id from  msdb.dbo.syssessions order by agent_start_date desc) s
		inner join msdb.dbo.sysjobactivity ja on ja.session_id = s.session_id
		inner join msdb.dbo.sysjobs j on j.job_id = ja.job_id
		left join msdb.dbo.sysjobhistory h on h.instance_id = ja.job_history_id
		outer apply(select top 1 h1.run_status, h1.run_date, h1.run_time, h1.run_duration  from msdb.dbo.sysjobhistory h1 where h1.job_id = j.job_id order by h1.instance_id desc) h1
	union all
	select	'Omega' as JobLocation,
			JobName,
			Status,
			StartDate,
			EndDate,
			Message,
			null as NextScheduledRundate,
			1 as IsEnabled
	from admin_all.JobStatus

go
