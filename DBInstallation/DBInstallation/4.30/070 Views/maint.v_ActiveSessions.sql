set ansi_nulls, quoted_identifier on
go
create or alter view maint.v_ActiveSessions
as
	select  r.Session_ID SessionID, r.start_time StartDate,r.blocking_session_id BlockingSessionID, datediff(second,r.start_time, getdate()) DurationInSecond, 
		r.wait_type WaitType, r.wait_time WaitTime, r.wait_resource WaitResource, r.last_wait_type LastWaitType,
		r.cpu_time CPU, r.reads Reads, r.writes Writes, r.logical_reads LogicalReads, r.total_elapsed_time TotalElapsedTime,
		t.text SQLText, p.query_plan QueryPlan, 
		r.granted_query_memory GrantedMemory, r.nest_level NestedLevel, r.row_count [RowCount], r.transaction_isolation_level TransactionIsolationLevel, 
		r.executing_managed_code ExecutingManagedCode
	from sys.dm_exec_requests r
		cross apply sys.dm_exec_sql_text(r.sql_handle) t
		outer apply sys.dm_exec_query_plan(r.plan_handle) p
	where r.session_id not in (@@SPID)

go
