set ansi_nulls, quoted_identifier on
go
create or alter view [admin_all].[v_metrics_monthly] as select     MONTH,     sum(SIGNUP) as SIGNUP,     sum(FUNDED) as FUNDED from     v_metrics_daily group by     month
go
