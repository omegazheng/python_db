if object_id('[external_mpt].[FK_external_mpt_USER_CONF_BRAND_CCLEVEL_BRAND_CCLEVEL_CCLEVEL_ID]') is null
	alter table [external_mpt].[USER_CONF] add constraint [FK_external_mpt_USER_CONF_BRAND_CCLEVEL_BRAND_CCLEVEL_CCLEVEL_ID] foreign key ([CCLEVEL_ID]) references [admin_all].[BRAND_CCLEVEL]([ID]) ON DELETE SET NULL;

	--alter table [external_mpt].[USER_CONF] drop constraint [FK_external_mpt_USER_CONF_BRAND_CCLEVEL_BRAND_CCLEVEL_CCLEVEL_ID] 