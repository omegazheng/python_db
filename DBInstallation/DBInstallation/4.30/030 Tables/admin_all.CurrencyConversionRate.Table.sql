SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[CurrencyConversionRate]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[CurrencyConversionRate](
	[RateID] [bigint] IDENTITY(1,1) NOT NULL,
	[CurrencyFrom] [nchar](3) NOT NULL,
	[CurrencyTo] [nchar](3) NOT NULL,
	[Rate] [numeric](38, 18) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[ExpiryDate] [datetime] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_admin_all_CurrencyConversionRate] PRIMARY KEY CLUSTERED 
(
	[RateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[CurrencyConversionRate]') AND name = N'IDX_admin_all_CurrencyConversionRate_CurrencyFrom_CurrencyTo_EffectiveDate_ExpiryDate')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_admin_all_CurrencyConversionRate_CurrencyFrom_CurrencyTo_EffectiveDate_ExpiryDate] ON [admin_all].[CurrencyConversionRate]
(
	[CurrencyFrom] ASC,
	[CurrencyTo] ASC,
	[EffectiveDate] ASC,
	[ExpiryDate] ASC
)
INCLUDE ( 	[Rate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[CurrencyConversionRate]') AND name = N'IDX_admin_all_CurrencyConversionRate_CurrencyTo')
CREATE NONCLUSTERED INDEX [IDX_admin_all_CurrencyConversionRate_CurrencyTo] ON [admin_all].[CurrencyConversionRate]
(
	[CurrencyTo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
