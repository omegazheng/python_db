SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[FilterType]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[FilterType](
	[FilterType] [varchar](100) NOT NULL,
	[Comment] [varchar](128) NULL,
	[FromValueQuery] [varchar](max) NULL,
	[FromValueQueryEvaluationType] [varchar](20) NOT NULL,
	[FromValueAdjustment] [varchar](max) NULL,
	[ToValueQuery] [varchar](max) NULL,
	[ToValueQueryEvaluationType] [varchar](20) NOT NULL,
	[ToValueAdjustment] [varchar](max) NULL,
 CONSTRAINT [PK_maint_FilterType] PRIMARY KEY CLUSTERED 
(
	[FilterType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_FilterType_FromValueQueryEvaluationType]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[FilterType] ADD  CONSTRAINT [DF_maint_FilterType_FromValueQueryEvaluationType]  DEFAULT ('None') FOR [FromValueQueryEvaluationType]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_FilterType_ToValueQueryEvaluationType]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[FilterType] ADD  CONSTRAINT [DF_maint_FilterType_ToValueQueryEvaluationType]  DEFAULT ('None') FOR [ToValueQueryEvaluationType]
END
GO
