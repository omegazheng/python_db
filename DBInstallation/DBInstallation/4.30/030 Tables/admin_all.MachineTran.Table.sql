SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTran]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MachineTran](
	[MachineTranID] [bigint] NOT NULL,
	[PartyID] [int] NOT NULL,
	[MachineID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_admin_all_MachineTran] PRIMARY KEY CLUSTERED 
(
	[MachineTranID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTran]') AND name = N'IDX_admin_all_MachineTran_MachineID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_MachineTran_MachineID] ON [admin_all].[MachineTran]
(
	[MachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTran]') AND name = N'IDX_admin_all_MachineTran_PartyID_MachineID_EndDate')
CREATE NONCLUSTERED INDEX [IDX_admin_all_MachineTran_PartyID_MachineID_EndDate] ON [admin_all].[MachineTran]
(
	[PartyID] ASC,
	[MachineID] ASC,
	[EndDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MachineTran_StartDate]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MachineTran] ADD  CONSTRAINT [DF_admin_all_MachineTran_StartDate]  DEFAULT (getdate()) FOR [StartDate]
END
GO
