SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[ReplicationLastExecution]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[ReplicationLastExecution](
	[ConfigurationID] [int] NOT NULL,
	[FromValue] [sql_variant] NULL,
	[ToValue] [sql_variant] NULL,
	[Rows] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[Error] [varchar](max) NULL,
	[ReplicationHistoryID] [bigint] NULL,
	[SessionID] [int] NULL,
	[ApplicationName] [varchar](128) NULL,
	[LoginName] [varchar](128) NULL,
 CONSTRAINT [PK_maint_ReplicationLastExecution] PRIMARY KEY CLUSTERED 
(
	[ConfigurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
