SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[GameWinnerList]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[GameWinnerList](
	[Datetime] [datetime] NOT NULL,
	[GameInfoID] [int] NOT NULL,
	[GameTranIDHash] [uniqueidentifier] NOT NULL,
	[AccountTranID] [bigint] NOT NULL,
	[BrandID] [int] NOT NULL,
	[PartyID] [int] NOT NULL,
	[BetAmount] [numeric](38, 18) NOT NULL,
	[WonAmount] [numeric](38, 18) NOT NULL,
	[Currency] [nvarchar](3) NOT NULL,
	[ConvertedWonAmount] [numeric](38, 18) NULL,
	[PayoutRate]  AS (([WonAmount]*(100))/nullif([BetAmount],(0))),
	[Rank] [int] NULL,
 CONSTRAINT [PK_admin_all_GameWinnerList] PRIMARY KEY CLUSTERED 
(
	[Datetime] ASC,
	[GameInfoID] ASC,
	[GameTranIDHash] ASC,
	[PartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UK_admin_all_GameWinnerList] UNIQUE NONCLUSTERED 
(
	[GameInfoID] ASC,
	[GameTranIDHash] ASC,
	[PartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
