SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[agency].[commission_payment]') AND type in (N'U'))
BEGIN
CREATE TABLE [agency].[commission_payment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[state] [varchar](20) NULL,
	[agent_id] [int] NULL,
	[plan_id] [int] NULL,
	[start_date] [datetime] NULL,
	[end_date] [datetime] NULL,
	[value] [numeric](38, 18) NULL,
	[comments] [varchar](255) NULL,
	[TYPE] [varchar](20) NULL,
	[PRODUCT_ID] [int] NULL,
	[processed_date] [datetime] NULL,
 CONSTRAINT [PK_agency_commission_payment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
