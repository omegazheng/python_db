SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[HOUSE]') AND type in (N'U'))
BEGIN
CREATE TABLE [external_mpt].[HOUSE](
	[HOUSEID] [int] IDENTITY(201,1) NOT NULL,
	[HOUSENAME] [varchar](16) NOT NULL,
	[PASSWORD] [varchar](16) NOT NULL,
	[COOKIE] [varchar](50) NULL,
	[FIRST_NAME] [varchar](20) NOT NULL,
	[LAST_NAME] [varchar](20) NOT NULL,
	[SITETYPE] [varchar](50) NOT NULL,
	[ORGANIZATION] [varchar](50) NULL,
	[ADDRESS] [varchar](100) NOT NULL,
	[CITY] [varchar](30) NULL,
	[PROVINCE] [varchar](30) NULL,
	[COUNTRY] [varchar](50) NOT NULL,
	[POSTAL_CODE] [varchar](15) NULL,
	[PHONE] [varchar](18) NULL,
	[EMAIL] [varchar](75) NOT NULL,
	[BIRTHDATE] [datetime] NULL,
	[MIN_CHECK_AMOUNT] [numeric](38, 18) NOT NULL,
	[URL] [varchar](100) NULL,
	[HINT_QUESTION] [varchar](100) NULL,
	[ANSWER] [varchar](25) NULL,
	[WEEKLY_CASHFLOW] [numeric](38, 18) NOT NULL,
	[REGISTRATION_DATE] [datetime] NOT NULL,
	[PAYRATE_TYPE] [varchar](2) NOT NULL,
	[GRAPHICSID] [numeric](5, 0) NOT NULL,
	[ADMIN_FLAG] [char](1) NOT NULL,
	[HOUSE_CASINO_NAME] [varchar](50) NOT NULL,
	[PRE_URL] [varchar](100) NULL,
	[PRE_HOUSE_CASINO_NAME] [varchar](50) NULL,
	[POKER_PAYRATE_TYPE] [varchar](2) NOT NULL,
	[EXCLUSIVE_FLAG] [char](1) NOT NULL,
 CONSTRAINT [PK_external_mpt_HOUSE] PRIMARY KEY CLUSTERED 
(
	[HOUSEID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_SITETYPE]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_SITETYPE]  DEFAULT ('Unknown') FOR [SITETYPE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_ORGANIZATION]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_ORGANIZATION]  DEFAULT ('Unknown') FOR [ORGANIZATION]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_CITY]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_CITY]  DEFAULT ('Unknown') FOR [CITY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_MIN_CHECK_AMOUNT]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_MIN_CHECK_AMOUNT]  DEFAULT ((20)) FOR [MIN_CHECK_AMOUNT]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_WEEKLY_CASHFLOW]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_WEEKLY_CASHFLOW]  DEFAULT ((0)) FOR [WEEKLY_CASHFLOW]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_REGISTRATION_DATE]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_REGISTRATION_DATE]  DEFAULT (getdate()) FOR [REGISTRATION_DATE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_PAYRATE_TYPE]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_PAYRATE_TYPE]  DEFAULT ('A') FOR [PAYRATE_TYPE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_GRAPHICSID]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_GRAPHICSID]  DEFAULT ((0)) FOR [GRAPHICSID]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_ADMIN_FLAG]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_ADMIN_FLAG]  DEFAULT ('0') FOR [ADMIN_FLAG]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_HOUSE_CASINO_NAME]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_HOUSE_CASINO_NAME]  DEFAULT ('no name') FOR [HOUSE_CASINO_NAME]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_POKER_PAYRATE_TYPE]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_POKER_PAYRATE_TYPE]  DEFAULT ('A') FOR [POKER_PAYRATE_TYPE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_HOUSE_EXCLUSIVE_FLAG]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[HOUSE] ADD  CONSTRAINT [DF_external_mpt_HOUSE_EXCLUSIVE_FLAG]  DEFAULT ('0') FOR [EXCLUSIVE_FLAG]
END
GO
