SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[GEO_IP_RANGE]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[GEO_IP_RANGE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[begin_ip] [varchar](20) NOT NULL,
	[end_ip] [varchar](20) NOT NULL,
	[begin_ip_num] [bigint] NOT NULL,
	[end_ip_num] [bigint] NOT NULL,
	[country_code] [char](2) NOT NULL,
 CONSTRAINT [PK_admin_all_GEO_IP_RANGE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[GEO_IP_RANGE]') AND name = N'IDX_admin_all_GEO_IP_RANGE_country_code')
CREATE NONCLUSTERED INDEX [IDX_admin_all_GEO_IP_RANGE_country_code] ON [admin_all].[GEO_IP_RANGE]
(
	[country_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
