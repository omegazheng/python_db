SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[LargeValue]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[LargeValue](
	[ValueID] [uniqueidentifier] NOT NULL,
	[IsCompressed] [bit] NOT NULL,
	[Data] [varbinary](max) NULL,
 CONSTRAINT [PK_admin_all_LargeValue] PRIMARY KEY CLUSTERED 
(
	[ValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
EXECUTE sp_tableoption @TableNamePattern = N'[admin_all].[LargeValue]', @OptionName = N'large value types out of row', @OptionValue = N'1';