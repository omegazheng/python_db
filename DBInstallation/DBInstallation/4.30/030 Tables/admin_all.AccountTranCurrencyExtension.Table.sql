SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT *
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[admin_all].[AccountTranCurrencyExtension]') AND type in (N'U'))
BEGIN
	CREATE TABLE [admin_all].[AccountTranCurrencyExtension]
	(
		[AccountTranID] [bigint] NOT NULL,
		[RateID] [bigint] NOT NULL,
		[Currency] [nchar](3) NOT NULL,
		[AmountReal] [numeric](38, 18) NOT NULL,
		[ReleasedBonus] [numeric](38, 18) NOT NULL,
		[PlayableBonus] [numeric](38, 18) NOT NULL,
		CONSTRAINT [PK_admin_all_AccountTranCurrencyExtension] PRIMARY KEY CLUSTERED 
(
	[Currency] ASC,
	[AccountTranID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
	)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT *
FROM sys.indexes
WHERE object_id = OBJECT_ID(N'[admin_all].[AccountTranCurrencyExtension]') AND name = N'IDX_admin_all_AccountTranCurrencyExtension_AccountTranID_Currency')
CREATE NONCLUSTERED INDEX [IDX_admin_all_AccountTranCurrencyExtension_AccountTranID_Currency] ON [admin_all].[AccountTranCurrencyExtension]
(
	[AccountTranID] ASC,
	[Currency] ASC
)
INCLUDE ( 	[AmountReal],
	[ReleasedBonus],
	[PlayableBonus],
	[RateID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
GO
IF NOT EXISTS (SELECT *
FROM sys.indexes
WHERE object_id = OBJECT_ID(N'[admin_all].[AccountTranCurrencyExtension]') AND name = N'IDX_admin_all_AccountTranCurrencyExtension_RateID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_AccountTranCurrencyExtension_RateID] ON [admin_all].[AccountTranCurrencyExtension]
(
	[RateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
GO
