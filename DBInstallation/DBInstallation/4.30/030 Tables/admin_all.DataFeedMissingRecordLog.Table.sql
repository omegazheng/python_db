SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DataFeedMissingRecordLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[DataFeedMissingRecordLog](
	[LogDate] [datetime] NOT NULL,
	[DateFrom] [datetime] NOT NULL,
	[DateTo] [datetime] NOT NULL,
	[Count] [int] NULL,
 CONSTRAINT [PK_admin_all_DataFeedMissingRecordLog] PRIMARY KEY CLUSTERED 
(
	[LogDate] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
)
END
GO
