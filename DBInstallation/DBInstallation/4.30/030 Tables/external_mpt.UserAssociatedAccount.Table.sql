SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[UserAssociatedAccount]') AND type in (N'U'))
BEGIN
CREATE TABLE [external_mpt].[UserAssociatedAccount](
	[PartyID] [int] NOT NULL,
	[AssociatedPartyID] [int] NOT NULL,
	[IsPrimary] [bit] NOT NULL,
 CONSTRAINT [PK_external_mpt_UserAssociatedAccount] PRIMARY KEY CLUSTERED 
(
	[AssociatedPartyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[external_mpt].[UserAssociatedAccount]') AND name = N'IDX_external_mpt_UserAssociatedAccount_PartyID')
CREATE NONCLUSTERED INDEX [IDX_external_mpt_UserAssociatedAccount_PartyID] ON [external_mpt].[UserAssociatedAccount]
(
	[PartyID] ASC
)
INCLUDE ( 	[IsPrimary]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[external_mpt].[UserAssociatedAccount]') AND name = N'IDX_external_mpt_UserAssociatedAccount_PartyID_IsPrimary')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_external_mpt_UserAssociatedAccount_PartyID_IsPrimary] ON [external_mpt].[UserAssociatedAccount]
(
	[PartyID] ASC,
	[IsPrimary] ASC
)
WHERE ([IsPrimary]=(1))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[external_mpt].[DF_external_mpt_UserAssociatedAccount_IsPrimary]') AND type = 'D')
BEGIN
ALTER TABLE [external_mpt].[UserAssociatedAccount] ADD  CONSTRAINT [DF_external_mpt_UserAssociatedAccount_IsPrimary]  DEFAULT ((0)) FOR [IsPrimary]
END
GO
