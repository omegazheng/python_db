SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[BRAND_ALIAS]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[BRAND_ALIAS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BRANDID] [int] NOT NULL,
	[DOMAIN] [varchar](100) NOT NULL,
	[SKIN_NAME] [varchar](30) NULL,
 CONSTRAINT [PK_admin_all_BRAND_ALIAS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_BRAND_ALIAS_DOMAIN] UNIQUE NONCLUSTERED 
(
	[DOMAIN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[BRAND_ALIAS]') AND name = N'IDX_admin_all_BRAND_ALIAS_BRANDID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_BRAND_ALIAS_BRANDID] ON [admin_all].[BRAND_ALIAS]
(
	[BRANDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
