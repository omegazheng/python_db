SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MANUAL_BANK_AGENT]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MANUAL_BANK_AGENT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BRAND_ID] [int] NOT NULL,
	[FIRST_NAME] [nvarchar](255) NULL,
	[LAST_NAME] [nvarchar](255) NULL,
	[EMAIL] [nvarchar](255) NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
 CONSTRAINT [PK_admin_all_MANUAL_BANK_AGENT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_MANUAL_BANK_AGENT_BRAND_ID_EMAIL] UNIQUE NONCLUSTERED 
(
	[BRAND_ID] ASC,
	[EMAIL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_MANUAL_BANK_AGENT_IS_ACTIVE]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[MANUAL_BANK_AGENT] ADD  CONSTRAINT [DF_admin_all_MANUAL_BANK_AGENT_IS_ACTIVE]  DEFAULT ((0)) FOR [IS_ACTIVE]
END
GO
