SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[ReplicationHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[ReplicationHistory](
	[ReplicationHistoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[ConfigurationID] [int] NOT NULL,
	[FromValue] [sql_variant] NULL,
	[ToValue] [sql_variant] NULL,
	[Rows] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[Error] [varchar](max) NULL,
	[ApplicationName] [varchar](128) NULL,
	[LoginName] [varchar](128) NULL,
 CONSTRAINT [PK_maint_ReplicationHistory] PRIMARY KEY CLUSTERED 
(
	[ConfigurationID] ASC,
	[ReplicationHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
