SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[WORLDPAY_TOKEN]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[WORLDPAY_TOKEN](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PARTYID] [int] NOT NULL,
	[BANK_ALIAS] [nvarchar](30) NOT NULL,
	[TOKEN] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_admin_all_WORLDPAY_TOKEN] PRIMARY KEY CLUSTERED 
(
	[TOKEN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_WORLDPAY_TOKEN_PARTYID_BANK_ALIAS] UNIQUE NONCLUSTERED 
(
	[PARTYID] ASC,
	[BANK_ALIAS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_WORLDPAY_TOKEN_PARTYID_TOKEN] UNIQUE NONCLUSTERED 
(
	[PARTYID] ASC,
	[TOKEN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
