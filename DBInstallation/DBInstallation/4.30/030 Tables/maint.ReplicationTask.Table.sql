SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[ReplicationTask]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[ReplicationTask](
	[ConfigurationID] [int] NOT NULL,
	[SessionID] [int] NULL,
 CONSTRAINT [PK_maint_ReplicationTask] PRIMARY KEY CLUSTERED 
(
	[ConfigurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
