SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[ACCOUNT_TRAN_UNDERFLOW]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[ACCOUNT_TRAN_UNDERFLOW](
	[ID] [bigint] NOT NULL,
	[ACCOUNT_ID] [int] NOT NULL,
	[DATETIME] [datetime] NOT NULL,
	[AMOUNT_REAL] [numeric](38, 18) NOT NULL,
	[AMOUNT_RELEASED_BONUS] [numeric](38, 18) NOT NULL,
	[AMOUNT_PLAYABLE_BONUS] [numeric](38, 18) NOT NULL,
 CONSTRAINT [PK_admin_all_ACCOUNT_TRAN_UNDERFLOW] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[ACCOUNT_TRAN_UNDERFLOW]') AND name = N'IDX_admin_all_ACCOUNT_TRAN_UNDERFLOW_DATETIME')
CREATE NONCLUSTERED INDEX [IDX_admin_all_ACCOUNT_TRAN_UNDERFLOW_DATETIME] ON [admin_all].[ACCOUNT_TRAN_UNDERFLOW]
(
	[DATETIME] ASC
)
INCLUDE ( 	[ACCOUNT_ID],
	[AMOUNT_REAL],
	[AMOUNT_RELEASED_BONUS],
	[AMOUNT_PLAYABLE_BONUS]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_TRAN_UNDERFLOW_AMOUNT_REAL]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT_TRAN_UNDERFLOW] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_TRAN_UNDERFLOW_AMOUNT_REAL]  DEFAULT ((0)) FOR [AMOUNT_REAL]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_TRAN_UNDERFLOW_AMOUNT_RELEASED_BONUS]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT_TRAN_UNDERFLOW] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_TRAN_UNDERFLOW_AMOUNT_RELEASED_BONUS]  DEFAULT ((0)) FOR [AMOUNT_RELEASED_BONUS]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_TRAN_UNDERFLOW_AMOUNT_PLAYABLE_BONUS]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT_TRAN_UNDERFLOW] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_TRAN_UNDERFLOW_AMOUNT_PLAYABLE_BONUS]  DEFAULT ((0)) FOR [AMOUNT_PLAYABLE_BONUS]
END
GO
