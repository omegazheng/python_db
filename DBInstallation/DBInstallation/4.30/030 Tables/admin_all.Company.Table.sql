SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[Company]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[Company](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[ParentCompanyID] [int] NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_admin_all_Company] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_Company_Code] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[Company]') AND name = N'IDX_admin_all_Company_ParentCompanyID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_Company_ParentCompanyID] ON [admin_all].[Company]
(
	[ParentCompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_Company_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[Company] ADD  CONSTRAINT [DF_admin_all_Company_IsActive]  DEFAULT ((1)) FOR [IsActive]
END
GO
