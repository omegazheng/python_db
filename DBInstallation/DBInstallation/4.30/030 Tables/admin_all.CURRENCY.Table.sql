SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[CURRENCY]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[CURRENCY](
	[iso_code] [nchar](3) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[numeric_code] [char](10) NULL,
	[symbol] [nchar](10) NULL,
	[is_default] [tinyint] NOT NULL,
	[symbol_code] [varchar](50) NULL,
	[NUMBER_DECIMAL_POINT] [int] NULL,
	[is_virtual] [bit] NULL,
	[TYPE] [char](1) NULL,
	[REFRESH_INTERVAL] [int] NOT NULL,
	[AlertThreshold] [int] NULL,
	[StopThreshold] [int] NULL,
 CONSTRAINT [PK_admin_all_CURRENCY] PRIMARY KEY CLUSTERED 
(
	[iso_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CURRENCY_is_default]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CURRENCY] ADD  CONSTRAINT [DF_admin_all_CURRENCY_is_default]  DEFAULT ((0)) FOR [is_default]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CURRENCY_is_virtual]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CURRENCY] ADD  CONSTRAINT [DF_admin_all_CURRENCY_is_virtual]  DEFAULT ((0)) FOR [is_virtual]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CURRENCY_TYPE]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CURRENCY] ADD  CONSTRAINT [DF_admin_all_CURRENCY_TYPE]  DEFAULT ('C') FOR [TYPE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CURRENCY_REFRESH_INTERVAL]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CURRENCY] ADD  CONSTRAINT [DF_admin_all_CURRENCY_REFRESH_INTERVAL]  DEFAULT ((1440)) FOR [REFRESH_INTERVAL]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CURRENCY_AlertThreshold]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CURRENCY] ADD  CONSTRAINT [DF_admin_all_CURRENCY_AlertThreshold]  DEFAULT ((2160)) FOR [AlertThreshold]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CURRENCY_StopThreshold]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CURRENCY] ADD  CONSTRAINT [DF_admin_all_CURRENCY_StopThreshold]  DEFAULT ((2880)) FOR [StopThreshold]
END
GO
