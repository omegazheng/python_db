SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SafePlay].[SafePlayEFTState]') AND type in (N'U'))
BEGIN
CREATE TABLE [SafePlay].[SafePlayEFTState](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountTranID] [bigint] NOT NULL,
	[ExtTranID] [varchar](100) NULL,
	[State] [nvarchar](20) NULL,
	[RequestTime] [datetime] NOT NULL,
	[LastAccessTime] [datetime] NULL,
	[Reference] [nvarchar](300) NULL,
 CONSTRAINT [PK_SAFEPLAY_EFT_STATE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[SafePlay].[SafePlayEFTState]') AND name = N'IDX_SafePlay_SafePlayEFTState_AccountTranID')
CREATE NONCLUSTERED INDEX [IDX_SafePlay_SafePlayEFTState_AccountTranID] ON [SafePlay].[SafePlayEFTState]
(
	[AccountTranID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
