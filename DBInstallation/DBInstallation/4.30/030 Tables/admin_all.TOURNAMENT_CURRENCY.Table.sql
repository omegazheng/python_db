SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[TOURNAMENT_CURRENCY]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[TOURNAMENT_CURRENCY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TOURNAMENT_ID] [int] NOT NULL,
	[CURRENCY] [nchar](3) NOT NULL,
	[MIN_BET_AMOUNT] [numeric](38, 18) NOT NULL,
	[ENTRY_COST] [numeric](38, 18) NULL,
	[JACKPOT] [numeric](38, 18) NULL,
	[CREATED_DATE] [datetime] NOT NULL,
	[LAST_MODIFIED_DATE] [datetime] NULL,
 CONSTRAINT [PK_admin_all_TOURNAMENT_CURRENCY] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[TOURNAMENT_CURRENCY]') AND name = N'IDX_admin_all_TOURNAMENT_CURRENCY_CURRENCY')
CREATE NONCLUSTERED INDEX [IDX_admin_all_TOURNAMENT_CURRENCY_CURRENCY] ON [admin_all].[TOURNAMENT_CURRENCY]
(
	[CURRENCY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[TOURNAMENT_CURRENCY]') AND name = N'IDX_admin_all_TOURNAMENT_CURRENCY_TOURNAMENT_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_TOURNAMENT_CURRENCY_TOURNAMENT_ID] ON [admin_all].[TOURNAMENT_CURRENCY]
(
	[TOURNAMENT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_TOURNAMENT_CURRENCY_MIN_BET_AMOUNT]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[TOURNAMENT_CURRENCY] ADD  CONSTRAINT [DF_admin_all_TOURNAMENT_CURRENCY_MIN_BET_AMOUNT]  DEFAULT ((0)) FOR [MIN_BET_AMOUNT]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_TOURNAMENT_CURRENCY_ENTRY_COST]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[TOURNAMENT_CURRENCY] ADD  CONSTRAINT [DF_admin_all_TOURNAMENT_CURRENCY_ENTRY_COST]  DEFAULT ((0)) FOR [ENTRY_COST]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_TOURNAMENT_CURRENCY_JACKPOT]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[TOURNAMENT_CURRENCY] ADD  CONSTRAINT [DF_admin_all_TOURNAMENT_CURRENCY_JACKPOT]  DEFAULT ((0)) FOR [JACKPOT]
END
GO
