SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[ACCOUNT]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[ACCOUNT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[PARTYID] [int] NOT NULL,
	[BALANCE_REAL] [numeric](38, 18) NOT NULL,
	[RELEASED_BONUS] [numeric](38, 18) NOT NULL,
	[PLAYABLE_BONUS] [numeric](38, 18) NOT NULL,
	[RAW_LOYALTY_POINTS] [bigint] NOT NULL,
	[SECONDARY_BALANCE] [numeric](38, 18) NOT NULL,
	[ROWVERSION] [timestamp] NOT NULL,
	[UNPAID_CIT] [numeric](38, 18) NOT NULL,
	[CIT] [numeric](38, 18) NOT NULL,
 CONSTRAINT [PK_admin_all_ACCOUNT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[ACCOUNT]') AND name = N'IDX_admin_all_ACCOUNT_PARTYID')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_admin_all_ACCOUNT_PARTYID] ON [admin_all].[ACCOUNT]
(
	[PARTYID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_BALANCE_REAL]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_BALANCE_REAL]  DEFAULT ((0)) FOR [BALANCE_REAL]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_RELEASED_BONUS]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_RELEASED_BONUS]  DEFAULT ((0)) FOR [RELEASED_BONUS]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_PLAYABLE_BONUS]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_PLAYABLE_BONUS]  DEFAULT ((0)) FOR [PLAYABLE_BONUS]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_RAW_LOYALTY_POINTS]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_RAW_LOYALTY_POINTS]  DEFAULT ((0)) FOR [RAW_LOYALTY_POINTS]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_SECONDARY_BALANCE]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_SECONDARY_BALANCE]  DEFAULT ((0)) FOR [SECONDARY_BALANCE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_UNPAID_CIT]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_UNPAID_CIT]  DEFAULT ((0)) FOR [UNPAID_CIT]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_ACCOUNT_CIT]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[ACCOUNT] ADD  CONSTRAINT [DF_admin_all_ACCOUNT_CIT]  DEFAULT ((0)) FOR [CIT]
END
GO
