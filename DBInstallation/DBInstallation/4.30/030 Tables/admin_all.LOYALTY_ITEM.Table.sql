SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[LOYALTY_ITEM]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[LOYALTY_ITEM](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [varchar](50) NOT NULL,
	[PRICE] [int] NOT NULL,
	[STATUS] [varchar](25) NOT NULL,
	[UPDATE_DATE] [datetime] NOT NULL,
	[ITEM_CODE] [varchar](50) NOT NULL,
	[IS_FREESPIN] [bit] NOT NULL,
	[NO_OF_FREESPIN] [smallint] NOT NULL,
	[GAME_INFO_ID] [int] NULL,
	[GAME_INFO_ID2] [int] NULL,
	[TYPE] [varchar](30) NULL,
	[BONUS_PLAN_ID] [int] NULL,
	[BRAND_ID] [int] NULL,
	[IMAGE_URL] [varchar](1000) NULL,
	[IS_DELETED] [bit] NOT NULL,
 CONSTRAINT [PK_admin_all_LOYALTY_ITEM] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[LOYALTY_ITEM]') AND name = N'IDX_admin_all_LOYALTY_ITEM_BONUS_PLAN_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_LOYALTY_ITEM_BONUS_PLAN_ID] ON [admin_all].[LOYALTY_ITEM]
(
	[BONUS_PLAN_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[LOYALTY_ITEM]') AND name = N'IDX_admin_all_LOYALTY_ITEM_BRAND_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_LOYALTY_ITEM_BRAND_ID] ON [admin_all].[LOYALTY_ITEM]
(
	[BRAND_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[LOYALTY_ITEM]') AND name = N'IDX_admin_all_LOYALTY_ITEM_ITEM_CODE')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_admin_all_LOYALTY_ITEM_ITEM_CODE] ON [admin_all].[LOYALTY_ITEM]
(
	[ITEM_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_LOYALTY_ITEM_IS_FREESPIN]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[LOYALTY_ITEM] ADD  CONSTRAINT [DF_admin_all_LOYALTY_ITEM_IS_FREESPIN]  DEFAULT ((0)) FOR [IS_FREESPIN]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_LOYALTY_ITEM_NO_OF_FREESPIN]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[LOYALTY_ITEM] ADD  CONSTRAINT [DF_admin_all_LOYALTY_ITEM_NO_OF_FREESPIN]  DEFAULT ((0)) FOR [NO_OF_FREESPIN]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_LOYALTY_ITEM_TYPE]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[LOYALTY_ITEM] ADD  CONSTRAINT [DF_admin_all_LOYALTY_ITEM_TYPE]  DEFAULT ('NORMAL_ITEM') FOR [TYPE]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_LOYALTY_ITEM_IS_DELETED]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[LOYALTY_ITEM] ADD  CONSTRAINT [DF_admin_all_LOYALTY_ITEM_IS_DELETED]  DEFAULT ((0)) FOR [IS_DELETED]
END
GO
