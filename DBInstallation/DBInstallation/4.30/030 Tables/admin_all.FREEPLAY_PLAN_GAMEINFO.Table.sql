SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[FREEPLAY_PLAN_GAMEINFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[FREEPLAY_PLAN_GAMEINFO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FREEPLAY_PLAN_ID] [int] NOT NULL,
	[GAMEINFO_ID] [int] NOT NULL,
 CONSTRAINT [PK_admin_all_FREEPLAY_PLAN_GAMEINFO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_FREEPLAY_PLAN_GAMEINFO_FREEPLAY_PLAN_ID_GAMEINFO_ID] UNIQUE NONCLUSTERED 
(
	[FREEPLAY_PLAN_ID] ASC,
	[GAMEINFO_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[FREEPLAY_PLAN_GAMEINFO]') AND name = N'IDX_admin_all_FREEPLAY_PLAN_GAMEINFO_GAMEINFO_ID')
CREATE NONCLUSTERED INDEX [IDX_admin_all_FREEPLAY_PLAN_GAMEINFO_GAMEINFO_ID] ON [admin_all].[FREEPLAY_PLAN_GAMEINFO]
(
	[GAMEINFO_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
