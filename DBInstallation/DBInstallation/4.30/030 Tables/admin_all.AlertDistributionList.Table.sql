SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[AlertDistributionList]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[AlertDistributionList](
	[DistributionListID] [int] IDENTITY(1,1) NOT NULL,
	[AlertCode] [varchar](20) NOT NULL,
	[SeverityLevelFrom] [int] NOT NULL,
	[SeverityLevelTo] [int] NOT NULL,
	[Recipients] [varchar](max) NOT NULL,
	[Subject] [nvarchar](255) NOT NULL,
	[Importance] [varchar](6) NULL,
	[IncludeStackTrace] [bit] NOT NULL,
	[IncludeMessage] [bit] NOT NULL,
	[Information] [nvarchar](max) NULL,
 CONSTRAINT [PK_admin_all_AlertDistributionList] PRIMARY KEY CLUSTERED 
(
	[AlertCode] ASC,
	[SeverityLevelFrom] ASC,
	[SeverityLevelTo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_AlertDistributionList] UNIQUE NONCLUSTERED 
(
	[DistributionListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertDistributionList_SeverityLevelFrom]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertDistributionList] ADD  CONSTRAINT [DF_admin_all_AlertDistributionList_SeverityLevelFrom]  DEFAULT ((0)) FOR [SeverityLevelFrom]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertDistributionList_SeverityLevelTo]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertDistributionList] ADD  CONSTRAINT [DF_admin_all_AlertDistributionList_SeverityLevelTo]  DEFAULT ((100)) FOR [SeverityLevelTo]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertDistributionList_Importance]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertDistributionList] ADD  CONSTRAINT [DF_admin_all_AlertDistributionList_Importance]  DEFAULT ('Normal') FOR [Importance]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertDistributionList_IncludeStackTrace]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertDistributionList] ADD  CONSTRAINT [DF_admin_all_AlertDistributionList_IncludeStackTrace]  DEFAULT ((1)) FOR [IncludeStackTrace]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertDistributionList_IncludeMessage]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertDistributionList] ADD  CONSTRAINT [DF_admin_all_AlertDistributionList_IncludeMessage]  DEFAULT ((1)) FOR [IncludeMessage]
END
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[admin_all].[CK_admin_all_AlertDistributionList:Importance must be Low, Normal, or High]') AND parent_object_id = OBJECT_ID(N'[admin_all].[AlertDistributionList]'))
ALTER TABLE [admin_all].[AlertDistributionList]  WITH CHECK ADD  CONSTRAINT [CK_admin_all_AlertDistributionList:Importance must be Low, Normal, or High] CHECK  (([Importance]='High' OR [Importance]='Normal' OR [Importance]='Low'))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[admin_all].[CK_admin_all_AlertDistributionList:Importance must be Low, Normal, or High]') AND parent_object_id = OBJECT_ID(N'[admin_all].[AlertDistributionList]'))
ALTER TABLE [admin_all].[AlertDistributionList] CHECK CONSTRAINT [CK_admin_all_AlertDistributionList:Importance must be Low, Normal, or High]
GO
