SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[SafePlay].[SafePlayMachineInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [SafePlay].[SafePlayMachineInfo](
	[ID] [int] NOT NULL,
	[QRCode] [nvarchar](100) NOT NULL,
	[SiteCode] [nvarchar](100) NULL,
	[SiteName] [nvarchar](100) NULL,
	[SiteTown] [nvarchar](100) NULL,
	[SitePostalCode] [nvarchar](100) NULL,
	[AssetNumber] [nvarchar](100) NULL,
	[SerialNumber] [nvarchar](100) NULL,
	[GameName] [nvarchar](100) NULL,
	[GameCategory] [int] NULL,
	[Manufacturer] [nvarchar](100) NULL,
	[OperatorCode] [nvarchar](100) NULL,
	[OperatorName] [nvarchar](100) NULL,
	[CompanyCode] [nvarchar](100) NULL,
	[CompanyName] [nvarchar](100) NULL,
	[SubCompanyCode] [nvarchar](100) NULL,
	[SubCompanyName] [nvarchar](100) NULL,
	[PromoSupported] [int] NULL,
 CONSTRAINT [PK_SAFEPLAY_MACHINE_INFO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
