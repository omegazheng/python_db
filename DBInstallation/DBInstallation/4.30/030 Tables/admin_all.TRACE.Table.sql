SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[TRACE]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[TRACE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LOGTIME] [datetime] NOT NULL,
	[PARTYID] [int] NULL,
	[TASK] [varchar](50) NOT NULL,
	[SUBTASK] [varchar](50) NULL,
	[STATUS] [varchar](50) NULL,
	[METHOD] [varchar](50) NULL,
	[ADDITIONAL_INFO] [varchar](max) NULL,
	[TIME_TAKEN] [int] NULL,
 CONSTRAINT [PK_admin_all_TRACE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
