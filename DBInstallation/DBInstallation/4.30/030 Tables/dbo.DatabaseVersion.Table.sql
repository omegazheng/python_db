SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT *
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[DatabaseVersion]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[DatabaseVersion]
	(
		[Version] varchar(max) NOT NULL,
		[Date] [datetime] NOT NULL
	)
END
GO
IF NOT EXISTS (SELECT *
FROM sys.objects
WHERE object_id = OBJECT_ID(N'[dbo].[DF_dbo_DatabaseVersion_Date]') AND type = 'D')
BEGIN
	ALTER TABLE [dbo].[DatabaseVersion] ADD  CONSTRAINT [DF_dbo_DatabaseVersion_Date]  DEFAULT (getutcdate()) FOR [Date]
END
GO
