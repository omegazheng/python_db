SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[QueryEvaluationType]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[QueryEvaluationType](
	[EvaluationType] [varchar](20) NOT NULL,
	[Comment] [varchar](128) NULL,
 CONSTRAINT [PK_maint_QueryEvaluationType] PRIMARY KEY CLUSTERED 
(
	[EvaluationType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
