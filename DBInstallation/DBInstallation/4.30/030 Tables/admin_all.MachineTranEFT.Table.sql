SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MachineTranEFT]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MachineTranEFT](
	[EFTID] [bigint] NOT NULL,
	[MachineTranID] [bigint] NOT NULL,
	[AccountTranID] [bigint] NULL,
	[RollbackTranID] [bigint] NULL,
	[TransferType] [varchar](50) NOT NULL,
	[Status] [varchar](30) NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[AmountReal] [numeric](38, 18) NOT NULL,
	[ReleasedBonus] [numeric](38, 18) NOT NULL,
	[PlayableBonus] [numeric](38, 18) NOT NULL,
	[Reference] [varchar](max) NULL,
	[ProductTranID] [varchar](100) NULL,
 CONSTRAINT [PK_admin_all_MachineTranEFT] PRIMARY KEY CLUSTERED 
(
	[EFTID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
