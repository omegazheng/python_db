SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[AccountTranHourlyAggregateRecalculate]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[AccountTranHourlyAggregateRecalculate](
	[Datetime] [datetime] NOT NULL,
	[CalculationInProgress] [bit] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
 CONSTRAINT [PK_admin_all_AccountTranHourlyAggregateRecalculate] PRIMARY KEY CLUSTERED 
(
	[Datetime] ASC,
	[CalculationInProgress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = ON, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UK_admin_all_AccountTranHourlyAggregateRecalculate] UNIQUE NONCLUSTERED 
(
	[RowVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
