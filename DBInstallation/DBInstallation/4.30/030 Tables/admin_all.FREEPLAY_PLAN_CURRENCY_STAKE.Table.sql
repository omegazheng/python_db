SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FREEPLAY_PLAN_ID] [int] NOT NULL,
	[CURRENCY] [nchar](3) NOT NULL,
	[BET_PER_ROUND] [numeric](38, 18) NOT NULL,
 CONSTRAINT [PK_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_FREEPLAY_PLAN_ID_CURRENCY] UNIQUE NONCLUSTERED 
(
	[FREEPLAY_PLAN_ID] ASC,
	[CURRENCY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]') AND name = N'IDX_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY')
CREATE NONCLUSTERED INDEX [IDX_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY] ON [admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]
(
	[CURRENCY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
