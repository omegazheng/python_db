SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[AccountTranHourlyAggregate]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[AccountTranHourlyAggregate](
	[AggregateType] [tinyint] NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[BrandID] [int] NOT NULL,
	[PartyID] [int] NOT NULL,
	[Currency] [nchar](3) NULL,
	[TranType] [varchar](10) NOT NULL,
	[ProductID] [int] NOT NULL,
	[GameID] [varchar](100) NOT NULL,
	[AmountReal] [numeric](38, 18) NOT NULL,
	[AmountReleasedBonus] [numeric](38, 18) NOT NULL,
	[AmountPlayableBonus] [numeric](38, 18) NOT NULL,
	[GameCount] [int] NOT NULL,
	[TranCount] [int] NOT NULL,
 CONSTRAINT [PK_admin_all_AccountTranHourlyAggregate] PRIMARY KEY CLUSTERED 
(
	[AggregateType] ASC,
	[Datetime] ASC,
	[TranType] ASC,
	[PartyID] ASC,
	[ProductID] ASC,
	[GameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE) ON [PS_AccountTranHourlyAggregate]([AggregateType])
) ON [PS_AccountTranHourlyAggregate]([AggregateType])
END
GO
