SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[CurrencyConversionRateCurrent]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[CurrencyConversionRateCurrent](
	[RateID] [bigint] NOT NULL,
	[CurrencyFrom] [nchar](3) NOT NULL,
	[CurrencyTo] [nchar](3) NOT NULL,
	[Rate] [numeric](38, 18) NOT NULL,
	[EffectiveDate] [datetime] NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_admin_all_CurrencyConversionRateCurrent] PRIMARY KEY CLUSTERED 
(
	[RateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[admin_all].[CurrencyConversionRateCurrent]') AND name = N'IDX_admin_all_CurrencyConversionRateCurrent_CurrencyFrom_CurrencyTo')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_admin_all_CurrencyConversionRateCurrent_CurrencyFrom_CurrencyTo] ON [admin_all].[CurrencyConversionRateCurrent]
(
	[CurrencyFrom] ASC,
	[CurrencyTo] ASC
)
INCLUDE ( 	[Rate],
	[LastUpdateDate],
	[EffectiveDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
