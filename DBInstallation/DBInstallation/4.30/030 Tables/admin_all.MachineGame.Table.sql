SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MachineGame]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MachineGame](
	[MachineID] [int] NOT NULL,
	[GameInfoID] [int] NOT NULL,
 CONSTRAINT [PK_admin_all_MachineGame] PRIMARY KEY CLUSTERED 
(
	[MachineID] ASC,
	[GameInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_admin_all_MachineGame] UNIQUE NONCLUSTERED 
(
	[GameInfoID] ASC,
	[MachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
