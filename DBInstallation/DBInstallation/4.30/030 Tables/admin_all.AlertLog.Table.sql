SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[AlertLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[AlertLog](
	[AlertLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AlertCode] [varchar](20) NOT NULL,
	[SeverityLevel] [int] NOT NULL,
	[EventCount] [int] NOT NULL,
	[DistributionListID] [int] NULL,
	[StackTrace] [uniqueidentifier] NOT NULL,
	[Message] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_admin_all_AlertLog] PRIMARY KEY CLUSTERED 
(
	[AlertLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertLog_StartDate]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertLog] ADD  CONSTRAINT [DF_admin_all_AlertLog_StartDate]  DEFAULT (getdate()) FOR [StartDate]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertLog_EndDate]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertLog] ADD  CONSTRAINT [DF_admin_all_AlertLog_EndDate]  DEFAULT (getdate()) FOR [EndDate]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertLog_SeverityLevel]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertLog] ADD  CONSTRAINT [DF_admin_all_AlertLog_SeverityLevel]  DEFAULT ((0)) FOR [SeverityLevel]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_AlertLog_EventCount]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[AlertLog] ADD  CONSTRAINT [DF_admin_all_AlertLog_EventCount]  DEFAULT ((1)) FOR [EventCount]
END
GO
