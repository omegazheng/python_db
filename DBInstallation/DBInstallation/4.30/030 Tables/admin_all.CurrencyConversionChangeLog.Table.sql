SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[CurrencyConversionChangeLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[CurrencyConversionChangeLog](
	[LogID] [bigint] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime2](4) NULL,
	[RateID] [bigint] NULL,
	[InfoID] [uniqueidentifier] NOT NULL,
	[Operation] [char](1) NOT NULL,
	[OriginalValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_admin_all_CurrencyConversionChangeLog] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
) TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[DF_admin_all_CurrencyConversionChangeLog_Datetime]') AND type = 'D')
BEGIN
ALTER TABLE [admin_all].[CurrencyConversionChangeLog] ADD  CONSTRAINT [DF_admin_all_CurrencyConversionChangeLog_Datetime]  DEFAULT (sysdatetime()) FOR [DateTime]
END
GO
