SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[ReplicationConfiguration]') AND type in (N'U'))
BEGIN
CREATE TABLE [maint].[ReplicationConfiguration](
	[ConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[Comment] [varchar](128) NULL,
	[SourceConnectionID] [int] NOT NULL,
	[SourceTableOrQuery] [varchar](max) NOT NULL,
	[Columns] [varchar](max) NULL,
	[FilterType] [varchar](100) NOT NULL,
	[TargetConnectionID] [int] NOT NULL,
	[TargetSchemaName] [varchar](128) NOT NULL,
	[TargetObjectName] [varchar](128) NOT NULL,
	[BatchSize] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_maint_ReplicationConfiguration] PRIMARY KEY CLUSTERED 
(
	[ConfigurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UQ_maint_ReplicationConfiguration_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[maint].[ReplicationConfiguration]') AND name = N'UQ_maint_ReplicationConfiguration')
CREATE UNIQUE NONCLUSTERED INDEX [UQ_maint_ReplicationConfiguration] ON [maint].[ReplicationConfiguration]
(
	[ConfigurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_ReplicationConfiguration_FilterType]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[ReplicationConfiguration] ADD  CONSTRAINT [DF_maint_ReplicationConfiguration_FilterType]  DEFAULT ('NONE') FOR [FilterType]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_ReplicationConfiguration_BatchSize]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[ReplicationConfiguration] ADD  CONSTRAINT [DF_maint_ReplicationConfiguration_BatchSize]  DEFAULT ((1500)) FOR [BatchSize]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[maint].[DF_maint_ReplicationConfiguration_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [maint].[ReplicationConfiguration] ADD  CONSTRAINT [DF_maint_ReplicationConfiguration_IsActive]  DEFAULT ((0)) FOR [IsActive]
END
GO
