SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatabaseUpdateLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatabaseUpdateLog](
	[BatchID] [bigint] NOT NULL,
	[BatchLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[Version] [money] NOT NULL,
	[Path] [varchar](max) NOT NULL,
	[FileName] [varchar](max) NOT NULL,
	[Messge] [varchar](max) NULL,
	[IsErrorMessage] [bit] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_dbo_DatabaseUpdateLog] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC,
	[BatchLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_dbo_DatabaseUpdateLog_IsErrorMessage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DatabaseUpdateLog] ADD  CONSTRAINT [DF_dbo_DatabaseUpdateLog_IsErrorMessage]  DEFAULT ((0)) FOR [IsErrorMessage]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DF_dbo_DatabaseUpdateLog_Date]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DatabaseUpdateLog] ADD  CONSTRAINT [DF_dbo_DatabaseUpdateLog_Date]  DEFAULT (getutcdate()) FOR [StartDate]
END
GO
