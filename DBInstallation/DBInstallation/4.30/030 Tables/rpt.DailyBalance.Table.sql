SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[rpt].[DailyBalance]') AND type in (N'U'))
BEGIN
CREATE TABLE [rpt].[DailyBalance](
	[AccountTranID] [bigint] NOT NULL,
	[AccountID] [int] NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[BalanceReal] [numeric](38, 18) NOT NULL,
	[ReleasedBonus] [numeric](38, 18) NOT NULL,
	[PlayableBonus] [numeric](38, 18) NOT NULL,
 CONSTRAINT [PK_rpt_DailyBalance] PRIMARY KEY CLUSTERED 
(
	[Datetime] ASC,
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[rpt].[DailyBalance]') AND name = N'IDX_rpt_DailyBalance_AccountID_Datetime')
CREATE NONCLUSTERED INDEX [IDX_rpt_DailyBalance_AccountID_Datetime] ON [rpt].[DailyBalance]
(
	[AccountID] ASC,
	[Datetime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[rpt].[DailyBalance]') AND name = N'IDX_rpt_DailyBalance_AccountTranID')
CREATE UNIQUE NONCLUSTERED INDEX [IDX_rpt_DailyBalance_AccountTranID] ON [rpt].[DailyBalance]
(
	[AccountTranID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
GO
