SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[MachineEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[MachineEvent](
	[EventID] [bigint] IDENTITY(1,1) NOT NULL,
	[Datetime] [datetime] NOT NULL,
	[MachineID] [int] NOT NULL,
	[EventKey] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[SessionKey] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_admin_all_MachineEvent] PRIMARY KEY CLUSTERED 
(
	[MachineID] ASC,
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DATA_COMPRESSION = PAGE)
) TEXTIMAGE_ON [PRIMARY]
END
GO
