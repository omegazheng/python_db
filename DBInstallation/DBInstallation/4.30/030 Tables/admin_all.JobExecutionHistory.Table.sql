SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[JobExecutionHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[JobExecutionHistory](
	[JobExecutionHistoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[JobName] [nvarchar](400) NOT NULL,
	[Status] [nvarchar](20) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Message] [nvarchar](max) NULL,
 CONSTRAINT [PK_admin_all_JobExecutionHistory] PRIMARY KEY CLUSTERED 
(
	[JobName] ASC,
	[JobExecutionHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
 CONSTRAINT [UK_admin_all_JobExecutionHistory] UNIQUE NONCLUSTERED 
(
	[JobExecutionHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) TEXTIMAGE_ON [PRIMARY]
END
GO
