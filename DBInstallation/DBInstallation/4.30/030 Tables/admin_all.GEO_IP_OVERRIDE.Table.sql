SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[GEO_IP_OVERRIDE]') AND type in (N'U'))
BEGIN
CREATE TABLE [admin_all].[GEO_IP_OVERRIDE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[begin_ip] [varchar](20) NOT NULL,
	[end_ip] [varchar](20) NOT NULL,
	[begin_ip_num] [bigint] NOT NULL,
	[end_ip_num] [bigint] NOT NULL,
	[note] [varchar](255) NULL,
	[country] [char](2) NULL,
 CONSTRAINT [PK_admin_all_GEO_IP_OVERRIDE] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
END
GO
