set ansi_nulls, quoted_identifier on
go


create or alter procedure admin_all.usp_GetFreePlayPromotionBySession
  (
    @SESSION NVARCHAR(255)
  )
as
  begin

    declare @ID int

    if @SESSION is null
      begin
        raiserror('SESSION should not be null ', 16,1)
        return
      end

    select  promotion.id as id,
            promotion.freeplay_plan_id as freePlayPlanId,
            promotion.code as code,
            promotion.status as status,
            promotion.trigger_date as triggerDate,
            promotion.last_updated_time as lastUpdatedTime,
            promotion.is_completed as isCompleted,
            promotion.party_id as partyId,
            isnull(promotion.total_balance,0) as totalBalance,
            isnull(promotion.remaining_balance,0) as remainingBalance,
            isnull(promotion.amount_won,0) as amountWon,
            promotion.default_stake_level as defaultStakeLevel,
            promotion.GAME_INFO_ID as gameInfoId,
            promotion.line as line,
            isnull(promotion.coin,0) as coin,
            isnull(promotion.denomination,0) as denomination,
            isnull(promotion.bet_per_round,0) as betPerRound
    from
      admin_all.FREEPLAY_PROMOTION promotion
      join admin_all.FREEPLAY_PROMOTION_SESSION session on session.FREEPLAY_PROMOTION_ID = promotion.ID
    where
      session.SESSION = @SESSION

  end

go
