set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddMachine
(
	@MachineID int, @PreviousMachineID int,
	@SiteCode nvarchar(20), @SiteName nvarchar(50), @SiteTown nvarchar(50), @SitePostCode nvarchar(50),
	@MachineHostNo nvarchar(50), @MachineSerialNo nvarchar(50), 
	@GameName nvarchar(50),
	@MachineGameCategory nvarchar(20),
	@Manufacturer nvarchar(50),
	@OperatorCode nvarchar(50),
	@OperatorName nvarchar(100), 
	@CompanyCode nvarchar(50),
	@CompanyName nvarchar(100),
	@SubCompanyCode nvarchar(50),
	@SubCompanyName nvarchar(100),
	@PromoSupported int,
	@Active bit,
	@DateTime datetime
)
as
begin
	set nocount on 
	if exists(select * from admin_all.Machine where MachineID = @MachineID)
		return
	declare  @CompanyID int, @LocationID int, @CategoryID int, @ProductID int, @ManufacturerID int
	exec admin_all.usp_AddCompany @CompanyID output, @CompanyCode, @CompanyName, @SubCompanyCode, @SubCompanyName, null
	exec admin_all.usp_AddLocation @LocationID output, @SiteCode, @SiteName, @SiteTown, @SitePostCode, @CompanyID
	exec admin_all.usp_AddMachineCategory @CategoryID output, @MachineGameCategory
	exec admin_all.usp_AddMachineProduct @ProductID output,  @OperatorCode, @OperatorName
	exec admin_all.usp_AddMachineManufacturer @ManufacturerID output, @Manufacturer, null;
	
	insert into admin_all.Machine (MachineID, PreviousMachineID, LocationID, CategoryID, ProductID, ManufacturerID, Datetime, HostNumber, SerialNumber, PromoCreditSupported, IsActive)
			values(@MachineID, @PreviousMachineID, @LocationID, @CategoryID, @ProductID, @ManufacturerID, @Datetime, @MachineHostNo, @MachineSerialNo, @PromoSupported, @Active);
	exec admin_all.usp_AddMachineGame @MachineID, @ProductID, @GameName
	
end
go
