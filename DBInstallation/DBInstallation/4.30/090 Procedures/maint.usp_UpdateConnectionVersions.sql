set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_UpdateConnectionVersions
as
begin
	set nocount on
	declare @Error nvarchar(max) = '', @ConnectionID int
	declare c cursor local static for
		select ConnectionID 
		from maint.Connection 
		where AutoUpdateVersion = 1
	open c
	fetch next from c into @ConnectionID
	while @@fetch_status = 0
	begin
		begin try
			update maint.Connection 
				set CurrentRowVersion =  cast(maint.fn_ExecuteSQLScalarObject(maint.fn_GetSQLConnectionStringByID(@ConnectionID), 'select case when min_active_rowversion()< @@dbts  then min_active_rowversion() else @@dbts end') as binary(8))
			where ConnectionID = @ConnectionID

		end try
		begin catch
			select @Error = @Error + '
			------'+cast(@ConnectionID as varchar(20))+'-----------------------------------------------------------
			' + error_message()
		end catch

		begin try
			update maint.Connection 
				set CurrentChangeTrackingVersion =  cast(maint.fn_ExecuteSQLScalarObject(maint.fn_GetSQLConnectionStringByID(@ConnectionID), 'select isnull(change_tracking_current_version ()  , 0)') as bigint)
			where ConnectionID = @ConnectionID

		end try
		begin catch
			select @Error = @Error + '
			------'+cast(@ConnectionID as varchar(20))+'-----------------------------------------------------------
			' + error_message()
		end catch
		fetch next from c into @ConnectionID
	end
	close c
	deallocate c	
	if len(@Error) > 10
		raiserror(@Error, 16, 1)
end

go
