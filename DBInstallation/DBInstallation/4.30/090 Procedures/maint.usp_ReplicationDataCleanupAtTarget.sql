set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicationDataCleanupAtTarget(@Confirm int)
as
begin
	set nocount on
	set xact_abort on
	if isnull(@Confirm , 0)<> 65
	begin
		raiserror('This procedure removes data, please run it with parameter 65 to confirm', 16, 1)
		return
	end
	declare @BrandID int, @EnablePrevious bit, @EnableCurrent bit, @RetentionPeriodPrevious int, @RetentionPeriodCurrent int
	---brand user account clean up
	
	delete b
	from maint.ReplicationBrandFilterConfiguration c
		inner join admin.CASINO_BRAND_DEF b on b.BRANDID = c.BrandID
	where c.EnableCurrent = 0 

	delete u
	from external_mpt.USER_CONF u 
	where not exists(select * from admin.CASINO_BRAND_DEF b where b.BRANDID = u.BRANDID)

	delete a
	from admin_all.ACCOUNT a 
	where not exists(select * from external_mpt.USER_CONF u where u.PARTYID = a.PARTYID)
	
	
	exec maint.usp_ReplicationRemoveTransactionAtTarget null, 0

	if (select count(distinct RetentionPeriodCurrent) from maint.ReplicationBrandFilterConfiguration) = 1
	begin
		--all data is replicated over, we only need to remove data from account tran and payment
		select top 1 @RetentionPeriodCurrent = RetentionPeriodCurrent
		from ReplicationBrandFilterConfiguration
		if @@rowcount = 0 or @RetentionPeriodCurrent <=0
		begin
			-- invalid settings
			return
		end
		exec maint.usp_ReplicationRemoveTransactionAtTarget @RetentionPeriodCurrent, 0
		return
	end
	--- cleanup brand based transactions
	declare c1 cursor local static for
		select distinct RetentionPeriodCurrent from maint.ReplicationBrandFilterConfiguration
	open c1
	fetch next from c1 into @RetentionPeriodCurrent
	while @@fetch_status = 0
	begin
		exec maint.usp_ReplicationRemoveTransactionAtTarget @RetentionPeriodCurrent, 1
		fetch next from c1 into @RetentionPeriodCurrent
	end
	close c1
	deallocate c1

end

go
