set ansi_nulls, quoted_identifier on
go

create or alter procedure SafePlay.usp_InsertSafePlayState
  (
    @AccountTranID BIGINT,
    @ExtTranID NVARCHAR(100),
    @State NVARCHAR(20),
    @Reference NVARCHAR(300)
  )
as
  begin
    DECLARE @ID bigint

    insert into SafePlay.SafePlayEFTState
    (
      AccountTranID,
      ExtTranID,
      State,
      RequestTime,
      LastAccessTime,
      REFERENCE
    )
    values
      (
        @AccountTranID,
        @ExtTranID,
        @State,
        GETDATE(),
        GETDATE(),
        @Reference
      )

    select @@IDENTITY

  end

go
