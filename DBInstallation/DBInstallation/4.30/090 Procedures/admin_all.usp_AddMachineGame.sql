set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddMachineGame
(
	@MachineID int,
	@ProductID int=null, 
	@GameName nvarchar(50)= null,
	@GameInfoID int = null output
)
as
begin
	set nocount on 
	if @GameInfoID is null
	begin
		select @GameInfoID = ID 
		from admin_all.GAME_INFO 
		where GAME_ID = @GameName 
			and PLATFORM_ID = @ProductID
	end
	if @GameInfoID is null
	begin
		insert into admin_all.GAME_INFO(PLATFORM_ID, GAME_ID, NAME, GAME_CATEGORY_ID, GAME_LAUNCH_ID)
			select @ProductID, @GameName, @GameName, ID,@GameName
			from admin_all.GAME_CATEGORY
			where NAME = 'EFT'
		select @GameInfoID = scope_identity()
	end
	insert into admin_all.MachineGame(MachineID, GameInfoID)
		values(@MachineID, @GameInfoID)
end
go
