set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findAllCommissionPlans]
AS
  BEGIN
    select
    	uc.partyid as agentId,
    	u.hierarchy.ToString() as hierarchy,
    	c.id as planId,
    	c.scheme as scheme,
    	c.category as category,
    	cs.period as period
    from admin_all.user_commission as uc
    	join admin_all.commission as c on c.id=uc.commission_id
    		join admin_all.commission_structure as cs on cs.commission_id=c.id
    	join external_mpt.user_conf as u on u.partyid=uc.partyid
    where c.state = 'ACTIVE'
      and exists (select * from external_mpt.user_conf as child where child.parentid=uc.partyid)
    order by u.hierarchy desc, c.category asc, cs.period asc
  END

go
