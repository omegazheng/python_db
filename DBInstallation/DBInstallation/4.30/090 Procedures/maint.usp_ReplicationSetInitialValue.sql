set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicationSetInitialValue
as
begin
	set nocount, xact_abort on 
	if not exists(select * from maint.QueryEvaluationType where EvaluationType = 'Local') insert into maint.QueryEvaluationType(EvaluationType, Comment) values('Local', 'Query will be evaluated locally')
	if not exists(select * from maint.QueryEvaluationType where EvaluationType = 'None') insert into maint.QueryEvaluationType(EvaluationType, Comment) values('None', 'Query will not be evaluated')
	if not exists(select * from maint.QueryEvaluationType where EvaluationType = 'Source') insert into maint.QueryEvaluationType(EvaluationType, Comment) values('Source', 'The result of query evaluated locally will be evaluated at the source connection')
	if not exists(select * from maint.QueryEvaluationType where EvaluationType = 'Target') insert into maint.QueryEvaluationType(EvaluationType, Comment) values('Target', 'The result of query evaluated locally will be evaluated at the target connection')
	
	if not exists(select * from maint.FilterType where FilterType = 'ChangeTracking') insert into maint.FilterType(FilterType, FromValueQuery,FromValueQueryEvaluationType, ToValueQuery, ToValueQueryEvaluationType) values('ChangeTracking','cast(@LastLoadedValue as bigint)','Local','cast(@CurrentChangeTrackingVersion as bigint)','Local')
	if not exists(select * from maint.FilterType where FilterType = 'RowVersion') insert into maint.FilterType(FilterType, FromValueQuery,FromValueQueryEvaluationType, FromValueAdjustment ,ToValueQuery, ToValueQueryEvaluationType, ToValueAdjustment) values('RowVersion','convert(varchar(30),isnull(cast(@ToValue as binary(8)), 0x00), 1)','Local','convert(binary(8), @NewFromValue, 1)','convert(varchar(30),isnull(cast(@CurrentRowVersion as binary(8)), 0x00),1)','Local', 'convert(binary(8), @NewToValue, 1)')
	if not exists(select * from maint.FilterType where FilterType = 'Snapshot') insert into maint.FilterType(FilterType, FromValueQuery,FromValueQueryEvaluationType, ToValueQuery, ToValueQueryEvaluationType) values('Snapshot',null,'None',null,'None')
	

	if not exists(select * from maint.FilterType where FilterType = 'DateTime-Back20min-Forward24hr') 
	begin
		insert into maint.FilterType(
									FilterType, 
									FromValueQuery, FromValueQueryEvaluationType, FromValueAdjustment,
									ToValueQuery, ToValueQueryEvaluationType, ToValueAdjustment
								) 
			values(
						'DateTime-Back20min-Forward24hr',
						'convert(varchar(16), dateadd(minute, -20, case when isnull(cast(@ToValue as datetime),   getdate()) > isnull(cast(@ToValue as datetime),   getdate()) then getdate() else isnull(cast(@ToValue as datetime),   getdate()) end  ), 120)', 'Local', 'convert(datetime, @NewFromValue, 120)',
						'convert(varchar(16), dateadd(hour, 24, getdate()), 120)', 'Local', 'convert(datetime, dateadd(hour, -24,@NewToValue), 120)'
				)
	end

	--if not exists(select * from maint.FilterType where FilterType = 'ID-Bigint') 
	--begin
	--	insert into maint.FilterType(
	--								FilterType, 
	--								FromValueQuery, FromValueQueryEvaluationType, FromValueAdjustment,
	--								ToValueQuery, ToValueQueryEvaluationType, ToValueAdjustment
	--							) 
	--		values(
	--					'ID-Bigint',
	--					'convert(varchar(16), dateadd(minute, -20, case when isnull(cast(@ToValue as datetime),   getdate()) > isnull(cast(@ToValue as datetime),   getdate()) then getdate() else isnull(cast(@ToValue as datetime),   getdate()) end  ), 120)', 'Local', 'convert(datetime, @NewFromValue, 120)',
	--					'convert(varchar(16), dateadd(hour, 24, getdate()), 120)', 'Local', 'convert(datetime, dateadd(hour, -24,@NewToValue), 120)'
	--			)
	--end
end

go
