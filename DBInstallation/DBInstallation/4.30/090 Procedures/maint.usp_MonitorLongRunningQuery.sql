set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_MonitorLongRunningQuery
as
begin
	set nocount on 
	declare @ts_now bigint = (SELECT cpu_ticks/(cpu_ticks/ms_ticks)FROM sys.dm_os_sys_info); 
	declare @Body varchar(max)
	if not exists(
				select  *
				from sys.dm_exec_requests r
				where exists(
								select * 
								from sys.dm_exec_connections c 
								where r.session_id = c.session_id
									
							)
					and exists(
								select *
								from sys.dm_exec_sessions s
								where r.session_id = s.session_id
								and login_name in('admin_all')
							)
					and start_time < dateadd(second, -120 , getdate())
				)
		return
	exec maint.usp_TakeSnapshotActiveSessions @Body output
	exec maint.usp_SendAlert @Subject = 'There are queries running over 2 minutes.', @Body = @Body, @TestMode = 0
end

go
