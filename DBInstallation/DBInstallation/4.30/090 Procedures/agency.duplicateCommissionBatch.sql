set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[duplicateCommissionBatch]
    (@sourceCommissionIds nvarchar(max), @AgentIds nvarchar(max))
AS
    BEGIN

        declare @Agent_id int, @commission_id int
        -- select @AgentIds = '1,2,3,4,5'
        -- select @sourceCommissionIds = '7,8,9'

        declare c cursor local static for
            select
                commission_id, agent_id
            from (select n.c.value('.', 'int') agent_id from (select cast('<r>'+replace(@AgentIds, ',','</r><r>') +'</r>' as xml) as c) x cross apply x.c.nodes('/r') n(c)) x0
                cross join (select n.c.value('.', 'int') commission_id from (select cast('<r>'+replace(@sourceCommissionIds, ',','</r><r>') +'</r>' as xml) as c) x cross apply x.c.nodes('/r') n(c))x1
        open c
        fetch next from c into @commission_id, @agent_id
        while @@fetch_status = 0
            begin
                exec [agency].[duplicateCommission] @commission_id, @agent_id
                fetch next from c into @commission_id, @agent_id
            end
        close c
        deallocate c
    END


go
