set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetMachineList
(
	@StaffID int,
	@CompanyID int,
	@MachineID int = null
)
as
begin
	set nocount on
	if isnull(@MachineID, @CompanyID) is null
	begin
		raiserror('@CompanyID or/and @MachineID should be provided.', 16, 1)
		return
	end
	declare @StaffType varchar(10) = admin_all.fn_StaffType(@StaffID)
	declare @SQL nvarchar(max)
	select @SQL = '
		select	m.*,
				l.Name as LocationName,
				mc.Code as CategoryCode, mc.Limit, 
				p.NAME as ProductName,
				mm.Name as ManufacturerName
		from admin_all.Machine m 
			left join admin_all.Location l on l.LocationID = m.LocationID
			left join admin_all.MachineCategory mc on mc.CategoryID = m.CategoryID
			left join admin_all.PLATFORM p on p.id = m.ProductID
			left join admin_all.MachineManufacturer mm on mm.ManufacturerID = m.ManufacturerID
		where '+case 
					when @CompanyID is not null then 'l.CompanyID = @CompanyID'
					when @MachineID is not null then 'm.MachineID = @MachineID'
					when @CompanyID is not null  and @MachineID is not null  then 'l.CompanyID = @CompanyID and m.MachineID = @MachineID'
				end +'
			'+case when @StaffType = 'Company' then 'and exists(
						select * 
						from admin_all.StaffCompany sc
						where CompanyID = @CompanyID
							and sc.StaffID = @StaffID
					)' 
					when @StaffType = 'Product' then '
			and exists(
					select *
					from admin_all.StaffProduct sp
					where sp.StaffID = @StaffID
						and sp.ProductID = m.ProductID
						
					)'
					else ''
			end
	exec sp_executesql @SQL, N'@StaffID int, @CompanyID int, @MachineID int', @StaffID, @CompanyID, @MachineID
	
end

go
