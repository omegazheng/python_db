set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetGrantedAndReleasedBonusInfo]
  (@partyid INT)
AS
  BEGIN

    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = CAST(DATEADD(YEAR, 100, 0) AS DATE)

    SELECT *
    INTO #Temp
    FROM (
           SELECT
             'GRANTED'                AS TYPE,
             CAST(at.datetime AS DATE)   DATE,
             at.AMOUNT_PLAYABLE_BONUS AS AMOUNT
           FROM admin_all.ACCOUNT_TRAN at
             JOIN admin_all.ACCOUNT a ON at.ACCOUNT_ID = a.ID
           WHERE
             a.PARTYID = @partyid
             AND TRAN_TYPE = 'CRE_BONUS'

           UNION ALL

           SELECT
             'RELEASED'               AS TYPE,
             CAST(at.datetime AS DATE)   DATE,
             at.AMOUNT_RELEASED_BONUS AS AMOUNT
           FROM admin_all.ACCOUNT_TRAN at
             JOIN admin_all.ACCOUNT a ON at.ACCOUNT_ID = a.ID
           WHERE
             a.PARTYID = @partyid
             AND TRAN_TYPE = 'BONUS_REL'
         ) BONUSES


    SELECT
      'TODAY' AS PERIOD,
      TYPE    AS TYPE,
      AMOUNT  AS AMOUNT
    FROM #Temp
    WHERE DATE = CAST(@endDateLocal AS DATE)

    UNION

    SELECT
      'YESTERDAY' AS PERIOD,
      TYPE        AS TYPE,
      AMOUNT      AS AMOUNT
    FROM #Temp
    WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

    UNION

    SELECT
      'WTD'       AS PERIOD,
      TYPE        AS TYPE,
      SUM(AMOUNT) AS AMOUNT
    FROM #Temp
    WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
          AND DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY TYPE

    UNION

    SELECT
      'MTD'       AS PERIOD,
      TYPE        AS TYPE,
      SUM(AMOUNT) AS AMOUNT
    FROM #Temp
    WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
          AND DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY TYPE

    UNION

    SELECT
      'LTD'       AS PERIOD,
      TYPE        AS TYPE,
      SUM(AMOUNT) AS AMOUNT
    FROM #Temp
    WHERE DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY TYPE

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END


  END

go
