set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findUnpaidPaymentsOfAgent]
    (@sp_name varchar(20), @agent_id int)
AS
BEGIN
    select id, agent_id, plan_id, start_date, end_date, state, type, comments, sum(value) as value
    from agency.commission_payment
    where (state not like 'COMPLETED%')
        and agent_id = @agent_id
    group by id, agent_id, plan_id, start_date, end_date, state, type, comments
END

go
