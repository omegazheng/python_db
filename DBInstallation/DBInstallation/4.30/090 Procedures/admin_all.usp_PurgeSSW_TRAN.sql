set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_PurgeSSW_TRAN] @Retention int = 1000000, @BatchSize int = 150, @ArchiveDatabase nvarchar(128)= null
as
begin
	set nocount, xact_abort on
	declare @i bigint, @ID bigint, @db nvarchar(128), @SQL nvarchar(max), @Proc sysname, @FullTargetTableName2 sysname
	
	if @ArchiveDatabase is not null
		begin
			select @db = db_name(db_id(@ArchiveDatabase))
			if @db is null
				begin
					raiserror('Archive database is not valid %s', 16,1, @ArchiveDatabase)
					return
				end
			if @db is not null
				begin
					select @Proc = quotename(@db) + '..sp_executesql'
					select @SQL = 'select @ret = case when exists(select * from sys.schemas where name = ''admin_all'') then 1 else 0 end'
					exec @Proc @SQL, N'@ret int output', @i output
					if @i = 0
						begin
							select @SQL = 'create schema admin_all'
							begin try
							exec @proc @SQL
							end try
							begin catch
							end catch
						end
	
					select @FullTargetTableName2 = quotename(@db)+'.admin_all.SSW_TRAN'
					if object_id(@FullTargetTableName2) is null
						begin
							select @SQL = 'create table ' + @FullTargetTableName2 +'(
	[ID] [bigint]not null,[SEQ_NUMBER] [varchar](100),[PARTY_ID] [int],[BRAND_ID] [int],[OMEGA_SESSION_KEY] [varchar](32),[REQUEST_TIME] [datetime],[RESPONSE_TIME] [datetime],
	[TRAN_TYPE] [varchar](10),[AMOUNT_REAL] numeric(38,18),[BALANCE_REAL] numeric(38,18),
	[AMOUNT_PLAYABLE] numeric(38,18),[BALANCE_PLAYABLE] numeric(38,18),[PLATFORM_CODE] [varchar](20),
	[GAME_INFO_ID] [int],[PROVIDER_TRAN_ID] [varchar](100),[PRIMARY_WALLET_TRAN_ID] [varchar](100),
	[ACCOUNT_TRAN_ROLLBACK_TRAN_ID] [bigint],[GAME_ID] [varchar](100),[GAME_TRAN_ID] [varchar](100),
	[IS_FINAL] [bit],[ERROR_CODE] [varchar](20),[ERROR_MESSAGE] [varchar](4000), constraint [PK_admin_all_SSW_TRAN] primary key(ID))'
							exec(@SQL)
						end
					select @SQL = '
		insert into ' + @FullTargetTableName2 + '(ID, SEQ_NUMBER, PARTY_ID, BRAND_ID, OMEGA_SESSION_KEY, REQUEST_TIME, RESPONSE_TIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, AMOUNT_PLAYABLE, BALANCE_PLAYABLE, PLATFORM_CODE, GAME_INFO_ID, PROVIDER_TRAN_ID, PRIMARY_WALLET_TRAN_ID, ACCOUNT_TRAN_ROLLBACK_TRAN_ID, GAME_ID, GAME_TRAN_ID, IS_FINAL, ERROR_CODE, ERROR_MESSAGE)
	select ID, SEQ_NUMBER, PARTY_ID, BRAND_ID, OMEGA_SESSION_KEY, REQUEST_TIME, RESPONSE_TIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, AMOUNT_PLAYABLE, BALANCE_PLAYABLE, PLATFORM_CODE, GAME_INFO_ID, PROVIDER_TRAN_ID, PRIMARY_WALLET_TRAN_ID, ACCOUNT_TRAN_ROLLBACK_TRAN_ID, GAME_ID, GAME_TRAN_ID, IS_FINAL, ERROR_CODE, ERROR_MESSAGE
	from (
			delete t
				output deleted.ID, deleted.SEQ_NUMBER, deleted.PARTY_ID, deleted.BRAND_ID, deleted.OMEGA_SESSION_KEY, deleted.REQUEST_TIME, deleted.RESPONSE_TIME, deleted.TRAN_TYPE, deleted.AMOUNT_REAL, deleted.BALANCE_REAL, deleted.AMOUNT_PLAYABLE, deleted.BALANCE_PLAYABLE, deleted.PLATFORM_CODE, deleted.GAME_INFO_ID, deleted.PROVIDER_TRAN_ID, deleted.PRIMARY_WALLET_TRAN_ID, deleted.ACCOUNT_TRAN_ROLLBACK_TRAN_ID, deleted.GAME_ID, deleted.GAME_TRAN_ID, deleted.IS_FINAL, deleted.ERROR_CODE, deleted.ERROR_MESSAGE
			from admin_all.SSW_TRAN t
			where ID = @ID
		) t1
		'
				end
		end
	select @i = 0
	
	declare c cursor local static for
		select ID from admin_all.SSW_TRAN where ID < (select max(ID) from admin_all.SSW_TRAN) - @Retention order by 1
	open c
	fetch next from c into @ID
	while @@fetch_status = 0
		begin
			select @i = @i +1
			if @@trancount = 0
				begin
					begin transaction
				end
			if @db is null
				begin
					delete admin_all.SSW_TRAN where ID = @ID
				end
			else
				begin
					exec sp_executesql @SQL, N'@ID bigint', @ID = @ID
				end
			if @i % @BatchSize = 0
				begin
					while @@trancount > 0
						commit
				end
			fetch next from c into @ID
		end
	close c
	deallocate c
	while @@trancount > 0
		commit
end

go
