set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetNetworkPerformanceByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)
    declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);

    if @user_type <> 20

        select
            subagent.partyid as partyid,
            subagent.userid as userid,
            subagent.parentid as parentid,
            subagent.hierarchy.ToString() as hierarchy,
            subagent.user_type as userType,
            subagent.first_name + ' ' + subagent.last_name as name,
            [agency].[fn_GetGgrByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as ggr,
            [agency].[fn_GetBonusesByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as bonuses,
            [agency].[fn_GetNgrByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as ngr,
            [agency].[fn_GetSignupsByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as signups,
            [agency].[fn_GetActivesByAgentAndDateRange](subagent.partyid, @start_date, @end_date)  as actives,
            [agency].[fn_GetPaymentSumByTypeAndAgentAndDateRange]('DEPOSIT', subagent.partyid, @start_date, @end_date)  as deposits,
            [agency].[fn_GetPaymentSumByTypeAndAgentAndDateRange]('WITHDRAWAL', subagent.partyid, @start_date, @end_date)  as withdrawals

        from external_mpt.user_conf as subagent
        where
            subagent.hierarchy.IsDescendantOf(@agentHierarchy)=1
            and subagent.partyid <> @agent_id
            and subagent.user_type <> 0
        order by subagent.hierarchy asc

    else

        select
            subagent.partyid as partyid,
            subagent.userid as userid,
            subagent.parentid as parentid,
            subagent.hierarchy.ToString() as hierarchy,
            subagent.user_type as userType,
            subagent.first_name + ' ' + subagent.last_name as name,
            [agency].[fn_GetGgrByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as ggr,
            [agency].[fn_GetBonusesByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as bonuses,
            [agency].[fn_GetNgrByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as ngr,
            [agency].[fn_GetSignupsByAgentAndDateRange](subagent.partyid, @start_date, @end_date) as signups,
            [agency].[fn_GetActivesByAgentAndDateRange](subagent.partyid, @start_date, @end_date)  as actives,
            [agency].[fn_GetPaymentSumByTypeAndAgentAndDateRange]('DEPOSIT', subagent.partyid, @start_date, @end_date)  as deposits,
            [agency].[fn_GetPaymentSumByTypeAndAgentAndDateRange]('WITHDRAWAL', subagent.partyid, @start_date, @end_date)  as withdrawals

        from external_mpt.user_conf as subagent
        where
            subagent.hierarchy.IsDescendantOf(@agentHierarchy)=1
            and subagent.partyid <> @agent_id
        order by subagent.hierarchy asc



  END

go
