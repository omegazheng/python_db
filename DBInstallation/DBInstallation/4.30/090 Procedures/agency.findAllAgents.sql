set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findAllAgents]
AS
  BEGIN
    select *
    from external_mpt.user_conf
    where
        (user_type is null or user_type=0) and parentid is not null
        or
        user_type is not null and user_type<>0
  END

go
