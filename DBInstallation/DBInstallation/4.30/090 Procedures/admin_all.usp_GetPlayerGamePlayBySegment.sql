set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerGamePlayBySegment]
  (@partyid INT)
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @endDateLocal DATETIME = GETDATE()
    DECLARE @startDateLocal DATETIME = CAST(DATEADD(YEAR, 100, 0) AS DATE)
    DECLARE @amountLocal INT = 5

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
      'TODAY'         AS PERIOD,
        gi.segment_id,
        segment.name segment_name,
      SUM(dw.GAME_COUNT) AS game_count
    FROM admin_all.DW_GAME_PLAYER_DAILY as dw
      left join admin_all.game_info gi on dw.game_id = gi.game_id and dw.platform_id = gi.platform_id
            join admin_all.segment on segment.id = gi.segment_id
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE = CAST(@endDateLocal AS DATE)
    GROUP BY
      gi.segment_id, segment.name
    ORDER BY SUM(GAME_COUNT) DESC) today

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
        'YESTERDAY'         AS PERIOD,
        gi.segment_id,
        segment.name segment_name,
        SUM(dw.GAME_COUNT) AS game_count
        FROM admin_all.DW_GAME_PLAYER_DAILY as dw
          left join admin_all.game_info gi on dw.game_id = gi.game_id and dw.platform_id = gi.platform_id
                join admin_all.segment on segment.id = gi.segment_id
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))
    GROUP BY
      gi.segment_id, segment.name
    ORDER BY SUM(GAME_COUNT) DESC) yesterday

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
        'WTD'         AS PERIOD,
        gi.segment_id,
        segment.name segment_name,
        SUM(dw.GAME_COUNT) AS game_count
        FROM admin_all.DW_GAME_PLAYER_DAILY as dw
          left join admin_all.game_info gi on dw.game_id = gi.game_id and dw.platform_id = gi.platform_id
                join admin_all.segment on segment.id = gi.segment_id
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
          AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
      gi.segment_id, segment.name
    ORDER BY SUM(GAME_COUNT) DESC) wtd

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
        'MTD'         AS PERIOD,
        gi.segment_id,
        segment.name segment_name,
        SUM(dw.GAME_COUNT) AS game_count
        FROM admin_all.DW_GAME_PLAYER_DAILY as dw
          left join admin_all.game_info gi on dw.game_id = gi.game_id and dw.platform_id = gi.platform_id
                join admin_all.segment on segment.id = gi.segment_id
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
          AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
      gi.segment_id, segment.name
    ORDER BY SUM(GAME_COUNT) DESC) mtd

    UNION

    SELECT *
    FROM(
      SELECT TOP (@amountLocal)
      'LTD'         AS PERIOD,
        gi.segment_id,
        segment.name segment_name,
        SUM(dw.GAME_COUNT) AS game_count
        FROM admin_all.DW_GAME_PLAYER_DAILY as dw
          left join admin_all.game_info gi on dw.game_id = gi.game_id and dw.platform_id = gi.platform_id
                join admin_all.segment on segment.id = gi.segment_id
    WHERE PARTY_ID = @partyid
          AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
    GROUP BY
      gi.segment_id, segment.name
    ORDER BY SUM(GAME_COUNT) DESC) ltd

  END

go
