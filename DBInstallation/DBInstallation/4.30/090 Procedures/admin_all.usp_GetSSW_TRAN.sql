set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetSSW_TRAN]
  (
    @requestStartTime DATETIME,
    @requestEndTime DATETIME,
    @brands VARCHAR(255),
    @platforms VARCHAR(255),
    @errorlogOnly bit,
    @platformTranId VARCHAR(255),
    @gameTranId VARCHAR(255),
    @seqNumber VARCHAR(255)
  )
as
  BEGIN
    DECLARE @requestStartTimeLocal DATETIME
    DECLARE @requestEndTimeLocal DATETIME
    DECLARE @brandsLocal VARCHAR(255)
    DECLARE @platformsLocal VARCHAR(255)
    DECLARE @errorlogOnlyLocal BIT
    DECLARE @platformTranIdLocal VARCHAR(255)
    DECLARE @gameTranIdLocal VARCHAR(255)
    DECLARE @seqNumberLocal VARCHAR(255)
    DECLARE @MAX_RECORDS INT
    DECLARE @SQL nvarchar(max);

    SET @requestStartTimeLocal = @requestStartTime
    SET @requestEndTimeLocal = @requestEndTime
    SET @brandsLocal = @brands
    SET @platformsLocal = @platforms
    SET @errorlogOnlyLocal = @errorlogOnly
    SET @platformTranIdLocal = @platformTranId
    SET @gameTranIdLocal = @gameTranId
    SET @seqNumberLocal = @seqNumber
    SET @MAX_RECORDS = 5000;

    SET @SQL = ' from admin_all.SSW_TRAN '

    IF @platformsLocal IS NOT NULL
      BEGIN
--         SET @SQL = @SQL + ' join PLATFORM on PLATFORM.CODE=SSW_TRAN.PLATFORM_CODE and PLATFORM.ID in (SELECT * FROM [admin_all].fc_splitDelimiterString(' + @platformsLocal  + ', '',''))'
        SET @SQL = @SQL + ' join admin_all.PLATFORM on admin_all.PLATFORM.CODE=admin_all.SSW_TRAN.PLATFORM_CODE and admin_all.PLATFORM.ID in (' + @platformsLocal  + ')'
      END

    SET @SQL = @SQL + ' where REQUEST_TIME >=''' +  CONVERT(nvarchar(25), @requestStartTimeLocal, 112) + ''''
                   + ' and REQUEST_TIME < ''' + CONVERT(nvarchar(25), @requestEndTimeLocal, 112) + ''''

    IF @brandsLocal IS NOT NULL
      BEGIN
        set @SQL = @SQL + ' and BRAND_ID in (' + @brandsLocal + ')'
      END

    IF @errorlogOnly = 1
      BEGIN
        set @SQL = @SQL + ' and (ERROR_CODE is NOT NULL or ERROR_MESSAGE IS NOT NULL)'
      END

    IF @platformTranId IS NOT NULL
      BEGIN
        set @SQL = @SQL + ' and PROVIDER_TRAN_ID =''' + @platformTranId + ''''
      END

    IF @gameTranIdLocal IS NOT NULL
      BEGIN
        set @SQL = @SQL + ' and GAME_TRAN_ID =''' + @gameTranIdLocal + ''''
      END

    IF @seqNumberLocal IS NOT NULL
      BEGIN
        set @SQL = @SQL + ' and SEQ_NUMBER =''' + @seqNumberLocal + ''''
      END

--     print @SQL

    DECLARE @totalCount int, @SQL_TOTAL nvarchar(max), @Proc sysname
    SET @Proc = 'dbo.sp_executesql'
    SET @SQL_TOTAL = 'select @ret=count(*) ' + @SQL
    exec  @Proc @SQL_TOTAL,  N'@ret int output', @totalCount OUTPUT

    if @totalCount >= @MAX_RECORDS
      BEGIN
            raiserror('Search criteria exceed maximum %d rows', 16, 1, @MAX_RECORDS)
      END

    SET @SQL = 'Select admin_all.SSW_TRAN.* ' + @SQL
    exec(@SQL)

  END

go
