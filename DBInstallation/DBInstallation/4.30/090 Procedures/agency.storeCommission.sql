set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[storeCommission]
    (@agent_id int, @plan_id int, @type varchar(20), @start_date datetime, @end_date datetime, @value numeric(38,18))
AS
BEGIN

    delete from agency.potential_commission
    where   agent_id = @agent_id
        and plan_id = @plan_id
        and type = @type
        and start_date = @start_date
        and end_date = @end_date


    insert into agency.potential_commission(agent_id, plan_id, type, start_date, end_date, value)
        values (@agent_id, @plan_id, @type, @start_date, @end_date, @value)

    delete from agency.commission_payment
        where   agent_id = @agent_id
            and plan_id = @plan_id
            and type = @type
            and start_date = @start_date
            and end_date = @end_date
            and state not like 'COMPLETED%'

    if not exists (
        select * from commission_payment
        where   agent_id = @agent_id
                    and plan_id = @plan_id
                    and type = @type
                    and start_date = @start_date
                    and end_date = @end_date
                    and state like 'COMPLETED%'
    )
    BEGIN
        insert into agency.commission_payment(agent_id, plan_id, type, start_date, end_date, value, state)
            values (@agent_id, @plan_id, @type, @start_date, @end_date, @value, 'PENDING')
    END
END

go
