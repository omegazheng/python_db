set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_GetFreeSpinOption
  (
    @PRODUCT_ID INT,
    @GAME_ID NVARCHAR(100)
  )
as
  begin

    select  id as id,
            PLATFORM_ID as productId,
            GAME_ID as gameId,
            GAME_NAME as gameName,
            LINES as lines,
            COINS as coins,
            DENOMINATIONS as denominations
    from
      admin_all.FREESPIN_OPTION
    where
      GAME_ID = @GAME_ID
      and PLATFORM_ID = @PRODUCT_ID

  end

go
