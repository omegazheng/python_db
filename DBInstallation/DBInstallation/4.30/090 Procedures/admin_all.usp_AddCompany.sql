set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddCompany
(
	@CompanyID int = null output ,
	@Code nvarchar(50),
	@Name nvarchar(200),
	@SubCompanyCode nvarchar(50) = null,
	@SubCompanyName nvarchar(200) = null,
	@SubCompanyParentID int
)
as
begin
	set nocount on 
	declare @SubCompanyID int = null
	select @CompanyID = CompanyID
	from admin_all.Company
	where Code = @Code
	if @@rowcount = 0
	begin
		insert into admin_all.Company(Code, Name, ParentCompanyID)
			values(@Code, @Name, @SubCompanyParentID)
		select @CompanyID = scope_identity()
	end
	if @SubCompanyCode is not null
		exec admin_all.usp_AddCompany @SubCompanyID output, @SubCompanyCode, @SubCompanyName, null, null, @CompanyID
	select @CompanyID = isnull(@SubCompanyID, @CompanyID)
end
go
