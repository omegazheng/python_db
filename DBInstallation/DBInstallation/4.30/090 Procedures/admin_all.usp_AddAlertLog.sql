set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_AddAlertLog
(
	@AlertCode varchar(20),
	@StackTrace varchar(max),
	@Message varchar(max),
	@SeverityLevel int = 0
)
as
begin
	set nocount, xact_abort on
	declare @StackTraceID uniqueidentifier, @MessageID uniqueidentifier, @EventCount int = null, @AlertLogID bigint = null
	declare @StackTraceB varbinary(max) = cast(isnull(@StackTrace, ' ') as varbinary(max)), @MessageB varbinary(max) = cast(isnull(@Message, ' ') as varbinary(max))
	
	exec admin_all.usp_SaveLargeValue @StackTraceB, @StackTraceID output
	exec admin_all.usp_SaveLargeValue @MessageB, @MessageID output

	begin transaction
	select @EventCount = EventCount + 1, @AlertLogID = AlertLogID
	from admin_all.AlertLog with(rowlock, xlock)
	where StartDate >= dateadd(minute, -10, getdate())
		and SeverityLevel = @SeverityLevel
		and StackTrace = @StackTraceID
		and Message = @MessageID
		and AlertCode = @AlertCode
	
	update admin_all.AlertLog
		set EventCount = @EventCount, DistributionListID = null, EndDate = getdate()
	where AlertLogID = @AlertLogID
	if @@rowcount = 0
	begin
		insert into admin_all.AlertLog(StartDate, EndDate, AlertCode, SeverityLevel, EventCount, DistributionListID, StackTrace, Message)
			values(getdate(),getdate(), @AlertCode, @SeverityLevel, 1, null, @StackTraceID, @MessageID)
	end
	commit
end

go
