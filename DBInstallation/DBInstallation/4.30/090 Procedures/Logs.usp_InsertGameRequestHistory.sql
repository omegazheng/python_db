set ansi_nulls, quoted_identifier on
go

create or alter procedure Logs.usp_InsertGameRequestHistory
	(
		@PartyID int,
		@SessionKey nvarchar(40),
		@RequestTime datetime2(3),
		@ResponseTime datetime2(3),
		@RequestIP varchar(50),
		@ServerName varchar(50),
		@ProductID int,
		@Method varchar(30),
		@Request nvarchar(500),
		@Response nvarchar(500)
	)
as
	begin
			insert into Logs.GameRequestHistory
			(
				PartyID,
				SessionKey,
				RequestTime,
				ResponseTime,
				RequestIP,
				ServerName,
				ProductID,
				Method,
				Request,
				Response
			)
			values
			(
				@PartyID,
				@SessionKey,
				@RequestTime,
				@ResponseTime,
				@RequestIP,
				@ServerName,
				@ProductID,
				@Method,
				@Request,
				@Response
			)
		end

go
