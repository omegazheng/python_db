set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DailyBalanceUpdater
(
	@Date datetime
)
as
begin
	set nocount, xact_abort on 
	select @Date = cast(@Date as date)
	
	select
		sum(case when a.tran_type='DEPOSIT' then a.AMOUNT_REAL else 0 end) as DEPOSIT,
		sum(case when a.tran_type='PAYMNT_FEE' then a.AMOUNT_REAL * -1 else 0 end) as FEE,
		sum(case when a.tran_type='WITHDRAWAL' then a.AMOUNT_REAL * -1 else 0 end) as WITHDRAWAL_REQUESTED,
		sum(case when a.tran_type='WITHDRAWAL' then a.AMOUNT_RELEASED_BONUS * -1 else 0 end) as WITHDRAWAL_REQUESTED_RELEASED_BONUS,
		sum(case when a.tran_type ='MAN_ADJUST' then a.AMOUNT_REAL
				 when a.tran_type = 'CHARGE_BCK' then a.AMOUNT_REAL
				 when a.tran_type = 'BONUS_REL' then a.AMOUNT_REAL
				 when a.tran_type = 'MAN_BONUS' then a.AMOUNT_REAL
				 when a.tran_type = 'GAME_ADJ' then a.AMOUNT_REAL
				 when a.tran_type = 'PLTFRM_BON' then a.AMOUNT_REAL
				 when a.tran_type = 'TOURN_WIN' then a.AMOUNT_REAL
				 else 0 end) as ADJUSTMENT,
		sum(case when a.tran_type ='MAN_ADJUST' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'CHARGE_BCK' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'BONUS_REL' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'MAN_BONUS' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'GAME_ADJ' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'PLTFRM_BON' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'TOURN_WIN' then a.AMOUNT_RELEASED_BONUS
				 else 0 end) as ADJUSTMENT_BONUS,
		sum(case when a.tran_type ='TRANSF_IN' then a.AMOUNT_REAL
				 when a.tran_type = 'TRANSF_OUT' then a.AMOUNT_REAL
				 when a.tran_type = 'TRANSF_RB' then a.AMOUNT_REAL
				 else 0 end) as TRANSFER,
		sum(case when a.tran_type ='TRANSF_IN' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'TRANSF_OUT' then a.AMOUNT_RELEASED_BONUS
				 when a.tran_type = 'TRANSF_RB' then a.AMOUNT_RELEASED_BONUS
				 else 0 end) as TRANSFER_RELEASED_BONUS,
		sum(case when a.tran_type ='TIPS' then a.AMOUNT_REAL else 0 end) as TIPS,
		sum(case when a.tran_type ='TIPS' then a.AMOUNT_RELEASED_BONUS else 0 end) as TIPS_RELEASED_BONUS,
		sum(case 
				when a.TRAN_TYPE <> 'ROLLBACK' and a.TRAN_TYPE in ('GAME_PLAY','ROLLBACK','GAME_BET','GAME_WIN', 'STAKE_DEC', 'REFUND','TOURN_WIN','TIPS') then -a.AMOUNT_REAL 
				when a.TRAN_TYPE = 'ROLLBACK' and r.TRAN_TYPE in ('GAME_PLAY','ROLLBACK','GAME_BET','GAME_WIN', 'STAKE_DEC', 'REFUND','TOURN_WIN','TIPS') then r.AMOUNT_REAL 
				else 0 end 
			)  as HOUSE_HOLD,
		sum(case 
				when a.TRAN_TYPE <> 'ROLLBACK' and a.TRAN_TYPE in ('GAME_PLAY','ROLLBACK','GAME_BET','GAME_WIN', 'STAKE_DEC', 'REFUND','TOURN_WIN','TIPS') then -a.AMOUNT_RELEASED_BONUS 
				when a.TRAN_TYPE = 'ROLLBACK' and r.TRAN_TYPE in ('GAME_PLAY','ROLLBACK','GAME_BET','GAME_WIN', 'STAKE_DEC', 'REFUND','TOURN_WIN','TIPS') then r.AMOUNT_RELEASED_BONUS
				else 0 end 
			) as HOUSE_HOLD_RELEASED_BONUS,
		u1.CURRENCY as CURRENCY,
		u1.BRANDID as BRANDID
		into #a
	from admin_all.ACCOUNT_TRAN a 
		inner join admin_all.ACCOUNT b1 on a.account_id = b1.id
		inner join external_mpt.USER_CONF u1 on b1.partyid = u1.partyid
		left outer loop join admin_all.ACCOUNT_TRAN r on r.ID = a.ROLLBACK_TRAN_ID
	where a.datetime>= @Date and a.datetime< dateadd(day, 1, @Date)
	group by  u1.CURRENCY, u1.BRANDID



	select			
			sum(p.AMOUNT_REAL) as WITHDRAWAL_APPROVED,
			sum(p.AMOUNT_RELEASED_BONUS) as WITHDRAWAL_APPROVED_RELEASED_BONUS,
			u2.CURRENCY as CURRENCY,
			u2.BRANDID as BRANDID
			into #b
	from admin_all.PAYMENT p 
		inner join admin_all.ACCOUNT b2 on p.ACCOUNT_ID = b2.id
		inner join external_mpt.USER_CONF u2 on b2.partyid = u2.partyid
	where p.STATUS = 'COMPLETED'
		and p.TYPE='WITHDRAWAL'
		and p.PROCESS_DATE >= @Date and PROCESS_DATE < dateadd(day, 1, @Date)
	group by u2.CURRENCY, u2.BRANDID
	
	
	select
		SUM(b.balance_real) as CLOSING_BALANCE,
		SUM(b.released_bonus) as CLOSING_RELEASED_BONUS,
		SUM(b.playable_bonus) as CLOSING_PLAYABLE_BONUS,
		u.CURRENCY as CURRENCY,
        u.BRANDID as BRANDID
		into #c
	from admin_all.ACCOUNT a
		inner join admin_all.DAILY_BALANCE_HISTORY b on a.id = b.Account_ID
		inner join external_mpt.USER_CONF u on a.PARTYID = u.PARTYID
	where b.datetime = @Date
	group by u.CURRENCY, u.BRANDID
	
	select sum(a.AMOUNT_REAL) as AMOUNT_UNDERFLOW,
		u1.CURRENCY as CURRENCY,
		u1.BRANDID as BRANDID
		into #d
	from admin_all.ACCOUNT_TRAN_UNDERFLOW a 
		inner join admin_all.ACCOUNT b1 on a.account_id = b1.id
		inner join external_mpt.USER_CONF u1 on b1.partyid = u1.partyid
	where a.datetime>= @Date and a.datetime< dateadd(day, 1, @Date)
	group by  u1.CURRENCY, u1.BRANDID


	select	sum(p.amount_real) as PENDING_WITHDRAWAL, 
			sum(p.amount_released_bonus) as PENDING_WITHDRAWAL_RELEASED_BONUS,
			u.CURRENCY as CURRENCY, u.BRANDID as BRANDID 
			into #e
	from  admin_all.PAYMENT p
		inner join admin_all.ACCOUNT a on p.account_id=a.id
		inner join external_mpt.USER_CONF u on a.partyid=u.partyid 
	where p.type='WITHDRAWAL' 
		and p.request_date < dateadd(day, 1, @Date)
		and (p.process_date >= @Date or p.process_date is null) 
	group by u.CURRENCY, u.BRANDID;

	--;with s as
	--(
		select  coalesce (a.BRANDID, b.BRANDID, c.BRANDID) as BRANDID,
				coalesce (a.CURRENCY, b.CURRENCY, c.CURRENCY) as CURRENCY,
				isnull(a.DEPOSIT, 0) as DEPOSIT, isnull(a.WITHDRAWAL_REQUESTED, 0) as WITHDRAWAL_REQUESTED, isnull(b.WITHDRAWAL_APPROVED, 0) as WITHDRAWAL_APPROVED, 
				isnull(a.ADJUSTMENT, 0) as ADJUSTMENT, isnull(c.CLOSING_BALANCE, 0) as CLOSING_BALANCE, isnull(a.HOUSE_HOLD, 0) as HOUSE_HOLD, 
				isnull(e.PENDING_WITHDRAWAL, 0) as WITHDRAWAL_PENDING, isnull(c.CLOSING_RELEASED_BONUS, 0) as CLOSING_RELEASED_BONUS, isnull(c.CLOSING_PLAYABLE_BONUS, 0) as CLOSING_PLAYABLE_BONUS, 
				isnull(a.WITHDRAWAL_REQUESTED_RELEASED_BONUS, 0) as WITHDRAWAL_REQUESTED_RELEASED_BONUS, isnull(b.WITHDRAWAL_APPROVED_RELEASED_BONUS, 0) as WITHDRAWAL_APPROVED_RELEASED_BONUS, isnull(e.PENDING_WITHDRAWAL_RELEASED_BONUS,0) as WITHDRAWAL_PENDING_RELEASED_BONUS, 
				isnull(a.HOUSE_HOLD_RELEASED_BONUS, 0) as HOUSE_HOLD_RELEASED_BONUS, isnull(a.TRANSFER, 0) as TRANSFER, isnull(d.AMOUNT_UNDERFLOW, 0) as AMOUNT_UNDERFLOW, 
				isnull(a.ADJUSTMENT_BONUS, 0) as ADJUSTMENT_BONUS, isnull(a.FEE, 0) as FEE, isnull(a.TIPS, 0) as TIPS, 
				isnull(a.TIPS_RELEASED_BONUS, 0) as TIPS_RELEASED_BONUS, isnull(a.TRANSFER_RELEASED_BONUS, 0) as TRANSFER_RELEASED_BONUS
		from #a a
			full outer join #b b on a.CURRENCY = b.CURRENCY and a.BRANDID = b.BRANDID
			full outer join #c c on isnull(a.CURRENCY, b.CURRENCY) = c.CURRENCY and isnull(a.BRANDID, b.BRANDID) = c.BRANDID
			full outer join #d d on coalesce(a.CURRENCY, b.CURRENCY, c.CURRENCY) = d.CURRENCY and coalesce(a.BRANDID, b.BRANDID, c.BRANDID) = d.BRANDID
			full outer join #e e on coalesce(a.CURRENCY, b.CURRENCY, c.CURRENCY, d.CURRENCY) = e.CURRENCY and coalesce(a.BRANDID, b.BRANDID, c.BRANDID, d.BRANDID) = e.BRANDID
	--)
	--merge admin_all.DW_CASH_LIABILITY_SUMMARY as t
	--using s on t.SUMMARY_DATE = @Date and t.BRANDID = s.BRANDID and t.CURRENCY = s.CURRENCY
	--when not matched then 
	--	insert (SUMMARY_DATE, DEPOSIT, WITHDRAWAL_REQUESTED, WITHDRAWAL_APPROVED, ADJUSTMENT, CLOSING_BALANCE, CURRENCY, HOUSE_HOLD, WITHDRAWAL_PENDING, BRANDID, CLOSING_RELEASED_BONUS, CLOSING_PLAYABLE_BONUS, WITHDRAWAL_REQUESTED_RELEASED_BONUS, WITHDRAWAL_APPROVED_RELEASED_BONUS, WITHDRAWAL_PENDING_RELEASED_BONUS, HOUSE_HOLD_RELEASED_BONUS, TRANSFER, AMOUNT_UNDERFLOW, ADJUSTMENT_BONUS, FEE, TIPS, TIPS_RELEASED_BONUS, TRANSFER_RELEASED_BONUS)
	--		values(@Date, s.DEPOSIT, s.WITHDRAWAL_REQUESTED, s.WITHDRAWAL_APPROVED, s.ADJUSTMENT, s.CLOSING_BALANCE, s.CURRENCY, s.HOUSE_HOLD, s.WITHDRAWAL_PENDING, s.BRANDID, s.CLOSING_RELEASED_BONUS, s.CLOSING_PLAYABLE_BONUS, s.WITHDRAWAL_REQUESTED_RELEASED_BONUS, s.WITHDRAWAL_APPROVED_RELEASED_BONUS, s.WITHDRAWAL_PENDING_RELEASED_BONUS, s.HOUSE_HOLD_RELEASED_BONUS, s.TRANSFER, s.AMOUNT_UNDERFLOW, s.ADJUSTMENT_BONUS, s.FEE, s.TIPS, s.TIPS_RELEASED_BONUS, s.TRANSFER_RELEASED_BONUS)
	--when matched and (s.DEPOSIT != t.DEPOSIT or s.WITHDRAWAL_REQUESTED != t.WITHDRAWAL_REQUESTED or s.WITHDRAWAL_APPROVED != t.WITHDRAWAL_APPROVED or s.ADJUSTMENT != t.ADJUSTMENT or s.CLOSING_BALANCE != t.CLOSING_BALANCE or s.CURRENCY != t.CURRENCY or s.HOUSE_HOLD != t.HOUSE_HOLD or s.WITHDRAWAL_PENDING != t.WITHDRAWAL_PENDING or s.BRANDID != t.BRANDID or s.CLOSING_RELEASED_BONUS != t.CLOSING_RELEASED_BONUS or s.CLOSING_PLAYABLE_BONUS != t.CLOSING_PLAYABLE_BONUS or s.WITHDRAWAL_REQUESTED_RELEASED_BONUS != t.WITHDRAWAL_REQUESTED_RELEASED_BONUS or s.WITHDRAWAL_APPROVED_RELEASED_BONUS != t.WITHDRAWAL_APPROVED_RELEASED_BONUS or s.WITHDRAWAL_PENDING_RELEASED_BONUS != t.WITHDRAWAL_PENDING_RELEASED_BONUS or s.HOUSE_HOLD_RELEASED_BONUS != t.HOUSE_HOLD_RELEASED_BONUS or s.TRANSFER != t.TRANSFER or s.AMOUNT_UNDERFLOW != t.AMOUNT_UNDERFLOW or s.ADJUSTMENT_BONUS != t.ADJUSTMENT_BONUS or s.FEE != t.FEE or s.TIPS != t.TIPS or s.TIPS_RELEASED_BONUS != t.TIPS_RELEASED_BONUS or s.TRANSFER_RELEASED_BONUS != t.TRANSFER_RELEASED_BONUS) then
	--	update set t.DEPOSIT = s.DEPOSIT, t.WITHDRAWAL_REQUESTED = s.WITHDRAWAL_REQUESTED, t.WITHDRAWAL_APPROVED = s.WITHDRAWAL_APPROVED, t.ADJUSTMENT = s.ADJUSTMENT, t.CLOSING_BALANCE = s.CLOSING_BALANCE, t.CURRENCY = s.CURRENCY, t.HOUSE_HOLD = s.HOUSE_HOLD, t.WITHDRAWAL_PENDING = s.WITHDRAWAL_PENDING, t.BRANDID = s.BRANDID, t.CLOSING_RELEASED_BONUS = s.CLOSING_RELEASED_BONUS, t.CLOSING_PLAYABLE_BONUS = s.CLOSING_PLAYABLE_BONUS, t.WITHDRAWAL_REQUESTED_RELEASED_BONUS = s.WITHDRAWAL_REQUESTED_RELEASED_BONUS, t.WITHDRAWAL_APPROVED_RELEASED_BONUS = s.WITHDRAWAL_APPROVED_RELEASED_BONUS, t.WITHDRAWAL_PENDING_RELEASED_BONUS = s.WITHDRAWAL_PENDING_RELEASED_BONUS, t.HOUSE_HOLD_RELEASED_BONUS = s.HOUSE_HOLD_RELEASED_BONUS, t.TRANSFER = s.TRANSFER, t.AMOUNT_UNDERFLOW = s.AMOUNT_UNDERFLOW, t.ADJUSTMENT_BONUS = s.ADJUSTMENT_BONUS, t.FEE = s.FEE, t.TIPS = s.TIPS, t.TIPS_RELEASED_BONUS = s.TIPS_RELEASED_BONUS, t.TRANSFER_RELEASED_BONUS = s.TRANSFER_RELEASED_BONUS
	--when not matched by source then
	--	delete
	--;
end

go
