set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_DeleteFreePlayPlan]
  (
    @ID INT
  )
as
  BEGIN
    set nocount on
    --   begin transaction

    delete from admin_all.FREEPLAY_PLAN where ID = @ID

    --   commit

  END

go
