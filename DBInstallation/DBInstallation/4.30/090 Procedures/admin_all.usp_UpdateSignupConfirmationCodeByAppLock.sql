set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_UpdateSignupConfirmationCodeByAppLock
(
    @Resource NVARCHAR(255),
    @LockTimeout INT,
    @PartyID INT,
    @ConfirmationCode NVARCHAR(255),
    @result INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT, xact_abort ON
    DECLARE @ret int
--    DECLARE @ConfirmationCode  NVARCHAR(255)
    DECLARE @count int

    SELECT @result = 0
    SELECT @count = 0

    IF @Resource IS NULL
        BEGIN
            RAISERROR ('Resource can not be null', 16, 1)
        END

    BEGIN transaction
        EXEC @ret = sp_getapplock @Resource, 'Exclusive', 'Transaction', -1
        if @ret < 0
            begin
                rollback
                raiserror('Resource %i is being acquired by another session.', 16, 1, @Resource)
            end

        SET NOCOUNT, xact_abort ON

        UPDATE external_mpt.USER_CONF
        SET CONFIRMATION_CODE= CONCAT('CONFIRMED-', CONFIRMATION_CODE)
        where PARTYID = @PartyID and CONFIRMATION_CODE = @ConfirmationCode
        select @result = @@rowcount

        COMMIT

    --
--     UPDATE external_mpt.USER_CONF
--     SET CONFIRMATION_CODE= CONCAT('CONFIRMED-', CONFIRMATION_CODE)
--     where PARTYID = @PartyID

--    PRINT @result

END

go
