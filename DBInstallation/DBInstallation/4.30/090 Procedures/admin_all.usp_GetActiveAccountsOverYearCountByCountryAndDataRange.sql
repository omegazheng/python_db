set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetActiveAccountsOverYearCountByCountryAndDataRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

    SELECT
      count(*) as counts,
      country as COUNTRY
    FROM
      (
        SELECT
          DISTINCT
          dw.PARTY_ID,
          uc.COUNTRY
        FROM
          [admin_all].[DW_GAME_PLAYER_DAILY] dw
        JOIN external_mpt.USER_CONF uc ON uc.PARTYID = dw.PARTY_ID
        WHERE
          --dw.BRANDID in (:brandIds) and
          dw.SUMMARY_DATE >= @oneYearBack
          AND dw.SUMMARY_DATE < @endDateLocal
      ) ACTIVE_YEAR
    group by country
    order by counts desc
  END

go
