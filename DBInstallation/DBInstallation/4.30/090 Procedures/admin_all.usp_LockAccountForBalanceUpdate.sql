set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_LockAccountForBalanceUpdate
	(
		@AccountID int
	)
as
	begin
		DECLARE @PartyId int
		select @PartyId=PARTYID from admin_all.account with (ROWLOCK, xlock) WHERE id = @AccountID
		select @PartyId as PartyId
	end

go
