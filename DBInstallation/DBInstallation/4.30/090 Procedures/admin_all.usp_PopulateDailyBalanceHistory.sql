set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_PopulateDailyBalanceHistory
(
	@StartDate datetime = null,
	@EndDate datetime = null
)
as
begin
	set nocount, xact_abort on 
	declare @AccountID int, @BalanceReal money, @ReleasedBonus money, @PlayableBonus money, @AccountTranID bigint
	if @StartDate is null
	begin
		select @StartDate = max(DATETIME) from admin_all.ACCOUNT_TRAN_REALTIME
		if @StartDate is null
			select @StartDate = max(DATETIME) from admin_all.ACCOUNT_TRAN_ALL
		if @StartDate is null
			select @StartDate = getdate()
		select @StartDate = cast(isnull(@StartDate, dateadd(day, -1, getdate())) as date)
	end
	
	select @StartDate = cast(@StartDate as date)
	select @EndDate = isnull(@EndDate, @StartDate)
	
	create table #History(AccountTranID bigint, ACCOUNT_ID int not null, BALANCE_REAL money not null, RELEASED_BONUS money not null, PLAYABLE_BONUS money not null, primary key (ACCOUNT_ID))
	while @StartDate <= @EndDate
	begin
		declare c cursor local fast_forward for
			select ID, BALANCE_REAL, RELEASED_BONUS, PLAYABLE_BONUS from admin_all.ACCOUNT order by ID desc
		open c
		fetch next from c into @AccountID, @BalanceReal, @ReleasedBonus, @PlayableBonus
		while @@fetch_status = 0
		begin
			select @AccountTranID = null
			select top 1 @AccountTranID = ID
				from admin_all.ACCOUNT_TRAN_REALTIME with(index=IDX_admin_all_ACCOUNT_TRAN_REALTIME_ACCOUNT_ID_DATETIME)
				where ACCOUNT_ID = @AccountID
					and DATETIME > dateadd(day, 1, @StartDate)
				order by DATETIME asc
				
			if @AccountTranID is null
			begin
				select top 1 @AccountTranID = ID
				from admin_all.ACCOUNT_TRAN_ALL with (index = IDX_admin_all_ACCOUNT_TRAN_ALL_ACCOUNT_ID_DATETIME)
				where ACCOUNT_ID = @AccountID
					and DATETIME > dateadd(day, 1, @StartDate)
				order by DATETIME asc
			end
			if @AccountTranID is null
			begin
				select top 1 @AccountTranID = ID
				from admin_all.ACCOUNT_TRAN_REALTIME with(index=IDX_admin_all_ACCOUNT_TRAN_REALTIME_ACCOUNT_ID_DATETIME)
				where ACCOUNT_ID = @AccountID
					and DATETIME < dateadd(day, 1, @StartDate) 
				order by DATETIME desc
			end
			if @AccountTranID is null
			begin
				select top 1 @AccountTranID = ID
				from admin_all.ACCOUNT_TRAN_ALL with (index = IDX_admin_all_ACCOUNT_TRAN_ALL_ACCOUNT_ID_DATETIME)
				where ACCOUNT_ID = @AccountID
					and DATETIME < dateadd(day, 1, @StartDate) 
				order by DATETIME desc
			end
___Next___:
			insert into #History(AccountTranID, ACCOUNT_ID, BALANCE_REAL, RELEASED_BONUS, PLAYABLE_BONUS)
				values(@AccountTranID, @AccountID, @BalanceReal, @ReleasedBonus, @PlayableBonus)
			fetch next from c into @AccountID, @BalanceReal, @ReleasedBonus, @PlayableBonus
		end
		close c 
		deallocate c
		
		update h
			set h.BALANCE_REAL = case when at.DATETIME < dateadd(day, 1, @StartDate)  then at.BALANCE_REAL else at.BALANCE_REAL - at.AMOUNT_REAL end,
				h.RELEASED_BONUS = case when at.DATETIME < dateadd(day, 1, @StartDate)  then at.BALANCE_RELEASED_BONUS else at.BALANCE_RELEASED_BONUS - at.AMOUNT_RELEASED_BONUS end,
				h.PLAYABLE_BONUS = case when at.DATETIME < dateadd(day, 1, @StartDate)  then at.BALANCE_PLAYABLE_BONUS else at.BALANCE_PLAYABLE_BONUS - at.AMOUNT_PLAYABLE_BONUS end
		from #History h
			inner join admin_all.ACCOUNT_TRAN at on h.AccountTranID = at.ID
		--select * from #History
	
		;with t as
		(
			select  * 
			from admin_all.DAILY_BALANCE_HISTORY 
			where DATETIME = @StartDate
		)
		merge t 
		using #History s on t.ACCOUNT_ID = s.ACCOUNT_ID
		when not matched then
			insert (ACCOUNT_ID, BALANCE_REAL, DATETIME, RELEASED_BONUS, PLAYABLE_BONUS)
				values(s.ACCOUNT_ID, s.BALANCE_REAL, @StartDate, s.RELEASED_BONUS, s.PLAYABLE_BONUS)
		when matched and (s.BALANCE_REAL <> t.BALANCE_REAL or s.RELEASED_BONUS <> t.RELEASED_BONUS or s.PLAYABLE_BONUS <> t.PLAYABLE_BONUS) then
			update set t.BALANCE_REAL = s.BALANCE_REAL, t.RELEASED_BONUS = s.RELEASED_BONUS, t.PLAYABLE_BONUS = s.PLAYABLE_BONUS
		when not matched by source then
			delete
		;
		
		select @StartDate = dateadd(day, 1, @StartDate)
	end
	
end

go
