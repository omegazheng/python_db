set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_RollbackTransaction
(
	@TransactionID bigint = null, 
	@BrandID int = null,
	@GameTranID nvarchar(100) = null, 
	@ProductID int = null,
	@ProductTranID nvarchar(100) = null
)
as
begin
	set nocount, xact_abort on
	declare @RollbackTransactionID bigint
	exec admin_all.usp_rollbackTransactionInternal @TransactionID, @BrandID, @GameTranID, @ProductID, @ProductTranID, @RollbackTransactionID output
	select * from admin_all.ACCOUNT_TRAN where ID = @RollbackTransactionID
end
go
