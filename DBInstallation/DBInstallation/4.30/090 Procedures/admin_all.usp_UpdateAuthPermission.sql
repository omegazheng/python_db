set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_UpdateAuthPermission
(
	@Permissions nvarchar(max) = null,
	@UserID integer = null,
	@UserType nvarchar(1000) = null
)
as
begin
	set nocount on
	--print @Permissions;
	create table #Permissions(PermissionType nvarchar(200) primary key)
	insert into #Permissions(PermissionType)
		select distinct rtrim(ltrim(Item))
		from admin_all.fc_splitDelimiterString(@Permissions,',')
		where rtrim(Item)<> '';

	;with s as
	(
		select PermissionType as PERMISSION_TYPE, @UserID as USER_ID, @UserType as USER_TYPE, 1 as ENABLE
		from #Permissions
	), t as
	(
		select  PERMISSION_TYPE, USER_ID, USER_TYPE, ENABLE 
		from admin_all.AUTH_PERMISSION where 
		USER_TYPE = @UserType and USER_ID = @UserID
	)
	merge t
	using s on t.USER_TYPE  = s.USER_TYPE collate database_default and t.USER_ID = s.USER_ID and s.PERMISSION_TYPE = t.PERMISSION_TYPE collate database_default
	when not matched then
		insert (USER_ID, USER_TYPE, PERMISSION_TYPE, ENABLE)
		values(@UserID, @UserType, s.PERMISSION_TYPE, s.enable)
	when matched and (s.ENABLE <> t.ENABLE) then
		update set t.enable = s.enable
	when not matched by source then
		delete
	;

	select  ID, PERMISSION_TYPE, USER_ID, USER_TYPE, ENABLE 
	from admin_all.AUTH_PERMISSION 
	where USER_TYPE = @UserType 
		and USER_ID = @UserID
		and ENABLE = 1
end

go
