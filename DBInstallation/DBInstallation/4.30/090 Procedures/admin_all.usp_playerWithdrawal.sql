set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_playerWithdrawal]
  (
    @updatorid        INT,
    @playerid         INT,
    @withdrawalAmount numeric(38,18),
    @misc             NVARCHAR(MAX)
    )
  AS
  BEGIN
    SET XACT_ABORT ON

    DECLARE @uid INT
    DECLARE @pid INT
    DECLARE @amount numeric(38,18)
    DECLARE @msg NVARCHAR(MAX)
    declare @uBrandID int, @pBrandID int


    SET @uid = @updatorid
    SET @pid = @playerid
    SET @amount = @withdrawalAmount
    SET @msg = CAST(@uid AS VARCHAR(10)) + ' withdrawal amount: ' + CAST(CAST(@amount AS decimal (38,2)) AS VARCHAR(60)) + ' for ' +
               CAST(@pid AS VARCHAR(10)) + '; ' + @misc

    DECLARE @pAccountID INT
    DECLARE @pAccountBalanceReal numeric(38,18)
    DECLARE @pAccountRelBonus numeric(38,18)
    DECLARE @pAccountPlaBonus numeric(38,18)
    DECLARE @uAccountId INT
    DECLARE @uAccountBalanceReal numeric(38,18)
    DECLARE @uAccountRelBonus numeric(38,18)
    DECLARE @uAccountPlaBonus numeric(38,18)

    SELECT
        @uAccountId = a.ID,
        @uAccountBalanceReal = a.BALANCE_REAL,
        @uAccountRelBonus = a.RELEASED_BONUS,
        @uAccountPlaBonus = a.PLAYABLE_BONUS,
        @uBrandID = u.BRANDID
    FROM admin_all.ACCOUNT a with(rowlock, updlock)
           inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
    WHERE u.PARTYID = @uid;

    SELECT
        @pAccountID = ID,
        @pAccountBalanceReal = BALANCE_REAL,
        @pAccountRelBonus = RELEASED_BONUS,
        @pAccountPlaBonus = PLAYABLE_BONUS,
        @pBrandID = u.BRANDID
    FROM admin_all.ACCOUNT a with(rowlock, updlock)
           inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
    WHERE u.PARTYID = @pid;

    DECLARE @pUserType INT
    SELECT @pUserType = user_type
    FROM external_mpt.USER_CONF
    WHERE PARTYID = @pid;

    DECLARE @deductFromBalanceReal numeric(38,18)
    DECLARE @deductFromReleasedBonus numeric(38,18)

    SELECT
        @deductFromBalanceReal = @amount,
        @deductFromReleasedBonus = 0;

    IF @pUserType = 0 AND @pAccountBalanceReal < @amount
      SELECT
          @deductFromBalanceReal = @pAccountBalanceReal,
          @deductFromReleasedBonus = @amount - @pAccountBalanceReal

    BEGIN TRANSACTION

      DECLARE @paymentId INT
      -- create payment
      INSERT INTO admin_all.PAYMENT (TYPE, METHOD, REF_NUMBER, IP_ADDRESS, REQUEST_DATE, PROCESS_DATE, STATUS, AMOUNT,
                                     ACCOUNT_ID, MISC, INFO, AMOUNT_REAL, AMOUNT_RELEASED_BONUS, REQ_BONUS_PLAN_ID,
                                     FEE, WORLD_PAY_CARD_ID, MANUAL_BANK_FIELDS, MANUAL_BANK_AGENT_ID, LAST_UPDATED_DATE,
                                     REJECTED_REASON, IS_CONVERTED, CONV_RATE, CONV_MARGIN_PERCENT, CONV_MARGIN_AMOUNT,
                                     CONV_AMOUNT, CONV_CURRENCY, CONV_RATE_AFTER_MARGIN, REQUEST_AMOUNT, REQUEST_METHOD)
      VALUES
      ('WITHDRAWAL', 'CASH', NULL, NULL, GETDATE(), GETDATE(), 'COMPLETED', @amount, @pAccountID, @msg, NULL,
       @deductFromBalanceReal,
       @deductFromReleasedBonus,
       NULL,
       0, NULL,
       NULL,
       NULL,
       GETDATE(),
       NULL,
       0,
       NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      SELECT @paymentId = SCOPE_IDENTITY()

      -- update player account
      UPDATE admin_all.ACCOUNT
      SET BALANCE_REAL = BALANCE_REAL - @deductFromBalanceReal,
          RELEASED_BONUS = RELEASED_BONUS - @deductFromReleasedBonus
      WHERE PARTYID = @pid;

      IF @deductFromReleasedBonus > 0
        BEGIN
          DECLARE @deductRelBonus numeric(38,18)
          SET @deductRelBonus = -@deductFromReleasedBonus
          DECLARE cBonus CURSOR LOCAL STATIC FOR
            SELECT
              ID,
              RELEASED_BONUS,
              RELEASED_BONUS_WINNINGS
            FROM admin_all.BONUS
            WHERE PARTYID = @pid
              AND STATUS NOT IN ('CANCELED')
            ORDER BY TRIGGER_DATE

          DECLARE @BonusID INT, @BonusReleasedBonus numeric(38,18), @BonusReleasedBonusWinning numeric(38,18), @IsChanged BIT

          OPEN cBonus
          FETCH NEXT FROM cBonus
            INTO @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning
          WHILE @@fetch_status = 0
          BEGIN
            SELECT @IsChanged = 0
            IF @deductRelBonus < 0
              BEGIN
                IF @BonusReleasedBonus > 0
                  BEGIN
                    IF @BonusReleasedBonus >= abs(@deductRelBonus)
                      BEGIN
                        SELECT @BonusReleasedBonus = @BonusReleasedBonus + @deductRelBonus
                        SELECT
                            @deductRelBonus = 0,
                            @IsChanged = 1
                      END
                    ELSE
                      BEGIN
                        SELECT
                            @deductRelBonus = @BonusReleasedBonus + @deductRelBonus,
                            @IsChanged = 1
                        SELECT @BonusReleasedBonus = 0
                      END
                  END
              END

            IF @deductRelBonus < 0
              BEGIN
                IF @BonusReleasedBonusWinning > 0
                  BEGIN
                    IF @BonusReleasedBonusWinning >= abs(@deductRelBonus)
                      BEGIN
                        SELECT @BonusReleasedBonusWinning = @BonusReleasedBonusWinning + @deductRelBonus
                        SELECT
                            @deductRelBonus = 0,
                            @IsChanged = 1
                      END
                    ELSE
                      BEGIN
                        SELECT
                            @deductRelBonus = @BonusReleasedBonusWinning + @deductRelBonus,
                            @IsChanged = 1
                        SELECT @BonusReleasedBonusWinning = 0
                      END
                  END
              END

            IF @IsChanged = 1
              BEGIN
                UPDATE admin_all.BONUS
                SET RELEASED_BONUS        = @BonusReleasedBonus,
                    RELEASED_BONUS_WINNINGS = @BonusReleasedBonusWinning
                WHERE ID = @BonusID
              END
            IF @deductRelBonus = 0
              BREAK;
            FETCH NEXT FROM cBonus
              INTO @BonusID, @BonusReleasedBonus, @BonusReleasedBonusWinning
          END
          CLOSE cBonus
          DEALLOCATE cBonus
        END

      DECLARE @reference VARCHAR(100)
      SET @reference = 'BEEVOUCHER ' + CAST(@uid AS VARCHAR(10)) + ' - ' + CAST(@pid AS VARCHAR(10))

      -- create accountTran
      DECLARE @accountTranId INT
      INSERT INTO admin_all.ACCOUNT_TRAN (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID,
                                          GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID,
                                          AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS,
                                          BALANCE_PLAYABLE_BONUS, AMOUNT_UNDERFLOW, AMOUNT_RAW_LOYALTY,
                                          BALANCE_RAW_LOYALTY, TRANSACTION_ON_HOLD_ID,
                                          SSW_TRAN_ID, REFERENCE, BRAND_ID)
      VALUES
      (@pAccountID, GETDATE(), 'WITHDRAWAL', -@deductFromBalanceReal, @pAccountBalanceReal - @deductFromBalanceReal, NULL,
       NULL, NULL, NULL, @paymentId,0, NULL,
       -@deductFromReleasedBonus, 0, @pAccountRelBonus - @deductFromReleasedBonus,
       @pAccountPlaBonus, 0, 0,
       0, NULL,
       NULL, @reference, @pBrandID);
      SELECT @accountTranId = @@identity

      -- create userActionLog
      INSERT INTO admin_all.USER_ACTION_LOG (DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME,
                                             OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
      VALUES (GETDATE(), NULL, @msg, @pid, 'WITHDRAWAL', @accountTranId, 'BALANCE_REAL', @pAccountBalanceReal,
              @pAccountBalanceReal - @deductFromBalanceReal, NULL);

      -- cancel bonus
      BEGIN
        DECLARE @cancelReleasedBonus numeric(38,18)
        DECLARE @cancelPlayableBonus numeric(38,18)
        SET @cancelReleasedBonus = 0
        SET @cancelPlayableBonus = 0
        DECLARE @cancelBonus VARCHAR(25)
        SET @cancelBonus = 'CANCELED'

        DECLARE @bonusCancelStrategy VARCHAR(100)
        SELECT @bonusCancelStrategy = VALUE
        FROM REGISTRY_HASH
        WHERE MAP_KEY LIKE '%bonus.withdrawalCancelStrategy%'

        -- set default bonus cancelation strategy
        IF @bonusCancelStrategy IS NULL
          SET @bonusCancelStrategy = 'oldest'

        SELECT b.*
        INTO #bonusTemp
        FROM BONUS b
               JOIN BONUS_PLAN p ON b.BONUS_PLAN_ID = p.ID
        WHERE b.PARTYID = @pid
          AND b.STATUS IN ('QUEUED', 'ACTIVE', 'SPENT_ACTIVE')
          AND p.IS_CANCEL_ON_WITHDRAWAL = 1

        DECLARE @bonusIdGeneral INT
        IF @bonusCancelStrategy = 'oldest'
          BEGIN
            SELECT TOP 1
                @bonusIdGeneral = ID,
                @cancelReleasedBonus = RELEASED_BONUS + RELEASED_BONUS_WINNINGS,
                @cancelPlayableBonus = PLAYABLE_BONUS + PLAYABLE_BONUS_WINNINGS
            FROM #bonusTemp
            ORDER BY TRIGGER_DATE ASC
          END

        IF @bonusCancelStrategy = 'newest'
          BEGIN
            SELECT TOP 1
                @bonusIdGeneral = ID,
                @cancelReleasedBonus = RELEASED_BONUS + RELEASED_BONUS_WINNINGS,
                @cancelPlayableBonus = PLAYABLE_BONUS + PLAYABLE_BONUS_WINNINGS
            FROM #bonusTemp
            ORDER BY TRIGGER_DATE DESC
          END

        IF @bonusIdGeneral IS NOT NULL
          BEGIN
            UPDATE BONUS
            SET STATUS = @cancelBonus, RELEASED_BONUS = 0, RELEASED_BONUS_WINNINGS = 0
            WHERE ID = @bonusIdGeneral
          END

        IF @bonusCancelStrategy = 'all'
          BEGIN
            DECLARE @bonusIdAll INT
            SELECT TOP 1
                @bonusIdAll = ID,
                @cancelReleasedBonus = RELEASED_BONUS + RELEASED_BONUS_WINNINGS,
                @cancelPlayableBonus = PLAYABLE_BONUS + PLAYABLE_BONUS_WINNINGS
            FROM #bonusTemp
            ORDER BY ID ASC

            WHILE @bonusIdAll IS NOT NULL
            BEGIN

              UPDATE BONUS
              SET STATUS = @cancelBonus, RELEASED_BONUS = 0, RELEASED_BONUS_WINNINGS = 0
              WHERE ID = @bonusIdAll

              SELECT TOP 1
                  @bonusIdAll = ID,
                  @cancelReleasedBonus = @cancelReleasedBonus + RELEASED_BONUS + RELEASED_BONUS_WINNINGS,
                  @cancelPlayableBonus = @cancelPlayableBonus + PLAYABLE_BONUS + PLAYABLE_BONUS_WINNINGS
              FROM #bonusTemp
              WHERE ID > @bonusIdAll
              ORDER BY ID

              IF @@ROWCOUNT = 0
                BREAK
            END
          END

        --update player account info
        SELECT
            @pAccountID = ID,
            @pAccountBalanceReal = BALANCE_REAL,
            @pAccountRelBonus = RELEASED_BONUS,
            @pAccountPlaBonus = PLAYABLE_BONUS
        FROM admin_all.ACCOUNT
        WHERE PARTYID = @pid;

        -- update player account when cancelling bonus

        DECLARE @newReleasedBonus numeric(38,18)
        DECLARE @newPlayableBonus numeric(38,18)
        SET @newReleasedBonus = 0
        SET @newPlayableBonus = 0

        IF @pAccountRelBonus - @cancelReleasedBonus > 0
          SET @newReleasedBonus = @pAccountRelBonus - @cancelReleasedBonus
        IF @pAccountPlaBonus - @cancelPlayableBonus > 0
          SET @newPlayableBonus = @pAccountPlaBonus - @cancelPlayableBonus

        UPDATE ACCOUNT
        SET RELEASED_BONUS = @newReleasedBonus,
            PLAYABLE_BONUS   = @newPlayableBonus
        WHERE PARTYID = @pid;

        IF (SELECT COUNT(*)
            FROM #bonusTemp) > 0
          BEGIN
            -- create accountTran
            DECLARE @accountTranId2 INT
            INSERT INTO admin_all.ACCOUNT_TRAN (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID,
                                                GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID,
                                                AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS,
                                                BALANCE_PLAYABLE_BONUS, AMOUNT_UNDERFLOW, AMOUNT_RAW_LOYALTY,
                                                BALANCE_RAW_LOYALTY,  TRANSACTION_ON_HOLD_ID,
                                                SSW_TRAN_ID, REFERENCE, BRAND_ID)
            VALUES
            (@pAccountID, GETDATE(), 'CANC_BONUS', 0, @pAccountBalanceReal, NULL, NULL, NULL,
             NULL, NULL,
             0, NULL,
             -@cancelReleasedBonus,
             -@cancelPlayableBonus,
             @newReleasedBonus,
             @newPlayableBonus,
             0, 0, 0, NULL, NULL, NULL, @pBrandID);
            SELECT @accountTranId2 = @@identity

            -- create userActionLog
            INSERT INTO admin_all.USER_ACTION_LOG (DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME,
                                                   OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
            VALUES (GETDATE(), NULL, @cancelBonus + ' Bonus', @pid, 'CANCEL_BONUS', @accountTranId2, 'PLAYABLE_BONUS',
                    @pAccountPlaBonus,
                    @newPlayableBonus, NULL);
          END

        DROP TABLE #bonusTemp
      END

      -- update player
      UPDATE external_mpt.USER_CONF
      SET LAST_WITHDRAW_DATE = GETDATE(),
          LAST_WITHDRAW_AMOUNT = @amount
      WHERE PARTYID = @pid;

      -- update agent account
      UPDATE admin_all.ACCOUNT
      SET BALANCE_REAL = BALANCE_REAL + @amount
      WHERE PARTYID = @uid;

      -- create accountTran
      INSERT INTO admin_all.ACCOUNT_TRAN (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID,
                                          GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID,
                                          AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS,
                                          BALANCE_PLAYABLE_BONUS, AMOUNT_UNDERFLOW, AMOUNT_RAW_LOYALTY,
                                          BALANCE_RAW_LOYALTY, TRANSACTION_ON_HOLD_ID,
                                          SSW_TRAN_ID, REFERENCE, BRAND_ID)
      VALUES
      (@uAccountID, GETDATE(), 'P_WITHDRAW', @amount, @uAccountBalanceReal + @amount, NULL, NULL, NULL, NULL, NULL,
       0, NULL,
       0,
       0,
       @uAccountRelBonus,
       @uAccountPlaBonus,
       0, 0, 0,
       NULL, NULL, @reference, @uBrandID);

      SELECT @accountTranId = @@identity

      -- create userActionLog
      INSERT INTO admin_all.USER_ACTION_LOG (DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME,
                                             OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
      VALUES (GETDATE(), NULL, @msg, @uid, 'P_WITHDRAW', @accountTranId, 'BALANCE_REAL', @uAccountBalanceReal,
              @uAccountBalanceReal + @amount, NULL);

      SELECT *
      FROM admin_all.ACCOUNT
      WHERE PARTYID = @pid;

    COMMIT

  END

go
