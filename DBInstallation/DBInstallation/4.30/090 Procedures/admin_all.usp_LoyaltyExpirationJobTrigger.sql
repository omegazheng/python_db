set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_LoyaltyExpirationJobTrigger
as
begin
	set nocount on
	SELECT u.PARTYID,  DATEADD(month, -bl.MONTHS_TO_EXPIRE_POINTS, getdate()) as ExpiryDate, a.id Account_ID
		into #Users
	FROM [admin_all].[BRAND_LOYALTY] bl with (NOLOCK) 
		inner join [external_mpt].[USER_CONF] u on bl.BRANDID = u.BRANDID
		inner join admin_all.ACCOUNT a on a.PARTYID = u.PARTYID
	where bl.MONTHS_TO_EXPIRE_POINTS is not null
		and a.RAW_LOYALTY_POINTS <> 0

	SELECT 
			u.PARTYID
	FROM #Users u
	where exists(
					select *
					from [admin_all].[ACCOUNT_TRAN] tranTable with (forceseek)
							
					where tranTable.ACCOUNT_ID = u.Account_ID
						and tranTable.TRAN_TYPE='GAME_BET'
						and tranTable.AMOUNT_RAW_LOYALTY > 0
						and tranTable.BALANCE_RAW_LOYALTY != 0
						and tranTable.DATETIME  < u.ExpiryDate
				)


	--return
	--SELECT *
	--FROM
	--(
	--	SELECT 
	--			u.PARTYID, tranTable.DATETIME as lastAccrualTime, 
	--			tranTable.AMOUNT_RAW_LOYALTY amountLPPoint, a.RAW_LOYALTY_POINTS as balanceLPPoint,  
	--			DATEADD(month, bl.MONTHS_TO_EXPIRE_POINTS, tranTable.DATETIME) as expireDate,
	--			row_number () over(partition by tranTable.ACCOUNT_ID order by tranTable.Datetime desc) RN
	--	FROM [admin_all].[ACCOUNT_TRAN] tranTable with (NOLOCK)
	--		inner join [admin_all].[ACCOUNT] a with (NOLOCK)  on tranTable.ACCOUNT_ID = a.id
	--		inner join [external_mpt].[USER_CONF] u with (NOLOCK) on a.PARTYID = u.PARTYID
	--		inner join [admin_all].[BRAND_LOYALTY] bl with (NOLOCK) on bl.BRANDID = u.BRANDID
	--	where tranTable.TRAN_TYPE='GAME_BET'
	--		and tranTable.AMOUNT_RAW_LOYALTY > 0
	--		and tranTable.BALANCE_RAW_LOYALTY != 0
	--		and a.RAW_LOYALTY_POINTS != 0
	--)  et
	--where et.expireDate is not null
	--	and et.expireDate <= getdate()
	--	and RN = 1
end

go
