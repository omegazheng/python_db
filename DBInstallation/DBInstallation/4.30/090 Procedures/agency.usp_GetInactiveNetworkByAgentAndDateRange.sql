set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetInactiveNetworkByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME, @base_start_date DATETIME, @base_end_date DATETIME)
AS
  BEGIN
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)
    declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);
    declare @subagents TABLE(partyid int, parentid int)

    insert into @subagents
        select
            partyid,
            parentid
        from external_mpt.user_conf as player
        where
            player.partyid <> @agent_id
            and player.user_type = 0
            and player.hierarchy.IsDescendantOf(@agentHierarchy) = 1

    if @user_type = 20
        select
            partyid,
            first_name + ' ' + last_name as name,
            user_type as type,
            parentid,
            hierarchy.ToString() as hierarchy
        from external_mpt.user_conf
        where partyid in (

            select partyid
            from @subagents as subagents
            where
                exists(
                    select *
                    from admin_all.account as account
                        inner loop join  admin_all.account_tran as t on account.id = t.account_id
                    where t.datetime >= @base_start_date and t.datetime < @base_end_date
                    and account.partyid = subagents.partyid
                )

            except

            select partyid
            from @subagents as subagents
            where
                exists(
                    select *
                    from admin_all.account as account
                        inner loop join  admin_all.account_tran as t on account.id = t.account_id
                    where t.datetime >= @start_date and t.datetime < @end_date
                    and account.partyid = subagents.partyid
                )
         )
     else
     begin
        select
            partyid,
            first_name + ' ' + last_name as name,
            user_type as type,
            parentid,
            hierarchy.ToString() as hierarchy
        from external_mpt.user_conf
        where partyid in (

            select parentid
            from @subagents as subagents
            where
                exists(
                    select *
                    from admin_all.account as account
                        inner loop join  admin_all.account_tran as t on account.id = t.account_id
                    where t.datetime >= @base_start_date and t.datetime < @base_end_date
                    and account.partyid = subagents.partyid
                )

            except

            select parentid
            from @subagents as subagents
            where
                exists(
                    select *
                    from admin_all.account as account
                        inner loop join  admin_all.account_tran as t on account.id = t.account_id
                    where t.datetime >= @start_date and t.datetime < @end_date
                    and account.partyid = subagents.partyid
                )
         )
     end


  END

go
