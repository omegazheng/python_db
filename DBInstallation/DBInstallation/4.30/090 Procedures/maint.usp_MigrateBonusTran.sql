set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_MigrateBonusTran
as
begin
	set nocount on
	set xact_abort on
	if object_id('admin_all.BONUS_TRAN_BeforeMigration') is null
		return
	declare @IDs table (ID bigint primary key)
	while exists(select * from admin_all.BONUS_TRAN_BeforeMigration)
	begin
		delete @IDs;
		begin transaction
		set identity_insert admin_all.BONUS_TRAN on
		insert into admin_all.BONUS_TRAN([ID], [TIMESTAMP], [BONUS_ID], [STATUS], [WAGERED_AMOUNT], [WAGERED_TOTAL], [WITHDRAWN_AMOUNT], [WITHDRAWN_TOTAL], [RELEASED_BONUS_WINNINGS_AMOUNT], [RELEASED_BONUS_WINNINGS_BALANCE], [RELEASED_BONUS_AMOUNT], [RELEASED_BONUS_BALANCE], [PLAYABLE_BONUS_WINNINGS_AMOUNT], [PLAYABLE_BONUS_WINNINGS_BALANCE], [PLAYABLE_BONUS_AMOUNT], [PLAYABLE_BONUS_BALANCE], [SUMMARY_DATE] )
			output inserted.ID into @IDs
			select top 1000 [ID], [TIMESTAMP], [BONUS_ID], [STATUS], [WAGERED_AMOUNT], [WAGERED_TOTAL], [WITHDRAWN_AMOUNT], [WITHDRAWN_TOTAL], [RELEASED_BONUS_WINNINGS_AMOUNT], [RELEASED_BONUS_WINNINGS_BALANCE], [RELEASED_BONUS_AMOUNT], [RELEASED_BONUS_BALANCE], [PLAYABLE_BONUS_WINNINGS_AMOUNT], [PLAYABLE_BONUS_WINNINGS_BALANCE], [PLAYABLE_BONUS_AMOUNT], [PLAYABLE_BONUS_BALANCE], [SUMMARY_DATE]
			from admin_all.BONUS_TRAN_BeforeMigration
			order by ID desc
		set identity_insert admin_all.BONUS_TRAN off
		delete admin_all.BONUS_TRAN_BeforeMigration where ID in (select a.ID from @IDs a)
		commit
	end
	drop table admin_all.BONUS_TRAN_BeforeMigration
	
	exec maint.usp_ForceNamingConvention 'admin_all.BONUS_TRAN'
end

go
