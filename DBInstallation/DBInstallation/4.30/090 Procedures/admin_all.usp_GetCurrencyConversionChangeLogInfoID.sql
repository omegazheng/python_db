set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_GetCurrencyConversionChangeLogInfoID @InfoID uniqueidentifier output
as
begin
	set nocount on
	declare @Description nvarchar(4000) = isnull(cast(session_context(N'CurrencyRateImportDescription') as nvarchar(4000)),  N'Manual Adjustment'),
			@ApplicationName nvarchar(128) = isnull(app_name(), N'Unknown'),
			@LoginName nvarchar(128) = isnull(system_user, N'Unknown'),
			@HostName nvarchar(128) = isnull(host_name(), N'Unknown'),
			@HostIPAddress nvarchar(128) = isnull(cast(connectionproperty('client_net_address') as nvarchar(max)), N'Unknown')
	select @InfoID = hashbytes('MD5',cast(@Description+'-'+@ApplicationName+'-'+@LoginName+'-'+@HostName+'-'+@HostIPAddress as varbinary(max)))
	if not exists(select * from admin_all.CurrencyConversionChangeLogInfo where InfoID = @InfoID)	
	begin	
		insert into admin_all.CurrencyConversionChangeLogInfo(InfoID, Description, ApplicationName, LoginName, HostName, HostIPAddress, Date)
			values(@InfoID, @Description, @ApplicationName, @LoginName, @HostName, @HostIPAddress, getdate())
	end
	
end

go
