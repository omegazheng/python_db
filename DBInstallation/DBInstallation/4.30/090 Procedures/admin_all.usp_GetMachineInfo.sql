set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetMachineInfo
    (
        @MachineID int
    )
    as
    begin
        select m.MachineID, l.Name from admin_all.Machine m
            join admin_all.Location l on l.LocationID = m.LocationID
        where m.MachineID = @MachineID
    end

go
