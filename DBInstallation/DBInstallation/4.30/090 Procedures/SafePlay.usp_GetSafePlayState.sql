set ansi_nulls, quoted_identifier on
go

create or alter procedure SafePlay.usp_GetSafePlayState
  (
    @AccountTranID BIGINT
  )
as
  begin

    declare @tempAccountTranId NVARCHAR(100)
    set @tempAccountTranId = CAST(@AccountTranID AS NVARCHAR(100));

    select STATE FROM SafePlay.SafePlayEFTState where AccountTranID = @tempAccountTranId

  end

go
