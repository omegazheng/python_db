set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_ExitAccountTranMaintenanceMode
as
begin
	set nocount, xact_abort on
	declare @SQL nvarchar(max)
	exec(@SQL)
	select @SQL = 'drop synonym admin_all.Synonym_ACCOUNT_TRAN_ALL'
	exec(@SQL)
	select @SQL = 'create synonym admin_all.Synonym_ACCOUNT_TRAN_ALL for admin_all.ACCOUNT_TRAN_ALL'
	exec(@SQL)

	begin transaction
select @SQL = 'alter view admin_all.ACCOUNT_TRAN
as
select ' +stuff((select ','+quotename(name) from sys.columns where object_id = object_id('admin_all.ACCOUNT_TRAN_ALL') order by column_id for xml path(''), type).value('.','nvarchar(max)'), 1, 1, '') +'
from admin_all.ACCOUNT_TRAN_REALTIME
union all
select ' +stuff((select ','+quotename(name) from sys.columns where object_id = object_id('admin_all.ACCOUNT_TRAN_ALL') order by column_id for xml path(''), type).value('.','nvarchar(max)'), 1, 1, '') +'
from admin_all.Synonym_ACCOUNT_TRAN_ALL a
'
	exec(@SQL)
	commit
	select @SQL = 'drop database ' + quotename(db_name()+'_snapshot')
	if db_id(quotename(db_name()+'_snapshot')) is not null
	exec(@SQL)
	exec admin_all.usp_CopyAccountTranRealtimeToAccountTranAll
end

go
