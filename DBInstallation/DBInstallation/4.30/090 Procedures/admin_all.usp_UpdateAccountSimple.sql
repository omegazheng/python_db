set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_UpdateAccountSimple]
(
	@ID int = null output,
	@PARTYID int = null, 
	@BALANCE_REAL numeric(38,18) = 0, 
	@RELEASED_BONUS numeric(38,18) =0,  
	@PLAYABLE_BONUS numeric(38,18) =0, 
	@RAW_LOYALTY_POINTS numeric(38,18)=0, 
	@SECONDARY_BALANCE numeric(38,18)=0, 
	@UNPAID_CIT numeric(38,18)=0, 
	@CIT numeric(38,18)=0
)
as
begin
	set nocount on
	if @ID is null
	begin
		select @ID = ID from admin_all.ACCOUNT where PARTYID = @PARTYID
	end
	if @ID is null
	begin
		insert into admin_all.ACCOUNT(PARTYID, BALANCE_REAL, RELEASED_BONUS, PLAYABLE_BONUS, RAW_LOYALTY_POINTS, SECONDARY_BALANCE, UNPAID_CIT, CIT)
			output inserted.*
			values(@PARTYID, isnull(@BALANCE_REAL,0), isnull(@RELEASED_BONUS,0), isnull(@PLAYABLE_BONUS, 0), isnull(@RAW_LOYALTY_POINTS,0), isnull(@SECONDARY_BALANCE,0), isnull(@UNPAID_CIT,0), isnull(@CIT,0))
		select @ID = scope_identity()
		return
	end
	update a
		set BALANCE_REAL = BALANCE_REAL + isnull(@BALANCE_REAL, 0),
			RELEASED_BONUS = RELEASED_BONUS + isnull(@RELEASED_BONUS, 0),
			PLAYABLE_BONUS = PLAYABLE_BONUS + isnull(@PLAYABLE_BONUS, 0),
			RAW_LOYALTY_POINTS = RAW_LOYALTY_POINTS + isnull(@RAW_LOYALTY_POINTS, 0),
			SECONDARY_BALANCE = SECONDARY_BALANCE + isnull(@SECONDARY_BALANCE, 0),
			UNPAID_CIT = UNPAID_CIT + isnull(@UNPAID_CIT, 0),
			CIT = CIT + isnull(@CIT, 0)
		output inserted.*
	from admin_all.account a
	where ID = @ID
end

go
