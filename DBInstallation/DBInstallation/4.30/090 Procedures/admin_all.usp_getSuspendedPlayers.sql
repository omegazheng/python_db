set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_getSuspendedPlayers]
  (
    @period  INT,
    @brandId INT
  )
AS
  BEGIN
    SELECT
      users.PARTYID,
      users.BRANDID
    FROM external_mpt.user_conf users
      LEFT JOIN admin_all.ACCOUNT acc ON users.PARTYID = acc.PARTYID
      LEFT JOIN admin_all.PAYMENT payment ON payment.ACCOUNT_ID = acc.id
    WHERE
      users.ACTIVE_FLAG = 1 AND
      users.BRANDID = @brandId
      AND users.KYC_STATUS = 'N/A'
      AND (SELECT count(*)
           FROM payment
           WHERE payment.TYPE = 'DEPOSIT' AND payment.ACCOUNT_ID = acc.id AND
                 payment.PROCESS_DATE > (getdate() - (@period))) < 1
    GROUP BY users.PARTYID, users.BRANDID
  END

go
