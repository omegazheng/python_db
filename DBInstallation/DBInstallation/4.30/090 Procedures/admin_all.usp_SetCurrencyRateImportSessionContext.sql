set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_SetCurrencyRateImportSessionContext
(
	@Date datetime = null,
	@Description nvarchar(4000) = null
)
as
begin
	set nocount on
	exec sys.sp_set_session_context N'CurrencyRateImportDate', @Date
	exec sys.sp_set_session_context N'CurrencyRateImportDescription', @Description
end

go
