set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CreateProxy
as
begin
	set xact_abort, nocount on
	
	declare @ProxyName sysname = db_name() + '_Proxy',
			@SQL nvarchar(max), @SchemaName sysname, @ObjectName sysname
	declare @ProcName sysname = @ProxyName + '..sp_executesql'
	if db_id(@ProxyName) is not null
	begin
		select @SQL ='alter database ' + quotename(@ProxyName) + ' set single_user with rollback immediate;
drop database ' + quotename(@ProxyName)
		exec(@SQL)
	end
	select @ProxyName = quotename(@ProxyName)
	select @SQL = 'dbcc clonedatabase('+quotename(db_name())+', '+@ProxyName+')'
	exec(@SQL)
	select @SQL = 'alter database ' + @ProxyName + ' set read_write'
	exec(@SQL)

	declare c cursor local for
		select 'alter table ' +quotename(object_schema_name(parent_object_id))+'.'+ quotename(object_name(parent_object_id)) + ' drop constraint ' + quotename(name) + ';' from sys.foreign_keys
	open c 
	fetch next from c into @SQL
	while @@fetch_status = 0
	begin
		exec @ProcName @SQL
		fetch next from c into @SQL
	end
	close c
	deallocate c

	declare c cursor local for
		select 
			schema_name(schema_id), name
		from sys.views
		where schema_id not in (schema_id('sys'))
	open c 
	fetch next from c into @SchemaName, @ObjectName
	while @@fetch_status = 0
	begin
		select @SQL = 'drop view ' + quotename(@SchemaName)+'.'+ quotename(@ObjectName) + ';'
		exec @ProcName @SQL
		select @SQL = 'create view ' + quotename(@SchemaName)+'.'+quotename(@ObjectName) + ' as select * from '+quotename(db_name())+'.' + quotename(@SchemaName)+'.'+quotename(@ObjectName) + ';'
		exec @ProcName @SQL
		select @SQL = 'create trigger ' + quotename(@SchemaName)+'.'+quotename('TRI_'+@ObjectName) + ' on ' +quotename(@SchemaName)+'.'+quotename(@ObjectName)+' instead of insert, delete, update as return'
		exec @ProcName @SQL
		fetch next from c into @SchemaName, @ObjectName
	end
	close c
	deallocate c

	declare c cursor local for
		select 
			schema_name(schema_id), name
		from sys.tables
		where schema_id not in (schema_id('sys'))
			and quotename(schema_name(schema_id))+'.'+quotename(name) <> '[admin_all].[STAFF_AUTH_TBL]'
	open c 
	fetch next from c into @SchemaName, @ObjectName
	while @@fetch_status = 0
	begin
		select @SQL = 'drop table ' + quotename(@SchemaName)+'.'+ quotename(@ObjectName) + ';'
		exec @ProcName @SQL
		select @SQL = 'create view ' + quotename(@SchemaName)+'.'+quotename(@ObjectName) + ' as select * from '+quotename(db_name())+'.' + quotename(@SchemaName)+'.'+quotename(@ObjectName) + ';'
		exec @ProcName @SQL
		select @SQL = 'create trigger ' + quotename(@SchemaName)+'.'+quotename('TRI_'+@ObjectName) + ' on ' +quotename(@SchemaName)+'.'+quotename(@ObjectName)+' instead of insert, delete, update as return'
		exec @ProcName @SQL
		fetch next from c into @SchemaName, @ObjectName
	end
	close c
	deallocate c
	
	select @SQL = ' drop table '+@ProxyName+'.[admin_all].[STAFF_AUTH_TBL]'
	exec(@SQL)
	select @SQL = 'select * into '+@ProxyName+'.[admin_all].[STAFF_AUTH_TBL] from [admin_all].[STAFF_AUTH_TBL]'
	exec(@SQL)
	select @SQL = 'ALTER TABLE [admin_all].[STAFF_AUTH_TBL] ADD  CONSTRAINT [PK_admin_all_STAFF_AUTH_TBL] PRIMARY KEY CLUSTERED ([STAFFID] ASC)'
	exec @ProcName @SQL

	select @SQL = 'ALTER TABLE [admin_all].[STAFF_AUTH_TBL] ADD  CONSTRAINT [UQ_admin_all_STAFF_AUTH_TBL_LOGINNAME] UNIQUE NONCLUSTERED ([LOGINNAME] ASC)'
	exec @ProcName @SQL
	
end

go
