set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayersByStaffAndGivenFieldSummary]
  (
    @staffid INT,
    @field   VARCHAR(20),
    @operatorId INT,
    @value NVARCHAR(50)
  )
AS
  BEGIN
    DECLARE @sql NVARCHAR(1000);
    DECLARE @condition NVARCHAR(255);

    IF @operatorId = 1
      SET @condition = ' like '''' + @value + N''%'''
    IF @operatorId = 2
      SET @condition = ' = '''' + @value + '''''
    IF @operatorId = 3
      SET @condition = ' like N''%'' + @value + N''%'''

    SET @sql = N'
    select
        COUNT(*) as totalRecord
    from external_mpt.user_conf u
        join admin.casino_brand_def brand on brand.brandid = u.brandid
        left join external_mpt.UserAssociatedAccount ua on u.partyid = ua.AssociatedPartyID
    where
        u.brandid in (select brandid from [admin_all].[staff_brand_tbl] where staffid = @staffid)
        and u.' + @field + @condition + 'and (ua.IsPrimary is NULL or ua.IsPrimary = 1)';

    EXEC sp_executesql @sql,
                       N'@staffid int, @value nvarchar(500)',
                       @staffid = @staffid, @value = @value

  END

go
