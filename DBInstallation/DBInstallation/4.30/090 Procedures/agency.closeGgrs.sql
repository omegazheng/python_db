set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[closeGgrs]
    (@date DATETIME)
AS
begin
    delete from agency.player_ggr where date = @date
    delete from agency.direct_ggr where date = @date
    delete from agency.network_ggr where date = @date

    DECLARE @allowRelBonus NVARCHAR(50);

    SELECT @allowRelBonus = VALUE
    FROM admin_all.REGISTRY_HASH WHERE MAP_KEY = 'agency.comm.allowRelBonus';

    IF @allowRelBonus = 'false'
      BEGIN
        insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)
          select
            agent.partyid agent,
            player.partyid player,
            product.id product,
            -sum(case
                 when a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real)
                 when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                 end) value,
            @date,
            -sum(case
                 when a.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real)
                 when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real)
                 else 0
                 end),
            -sum(case
                 when a.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real)
                 when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real)
                 else 0
                 end),
            -sum(case
                 when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                 else 0
                 end)
          from admin_all.account_tran as a
            join admin_all.platform as product on product.id = a.platform_id
            left join admin_all.account_tran as r on r.id = a.rollback_tran_id
            join admin_all.account as ac on ac.id = a.account_id
            join external_mpt.user_conf as player on player.partyid = ac.partyid
            join external_mpt.user_conf as agent on agent.partyid = player.parentid
          where a.datetime >= @date and a.datetime < DATEADD(day, 1, @date)
                and a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
                and product.id not in
                  (select
                    p.id
                    from admin_all.platform p
                    join admin_all.platform provider on p.provider_id = provider.id and provider.code = 'BIA_SPORTBOOK'
                  )
          group by agent.partyid, player.partyid, product.id
          order by agent.partyid, player.partyid, product.id

        insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)
          select
            agent.partyid agent,
            player.partyid player,
            product.id product,
            -sum(case
                 when a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real)
                 when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                 end) value,
            @date,
            -sum(case
                 when a.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real)
                 when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real)
                 else 0
                 end),
            -sum(case
                 when a.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real)
                 when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real)
                 else 0
                 end),
            -sum(case
                 when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                 else 0
                 end)
          from admin_all.betting_detail as b
            join admin_all.account_tran as a on b.bet_reference_num = a.game_tran_id and b.platform_id = a.platform_id
            left join admin_all.account_tran as r on a.rollback_tran_id = r.id
            join admin_all.account as ac on ac.id = a.account_id
            join external_mpt.user_conf as player on player.partyid = ac.partyid
            join external_mpt.user_conf as agent on agent.partyid = player.parentid
            join admin_all.platform as product on product.id = a.platform_id
          where b.settlement_date >= @date and b.settlement_date < DATEADD(day, 1, @date)
                and a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
                and product.id  in
                    (select
                       p.id
                     from admin_all.platform p
                       join admin_all.platform provider on p.provider_id = provider.id and provider.code = 'BIA_SPORTBOOK'
                    )
          group by agent.partyid, player.partyid, product.id
          order by agent.partyid, player.partyid, product.id

        insert into agency.direct_ggr(agent_id, product_id, value, date, wins, bets, released_bonuses)
          select agent_id, product_id, sum(value), @date, sum(wins), sum(bets), sum(released_bonuses)
          from agency.player_ggr
          where agency.player_ggr.date = @date
          group by agent_id, product_id
      END
    ELSE
      BEGIN
        insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)
        select
            agent.partyid agent,
            player.partyid player,
            product.id product,
            -sum(case
                    when a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                    when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                end) value,
            @date,
            -sum(case
                    when a.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                    when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                    else 0
                end),
            -sum(case
                    when a.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                    when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                    else 0
                end),
            -sum(case
                    when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                    else 0
                end)
        from admin_all.account_tran as a
            join admin_all.platform as product on product.id = a.platform_id
            left join admin_all.account_tran as r on r.id = a.rollback_tran_id
            join admin_all.account as ac on ac.id = a.account_id
                join external_mpt.user_conf as player on player.partyid = ac.partyid
                    join external_mpt.user_conf as agent on agent.partyid = player.parentid
        where a.datetime >= @date and a.datetime < DATEADD(day, 1, @date)
            and a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
            and product.id not in
                (select
                   p.id
                 from admin_all.platform p
                   join admin_all.platform provider on p.provider_id = provider.id and provider.code = 'BIA_SPORTBOOK'
                )
        group by agent.partyid, player.partyid, product.id
        order by agent.partyid, player.partyid, product.id

        insert into agency.player_ggr (agent_id, player_id, product_id, value, date, wins, bets, released_bonuses)
        select
            agent.partyid agent,
            player.partyid player,
            product.id product,
            -sum(case
                when a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'CASH_OUT', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
            end) value,
            @date,
            -sum(case
                 when a.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                 when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_WIN', 'CASH_OUT') then (a.amount_real + a.amount_released_bonus)
                 else 0
             end),
            -sum(case
                 when a.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                 when a.tran_type in ('ROLLBACK') and r.tran_type in ('GAME_BET', 'STAKE_DEC', 'REFUND') then (a.amount_real + a.amount_released_bonus)
                 else 0
             end),
            -sum(case
                 when a.tran_type in ('BONUS_REL') then (a.amount_released_bonus)
                 else 0
             end)
        from admin_all.betting_detail as b
            join admin_all.account_tran as a on b.bet_reference_num = a.game_tran_id and b.platform_id = a.platform_id
            left join admin_all.account_tran as r on a.rollback_tran_id = r.id
            join admin_all.account as ac on ac.id = a.account_id
                join external_mpt.user_conf as player on player.partyid = ac.partyid
                        join external_mpt.user_conf as agent on agent.partyid = player.parentid
            join admin_all.platform as product on product.id = a.platform_id
        where b.settlement_date >= @date and b.settlement_date < DATEADD(day, 1, @date)
            and a.tran_type in ('GAME_BET', 'GAME_WIN', 'ROLLBACK', 'BONUS_REL', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
              and product.id in
                  (select
                     p.id
                   from admin_all.platform p
                     join admin_all.platform provider on p.provider_id = provider.id and provider.code = 'BIA_SPORTBOOK'
                  )
        group by agent.partyid, player.partyid, product.id
        order by agent.partyid, player.partyid, product.id

        insert into agency.direct_ggr(agent_id, product_id, value, date, wins, bets, released_bonuses)
            select agent_id, product_id, sum(value), @date, sum(wins), sum(bets), sum(released_bonuses)
            from agency.player_ggr
            where agency.player_ggr.date = @date
            group by agent_id, product_id
      END
end

go
