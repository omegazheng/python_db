set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CheckSessionKey
	(
		@SessionKey [nvarchar](100),
		@PartyID [int] = null,
	  @ActiveSessionOnly bit = 0
	)
as
begin
	set nocount on
	if @ActiveSessionOnly=1
	begin
		if exists(SELECT * FROM admin_all.USER_WEB_SESSION where SESSION_KEY = @SessionKey and (@PartyID is null or @PartyID = PARTYID))
		begin
			select 1 value
		end
		else  
		begin
			select 0 value
		end
	end
	else 
	begin
		if (
			exists(SELECT * FROM admin_all.USER_LOGIN_LOG where SESSION_KEY = @SessionKey and (@PartyID is null or @PartyID = PARTYID)) 
			or exists(SELECT * FROM admin_all.USER_WEB_SESSION where SESSION_KEY = @SessionKey and (@PartyID is null or @PartyID = PARTYID)) 
			)
		begin
			select 1 value
		end
		else
		begin
			select 0 value
		end
	end
end

go
