set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getRevenueByMonth]
  (@startDate DATETIME, @endDate DATETIME, @isPlatform INT, @isBrand INT)
AS
  BEGIN
    DECLARE @platform BIT
    DECLARE @brand BIT
    DECLARE @startYear DATETIME
    DECLARE @endYear DATETIME
    SET @startYear = @startDate
    SET @endYear = @endDate
    SET @platform = @isPlatform
    SET @brand = @isBrand

    SELECT
      ss.*,
      p.NAME      AS PLATFORM_NAME,
      b.BRANDNAME AS BRAND_NAME
    FROM
      (
        SELECT
          MONTH,
          CURRENCY,
          CASE WHEN @platform = 1
            THEN PLATFORM_ID
          ELSE NULL END                       AS PLATFORM_ID,
          --   CASE WHEN @platform = 1 THEN PLATFORM ELSE NULL END AS PLATFORM,
          CASE WHEN @brand = 1
            THEN BRAND_ID
          ELSE NULL END                       AS BRAND_ID,
          --   CASE WHEN @brand = 1 THEN BRAND ELSE NULL END AS BRAND,
          SUM(HANDLE_REAL)                    AS HANDLE_REAL,
          SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
          SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
          SUM(PNL_REAL)                       AS PNL_REAL,
          SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
          SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
          SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
          SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
        FROM
          admin_all.DW_GAME_PLAYER_DAILY
        WHERE
          MONTH >= @startYear
          AND MONTH <= @endYear
        GROUP BY MONTH, CURRENCY,
          CASE WHEN @platform = 1
            THEN PLATFORM_ID
          ELSE NULL END,
          CASE WHEN @brand = 1
            THEN BRAND_ID
          ELSE NULL END
      ) ss
      LEFT JOIN admin_all.PLATFORM p
        ON ss.PLATFORM_ID = p.ID
      LEFT JOIN admin.CASINO_BRAND_DEF b
        ON ss.BRAND_ID = b.BRANDID
  END

go
