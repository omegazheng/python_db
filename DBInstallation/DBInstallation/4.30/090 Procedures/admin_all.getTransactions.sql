set ansi_nulls, quoted_identifier on
go


create or alter procedure [admin_all].[getTransactions]
  (@sp_name VARCHAR(50), @partyId INT, @startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    set @endDate =  DATEADD(day, 1,@endDate);
    select
      ACCOUNT_TRAN.ID,
      ACCOUNT_TRAN.ACCOUNT_ID,
      users.PARTYID,
      users.USERID,
      ACCOUNT_TRAN.DATETIME,
      ACCOUNT_TRAN.TRAN_TYPE,
      ACCOUNT_TRAN.AMOUNT_REAL,
      ACCOUNT_TRAN.BALANCE_REAL,
      ACCOUNT_TRAN.AMOUNT_RELEASED_BONUS,
      ACCOUNT_TRAN.BALANCE_RELEASED_BONUS,
      ACCOUNT_TRAN.AMOUNT_PLAYABLE_BONUS,
      ACCOUNT_TRAN.BALANCE_PLAYABLE_BONUS,
      ACCOUNT_TRAN.AMOUNT_UNDERFLOW,
      ACCOUNT_TRAN.AMOUNT_RAW_LOYALTY,
      ACCOUNT_TRAN.BALANCE_RAW_LOYALTY,
      ACCOUNT_TRAN.REFERENCE,
      ACCOUNT_TRAN.PLATFORM_TRAN_ID,
      ACCOUNT_TRAN.GAME_TRAN_ID,
      ACCOUNT_TRAN.PLATFORM_ID,
      Platform.CODE PLATFORM_CODE,
      ACCOUNT_TRAN.GAME_ID,
      game_info.NAME as GAME_NAME,
      ACCOUNT_TRAN.PAYMENT_ID,
      ACCOUNT_TRAN.ROLLED_BACK,
      ACCOUNT_TRAN.ROLLBACK_TRAN_ID,
      ACCOUNT_TRAN.REFERENCE,
      CASE WHEN ACCOUNT_TRAN.REFERENCE IS NULL THEN USER_ACTION_LOG.COMMENT ELSE ACCOUNT_TRAN.REFERENCE END AS SUBJECT,
      users.BRANDID,
      brands.BRANDNAME
    from ACCOUNT
      JOIN external_mpt.USER_CONF users on users.PARTYID = ACCOUNT.PARTYID
      JOIN admin.casino_brand_def brands on users.brandid = brands.brandid
      JOIN account_tran on account.id = ACCOUNT_TRAN.ACCOUNT_ID
      LEFT JOIN USER_ACTION_LOG on account_tran.id = USER_ACTION_LOG.ACTION_ID AND TRAN_TYPE IN( 'P_DEPOSIT', 'P_WITHDRAW')
      LEFT JOIN GAME_INFO on aCCOUNT_TRAN.game_id = game_info.game_id and Account_tran.PLATFORM_ID = GAME_INFO.PLATFORM_ID
      LEFT JOIN admin_all.PLATFORM on platform.id = account_tran.platform_id
    WHERE DATETIME >= @startDate and DATETIME < @endDate and users.PARTYID = @partyId
  END

go
