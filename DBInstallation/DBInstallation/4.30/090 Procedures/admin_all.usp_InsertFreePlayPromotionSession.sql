set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_InsertFreePlayPromotionSession
  (
    @FREEPLAY_PROMOTION_ID INT,
    @SESSION NVARCHAR(255)
  )
as
  begin
    DECLARE @ID int

    insert into admin_all.FREEPLAY_PROMOTION_SESSION
    (
      FREEPLAY_PROMOTION_ID,
      SESSION
    )
    values
      (
        @FREEPLAY_PROMOTION_ID,
        @SESSION
      )

    select @ID = @@IDENTITY
    select  id as id,
            freeplay_promotion_id as freePlayPromotionId,
            session as session
    from admin_all.FREEPLAY_PROMOTION_SESSION where ID = @ID

  end

go
