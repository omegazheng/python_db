set ansi_nulls, quoted_identifier on
go

create or alter procedure maint.usp_ReplicationCreateTarget
(
	@ConnectionName sysname,
	@TargetTable sysname
)
as
begin
	set nocount, xact_abort on 
	
	declare @SQL nvarchar(max), @SchemaName sysname, @TableName sysname, @FullTableName nvarchar(255),@ConnectionString varchar(max)
	declare @InterfaceName sysname, @TriggerName sysname
	select @ConnectionString = maint.fn_GetSQLConnectionStringByName(@ConnectionName)
	select	@SchemaName = maint.fn_ExecuteSQLScalarString(@ConnectionString, 'select object_schema_name(object_id('''+@TargetTable+'''))'),
			@TableName = maint.fn_ExecuteSQLScalarString(@ConnectionString, 'select object_name(object_id('''+@TargetTable+'''))')
	select	@FullTableName = quotename(@SchemaName) + '.' + quotename(@TableName),
			@InterfaceName = maint.fn_GetInterfaceName(@SchemaName, @TableName),
			@TriggerName = quotename(@SchemaName) + '.' + quotename('TRI_'+@TableName+'_Interface')
			

	select * into #TargetDefinition from maint.fn_GetTableSchema(@ConnectionString, @FullTableName)
	
	
	if maint.fn_ExecuteSQLScalarString(@ConnectionString, 'select case when object_id('''+@InterfaceName+''') is null then ''False'' else ''True'' end') = 'False'
	begin
		select @SQL = 'create view '+@InterfaceName+' as select 1 as one'
		exec maint.usp_ExecuteSQL @ConnectionString, @SQL
	end
	
	-- create interface
	select @SQL = 'alter view '+@InterfaceName+' as select ' + (select 'cast('+quotename(ColumnName)+' as '+DataType+') as '+quotename(ColumnName)+',' from #TargetDefinition order by ColumnID for xml path(''), type).value('.', 'varchar(max)') + 'cast(0 as bit) as ___IsDeleted___ from ' + @FullTableName
	exec maint.usp_ExecuteSQL @ConnectionString, @SQL
	
	if exists(select * from #TargetDefinition where ReadOnly = 0 and PrimaryKeyOrder = 0)
	begin
		select @SQL = case when maint.fn_ExecuteSQLScalarString(@ConnectionString, 'select case when object_id('''+@TriggerName+''') is null then ''False'' else ''True'' end') = 'False' then 'create trigger ' else 'alter trigger ' end + @TriggerName + ' on ' + @InterfaceName +'
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	'+isnull((select 'set identity_insert '+@FullTableName + ' on ' from #TargetDefinition where IsIdentity = 1), '')+'
	merge '+@FullTableName+' t
	using inserted s on '+stuff((select ' and s.'+quotename(ColumnName)+' = t.'+quotename(ColumnName) from #TargetDefinition where PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'varchar(max)'), 1, 5, '')+'
	when not matched and isnull(s.___IsDeleted___, 0) = 0 then
		insert (' +stuff((select ','+quotename(ColumnName) from #TargetDefinition where IsComputed <> 1 and IsTimestamp <> 1 order by ColumnID for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '')+ ')
			values (' +stuff((select ',s.'+quotename(ColumnName) from #TargetDefinition where IsComputed <> 1 and IsTimestamp <> 1 order by ColumnID for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '')+ ')
	when matched and isnull(s.___IsDeleted___, 0) = 0 and ('+stuff((select ' or s.'+quotename(ColumnName)+' is null and t.'+quotename(ColumnName)+' is not null or s.'+quotename(ColumnName)+' is not null and t.'+quotename(ColumnName)+' is null or '+case when DataType in ('xml', 'text','ntext', 'image') then 'cast(s.'+quotename(ColumnName)+' as varbinary(max)) <> cast(t.'+quotename(ColumnName)+' as varbinary(max))' else 's.'+quotename(ColumnName)+' <> t.'+quotename(ColumnName) end from #TargetDefinition where ReadOnly = 0 and PrimaryKeyOrder = 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'varchar(max)'), 1, 3, '')+') then
		update set '+stuff((select ',t.'+quotename(ColumnName)+' = s.'+quotename(ColumnName) from #TargetDefinition where PrimaryKeyOrder = 0 and ReadOnly = 0  order by PrimaryKeyOrder for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '')+'
	when matched and isnull(s.___IsDeleted___, 0) = 1 then
		delete
	;
	'+isnull((select 'set identity_insert '+@FullTableName + ' off ' from #TargetDefinition where IsIdentity = 1), '')+'
end'
		end
		else 
		begin
			select @SQL = case when maint.fn_ExecuteSQLScalarString(@ConnectionString, 'select case when object_id('''+@TriggerName+''') is null then ''False'' else ''True'' end') = 'False' then 'create trigger ' else 'alter trigger ' end + @TriggerName + ' on ' + @InterfaceName +'
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	'+isnull((select 'set identity_insert '+@FullTableName + ' on ' from #TargetDefinition where IsIdentity = 1), '')+'
	merge '+@FullTableName+' t
	using inserted s on '+stuff((select ' and s.'+quotename(ColumnName)+' = t.'+quotename(ColumnName) from #TargetDefinition where PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'varchar(max)'), 1, 5, '')+'
	when not matched and isnull(s.___IsDeleted___, 0) = 0 then
		insert (' +stuff((select ','+quotename(ColumnName) from #TargetDefinition where IsComputed <> 1 and IsTimestamp <> 1 order by ColumnID for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '')+ ')
			values (' +stuff((select ',s.'+quotename(ColumnName) from #TargetDefinition where IsComputed <> 1 and IsTimestamp <> 1 order by ColumnID for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '')+ ')
	when matched and isnull(s.___IsDeleted___, 0) = 1 then
		delete
	;
	'+isnull((select 'set identity_insert '+@FullTableName + ' off ' from #TargetDefinition where IsIdentity = 1), '')+'
end'

		end
		--exec maint.usp_PrintString @SQL
		exec maint.usp_ExecuteSQL @ConnectionString, @SQL
	
end

go
