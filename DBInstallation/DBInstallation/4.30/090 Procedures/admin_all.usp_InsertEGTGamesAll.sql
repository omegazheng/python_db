set ansi_nulls, quoted_identifier on
go


create or alter procedure admin_all.usp_InsertEGTGamesAll
as
  begin
    exec admin_all.usp_InsertEGTGame @gameID='533', @gameName='10 Burning Heart', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='548', @gameName='100 Burning Hot', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='847', @gameName='100 Cats', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='527', @gameName='100 Super Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='882', @gameName='2 Dragons', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='530', @gameName='20 Burning Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='544', @gameName='20 Dazzling Hot', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='854', @gameName='20 Diamonds', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='803', @gameName='20 Super Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='528', @gameName='30 Spicy Fruits', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='903', @gameName='4 of a Kind Bonus Poker', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='529', @gameName='40 Burning Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='545', @gameName='40 Hot and Cash', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='546', @gameName='40 Lucky King', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='547', @gameName='40 Mega Clover', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='804', @gameName='40 Super Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='532', @gameName='5 Burning Heart', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='810', @gameName='5 Dazzling Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='879', @gameName='50 Horses', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='851', @gameName='Action Money', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='812', @gameName='Age of Troy', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='884', @gameName='Aloha Party', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='808', @gameName='Amazing Amazonia', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='859', @gameName='Amazons'' Battle', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='511', @gameName='Aztec Glory', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='814', @gameName='Blue Heart', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='826', @gameName='Book of Magic', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='525', @gameName='Brave Cat', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='801', @gameName='Burning Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='506', @gameName='Caramel Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='850', @gameName='Casino Mania', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='871', @gameName='Cats Royal', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='829', @gameName='Circus Brilliant', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='514', @gameName='Coral Island', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='858', @gameName='Dark Queen', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='824', @gameName='Dice & Roll', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='508', @gameName='Dragon Reborn', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='813', @gameName='Dragon Reels', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='825', @gameName='Egypt Sky', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='822', @gameName='Extra Stars', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='880', @gameName='Extremely Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='501', @gameName='Fast Money', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='805', @gameName='Flaming Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='835', @gameName='Forest Band', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='820', @gameName='Fortune Spells', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='853', @gameName='Frog Story', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='811', @gameName='Fruits Kingdom', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='823', @gameName='Game of Luck', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='872', @gameName='Genius of Leonardo', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='817', @gameName='Grace of Cleopatra', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='837', @gameName='Great Adventure', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='864', @gameName='Great Empire', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='833', @gameName='Halloween', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='866', @gameName='Hot & Cash', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='841', @gameName='Imperial Wars', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='865', @gameName='Inca Gold II', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='901', @gameName='Jacks or Better Poker', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='902', @gameName='Joker Poker', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='503', @gameName='Jungle Adventure', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='836', @gameName='Kangaroo Land', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='846', @gameName='Kashmir Gold', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='515', @gameName='Like a Diamond', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='868', @gameName='Lucky & Wild', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='881', @gameName='Lucky Buzz', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='849', @gameName='Lucky Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='502', @gameName='Magellan', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='830', @gameName='Majestic Forest', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='898', @gameName='Mayan Spirit', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='875', @gameName='More Dice & Roll', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='516', @gameName='More Like a Diamond', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='869', @gameName='More Lucky & Wild', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='839', @gameName='Ocean Rush', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='848', @gameName='Oil Company II', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='819', @gameName='Olympus Glory', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='852', @gameName='Penguin Style', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='512', @gameName='Queen of Rio', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='840', @gameName='Rainbow Queen', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='889', @gameName='Retro Style', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='899', @gameName='Rich World', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='806', @gameName='Rise of RA', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='504', @gameName='Route of Mexico', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='809', @gameName='Royal Secrets', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='892', @gameName='Savanna''s Life', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='828', @gameName='Secrets of Alchemy', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='831', @gameName='Shining Crown', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='895', @gameName='Spanish Passion', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='855', @gameName='Summer Bliss', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='897', @gameName='Super 20', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='821', @gameName='Supreme Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='517', @gameName='Sweet Cheese', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='513', @gameName='The Big Journey', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='834', @gameName='The Explorers', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='885', @gameName='The Great Egypt', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='873', @gameName='The Secrets of London', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='896', @gameName='The Story of Alexander', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='863', @gameName='The White Wolf', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='802', @gameName='Ultimate Hot', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='832', @gameName='Venezia D''oro', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='818', @gameName='Versailles Gold', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='815', @gameName='Witches'' Charm', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='505', @gameName='Wonder Tree', @isTouch=1
    exec admin_all.usp_InsertEGTGame @gameID='523', @gameName='Wonderheart', @isTouch=0
    exec admin_all.usp_InsertEGTGame @gameID='807', @gameName='Zodiac Wheel', @isTouch=1
  end


go
