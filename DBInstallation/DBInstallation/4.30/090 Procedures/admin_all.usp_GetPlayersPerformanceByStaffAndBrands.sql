set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayersPerformanceByStaffAndBrands]
(
	@staffid int, 
	@brands nvarchar(2000), 
	@segments nvarchar(2000),
	@AggregateType tinyint = 0
)
AS
BEGIN
	set nocount on
	create table #Brands (BrandID int primary key)
	insert into #Brands(BrandID)
		SELECT distinct BRANDID
        FROM [admin_all].[STAFF_BRAND_TBL]
        WHERE STAFFID = @staffid
            AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands,','))
	create table #Segments(SegmentID int primary key)
	insert into #Segments(SegmentID)
		select cast(Item as int) segmentId from admin_all.fc_splitDelimiterString(@segments,',')

    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()

  declare @SourceFrom varchar(100)
  select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
  if @SourceFrom is null
    begin
      exec admin_all.usp_SetRegistry 'core.dashboard.source', 'dw'
      select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
    end

  if @SourceFrom = 'dw'
    begin

      SELECT
        USERID,
        USERS.PARTYID AS PARTYID,
        SUM((daily.PNL_REAL + daily.PNL_RELEASED_BONUS) * -1) AS NET_WIN,
        SUM(daily.PNL_PLAYABLE_BONUS * -1) AS NET_WIN_BONUS,
        USERS.CURRENCY
      FROM
        admin_all.DW_GAME_PLAYER_DAILY AS daily
        JOIN external_mpt.USER_CONF AS USERS ON daily.PARTY_ID = USERS.PARTYID
        left join admin_all.PLATFORM pl on daily.PLATFORM_ID = pl.ID
      WHERE
        SUMMARY_DATE > DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))
        AND SUMMARY_DATE <= CAST(GETDATE() AS DATE)
        AND daily.BRAND_ID IN (select x.BrandID from #Brands x)
        AND pl.segment_id in (select x.SegmentID from #Segments x)
      GROUP BY USERS.PARTYID, USERID, USERS.CURRENCY

      return
      END

  ELSE
    BEGIN
      -- Aggregate
      SELECT
        USERID,
        daily.PartyID AS PARTYID,
        SUM((daily.PNLReal + daily.PNLReleasedBonus) * -1) AS NET_WIN,
        SUM(daily.PNLPlayableBonus * -1) AS NET_WIN_BONUS,
        USERS.CURRENCY
      FROM admin_all.fn_GetPlayerDailyAggregate(@AggregateType, DATEADD(DAY, -1, CAST(@endDateLocal AS DATE)), dateadd(day, 1, CAST(GETDATE() AS DATE))) AS daily
        inner join external_mpt.USER_CONF AS USERS ON daily.PARTYID = USERS.PARTYID
        inner join admin_all.PLATFORM pl on daily.ProductID = pl.ID
      WHERE daily.BrandID IN (select x.BrandID from #Brands x)
            AND pl.segment_id in (select x.SegmentID from #Segments x)
      GROUP BY daily.PARTYID, USERID, USERS.CURRENCY
    END
  END

go
