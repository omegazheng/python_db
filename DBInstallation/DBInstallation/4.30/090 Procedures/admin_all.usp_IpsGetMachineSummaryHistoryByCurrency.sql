set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_IpsGetMachineSummaryHistoryByCurrency]
(
  @partyID INT,
  --   @machineID INT,
  @startDate VARCHAR(255),
  @endDate   VARCHAR(255),
  @pageNum   INT,
  @pageSize  INT,
  @orderBy VARCHAR(255)
)
AS
BEGIN

  DECLARE @count INT = 0;

  SET @count = (
    SELECT COUNT(*)
    FROM admin_all.MachinePlayGameSummary a WITH (NOLOCK)
    WHERE a.PartyID = @partyID
      --       AND a.MachineID = @machineID
      AND a.datetime >= @startDate
      AND a.datetime < @endDate
  )

  SELECT
    @count                                                        AS totalRecords,
    a.SummaryID                                                   AS summaryID,
    a.PartyID                                                     AS partyID,
    a.DATETIME                                                    AS dateTime,
    a.MachineID                                                   AS machineID,
    a.GameID                                                      AS gameID,
    p.code                                                        AS platformCode,
    ISNULL(a.AmountReal, 0)                                       AS amountReal,
    ISNULL(a.PlayableBonus, 0)                                    AS playableBonus,
    a.GamesPlayed                                                 AS gamesPlayed,
    ISNULL(a.TotalCreditWin, 0)                                   AS totalCreditWin,
    a.GameWin                                                     AS gameWin,
    ISNULL(a.AmountTransferIn, 0)                                 AS amountTransferIn,
    ISNULL(a.AmountTransferOut, 0)                                AS amountTransferOut,
    ISNULL(a.BonusTransferIn, 0)                                  AS bonusTransferIn,
    ISNULL(a.BonusTransferOut, 0)                                 AS bonusTransferOut,
    a.LoyaltyPoint                                                AS loyaltyPoint,
    lo.Name                                                       AS site,
    ISNULL(a.TotalPromoAmount, 0)                                 AS totalPromoAmount,
    ISNULL(a.TotalWinLossAmount, 0)                                 AS totalWinLossAmount,
    ISNULL(a.TicketIn, 0)                                           AS ticketIn,
    ISNULL(a.TicketOut, 0)                                          AS ticketOut,
    ISNULL(a.TicketPromoIn, 0)                                      AS ticketPromoIn,
    ISNULL(a.TicketPromoOut, 0)                                     AS ticketPromoOut
    INTO #Temp
  FROM admin_all.MachinePlayGameSummary a WITH (NOLOCK)
         LEFT JOIN PLATFORM p WITH (NOLOCK) ON p.id = a.ProductID
         LEFT JOIN Machine m WITH (NOLOCK) ON m.MachineID = a.MachineID
         LEFT JOIN Location lo WITH (NOLOCK) ON lo.LocationID = m.LocationID
  WHERE a.PartyID = @partyID
    AND a.datetime >= @startDate
    AND a.datetime < @endDate
  --         AND a.MachineID = @machineID

  SELECT * FROM (  SELECT
                     CASE
                       WHEN @OrderBy = 'DATETIMEASC' THEN ROW_NUMBER() OVER (ORDER BY dateTime)
                       WHEN @OrderBy = 'DATETIMEDESC' THEN ROW_NUMBER() OVER (ORDER BY dateTime DESC)
                       WHEN @OrderBy = 'MACHINEIDASC' THEN ROW_NUMBER() OVER (ORDER BY machineID)
                       WHEN @OrderBy = 'MACHINEIDDESC' THEN ROW_NUMBER() OVER (ORDER BY machineID DESC)
                       WHEN @OrderBy = 'GAMEIDASC' THEN ROW_NUMBER() OVER (ORDER BY gameID)
                       WHEN @OrderBy = 'GAMEIDDESC' THEN ROW_NUMBER() OVER (ORDER BY gameID DESC)
                       WHEN @OrderBy = 'AMOUNTREALASC' THEN ROW_NUMBER() OVER (ORDER BY amountReal)
                       WHEN @OrderBy = 'AMOUNTREALDESC' THEN ROW_NUMBER() OVER (ORDER BY amountReal DESC)
                       WHEN @OrderBy = 'PLAYABLEBONUSASC' THEN ROW_NUMBER() OVER (ORDER BY playableBonus)
                       WHEN @OrderBy = 'PLAYABLEBONUSDESC' THEN ROW_NUMBER() OVER (ORDER BY playableBonus DESC)
                       WHEN @OrderBy = 'GAMEWINASC' THEN ROW_NUMBER() OVER (ORDER BY gameWin)
                       WHEN @OrderBy = 'GAMEWINDESC' THEN ROW_NUMBER() OVER (ORDER BY gameWin DESC)
                       WHEN @OrderBy = 'AMOUNTTRANSFERINASC' THEN ROW_NUMBER() OVER (ORDER BY amountTransferIn)
                       WHEN @OrderBy = 'AMOUNTTRANSFERINDESC' THEN ROW_NUMBER() OVER (ORDER BY amountTransferIn DESC)
                       WHEN @OrderBy = 'AMOUNTTRANSFEROUTASC' THEN ROW_NUMBER() OVER (ORDER BY amountTransferOut)
                       WHEN @OrderBy = 'AMOUNTTRANSFEROUTDESC' THEN ROW_NUMBER() OVER (ORDER BY amountTransferOut DESC)
                       WHEN @OrderBy = 'BONUSTRANSFERINASC' THEN ROW_NUMBER() OVER (ORDER BY bonusTransferIn)
                       WHEN @OrderBy = 'BONUSTRANSFERINDESC' THEN ROW_NUMBER() OVER (ORDER BY bonusTransferIn DESC)
                       WHEN @OrderBy = 'BONUSTRANSFEROUTASC' THEN ROW_NUMBER() OVER (ORDER BY bonusTransferOut)
                       WHEN @OrderBy = 'BONUSTRANSFEROUTDESC' THEN ROW_NUMBER() OVER (ORDER BY bonusTransferOut DESC)
                       WHEN @OrderBy = 'LOYALRYASC' THEN ROW_NUMBER() OVER (ORDER BY loyaltyPoint)
                       WHEN @OrderBy = 'LOYALRYDESC' THEN ROW_NUMBER() OVER (ORDER BY loyaltyPoint DESC)

                       END
                       AS RowNum,*
                   FROM #Temp
                ) as alias
  WHERE RowNum BETWEEN @PageSize * (@PageNum - 1) + 1 AND @PageSize * @PageNum
  ORDER BY RowNum

  IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
    BEGIN
      DROP TABLE #Temp
    END


END

go
