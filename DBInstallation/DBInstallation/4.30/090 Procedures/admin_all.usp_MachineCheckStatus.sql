set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_MachineCheckStatus
(
	@EFTID bigint = null
)
as
begin
	set nocount on
	select EFTID, MachineTranID, AccountTranID, RollbackTranID, TransferType, Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, Reference, ProductTranID
	from admin_all.MachineTranEFT
	where EFTID = @EFTID
	union all
	select EFTID, null MachineTranID, AccountTranID, null RollbackTranID, 'MACHINE_EFT' TransferType, 'Pending' Status, Datetime, AmountReal, ReleasedBonus, PlayableBonus, null Reference, null ProductTranID
	from admin_all.MachineTranPendingEFT
	where EFTID = @EFTID
end

go
