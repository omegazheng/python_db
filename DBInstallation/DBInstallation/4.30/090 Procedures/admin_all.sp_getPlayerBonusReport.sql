set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getPlayerBonusReport]
  (@staffid int, @startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    set nocount on
    create table #Brands (BrandID int primary key)
    insert into #Brands(BrandID)
    SELECT distinct BRANDID
    FROM [admin_all].[STAFF_BRAND_TBL]
    WHERE STAFFID = @staffid

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      u.BRANDID,
      br.BRANDNAME,
      u.USERID,
      bp.TRIGGER_TYPE,
      b.ID AS BONUS_ID,
      b.STATUS,
      b.TRIGGER_DATE,
      b.AMOUNT,
      b.RELEASED_BONUS,
      b.PLAYABLE_BONUS,
      c.PLATFORM_ID,
      c.LAST_UPDATE_DATE
    FROM external_mpt.USER_CONF u
      JOIN admin.CASINO_BRAND_DEF br
        ON u.BRANDID = br.BRANDID
      JOIN admin_all.BONUS b
        ON u.PARTYID = b.PARTYID
      JOIN admin_all.BONUS_PLAN bp
        ON b.BONUS_PLAN_ID = bp.ID
      JOIN (
             SELECT
               PLATFORM_ID,
               BONUS_ID,
               MAX(LAST_UPDATE_DATE) AS LAST_UPDATE_DATE
             FROM
               admin_all.GAME_BONUS_BUCKET
             WHERE LAST_UPDATE_DATE >= @startDateLocal
                   AND LAST_UPDATE_DATE < @endDateLocal
             GROUP BY BONUS_ID, PLATFORM_ID
           ) c
        ON b.ID = c.BONUS_ID
    WHERE u.BRANDID IN (select x.BrandID from #Brands x)
  END

go
