set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetGamstopStatusByDateRange]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);


-- Passed

    DECLARE @passedGBTotal INT
    SET @passedGBTotal = (SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE < @endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_STATUS'
  and uc.COUNTRY = 'GB'
  and ual.COMMENT like '%(P)%')

 DECLARE @passedNonGBTotal INT
    SET @passedNonGBTotal = (
SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE < @endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_CHECK'
  and uc.COUNTRY <> 'GB'
  and ual.COMMENT like '%(P)%')

  DECLARE @passedTotal INT
  SET @passedTotal = @passedGBTotal + @passedNonGBTotal


-- Excludeed

    DECLARE @excludedGBTotal INT
    SET @excludedGBTotal = (SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE < @endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_CHECK'
  and uc.COUNTRY = 'GB'
  and ual.COMMENT like '%(Y)%')

 DECLARE @excludedNonGBTotal INT
    SET @excludedNonGBTotal = (
SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE <@endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_CHECK'
  and uc.COUNTRY <> 'GB'
  and ual.COMMENT like '%(Y)%')

  DECLARE @excludedTotal INT
  SET @excludedTotal = @excludedGBTotal + @excludedNonGBTotal

-- Not Registered
     DECLARE @notRegisteredGBTotal INT
    SET @notRegisteredGBTotal = (SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE < @endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_CHECK'
  and uc.COUNTRY = 'GB'
  and ual.COMMENT like '%(N)%')

 DECLARE @notRegisteredNonGBTotal INT
    SET @notRegisteredNonGBTotal = (
SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE < @endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_CHECK'
  and uc.COUNTRY <> 'GB'
  and ual.COMMENT like '%(N)%')

  DECLARE @notRegisteredTotal INT
  SET @notRegisteredTotal = @notRegisteredGBTotal + @notRegisteredNonGBTotal

-- Not available

     DECLARE @notAvailableGBTotal INT
    SET @notAvailableGBTotal = (SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE < @endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_CHECK'
  and uc.COUNTRY = 'GB'
  and ual.COMMENT like '%not available%')

 DECLARE @notAvailableNonGBTotal INT
    SET @notAvailableNonGBTotal = (
SELECT count(*)
FROM admin_all.USER_ACTION_LOG ual
  JOIN external_mpt.USER_CONF uc
    ON uc.PARTYID = ual.PARTYID
where
  ual.DATE >= @startDateLocal
  and ual.DATE < @endDatelocal
  and ual.ACTION_TYPE= 'GAMSTOP_CHECK'
  and uc.COUNTRY <> 'GB'
  and ual.COMMENT like '%%not available%%')

  DECLARE @notAvailableTotal INT
  SET @notAvailableTotal = @notAvailableGBTotal + @notAvailableNonGBTotal

-- Return

 SELECT @passedGBTotal AS passedGamstopGB,
        @passedNonGBTotal AS passedGamstopNonGB,
        @passedTotal AS passedGamstopTotal,
        @excludedGBTotal AS excludedGamstopGB,
        @excludedNonGBTotal AS excludedGamstopNonGB,
        @excludedTotal AS excludedGamstopTotal,
        @notRegisteredGBTotal AS notRegisteredGamstopGB,
        @notRegisteredNonGBTotal AS notRegisteredGamstopNonGB,
        @notRegisteredTotal AS notRegisteredGamstopTotal,
        @notAvailableGBTotal AS notAvailabLeGamstopGB,
        @notAvailableNonGBTotal AS notAvailableGamstopNonGB,
        @notAvailableTotal AS notAvailableGamstopTotal

  END

go
