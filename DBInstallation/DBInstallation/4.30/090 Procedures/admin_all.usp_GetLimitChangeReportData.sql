set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetLimitChangeReportData]
  ( @StartDate date,
    @EndDate date,
    --@Brands nvarchar(max),
    @Types nvarchar(max)
    --@StaffID int
  )
AS
  BEGIN

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

	set nocount on
    declare @Unlimited numeric(38,18) = 99999999999999999

--  create table #Brands (BrandID int primary key)
--	insert into #Brands(BrandID)
--		SELECT distinct BRANDID
--        FROM [admin_all].[STAFF_BRAND_TBL]
--        WHERE STAFFID = @StaffID
--            AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@Brands,','))

  create table #ChangeType (Type nvarchar(100) primary key)
  insert into #ChangeType(Type)
    select cast(rtrim(ltrim(Item)) as nvarchar(100)) as Type from admin_all.fc_splitDelimiterString(@Types,',')


    select
      'GB'  AS Country,
      isnull(sum(up),0) as UpCount,
      isnull(sum(down),0) as DownCount
--      BrandID,

    from (
           select
             ACTION_TYPE,
             cast ( cast (ISNULL(NULLIF(l.old_value, ''),  @Unlimited) as float) as numeric(38,18)) as old,
             cast ( cast (ISNULL(NULLIF(l.new_value, ''),  @Unlimited) as float) as numeric(38,18)) as new,
             case when (cast (cast (ISNULL(NULLIF(l.new_value, ''),  @Unlimited) as float) as numeric(38,18)) - cast ( cast (ISNULL(NULLIF(l.old_value, ''), @Unlimited) as float) as numeric(38,18))) > 0 then 1 else 0 end as up,
             case when (cast (cast (ISNULL(NULLIF(l.new_value, ''),  @Unlimited) as float) as numeric(38,18)) - cast ( cast (ISNULL(NULLIF(l.old_value, ''), @Unlimited) as float) as numeric(38,18))) < 0 then 1 else 0 end as down,
             u.PartyID,
--             u.BrandID,
             u.UserID,
             u.Country
           from admin_all.user_action_log l
           join external_mpt.user_conf u on l.PartyID = u.PartyID
 --          join #Brands b on b.BrandID = u.BRANDID
           join #ChangeType t on l.ACTION_TYPE = t.Type COLLATE DATABASE_DEFAULT
           where
             -- Possible Action Type  'CHANGE_DEPOSIT_LIMIT'
             date >= @startDateLocal  and date < @endDateLocal
             and u.COUNTRY = 'GB'
         ) result
--     group by
--       --BrandID,
--       Country

    union all

    select
      'NonGB' Country,
      isnull(sum(up),0) as UpCount,
      isnull(sum(down),0) as DownCount
      --      BrandID,

    from (
           select
             ACTION_TYPE,
             cast ( cast (ISNULL(NULLIF(l.old_value, ''),  @Unlimited) as float) as numeric(38,18)) as old,
             cast ( cast (ISNULL(NULLIF(l.new_value, ''),  @Unlimited) as float) as numeric(38,18)) as new,
             case when (cast ( cast (ISNULL(NULLIF(l.new_value, ''),  @Unlimited) as float) as numeric(38,18)) - cast ( cast (ISNULL(NULLIF(l.old_value, ''), @Unlimited) as float) as numeric(38,18))) > 0 then 1 else 0 end as up,
             case when (cast ( cast (ISNULL(NULLIF(l.new_value, ''),  @Unlimited) as float) as numeric(38,18)) - cast ( cast (ISNULL(NULLIF(l.old_value, ''), @Unlimited) as float) as numeric(38,18))) < 0 then 1 else 0 end as down,
             u.PartyID,
--             u.BrandID,
             u.UserID,
             u.Country
           from admin_all.user_action_log l
             join external_mpt.user_conf u on l.PartyID = u.PartyID
--             join #Brands b on b.BrandID = u.BRANDID
             join #ChangeType t on l.ACTION_TYPE = t.Type COLLATE DATABASE_DEFAULT
           where
             date >= @startDateLocal  and date < @endDateLocal
             and (u.COUNTRY IS null or u.COUNTRY <> 'GB')

         ) result
  END


go
