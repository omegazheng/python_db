set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerGamePlaySummaryV2]
  (@partyId int, @startDate DATETIME, @endDate DATETIME)
AS
  BEGIN

    SET DATEFIRST 1

    SELECT
      platform,
      game_name,
      sum(game_count)            as game_count,
      sum(bet_count)             as bet_count,
      sum(handle_real)           as handle_real,
      sum(HANDLE_RELEASED_BONUS) as handle_released_bonus,
      sum(HANDLE_PLAYABLE_BONUS) as handle_playable_bonus,
      sum(pnl_real)              as pnl_real,
      sum(PNL_RELEASED_BONUS)    as pnl_released_bonus,
      sum(PNL_PLAYABLE_BONUS)    as pnl_playable_bonus,
      currency
    FROM
      admin_all.dw_game_player_daily
    WHERE
      PARTY_ID = @partyId
      AND summary_date >= @startDate
      AND summary_date <= @endDate
    GROUP BY
      platform, game_name, currency
  END

--exec admin_all.[usp_GetPlayerGamePlaySummary] @partyId = 91429948, @startDate = '2018-01-15', @endDate = '2018-08-15'

go
