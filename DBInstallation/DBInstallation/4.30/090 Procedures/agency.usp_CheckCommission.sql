set ansi_nulls, quoted_identifier on
go
create or alter procedure agency.usp_CheckCommission(@StartDate datetime,@EndDate datetime )
as
	begin

		select
			commission.ID,
			commission.NAME,
			calculated.agent_id,
			u.userid,
			calculated.plan_id,
			calculated.type,
			isnull(calculated.value,0) as calculatedValue,
			isnull(payment.value,0) as actualValue,
			(isnull(calculated.value,0) - isnull(payment.value,0)) as diff,
			payment.comments,
			payment.start_date, payment.end_date, payment.processed_date
		from agency.potential_commission calculated
			full outer join agency.commission_payment payment on calculated.plan_id = payment.plan_id
			join admin_all.commission on calculated.plan_id = commission.id
			join external_mpt.USER_CONF u on calculated.agent_id = u.PARTYID
																			 and calculated.agent_id = payment.agent_id and calculated.start_date = payment.start_date and calculated.end_date = payment.end_date
																			 and calculated.TYPE = payment.TYPE
		where calculated.value <> payment.value and commission.ID is not null and calculated.TYPE IN ('BASE', 'BONUS')
					and calculated.product_id is null and calculated.start_date >= @StartDate and calculated.end_date <=  DATEADD(month, 2, @EndDate)
		order by payment.agent_id, payment.plan_id

	end


go
