set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetMutexByPartyidAndThreshold(@PartyID int, @ThresholdInSecond int)
as
begin
  set nocount, xact_abort on
  declare @ret int, @res nvarchar(200)
  begin transaction
  select @res = 'usp_GetMutexByPartyidAndThreshold-' + cast(@PartyID as nvarchar(20))
  exec @ret = sp_getapplock @res, 'Exclusive', 'Transaction', 0
  if @ret < 0
    begin
      rollback
      raiserror('Mutex for user %i is being acquired by another session.', 16, 1, @PartyID)
      return
    end

  if exists(select * from admin_all.DEPOSIT_MUTEX where PARTYID = @PartyID and UPDATE_TIME>dateadd(second, - @ThresholdInSecond,getdate()))
    begin
      rollback
      raiserror('Could not acquire mutex for user %i.', 16, 1, @PartyID)
      return
    end
  update admin_all.DEPOSIT_MUTEX
  set UPDATE_TIME = getdate()
  where partyid = @PartyID
  if @@rowcount = 0
    begin
      insert into admin_all.DEPOSIT_MUTEX(PARTYID, UPDATE_TIME)
      values(@PartyID, getdate())
    end
  commit

end

go
