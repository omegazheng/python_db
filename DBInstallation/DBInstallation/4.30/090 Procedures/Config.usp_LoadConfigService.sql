set ansi_nulls, quoted_identifier on
go


/******
When BrandId = -1, we would read from registry Hash
When BrandId = N where N >= 1, WE would read from brand registry
When BrandId = null, we would read from both registry and brand registry
******/

create or alter procedure Config.usp_LoadConfigService
	(
 		@BrandID int = null,
		@MapKey NVARCHAR (1000) = null
	)
as
	begin
	declare @GlobalRegistrySettingID int = -1

		if @BrandID is null
			begin
				if @MapKey is not null
					begin
						select
							@GlobalRegistrySettingID as BrandID,
							MAP_KEY as MapKey,
							VALUE as Value,
							ENCRYPTED as Encrypted,
							KEY_IDX as KeyIdx
						from admin_all.REGISTRY_HASH where MAP_KEY = @MapKey
						union
						select
							BRANDID as BrandId,
							MAP_KEY as MapKey,
							VALUE as Value,
							ENCRYPTED as Encrypted,
							KEY_IDX as KeyIdx
						from admin_all.BRAND_REGISTRY where MAP_KEY = @MapKey
					end
				else
				begin
					select
						@GlobalRegistrySettingID as BrandID,
						MAP_KEY as MapKey,
						VALUE as Value,
						ENCRYPTED as Encrypted,
						KEY_IDX as KeyIdx
					from admin_all.REGISTRY_HASH
					union
					select
						BRANDID as BrandId,
						MAP_KEY as MapKey,
						VALUE as Value,
						ENCRYPTED as Encrypted,
						KEY_IDX as KeyIdx
					from admin_all.BRAND_REGISTRY
				end
			end


		if @BrandID = @GlobalRegistrySettingID
			if @MapKey is not null
				begin
					select
						@BrandID as BrandId,
						MAP_KEY as MapKey,
						VALUE as Value,
						ENCRYPTED as Encrypted,
						KEY_IDX as KeyIdx
					from admin_all.REGISTRY_HASH where MAP_KEY = @MapKey
				end
			else
				begin
					select
						@BrandID as BrandId,
						MAP_KEY as MapKey,
						VALUE as Value,
						ENCRYPTED as Encrypted,
						KEY_IDX as KeyIdx
					from admin_all.REGISTRY_HASH
					return
				end


		if @BrandID >= 0
				begin
					if @MapKey is not null
						begin
							select
								BRANDID as BrandId,
								MAP_KEY as MapKey,
								VALUE as Value,
								ENCRYPTED as Encrypted,
								KEY_IDX as KeyIdx
							from admin_all.BRAND_REGISTRY where BRANDID = @BrandID and MAP_KEY = @MapKey
							end
					else
						begin
							select
								BRANDID as BrandId,
								MAP_KEY as MapKey,
								VALUE as Value,
								ENCRYPTED as Encrypted,
								KEY_IDX as KeyIdx
							from admin_all.BRAND_REGISTRY where BRANDID = @BrandID
						end
				end

end

go
