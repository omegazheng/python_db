set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_UpdateAccount]
  (
    @PartyID			int,
    @AmountReal			numeric(38,18),
    @ReleasedBonus		numeric(38,18) = null,
    @PlayableBonus		numeric(38,18) = null,
    @AmountSecondary	numeric(38,18) = null,
    @TranType			varchar(10),
    @PlatformID			int = null,
    @PlatformTranID		nvarchar(100) = null,
    @GameTranID			nvarchar(100) = null,
    @GameID				varchar(100) = null,
    @PaymentID			int = null,
    @AmountRawLoyalty	bigint = 0,
    @Reference			varchar(100) = null,
    @Cit				numeric(38,18) = 0,
    @DateTime			datetime2 = null,
    @UpdateBonuses		bit = 0,
	@MachineID			int = null,
	@AccountTranID		bigint =null output
  )
as
begin
	set nocount on
	set xact_abort on
	exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = @AmountReal, @ReleasedBonus = @ReleasedBonus, @PlayableBonus = @PlayableBonus, @AmountSecondary = @AmountSecondary,
												@TranType = @TranType, @PlatformID = @PlatformID, @PlatformTranID = @PlatformTranID, @GameTranID = @GameTranID, @GameID = @GameID,
												@PaymentID = @PaymentID, @AmountRawLoyalty = @AmountRawLoyalty, @Reference = @Reference, @Cit = @Cit, @DateTime = @DateTime, @UpdateBonuses = @UpdateBonuses,
												@MachineID = @MachineID, @AccountTranID = @AccountTranID output
	select
		@AccountTranID as AccountTranID,
		*
	from admin_all.ACCOUNT
	where PARTYID = @PartyID
end

go
