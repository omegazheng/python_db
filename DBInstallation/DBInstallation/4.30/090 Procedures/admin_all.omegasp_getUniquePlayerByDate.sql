set ansi_nulls, quoted_identifier on
go
-- get unique players by date, brand
-- trace back to 32 days
create or alter procedure [admin_all].[omegasp_getUniquePlayerByDate]
  (
    @date   DATETIME,
    @staffid INT
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = DATEADD(DD, 1, @date)
    SET @startDateLocal = DATEADD(DAY, -32, CAST(@date AS DATE))
    SELECT
      SUMMARY_DATE               AS DATE,
      BRAND_ID                   AS BRAND_ID,
      COUNT(DISTINCT (PARTY_ID)) AS UNIQUE_PLAYERS
    FROM ADMIN_ALL.DW_GAME_PLAYER_DAILY DW
    WHERE DW.SUMMARY_DATE >= @startDateLocal
          AND
          DW.SUMMARY_DATE < @endDateLocal
          AND BRAND_ID IN (SELECT BRANDID
                           FROM [admin_all].[STAFF_BRAND_TBL]
                           WHERE STAFFID = @staffid)
    GROUP BY SUMMARY_DATE, BRAND_ID;
  END

go
