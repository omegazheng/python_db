set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CheckDatabaseMail
as
begin
	set nocount on
	exec sp_configure 'show advanced options',1
	reconfigure with override 
	exec sp_configure 'Database Mail XPs',1
	reconfigure with override 

	if not exists(select * from msdb.dbo.sysmail_profile where  name = 'OmegaAlert') 
	begin
		exec msdb.dbo.sysmail_add_profile_sp @profile_name = 'OmegaAlert', @description  = 'This profile is used to send alert by Omega system.';
	end
	if not exists(select * from msdb.dbo.sysmail_account WHERE  name = 'OmegaAlert')
	begin
		exec msdb.dbo.sysmail_add_account_sp	@account_name            = 'OmegaAlert',
												@email_address           = 'alert@omegasys.eu',
												@display_name            = 'OmegaAlert',
												@replyto_address         = 'john.huang@sqlnotes.info',
												@description             = '',
												@mailserver_name         = 'smtp.office365.com',
												@mailserver_type         = 'SMTP',
												@port                    = '587',
												@username                = 'alert@omegasys.eu',
												@password                = 'Omega123', 
												@use_default_credentials =  0 ,
												@enable_ssl              =  1 ;
	end
  
	if not exists(
					select *
					from msdb.dbo.sysmail_profileaccount pa
						inner join msdb.dbo.sysmail_profile p on pa.profile_id = p.profile_id
						inner join msdb.dbo.sysmail_account a on pa.account_id = a.account_id  
					where p.name = 'OmegaAlert'
						and a.name = 'OmegaAlert'
				) 
	begin
		exec msdb.dbo.sysmail_add_profileaccount_sp @profile_name = 'OmegaAlert', @account_name = 'OmegaAlert', @sequence_number = 1 ;
	end
	declare @SQL nvarchar(max)
	select @SQL = (
					select 'exec msdb.dbo.sysmail_delete_principalprofile_sp @principal_name=N''guest'', @profile_name=N'''+p.name+''';'
					from msdb.dbo.sysmail_profile  p
						inner join msdb.dbo.sysmail_principalprofile pp on p.profile_id = pp.profile_id
					where pp.principal_sid = 0x00
						and pp.is_default = 1
					for xml path(''), type
					).value('.', 'nvarchar(max)')
	exec(@SQL)
	exec msdb.dbo.sysmail_add_principalprofile_sp @principal_name=N'guest', @profile_name=N'OmegaAlert', @is_default=1
end

go
