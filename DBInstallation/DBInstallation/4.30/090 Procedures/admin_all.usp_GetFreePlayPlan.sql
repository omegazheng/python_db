set ansi_nulls, quoted_identifier on
go

-- if brand_id != null, get the active FreePlayPlanList by brandId
-- brand_id_array != null, get all FreePlayPlanList by the brand array
-- if id != null, just get that active FreePlayPlan
-- if code != null, get FreePlayPlan, no care about status. check the code unique
-- all null, return all freePlayPlan

create or alter procedure admin_all.usp_GetFreePlayPlan
  (
    @BRAND_ID INT,
    @BRAND_ID_ARRAY VARCHAR(255),
    @ID INT,
    @CODE VARCHAR(30)
  )
as
  begin

    set nocount on
    -- expiry old plan.
    update admin_all.FREEPLAY_PLAN set STATUS='EXPIRED' where STATUS in ('ACTIVE', 'INACTIVE') and EXPIRY_DATE < getdate()

    IF @ID IS NOT NULL
      BEGIN
        select  id as id,
                code as code,
                name as name,
                status as status,
                description as description,
                brand_id as brandId,
                start_date as startDate,
                end_date as endDate,
                expiry_date as expiryDate,
                product_id as productId,
                channel as channel,
                isnull(bet_per_round,0) as betPerRound,
                rounds as rounds
        from
          admin_all.FREEPLAY_PLAN
        where
          ID = @ID
      END
    ELSE
      IF @BRAND_ID IS NOT NULL
        BEGIN
          select  id as id,
                  code as code,
                  name as name,
                  status as status,
                  description as description,
                  brand_id as brandId,
                  start_date as startDate,
                  end_date as endDate,
                  expiry_date as expiryDate,
                  product_id as productId,
                  channel as channel,
                  isnull(bet_per_round,0) as betPerRound,
                  rounds as rounds
          from
            admin_all.FREEPLAY_PLAN
          where
            brand_id = @BRAND_ID
            and status='ACTIVE'
        END

      ELSE
        IF @BRAND_ID_ARRAY IS NOT NULL
          BEGIN
            select  id as id,
                    code as code,
                    name as name,
                    status as status,
                    description as description,
                    brand_id as brandId,
                    start_date as startDate,
                    end_date as endDate,
                    expiry_date as expiryDate,
                    product_id as productId,
                    channel as channel,
                    isnull(bet_per_round,0) as betPerRound,
                    rounds as rounds
            from
              admin_all.FREEPLAY_PLAN
            where
              brand_id in (SELECT * FROM
                  [admin_all].fc_splitDelimiterString(@BRAND_ID_ARRAY, ','))
          END

        ELSE
      IF @CODE IS NOT NULL
        BEGIN
          select  id as id,
                  code as code,
                  name as name,
                  status as status,
                  description as description,
                  brand_id as brandId,
                  start_date as startDate,
                  end_date as endDate,
                  expiry_date as expiryDate,
                  product_id as productId,
                  channel as channel,
                  isnull(bet_per_round,0) as betPerRound,
                  rounds as rounds
          from
            admin_all.FREEPLAY_PLAN
          where
            code = @CODE
        END

      ELSE
--           return all
          BEGIN
            select  id as id,
                    code as code,
                    name as name,
                    status as status,
                    description as description,
                    brand_id as brandId,
                    start_date as startDate,
                    end_date as endDate,
                    expiry_date as expiryDate,
                    product_id as productId,
                    channel as channel,
                    isnull(bet_per_round,0) as betPerRound,
                    rounds as rounds
            from
              admin_all.FREEPLAY_PLAN
          END

  end

go
