set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetGbDemographicDetail]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      UPPER(GENDER)             AS GENDER,
      count(DISTINCT u.partyId) AS COUNT
    FROM
      dw_game_player_daily dw
      JOIN external_mpt.user_conf u ON dw.party_Id = u.partyId
    WHERE
      dw.COUNTRY = 'GB' AND dw.SUMMARY_DATE >= @startDateLocal AND dw.SUMMARY_DATE < @endDateLocal
    GROUP BY GENDER

  END

go
