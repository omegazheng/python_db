set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_RegistrationsNoActivity
(
	@DateFrom date,
	@DateTo date,
	@BrandIDs nvarchar(max),
	@CheckBoxNoDeposits bit,
	@CheckBoxNoGamePlayed bit
)
as
begin
	set nocount on
	declare @SQL nvarchar(max)
	create table #BrandIDs (BRANDID int not null primary key, BRANDNAME varchar(50))
	select @SQL = 'select BrandID, BrandName from admin.CASINO_BRAND_DEF where BRANDID in ('+@BrandIDs+')'
	insert into #BrandIDs(BRANDID, BRANDNAME)
		select brandid, BRANDNAME
		from admin.CASINO_BRAND_DEF 
		where brandid in (
							select cast(v as int)
							from (
									select rtrim(ltrim(Item)) v
									from admin_all.fc_splitDelimiterString(@BrandIDs, ',')
									where isnumeric(rtrim(ltrim(Item))) = 1
								) x
						)
	
	select @SQL = '
					select 
							A.account_id, A.partyid, A.currency,A.first_name, A.last_name,
							A.email,A.country,A.reg_date,A.registration_status, A.locked_status, 
							A.brandname,B.first_deposit_date, B.first_deposit_amount, C.last_login_date,D.number_of_bonus_triggered,
							D.number_of_bonus_released
					from (
							select a.id as account_id,u.partyid,u.currency,u.first_name,u.last_name, u.email,u.country,u.reg_date,u.registration_status, u.locked_status, b.brandname 
							from [external_mpt].[user_conf] u
								inner join #BrandIDs b on u.brandid = b.brandid
								inner join [admin_all].[account] a on a.partyid=u.partyid		
							where u.partyid in (
													SELECT u.partyid as partyid 
													FROM [external_mpt].[user_conf] u
													where u.PARTYID = a.PARTYID	and u.REG_DATE between @DateFrom and @DateTo 
														and (' 
															+ case when @CheckBoxNoDeposits = 1 then  ' u.MADE_DEPOSIT=0' else '' end
															+ case when @CheckBoxNoDeposits = 1 and @CheckBoxNoGamePlayed = 1 then  ' or ' else '' end
															+ case when @CheckBoxNoGamePlayed = 1 then ' u.partyid not in (select Party_ID from admin_all.DW_GAME_PLAYER_DAILY where SUMMARY_DATE >= @DateFrom)' else '' end 
															+ case when @CheckBoxNoDeposits = 0 and @CheckBoxNoGamePlayed = 0 then  ' 1=1 ' else '' end
															+ ')
													)
							) A 
							left join (
										select	p.account_id, p.process_date as first_deposit_date, 
												amount as first_deposit_amount 
										from [admin_all].[payment] p
												inner join (
																select ACCOUNT_ID, min(process_date) as first_deposit_date 
																from [admin_all].[payment]
																where TYPE=''DEPOSIT'' and STATUS=''COMPLETED'' 
																group by account_id
															) b  on p.account_id = b.account_id
										where p.PROCESS_DATE = b.first_deposit_date
									) B on A.account_id=B.ACCOUNT_ID
							left join (
										SELECT partyid, MAX(login_time) as last_login_date
										FROM [admin_all].[USER_LOGIN_LOG] 
										where login_type = ''Login'' 
										group by partyid
									) C on A.partyid = C.partyid
							left join (
											select A.partyid as partyid, number_of_bonus_triggered,
													number_of_bonus_released 
											from (
													SELECT PARTYID, COUNT(*) as number_of_bonus_triggered
													FROM [admin_all].[BONUS] group by partyid
												) A 
												left join (
															SELECT PARTYID, COUNT(*) as number_of_bonus_released 
															FROM [admin_all].[BONUS]
															where STATUS=''RELEASED'' 
															group by partyid
														) B on A.PARTYID = B.PARTYID
									) D  on A.PARTYID = D.PARTYID
					order by A.partyid';
	--print @SQL
	exec sp_executesql @SQL , N'@DateFrom date, @DateTo Date', @DateFrom, @DateTo
end

go
