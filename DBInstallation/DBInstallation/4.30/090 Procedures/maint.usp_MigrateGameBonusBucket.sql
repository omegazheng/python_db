set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_MigrateGameBonusBucket
as
begin
	set nocount on
	set xact_abort on
	if object_id('admin_all.GAME_BONUS_BUCKET_BeforeMigration') is null
		return
	declare @IDs table (ID bigint primary key)
	while exists(select * from admin_all.GAME_BONUS_BUCKET_BeforeMigration)
	begin
		delete @IDs;
		begin transaction
		set identity_insert admin_all.GAME_BONUS_BUCKET on
		insert into admin_all.GAME_BONUS_BUCKET([ID], [GAME_TRAN_ID], [PLATFORM_ID], [BONUS_ID], [CREATE_DATE], [LAST_UPDATE_DATE], [AMOUNT_REAL], [RELEASED_BONUS_WINNINGS], [RELEASED_BONUS], [PLAYABLE_BONUS_WINNINGS], [PLAYABLE_BONUS], [MAX_RELEASED_BONUS], [MAX_PLAYABLE_BONUS], [ACCOUNT_ID], [AMOUNT_FREE_BET], [GAME_ID])
			output inserted.ID into @IDs
			select top 1000 [ID], [GAME_TRAN_ID], [PLATFORM_ID], [BONUS_ID], [CREATE_DATE], [LAST_UPDATE_DATE], [AMOUNT_REAL], [RELEASED_BONUS_WINNINGS], [RELEASED_BONUS], [PLAYABLE_BONUS_WINNINGS], [PLAYABLE_BONUS], [MAX_RELEASED_BONUS], [MAX_PLAYABLE_BONUS], [ACCOUNT_ID], [AMOUNT_FREE_BET], [GAME_ID]
			from admin_all.GAME_BONUS_BUCKET_BeforeMigration
			order by ID desc
		set identity_insert admin_all.GAME_BONUS_BUCKET off
		delete admin_all.GAME_BONUS_BUCKET_BeforeMigration where ID in (select a.ID from @IDs a)
		commit
	end
	drop table admin_all.GAME_BONUS_BUCKET_BeforeMigration

	exec maint.usp_ForceNamingConvention 'admin_all.GAME_BONUS_BUCKET'
end

go
