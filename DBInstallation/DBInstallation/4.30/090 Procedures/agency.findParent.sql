set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findParent]
    (@agent_id int)
AS
  BEGIN
    select parentid as id from external_mpt.user_conf where partyid = @agent_id
  END

go
