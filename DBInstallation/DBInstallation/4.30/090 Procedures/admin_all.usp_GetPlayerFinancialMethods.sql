set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerFinancialMethods]
  (@partyid int)
AS
  BEGIN
  SET DATEFIRST 1
    declare @currency varchar(3) = (select currency from external_mpt.user_conf where partyid = @partyid)
    DECLARE @startDate DATETIME = DATEADD(YEAR, 100, 0)
    DECLARE @endDate DATETIME = GETDATE()

    SELECT *
        INTO #Temp
    FROM (SELECT CURRENCY, SUMMARY_DATE, METHOD, 'DEPOSIT' AS TRAN_TYPE, sum(deposit) AS AMOUNT
          FROM v_accounting_player_daily WITH (NOLOCK)
          WHERE summary_date >= @startDate
            AND summary_date <= @endDate
            AND currency = @currency
            AND partyid = @partyId
            AND DEPOSIT_COUNT > 0
          GROUP BY SUMMARY_DATE, CURRENCY, METHOD

--           UNION ALL
--
--           SELECT CURRENCY, SUMMARY_DATE, METHOD, 'WITHDRAWAL' AS TRAN_TYPE, sum(WITHDRAWAL) AS WITHDRAWAL
--           FROM v_accounting_player_daily WITH (NOLOCK)
--           WHERE summary_date >= @startDate
--             AND summary_date <= @endDate
--             AND currency = @currency
--             AND partyid = @partyId
--             AND WITHDRAWAL_COUNT > 0
--           GROUP BY SUMMARY_DATE, CURRENCY, METHOD
         ) METHODS

    SELECT tt.PERIOD      AS PERIOD,
           tt.CURRENCY    AS CURRENCY,
           tt.METHOD      AS METHOD,
           tt.TRAN_TYPE   AS TRAN_TYPE,
           SUM(tt.AMOUNT) AS AMOUNT
    FROM (SELECT top 5 'TODAY' AS PERIOD, CURRENCY, TRAN_TYPE, METHOD, AMOUNT AS AMOUNT
          FROM #Temp
          WHERE SUMMARY_DATE = CAST(@endDate AS DATE)

          UNION

          SELECT top 5 'YESTERDAY' AS PERIOD, CURRENCY, TRAN_TYPE, METHOD, AMOUNT AS AMOUNT
          FROM #Temp
          WHERE SUMMARY_DATE = DATEADD(DAY, -1, CAST(@endDate AS DATE))

          UNION

          SELECT top 5 'WTD' AS PERIOD, CURRENCY, TRAN_TYPE, METHOD, SUM(AMOUNT) AS AMOUNT
          FROM #Temp
          WHERE SUMMARY_DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDate), @endDate) AS DATE)
            AND SUMMARY_DATE <= CAST(@endDate AS DATE)
          GROUP BY CURRENCY, TRAN_TYPE, METHOD

          UNION

          SELECT top 5 'MTD' AS PERIOD, CURRENCY, TRAN_TYPE, METHOD, SUM(AMOUNT) AS AMOUNT
          FROM #Temp
          WHERE SUMMARY_DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDate), 0) AS DATE)
            AND SUMMARY_DATE <= CAST(@endDate AS DATE)
          GROUP BY CURRENCY, TRAN_TYPE, METHOD
          ORDER BY AMOUNT DESC

          UNION

          SELECT top 5 'LTD' AS PERIOD, CURRENCY, TRAN_TYPE, METHOD, SUM(AMOUNT) AS AMOUNT
          FROM #Temp
          WHERE SUMMARY_DATE <= CAST(@endDate AS DATE)
          GROUP BY CURRENCY, TRAN_TYPE, METHOD
          ORDER BY AMOUNT DESC
         ) tt
    GROUP BY tt.PERIOD, tt.CURRENCY, TRAN_TYPE, METHOD

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END


  END

go
