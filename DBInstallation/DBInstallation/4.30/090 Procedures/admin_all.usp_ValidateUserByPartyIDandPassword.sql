set ansi_nulls, quoted_identifier on
go


create or alter procedure [admin_all].[usp_ValidateUserByPartyIDandPassword]
  (@PartyID INT, @Password NVARCHAR(250))
  AS
  BEGIN
    declare @PasswordMatch bit = 0
    declare @SessionKey nvarchar(255) = null
    select @PasswordMatch = pwdcompare(@Password, PASSWORD) from external_mpt.USER_CONF where PARTYID =@PartyID
    if @PasswordMatch >= 1
      begin
        select top 1 @SessionKey = session_key from admin_all.user_web_session where partyId = @PartyID
        if @SessionKey is null
          begin
            select @SessionKey = left(REPLACE(NEWID(),'-',''),32)
            insert into admin_all.USER_WEB_SESSION (PARTYID, SESSION_KEY, LAST_ACCESS_TIME)
            values (@PartyID,  @SessionKey, getdate())
          end
      end
    select @SessionKey
  END

go
