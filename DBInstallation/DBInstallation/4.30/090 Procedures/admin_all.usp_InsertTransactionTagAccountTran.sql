set ansi_nulls, quoted_identifier on
go

-- if tag do not exist, insert first

create or alter procedure admin_all.usp_InsertTransactionTagAccountTran
  (
    @BRAND_ID INT,
    @TAG varchar(10),
    @TRANSACTION_TRAN_ID BIGINT
  )
as
  begin
    DECLARE @TagId INT
    DECLARE @TagTranId INT

    begin transaction

      SELECT @TagId = (SELECT ID from admin_all.TRANSACTION_TAG where BRANDID = @BRAND_ID and CODE = upper(@TAG))
      PRINT @TagId

    if (@TagId IS NULL)
        BEGIN
          insert into admin_all.TRANSACTION_TAG (CODE, NAME, BRANDID)
          VALUES (upper(@TAG), @TAG, @BRAND_ID)

          select @TagId = @@IDENTITY
        END

      -- insert to tagTran
      INSERT INTO admin_all.TRANSACTION_TAG_ACCOUNT_TRAN (ACCOUNT_TRAN_ID, TRANSACTION_TAG_ID)
      VALUES (@TRANSACTION_TRAN_ID, @TagId)

      select @TagTranId = @@IDENTITY
      select  id as id,
              account_tran_id as accountTranId,
              transaction_tag_id as transactionTagId
      from admin_all.TRANSACTION_TAG_ACCOUNT_TRAN where ID = @TagTranId

    commit

  end

go
