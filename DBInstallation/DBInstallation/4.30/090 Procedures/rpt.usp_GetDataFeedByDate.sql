set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetDataFeedByDate
(
	@BrandID bigint,
	@DateFrom datetime,
	@DateTo datetime = null
)
as
begin
	set nocount, xact_abort  on
	select	AccountTranID as ID, PartyID as PARTYID, UserID as USERID, GameInfoID as gameInfoId, GameID as gameId,
			GameTranID as gameTranId, ProductTranID platformTranId, TranType as tranType, Datetime as datetime,
			AmountReal + AmountPlayableBonus + AmountReleasedBonus as amount,
			BalanceReal + BalancePlayableBonus + BalanceReleasedBonus as balance,
			Currency as CURRENCY, ProductCode platformCode, RollbackTranID rollbackTranId, RollbackTranType rollbackTranType
	from admin_all.DataFeed
	where BrandID = @BrandID
		and Datetime >= @DateFrom
		and Datetime < isnull(@DateTo, dateadd(day, 1, getdate()))
	--order by AccountTranID asc
end
go
