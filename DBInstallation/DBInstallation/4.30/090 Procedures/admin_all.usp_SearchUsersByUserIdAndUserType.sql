set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_SearchUsersByUserIdAndUserType]
  (
    @userType VARCHAR(20),
    @name VARCHAR(50),
    @pageNum     INT,
    @pageSize    INT
  )
AS
  BEGIN
    DECLARE @TYPE VARCHAR(20)
    SET @TYPE = @userType;

  IF @TYPE = 'staff'
    BEGIN
      WITH TEMP AS (
          SELECT
            ROW_NUMBER()
            OVER (
              ORDER BY STAFFID ASC )     AS RowNum,
            STAFFID                      AS PARTYID,
            LOGINNAME                    AS USERID,
            FIRST_NAME + ' ' + LAST_NAME AS FULLNAME,
            ACTIVE,
            100                          AS TYPE
          FROM [admin_all].[STAFF_AUTH_TBL]
          WHERE LOGINNAME like '%' + @name + '%'
      )
      SELECT *
      FROM TEMP
      WHERE
        (RowNum > @pageSize * (@pageNum - 1))
        AND
        (RowNum <= @pageSize * @pageNum)
      ORDER BY RowNum
    END
  IF @TYPE = 'agency'
    BEGIN
      WITH TEMP AS (
          SELECT
            ROW_NUMBER()
            OVER (
              ORDER BY PARTYID ASC )     AS RowNum,
            PARTYID,
            USERID,
            FIRST_NAME + ' ' + LAST_NAME AS FULLNAME,
            ACTIVE_FLAG                  as ACTIVE,
            user_type                    AS TYPE
          FROM external_mpt.USER_CONF
          WHERE USERID like '%' + @name + '%'
            AND USER_TYPE IS NOT NULL AND user_type <> 0
      )
      SELECT *
      FROM TEMP
      WHERE
        (RowNum > @pageSize * (@pageNum - 1))
        AND
        (RowNum <= @pageSize * @pageNum)
      ORDER BY RowNum
    END
END

go
