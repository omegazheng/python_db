set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetDeviceType]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      'GB' AS TYPE,
      isnull(sum(SUMMARY.MOBILE), 0) as MOBILE,
      isnull(sum(SUMMARY.NON_MOBILE), 0) as NON_MOBILE
    FROM
      (
        SELECT
          CASE WHEN loginLog.IS_MOBILE = 1
            THEN 1
          ELSE 0 END AS MOBILE,
          CASE WHEN loginLog.IS_MOBILE = 0
            THEN 1
          ELSE 0 END AS NON_MOBILE
        FROM
          USER_LOGIN_LOG loginLog
          INNER JOIN [EXTERNAL_MPT].[USER_CONF] USERS ON USERS.PARTYID = loginLog.PARTYID
        WHERE
            loginLog.login_time >= @startDateLocal
            AND loginLog.login_time < @endDateLocal
            AND USERS.COUNTRY = 'GB'
      ) SUMMARY

    UNION

    SELECT
      'NONGB' AS TYPE,
      isnull(sum(SUMMARY.MOBILE), 0) as MOBILE,
      isnull(sum(SUMMARY.NON_MOBILE), 0) as NON_MOBILE
    FROM
      (
        SELECT
          CASE WHEN loginLog.IS_MOBILE = 1
            THEN 1
          ELSE 0 END AS MOBILE,
          CASE WHEN loginLog.IS_MOBILE = 0
            THEN 1
          ELSE 0 END AS NON_MOBILE
        FROM
          USER_LOGIN_LOG loginLog
          INNER JOIN [EXTERNAL_MPT].[USER_CONF] USERS ON USERS.PARTYID = loginLog.PARTYID
        WHERE
          loginLog.login_time >= @startDateLocal
          AND loginLog.login_time < @endDateLocal
          AND USERS.COUNTRY <> 'GB'
      ) SUMMARY


  END

go
