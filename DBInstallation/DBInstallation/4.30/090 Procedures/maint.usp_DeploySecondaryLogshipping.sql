set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_DeploySecondaryLogshipping
(
	@LogbackupPath varchar(1000) = 'D:\Dropbox\LSLLogShipping\omega_prod',
	@StandByPath nvarchar(1000) = 'D:\Dropbox\LSLLogShipping\omega_prod',
	@LogFileExtension nvarchar(1000) = '%.trn'
)
as
begin
	set xact_abort, nocount on
	-- create logshipping 
	declare @Proc sysname = 'msdb.dbo.sp_executesql', @SQL nvarchar(max)
	exec @Proc N'if object_id(''dbo.OmegaRestoreHistory'') is null
	begin
		create table dbo.OmegaRestoreHistory(DatabaseName varchar(128), FileName varchar(772) not null , Date datetime not null default(getdate()), primary key (DatabaseName, FileName) with(ignore_dup_key=on))
	end'
	exec @Proc N'if object_id(''[dbo].[ApplyLog]'') is null
	exec(''create or alter procedure [dbo].[ApplyLog] as --'')'

	exec @Proc N'ALTER procedure [dbo].[ApplyLog] 
(
	@DatabaseName sysname, -- = ''omega_prod'',
	@Path nvarchar(1000), -- = ''D:\Dropbox\LSLLogShipping\omega_prod'', 
	@StandByPath nvarchar(1000), -- = ''D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA'',
	@Extension nvarchar(10) = ''%.trn''
)
as
begin
	set xact_abort on
	set deadlock_priority high
	if object_id(''dbo.OmegaRestoreHistory'') is null
	begin
		create table dbo.OmegaRestoreHistory(DatabaseName sysname, FileName varchar(900) not null , Date datetime not null default(getdate()), primary key (DatabaseName, FileName) with(ignore_dup_key=on))
	end
	
	declare @FileName nvarchar(max), @SQL nvarchar(max), @StandbyName nvarchar(max)
	if right(@Path, 1) <>''\''
		select @Path = @Path + ''\''
	if right(@StandByPath, 1) <>''\''
		select @StandByPath = @StandByPath + ''\''
	declare @Files table (FileName nvarchar(1000), d int, f int)
	
	insert into @Files
		exec master.sys.xp_dirtree @Path, 0, 1

	declare c cursor local static for
		select FileName from @Files where FileName like @Extension and f = 1 order by FileName
	open c
	select @SQL = ''alter database ''+quotename(@DatabaseName)+'' set multi_user with rollback immediate''
	exec(@SQL)

	select @SQL = ''alter database ''+quotename(@DatabaseName)+'' set single_user with rollback immediate''
	exec(@SQL)
	fetch next from c into @FileName
	while @@fetch_status = 0
	begin
	
		select @StandbyName = @StandByPath + @FileName+''.standby'',
			@FileName = @Path + @FileName 
		print @FileName
		begin try
			--print ''Restore '' + @FileName
			restore log @DatabaseName from disk = @FileName with standby = @StandbyName
			insert into OmegaRestoreHistory(DatabaseName, FileName) values(@DatabaseName, @FileName)
		end try
		begin catch
	
		end catch
		fetch next from c into @FileName
	end
	close c
	deallocate c
	select @SQL = ''alter database ''+quotename(@DatabaseName)+'' set multi_user with rollback immediate''
	exec(@SQL)

	delete dbo.OmegaRestoreHistory 
	where DatabaseName = @DatabaseName
		and Date < (
					select min(Date)
					from(
							select top 100 Date 
							from dbo.OmegaRestoreHistory 
							where DatabaseName = @DatabaseName
							order by Date desc
						) a
				)
end

'
	exec maint.usp_CreateProxy
	
	-- create job
	begin transaction
	declare @JobID uniqueidentifier, @JobName sysname = 'Logshipping and Proxy Refresh - '  + quotename(db_name()), 
			@ScheduleUID uniqueidentifier, @ScheduleID int, @ProxyName sysname = db_name() + '_Proxy'
	if not exists (select name from msdb.dbo.syscategories where name=N'Omega Jobs' AND category_class=1)
		exec msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Omega Jobs'
	
	select @ScheduleUID = schedule_uid, @ScheduleID = schedule_id from msdb.dbo.sysschedules where name = 'Omega Jobs - Every hour'
	if @@rowcount = 0
		exec  msdb.dbo.sp_add_schedule @schedule_uid = @ScheduleUID output, @schedule_id = @ScheduleID output, @schedule_name=N'Omega Jobs - Every hour', @enabled=1, @freq_type=4, @freq_interval=1, @freq_subday_type=8, @freq_subday_interval=1, @freq_relative_interval=0, @freq_recurrence_factor=0, @active_start_date=20170720, @active_end_date=99991231, @active_start_time=0, @active_end_time=235959

	if not exists(select * from msdb.dbo.sysjobs where name = @JobName)
	begin
	
		exec msdb.dbo.sp_add_job @job_name=@JobName, @enabled=1, @notify_level_eventlog=0, @notify_level_email=0, @notify_level_netsend=0, @notify_level_page=0, @delete_level=0, @description=N'No description available.', @category_name=N'Omega Jobs', @owner_login_name=N'sa', @job_id = @JobID output
		select @SQL = 'ALTER LOGIN [admin_all] Disable'
		exec msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=N'ALTER LOGIN [admin_all] Disable', @step_id=1, @cmdexec_success_code=0, @on_success_action=3, @on_success_step_id=0, @on_fail_action=3, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=@SQL, @database_name=N'master', @flags=0
		select @SQL = 'exec msdb.dbo.ApplyLog @DatabaseName ='''+replace(db_name(), '''','''''')+''', @Path ='''+replace(@LogbackupPath, '''','''''')+''', @StandByPath ='''+replace(@StandByPath, '''','''''')+''', @Extension ='''+replace(@LogFileExtension, '''','''''')+''''
		exec msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=N'ApplyLog', @step_id=2, @cmdexec_success_code=0, @on_success_action=3, @on_success_step_id=0, @on_fail_action=3, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=@SQL, @database_name=N'master', @flags=0
		select @SQL = 'exec ' + quotename(db_name()) + '.maint.usp_CreateProxy'
		exec msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=N'Create proxy', @step_id=3, @cmdexec_success_code=0, @on_success_action=3, @on_success_step_id=0, @on_fail_action=3, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=@SQL, @database_name=N'master', @flags=0
		select @SQL = 'ALTER LOGIN [admin_all] Enable'
		exec msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=N'ALTER LOGIN [admin_all] Enable', @step_id=4, @cmdexec_success_code=0, @on_success_action=3, @on_success_step_id=0, @on_fail_action=3, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=@SQL, @database_name=N'master', @flags=0
		select @SQL = 'exec ' + quotename(db_name()) + '.maint.usp_CheckLogShippingStatus'
		exec msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=N'Check log-shipping status', @step_id=5, @cmdexec_success_code=0, @on_success_action=1, @on_success_step_id=0, @on_fail_action=2, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=@SQL, @database_name=N'master', @flags=0
		exec msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1
		exec msdb.dbo.sp_attach_schedule @job_id=@JobID,@schedule_id = @ScheduleID
		exec msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)'
	end
	commit

	
end

go
