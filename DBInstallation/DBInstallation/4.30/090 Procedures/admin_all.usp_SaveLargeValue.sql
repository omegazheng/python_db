set ansi_nulls on
go
set quoted_identifier on
go
create or alter procedure admin_all.usp_SaveLargeValue
(
	@Value varbinary(max),
	@ValueID uniqueidentifier output
)
as
begin
	set nocount on 
	select @ValueID = null
	if @Value is null
		return
	declare @len bigint = datalength(@Value)
	if @len < 16
	begin
		select @ValueID = @Value
		insert into admin_all.LargeValue(ValueID, IsCompressed, Data)
			values(@ValueID, 0, @Value)
		return
	end
	select @ValueID = hashbytes('MD5', @Value);
	if @len < 100
	begin
		insert into admin_all.LargeValue(ValueID, IsCompressed, Data)
			values(@ValueID, 0, @Value)
		return
	end
	insert into admin_all.LargeValue(ValueID, IsCompressed, Data)
		values(@ValueID, 1, compress(@Value))
end
go

--exec admin_all.usp_SaveLargeValue 0x1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890, null
--select *, datalength(data), datalength(case when IsCompressed = 1 then decompress(data) else data end) from admin_all.LargeValue order by 5