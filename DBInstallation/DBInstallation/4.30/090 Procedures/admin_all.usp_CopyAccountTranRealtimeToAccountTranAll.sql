set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_CopyAccountTranRealtimeToAccountTranAll] @WaitOrQuit bit = 1
as
begin
	set nocount on

	if exists(select * from sys.databases where database_id = db_id((select PARSENAME ( base_object_name , 3) from sys.synonyms where object_id = object_id('admin_all.Synonym_ACCOUNT_TRAN_ALL'))) and is_read_only = 1)
		return
	
	declare @SQL nvarchar(max)
	select a.name, a.column_id into #1
	from sys.columns a
		inner join sys.columns b on a.name = b.name
	where a.object_id = object_id('admin_all.ACCOUNT_TRAN_REALTIME')
		and b.object_id = object_id('admin_all.ACCOUNT_TRAN_ALL')
		and a.name not in ('ID')
	select @SQL = '
set xact_abort on
declare @MaxID bigint, @ret int, @ID bigint, @i int
while(1=1)
begin
	
	if not exists(select * from admin_all.ACCOUNT_TRAN_REALTIME)
		goto ___Exit___
	
	select @i = 0
	declare c cursor local fast_forward for
		select ID
		from admin_all.ACCOUNT_TRAN_REALTIME
		order by ID asc
	open c
	fetch next from c into @ID
	while @@fetch_status = 0
	begin
		select @i = @i + 1
		if @i >= 250
		begin
			select @i = 0
			while @@trancount >0
				commit
		end
		if @@trancount = 0
		begin
			begin transaction
		end
		' + case when @WaitOrQuit = 1 then 
				'exec @ret = sp_getapplock @Resource = ''usp_CopyAccountTranRealtimeToAccountTranAll'', @LockMode = ''Exclusive'', @LockOwner = ''Transaction'', @LockTimeout = 0; '
				else
				'exec @ret = sp_getapplock @Resource = ''usp_CopyAccountTranRealtimeToAccountTranAll'', @LockMode = ''Exclusive'', @LockOwner = ''Transaction'', @LockTimeout = -1; '
			end+
		'
		if @ret < 0
		begin
			goto ___Exit___
		end

		update t 
			set '+stuff((select ',t.'+quotename(name) +'=s.' +quotename(name) from #1 order by column_id for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '')+'
		from admin_all.ACCOUNT_TRAN_REALTIME s 
			inner loop join admin_all.ACCOUNT_TRAN_ALL t on t.ID =  s.ID
		where s.ID =@ID and t.ID = @ID
		if @@rowcount = 0
		begin
			set identity_insert admin_all.ACCOUNT_TRAN_ALL on 
			insert into admin_all.ACCOUNT_TRAN_ALL(ID'+(select ','+quotename(name) from #1 order by column_id for xml path(''), type).value('.', 'nvarchar(max)')+')
				select s.ID'+(select ',s.'+quotename(name) from #1 order by column_id for xml path(''), type).value('.', 'nvarchar(max)')+'
				from admin_all.ACCOUNT_TRAN_REALTIME s
				where s.ID = @ID
			set identity_insert admin_all.ACCOUNT_TRAN_ALL off
		end

		delete s
		from admin_all.ACCOUNT_TRAN_REALTIME s
		where s.ID = @ID
		fetch next from c into @ID
	end
	close c
	deallocate c

	while (@@trancount >0)
		commit

	waitfor delay ''00:00:01''
end
___Exit___:
	while (@@trancount >0)
		commit
	'
	exec(@SQL)
	return
end


go
