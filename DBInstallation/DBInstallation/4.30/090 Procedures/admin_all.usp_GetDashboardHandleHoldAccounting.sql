set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetDashboardHandleHoldAccounting]
(
	@staffid INT, 
	@brands NVARCHAR(2000), 
	@segments nvarchar(2000), 
	@period NVARCHAR(200),
	@AggregateType tinyint = 0
)
AS
  BEGIN
    set nocount on
    SET DATEFIRST 1
    -- get sourceForm
    declare @SourceFrom varchar(100)
	  select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
	  if @SourceFrom is null
	  begin
		  exec admin_all.usp_SetRegistry 'core.dashboard.source', 'dw'
		  select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
  	end

    DECLARE @endDate DATE = cast(dateadd(DAY, 1, getdate()) AS DATE)
    DECLARE @startDate DATE = cast(getdate() AS DATE)
    create table #Brands (
      BrandID int primary key
    )
    insert into #Brands (BrandID)
      SELECT distinct BRANDID
      FROM [admin_all].[STAFF_BRAND_TBL]
      WHERE STAFFID = @staffid
            AND BRANDID IN (select cast(Item as int) brandId
                            from admin_all.fc_splitDelimiterString(@brands, ','))
    create table #Segments (
      SegmentID int primary key
    )
    insert into #Segments (SegmentID)
      select cast(Item as int) segmentId
      from admin_all.fc_splitDelimiterString(@segments, ',')

    if @period = 'TODAY'
      begin
        set @EndDate = CAST(getdate() AS DATE)
        set @StartDate = CAST(getdate() AS DATE)
      end
    else if @period = 'YESTERDAY'
      begin
        SET @EndDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
        SET @StartDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
      end
    else if @period = 'WTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = CAST(DATEADD(DD, 1 - DATEPART(DW, GETDATE()), GETDATE()) AS DATE)
      end
    else if @period = 'MTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = cast(dateadd(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AS DATE)
      end
    else if @period = 'LTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
                SET @StartDate = cast(DATEADD(YEAR, -100, getdate()) AS DATE)
      end

	if @SourceFrom = 'dw'
    BEGIN
      SELECT
        @period                             AS PERIOD,
        CURRENCY                            AS CURRENCY,
        SUM(HANDLE_REAL)                    AS HANDLE_REAL,
        SUM(HANDLE_RELEASED_BONUS)          AS HANDLE_RELEASED_BONUS,
        SUM(HANDLE_PLAYABLE_BONUS)          AS HANDLE_PLAYABLE_BONUS,
        SUM(PNL_REAL)                       AS PNL_REAL,
        SUM(PNL_RELEASED_BONUS)             AS PNL_RELEASED_BONUS,
        SUM(PNL_PLAYABLE_BONUS)             AS PNL_PLAYABLE_BONUS,
        SUM(IN_GAME_RELEASED_BONUS_RELEASE) AS IN_GAME_RELEASED_BONUS_RELEASE,
        SUM(IN_GAME_PLAYABLE_BONUS_RELEASE) AS IN_GAME_PLAYABLE_BONUS_RELEASE
      FROM
        ADMIN_ALL.DW_GAME_PLAYER_DAILY dw
        left join admin_all.PLATFORM pl on dw.PLATFORM_ID = pl.ID
      WHERE
        dw.SUMMARY_DATE >= @startDate AND dw.SUMMARY_DATE <= @endDate
        AND dw.BRAND_ID IN (select x.BrandID from #Brands x)
        AND pl.segment_id in (select x.SegmentID from #Segments x)
      GROUP BY
        dw.CURRENCY
    END -- sourceForm = 'dw'
  ELSE
    BEGIN


      SELECT
        @period AS                            PERIOD,
        CURRENCY,
        SUM(ISNULL(dw.HandleReal, 0))           HANDLE_REAL,
        SUM(ISNULL(dw.HandleReleasedBonus, 0)) HANDLE_RELEASED_BONUS,
        SUM(ISNULL(dw.HandlePlayableBonus, 0)) HANDLE_PLAYABLE_BONUS,
        SUM(ISNULL(dw.PNLReal, 0))              PNL_REAL,
        SUM(ISNULL(dw.PNLReleasedBonus, 0))    PNL_RELEASED_BONUS,
        SUM(ISNULL(dw.PNLPlayableBonus, 0))    PNL_PLAYABLE_BONUS,
        SUM(ISNULL(dw.InGameReleasedBonusRelease, 0))    IN_GAME_RELEASED_BONUS_RELEASE,
        SUM(ISNULL(dw.InGamePlayableBonusRelease, 0)) IN_GAME_PLAYABLE_BONUS_RELEASE
      FROM
        admin_all.DW_ALL_DAYS d
        LEFT JOIN admin_all.fn_GetPlayerDailyAggregate(@AggregateType, @StartDate, dateadd(day, 1, @EndDate)) dw
        inner join  admin_all.platform p ON dw.ProductID = p.Id AND p.segment_id IN (SELECT x.SegmentID from #Segments x)
          ON dw.Date = d.DAY AND dw.BrandID IN (SELECT x.BRANDID from #Brands x)
      WHERE
        d.DAY >= @StartDate AND d.DAY <= @EndDate AND (CURRENCY is not null)
      GROUP BY
        CURRENCY

     END -- sourceFrom = 'ath'

  END

go
