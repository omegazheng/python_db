set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_ResetStaffPassword
		@StaffID int = null,
		@LoginName nvarchar(100) = null,
	@Password nvarchar(100)
as
begin
	SELECT @StaffID = StaffID FROM admin_all.STAFF_AUTH_TBL where LOGINNAME = @LoginName or STAFFID = @StaffID
	if @StaffID is null
		raiserror ('Staff not Found', 16,1)
	update admin_all.STAFF_AUTH_TBL set PASSWORD_V2 = admin_all.fn_getMD5(@Password +'_' + CONVERT(varchar(10), STAFFID)),
																			PASSWORD_EXPIRED = 0, LOGIN_ATTEMPTS = 0, LOCKED_STATUS = 'NOT_LOCKED', PASSWORD_SET_DATE = getdate()
	where STAFFID = @StaffID;
end

go
