set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[duplicateCommission]
    (@source_commission_id int, @agent_id int)
AS
begin
    declare @source_commission_structure_id int
        set @source_commission_structure_id = (select id from admin_all.commission_structure where commission_id = @source_commission_id)
    declare @source_commission_bonus_id int
        set @source_commission_bonus_id = (select id from admin_all.commission_bonus where commission_structure_id = @source_commission_structure_id)

    insert into admin_all.commission(
        name, description, scheme, state, category, network, start_date, end_date, created_date, created_by,
        last_modified_date, last_modified_by, template_id, is_template, brandid, is_autopay, threshold
    )
    select
        name, description, scheme, state, category, network, start_date, end_date, created_date, created_by,
        last_modified_date, last_modified_by, template_id, 0, brandid, is_autopay, threshold
    from admin_all.commission where id = @source_commission_id
    declare @new_commission_id int
        set @new_commission_id = (select IDENT_CURRENT('admin_all.commission'))

    insert into admin_all.user_commission (
        partyid, commission_id
    ) values (
        @agent_id, @new_commission_id
    )

    insert into admin_all.commission_platform (
        commission_id, platform_id
    ) select
        @new_commission_id, platform_id from admin_all.commission_platform where commission_id = @source_commission_id


    insert into admin_all.commission_structure (
        commission_id, period, has_bonus, has_range, has_group, has_parameter, provider_percentage, profit_percentage
    ) select
        @new_commission_id, period, has_bonus, has_range, has_group, has_parameter, provider_percentage, profit_percentage
      from admin_all.commission_structure where id = @source_commission_structure_id
    declare @new_commission_structure_id int
        set @new_commission_structure_id = (select IDENT_CURRENT('admin_all.commission_structure'))

    insert into admin_all.commission_group(
        commission_structure_id, grouping, percentage, has_exception, min_odds, user_percentage, operator
    ) select
        @new_commission_structure_id, grouping, percentage, has_exception, min_odds, user_percentage, operator
      from admin_all.commission_group where commission_structure_id = @source_commission_structure_id

    insert into admin_all.commission_parameter(
        commission_structure_id, type, value
    ) select
        @new_commission_structure_id, type, value
      from admin_all.commission_parameter where commission_structure_id = @source_commission_structure_id

    insert into admin_all.commission_bonus(
        commission_structure_id, type, period, has_range
    ) select
        @new_commission_structure_id, type, period, has_range
      from admin_all.commission_bonus where commission_structure_id = @source_commission_structure_id
    declare @new_bonus_id int
        set @new_bonus_id = (select IDENT_CURRENT('admin_all.commission_bonus'))

    insert into admin_all.commission_range(
        commission_bonus_id, commission_structure_id, start_amount, end_amount, percentage
    ) select
        @new_bonus_id, commission_structure_id, start_amount, end_amount, percentage
      from admin_all.commission_range where commission_bonus_id = @source_commission_bonus_id



    insert into admin_all.commission_range(
        commission_structure_id, commission_bonus_id, start_amount, end_amount, percentage
    ) select
        @new_commission_structure_id, commission_bonus_id, start_amount, end_amount, percentage
      from admin_all.commission_range where commission_structure_id = @source_commission_structure_id

    insert into admin_all.commission_level(
        commission_id, authority_name
    ) select
        @new_commission_id, authority_name
      from admin_all.commission_level where commission_id = @source_commission_id
end

go
