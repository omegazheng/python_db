set ansi_nulls, quoted_identifier on
go


create or alter procedure [admin_all].[usp_updateUserWebSessionEFT]
(@SessionKey NVARCHAR(255), @SessionState NVARCHAR(50), @UpdateTime datetime2 = null,  @MachineID int = null)
AS
BEGIN
  if @SessionKey is null return

  if @UpdateTime is null set @UpdateTime = getdate()
  begin

    if @MachineID is null
      begin
        update admin_all.user_web_session set SessionState = @SessionState, last_access_time = @UpdateTime
        where session_key = @SessionKey
      end

    else
      begin
        update admin_all.user_web_session set SessionState = @SessionState, last_access_time = @UpdateTime, MachineID = @MachineID
        where session_key = @SessionKey
      end
  end

  select session_key, partyID, MachineID, last_access_time, SessionType, SessionState from admin_all.user_web_session where session_key = @SessionKey
END

go
