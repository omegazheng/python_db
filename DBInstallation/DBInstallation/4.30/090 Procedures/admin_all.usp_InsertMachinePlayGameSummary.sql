set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_InsertMachinePlayGameSummary
  (
    @PartyID	int,
    @MachineID int,
    @GameID varchar(100),
    @AmountReal  numeric(38,18),
    @PlayableBonus numeric(38,18),
    @GamesPlayed	int,
    @TotalCreditWin numeric(38,18),
    @GameWin int,
    @AmountTransferIn numeric(38,18),
    @AmountTransferOut numeric(38,18),
    @BonusTransferIn numeric(38,18),
    @BonusTransferOut numeric(38,18),
    @LoyaltyPoint bigint,
    @TotalPromoAmount numeric(38,18),
    @TotalWinLossAmount numeric(38,18),
    @TicketInAmount numeric(38,18),
    @TicketOutAmount numeric(38,18),
    @TicketPromoInAmount numeric(38,18),
    @TicketPromoOutAmount numeric(38,18)
    )
  as
  begin
    set nocount on
    declare @ProductID int, @DateTime datetime = getdate()
    begin transaction
      insert into admin_all.MachinePlayGameSummary(PartyID, DateTime, MachineID, AmountReal, PlayableBonus, GamesPlayed, TotalCreditWin, GameWin, AmountTransferIn, AmountTransferOut, BonusTransferIn, BonusTransferOut, LoyaltyPoint, GameID, ProductID, GameInfoID, TotalPromoAmount, TotalWinLossAmount, TicketIn, TicketOut, TicketPromoIn, TicketPromoOut)
      select @PartyID, isnull(@DateTime, getdate()), @MachineID, isnull(@AmountReal,0), isnull(@PlayableBonus, 0), isnull(@GamesPlayed, 0), isnull(@TotalCreditWin, 0), isnull(@GameWin, 0), isnull(@AmountTransferIn, 0), isnull(@AmountTransferOut, 0), isnull(@BonusTransferIn, 0), isnull(@BonusTransferOut, 0), isnull(@LoyaltyPoint, 0), @GameID, m.ProductID, gi.ID, isnull(@TotalPromoAmount, 0), isnull(@TotalWinLossAmount, 0), isnull(@TicketInAmount, 0), isnull(@TicketOutAmount, 0), isnull(@TicketPromoInAmount, 0), isnull(@TicketPromoOutAmount, 0)
      from admin_all.Machine m
             left outer join admin_all.GAME_INFO gi on gi.PLATFORM_ID = m.ProductID and gi.GAME_ID = @GameID
      where m.MachineID = @MachineID
      if isnull(@LoyaltyPoint, 0) <> 0
        begin
          select @ProductID = ProductID
          from admin_all.Machine
          where MachineID = @MachineID
          if @@rowcount = 0
            begin
              raiserror('Could not find Machine', 16, 1)
              rollback
              return
            end
          exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = 0, @ReleasedBonus = 0,  @PlayableBonus = 0, @TranType = 'MACHIN_LOY',
               @PlatformID = @ProductID, @MachineID = @MachineID,
               @AmountRawLoyalty= @LoyaltyPoint
        end
    commit
  end


go
