set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.PrintString (@str varchar(max), @ReturnToDataset bit = 1)
as
begin
	set nocount on
	declare @line varchar(max), @StartLocation int, @Length int, @TotalLength int, @Current int
	select @StartLocation = 1, @TotalLength = datalength(@str), @Current = 1, @Length = 0
	declare @ret table (ID int identity(1,1) primary key, Line varchar(max))

	declare @PrintLine nvarchar(max) =
'declare @pos int = 1
while @pos <= len(@line)
begin
	print substring(@line, @pos, 8000)
	select @pos = @pos + 8000
end'
	while @Current <= @TotalLength
	begin
		if(substring(@str, @Current, 2) in( char(0x0d) + char(0x0a), char(0x0a) + char(0x0d)))
		begin
			if @Length <= 0
				insert into @ret(Line) values('')
			else
			begin -- line
				select @line = substring(@str, @StartLocation, @Length)
				insert into @ret(Line) values(@line)
				--exec sp_executesql @PrintLine, N'@Line varchar(max)' , @line
			end
			select @StartLocation = @Current + 2, @Current = @Current + 2, @Length = 0
			continue;
		end
		else if (substring(@str, @Current, 1) in(char(0x0d) , char(0x0a)))
		begin
			if @Length <= 0
				insert into @ret(Line) values('')
			else
			begin
				select @line = substring(@str, @StartLocation, @Length)
				--exec sp_executesql @PrintLine, N'@Line varchar(max)' , @line
				insert into @ret(Line) values(@line)
			end
			select @StartLocation = @Current + 1, @Current = @Current + 1, @Length = 0
			continue;
		end
		select @Current = @Current + 1, @Length = @Length + 1
	end
	if(@StartLocation <= datalength(@str))
	begin
		insert into @ret(Line) values(substring(@str, @StartLocation, datalength(@str)))
	end
	if @ReturnToDataset = 1
	begin
		select Line
		from @ret
		order by ID asc
		return
	end
	declare c cursor local for
		select line
		from @ret
		order by ID asc
	open c
	fetch next from c into @line
	while @@fetch_status = 0
	begin
		exec sp_executesql @PrintLine, N'@Line varchar(max)' , @line
		fetch next from c into @line
	end
	close c
	deallocate c
end

go
