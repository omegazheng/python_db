set ansi_nulls, quoted_identifier on
go

-- if freePlay_plan_id != null, delete by planId
-- id id != null, just delete that FreePlayPlanGameInfo
create or alter procedure [admin_all].[usp_DeleteFreePlayPlanGameInfo]
  (
    @ID INT,
    @FREEPLAY_PLAN_ID INT
  )
as
  BEGIN
    set nocount on
    --   begin transaction

    IF @ID IS NOT NULL
      BEGIN
        delete from admin_all.FREEPLAY_PLAN_GAMEINFO where ID = @ID
      END
    ELSE
      IF @FREEPLAY_PLAN_ID IS NOT NULL
        BEGIN
          delete from admin_all.FREEPLAY_PLAN_GAMEINFO where FREEPLAY_PLAN_ID = @FREEPLAY_PLAN_ID
        END

    --   commit

  END

go
