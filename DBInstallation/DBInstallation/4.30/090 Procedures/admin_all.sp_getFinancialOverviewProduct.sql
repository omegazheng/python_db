set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getFinancialOverviewProduct]
(@startDate DATETIME, @endDate DATETIME)
AS
BEGIN
  DECLARE @startDateLocal DATETIME
  DECLARE @endDateLocal DATETIME
  SET @startDateLocal = @startDate
  SET @endDateLocal = @endDate
  SELECT
    tranTypeCurrency.PLATFORM_TYPE,
    tranTypeCurrency.TRAN_TYPE,
    tranTypeCurrency.CURRENCY,
    isnull(summary.AMOUNT_REAL, 0) AMOUNT_REAL,
    isnull(summary.AMOUNT_PLAYABLE_BONUS, 0) AMOUNT_PLAYABLE_BONUS,
    isnull(summary.AMOUNT_RELEASED_BONUS, 0) AMOUNT_RELEASED_BONUS
  FROM
    (
      SELECT
        platformTypes.PLATFORM_TYPE,
        tranTypes.TRAN_TYPE,
        ISO_CODE as CURRENCY
      FROM
        (
          SELECT 'GAME_BET' AS TRAN_TYPE
          UNION SELECT 'GAME_WIN' AS TRAN_TYPE
          UNION SELECT 'GAME_WIN_IN_GAME_RELEASE' AS TRAN_TYPE
          UNION SELECT 'BONUS_REL' AS TRAN_TYPE
          UNION SELECT 'BONUS_WITHDRAWAL' AS TRAN_TYPE
        ) tranTypes, CURRENCY,
        (
          SELECT DISTINCT PLATFORM_TYPE FROM PLATFORM
        ) platformTypes
    ) tranTypeCurrency

    left join
    (
      SELECT
        ISNULL(PLATFORM.PLATFORM_TYPE, 'CASINO') PLATFORM_TYPE,
        ACCOUNT_TRAN.TRAN_TYPE,
        USER_CONF.CURRENCY,
        SUM(AMOUNT_REAL) AMOUNT_REAL,
        SUM(AMOUNT_PLAYABLE_BONUS) AMOUNT_PLAYABLE_BONUS,
        SUM(AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
      FROM ACCOUNT_TRAN WITH(NOLOCK)
        JOIN ACCOUNT ON ACCOUNT_TRAN.ACCOUNT_ID = ACCOUNT.id
        JOIN EXTERNAL_MPT.USER_CONF ON ACCOUNT.PARTYID = USER_CONF.PARTYID
        LEFT JOIN PLATFORM ON ACCOUNT_TRAN.PLATFORM_ID = PLATFORM.ID
      WHERE DATETIME >= @startDateLocal AND ACCOUNT_TRAN.DATETIME < @endDateLocal
            AND ACCOUNT_TRAN.TRAN_TYPE IN ('GAME_BET', 'GAME_WIN', 'BONUS_REL')
            AND ACCOUNT_TRAN.ROLLED_BACK = 0
      GROUP BY
        ACCOUNT_TRAN.TRAN_TYPE,
        USER_CONF.CURRENCY,
        PLATFORM.PLATFORM_TYPE

      UNION

      SELECT
        PLATFORM.PLATFORM_TYPE,
        'GAME_WIN_IN_GAME_RELEASE' AS TRAN_TYPE,
        USER_CONF.CURRENCY,
        0 AMOUNT_REAL,
        0 AMOUNT_PLAYABLE_BONUS,
        SUM(
            CASE WHEN (tran_type = 'BONUS_REL' and platform_id IS NOT null and game_id is NOT null) THEN AMOUNT_RELEASED_BONUS * -1
            ELSE 0
            END
        ) AMOUNT_RELEASED_BONUS
      FROM ACCOUNT_TRAN WITH(NOLOCK)
        JOIN ACCOUNT ON ACCOUNT_TRAN.ACCOUNT_ID = ACCOUNT.id
        JOIN EXTERNAL_MPT.USER_CONF ON ACCOUNT.PARTYID = USER_CONF.PARTYID
        JOIN PLATFORM ON ACCOUNT_TRAN.PLATFORM_ID = PLATFORM.ID
      WHERE DATETIME >= @startDateLocal AND ACCOUNT_TRAN.DATETIME < @endDateLocal
            AND ACCOUNT_TRAN.TRAN_TYPE IN ('BONUS_REL')
            AND ACCOUNT_TRAN.ROLLED_BACK = 0
      GROUP BY
        ACCOUNT_TRAN.TRAN_TYPE,
        PLATFORM.PLATFORM_TYPE,
        USER_CONF.CURRENCY

      UNION

      SELECT
        'BINGO' as PLATFORM_TYPE,
        'BONUS_WITHDRAWAL' AS TRAN_TYPE,
        users.CURRENCY,
        0 AMOUNT_REAL,
        0 AMOUNT_PLAYABLE_BONUS,

        sum(WITHDRAWN_AMOUNT) as IN_GAME_RELEASE_WITHDRAWAL from BONUS_TRAN
        join bonus on bonus.id = bonus_tran.BONUS_ID
        join BONUS_PLAN on bonus.BONUS_PLAN_ID = bonus_plan.id
        join external_mpt.user_conf users on users.partyId = bonus.PARTYID
      where SUMMARY_DATE >= @startDateLocal and SUMMARY_DATE < @endDateLocal and BONUS_PLAN.IS_PLAYABLE_WINNINGS_RELEASED = 1
      group by users.CURRENCY

      UNION

      SELECT
        'CASINO' as PLATFORM_TYPE,
        'BONUS_WITHDRAWAL' AS TRAN_TYPE,
        users.CURRENCY,
        0 AMOUNT_REAL,
        0 AMOUNT_PLAYABLE_BONUS,
        sum(WITHDRAWN_AMOUNT) as IN_GAME_RELEASE_WITHDRAWAL from BONUS_TRAN
        join bonus on bonus.id = bonus_tran.BONUS_ID
        join BONUS_PLAN on bonus.BONUS_PLAN_ID = bonus_plan.id
        join external_mpt.user_conf users on users.partyId = bonus.PARTYID
      where SUMMARY_DATE >= @startDateLocal and SUMMARY_DATE < @endDateLocal and BONUS_PLAN.IS_PLAYABLE_WINNINGS_RELEASED = 0
      group by users.CURRENCY
    )
    summary on summary.TRAN_TYPE = tranTypeCurrency.TRAN_TYPE and tranTypeCurrency.CURRENCY = summary.CURRENCY
               and summary.PLATFORM_TYPE = tranTypeCurrency.PLATFORM_TYPE
  ORDER BY tranTypeCurrency.PLATFORM_TYPE, tranTypeCurrency.CURRENCY, tranTypeCurrency.TRAN_TYPE;
END

go
