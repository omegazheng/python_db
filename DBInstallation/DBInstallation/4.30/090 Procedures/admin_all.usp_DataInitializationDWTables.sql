set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationDWTables
as
begin
	set nocount on
	begin transaction
	;with x0 as
	(
		select cast('1990-01-01' as date) as Day
		union all
		select Dateadd(day, 1, Day)
		from x0
		where Day < '2030-12-31'
	)
	insert into admin_all.DW_ALL_DAYS(DAY)
		select Day 
		from x0
		where not exists(select * from admin_all.DW_ALL_DAYS d where d.DAY = x0.Day)
		option (maxrecursion 0)

	;with x0 as
	(
		select cast('1990-01-01' as date) as Month
		union all
		select Dateadd(Month, 1, Month)
		from x0
		where Month < '2030-12-31'
	)
	insert into admin_all.DW_ALL_MONTHS(MONTH)
		select Month 
		from x0
		where not exists(select * from admin_all.DW_ALL_MONTHS d where d.MONTH = x0.MONTH)
		option (maxrecursion 0)

	;with x0 as
	(
		select cast('1990-01-01' as date) as Year
		union all
		select Dateadd(Year, 1, Year)
		from x0
		where Year < '2030-12-31'
	)
	insert into admin_all.DW_ALL_YEARS(YEAR)
		select Year 
		from x0
		where not exists(select * from admin_all.DW_ALL_YEARS d where d.YEAR = x0.Year)
		option (maxrecursion 0)

	commit
end
go
