set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetDataFeedSummary
(
	@BrandID bigint,
	@DateFrom datetime,
	@DateTo datetime = null
)
as
begin
	set nocount, xact_abort  on
	select Currency as currency,
		sum(case when TranType = 'GAME_BET' or RollbackTranType = 'GAME_BET'  Then (AmountReal + AmountReleasedBonus + AmountPlayableBonus) * -1 else 0 end) handle,
		sum(case when TranType in ('GAME_WIN', 'GAME_BET', 'PLTFRM_BON') or RollbackTranType in ('GAME_WIN', 'GAME_BET', 'PLTFRM_BON') then (AmountReal + AmountReleasedBonus + AmountPlayableBonus) * -1 else 0 end) hold,
		sum(case when RollbackTranID is not null then (AmountReal + AmountReleasedBonus + AmountPlayableBonus) * -1 else 0 end) rollbacks,
		count(distinct GameTranID) number_of_games,
		sum(case when TranType = 'GAME_BET' or RollbackTranType = 'GAME_BET' and RollbackTranID is null then 1 else 0 end) number_of_bets
	from  admin_all.DataFeed
	where BrandID = @BrandID
		and (
				TranType in ('GAME_WIN', 'GAME_BET', 'PLTFRM_BON')
				or
				RollbackTranType in ('GAME_WIN', 'GAME_BET', 'PLTFRM_BON')
			)
		and Datetime >= @DateFrom
		AND Datetime < @DateTo
	group by Currency
	order by Currency
end
go
