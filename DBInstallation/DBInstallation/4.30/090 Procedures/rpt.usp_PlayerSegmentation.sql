set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_PlayerSegmentation
(
	@StartDate date,--= '2018-08-01', 
	@EndDate date,--= '2018-08-08'
	@Brands varchar(8000) = null,
	@PartyID int = null

)
as
begin
	set nocount on
	declare @SQL nvarchar(max)
	if object_id('tempdb..#BrandIDs') is not null
		drop table #BrandIDs
	create table #BrandIDs (BrandID int primary key )
	if @PartyID is not null
		select @Brands = null

	if @Brands is not null
	begin
		insert into #BrandIDs 
		select brandid 
		from admin.CASINO_BRAND_DEF 
		where brandid in (
							select cast(v as int)
							from (
									select rtrim(ltrim(Item)) v
									from admin_all.fc_splitDelimiterString(@Brands, ',')
									where isnumeric(rtrim(ltrim(Item))) = 1
								) x
						)
		
	end
	
	select @SQL = ' 

	;WITH CTE AS
	(
		SELECT	party_id, GAME_ID, ROW_NUMBER() OVER (PARTITION BY party_id ORDER BY SUM(GAME_COUNT) desc)  AS RowNo
		FROM admin_all.DW_GAME_PLAYER_DAILY
		where summary_date between @StartDate and @EndDate
		'+case when @PartyID is not null then ' and party_id = @PartyID' else '' end+'
		group by party_id, game_id
	), PreTopGame as
	(
		select	Party_ID, 
				max(case when RowNo = 1 then g.NAME end) TopGame1,
				max(case when RowNo = 2 then g.NAME end) TopGame2,
				max(case when RowNo = 3 then g.NAME end) TopGame3,
				max(case when RowNo = 4 then g.NAME end) TopGame4
		from CTE
			left join admin_all.GAME_INFO g on CTE.GAME_ID = g.game_id
		where RowNo <= 4
		group by PARTY_ID
	), TopGame as
	(
		select
				u.partyid as PARTYID, u.USERID as userid, u.FIRST_NAME, u.LAST_NAME, u.EMAIL,u.REG_DATE,u.currency as currency,
				t.TopGame1, t.TopGame2, t.TopGame3, t.TopGame4
		from external_mpt.user_conf u
			left outer join PreTopGame t on t.Party_ID = u.PARTYID

	),
	Bonus as
	(
		SELECT	PARTYID, count(*) TRIGGERED_COUNT, 
				isnull(count(case when STATUS = ''RELEASED'' then 1 end), 0) RELEASED_COUNT, 
				isnull(sum(case when STATUS = ''RELEASED'' then isnull(Bonus.RELEASED_BONUS,0) + isnull(Bonus.RELEASED_BONUS_WINNINGS,0) end), 0) RELEASED_AMOUNT
		from admin_all.BONUS
		where TRIGGER_DATE between @StartDate and @EndDate
		'+case when @PartyID is not null then ' and PARTYID = @PartyID' else '' end+'
		group by PARTYID
	),
	GamePartyMonthly as
	(
		select	d.PARTY_ID PartyID, MONTH, SUM(handle) as Handle, MAX(summary_date) as LastPlayed,
				ROW_NUMBER() OVER (PARTITION BY d.Party_ID ORDER BY SUM(handle) desc) AS RowNo,
				sum(case when SUMMARY_DATE between @StartDate and @EndDate then handle end) TOTAL_HANDLE_PERIOD
		FROM (
					select  party_id, summary_date, sum(handle) handle,  month
					from admin_all.dw_game_player_daily 
					'+case when @PartyID is not null then ' where party_id = @PartyID' else '' end+'
					group by party_id, summary_date, month
			) d--admin_all.DW_GAME_CUST_DAILY
			inner join external_mpt.user_conf on d.PARTY_ID = external_mpt.user_conf.PARTYID
			'+case when @Brands is null then '' else 'inner join #BrandIDs b on b.BrandID = USER_CONF.BRANDID' end +'
		group by PARTY_ID, MONTH
	),
	GameParty as 
	(
		select PartyID, SUM(Handle)/(DATEDIFF(month, min(month),max(month))+1) as AverageMonthLTD, max(LastPlayed) as LastPlayed,
				max(case when RowNo = 1 then MONTH end) Month,max(case when RowNo = 1 then Handle end) PeakMonthhandle,
				sum(TOTAL_HANDLE_PERIOD) TOTAL_HANDLE_PERIOD, sum(Handle) TOTAL_HANDLE_LTD
		from GamePartyMonthly
		group by PartyID
	)
	select	topgame.partyid, topgame.first_name, topgame.last_name,
			topgame.email, topgame.reg_date,topgame.currency,
			GameParty.TOTAL_HANDLE_LTD, GameParty.TOTAL_HANDLE_PERIOD,
			CONVERT(VARCHAR(10),year(GameParty.month))+''-''+CONVERT(VARCHAR(10),month(GameParty.month)) as peakmonth,
			GameParty.PeakMonthhandle, GameParty.AverageMonthLTD,
			GameParty.LastPlayed, bonus.TRIGGERED_COUNT,bonus.RELEASED_COUNT,
			bonus.RELEASED_AMOUNT, topgame.topgame1,topgame.topgame2,
			topgame.topgame3,topgame.topgame4
	from GameParty
		left join TopGame on GameParty.partyid = topgame.partyid
		left join bonus on GameParty.partyid = bonus.partyid'

	exec sp_executesql @SQL, N'@PartyID int, @StartDate date, @EndDate date', @PartyID, @StartDate, @EndDate
end

go
