set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPaymentMethodsReport]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

SELECT
  CONV_DATA.METHOD,
  SUM(CASE WHEN (CONV_DATA.TYPE = 'DEPOSIT')
        THEN CONV_DATA.CONV_AMOUNT
        ELSE 0
       END) AS DEPOSIT,
  SUM(CASE WHEN (CONV_DATA.TYPE = 'WITHDRAWAL')
    THEN CONV_DATA.CONV_AMOUNT
      ELSE 0
      END) AS WITHDRAWAL
  FROM
  (
    SELECT P.METHOD, P.TYPE,
      admin_all.fn_GetCurrencyConversionRate(P.PROCESS_DATE, U.CURRENCY, 'EUR') * p.AMOUNT AS CONV_AMOUNT
    FROM
      [admin_all].[PAYMENT] p
      JOIN [admin_all].[ACCOUNT] a ON p.ACCOUNT_ID = a.id
      JOIN [external_mpt].[USER_CONF] u ON a.PARTYID = u.PARTYID
    WHERE p.PROCESS_DATE >= @startDateLocal
          AND p.PROCESS_DATE < @endDateLocal
          AND p.STATUS = 'COMPLETED'
    ) CONV_DATA
    GROUP BY CONV_DATA.METHOD

   END

go
