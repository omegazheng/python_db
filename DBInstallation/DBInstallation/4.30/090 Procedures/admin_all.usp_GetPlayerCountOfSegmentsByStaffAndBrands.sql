set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetPlayerCountOfSegmentsByStaffAndBrands]
(
	@staffid int, 
	@brands nvarchar(2000), 
	@segments nvarchar(2000), 
	@start_date datetime
)
as
begin
	set nocount on 
	create table #Brands(BrandID int primary key)
	insert into #Brands(BrandID)
		select distinct b.BRANDID
		from admin_all.fc_splitDelimiterString(@brands,',') a
			inner join admin_all.staff_brand_tbl b on cast(a.Item as int) = b.BRANDID
		

	create table #Segments(SegmentID int primary key)
	insert into #Segments(SegmentID)
		select distinct cast(Item as int)
		from admin_all.fc_splitDelimiterString(@segments,',')

    select count(s.PARTYID) playerCount, isnull(sg.id, 0) segmentId
    from external_mpt.USER_CONF u
		inner join admin_all.user_session s on u.partyid = s.partyid
		left join admin_all.game_info g on s.game_info_id = g.id
		left join admin_all.platform p on p.id = g.platform_id
		left join admin_all.segment sg on p.segment_id = sg.id and sg.id in (select SegmentID from #Segments) 
	where s.last_game_activity >= @start_date
		--and (sg.id in (select SegmentID from #Segments) or sg.id is null)
		and u.brandid in (select BrandID from #Brands)
	group by isnull(sg.id, 0)
end


go
