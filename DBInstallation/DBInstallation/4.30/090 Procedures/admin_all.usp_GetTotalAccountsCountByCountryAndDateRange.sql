set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetTotalAccountsCountByCountryAndDateRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    select
      count(PARTYID) as counts,
      country
    from external_mpt.USER_CONF
    group by country
    order by counts desc
  END

go
