set ansi_nulls, quoted_identifier on
go


create or alter procedure admin_all.usp_DeleteUserPlatformOldRecord
(
    @PartyID int,
    @PlatformID int,
    @MaxSavedRecordNum int
)
as

begin
    declare @TotalRecords int
    declare @MinAllowedID int
    select @TotalRecords = count(*) from admin_all.USER_PLATFORM where PARTYID = @PartyID and PLATFORM_ID = @PlatformID;
    if @TotalRecords > @MaxSavedRecordNum
        begin
            select @MinAllowedID = ID from
              (
                select ROW_NUMBER() OVER(order by ID desc) as rank, ID from admin_all.USER_PLATFORM
                where PARTYID = @PartyID and PLATFORM_ID = @PlatformID
              ) result
            where rank = @MaxSavedRecordNum;
            delete from admin_all.USER_PLATFORM where ID < @MinAllowedID and PARTYID = @PartyID and PLATFORM_ID = @PlatformID;
        end
end

go
