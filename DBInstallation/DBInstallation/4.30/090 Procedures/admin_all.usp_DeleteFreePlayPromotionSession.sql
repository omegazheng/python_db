set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_DeleteFreePlayPromotionSession]
  (
    @ID INT,
    @SESSION NVARCHAR(255)
  )
as
  BEGIN
    set nocount on
    --   begin transaction

    IF @ID IS NOT NULL
      BEGIN
        delete from admin_all.FREEPLAY_PROMOTION_SESSION where ID = @ID
      END
    ELSE
      IF @SESSION IS NOT NULL
        BEGIN
          delete from admin_all.FREEPLAY_PROMOTION_SESSION where SESSION = @SESSION
        END

    --   commit

  END

go
