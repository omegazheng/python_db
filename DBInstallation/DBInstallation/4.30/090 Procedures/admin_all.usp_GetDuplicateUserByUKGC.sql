set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetDuplicateUserByUKGC]
  (@BirthDate DATETIME, @Email NVARCHAR(MAX), @FirstName NVARCHAR(MAX) = NULL, @LastName NVARCHAR(MAX) = NULL, @PostalCode NVARCHAR(MAX) = NULL, @BrandId INT = NULL)
AS
  BEGIN

    -- firstName, lastName and postalCode are optional fields

    IF @BrandId IS NULL
      BEGIN
        SELECT PARTYID as partyId, FIRST_NAME as firstName, LAST_NAME as lastName,
               BIRTHDATE as birthDate, POSTAL_CODE as postalCode, EMAIL as email,
               BRANDID as brandId, COUNTRY as country
        FROM external_mpt.USER_CONF
        WHERE EMAIL = @Email
              OR BIRTHDATE = @BirthDate ;
      END

    ELSE
      BEGIN
        SELECT *
        FROM external_mpt.USER_CONF
        WHERE BRANDID = @BrandId
              AND (EMAIL = @Email  OR BIRTHDATE = @BirthDate);


      END

  END

go
