set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[closeAgentIndirectNgrsOfImmediateSubsagents]
  (@agent_id int, @date DATETIME)
AS
  BEGIN
    delete from agency.network_ggr where agent_id = @agent_id and date = @date

    insert into agency.network_ggr(agent_id, product_id, value, date, wins, bets, released_bonuses)
        select @agent_id, product_id, sum(value), @date, sum(wins), sum(bets), sum(released_bonuses)
        from (
            select product_id, value, wins, bets, released_bonuses
            from agency.direct_ggr
                join external_mpt.user_conf as u on u.parentid=@agent_id
            where agency.direct_ggr.agent_id = u.partyid
                and agency.direct_ggr.date = @date

            union all

            select product_id, value, wins, bets, released_bonuses
            from agency.network_ggr
                join external_mpt.user_conf as u on u.parentid=@agent_id
            where agency.network_ggr.agent_id = u.partyid
                and agency.network_ggr.date = @date
        ) x
        group by product_id
  END

go
