set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CreateDatabase @TargetDatabaseName sysname, @SourceDatabaseName sysname = null, @BackupLocation nvarchar(1000) = null
as
begin
	set nocount, xact_abort on
	
	if db_id(@TargetDatabaseName) is not null
	begin
		raiserror('Target database %s already exists', 16, 1, @TargetDatabaseName)
		return
	end
	if exists(select * from sys.configurations where name = 'show advanced options' and value_in_use = 0)
	begin
		exec sp_configure 'show advanced options', 1;
		reconfigure with override;
	end
	
	if exists(select * from sys.configurations where name = 'xp_cmdshell' and value_in_use = 0)
	begin
		exec sp_configure 'xp_cmdshell', 1;
		reconfigure with override;
	end
	if @BackupLocation is null
	begin
		exec master.sys.xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory',  @BackupLocation OUTPUT,   'no_output' 
	end
	select @BackupLocation = rtrim(ltrim(@BackupLocation))
	if right(@BackupLocation, 1) <> '\'
		select @BackupLocation = @BackupLocation + '\'

	
	declare @SQL nvarchar(max), @cmd varchar(8000), @FullBackupFileName nvarchar(1000), @Proc nvarchar(2000)
	select @SourceDatabaseName = isnull(@SourceDatabaseName, db_name())
	select @FullBackupFileName = @BackupLocation + @SourceDatabaseName+convert(nvarchar(100), getdate(), 112)+'.bak'
	select @Proc = 	quotename(@SourceDatabaseName) +'..sp_executesql'

	
	backup database @SourceDatabaseName to disk = @FullBackupFileName with init, compression, copy_only;
	
	
	declare @t table(name sysname, physical_name nvarchar(1000), Type sysname, i int identity(1,1))
	insert into @t
		exec @Proc N'select name, physical_name, type_desc from sys.database_files'
	
	select @SQL = 'restore database '+quotename(@TargetDatabaseName)+' from disk='''+ @FullBackupFileName + ''' with file = 1' +
			(select  ', move ''' + name + ''' to ''' + physical_name +'_'+@TargetDatabaseName+'''' from @t for xml path(''), type).value('.', 'nvarchar(max)')
			print @sql
	exec(@sql)
	
	
	select @cmd = 'del "'+@FullBackupFileName+'"'
	exec master..xp_cmdshell @cmd, no_output
end

go
