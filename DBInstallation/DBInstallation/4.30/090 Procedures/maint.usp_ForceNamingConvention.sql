set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ForceNamingConvention @TableName sysname
as
begin
	set nocount on 
	set xact_abort off
	declare @SQL nvarchar(max)
	;with x0 as
	(
		-- index
		select cast('Index' as varchar(50)) ObjectType, quotename(object_schema_name(i.object_id))+'.'+quotename(object_name(i.object_id))+'.'+quotename(i.name) ObjectName, i.name OriginalName, 
				('IDX_'+isnull(xi.Secondary_Type_desc +'_', '')+object_schema_name(i.object_id)+'_'+object_name(i.object_id)+(select '_' + col_name(ic.object_id,ic.column_id) from sys.index_columns ic where ic.object_id = i.object_id and ic.index_id = i.index_id and ic.is_included_column = 0 order by ic.key_ordinal for xml path(''))) collate database_default NewName,
				i.object_id as ObjectID
		from sys.indexes i
			left outer join (select 'XML_'+isnull(Secondary_Type_desc, 'PRIMARY') Secondary_Type_desc, object_id, index_id from sys.xml_indexes )xi on xi.object_id = i.object_id and xi.index_id = i.index_id
		where not exists(select * from sys.key_constraints kc where kc.parent_object_id = i.object_id and kc.unique_index_id = i.index_id)
			and i.name is not null
			and i.object_id = object_id(@TableName)
		--PK and UQ
		union
		select cast('Object' as varchar(50)) ObjectType, quotename(object_schema_name(i.object_id))+'.'+quotename(kc.name) ObjectName, i.name OriginalName, 
			case 
				when kc.type = 'PK' then 'PK_'+object_schema_name(i.object_id)+'_'+object_name(i.object_id)
				when kc.type = 'UQ' then ('UQ_'+object_schema_name(i.object_id)+'_'+object_name(i.object_id)+(select '_' + col_name(ic.object_id,ic.column_id) from sys.index_columns ic where ic.object_id = i.object_id and ic.index_id = i.index_id order by ic.key_ordinal for xml path(''))) 
			end NewName,
			kc.object_id
		from sys.key_constraints kc
			inner join sys.indexes i on i.object_id = kc.parent_object_id and i.index_id = kc.unique_index_id
		where i.name is not null
			and i.object_id = object_id(@TableName)
		--Default
		union 
		select cast('Object' as varchar(50)) ObjectType, quotename(object_schema_name(dc.object_id))+'.'+quotename(dc.name) ObjectName, dc.name OriginalName, 
				'DF_'+object_schema_name(c.object_id)+'_'+object_name(c.object_id) + '_' + c.name NewName,
				dc.object_id
		from sys.default_constraints dc
			inner join sys.columns c on dc.parent_object_id = c.object_id and dc.parent_column_id = c.column_id
		where dc.parent_object_id  = object_id(@TableName)
		--FK
		union 
		select cast('Object' as varchar(50)) ObjectType, quotename(object_schema_name(fk.parent_object_id))+'.'+quotename(fk.name) ObjectName, fk.name OriginalName, 
				'FK_'+object_schema_name(fk.parent_object_id)+'_'+object_name(fk.parent_object_id) +  
				+ case when object_schema_name(fk.parent_object_id) = object_schema_name(fk.referenced_object_id) then '' else '_' + object_name(fk.referenced_object_id) end
				+ '_' + object_name(fk.referenced_object_id)
				+(select '_'+col_name(fkc.parent_object_id, fkc.parent_column_id) from sys.foreign_key_columns fkc where fkc.constraint_object_id = fk.object_id for xml path('')),
				fk.object_id
		from sys.foreign_keys fk
		where fk.parent_object_id  = object_id(@TableName)
	)
	--select * from x0 where OriginalName<>NewName
	select @SQL = (
					select 'exec sp_rename ' + quotename(ObjectName, '''')+','+quotename(NewName , '''') + ',' + quotename(ObjectType, '''') +';
	'
					from x0 
					where OriginalName<>NewName
						and not exists(select * from sys.all_objects ao where ao.is_ms_shipped = 1 and ao.object_id = x0.ObjectID)
					for xml path(''), type
				).value('.', 'nvarchar(max)')
--print @SQL
	exec(@SQL)
end
go
