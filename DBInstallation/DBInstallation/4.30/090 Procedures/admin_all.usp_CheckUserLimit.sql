set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CheckUserLimit
(
	@PartyID int,
	@TransactionType varchar(10),
	@Datetime datetime,
	@AmountReal numeric(38, 18)
)
as
begin
	set nocount on
	set datefirst 1
	if @TransactionType not in ('GAME_BET', 'GAME_WIN' )
		return
	select @AmountReal = isnull(@AmountReal, 0)
	if @AmountReal = 0
		return;
	declare	@CurrentMonday date, @NextMonday date,  @WagerLimite numeric(38,18), 
			@LossLimit numeric(38,18), @CurrentWager numeric(38,18), 
			@CurrentLoss numeric(38,18), @Currency nchar(3)
	if admin_all.fn_IsMultiCurrencyEnabled() = 1
	begin
		select top 1 @Currency = u.CURRENCY, @PartyID = aa.PartyID
		from external_mpt.UserAssociatedAccount aa
			inner join external_mpt.USER_CONF u on u.PARTYID = aa.PartyID
		where aa.AssociatedPartyID = @PartyID
	end
	
	select @CurrentMonday = dateadd(day, @@datefirst, dateadd(day, -datepart(weekday, @Datetime), @Datetime))
	select @NextMonday = dateadd(day, 7, @CurrentMonday)
	select 
			@CurrentWager = case when LIMIT_TYPE = 'WAGER_LIMIT' then CURRENT_AMOUNT else @CurrentWager end,  
			@CurrentLoss = case when LIMIT_TYPE = 'LOSS_LIMIT' then CURRENT_AMOUNT else @CurrentLoss end,
			@WagerLimite = case when LIMIT_TYPE = 'WAGER_LIMIT' then AMOUNT_LIMIT else @WagerLimite end,  
			@LossLimit = case when LIMIT_TYPE = 'LOSS_LIMIT' then AMOUNT_LIMIT else @LossLimit end
	from admin_all.USER_PERIOD_LIMIT with(rowlock, xlock)
	where PARTYID = @PartyID
		and PERIOD_START = @CurrentMonday
	if @@rowcount = 0
	begin
		select 
			@WagerLimite = case when LIMIT_TYPE = 'WAGER_LIMIT' then AMOUNT else @WagerLimite end, 
			@LossLimit = case when LIMIT_TYPE = 'LOSS_LIMIT' then AMOUNT else @LossLimit end
		from admin_all.USER_LIMIT
		where PARTYID = @PartyID
		if @@rowcount = 0
			return;
		if @WagerLimite is not null
		begin
			insert into admin_all.USER_PERIOD_LIMIT(PARTYID, LIMIT_TYPE, PERIOD_START, PERIOD_END, AMOUNT_LIMIT, CURRENT_AMOUNT)
				values(@PartyID, 'WAGER_LIMIT', @CurrentMonday, @NextMonday, @WagerLimite, 0)
			select @CurrentWager = 0
		end
		if @LossLimit is not null
		begin
			insert into admin_all.USER_PERIOD_LIMIT(PARTYID, LIMIT_TYPE, PERIOD_START, PERIOD_END, AMOUNT_LIMIT, CURRENT_AMOUNT)
				values(@PartyID, 'LOSS_LIMIT', @CurrentMonday, @NextMonday, @LossLimit, 0)
			select @CurrentLoss = 0
		end
	end
	if @TransactionType = 'GAME_BET'
	begin
		--check wager limit
		if @WagerLimite is not null
		begin
			select @CurrentWager = abs(@AmountReal)
			if @CurrentWager > abs(@WagerLimite)
			begin 
				throw 50000, 'Exceed wager limit', 1;
			end
			update admin_all.USER_PERIOD_LIMIT 
				set CURRENT_AMOUNT = @CurrentWager
			where PARTYID = @PartyID
				and PERIOD_START = @CurrentMonday
				and LIMIT_TYPE = 'WAGER_LIMIT'
		end
		-- check loss limit
		if @LossLimit is not null and @CurrentLoss >= @LossLimit
		begin
			throw 50000, 'Exceed loss amount', 1;
		end
	end
	--update loss amount
	if @LossLimit is not null
	begin
		select @CurrentLoss = @CurrentLoss + @AmountReal
		update admin_all.USER_PERIOD_LIMIT 
			set CURRENT_AMOUNT = @CurrentWager
		where PARTYID = @PartyID
			and PERIOD_START = @CurrentMonday
			and LIMIT_TYPE = 'LOSS_LIMIT'
	end

end

go
