set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicationReadBrandFilterConfiguration
as
begin
	set nocount, xact_abort on 
	begin transaction
	update maint.ReplicationBrandFilterConfiguration set EnablePrevious = EnableCurrent, RetentionPeriodPrevious = RetentionPeriodCurrent
	;with e as
	(
		select b.BRANDID, cast(coalesce(br.VALUE, r.VALUE, 0) as int) EnableCurrent
		from admin.CASINO_BRAND_DEF b 
			left join admin_all.BRAND_REGISTRY br on b.BRANDID = br.BRANDID and br.MAP_KEY = 'database.replication.enable'
			left join admin_all.REGISTRY_HASH r on r.MAP_KEY = 'database.replication.enable'
	), p as
	(
		select b.BRANDID, cast(coalesce(br.VALUE, r.VALUE, 180) as int) RetentionPeriodCurrent
		from admin.CASINO_BRAND_DEF b 
			left join admin_all.BRAND_REGISTRY br on b.BRANDID = br.BRANDID and br.MAP_KEY = 'database.replication.retention.period(days)'
			left join admin_all.REGISTRY_HASH r on r.MAP_KEY = 'database.replication.retention.period(days)'
	), s as
	(
		select p.BrandID, e.EnableCurrent, p.RetentionPeriodCurrent
		from p
			inner join e on p.BrandID = e.BrandID
	)
	merge maint.ReplicationBrandFilterConfiguration t
	using s on t.BrandID = s.BrandID
	when not matched then
		insert (BrandID, EnableCurrent, RetentionPeriodCurrent, EnablePrevious, RetentionPeriodPrevious)
			values(s.BrandID, s.EnableCurrent, s.RetentionPeriodCurrent, 0, 0)
	when matched and (s.EnableCurrent <> t.EnableCurrent or s.RetentionPeriodCurrent <> t.RetentionPeriodCurrent) then
		update set t.EnableCurrent = s.EnableCurrent, t.RetentionPeriodCurrent = s.RetentionPeriodCurrent
	when not matched by source then
		delete;
	commit
end

go
