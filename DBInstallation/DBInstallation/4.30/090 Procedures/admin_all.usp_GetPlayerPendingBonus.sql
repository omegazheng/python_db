set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerPendingBonus]
    (@partyid int)
AS
  BEGIN
    SELECT ISNULL(SUM(b.AMOUNT), 0) as pendingBonus
    FROM admin_all.BONUS b WITH(NOLOCK)
        LEFT JOIN admin_all.BONUS_PLAN bp WITH(NOLOCK) ON b.BONUS_PLAN_ID = bp.ID
    WHERE
        b.PARTYID = @partyid
        AND bp.STATUS = 'ACTIVE' AND bp.IS_PLAYABLE = 0
        AND (b.status = 'ACTIVE' OR b.STATUS = 'QUEUED')

  END

go
