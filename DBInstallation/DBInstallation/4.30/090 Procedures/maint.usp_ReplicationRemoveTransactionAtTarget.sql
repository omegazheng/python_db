set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicationRemoveTransactionAtTarget(@RetentionPeriodCurrent int , @CheckBrands bit )
as
begin
	set nocount on
	create table #b(BrandID int primary key)
	if @CheckBrands = 1
	begin
		insert into #b(BrandID)
			select BrandID
			from maint.ReplicationBrandFilterConfiguration
			where RetentionPeriodCurrent = @RetentionPeriodCurrent
	end
	declare @AccountTranID bigint, @PaymentID int,@i bigint, @BatchSize int = 150
	if @RetentionPeriodCurrent is not null
	begin
		if not exists(select * from #b)
		begin
			declare c cursor local static for
				select ID from admin_all.ACCOUNT_TRAN_ALL where DATETIME < dateadd(day, -@RetentionPeriodCurrent, getdate()) 
				union all
				select ID from admin_all.ACCOUNT_TRAN_REALTIME where DATETIME < dateadd(day, -@RetentionPeriodCurrent, getdate()) 
		end
		else
		begin
			select at.ID 
			from admin_all.ACCOUNT_TRAN_ALL at
			where at.DATETIME < dateadd(day, -@RetentionPeriodCurrent, getdate()) 
				and exists(
							select * 
							from admin_all.ACCOUNT a 
								inner join external_mpt.USER_CONF u on u.ParentID = a.PARTYID
								inner join #b b on b.BrandID = u.BRANDID
							where a.id = at.ACCOUNT_ID
						)
			union all
			select at.ID 
			from admin_all.ACCOUNT_TRAN_REALTIME at
			where at.DATETIME < dateadd(day, -@RetentionPeriodCurrent, getdate()) 
				and exists(
							select * 
							from admin_all.ACCOUNT a 
								inner join external_mpt.USER_CONF u on u.ParentID = a.PARTYID
								inner join #b b on b.BrandID = u.BRANDID
							where a.id = at.ACCOUNT_ID
						)
		end
	end
	else
	begin
		declare c cursor local static for
			select at.ID 
			from admin_all.ACCOUNT_TRAN_ALL at
			where not exists(select * from admin_all.ACCOUNT a where a.id = at.ACCOUNT_ID)
			union all
			select at.ID 
			from admin_all.ACCOUNT_TRAN_REALTIME at
			where not exists(select * from admin_all.ACCOUNT a where a.id = at.ACCOUNT_ID)
	end
	open c
	fetch next from c into @AccountTranID
	while @@fetch_status = 0
	begin
		select @i = @i +1
		if @@trancount = 0
		begin
			begin transaction
		end
				
		delete admin_all.TRANSACTION_TAG_ACCOUNT_TRAN where ACCOUNT_TRAN_ID = @AccountTranID
		delete admin_all.ACCOUNT_TRAN_ALL where ID = @AccountTranID
		delete admin_all.ACCOUNT_TRAN_REALTIME where ID = @AccountTranID		
		if @i % @BatchSize = 0
		begin
			while @@trancount > 0
				commit
		end
		fetch next from c into @AccountTranID
	end
	close c
	deallocate c
	while @@trancount > 0
		commit
	if @RetentionPeriodCurrent is not null
	begin
		if not exists(select * from #b)
		begin
			declare c cursor local static for
				select ID from admin_all.PAYMENT where PROCESS_DATE < dateadd(day, -@RetentionPeriodCurrent, getdate()) 
		end
		else
		begin
			declare c cursor local static for
				select ID 
				from admin_all.PAYMENT p
				where  p.PROCESS_DATE < dateadd(day, -@RetentionPeriodCurrent, getdate()) 
					and exists(	select * 
								from admin_all.ACCOUNT a 
									inner join external_mpt.USER_CONF u on u.ParentID = a.PARTYID
									inner join #b b on b.BrandID = u.BRANDID
								where a.id = p.ACCOUNT_ID
								)
		end
	end
	else
	begin
		declare c cursor local static for
			select ID 
			from admin_all.PAYMENT p
			where not exists(select * from admin_all.ACCOUNT a where a.id = p.ACCOUNT_ID)
	end
	open c
	fetch next from c into @PaymentID
	while @@fetch_status = 0
	begin
		select @i = @i +1
		if @@trancount = 0
		begin
			begin transaction
		end
		delete admin_all.PAYMENT where ID = @PaymentID
		if @i % @BatchSize = 0
		begin
			while @@trancount > 0
				commit
		end
		fetch next from c into @PaymentID
	end
	close c
	deallocate c
	while @@trancount > 0
		commit
end
go
