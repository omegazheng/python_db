set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_AddRecentPlayer]
    (@staffid int, @partyid int)
AS
BEGIN
	if not exists (select * from admin_all.recent_users where staffid=@staffid and partyid=@partyid)
	    insert into admin_all.recent_users(staffid, partyid, last_access_time)
	        values(@staffid, @partyid, GETDATE())
	else
		update admin_all.recent_users set last_access_time=GETDATE() where staffid=@staffid and partyid=@partyid

END

go
