set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetDeviceTypeByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
    declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);
    declare @results table (name varchar(100), occurences int)

    insert into @results(name, occurences)
        select
            'desktop' as name,
            count(*) as occurences
        from admin_all.user_login_log
            join external_mpt.user_conf as agent on agent.partyid = user_login_log.partyid
        where
            user_login_log.is_mobile = 0
            and user_login_log.executed_by = 'PLAYER'
            and user_login_log.login_type = 'LOGIN'
            and user_login_log.login_time >= @start_date and login_time < @end_date

            and agent.partyid <> @agent_id
            and agent.hierarchy.IsDescendantOf(@agentHierarchy)=1

    insert into @results(name, occurences)
        select
            device_os as name,
            count(*) as occurences
        from admin_all.user_login_log
            join external_mpt.user_conf as agent on agent.partyid = user_login_log.partyid
        where
            user_login_log.is_mobile = 1
            and user_login_log.executed_by = 'PLAYER'
            and user_login_log.login_type = 'LOGIN'
            and user_login_log.login_time >= @start_date and login_time < @end_date

            and agent.partyid <> @agent_id
            and agent.hierarchy.IsDescendantOf(@agentHierarchy)=1
        group by device_os


    select * from @results
  END

go
