set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetNetworkRevenuesByStaffAndDateRange]
  (@start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
--     select date, sum(value) value
--     from (
      select sum(value) value, date
      from agency.player_ggr
      WHERE date >= @start_date and date < @end_date

--       select
--         'direct' source,
--         date, sum(value) value
--       from agency.direct_ggr
--       where date >= @start_date and date < @end_date
--       group by date
--       union
--       select
--         'network' source,
--         date, sum(value) value
--       from agency.network_ggr
--       where date >= @start_date and date < @end_date
--       group by date
--     ) x
--

    group by date
    order by date
  END

go
