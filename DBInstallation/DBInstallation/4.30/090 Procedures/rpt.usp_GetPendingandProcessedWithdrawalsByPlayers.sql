set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetPendingandProcessedWithdrawalsByPlayers
(
	@AgentID int,
	@DateFrom datetime,
	@DateTo datetime
)
as
begin
	DECLARE @endDateLocal DATETIME
	SET @endDateLocal = DATEADD(DD, 1, @DateTo);
	set nocount on
	;with x0 as 
	(
		select u.PARTYID, u.ParentID, u.UserID, u.Currency, u.user_type
		from external_mpt.USER_CONF u
		where ParentID = @AgentID
		union all
		select u1.PARTYID, u1.ParentID, u1.UserID, u1.Currency,u1.user_type
		from external_mpt.USER_CONF u1
			inner join x0 on x0.PARTYID = u1.ParentID
	)
	select 
			p.ID as RequestNumber, p.REQUEST_DATE, p.PROCESS_DATE,x0.USERID,
			isnull(p.REQUEST_AMOUNT, p.AMOUNT) as Request_Amount,
			case when status = 'COMPLETED' then p.AMOUNT else 0 end as amount, -- processed amount
			x0.CURRENCY, p.METHOD, p.STATUS, u.USERID Agent, u.PARTYID PartyId
	from x0	
		inner join admin_all.ACCOUNT a on a.PARTYID = x0.PARTYID
		inner join admin_all.PAYMENT p on p.ACCOUNT_ID = a.id
		left join external_mpt.USER_CONF u on u.PARTYID = x0.ParentID
	where p.TYPE = 'WITHDRAWAL'
		and p.REQUEST_DATE >= @DateFrom
		and p.REQUEST_DATE < @endDateLocal
		and isnull(x0.user_type, 0) = 0
end

go
