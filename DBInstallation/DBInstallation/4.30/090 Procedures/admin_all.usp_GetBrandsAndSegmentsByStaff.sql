set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetBrandsAndSegmentsByStaff]
    (@staff_id int)
AS
  BEGIN
    select distinct
      brands.brandId as brandId, brands.brandname as brandName,
      isnull (segment.id , 0) as segmentId, isnull (segment.name, 'Others')as segmentName
    from admin_all.segment,
      admin.casino_brand_def as brands
      join admin_all.staff_brand_tbl staffBrands on staffBrands.brandId = brands.brandId
    where staffBrands.staffid = @staff_id
  END

go
