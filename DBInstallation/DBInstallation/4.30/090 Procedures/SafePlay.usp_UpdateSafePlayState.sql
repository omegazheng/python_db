set ansi_nulls, quoted_identifier on
go

create or alter procedure SafePlay.usp_UpdateSafePlayState
  (
    @AccountTranID BIGINT,
    @State NVARCHAR(20),
    @Reference NVARCHAR(300)
  )
as
  begin
--     set nocount on
    declare @tempAccountTranId NVARCHAR(100)
    set @tempAccountTranId = CAST(@AccountTranID AS NVARCHAR(100));

    if not exists(select * from SafePlay.SafePlayEFTState where AccountTranID = @tempAccountTranId)
      begin
        raiserror('Do not has transaction %I64d', 16, 1, @AccountTranID)
      end

    update SafePlay.SafePlayEFTState set State=@State, Reference=@Reference, LastAccessTime=GETDATE()
    where AccountTranID = @tempAccountTranId

  end

go
