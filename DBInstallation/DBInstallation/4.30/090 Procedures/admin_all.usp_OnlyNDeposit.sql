set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_OnlyNDeposit]
(
  @startDate DATETIME ,
  @endDate DATETIME,
  @brands  NVARCHAR(MAX) = NULL,
  @count   NVARCHAR(10) = NULL,
  @countConditions NVARCHAR(3) = '=' --'<=,=,>='
)
AS
BEGIN
  SET NOCOUNT ON
  CREATE TABLE #CountConditions (
    CountConditions VARCHAR(3) PRIMARY KEY WITH (IGNORE_DUP_KEY = ON)
  )
  create table #BrandIDs(BrandID int primary key)
  insert into #BrandIDs(BrandID)
    select distinct cast(Item as int)
    from admin_all.fc_splitDelimiterString(@brands,',')
    where rtrim(Item)<> ''
          and isnumeric(Item) = 1

  IF @CountConditions = '<='
    INSERT INTO #CountConditions (CountConditions) VALUES ('<=')
  IF @CountConditions = '='
    INSERT INTO #CountConditions (CountConditions) VALUES ('=')
  IF @CountConditions = '>='
    INSERT INTO #CountConditions (CountConditions) VALUES ('>=')

  DECLARE @SQL NVARCHAR(MAX)
  SELECT @SQL = 'select
    p.id,
    p.type,
    p.method,
    p.process_date,
    p.amount,
    u.currency,
    u.partyid,
    u.userid,
    u.first_name,
    u.last_name,
    u.address,
    u.city,
    u.province,
    u.country,
    u.postal_code,
    u.phone,
    u.email,
    u.reg_date,
    b.brandname
from
    [admin_all].[payment] p WITH (NOLOCK),
    [external_mpt].[user_conf] u WITH (NOLOCK),
    [admin_all].[account] a WITH (NOLOCK),
    [admin].[casino_brand_def] b WITH (NOLOCK)
where
    p.account_id = a.id and
    a.partyid = u.partyid and
    u.brandid = b.brandid and
    u.brandid in (select b.BrandID from #BrandIDs b)and
    p.type=''DEPOSIT'' and p.status=''COMPLETED'' and
    p.account_id in (
        select account_id
        from (
            select account_id, count(*) as COUNT
            from [admin_all].[payment] WITH (NOLOCK)
            where
                type=''DEPOSIT'' and status=''COMPLETED'' and
                account_id in (
                    select account_id
                    from (
                        select account_id, count(*) as COUNT
                        from [admin_all].[payment] WITH (NOLOCK)
                        where type=''DEPOSIT'' and status=''COMPLETED''
                        group by account_id
                    ) sub1 where' +
CASE WHEN @countConditions = '<=' THEN ' sub1.count <= '+@count
                  WHEN @countConditions = '='
                    THEN ' sub1.count = '+@count
                    WHEN @countConditions = '>='
                      THEN ' sub1.count >= '+@count
                    END
                    + ')
                and process_date >= @startDate and process_date <= @endDate
                group by account_id
        ) sub2 where' +
                CASE
                WHEN @countConditions = '<='
                  THEN ' sub2.count <= '+@count
                  WHEN @countConditions = '='
                    THEN ' sub2.count = '+@count
                    WHEN @countConditions = '>='
                      THEN ' sub2.count >= '+@count
                    END
                    + ')
    and p.process_date >=@startDate and p.process_date <= @endDate'
--   PRINT @SQL
  EXEC sp_executesql @SQL, N'@count NVARCHAR(10),	@countConditions NVARCHAR(3),@startDate DATETIME,@endDate DATETIME,@brands NVARCHAR(MAX)', @count, @countConditions,@startDate,@endDate,@brands
END

go
