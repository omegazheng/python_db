set ansi_nulls, quoted_identifier on
go
create or alter procedure [maint].[usp_ReplicateOneAccountTran](@FriendlyName varchar(128))
as
begin
	set nocount on
	declare @ConfigurationID int = maint.fn_GetReplicationConfigurationID(@FriendlyName)
	declare @Date datetime = getdate()
	exec maint.usp_SetReplicateOneContextInfo
	begin try
		declare @SourceConnectionID int, @SourceTableOrQuery varchar(max), @Columns varchar(max),
				@FilterType varchar(100), @TargetConnectionID int, @TargetSchemaName varchar(128),
				@TargetObjectName varchar(128), @BatchSize int, @IsActive bit, 
				@SourceConnectionString varchar(max), @FullTargetTableName nvarchar(256)
		select
				@SourceConnectionID = SourceConnectionID, @SourceTableOrQuery = SourceTableOrQuery, @Columns = Columns,
				@FilterType = FilterType, @TargetConnectionID = TargetConnectionID, @TargetSchemaName = TargetSchemaName,
				@TargetObjectName = TargetObjectName, @BatchSize = BatchSize, @IsActive = IsActive,
				@SourceConnectionString = maint.fn_GetSQLConnectionStringByID(SourceConnectionID),
				@FullTargetTableName = TargetSchemaName + '.' + TargetObjectName
		from maint.ReplicationConfiguration
		where ConfigurationID = @ConfigurationID
		if @@rowcount = 0 or @IsActive = 0
		begin
			print 'Could not find replication configuration'
			goto ___End___
		end


		declare @FromValueQuery varchar(max), @FromValueQueryEvaluationType varchar(20), @ToValueQuery varchar(max), 
				@ToValueQueryEvaluationType varchar(20), @ToValueAdjustment varchar(max), @FromValueAdjustment varchar(max),
				@CurrentChangeTrackingVersion bigint, @CurrentRowVersion binary(8)
		select 
				@FromValueQuery = FromValueQuery, @FromValueQueryEvaluationType = FromValueQueryEvaluationType, @ToValueQuery = ToValueQuery, 
				@ToValueQueryEvaluationType = ToValueQueryEvaluationType, @ToValueAdjustment = ToValueAdjustment, @FromValueAdjustment = FromValueAdjustment
		from maint.FilterType
		where FilterType = @FilterType
		if @@rowcount = 0
		begin
			print 'Cound not find Filter Type.'
			goto ___End___
		end
		
		select	@CurrentChangeTrackingVersion = CurrentChangeTrackingVersion,
				@CurrentRowVersion = CurrentRowVersion
		from maint.Connection 
		where ConnectionID = @SourceConnectionID

		declare @FromValue sql_variant, @ToValue sql_variant
		select @FromValue = FromValue, @ToValue = ToValue
		from maint.ReplicationLastExecution
		where ConfigurationID = @ConfigurationID

		exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @StartDate = @Date

		declare @Param nvarchar(max), @Param1 nvarchar(max), @Param2 nvarchar(max), @SQL nvarchar(max), @Rows int = 0, @Error varchar(max)
		
		

			
		if maint.fn_IsChangeTrackingEnabled(@SourceConnectionString) = 0
		begin
			exec maint.usp_EnableChangeTracking @SourceConnectionString
		end
		if maint.fn_IsChangeTrackingEnabledOnTable(@SourceConnectionString, @SourceTableOrQuery) = 0
		begin
			exec maint.usp_EnableChangeTrackingOnTable @SourceConnectionString, @SourceTableOrQuery
		end
		select * into #SourceDefinition from maint.fn_GetTableSchema(@SourceConnectionString, @SourceTableOrQuery)
		delete #SourceDefinition where ColumnName in ('___IsDeleted___')
		if @Columns is not null
			select * from #SourceDefinition where PrimaryKeyOrder > 0 or ColumnName in (select Value from maint.SplitDelimitedString(@Columns, ','))
		
		

		select @FromValue = @ToValue
		--select @FromValue = cast(@ToValue as bigint) - 1
		if @FromValue < 0
			select @FromValue = 0
		select @ToValue = CurrentChangeTrackingVersion 
		from maint.Connection 
		where ConnectionID = @SourceConnectionID
		
		if @FromValue is null
		begin
			select @SQL = '
							select 0 as ___IsDeleted___'+(select ',t.'+quotename(ColumnName) from #SourceDefinition order by ColumnID  for xml path(''), type).value('.', 'varchar(max)')+' 
							from ' + @SourceTableOrQuery + ' t 
								inner join maint . ReplicationBrandFilterConfiguration c on t.BRAND_ID = c.BrandID 
							where c.EnableCurrent = 1 and t.DATETIME >= dateadd(day, -c.RetentionPeriodCurrent, getdate())'
			select @FromValue = 0
			exec @Rows = maint.usp_ConnectionBulkCopy @SourceConnectionID = @SourceConnectionID, @CommandText = @SQL, @TargetConnectionID = @TargetConnectionID, @TargetTable = @FullTargetTableName, @BatchSize = @BatchSize
		end
		else
		begin
			select @Rows = 0
			-----Initialize target for a brands
			if maint.fn_ExecuteSQLScalarString(maint.fn_GetSQLConnectionStringByID(@SourceConnectionID),' 
				if exists(
						select * 
						from maint.ReplicationBrandFilterConfiguration c
						where isnull(c.EnablePrevious, EnableCurrent) = 0 and c.EnableCurrent = 1
							or isnull(c.RetentionPeriodPrevious, c.RetentionPeriodCurrent) < c.RetentionPeriodCurrent
						) select ''1'' else select ''0''')  = '1'
			begin
				select @SQL = 'select 0 as ___IsDeleted___'+(select ',t.'+quotename(ColumnName) from #SourceDefinition for xml path(''), type).value('.', 'varchar(max)')+' 
						from ' + @SourceTableOrQuery + ' t
							inner join maint . ReplicationBrandFilterConfiguration c on t.BRAND_ID = c.BrandID 
						where (
									isnull(c.EnablePrevious, EnableCurrent) = 0 and c.EnableCurrent = 1
									or isnull(c.RetentionPeriodPrevious, c.RetentionPeriodCurrent) < c.RetentionPeriodCurrent
								) and t.DATETIME >= dateadd(day, -c.RetentionPeriodCurrent, getdate())'
				exec @Rows = maint.usp_ConnectionBulkCopy @SourceConnectionID = @SourceConnectionID, @CommandText = @SQL, @TargetConnectionID = @TargetConnectionID, @TargetTable = @FullTargetTableName, @BatchSize = @BatchSize
			end

			select @SQL = 'exec sp_executesql N''
	select '+(select ''+case when PrimaryKeyOrder>0 then 'ct.'+quotename(ColumnName) else 't.'+quotename(ColumnName) end+' as ' + quotename(ColumnName) + ',' from #SourceDefinition order by ColumnID for xml path(''), type).value('.', 'varchar(max)')+'
		case when ct.SYS_CHANGE_OPERATION = ''''D'''' or t.Account_ID is null then 1 else 0 end ___IsDeleted___ 
	from changetable(changes '+@SourceTableOrQuery+', @Version) ct
		left loop join '+@SourceTableOrQuery+' t 
			inner join maint . ReplicationBrandFilterConfiguration c on t.BRAND_ID = c.BrandID  and c.EnableCurrent = 1  and t.DATETIME >= dateadd(day, -c.RetentionPeriodCurrent, getdate())
			on '+stuff((select ' and ct.'+quotename(ColumnName)+' = t.' + quotename(ColumnName) from #SourceDefinition where PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'varchar(max)'), 1, 5, '')+'
	where ct.SYS_CHANGE_CONTEXT is null
	'', N''@Version bigint'','+cast(cast(@FromValue as bigint) as varchar(100))
	
			exec @Rows = maint.usp_ConnectionBulkCopy @SourceConnectionID = @SourceConnectionID, @CommandText = @SQL, @TargetConnectionID = @TargetConnectionID, @TargetTable = @FullTargetTableName, @BatchSize = @BatchSize
		end
			
	
		exec maint.usp_SetReplicationHistory @ConfigurationID =@ConfigurationID, @FromValue= @FromValue, @ToValue = @ToValue
		

___End___:
	end try
	begin catch
		select @Error = error_message()
		exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @Error = @Error
	end catch
	select @Date = getdate()
	exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @EndDate = @Date, @Rows = @Rows
	exec maint.usp_ClearReplicateOneContextInfo
	return @ConfigurationID
end 

go
