set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DeactivateAccount(@PartyID int = 10000)
as
  begin
    set nocount, xact_abort on
    declare @NewPartyID int = -@PartyID -1234
    declare @SQL nvarchar(max)


    select @SQL = cast('
	declare @AccountID int
	set xact_abort, nocount on
	begin transaction
	set identity_insert external_mpt.USER_CONF on
	insert into external_mpt.USER_CONF(PARTYID, USERID, PASSWORD, GL_ACCOUNT, ACTIVE_FLAG, FIRST_NAME, LAST_NAME, ADDRESS, CITY, PROVINCE, COUNTRY, POSTAL_CODE, PHONE, EMAIL, BIRTHDATE, BONUS, HINT_QUESTION, ANSWER, LANGUAGE, REF_FRIENDID, LOGONMSG, LOGONMSG_FLAG, IP, REG_DATE, MAILLIST_FLAG, GENDER, PHONE2, LAST_DEPOSIT_DATE, LAST_DEPOSIT_AMOUNT, LAST_DEPOSIT_TYPE, LAST_WITHDRAW_DATE, LAST_WITHDRAW_AMOUNT, COMMENTS, FAX, CATEGORY, BRANDID, BUILDING, UNIT, CW_PLAYER_HANDLE, REGISTRATION_STATUS, CONFIRMATION_CODE, MOBILE_PHONE, CW_PLAYER_ID, TITLE, LOCKED_STATUS, LOCKED_UNTIL, LOCKED_BY_STAFFID, LOCKED_SINCE, LOGIN_ATTEMPTS, PASSWORD_V2, CURRENCY, REALITY_CHECK_INTERVAL, SESSION_LIMIT_INTERVAL, MADE_DEPOSIT, RESETPASSWORD_KEY, EGAME_TOKEN, NATIONAL_REG_NUMBER, PASSPORT_NUMBER, PROFESSION_ID, BIRTH_COUNTRY, BIRTH_CITY, ALLOW_EMAILS, WINNERS_LIST, VIP_STATUS, PRIMARY_WALLET_UUID, QUICK_REGISTER_CODE, COMP_REG_EMAIL_SENT, NICKNAME, DUPLICATE_PRIMARY_PARTY_ID, KYC_STATUS, LAST_KYC_REQUESTED_TIME, DISPLAY_MESSAGE, CCLEVEL_ID, player_lock_reason_id, WITHDRAWAL_THRESHOLD_VERIFIED, KYC_AGE_STATUS, ALTERNATIVE_COUNTRY, IOVATION_STATUS, user_type, ParentID, IS_AUTOPAY, BRAND_ALIAS_ID, IOVATION_CHECK)
	select @NewPartyID PARTYID, @NewPartyID USERID,  0x12345  PASSWORD, @NewPartyID GL_ACCOUNT, ''0'' ACTIVE_FLAG, ''-'' FIRST_NAME, ''-'' LAST_NAME,
			''-'' ADDRESS, CITY, PROVINCE, COUNTRY, ''-'' POSTAL_CODE, ''-'' PHONE, cast(@NewPartyID as varchar(20)) + ''@hotmail.com'' EMAIL, ''1980-01-01'' BIRTHDATE, 0 BONUS,
			''-'' HINT_QUESTION, ''-'' ANSWER, LANGUAGE, REF_FRIENDID, LOGONMSG, LOGONMSG_FLAG, IP, REG_DATE, MAILLIST_FLAG, GENDER, ''-'' PHONE2,
			null LAST_DEPOSIT_DATE, 0 LAST_DEPOSIT_AMOUNT, LAST_DEPOSIT_TYPE, null LAST_WITHDRAW_DATE, 0 LAST_WITHDRAW_AMOUNT, ''-'' COMMENTS,
			''-'' FAX,
			CATEGORY, BRANDID, ''-'' BUILDING, ''-''UNIT, 0 CW_PLAYER_HANDLE, REGISTRATION_STATUS, ''-'' CONFIRMATION_CODE, ''-'' MOBILE_PHONE,
			CW_PLAYER_ID, ''-'' TITLE, ''LOCKED'' LOCKED_STATUS, LOCKED_UNTIL, LOCKED_BY_STAFFID, LOCKED_SINCE,
			LOGIN_ATTEMPTS, 0x12345 PASSWORD_V2, CURRENCY, REALITY_CHECK_INTERVAL, SESSION_LIMIT_INTERVAL, MADE_DEPOSIT,
			RESETPASSWORD_KEY, EGAME_TOKEN, ''-'' NATIONAL_REG_NUMBER, ''-'' PASSPORT_NUMBER, PROFESSION_ID,
			BIRTH_COUNTRY, BIRTH_CITY, ALLOW_EMAILS, WINNERS_LIST, VIP_STATUS, cast(@NewPartyID as varchar(20))  PRIMARY_WALLET_UUID,
			QUICK_REGISTER_CODE, COMP_REG_EMAIL_SENT, ''-'' NICKNAME, null DUPLICATE_PRIMARY_PARTY_ID,
			KYC_STATUS, LAST_KYC_REQUESTED_TIME, 0 DISPLAY_MESSAGE,
			CCLEVEL_ID, player_lock_reason_id, WITHDRAWAL_THRESHOLD_VERIFIED,
			KYC_AGE_STATUS, ALTERNATIVE_COUNTRY, IOVATION_STATUS, user_type, ParentID,  IS_AUTOPAY, BRAND_ALIAS_ID, IOVATION_CHECK
	from external_mpt.USER_CONF
	where PARTYID = @PartyID
	set identity_insert external_mpt.USER_CONF off;' as nvarchar(max)) +

                  (select 'update t set t.'+quotename( col_name(fc.parent_object_id, fc.parent_column_id)) + '= @NewPartyID from '
                          +quotename(object_schema_name(f.parent_object_id)) +'.' +quotename(object_name(f.parent_object_id))
                          + ' t where t.'+quotename( col_name(fc.parent_object_id, fc.parent_column_id)) + '= @PartyID;
		'
                   from sys.foreign_keys f
                     inner join sys.foreign_key_columns fc on f.object_id = fc.constraint_object_id
                   where f.referenced_object_id = object_id('external_mpt.USER_CONF')
						and f.parent_object_id not in (object_id('admin_all.account'))
                   for xml path(''), type).value('.', 'nvarchar(max)')
                  +'
	;
	select @AccountID = ID from admin_all.ACCOUNT where PartyID = @PartyID
	if(@AccountID is not null)
	begin
		set identity_insert admin_all.account on 
		insert into admin_all.account([id], [PARTYID], [BALANCE_REAL], [RELEASED_BONUS], [PLAYABLE_BONUS], [RAW_LOYALTY_POINTS], [SECONDARY_BALANCE], [UNPAID_CIT], [CIT])
			select  @NewPartyID [id],  @NewPartyID [PARTYID], [BALANCE_REAL], [RELEASED_BONUS], [PLAYABLE_BONUS], [RAW_LOYALTY_POINTS], [SECONDARY_BALANCE], [UNPAID_CIT], [CIT]
			from admin_all.account
			where id = @AccountID
		set identity_insert  admin_all.account off
		'+

                  (select 'update t set t.'+quotename( col_name(fc.parent_object_id, fc.parent_column_id)) + '= @NewPartyID from '
                          +quotename(object_schema_name(f.parent_object_id)) +'.' +quotename(object_name(f.parent_object_id))
                          + ' t where t.'+quotename( col_name(fc.parent_object_id, fc.parent_column_id)) + '= @PartyID;
		'
                   from sys.foreign_keys f
                     inner join sys.foreign_key_columns fc on f.object_id = fc.constraint_object_id
                   where f.referenced_object_id = object_id('admin_all.account')
                   for xml path(''), type).value('.', 'nvarchar(max)')
                  +'
		delete admin_all.account where id = @AccountID
	end
	
delete from external_mpt.user_conf where partyId = @PartyID;

commit
	'
    exec sp_executesql @SQL, N'@PartyID int, @NewPartyID int', @PartyID, @NewPartyID
    --select * from external_mpt.USER_CONF where PARTYID = 10000
	--select * from admin_all.account where id = 10000
  end

go
