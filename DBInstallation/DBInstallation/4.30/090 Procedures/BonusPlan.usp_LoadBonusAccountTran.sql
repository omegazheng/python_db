set ansi_nulls, quoted_identifier on
go

create or alter procedure BonusPlan.usp_LoadBonusAccountTran
	(
		@id bigint = 0,
		@accountTranId bigint = 0,
		@bonusId int = 0,
		@accountTranIds varchar(max) = null
	)
as
	begin
		if @id <> 0
			BEGIN
				select
					ID,
					BonusID,
					PartyID,
					BrandID,
					DateTime,
					TranType,
					AmountPlayableBonus,
					AmountPlayableBonusWinnings,
					AmountReleasedBonus,
					AmountReleasedBonusWinnings,
					AmountWageredContribution,
					BalancePlayableBonus,
					BalancePlayableBonusWinnings,
					BalanceReleasedBonus,
					BalanceReleasedBonusWinnings,
					AccountTranId
				from BonusPlan.BonusAccountTran where id = @id
				return
			END

		if @accountTranId <> 0
			BEGIN
				select
					id,
					BonusID,
					PartyID,
					BrandID,
					DateTime,
					TranType,
					AmountPlayableBonus,
					AmountPlayableBonusWinnings,
					AmountReleasedBonus,
					AmountReleasedBonusWinnings,
					AmountWageredContribution,
					BalancePlayableBonus,
					BalancePlayableBonusWinnings,
					BalanceReleasedBonus,
					BalanceReleasedBonusWinnings,
					AccountTranId
				from BonusPlan.BonusAccountTran with(forceseek)
				where AccountTranId = @accountTranId --(AccountTranID)
				return
			END

		if @bonusId <> 0
			BEGIN
				select
					id,
					BonusID,
					PartyID,
					BrandID,
					DateTime,
					TranType,
					AmountPlayableBonus,
					AmountPlayableBonusWinnings,
					AmountReleasedBonus,
					AmountReleasedBonusWinnings,
					AmountWageredContribution,
					BalancePlayableBonus,
					BalancePlayableBonusWinnings,
					BalanceReleasedBonus,
					BalanceReleasedBonusWinnings,
					AccountTranId
				from BonusPlan.BonusAccountTran with(forceseek) where BonusID = @bonusId ---(BonusID)
			END

				if @accountTranIds is not null
					BEGIN
					select
						id,
						BonusID,
						PartyID,
						BrandID,
						DateTime,
						TranType,
						AmountPlayableBonus,
						AmountPlayableBonusWinnings,
						AmountReleasedBonus,
						AmountReleasedBonusWinnings,
						AmountWageredContribution,
						BalancePlayableBonus,
						BalancePlayableBonusWinnings,
						BalanceReleasedBonus,
						BalanceReleasedBonusWinnings,
						AccountTranId
					from BonusPlan.BonusAccountTran with(forceseek) where AccountTranId in (SELECT *
				FROM fc_splitDelimiterString(@accountTranIds, ','))
						end
				return

		---(PartyID), (BrandID),(BrandID, TranType, DateTime) include(amounts, partyID)
end

go
