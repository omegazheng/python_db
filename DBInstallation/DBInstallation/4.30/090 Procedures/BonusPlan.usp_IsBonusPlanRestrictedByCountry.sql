set ansi_nulls, quoted_identifier on
go

create or alter procedure BonusPlan.usp_IsBonusPlanRestrictedByCountry
		@BonusPlanId int,
		@Country nchar(2)
as
	begin
		if not exists(select * from admin_all.Bonus_Plan_Country where bonus_plan_id = @BonusPlanId)
			select 0 as IsRestricted
		else
			begin
				if exists (
						select * from admin_all.Bonus_Plan_Country where bonus_plan_id = @BonusPlanId and country = @Country
				)
					select 0 as IsRestricted
				else
					select 1 as IsRestricted
			end
	end


-- Test
--exec BonusPlan.usp_IsBonusPlanRestrictedByCountry @BonusPlanId = 31, @Country = 'US'
go
