set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerTransactionsReportSummary]
  (
    @partyid INT,
    @startDate DATETIME,
    @endDate DATETIME
  )
AS
  BEGIN
    DECLARE @partyidLocal INT
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = @endDate
    SET @partyidLocal = @partyid

    SELECT
      COUNT(*) as totalRecord
    from
      ACCOUNT_TRAN a with(NOLOCK)
      left join PLATFORM p with(NOLOCK) on p.id = a.platform_id
      left join GAME_INFO g with(NOLOCK) on a.GAME_ID = g.GAME_ID and a.platform_id = g.PLATFORM_ID
      left join TRANSACTION_PLATFORM_CONVERSION pc with(NOLOCK) on pc.TRANSACTION_ID = a.id
      join admin_all.ACCOUNT aa with(NOLOCK) on aa.id = a.account_id
      join external_mpt.user_conf u with(NOLOCK) on u.partyid = aa.partyid
    where
      u.partyid = @partyidLocal
      and a.datetime >= @startDateLocal
      and a.datetime < @endDateLocal

  END

go
