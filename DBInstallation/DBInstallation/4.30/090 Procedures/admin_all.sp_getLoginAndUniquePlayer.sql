set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getLoginAndUniquePlayer]
  (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);

    SELECT
      SUM(UNIQUE_PLAYERS) AS UNIQUE_PLAYERS,
      SUM(LOGINS)         AS LOGINS
    FROM
      (
        --UNIQUE PLAYERS
        SELECT
          COUNT(DISTINCT (PARTY_ID)) AS UNIQUE_PLAYERS,
          0                          AS LOGINS
        FROM ADMIN_ALL.DW_GAME_PLAYER_DAILY DW
        WHERE DW.SUMMARY_DATE >= @startDateLocal
              AND
              DW.SUMMARY_DATE < @endDateLocal

        UNION
        --LOGINS
        SELECT
          0        AS UNIQUE_PLAYERS,
          COUNT(*) AS LOGINS
        FROM ADMIN_ALL.USER_LOGIN_LOG
          LEFT JOIN EXTERNAL_MPT.USER_CONF
            ON USER_LOGIN_LOG.PARTYID = USER_CONF.PARTYID
        WHERE LOGIN_TIME >= @startDateLocal
              AND LOGIN_TIME < @endDateLocal
              AND LOGIN_TYPE = 'LOGIN'

      ) temp
  END

go
