set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetCompanyList
(
	@StaffID int
)
as
begin
	set nocount on
	declare @StaffType varchar(10) = admin_all.fn_StaffType(@StaffID)
	if @StaffType = 'Company'
	begin
		select * 
		from admin_all.Company c
		where exists(
						select * 
						from admin_all.StaffCompany sc 
						where sc.StaffID = @StaffID
							and c.CompanyID = sc.CompanyID
					)
		order by 2, 1
		return
	end
	if @StaffType = 'Product'
	begin
		select * 
		from admin_all.Company c
		where exists(
						select * 
						from admin_all.StaffProduct sp
							inner join admin_all.Machine m on m.ProductID = sp.ProductID
							inner join admin_all.Location l on l.LocationID = m.LocationID
						where sp.StaffID = @StaffID
							and c.CompanyID = l.CompanyID
					)
		order by 2, 1
		return
	end
	select * 
	from admin_all.Company c
	order by 2,1
end
go
