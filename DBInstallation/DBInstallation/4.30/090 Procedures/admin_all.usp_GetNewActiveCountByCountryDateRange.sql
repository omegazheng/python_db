set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetNewActiveCountByCountryDateRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

SELECT
  count(*) as counts,
  COUNTRY as COUNTRY
FROM
  (
    SELECT
      u.PARTYID,
      COUNTRY
    FROM
      external_mpt.user_conf u
      JOIN admin_all.ACCOUNT a ON a.PARTYID = u.PARTYID
      JOIN admin_all.PAYMENT p ON p.ACCOUNT_ID = a.ID
    WHERE
      u.REG_DATE >= @startDateLocal
      AND u.REG_DATE < @endDateLocal
      AND p.TYPE = 'DEPOSIT'
      AND p.STATUS = 'COMPLETED'
  ) NEW_ACTIVE
group by country
order by counts desc

  END

go
