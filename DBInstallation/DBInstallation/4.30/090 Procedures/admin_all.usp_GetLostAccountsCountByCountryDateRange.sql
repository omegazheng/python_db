set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetLostAccountsCountByCountryDateRange]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
  DECLARE @startDateLocal DATETIME
  DECLARE @endDateLocal DATETIME
  DECLARE @oneYearBack DATETIME
  DECLARE @twoYearBack DATETIME

  SET @startDateLocal = @startDate
  SET @endDateLocal = DATEADD(DD, 1, @endDate);
  SET @oneYearBack = DATEADD(YEAR, -1, @endDate)
  set @twoYearBack = DATEADD(YEAR, -2, @endDate)

    SELECT
      count(*) as counts,
      country as COUNTRY
    FROM
      (
        SELECT
          DISTINCT
          PARTY_ID,
          COUNTRY
      FROM
        [admin_all].[DW_GAME_PLAYER_DAILY] dw
      WHERE
        dw.SUMMARY_DATE >= @twoYearBack
        AND dw.SUMMARY_DATE < @oneYearBack
        AND dw.PARTY_ID NOT IN
            (SELECT dw1.PARTY_ID FROM [admin_all].[DW_GAME_PLAYER_DAILY] dw1
            WHERE
              dw1.SUMMARY_DATE >= @oneYearBack
              AND dw1.SUMMARY_DATE < @endDateLocal )
  ) ACTIVE_SPORTS
    group by country
    order by counts desc
  END

go
