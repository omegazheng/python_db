set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_RebuildIndexWihCompression(@TableName nvarchar(128)= null)
as
begin
	set nocount on
	declare @SQL nvarchar(max)
	declare c cursor local static for
		select 'alter index ' + quotename(i.name) collate database_default + ' on '+ quotename(schema_name(t.schema_id)) + '.' + quotename(t.name)
			+' rebuild partition = all with(data_compression =page) ;'
		from sys.indexes i
			inner join sys.tables t on t.object_id = i.object_id
		where exists(select * from sys.partitions p where p.object_id = i.object_id and p.index_id = i.index_id and p.data_compression = 0)
			and (@TableName is null or i.object_id = object_id(@TableName))
	open c
	fetch next from c into @SQL
	while @@fetch_status = 0
	begin
		--print @SQL
		exec(@SQL)
		fetch next from c into @SQL
	end
	close c
	deallocate c
end


go
