set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_getUserActivities(@spName varchar(50), @startDate DateTime, @endDate DateTime)
as
	begin
		SELECT
			users.PARTYID,
			BRANDID,
			CURRENCY,
			ISNULL(PLATFORM.ID,0) as PRODUCT_ID,
			ISNULL(PLATFORM.NAME, '') AS PRODUCT_NAME,
			userActivity.*
		FROM EXTERNAL_MPT.USER_CONF users
			JOIN ACCOUNT ON ACCOUNT.PARTYID = USERS.PARTYID
			JOIN
			(
				SELECT
					ACCOUNT_ID,
					PROCESS_DATE as Date,
					PLATFORM_ID,
					SUM(HANDLE_REAL) HANDLE_REAL,
					SUM(HANDLE_RELEASED_BONUS) HANDLE_RELEASED_BONUS,
					SUM(HANDLE_PLAYABLE_BONUS) HANDLE_PLAYABLE_BONUS,
					SUM(WIN_REAL) WIN_REAL,
					SUM(WIN_RELEASED_BONUS) WIN_RELEASED_BONUS,
					SUM(WIN_PLAYABLE_BONUS) WIN_PLAYABLE_BONUS,
					SUM(CHARGE_BACK) CHARGE_BACK,
					SUM(ROLLBACK_REAL) ROLLBACK_REAL,
					SUM(ROLLBACK_RELEASED_BONUS) ROLLBACK_RELEASED_BONUS,
					SUM(ROLLBACK_PLAYABLE_BONUS) ROLLBACK_PLAYABLE_BONUS,
					SUM(PAYMENT_FEE) PAYMENT_FEE,
					SUM(WITHDRAWAL_REAL) WITHDRAWAL_REAL,
					SUM(WITHDRAWAL_RELEASED_BONUS) WITHDRAWAL_RELEASED_BONUS,
					SUM(DEPOSIT) DEPOSIT,
					SUM(BONUS_RELEASED) BONUS_RELEASED
				FROM
					(
						SELECT
							ACCOUNT_ID,
							PROCESS_DATE,
							PLATFORM_ID,
							HANDLE_REAL,
							HANDLE_RELEASED_BONUS,
							HANDLE_PLAYABLE_BONUS,
							WIN_REAL,
							WIN_RELEASED_BONUS,
							WIN_PLAYABLE_BONUS,
							CHARGE_BACK,
							ROLLBACK_REAL,
							ROLLBACK_RELEASED_BONUS,
							ROLLBACK_PLAYABLE_BONUS,
							PAYMENT_FEE,
							0 AS WITHDRAWAL_REAL,
							0 AS WITHDRAWAL_RELEASED_BONUS,
							0 AS DEPOSIT,
							BONUS_RELEASED
						FROM
							(
								SELECT
									ACCOUNT_ID,
									CAST(DATETIME AS DATE) AS PROCESS_DATE,
									ISNULL(PLATFORM_ID,0) PLATFORM_ID,
									sum(
											CASE
											WHEN (tran_type = 'GAME_PLAY' AND amount_real < 0) OR tran_type in('GAME_BET','STAKE_DEC')
												THEN AMOUNT_REAL
											ELSE 0
											END)                  HANDLE_REAL,

									sum(
											CASE
											WHEN (tran_type = 'GAME_PLAY' AND amount_real < 0) OR tran_type in('GAME_BET','STAKE_DEC')
												THEN AMOUNT_RELEASED_BONUS
											ELSE 0
											END)                  HANDLE_RELEASED_BONUS,

									sum(
											CASE
											WHEN (tran_type = 'GAME_PLAY' AND amount_real < 0) OR tran_type in('GAME_BET','STAKE_DEC')
												THEN AMOUNT_PLAYABLE_BONUS
											ELSE 0
											END)                  HANDLE_PLAYABLE_BONUS,

									sum(
											CASE
											WHEN (tran_type in ('GAME_WIN','CASH_OUT'))
												THEN AMOUNT_REAL
											ELSE 0
											END)                  WIN_REAL,
									sum(
											CASE
											WHEN (tran_type in ('GAME_WIN','CASH_OUT'))
												THEN AMOUNT_RELEASED_BONUS
											ELSE 0
											END)                  WIN_RELEASED_BONUS,

									sum(
											CASE
											WHEN (tran_type in ('GAME_WIN','CASH_OUT'))
												THEN AMOUNT_PLAYABLE_BONUS
											ELSE 0
											END)                  WIN_PLAYABLE_BONUS,

									sum(
											CASE
											WHEN (tran_type = 'CHARGE_BCK')
												THEN (AMOUNT_REAL + AMOUNT_RELEASED_BONUS)
											ELSE 0
											END)                  CHARGE_BACK,

									sum(
											CASE
											WHEN (tran_type in ('ROLLBACK', 'REFUND'))
												THEN (AMOUNT_REAL)
											ELSE 0
											END)                  ROLLBACK_REAL,
									sum(
											CASE
											WHEN (tran_type in ('ROLLBACK', 'REFUND'))
												THEN (AMOUNT_RELEASED_BONUS)
											ELSE 0
											END)                  ROLLBACK_RELEASED_BONUS,
									sum(
											CASE
											WHEN (tran_type in ('ROLLBACK', 'REFUND'))
												THEN (AMOUNT_PLAYABLE_BONUS)
											ELSE 0
											END)                  ROLLBACK_PLAYABLE_BONUS,
									sum(
											CASE
											WHEN (tran_type = 'PAYMNT_FEE')
												THEN AMOUNT_REAL
											ELSE 0
											END)                  PAYMENT_FEE,
									sum(
											CASE
											WHEN (tran_type = 'BONUS_REL')
												THEN AMOUNT_RELEASED_BONUS
											ELSE 0
											END)                  BONUS_RELEASED
								FROM ACCOUNT_TRAN WITH ( NOLOCK )
								WHERE
									DATETIME >= @startDate
									AND DATETIME < @endDate
								GROUP BY ACCOUNT_TRAN.ACCOUNT_ID, PLATFORM_ID, CAST(ACCOUNT_TRAN.DATETIME AS DATE)
							) at

						UNION

						(
							SELECT
								ACCOUNT_ID,
								PROCESS_DATE,
								0 AS PLATFORM_ID,
								0 AS HANDLE_REAL,
								0 AS HANDLE_RELEASED_BONUS,
								0 AS HANDLE_PLAYABLE_BONUS,
								0 AS WIN_REAL,
								0 AS WIN_RELEASED_BONUS,
								0 AS WIN_PLAYABLE_BONUS,
								0 AS CHARGE_BACK,
								0 AS ROLLBACK_REAL,
								0 AS ROLLBACK_RELEASED_BONUS,
								0 AS ROLLBACK_PLAYABLE_BONUS,
								0 AS PAYMENT_FEE,
								WITHDRAWAL_REAL,
								WITHDRAWAL_RELEASED_BONUS,
								0 AS DEPOSIT,
								0 AS BONUS_RELEASED
							FROM
								(
									SELECT
										ACCOUNT_ID,
										CAST(PROCESS_DATE AS DATE) PROCESS_DATE,
										SUM(AMOUNT_REAL)           WITHDRAWAL_REAL,
										SUM(AMOUNT_RELEASED_BONUS) WITHDRAWAL_RELEASED_BONUS
									FROM PAYMENT WITH (NOLOCK )
									WHERE
										TYPE = 'WITHDRAWAL' AND STATUS = 'COMPLETED'
										AND PROCESS_DATE >= @startDate
										AND PROCESS_DATE < @endDate
									GROUP BY ACCOUNT_ID, CAST(PROCESS_DATE AS DATE)
								) WITHDRAWAL
						)

						UNION
						(
							SELECT
								ACCOUNT_ID,
								PROCESS_DATE,
								0 AS PLATFORM_ID,
								0 AS HANDLE_REAL,
								0 AS HANDLE_RELEASED_BONUS,
								0 AS HANDLE_PLAYABLE_BONUS,
								0 AS WIN_REAL,
								0 AS WIN_RELEASED_BONUS,
								0 AS WIN_PLAYABLE_BONUS,
								0 AS CHARGE_BACK,
								0 AS ROLLBACK_REAL,
								0 AS ROLLBACK_RELEASED_BONUS,
								0 AS ROLLBACK_PLAYABLE_BONUS,
								0 AS PAYMENT_FEE,
								0 AS WITHDRAWAL_REAL,
								0 AS WITHDRAWAL_RELEASED_BONUS,
								DEPOSIT.DEPOSIT,
								0 AS BONUS_RELEASED
							FROM
								(
									SELECT
										ACCOUNT_ID,
										CAST(PROCESS_DATE AS DATE) PROCESS_DATE,
										SUM(AMOUNT_REAL)           DEPOSIT
									FROM PAYMENT
									WHERE
										TYPE = 'DEPOSIT' AND STATUS = 'COMPLETED'
										AND PROCESS_DATE >= @startDate
										AND PROCESS_DATE < @endDate
									GROUP BY ACCOUNT_ID, CAST(PROCESS_DATE AS DATE)
								) DEPOSIT
						)
					) userActivity

				GROUP BY ACCOUNT_ID, PLATFORM_ID, PROCESS_DATE)
			userActivity on userActivity.ACCOUNT_ID = ACCOUNT.ID
			LEFT JOIN PLATFORM ON PLATFORM.ID = userActivity.PLATFORM_ID
	end

go
