/****** Object:  StoredProcedure maint.ReplicationSetInitialValue    Script Date: 10/2/2018 11:33:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('maint.usp_SetConnectionPassword') is null
	exec('create procedure maint.usp_SetConnectionPassword as ')
go
alter procedure maint.usp_SetConnectionPassword(@ConnectionID int, @Password nvarchar(128)) with encryption
as
begin
    set nocount on
    update dbo.Connection
		set Password = maint.fn_EncryptPassword(@Password,  cast(ConnectionID as varchar(20)) + '-' + ServerName + '-' + isnull(UserName,'') )
    where ConnectionID = @ConnectionID
end
