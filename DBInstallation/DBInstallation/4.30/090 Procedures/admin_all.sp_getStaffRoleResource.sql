set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getStaffRoleResource]
  (@sessionkey VARCHAR(40))
AS
  BEGIN
    DECLARE @cookie VARCHAR(40)
    SET @cookie = @sessionkey
    SELECT URL
    FROM ROLE_RESOURCE
    WHERE ROLE_ID IN (
      SELECT ROLE_ID
      FROM STAFF_ROLE
      WHERE STAFF_ID = (SELECT STAFFID
                        FROM
                          STAFF_AUTH_TBL
                        WHERE COOKIE = @cookie))
  END

go
