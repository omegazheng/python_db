set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_EnableLoadTest @Enable bit = 0
as
begin
	set nocount, xact_abort on
	
	declare @EnableLoadTest varchar(100),
	@PassKey nvarchar(255), @PassDefault nvarchar(255) = 'test',
	@Threads int, @ThreadDefault int = 200,
	@LoadTestProductPassword nvarchar(255), @LoadTestProductPasswordDefault nvarchar(255) = 'test'
	
	if @Enable = 1
		begin
			set @EnableLoadTest = 'true'
		end
	else set @EnableLoadTest = 'false'
	exec admin_all.usp_SetRegistry 'transaction.loadTest.enabled',  @EnableLoadTest


	select @PassKey = admin_all.fn_GetRegistry('transaction.loadTest.pass')
	if @PassKey is null
		begin
		exec admin_all.usp_SetRegistry 'transaction.loadTest.pass',  @PassDefault
			select @PassKey = admin_all.fn_GetRegistry('transaction.loadTest.pass')
		end

	if not exists (select admin_all.fn_GetRegistry('transaction.loadTest.pass'))
			exec admin_all.usp_SetRegistry 'transaction.loadTest.pass',  @PassDefault
	select @PassKey = admin_all.fn_GetRegistry('transaction.loadTest.pass')


	if not exists (select admin_all.fn_GetRegistry('transaction.loadTest.maxPoolNumber'))
		exec admin_all.usp_SetRegistry 'transaction.loadTest.maxPoolNumber',  @ThreadDefault
	select @Threads = cast(admin_all.fn_GetRegistry('transaction.loadTest.maxPoolNumber') as int)

	if not exists (select admin_all.fn_GetRegistry('omegaWallet.platform.superPassword'))
		exec admin_all.usp_SetRegistry 'omegaWallet.platform.superPassword',  @LoadTestProductPasswordDefault
	select @LoadTestProductPassword = cast(admin_all.fn_GetRegistry('omegaWallet.platform.superPassword') as nvarchar(255))

end

go
