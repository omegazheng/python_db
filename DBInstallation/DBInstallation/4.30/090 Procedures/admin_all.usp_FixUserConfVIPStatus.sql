set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_FixUserConfVIPStatus
(
  @BrandID Integer
)
as
begin
  set nocount, xact_abort on

  update u set u.VIP_STATUS = vip.targetVIPStatusID
  from external_mpt.USER_CONF u join
           (
           select
                  source.ID as sourceVIPStatusID,
                  target.ID as targetVIPStatusID,
                  source.weight
           from
                admin_all.VIP_STATUS source
           join admin_all.VIP_STATUS target on source.weight = target.weight and target.brand_id = @BrandID
           where source.brand_id <> @BrandID
           ) vip on u.VIP_STATUS = vip.sourceVIPStatusID and u.BRANDID = @BrandID
  where u.VIP_STATUS is not null
      and u.VIP_STATUS in (select id from VIP_STATUS where brand_id <> @BrandID)
end
go
