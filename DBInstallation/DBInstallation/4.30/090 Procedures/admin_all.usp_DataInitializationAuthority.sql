set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationAuthority
as
begin
	set nocount on
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.AUTHORITY', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--admin_all.AUTHORITY
	begin transaction
	set identity_insert [admin_all].[AUTHORITY] on;
	;with s as 
	(
		select [ID],[NAME]
		from (
				values (1,N'BROKER')
					,(2,N'PROMOTER')
					,(3,N'AGENT')
					,(4,N'USER')
					,(5,N'TEST_USER')
			) v([ID],[NAME])
	)
	merge admin_all.AUTHORITY t
	using s on s.[ID]= t.[ID]
	when not matched then
		insert ([ID],[NAME])
		values(s.[ID],s.[NAME])
	when matched and (s.[NAME] is null and t.[NAME] is not null or s.[NAME] is not null and t.[NAME] is null or s.[NAME] <> t.[NAME]) then 
		update set  t.[NAME]= s.[NAME]
	;
	set identity_insert [admin_all].[AUTHORITY] off;
	commit;
end
go
