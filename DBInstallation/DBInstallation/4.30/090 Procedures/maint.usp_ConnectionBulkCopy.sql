set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ConnectionBulkCopy
(
	@SourceConnectionID int, @CommandText nvarchar(max), @TargetConnectionID int, 
	@TargetTable nvarchar(256), @Mapping xml = null, @BatchSize int = 1500,
	@UseFieldNameMapping bit = 1, @KeepIdentity bit = 1, @CheckConstraints bit = 1, 
	@TableLock bit = 0,  @KeepNulls bit = 1, @FireTriggers bit = 1
)
as
begin
	declare @ret int, @IsOracle bit, @SourceConnectionString nvarchar(max), @TargetConnectionString nvarchar(max)
	select @SourceConnectionString = maint.fn_GetSQLConnectionStringByID(@SourceConnectionID)
	select @TargetConnectionString = maint.fn_GetSQLConnectionStringByID(@TargetConnectionID)
	exec @ret = maint.usp_BulkCopy	@SourceConnection = @SourceConnectionString, @CommandText = @CommandText, 
								@TargetConnection = @TargetConnectionString, @TargetTable = @TargetTable, @BatchSize = @BatchSize, 
								@Mapping = @Mapping, @UseFieldNameMapping = @UseFieldNameMapping, @KeepIdentity = @KeepIdentity, 
								@CheckConstraints = @CheckConstraints, @TableLock = @TableLock, @KeepNulls = @KeepNulls, @FireTriggers = @FireTriggers
	return @ret
end

go
