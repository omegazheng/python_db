set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SetReplicationHistory
(
	@ConfigurationID int,
	@FromValue sql_variant = null,
	@ToValue sql_variant = null,
	@Rows int = null,
	@StartDate datetime = null,
	@EndDate datetime = null,
	@Error varchar(MAX) = null
	
)
as
begin
	set nocount on
	if not exists(select* from maint.ReplicationLastExecution where ConfigurationID = @ConfigurationID)
	begin
		insert into maint.ReplicationLastExecution(ConfigurationID, StartDate, SessionID, ApplicationName) values(@ConfigurationID, getdate(), @@spid, app_name())
	end
	if @StartDate is not null
	begin
		update maint.ReplicationLastExecution
			set Rows = 0,
				StartDate = @StartDate,
				EndDate = null,
				Error = null,
				SessionID = @@spid,
				ApplicationName = app_name(),
				LoginName = system_user,
				ReplicationHistoryID = null
		where ConfigurationID = @ConfigurationID
	end
	update maint.ReplicationLastExecution
		set FromValue = case when @FromValue is null then FromValue else @FromValue end,
			ToValue = case when @ToValue is null then ToValue else @ToValue end,
			Rows = case when @Rows is null then Rows else @Rows end,
			StartDate = case when @StartDate is null then StartDate else @StartDate end,
			EndDate = case when @EndDate is null then EndDate else @EndDate end,
			Error = case when @Error is null then Error else @Error end,
			SessionID = @@spid,
			ApplicationName = app_name(),
			LoginName = system_user,
			ReplicationHistoryID = null
	where ConfigurationID = @ConfigurationID
	if @EndDate is null -- not completed yet
		return
	declare @ReplicationHistoryID bigint
	if exists(select * from maint.ReplicationLastExecution where ConfigurationID = @ConfigurationID and (Rows >0))
	begin
		insert into maint.ReplicationHistory(ConfigurationID, FromValue, ToValue, Rows, StartDate, EndDate, Error, ApplicationName, LoginName)
			select ConfigurationID, FromValue, ToValue, Rows, StartDate, EndDate, Error, ApplicationName, LoginName
			from maint.ReplicationLastExecution 
			where ConfigurationID = @ConfigurationID 
		select @ReplicationHistoryID = scope_identity()
		update maint.ReplicationLastExecution
			set ReplicationHistoryID = @ReplicationHistoryID
		where ConfigurationID = @ConfigurationID
	end
end

go
