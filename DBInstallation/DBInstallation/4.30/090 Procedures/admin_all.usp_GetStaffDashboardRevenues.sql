set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetStaffDashboardRevenues]
  (
    @staffid INT,
    @brands NVARCHAR(2000),
    @segments NVARCHAR(2000),
    @period NVARCHAR(200),
    @AggregateType tinyint = 0
    )
  AS
  BEGIN
    set nocount on
    SET DATEFIRST 1
    declare @SourceFrom varchar(100)
    select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
    if @SourceFrom is null
      begin
        exec admin_all.usp_SetRegistry 'core.dashboard.source', 'dw'
        select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
      end

    DECLARE @EndDate DATE = cast(getdate() AS DATE)
    DECLARE @StartDate DATE = cast(getdate() AS DATE)

    create table #Brands
    (
      BrandID int primary key
    )
    insert into #Brands(BrandID)
    SELECT distinct BRANDID
    FROM [admin_all].[STAFF_BRAND_TBL]
    WHERE STAFFID = @staffid
      AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands, ','))
    create table #Segments
    (
      SegmentID int primary key
    )
    insert into #Segments(SegmentID)
    select cast(Item as int) segmentId
    from admin_all.fc_splitDelimiterString(@segments, ',')
    IF @period = 'TODAY'
      BEGIN
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = CAST(getdate() AS DATE)
      END
    ELSE
      IF @period = 'YESTERDAY'
        BEGIN
          SET @EndDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
          SET @StartDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
        END
      ELSE
        IF @period = 'WTD'
          BEGIN
            SET @StartDate = CAST(DATEADD(DD, 1 - DATEPART(DW, GETDATE()), GETDATE()) AS DATE)
            --Jim requires the chart always shows 7 days value, zero or not
            SET @EndDate = DATEADD(DAY, 6, @StartDate)
          END
        ELSE
          IF @period = 'MTD'
            BEGIN
              SET @EndDate = CAST(getdate() AS DATE)
              SET @StartDate = cast(dateadd(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AS DATE)
            END
          ELSE
            IF @period = 'LTD'
              BEGIN
                SET @EndDate = CAST(getdate() AS DATE)
                SET @StartDate = cast(DATEADD(YEAR, -100, getdate()) AS DATE)
              END
    if @SourceFrom = 'dw'
      begin
        SELECT @period AS                            PERIOD,
               d.DAY   AS                            SUMMARY_DATE,
               CURRENCY,

               SUM(ISNULL(PNL, 0))                   PNL,
               SUM(ISNULL(HANDLE, 0))                HANDLE,
               CASE
                 WHEN SUM(ISNULL(HANDLE, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(PNL) / SUM(HANDLE) * 100, 2)
                 END   AS                            HOLD,

               SUM(ISNULL(PNL_REAL, 0))              PNL_REAL,
               SUM(ISNULL(HANDLE_REAL, 0))           HANDLE_REAL,
               CASE
                 WHEN SUM(ISNULL(HANDLE_REAL, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(PNL_REAL) / SUM(HANDLE_REAL) * 100, 2)
                 END   AS                            HOLD_REAL,

               SUM(ISNULL(PNL_RELEASED_BONUS, 0))    PNL_RELEASED_BONUS,
               SUM(ISNULL(HANDLE_RELEASED_BONUS, 0)) HANDLE_RELEASED_BONUS,
               CASE
                 WHEN SUM(ISNULL(HANDLE_RELEASED_BONUS, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(PNL_RELEASED_BONUS) / SUM(HANDLE_RELEASED_BONUS) * 100, 2)
                 END   AS                            HOLD_RELEASED_BONUS,

               SUM(ISNULL(PNL_PLAYABLE_BONUS, 0))    PNL_PLAYABLE_BONUS,
               SUM(ISNULL(HANDLE_PLAYABLE_BONUS, 0)) HANDLE_PLAYABLE_BONUS,
               CASE
                 WHEN SUM(ISNULL(HANDLE_PLAYABLE_BONUS, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(PNL_PLAYABLE_BONUS) / SUM(HANDLE_PLAYABLE_BONUS) * 100, 2)
                 END   AS                            HOLD_PLAYABLE_BONUS
        FROM admin_all.DW_ALL_DAYS d
               LEFT JOIN admin_all.DW_GAME_PLAYER_DAILY dw
               inner join admin_all.platform p
                          ON dw.PLATFORM_ID = p.Id AND p.segment_id IN (SELECT x.SegmentID from #Segments x)
                          ON dw.SUMMARY_DATE = d.DAY AND dw.BRAND_ID IN (SELECT x.BRANDID from #Brands x)
        WHERE d.DAY >= @StartDate
          AND d.DAY <= @EndDate
          AND (@period = 'WTD' OR CURRENCY is not null)
        GROUP BY d.DAY, CURRENCY
        ORDER BY d.DAY
        return
      end

    else
      BEGIN
        SELECT @period AS                             PERIOD,
               d.DAY   AS                             SUMMARY_DATE,
               CURRENCY,

               SUM(ISNULL(dw.PNL, 0))                 PNL,
               SUM(ISNULL(dw.Handle, 0))              HANDLE,
               CASE
                 WHEN SUM(ISNULL(dw.Handle, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(dw.PNL) / SUM(HANDLE) * 100, 2)
                 END   AS                             HOLD,

               SUM(ISNULL(dw.PNLReal, 0))             PNL_REAL,
               SUM(ISNULL(dw.HandleReal, 0))          HANDLE_REAL,
               CASE
                 WHEN SUM(ISNULL(dw.HandleReal, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(dw.PNLReal) / SUM(dw.HandleReal) * 100, 2)
                 END   AS                             HOLD_REAL,

               SUM(ISNULL(dw.PNLReleasedBonus, 0))    PNL_RELEASED_BONUS,
               SUM(ISNULL(dw.HandleReleasedBonus, 0)) HANDLE_RELEASED_BONUS,
               CASE
                 WHEN SUM(ISNULL(dw.HandleReleasedBonus, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(dw.PNLReleasedBonus) / SUM(dw.HandleReleasedBonus) * 100, 2)
                 END   AS                             HOLD_RELEASED_BONUS,

               SUM(ISNULL(dw.PNLPlayableBonus, 0))    PNL_PLAYABLE_BONUS,
               SUM(ISNULL(dw.HandleReleasedBonus, 0)) HANDLE_PLAYABLE_BONUS,
               CASE
                 WHEN SUM(ISNULL(dw.HandleReleasedBonus, 0)) = 0
                   THEN 0
                 ELSE ROUND(SUM(dw.PNLPlayableBonus) / SUM(dw.HandleReleasedBonus) * 100, 2)
                 END   AS                             HOLD_PLAYABLE_BONUS
        FROM admin_all.DW_ALL_DAYS d
               LEFT JOIN admin_all.fn_GetPlayerDailyAggregate(@AggregateType, @StartDate, dateadd(day, 1, @EndDate)) dw
               inner join admin_all.platform p
                          ON dw.ProductID = p.Id AND p.segment_id IN (SELECT x.SegmentID from #Segments x)
                          ON dw.Date = d.DAY AND dw.BrandID IN (SELECT x.BRANDID from #Brands x)
        WHERE d.DAY >= @StartDate
          AND d.DAY <= @EndDate
          AND (@period = 'WTD' OR CURRENCY is not null)
        GROUP BY d.DAY, CURRENCY
        ORDER BY d.DAY
      END
  END

go
