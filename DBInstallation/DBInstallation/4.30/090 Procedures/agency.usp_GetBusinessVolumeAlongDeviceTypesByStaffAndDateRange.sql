set ansi_nulls, quoted_identifier on
go
create or alter procedure [agency].[usp_GetBusinessVolumeAlongDeviceTypesByStaffAndDateRange]
  (@segmentIds VARCHAR(8000) = NULL, @brandIds VARCHAR(8000) = NULL, @period nvarchar(200))
AS
  BEGIN
	set nocount on
  SET DATEFIRST 1
	  -- get SourceForm
    declare @SourceFrom varchar(100)
    select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
    if @SourceFrom is null
    begin
      exec admin_all.usp_SetRegistry 'core.dashboard.source', 'dw'
      select @SourceFrom = admin_all.fn_GetRegistry('core.dashboard.source')
    end

    DECLARE @brandIdsLocal VARCHAR(8000)
    SET @brandIdsLocal = @brandIds
    DECLARE @segmentIdsLocal VARCHAR(8000)
    SET @segmentIdsLocal = @segmentIds;

    declare @EndDate date = cast (getdate() as  date)
    declare @StartDate date = cast (getdate() as  date)
	create table #Brands (BrandID int primary key)
	insert into #Brands(BrandID)
		select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brandIds,',')
	create table #Segments(SegmentID int primary key)
	insert into #Segments(SegmentID)
		select cast(Item as int) segmentId from admin_all.fc_splitDelimiterString(@segmentids,',')

    if @period = 'TODAY'
      begin
        set @EndDate = CAST(getdate() AS DATE)
        set @StartDate = CAST(getdate() AS DATE)
      end
    else if @period = 'YESTERDAY'
      begin
        SET @EndDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
        SET @StartDate = DATEADD(DAY, -1, CAST(getdate() AS DATE))
      end
    else if @period = 'WTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = CAST(DATEADD(DD, 1 - DATEPART(DW, GETDATE()), GETDATE()) AS DATE)
      end
    else if @period = 'MTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
        SET @StartDate = cast(dateadd(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) AS DATE)
      end
    else if @period = 'LTD'
      begin
        SET @EndDate = CAST(getdate() AS DATE)
                SET @StartDate = cast(DATEADD(YEAR, -100, getdate()) AS DATE)
      end

	if @SourceFrom = 'dw'
  	BEGIN
      select
          sum(
              case
                  when is_touch = 0 then handle
                  else 0
              end
          )
          as desktopVolume,
          sum(
              case
                  when is_touch = 1 then handle
                  else 0
              end
          )
          as mobileVolume
      from admin_all.dw_game_player_daily
          join admin_all.game_info on game_info.game_id = dw_game_player_daily.game_id
          join admin_all.platform as pl on pl.id = dw_game_player_daily.PLATFORM_ID
          join external_mpt.user_conf as player on player.partyid = DW_GAME_PLAYER_DAILY.PARTY_ID

      where summary_date >= @StartDate and summary_date <= @EndDate
          --and party_id in (select partyid from @subagents)
      and player.User_Type = 0
          and (player.BRANDID IN (SELECT x.BrandID from #Brands x ))
          and (pl.segment_id IN (SELECT x.SegmentID from #Segments x))
    END -- if SourceForm = 'dw'
  ELSE
    BEGIN
          select
          sum(
              case
                  when is_touch = 0 then
                    CASE
                      WHEN ((ath.TranType = 'GAME_PLAY' AND ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus < 0) OR
                            ath.TranType in ('GAME_BET','STAKE_DEC', 'REFUND'))
                        THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
                      ELSE 0
                    END
                  else 0
              end
          )
          as desktopVolume,
          sum(
              case
                  when is_touch = 1 then
                    CASE
                      WHEN ((ath.TranType = 'GAME_PLAY' AND ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus < 0) OR
                            ath.TranType in ('GAME_BET','STAKE_DEC', 'REFUND'))
                        THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
                      ELSE 0
                    END
                  else 0
              end
          )
          as mobileVolume
      from admin_all.AccountTranHourlyAggregate ath
          join admin_all.game_info as g on g.game_id = ath.gameId
          join admin_all.platform as pl on pl.id = g.PLATFORM_ID
          join admin_all.ACCOUNT as a on a.PARTYID = ath.PartyID
          join external_mpt.user_conf as player on player.partyid = a.PARTYID
      where ath.Datetime >= @StartDate and ath.Datetime < DATEADD(DAY, 1, @EndDate)
          --and party_id in (select partyid from @subagents)
          and player.User_Type = 0
          and (player.BRANDID IN (SELECT x.BrandID from #Brands x ))
          and (pl.segment_id IN (SELECT x.SegmentID from #Segments x))
    END -- if SourceForm = 'ath'
  END

go
