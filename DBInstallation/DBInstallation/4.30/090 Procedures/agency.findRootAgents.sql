set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findRootAgents]
AS
  BEGIN
    select partyid as id from external_mpt.user_conf where parentid is null and user_type=1
  END

go
