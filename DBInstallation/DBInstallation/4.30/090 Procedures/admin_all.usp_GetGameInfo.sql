set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetGameInfo]
  (
    @brandID INT
  )
AS
  BEGIN

    select
           g.ID as GameInfoID,
           p.ID as ProductID,
           g.name as GameName,
           p.code as ProductCode,
           gc.ID as GameCategoryID,
           gc.NAME as GameCategoryName,
           g.GAME_ID as GameID,
           g.GAME_LAUNCH_ID as GameLaunchID,
           g.IS_TOUCH as SupportMobile,
           g.IS_FREESPIN_ENABLE FreeSpinEnabled,
           sp.ID as SubProductID,
           sp.CODE as SubProductCode,
           s.ID as SegmentID,
           s.name as SegmentName
    from GAME_INFO g
           join platform p on g.PLATFORM_ID = p.ID
           join BRAND_PLATFORM bp on bp .PLATFORM_ID = p.ID and bp.IS_ENABLED=1 and bp.BRAND_ID=@brandID
           left join GAME_CATEGORY gc on g.GAME_CATEGORY_ID = gc.ID
           left join segment s on p.segment_id = s.id
           left join SUB_PLATFORM sp on g.SUB_PLATFORM_ID = sp.ID

  END

go
