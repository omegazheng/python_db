set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_InsertFreePlayTran]
  (
    @PARTY_ID int,
    @TRAN_TYPE varchar(10),
    @DATETIME datetime,
    @AMOUNT numeric(38,18),
    @PROVIDER_ID int = null,
    @PROVIDER_TRAN_ID nvarchar(100),
    @GAME_TRAN_ID nvarchar(100),
    @GAME_INFO_ID int,
    @REMAINING_BALANCE numeric(38,18)
  )
as
  begin
    DECLARE @ID bigint

    insert into admin_all.FREEPLAY_TRAN
    (
      PARTY_ID,
      TRAN_TYPE,
      DATE_TIME,
      AMOUNT,
      PROVIDER_ID,
      PROVIDER_TRAN_ID,
      GAME_TRAN_ID,
      GAME_INFO_ID,
      REMAINING_BALANCE
    )
    values
      (
        @PARTY_ID,
        @TRAN_TYPE,
        @DATETIME,
        @AMOUNT,
        @PROVIDER_ID,
        @PROVIDER_TRAN_ID,
        @GAME_TRAN_ID,
        @GAME_INFO_ID,
        @REMAINING_BALANCE
      )

    select @ID = @@IDENTITY
    select  id as id,
            party_id as partyId,
            date_time as dateTime,
            tran_type as tranType,
            isnull(amount, 0) as amount,
            provider_id as providerId,
            PROVIDER_TRAN_ID as providerTranId,
            game_tran_id as gameTranId,
            GAME_INFO_ID as gameInfoId,
            isnull(REMAINING_BALANCE, 0) as remainingBalance
    from admin_all.FREEPLAY_TRAN where ID = @ID

  end

go
