set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[commissionReportRevenue]
  (@agent_id INT, @start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
    SELECT
      partyid             partyid,
      userid              userid,
      userType            usertype,
      product_id          product_id,
      platform_code       platform_code,
      sum(bets)           bets,
      sum(wins)           wins,
      sum(released_bonus) released_bonus,
      sum(ngr)            ngr
    FROM (
           SELECT
             users.partyid         partyid,
             users.userid          userid,
             users.user_type       usertype,
             product_id            product_id,
             platform.code         platform_code,
             sum(bets)             bets,
             sum(wins)             wins,
             sum(released_bonuses) released_bonus,
             sum(ngr)              ngr
           FROM
             (
               SELECT
                 agent_id,
                 product_id,
                 bets,
                 wins,
                 released_bonuses,
                 ngr
               FROM (
                      SELECT
                        sum(isnull(bets, 0))                  bets,
                        -1 * sum(isnull(wins, 0))             wins,
                        -1 * sum(isnull(released_bonuses, 0)) released_bonuses,
                        sum(ngr)                              ngr,
                        platform.id                           product_id,
                        agent_id
                      FROM
                        (
                          SELECT
                            sum(isnull(bets, 0))             bets,
                            sum(isnull(wins, 0))             wins,
                            sum(isnull(released_bonuses, 0)) released_bonuses,
                            sum(value)                       ngr,
                            product_id,
                            agent_id
                          FROM
                            agency.direct_ggr
                          WHERE date >= @start_date AND date <= @end_date
                          GROUP BY agent_id, product_id

                          UNION

                          SELECT
                            sum(isnull(bets, 0))             bets,
                            sum(isnull(wins, 0))             wins,
                            sum(isnull(released_bonuses, 0)) released_bonuses,
                            sum(value)                       ngr,
                            product_id,
                            agent_id
                          FROM
                            agency.network_ggr
                          WHERE date >= @start_date AND date <= @end_date
                          GROUP BY agent_id, product_id
                        ) all_ngr
                        -- keep only sports betting ggr
                        JOIN admin_all.platform ON platform.id = all_ngr.product_id
                        -- keep only @agent_id's direct children in context
                        JOIN external_mpt.user_conf agent ON agent.partyid = all_ngr.agent_id
                      WHERE
                        --                  platform.platform_type = @platform_type AND
                        agent.parentid = @agent_id
                      GROUP BY
                        platform.id,
                        agent_id
                    ) agent_ngr_by_product
             )
             agent_summary
             JOIN external_mpt.user_conf users ON users.partyid = agent_summary.agent_id
             JOIN admin_all.platform ON agent_summary.product_id = platform.id
           GROUP BY users.partyid, users.userid, users.user_type, product_id, platform.code

           UNION

           SELECT
             users.partyId,
             users.userId,
             users.user_type usertype,
             product_id,
             platform.code   platform_code,
             bets,
             wins,
             released_bonuses,
             ngr
           FROM (
                  SELECT
                    sum(ISNULL(bets, 0))                  bets,
                    -1 * sum(ISNULL(wins, 0))             wins,
                    -1 * sum(ISNULL(released_bonuses, 0)) released_bonuses,
                    sum(ngr)                              ngr,
                    platform.id                           product_id,
                    player_id
                  FROM
                    (
                      SELECT
                        sum(ISNULL(bets, 0))             bets,
                        sum(ISNULL(wins, 0))             wins,
                        sum(ISNULL(released_bonuses, 0)) released_bonuses,
                        sum(value)                       ngr,
                        product_id,
                        player_id
                      FROM
                        agency.player_ggr
                      WHERE date >= @start_date AND date <= @end_date AND agent_id = @agent_id
                      GROUP BY player_id, product_id
                    ) player_ngr
                    -- keep only sports betting ggr
                    JOIN admin_all.platform ON platform.id = player_ngr.product_id
                  --            WHERE platform.platform_type = @platform_type
                  GROUP BY
                    platform.id,
                    player_id
                ) player_ngr_by_product
             JOIN external_mpt.user_conf users ON users.partyId = player_ngr_by_product.player_id
             JOIN admin_all.platform ON platform.id = player_ngr_by_product.product_id
         ) uni
    GROUP BY uni.partyid, uni.userid, uni.usertype, uni.product_id, uni.platform_code
  END

go
