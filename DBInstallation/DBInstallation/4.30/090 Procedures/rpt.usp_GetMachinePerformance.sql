set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetMachinePerformance
(
	@StaffID int,
	@DateFrom datetime,
	@DateTo datetime,
	@CompanyID int,
	@MachineID int = null
)
as
begin
	set nocount on
	if isnull(@MachineID, @CompanyID) is null
	begin
		raiserror('@CompanyID or/and @MachineID should be provided.', 16, 1)
		return
	end
	--select @DateFrom = cast(@DateFrom as date), @DateTo = dateadd(day, 1, cast(@DateTo as date))
	declare @StaffType varchar(10) = admin_all.fn_StaffType(@StaffID)
	declare @SQL nvarchar(max)
	select @SQL = '
		select  ms.MachineID, 
				sum(ms.AmountReal) AmountReal, sum(ms.PlayableBonus) PlayableBonus, sum(ms.GamesPlayed) GamesPlayed, 
				sum(ms.TotalCreditWin) TotalCreditWin, sum(ms.GameWin) GameWin, sum(ms.AmountTransferIn) AmountTransferIn, 
				sum(ms.AmountTransferOut) AmountTransferOut, sum(ms.BonusTransferIn) BonusTransferIn, 
				sum(ms.BonusTransferOut) BonusTransferOut, sum(ms.LoyaltyPoint) LoyaltyPoint,
				Count(*) Count
		from admin_all.MachinePlayGameSummary ms
			inner join admin_all.Machine m on m.MachineID = ms.MachineID
			left join admin_all.Location l on l.LocationID = m.LocationID
			left join admin_all.PLATFORM p on p.id = m.ProductID
		where ms.Datetime >= @DateFrom and ms.Datetime < @DateTo
			and ' +case 
					when @CompanyID is not null then 'l.CompanyID = @CompanyID '
					when @MachineID is not null then 'm.MachineID = @MachineID '
					when @CompanyID is not null  and @MachineID is not null  then 'l.CompanyID = @CompanyID and m.MachineID = @MachineID '
				end +'
			'+case when @StaffType = 'Company' then 'and exists(
						select * 
						from admin_all.StaffCompany sc
						where CompanyID = @CompanyID
							and sc.StaffID = @StaffID
					)' 
					when @StaffType = 'Product' then '
			and exists(
					select *
					from admin_all.StaffProduct sp
					where sp.StaffID = @StaffID
						and sp.ProductID = m.ProductID
						
					)'
					else ''
			end+'
		group by ms.MachineID'
	exec sp_executesql @SQL, N'@StaffID int, @CompanyID int, @MachineID int, @DateFrom datetime, @DateTo datetime', @StaffID, @CompanyID, @MachineID, @DateFrom, @DateTo
	
end

go
