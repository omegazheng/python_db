set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_IpsGetTransactionsHistoryByCurrency]
  (
    @accountId INT,
    @startDate VARCHAR(255),
    @endDate   VARCHAR(255),
    @type  VARCHAR(255),
    @tranTypes  VARCHAR(255),
    @pageNum   INT,
    @pageSize  INT,
    @orderBy VARCHAR(255)
  )
AS
  BEGIN

    DECLARE @count INT = 0;

    create table #TranType (Type nvarchar(100) primary key)
    insert into #TranType(Type)
    select cast(rtrim(ltrim(Item)) as nvarchar(100)) as Type from admin_all.fc_splitDelimiterString(@tranTypes,',')


    SET @count = (
      SELECT COUNT(*)
        FROM ACCOUNT_TRAN a WITH (NOLOCK)
        LEFT JOIN PLATFORM p WITH (NOLOCK) ON p.id = a.platform_id
        LEFT JOIN GAME_INFO g WITH (NOLOCK) ON a.GAME_ID = g.GAME_ID AND a.platform_id = g.PLATFORM_ID
        WHERE a.ACCOUNT_ID = @accountId
        AND a.datetime >= @startDate
        AND a.datetime < @endDate
                AND a.TRAN_TYPE LIKE @type
          and a.TRAN_TYPE IN (select x.Type from #TranType x)
--         or a.TRAN_TYPE LIKE @type

    )

    SELECT
      @count as totalRecords,
      a.ID,
      a.DATETIME                                                    AS dateTime,
      a.TRAN_TYPE                                                   AS tranType,
      p.code                                                        AS platformCode,
      g.game_launch_id                                              AS gameLaunchId,
      g.name                                                        AS gameName,
      ISNULL(a.amount_real, 0) + ISNULL(a.AMOUNT_RELEASED_BONUS, 0) +
      ISNULL(a.AMOUNT_PLAYABLE_BONUS, 0)                            AS amount,
      ISNULL(a.amount_real, 0) + ISNULL(a.AMOUNT_RELEASED_BONUS, 0) AS amountReal,
      ISNULL(a.AMOUNT_PLAYABLE_BONUS, 0)                            AS amountBonus,
      ISNULL(a.balance_real, 0) + ISNULL(a.amount_released_bonus, 0) +
      ISNULL(a.balance_playable_bonus, 0)                           AS postBalance,
      ISNULL(a.balance_real, 0) + ISNULL(a.amount_released_bonus,
                                         0)                         AS postBalanceReal,
      ISNULL(a.balance_playable_bonus,
             0)                                                     AS postBalanceBonus,
      a.GAME_TRAN_ID                                                AS gameTranId
    INTO #Temp
    FROM ACCOUNT_TRAN a WITH (NOLOCK)
      LEFT JOIN PLATFORM p WITH (NOLOCK) ON p.id = a.platform_id
      LEFT JOIN GAME_INFO g WITH (NOLOCK) ON a.GAME_ID = g.GAME_ID AND a.platform_id = g.PLATFORM_ID
    WHERE a.ACCOUNT_ID = @accountId
          AND a.datetime >= @startDate
          AND a.datetime < @endDate
--           AND a.TRAN_TYPE IN (@tranTypes)
            AND a.TRAN_TYPE LIKE @type
      and a.TRAN_TYPE IN (select x.Type from #TranType x)



    SELECT * FROM (  SELECT
                      CASE
                      WHEN @OrderBy = 'DATETIMEASC' THEN ROW_NUMBER() OVER (ORDER BY dateTime)
                      WHEN @OrderBy = 'DATETIMEDESC' THEN ROW_NUMBER() OVER (ORDER BY dateTime DESC)
                      WHEN @OrderBy = 'TRANTYPEASC' THEN ROW_NUMBER() OVER (ORDER BY tranType)
                      WHEN @OrderBy = 'TRANTYPEDESC' THEN ROW_NUMBER() OVER (ORDER BY tranType DESC)
                      WHEN @OrderBy = 'GAMENAMEASC' THEN ROW_NUMBER() OVER (ORDER BY gameName)
                      WHEN @OrderBy = 'GAMENAMEDESC' THEN ROW_NUMBER() OVER (ORDER BY gameName DESC)
                      WHEN @OrderBy = 'AMOUNTASC' THEN ROW_NUMBER() OVER (ORDER BY amount)
                      WHEN @OrderBy = 'AMOUNTDESC' THEN ROW_NUMBER() OVER (ORDER BY amount DESC)
                      WHEN @OrderBy = 'AMOUNTREALASC' THEN ROW_NUMBER() OVER (ORDER BY amountReal)
                      WHEN @OrderBy = 'AMOUNTREALDESC' THEN ROW_NUMBER() OVER (ORDER BY amountReal DESC)
                      WHEN @OrderBy = 'AMOUNTBONUSASC' THEN ROW_NUMBER() OVER (ORDER BY amountBonus)
                      WHEN @OrderBy = 'AMOUNTBONUSDESC' THEN ROW_NUMBER() OVER (ORDER BY amountBonus DESC)
                      WHEN @OrderBy = 'POSTBALANCEASC' THEN ROW_NUMBER() OVER (ORDER BY postBalance)
                      WHEN @OrderBy = 'POSTBALANCEDESC' THEN ROW_NUMBER() OVER (ORDER BY postBalance DESC)
                      WHEN @OrderBy = 'POSTBALANCEREALASC' THEN ROW_NUMBER() OVER (ORDER BY postBalanceReal)
                      WHEN @OrderBy = 'POSTBALANCEREALDESC' THEN ROW_NUMBER() OVER (ORDER BY postBalanceReal DESC)
                      WHEN @OrderBy = 'POSTBALANCEBONUSASC' THEN ROW_NUMBER() OVER (ORDER BY postBalanceBonus)
                      WHEN @OrderBy = 'POSTBALANCEBONUSDESC' THEN ROW_NUMBER() OVER (ORDER BY postBalanceBonus DESC)
                      WHEN @OrderBy = 'GAMETRANIDASC' THEN ROW_NUMBER() OVER (ORDER BY gameTranId)
                      WHEN @OrderBy = 'GAMETRANIDDESC' THEN ROW_NUMBER() OVER (ORDER BY gameTranId DESC)

                      END
                        AS RowNum,*
                    FROM #Temp
                  ) as alias
    WHERE RowNum BETWEEN @PageSize * (@PageNum - 1) + 1 AND @PageSize * @PageNum
    ORDER BY RowNum

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END


  END

go
