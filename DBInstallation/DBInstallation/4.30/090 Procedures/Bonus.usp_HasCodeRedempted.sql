set ansi_nulls, quoted_identifier on
go

create or alter procedure Bonus.usp_HasCodeRedempted
	(
		@PartyID int,
		@Code nvarchar(255)
	)
as
	begin
		Declare @Redempted bit = 0

		if exists (
				select * from
					admin_all.bonus b
					join admin_all.bonus_plan bp
						on bp.id  = b.BONUS_PLAN_ID
				where bp.BONUS_CODE = @Code and b.PARTYID = @PartyID
		)
			begin
				set @Redempted = 1
				select @Redempted Redempted
				return
			end

		if exists (
				select
					bc.ID
				from admin_all.BONUS_COUPON bc
					join admin_all.bonus b
						on bc.BONUS_ID = b.ID where bc.IS_USED = 1 and bc.COUPON = @Code and b.PARTYID = @PartyID
		)
			begin
				set @Redempted = 1
			end

		select @Redempted Redempted

	end


go
