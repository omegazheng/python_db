set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_GetAgentPlayerTransactionPage
 (
	@agentid     INT,
	@startDate   DATETIME,
	@endDate     DATETIME,
	@userid      VARCHAR(255) = NULL,
	@platformIds VARCHAR(255) = NULL,
	@pageNum     INT,
	@pageSize    INT
)
AS
BEGIN
	set nocount on
    DECLARE @agentidLocal INT
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @useridLocal VARCHAR(255)
    DECLARE @platformIdsLocal VARCHAR(255)
    SET @agentidLocal = @agentid
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate)
    SET @useridLocal = @userid
    SET @platformIdsLocal = @platformIds;
	declare @SQL nvarchar(max)
	create table #AccountTran(RowNum int primary key, AccountTranID bigint)
	create table #BIA_SportBook(ProductID int primary key)
	create table #ProductID(ProductID int primary key)
	create table #Users(AccountID int primary key)

	insert into #BIA_SportBook(ProductID)
	select platform.id
    from admin_all.PLATFORM platform
		inner join admin_all.platform provider on platform.provider_id = provider.id and provider.code = 'BIA_SPORTBOOK'

	insert into #Users(AccountID)
	select a.id
	from external_mpt.USER_CONF u
		inner join admin_all.ACCOUNT a on u.PARTYID = a.PARTYID
	where u.ParentID = @agentidLocal
		and u.user_type = 0
		and (@useridLocal IS NULL OR u.USERID LIKE '%' + @useridLocal + '%')

	if @platformIdsLocal is not null
	begin
		insert into #ProductID(ProductID)
			SELECT distinct Item
			FROM [admin_all].fc_splitDelimiterString(@platformIdsLocal, ',')
	end
	select @SQL = '; with CTE as(
		SELECT
          ROW_NUMBER() OVER ( ORDER BY t.datetime DESC )     AS RowNum,
		  t.ID
		from admin_all.ACCOUNT_TRAN t
			JOIN #Users u ON u.AccountID = t.ACCOUNT_ID
		where not exists(select * from #BIA_SportBook p1 where p1.ProductID = t.PLATFORM_ID)
			AND t.DATETIME >= @startDateLocal AND t.DATETIME < @endDateLocal
		'+case when exists(select * from #ProductID) then '	and exists(select * from #ProductID p2 where p2.ProductID = t.PLATFORM_ID)' else '' end+'
	)
	SELECT RowNum, ID
    FROM CTE
    WHERE
      (RowNum > @PageSize * (@PageNum - 1))
      AND
      (RowNum <= @PageSize * @PageNum)
	'
	
	insert into #AccountTran(RowNum, AccountTranID)
		exec sp_executesql @SQL, N'@startDateLocal datetime, @endDateLocal datetime, @PageSize int, @PageNum int', @startDateLocal, @endDateLocal, @PageSize, @PageNum
	
    ;WITH CTE AS (
        SELECT
          t0.RowNum,
          u.PARTYID,
          u.USERID,
          u.FIRST_NAME + ' ' + u.LAST_NAME AS NAME,
          u.CURRENCY,
          p.CODE                           AS PLATFORM_CODE,
          t.*
        FROM #AccountTran t0
			inner loop join admin_all.ACCOUNT_TRAN t on t0.AccountTranID = t.ID
			inner loop JOIN admin_all.ACCOUNT a ON t.ACCOUNT_ID = a.id
			inner loop JOIN external_mpt.USER_CONF u ON a.PARTYID = u.PARTYID
			inner loop JOIN admin_all.PLATFORM p ON t.PLATFORM_ID = p.ID
    
    )
    SELECT *
    FROM CTE
    ORDER BY RowNum
  END
go
