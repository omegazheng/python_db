set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[usp_GetBusinessVolumeAlongDeviceTypesByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME)
AS
  BEGIN
    declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);
    declare @subagents TABLE(partyid int)

    insert into @subagents
        select
            partyid
        from external_mpt.user_conf as player
        where
            player.partyid <> @agent_id
            and player.user_type = 0
            and player.hierarchy.IsDescendantOf(@agentHierarchy) = 1

    select
        sum(
            case
                when is_touch = 0 then handle
                else 0
            end
        )
        as desktopVolume,
        sum(
            case
                when is_touch = 1 then handle
                else 0
            end
        )
        as mobileVolume
    from admin_all.dw_game_player_daily
        join admin_all.game_info on game_info.game_id = dw_game_player_daily.game_id
    where summary_date >= @start_date and summary_date < @end_date
        and party_id in (select partyid from @subagents)


  END

go
