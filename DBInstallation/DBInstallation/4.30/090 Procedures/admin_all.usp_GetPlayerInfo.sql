set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_GetPlayerInfo]
    (@partyid int)
AS
  BEGIN
    select
    	userid,
    	nickname,
    	user_conf.partyid,
    	first_name + ' ' + last_name as name,
    	brand.brandname as brand,
    	city + ', ' + country + ' ' as location,
    	currency.name as currencyName,
    	currency.iso_code as currencyCode,
    	account.balance_real + account.released_bonus as accountBalance,
    	account.balance_real + account.released_bonus - account.playable_bonus as withdrawableBalance,
    	account.playable_bonus as playableBonusBalance,
    	email as email,
    	reg_date as regDate,
    	active_flag as accountStatus,
    	kyc_status as kycStatus
    from external_mpt.user_conf
    	join admin.casino_brand_def as brand on brand.brandid = user_conf.brandid
    	join admin_all.currency as currency on currency.iso_code = user_conf.currency
    	join admin_all.account on account.partyid = user_conf.partyid
    where user_conf.partyid = @partyid

  END

go
