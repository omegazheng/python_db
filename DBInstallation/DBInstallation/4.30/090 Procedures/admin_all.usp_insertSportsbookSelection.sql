set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_insertSportsbookSelection
(
    @BetDetailsId int,
    @Selection varchar(2000),
    @SelectionId nvarchar(50),
    @Odds numeric(38, 18),

    @Market nvarchar(300),
    @Period varchar(100),
    @PeriodDescription nvarchar(300),

    @SportName nvarchar(100),
    @Event varchar(2000),
    @EventPaths nvarchar(300),
    @EventStartTimestamp datetime,

    @Match nvarchar(100),
    @MatchInfo nvarchar(100),
    @OutcomeId nvarchar(100),
    @OutcomeDescription nvarchar(300),

    @Status nvarchar(20)
)
AS

BEGIN
    insert into admin_all.SPORTSBOOK_SELECTION
        (
        BET_DETAILS_ID, SELECTION, ODDS, MARKET, PERIOD,
        EVENT, OUTCOME_ID, OUTCOME_DESCRIPTION, PERIOD_DESCRIPTION, EVENT_START_TIMESTAMP, EVENT_PATHS,
        SELECTION_ID, STATUS, MATCH, MATCH_INFO, SPORT_NAME
        )
    values
        (
        @BetDetailsId, @Selection, @Odds, @Market, @Period,
        @Event, @OutcomeId, @OutcomeDescription, @PeriodDescription, @EventStartTimestamp, @EventPaths,
        @SelectionId, @Status, @Match, @MatchInfo, @SportName
        )
END


go
