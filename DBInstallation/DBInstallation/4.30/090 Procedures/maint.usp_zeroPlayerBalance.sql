set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_zeroPlayerBalance
  as
  begin
    set xact_abort, nocount on

    declare @BrandID int, @PartyID int, @AmountReal numeric(38,18), @AmountReleasedBonus numeric(38,18), @AmountPlayableBonus numeric(38,18)    declare c cursor local local dynamic for
      Select
        u.BrandID, a.PartyID, -a.BALANCE_REAL, -a.RELEASED_BONUS, -a.PLAYABLE_BONUS
      from maint.AccountBalanceClear players
             inner join admin_all.ACCOUNT a on a.PARTYID = players.PartyID
             inner join external_mpt.user_conf u on a.PARTYID = u.PARTYID
      where updated = 0 order by 1
    open c
    fetch next from c into @BrandID, @PartyID, @AmountReal,@AmountReleasedBonus,@AmountPlayableBonus
    while @@FETCH_STATUS = 0
    begin
      begin transaction

--
--         exec admin_all.usp_UpdateAccountInternal @PartyID = @PartyID, @AmountReal = @AmountReal, @ReleasedBonus = @AmountReleasedBonus, @PlayableBonus = @AmountPlayableBonus,
--              @AmountSecondary = 0, @TranType='MAN_ADJUST', @PlatformID=null, @PlatformTranID=null,
--              @GameTranID=null, @GameID=null, @PaymentID =null, @AmountRawLoyalty=0, @Reference=null, @Cit=0,
--              @DateTime=null, @UpdateBonuses=1, @MachineID=null, @AccountTranID= null

        insert into admin_all.ACCOUNT_TRAN(
        					ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, ROLLED_BACK, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS, AMOUNT_UNDERFLOW, AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY, BRAND_ID)
        	select ID ACCOUNT_ID, getdate() DATETIME, 'MAN_ADJUST' TRAN_TYPE, - BALANCE_REAL AMOUNT_REAL, 0 BALANCE_REAL, 0 ROLLED_BACK, -RELEASED_BONUS AMOUNT_RELEASED_BONUS, -PLAYABLE_BONUS AMOUNT_PLAYABLE_BONUS, 0 BALANCE_RELEASED_BONUS, 0 BALANCE_PLAYABLE_BONUS, 0 AMOUNT_UNDERFLOW, -RAW_LOYALTY_POINTS AMOUNT_RAW_LOYALTY, 0 BALANCE_RAW_LOYALTY, @BrandID
        	from admin_all.ACCOUNT
        	where PARTYID = @PartyID
        update a
        	set BALANCE_REAL = 0, RELEASED_BONUS = 0 , PLAYABLE_BONUS = 0
        from admin_all.ACCOUNT a
        	where PARTYID = @PartyID

        update admin_all.bonus
        	set RELEASED_BONUS = 0, RELEASED_BONUS_WINNINGS = 0, PLAYABLE_BONUS = 0, PLAYABLE_BONUS_WINNINGS = 0
        where PARTYID = @PartyID

        update maint.AccountBalanceClear set updated = 1, datetime = getdate() where PartyID = @PartyID
      commit
      fetch next from c into @BrandID, @PartyID, @AmountReal,@AmountReleasedBonus,@AmountPlayableBonus
    end
    close c
    deallocate c
  end

go
