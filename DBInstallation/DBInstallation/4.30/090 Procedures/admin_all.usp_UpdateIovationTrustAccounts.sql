set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_UpdateIovationTrustAccounts
	(
		@IvoationFirstAndLastCheckIntervalInDays int = 60,
		@IvoationNumberOfDaysCheckThreashold int = 10
	)
as
	begin

		set nocount on
		set xact_abort on

		declare @IovationTrusted table
		([PartyID] int)
		declare @Comment nvarchar(255);
		set @Comment = 'Daily Iovation Trust Account Update, First Last Check Interval = '
									 +CAST(@IvoationFirstAndLastCheckIntervalInDays AS VARCHAR(5)) +
									 ' Days. Number of Days Threashold = '
									 +CAST(@IvoationNumberOfDaysCheckThreashold AS VARCHAR(5));
-- 		print @Comment;
		;with ChecksByDays as (
			select [PARTYID] ,convert(date,[DATE]) as [DATE] ,count(*) CHECKS_QTY
			from [admin_all].[IOVATION_HISTORY]
			with (nolock)
			where [RESULT] = 'ALLOW'
			group by [PARTYID] ,convert(date,[DATE])
	)

		,DaysWithCheck as (
				select [PARTYID]
				from ChecksByDays
				group by [PARTYID]
				having count(*) >= @IvoationNumberOfDaysCheckThreashold
		)

		,ObservationPeriod as (
				select [PARTYID]
				from ChecksByDays
				group by [PARTYID]
				having datediff(day,min([DATE]),max([DATE])) > @IvoationFirstAndLastCheckIntervalInDays
		)

		,WithEvidence as (
				select distinct PARTYID
				from [admin_all].[IOVATION_HISTORY]
				with (nolock)
				where [RESULT] <> 'ALLOW'
		)

	insert into @IovationTrusted
		select a.PARTYID
		from DaysWithCheck a
			join ObservationPeriod b on a.PARTYID = b.PARTYID
			left join WithEvidence c on a.PARTYID = c.PARTYID
		where c.PARTYID is null

		begin transaction
-- 		select * from @IovationTrusted
		insert into [admin_all].[user_action_log]
				(action_type,
				 action_id,
				 comment,
				 date,
				 field_name,
				 new_value,
				 old_value,
				 operation_type,
				 partyid,
				 staffid
				)
		select
			'IOVATION_STATUS_CHANGE',
			null,
			@Comment,
			getdate(),
			'IOVATION_CHECK',
			'0',
			'1',
			null,
			PartyID,
			1
		from @IovationTrusted where
			PartyID in (select PartyID from external_mpt.user_conf where IOVATION_CHECK = 1)

		update [external_mpt].[USER_CONF]
		set [IOVATION_CHECK] = 0
		where [PARTYID] in (select PartyID from @IovationTrusted)
					and [IOVATION_CHECK] = 1

		commit
	end

go
