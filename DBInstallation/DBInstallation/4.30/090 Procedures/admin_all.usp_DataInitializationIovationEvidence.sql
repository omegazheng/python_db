set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationIovationEvidence
as
begin
	set nocount on 
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.IOVATION_EVIDENCE', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--admin_all.IOVATION_EVIDENCE
	begin transaction
	;with s as 
	(
		select [CODE],[CATEGORY],[NAME]
		from (
				values (N'10-1',N'B2B Financial',N'Business Identity Theft')
					,(N'10-2',N'B2B Financial',N'Fictitious Business')
					,(N'10-3',N'B2B Financial',N'Business Takeover')
					,(N'10-4',N'B2B Financial',N'Dealer Fraud')
					,(N'10-5',N'B2B Financial',N'Payment Evasion')
					,(N'10-6',N'B2B Financial',N'Business Misrepresentation')
					,(N'1-1',N'Financial',N'Credit Card Fraud')
					,(N'1-10',N'Financial',N'Affiliate Fraud')
					,(N'1-11',N'Financial',N'First Party Fraud')
					,(N'1-12',N'Financial',N'Loan Default')
					,(N'1-2',N'Financial',N'ACH/Debit Fraud')
					,(N'1-3',N'Financial',N'Friendly Chargeback')
					,(N'1-4',N'Financial',N'Insuffcient Funds')
					,(N'1-5',N'Financial',N'Fraud Other')
					,(N'1-6',N'Financial',N'Potential Fraud')
					,(N'1-7',N'Financial',N'Shipping Fraud')
					,(N'1-8',N'Financial',N'Counterfeit Money Order/Cashier''s Check')
					,(N'1-9',N'Financial',N'Click Fraud')
					,(N'2-1',N'Cheating',N'Collusion')
					,(N'2-2',N'Cheating',N'Chip Dumping')
					,(N'2-3',N'Cheating',N'All-In Abuse')
					,(N'2-4',N'Cheating',N'Trading Restriction')
					,(N'3-1',N'Misconduct',N'Chat Abuse')
					,(N'3-10',N'Misconduct',N'Code Hacking')
					,(N'3-11',N'Misconduct',N'Arbitrage Betting')
					,(N'3-12',N'Misconduct',N'Gold Farming')
					,(N'3-2',N'Misconduct',N'SPAM')
					,(N'3-3',N'Misconduct',N'Abusive to Support')
					,(N'3-4',N'Misconduct',N'Promotion Abuse')
					,(N'3-5',N'Misconduct',N'Policy / License Agreement Violations')
					,(N'3-6',N'Misconduct',N'Customer Harassment / Bullying')
					,(N'3-7',N'Misconduct',N'Inappropriate Content')
					,(N'3-8',N'Misconduct',N'Profile Misrepresentation')
					,(N'3-9',N'Misconduct',N'Scammer / Solicitation')
					,(N'4-1',N'Identity Theft',N'True Identity Theft')
					,(N'4-2',N'Identity Theft',N'Synthetic Identity Theft')
					,(N'4-3',N'Identity Theft',N'Identity Mining (Phishing)')
					,(N'4-4',N'Identity Theft',N'Account Take-Over / Hijacking')
					,(N'4-5',N'Identity Theft',N'Failed Multi-Factor Authentication')
					,(N'5-1',N'Policy Fraud',N'Application Fraud 1st Party')
					,(N'5-2',N'Policy Fraud',N'Application Fraud 3rd Party')
					,(N'5-3',N'Policy Fraud',N'Claims Fraud 1st Party')
					,(N'5-4',N'Policy Fraud',N'Claims Fraud 3rd Party')
					,(N'99-1',N'Miscellaneous',N'High Risk')
					,(N'99-2',N'Miscellaneous',N'Under / Over Age')
					,(N'99-3',N'Miscellaneous',N'Customer Requested Exclusion')
			) v([CODE],[CATEGORY],[NAME])
	)
	merge admin_all.IOVATION_EVIDENCE t
	using s on s.[CODE]= t.[CODE]
	when not matched then
		insert ([CODE],[CATEGORY],[NAME])
		values(s.[CODE],s.[CATEGORY],s.[NAME])
	when matched and (s.[CATEGORY] is null and t.[CATEGORY] is not null or s.[CATEGORY] is not null and t.[CATEGORY] is null or s.[CATEGORY] <> t.[CATEGORY] or s.[NAME] is null and t.[NAME] is not null or s.[NAME] is not null and t.[NAME] is null or s.[NAME] <> t.[NAME]) then 
		update set  t.[CATEGORY]= s.[CATEGORY], t.[NAME]= s.[NAME]
	;
	commit;
end
go
