set ansi_nulls, quoted_identifier on
go

-- get unique players by period, [today, yesterday, week to date, month to date]
create or alter procedure [admin_all].[omegasp_getUniquePlayerByPeriod]
  (
    @staffid INT
  )
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(DAY, -32, CAST(@endDateLocal AS DATE))
    SELECT
      SUMMARY_DATE               AS DATE,
      BRAND_ID                   AS BRAND_ID,
      COUNT(DISTINCT (PARTY_ID)) AS UNIQUE_PLAYERS
    INTO #Temp
    FROM ADMIN_ALL.DW_GAME_PLAYER_DAILY DW
    WHERE DW.SUMMARY_DATE >= @startDateLocal
          AND
          DW.SUMMARY_DATE < @endDateLocal
          AND BRAND_ID IN (SELECT BRANDID
                           FROM [admin_all].[STAFF_BRAND_TBL]
                           WHERE STAFFID = @staffid)
    GROUP BY SUMMARY_DATE, BRAND_ID;

    SELECT
      tt.PERIOD              AS PERIOD,
      SUM(tt.UNIQUE_PLAYERS) AS UNIQUE_PLAYERS
    FROM
      (
        SELECT
          'TODAY'        AS PERIOD,
          BRAND_ID       AS BRAND_ID,
          UNIQUE_PLAYERS AS UNIQUE_PLAYERS
        FROM #Temp
        WHERE DATE = CAST(@endDateLocal AS DATE)

        UNION

        SELECT
          'YESTERDAY'    AS PERIOD,
          BRAND_ID       AS BRAND_ID,
          UNIQUE_PLAYERS AS UNIQUE_PLAYERS
        FROM #Temp
        WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

        UNION
        -- UINQUE PLAYER IS TO THE SCOPE OF THE ENTIRE PERIOD, CAN NOT BE AGGREGATED FROM DAILY VALUE
        SELECT
          'WTD'               AS PERIOD,
          BRAND_ID            AS BRAND_ID,
          COUNT(DISTINCT PARTY_ID) AS UNIQUE_PLAYERS
        FROM DW_GAME_PLAYER_DAILY
        WHERE SUMMARY_DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
              AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
              AND BRAND_ID IN (SELECT BRANDID
                               FROM [admin_all].[STAFF_BRAND_TBL]
                               WHERE STAFFID = @staffid)
        GROUP BY BRAND_ID

        UNION
        -- UINQUE PLAYER IS TO THE SCOPE OF THE ENTIRE PERIOD, CAN NOT BE AGGREGATED FROM DAILY VALUE
        SELECT
          'MTD'               AS PERIOD,
          BRAND_ID            AS BRAND_ID,
          COUNT(DISTINCT PARTY_ID) AS UNIQUE_PLAYERS
        FROM DW_GAME_PLAYER_DAILY
        WHERE SUMMARY_DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
              AND SUMMARY_DATE <= CAST(@endDateLocal AS DATE)
              AND BRAND_ID IN (SELECT BRANDID
                               FROM [admin_all].[STAFF_BRAND_TBL]
                               WHERE STAFFID = @staffid)
        GROUP BY BRAND_ID
      ) tt
    GROUP BY tt.PERIOD

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END
  END

go
