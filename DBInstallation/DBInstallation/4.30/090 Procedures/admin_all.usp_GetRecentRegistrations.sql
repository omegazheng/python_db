set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetRecentRegistrations]
  (@brandid nvarchar(2000), @startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    set nocount on

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);


    SELECT u.PARTYID,
           u.USERID,
           u.NICKNAME,
           u.FIRST_NAME,
           u.LAST_NAME,
           u.CURRENCY,
           u.BRANDID,
           b.BRANDNAME,
           u.COUNTRY,
           c.NAME,
           u.REG_DATE,
           u.ACTIVE_FLAG,
           u.LOCKED_STATUS,
           u.email,
           u.PHONE,
           u.PHONE2,
           u.MOBILE_PHONE
    FROM [external_mpt].[user_conf] u WITH (NOLOCK)
           LEFT JOIN [admin].[casino_brand_def] b WITH (NOLOCK) ON b.brandid = u.brandid
           LEFT JOIN [admin_all].[country] c on u.country = c.iso2_Code
    WHERE u.REG_DATE >= @startDateLocal
      AND u.REG_DATE < @endDateLocal
      AND u.brandId = @brandid
    ORDER BY u.REG_DATE DESC
  END

go
