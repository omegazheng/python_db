set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_PurgeAccountTran] @Retention int = 500, @BatchSize int = 150, @ArchiveDatabase nvarchar(128)= null
as
begin
	set nocount, xact_abort on
	declare @i bigint, @ID bigint, @db nvarchar(128), @SQL nvarchar(max), @Proc sysname, @FullTargetTableName1 sysname, @FullTargetTableName2 sysname
	if @ArchiveDatabase is not null
		begin
			select @db = db_name(db_id(@ArchiveDatabase))
			if @db is null
				begin
					raiserror('Archive database is not valid %s', 16,1, @ArchiveDatabase)
					return
				end
			if @db is not null
				begin
					select @Proc = quotename(@db) + '..sp_executesql'
					select @SQL = 'select @ret = case when exists(select * from sys.schemas where name = ''admin_all'') then 1 else 0 end'
					exec @Proc @SQL, N'@ret int output', @i output
					if @i = 0
						begin
							select @SQL = 'create schema admin_all'
							begin try
							exec @proc @SQL
							end try
							begin catch
							end catch
						end
					select @FullTargetTableName1 = quotename(@db)+'.admin_all.TRANSACTION_TAG_ACCOUNT_TRAN'
					if object_id(@FullTargetTableName1) is null
						begin
							select @SQL = 'create table ' + @FullTargetTableName1 +' ([ID] int not null,[ACCOUNT_TRAN_ID] bigint,[TRANSACTION_TAG_ID] int, constraint PK_admin_all_TRANSACTION_TAG_ACCOUNT_TRAN primary key (ID))'
							exec(@SQL)
						end
					select @FullTargetTableName2 = quotename(@db)+'.admin_all.account_tran'
					if object_id(@FullTargetTableName2) is null
						begin
							select @SQL = 'create table ' + @FullTargetTableName2 +' ([ID] bigint not null,[ACCOUNT_ID] int,[DATETIME] datetime,[TRAN_TYPE] varchar(10),[AMOUNT_REAL] numeric(10,2),[BALANCE_REAL] numeric(10,2),[PLATFORM_TRAN_ID] nvarchar(100),[GAME_TRAN_ID] varchar(100),[GAME_ID] varchar(100),[PLATFORM_ID] int,[payment_id] int,[ROLLED_BACK] int,[ROLLBACK_TRAN_ID] bigint,[AMOUNT_RELEASED_BONUS] numeric(10,2),[AMOUNT_PLAYABLE_BONUS] numeric(10,2),[BALANCE_RELEASED_BONUS] numeric(10,2),[BALANCE_PLAYABLE_BONUS] numeric(10,2),[AMOUNT_UNDERFLOW] numeric(10,2),[AMOUNT_RAW_LOYALTY] bigint,[BALANCE_RAW_LOYALTY] bigint,[AMOUNT_FREE_BET] numeric(10,2),[GAME_INSTANCE_ID] bigint,[TRANSACTION_ON_HOLD_ID] bigint,[SSW_TRAN_ID] bigint,[REFERENCE] varchar(100),[BRAND_ID] int, constraint PK_admin_all_account_tran primary key (ID))'
							exec(@SQL)
						end
					select @SQL = '	insert into ' + @FullTargetTableName1 + '(ID, ACCOUNT_TRAN_ID, TRANSACTION_TAG_ID)
	select ID, ACCOUNT_TRAN_ID, TRANSACTION_TAG_ID
	from (
			delete t
				output deleted.ID, deleted.ACCOUNT_TRAN_ID, deleted.TRANSACTION_TAG_ID
			from admin_all.TRANSACTION_TAG_ACCOUNT_TRAN t
			where ACCOUNT_TRAN_ID = @ID
		) t1
		insert into ' + @FullTargetTableName2 + '(ID, ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID, GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS, AMOUNT_UNDERFLOW, AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY, AMOUNT_FREE_BET, GAME_INSTANCE_ID, TRANSACTION_ON_HOLD_ID, SSW_TRAN_ID, REFERENCE, BRAND_ID)
	select ID, ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, PLATFORM_TRAN_ID, GAME_TRAN_ID, GAME_ID, PLATFORM_ID, payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS, AMOUNT_UNDERFLOW, AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY, AMOUNT_FREE_BET, GAME_INSTANCE_ID, TRANSACTION_ON_HOLD_ID, SSW_TRAN_ID, REFERENCE, BRAND_ID
	from (
			delete t
				output deleted.ID,deleted.ACCOUNT_ID,deleted.DATETIME,deleted.TRAN_TYPE,deleted.AMOUNT_REAL,deleted.BALANCE_REAL,deleted.PLATFORM_TRAN_ID,deleted.GAME_TRAN_ID,deleted.GAME_ID,deleted.PLATFORM_ID,deleted.payment_id,deleted.ROLLED_BACK,deleted.ROLLBACK_TRAN_ID,deleted.AMOUNT_RELEASED_BONUS,deleted.AMOUNT_PLAYABLE_BONUS,deleted.BALANCE_RELEASED_BONUS,deleted.BALANCE_PLAYABLE_BONUS,deleted.AMOUNT_UNDERFLOW,deleted.AMOUNT_RAW_LOYALTY,deleted.BALANCE_RAW_LOYALTY,deleted.AMOUNT_FREE_BET,deleted.GAME_INSTANCE_ID,deleted.TRANSACTION_ON_HOLD_ID,deleted.SSW_TRAN_ID,deleted.REFERENCE,deleted.BRAND_ID
			from admin_all.ACCOUNT_TRAN t
			where ID = @ID
		) t1
		'
				end
		end
	select @i = 0
	declare c cursor local static for
		select ID from admin_all.account_tran where DATETIME < dateadd(day, -@Retention, getdate()) --or DATETIME < '2016-01-01'
	open c
	fetch next from c into @ID
	while @@fetch_status = 0
		begin
			select @i = @i +1
			if @@trancount = 0
				begin
					begin transaction
				end
			if @db is null
				begin
					delete admin_all.TRANSACTION_TAG_ACCOUNT_TRAN where ACCOUNT_TRAN_ID = @ID
					delete admin_all.account_tran where ID = @ID
				end
			else
				begin
					exec sp_executesql @SQL, N'@ID bigint', @ID = @ID
				end
			if @i % @BatchSize = 0
				begin
					while @@trancount > 0
						commit
				end
			fetch next from c into @ID
		end
	close c
	deallocate c
	while @@trancount > 0
		commit
end

go
