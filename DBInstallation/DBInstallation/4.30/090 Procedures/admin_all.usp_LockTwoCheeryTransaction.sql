set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_LockTwoCheeryTransaction(@PartyID int = null, @MachineID int = null, @EFTID bigint = null)
as
begin
	set nocount on 
	if @@trancount = 0
	begin
		print 'No transaction found, locking is skipt'
		return
	end
	declare @Resource nvarchar(128) 
	if @EFTID is not null
	begin
		select @PartyID = PartyID
		from admin_all.MachineTranPendingEFT
		where EFTID = @EFTID
		if @@rowcount = 0
		begin
			select @PartyID = b.PartyID, @MachineID = b.MachineID
			from admin_all.MachineTranEFT a
				inner join admin_all.MachineTran b on a.MachineTranID = b.MachineTranID
			where a.EFTID = @EFTID
		end
	end
	
	select @Resource = cast(@PartyID as nvarchar(100)) +'-'+ cast(isnull(@MachineID, -999) as nvarchar(100)) + N'-usp_LockTwoCheeryTransaction';
	if @EFTID is null
	begin	
		exec sp_getapplock @Resource = @Resource, @LockMode='Exclusive', @LockOwner = 'Transaction', @LockTimeout = -1
	end
	else
	begin
		exec sp_getapplock @Resource = @Resource, @LockMode='IntentExclusive', @LockOwner = 'Transaction', @LockTimeout = -1
		select @Resource = cast(@EFTID as nvarchar(100)) +N'-EFT-usp_LockTwoCheeryTransaction';
		exec sp_getapplock @Resource = @Resource, @LockMode='Exclusive', @LockOwner = 'Transaction', @LockTimeout = -1
	end
end


go
