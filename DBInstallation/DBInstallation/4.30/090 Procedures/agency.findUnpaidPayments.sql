set ansi_nulls, quoted_identifier on
go

create or alter procedure [agency].[findUnpaidPayments]
    (@sp_name varchar(20))
AS
  BEGIN
     select agent_id, plan_id, start_date, end_date, state, type, comments, sum(value) as value
     from agency.commission_payment
     where (state not like 'COMPLETED%')
     group by agent_id, plan_id, start_date, end_date, state, type, comments
  END

go
