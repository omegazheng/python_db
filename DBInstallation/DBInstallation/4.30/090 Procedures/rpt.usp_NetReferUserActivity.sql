set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_NetReferUserActivity
(
	@StartDate datetime,
	@EndDate datetime,
	@BrandID int,
	@AffiliateKeys varchar(max) = null
)
as
begin
	set nocount, xact_abort on
	declare @IsEnabled bit,  /*@ReleasedBonusToGrossRevenueIsEnabled bit, */ @BonusCalculationIsEnabled bit, @JackpotContributionIsEnabled bit,
			@ManualBonusAdjustmentPercentage numeric(38, 18),@TargetCurrency nchar(3)

	select	@IsEnabled = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'netReferUpload.isEnabled') as bit), 0),
			--@ReleasedBonusToGrossRevenueIsEnabled = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'netRefer.releasedBonusToGrossRevenue.isEnabled') as bit), 0),
			@BonusCalculationIsEnabled = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'netRefer.bonusCalculation.isEnabled') as bit), 0),
			@JackpotContributionIsEnabled = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'netReferUpload.jackpotContribution.isEnabled') as bit), 0),
			@ManualBonusAdjustmentPercentage = isnull(cast(admin_all.fn_GetBrandRegistry(@BrandID, 'netRefer.manualBonusAdjustmentPercentage') as numeric(38, 18)), 0),
			@TargetCurrency = nullif(rtrim(admin_all.fn_GetBrandRegistry(@BrandID, 'netRefer.targetCurrency')), '')
	--select @IsEnabled = 1 --- for testing purpose, should be commented out.
	if @IsEnabled = 0
		return

	create table #User (PartyID int, AccountID int, Currency nchar(3), BrandID int, ConversionRate numeric(38, 18),primary key(PartyID), unique(AccountID))
	create unique index #a on #User(AccountID)
	
	--select * from admin_all.PLATFORM
	if @AffiliateKeys is null
	begin
		if @TargetCurrency is null
		begin
			insert into #User(PartyID, AccountID, Currency, BrandID, ConversionRate)
				select u.PARTYID, a.id, u.CURRENCY, u.BRANDID, 1
				from external_mpt.USER_CONF u
					inner join admin_all.ACCOUNT a on u.PARTYID = a.PARTYID
				where u.BRANDID = @BrandID
		end
		else
		begin
			insert into #User(PartyID, AccountID, Currency, BrandID, ConversionRate)
				select u.PARTYID, a.id, @TargetCurrency, u.BRANDID, r.Rate
				from external_mpt.USER_CONF u
					inner join admin_all.ACCOUNT a on u.PARTYID = a.PARTYID
					outer apply admin_all.fn_GetCurrencyConversionRate1(null, u.CURRENCY, @TargetCurrency) r
				where u.BRANDID = @BrandID
		end
	end
	else
	begin
		if @TargetCurrency is null
		begin
			insert into #User(PartyID, AccountID, Currency, BrandID, ConversionRate)
				select u.PARTYID, a.id, u.CURRENCY, u.BRANDID, 1
				from external_mpt.USER_CONF u
					inner join admin_all.ACCOUNT a on u.PARTYID = a.PARTYID
					inner join admin_all.USER_TRACKING_CODE affiliate WITH (NOLOCK) ON affiliate.partyid = u.partyid
				WHERE u.BRANDID = @BrandID AND affiliate.CODE_KEY IN (select item from admin_all.fc_splitDelimiterString(@AffiliateKeys, ','))
		end
		else
		begin
			insert into #User(PartyID, AccountID, Currency, BrandID, ConversionRate)
				select u.PARTYID, a.id, @TargetCurrency, u.BRANDID, r.Rate
				from external_mpt.USER_CONF u
					inner join admin_all.ACCOUNT a on u.PARTYID = a.PARTYID
					inner join admin_all.USER_TRACKING_CODE affiliate on affiliate.partyid = u.partyid
					outer apply admin_all.fn_GetCurrencyConversionRate1(null, u.CURRENCY, @TargetCurrency) r
				WHERE u.BRANDID = @BrandID AND affiliate.CODE_KEY IN (select item from admin_all.fc_splitDelimiterString(@AffiliateKeys, ','))
		end
	end
	create table #Ratio(ProductID int, GameID varchar(100), IsTouch bit, Ratio numeric(38, 18), primary key (ProductID, GameID))
	insert into #Ratio(ProductID, GameID, Ratio, IsTouch)
		select gi.PLATFORM_ID ProductID, gi.GAME_ID GameID, Ratio, gi.IS_TOUCH
		from admin_all.GAME_INFO gi
			inner join(
							select jp.GAME_INFO_ID, RATIO, row_number() over(partition by jp.GAME_INFO_ID order by last_update desc) rn
							from admin_all.BRAND_GAME_JP_RATIO jp
							where jp.BRANDID = jp.BRANDID
						) r on gi.ID = r.GAME_INFO_ID
		where r.rn = 1

	create table #Product(ProductID int primary key)
	insert into #Product values(0),(1),(2)
	create table #AdjustmentType(AdjustmentTypeID int primary key)
	insert into #AdjustmentType values(1), (2), (3), (4)
	
	--AdjustmentTypeID int, --1 ? Activity (default), 2 ? Fee, 3 ? Customer Reward Deduction, 4 ? Chargebacks

	
	;with u as
	(
		select u.PartyID, u.Currency, p.ProductID, a.AdjustmentTypeID, u.BrandID, u.ConversionRate
		from #User u
			cross apply #AdjustmentType a 
			cross apply #Product p
	),
	p as
	(
		select
			cast(PROCESS_DATE as date) as ActivityDate,
			cast(0 as int) as ProductID,
			u.PartyID,
			abs(sum(case when p.TYPE= 'DEPOSIT' then p.AMOUNT_REAL else 0 end)) AS Deposits,
			abs(sum(case when p.TYPE= 'WITHDRAWAL' then p.AMOUNT_REAL else 0 end)) AS Withdrawals
		from admin_all.PAYMENT p
			--inner join admin_all.ACCOUNT_TRAN a on a.payment_id = p.id
			inner join #User u on u.AccountID = p.ACCOUNT_ID
		where p.TYPE in('WITHDRAWAL', 'DEPOSIT')
			and p.STATUS in ('COMPLETED', 'DP_ROLLBACK', 'WP_ROLLBACK')
			and p.PROCESS_DATE >= @StartDate
			and p.PROCESS_DATE < @EndDate
		group by u.PartyID, cast(PROCESS_DATE as date)
	),
	t as
	(
		select
			a.PartyID,
			r.IsTouch,
			cast(case when isnull(p.PLATFORM_TYPE, '') = 'CASINO' then 1 when isnull(PLATFORM_TYPE, '') = 'SPORTSBOOK' then 2 else 0 end as int) ProductID,
			cast(a.Datetime as date) ActivityDate,
			sum(case when a.TranType in ('GAME_BET') then a.AmountReal else 0 end) BetAmountReal, 
			sum(case when a.TranType in ('GAME_BET') then a.AmountReleasedBonus else 0 end) BetAmountReleasedBonus, 
			sum(case when a.TranType in ('GAME_BET') then a.AmountPlayableBonus else 0 end) BetAmountPlayableBonus, 

			sum(case when a.TranType in ('GAME_WIN') then a.AmountReal else 0 end) WinAmountReal, 
			sum(case when a.TranType in ('GAME_WIN') then a.AmountReleasedBonus else 0 end) WinAmountReleasedBonus, 

			sum(case when a.TranType in ('REFUND') then a.AmountReal else 0 end) RefundAmountReal, 
			sum(case when a.TranType in ('REFUND') then a.AmountReleasedBonus else 0 end) RefundAmountReleasedBonus, 

			sum(case when a.TranType in ('STAKE_DEC') then a.AmountReal else 0 end) StakeDecAmountReal, 
			sum(case when a.TranType in ('STAKE_DEC') then a.AmountReleasedBonus else 0 end) StakeDecAmountReleasedBonus, 

			sum(case when a.TranType in ('CASH_OUT') then a.AmountReal else 0 end) CashOutAmountReal, 
			sum(case when a.TranType in ('CASH_OUT') then a.AmountReleasedBonus else 0 end) CashOutAmountReleasedBonus, 

			sum(case when a.TranType in ('MAN_BONUS') then a.AmountReal else 0 end) ManBonusAmountReal, 
			sum(case when a.TranType in ('MAN_BONUS') then a.AmountReleasedBonus else 0 end) ManBonusAmountReleasedBonus, 
			sum(case when a.TranType in ('MAN_BONUS') then a.AmountPlayableBonus else 0 end) ManBonusAmountPlayableBonus, 

			sum(case when a.TranType in ('CHARGE_BCK') then a.AmountReal else 0 end) ChargeBackAmountReal, 
			sum(case when a.TranType in ('CHARGE_BCK') then a.AmountReleasedBonus else 0 end) ChargeBackAmountReleasedBonus, 
			sum(case when a.TranType in ('CHARGE_BCK') then a.AmountPlayableBonus else 0 end) ChargeBackAmountPlayableBonus, 

			sum(case when a.TranType in ('BONUS_REL') then a.AmountReleasedBonus else 0 end) BonusRelAmountReleasedBonus,

			sum(case when a.TranType in ('GAME_BET') then (a.AmountReal + a.AmountPlayableBonus + a.AmountReleasedBonus ) * r.Ratio else 0 end) BetJackpot, 
			sum(case when a.TranType in ('REFUND') then (a.AmountReal + a.AmountPlayableBonus + a.AmountReleasedBonus ) * r.Ratio else 0 end) RefundJackpot, 
			sum(case when a.TranType in ('STAKE_DEC') then (a.AmountReal + a.AmountPlayableBonus + a.AmountReleasedBonus ) * r.Ratio else 0 end) StakeJackpot

		from admin_all.AccountTranHourlyAggregate a
			left outer join admin_all.PLATFORM p on p.id = a.ProductID
			inner join #User u on u.PartyID = a.PartyID
			left outer join #Ratio r on r.ProductID = a.ProductID and r.GameID = a.GameID
		where a.TranType in ('GAME_BET', 'GAME_WIN', 'BONUS_REL', 'CASH_OUT', 'REFUND', 'STAKE_DEC', 'MAN_BONUS', 'CHARGE_BCK')
			and a.Datetime >= @StartDate
			and DATETIME < @EndDate
			and AggregateType = 0
		group by a.PartyID, cast(cast(case when isnull(p.PLATFORM_TYPE, '') = 'CASINO' then 1 when isnull(PLATFORM_TYPE, '') = 'SPORTSBOOK' then 2 else 0 end as int) as int), cast(a.Datetime as date), r.IsTouch
	),
	a as
	(
		select 
				isnull(p.PartyID, t.PartyID) as PartyID, isnull(p.ActivityDate, t.ActivityDate) ActivityDate,
				isnull(p.ProductID, t.ProductID) as ProductID, isnull(p.Deposits,0) Deposits, isnull(p.Withdrawals, 0) Withdrawals,
				isnull(t.BetAmountPlayableBonus, 0) BetAmountPlayableBonus, isnull(t.BetAmountReal, 0) BetAmountReal, isnull(t.BetAmountReleasedBonus, 0) BetAmountReleasedBonus,
				isnull(t.BonusRelAmountReleasedBonus, 0) BonusRelAmountReleasedBonus,
				isnull(t.CashOutAmountReal, 0) CashOutAmountReal, isnull(t.CashOutAmountReleasedBonus, 0) CashOutAmountReleasedBonus,
				isnull(t.ChargeBackAmountReal, 0) ChargeBackAmountReal, isnull(t.ChargeBackAmountPlayableBonus, 0) ChargeBackAmountPlayableBonus, isnull(t.ChargeBackAmountReleasedBonus, 0) ChargeBackAmountReleasedBonus,
				isnull(t.ManBonusAmountPlayableBonus, 0) ManBonusAmountPlayableBonus, isnull(t.ManBonusAmountReal, 0) ManBonusAmountReal, isnull(t.ManBonusAmountReleasedBonus, 0) ManBonusAmountReleasedBonus,
				isnull(t.RefundAmountReal, 0) RefundAmountReal, isnull(t.RefundAmountReleasedBonus, 0) RefundAmountReleasedBonus,
				isnull(t.StakeDecAmountReal, 0) StakeDecAmountReal, isnull(t.StakeDecAmountReleasedBonus, 0) StakeDecAmountReleasedBonus,
				isnull(t.WinAmountReal, 0) WinAmountReal, isnull(t.WinAmountReleasedBonus, 0) WinAmountReleasedBonus,
				isnull(t.BetJackpot, 0) BetJackpot, isnull(t.RefundJackpot, 0) RefundJackpot, isnull(t.StakeJackpot, 0) StakeJackpot,
				t.IsTouch
		from p 
			full outer join t on p.ActivityDate = t.ActivityDate and p.PartyID = t.PartyID and p.ProductID = t.ProductID
	),
	r as 
	(
		select 
				u.PartyID,
				a.ActivityDate,
				a.ProductID,
				u.BrandID,
				u.Currency,
				u.AdjustmentTypeID,
				a.IsTouch,
				Deposit  	=  case 
									when u.ProductID = 0 and u.AdjustmentTypeID = 1 then (a.Deposits)
									else 0
								end * u.ConversionRate,
				Withdrawal 	=  case 
									when u.ProductID = 0 and u.AdjustmentTypeID = 1 then (a.Withdrawals)
									else 0
								end * u.ConversionRate,
				GrossRevenue =  case 
									when u.ProductID = 1 and u.AdjustmentTypeID = 1 then -(a.BetAmountReal + a.WinAmountReal + a.BetAmountReleasedBonus + a.WinAmountReleasedBonus)
									when u.ProductID = 2 and u.AdjustmentTypeID = 1 then -(a.BetAmountReal + a.WinAmountReal + a.StakeDecAmountReal + a.RefundAmountReal + a.CashOutAmountReal + a.BetAmountReleasedBonus + a.WinAmountReleasedBonus + a.StakeDecAmountReleasedBonus + a.RefundAmountReleasedBonus + a.CashOutAmountReleasedBonus)
									else 0
								end * u.ConversionRate,
				Bonus		=  case 
									when u.ProductID = 0 and u.AdjustmentTypeID = 1 and @BonusCalculationIsEnabled = 1 then -(a.BonusRelAmountReleasedBonus)
									--when u.ProductID = 2 and u.AdjustmentTypeID = 1 and @BonusCalculationIsEnabled = 1 then -(a.BonusRelAmountReleasedBonus)
									else 0
								end * u.ConversionRate,
				TurnOver	=  case 
									when u.ProductID = 1 and u.AdjustmentTypeID = 1 then -(a.BetAmountReal + a.BetAmountReleasedBonus)
									when u.ProductID = 2 and u.AdjustmentTypeID = 1 then -(a.BetAmountReal + a.StakeDecAmountReal + a.RefundAmountReal + a.BetAmountReleasedBonus + a.StakeDecAmountReleasedBonus + a.RefundAmountReleasedBonus)
									else 0
								end * u.ConversionRate,
				Payout		=  case 
									when u.ProductID = 1 and u.AdjustmentTypeID = 1 then (a.WinAmountReal + a.WinAmountReleasedBonus)
									when u.ProductID = 2 and u.AdjustmentTypeID = 1 then (a.WinAmountReal + a.CashOutAmountReal + a.WinAmountReleasedBonus + a.CashOutAmountReleasedBonus)
									else 0
								end * u.ConversionRate,
				Adjustment	=  case 
									when u.ProductID = 1 and u.AdjustmentTypeID = 2 and @JackpotContributionIsEnabled = 1 then -(a.BetJackpot)
									when u.ProductID = 2 and u.AdjustmentTypeID = 2 and @JackpotContributionIsEnabled = 1 then -(a.BetJackpot + a.StakeJackpot + a.RefundJackpot)
									when u.ProductID = 0 and u.AdjustmentTypeID = 3 then (a.ManBonusAmountReal + a.ManBonusAmountReleasedBonus + a.ManBonusAmountPlayableBonus) * @ManualBonusAdjustmentPercentage / 100
									when u.ProductID = 0 and u.AdjustmentTypeID = 4 then -(a.ChargeBackAmountReal + a.ChargeBackAmountReleasedBonus + a.ChargeBackAmountPlayableBonus)
									else 0
								end * u.ConversionRate

		from u
			inner join a on u.ProductID = a.ProductID and u.PartyID = a.PartyID
)
	select *
	from r
	where not (r.Adjustment = 0 and r.Payout = 0 and TurnOver = 0 and Bonus = 0 and GrossRevenue = 0 and Withdrawal = 0 and Deposit = 0 and AdjustmentTypeID in (0,1,2,3,4))
end

go
