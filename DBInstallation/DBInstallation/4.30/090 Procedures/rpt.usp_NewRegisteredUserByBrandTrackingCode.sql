set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_NewRegisteredUserByBrandTrackingCode
(
	@TrackingCode varchar(20),
	@BrandID int,
	@StartDate datetime,
	@EndDate datetime
)
as
begin
	set nocount on
	Select
	  users.partyid,
	  users.country,
	  affiliate.value as trackingCode,
	  users.reg_date,
	  users.ip,
	  users.BRANDID, 
	  ei2.DATA CustomerSource
	From EXTERNAL_MPT.user_conf users
		Inner Join admin_all.USER_TRACKING_CODE affiliate on users.partyid = affiliate.PARTYID
		left join admin_all.USER_EXTRA_INFO ei2 on ei2.PARTYID = users.PARTYID and ei2.INFO_KEY = 'CustomerSourceID' 
	Where
	  affiliate.CODE_KEY = @TrackingCode
	  and users.brandId = @BrandID
	  AND reg_date >= @StartDate
	  AND reg_date < @EndDate
	  AND REGISTRATION_STATUS in ('PLAYER')
end

go
