set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetPlayersCashierTransaction
(
	@AgentID int,
	@DateFrom datetime,
	@DateTo datetime,
	--@PageNumber int,
 --   @PageSize int, 
	@TransactionType varchar(max),
	@FilterUserID varchar(128) = null
)
as
begin
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = DATEADD(DD, 1, @DateTo);
	set nocount on
	create table #TransactionType(TransactionType varchar(10) primary key)
	insert into #TransactionType(TransactionType)
		select rtrim(ltrim(Item))
		from admin_all.fc_splitDelimiterString(@TransactionType, ',')
	;with x0 as 
	(
		select u.PARTYID, u.ParentID, u.UserID, u.Currency, u.user_type
		from external_mpt.USER_CONF u

        WHERE
        @FilterUserID is null AND ParentID = @AgentID
           OR (PARTYID = @AgentID)

		union all
		select u1.PARTYID, u1.ParentID, u1.UserID, u1.Currency, u1.user_type
		from external_mpt.USER_CONF u1
			inner join x0 on x0.PARTYID = u1.ParentID
	),
	x1 as 
	(
		SELECT
        --ROW_NUMBER() OVER ( ORDER BY a.DATETIME DESC ) as RowNum,
        a.ID,
        a.DATETIME as dateTime,
        a.TRAN_TYPE as tranType,
        isnull((a.balance_real - a.amount_real),0) as preBalanceReal,
        isnull(a.amount_real,0) as amountReal,
        isnull(a.balance_real,0) as balanceReal,
        isnull((case when a.amount_real < 0 then amount_real else 0 end), 0) as playerDebit,
        isnull((case when a.amount_real >= 0 then amount_real else 0 end), 0) as playerCredit,
        isnull((a.BALANCE_RELEASED_BONUS - a.AMOUNT_RELEASED_BONUS), 0) AS preBalanceReleasedBonus,
        isnull(a.AMOUNT_RELEASED_BONUS, 0) AS amountReleasedBonus,
        isnull(a.BALANCE_RELEASED_BONUS, 0) AS balanceReleasedBonus,
        isnull((CASE WHEN a.AMOUNT_RELEASED_BONUS < 0 THEN AMOUNT_RELEASED_BONUS ELSE 0 END), 0) AS releasedBonusDebit,
        isnull((CASE WHEN a.AMOUNT_RELEASED_BONUS >= 0 THEN AMOUNT_RELEASED_BONUS ELSE 0 END), 0) AS releasedBonusCredit,
        isnull((a.BALANCE_PLAYABLE_BONUS - a.AMOUNT_PLAYABLE_BONUS), 0) AS preBalancePlayableBonus,
        isnull(a.AMOUNT_PLAYABLE_BONUS, 0) AS amountPlayableBonus,
        isnull(a.BALANCE_PLAYABLE_BONUS, 0) AS balancePlayableBonus,
        isnull((CASE WHEN a.AMOUNT_PLAYABLE_BONUS < 0 THEN AMOUNT_PLAYABLE_BONUS ELSE 0 END), 0) AS playableBonusDebit,
        isnull((CASE WHEN a.AMOUNT_PLAYABLE_BONUS >= 0 THEN AMOUNT_PLAYABLE_BONUS ELSE 0 END), 0) AS playableBonusCredit,
        isnull(((a.balance_real - a.amount_real) + (a.BALANCE_RELEASED_BONUS - a.AMOUNT_RELEASED_BONUS) + (a.BALANCE_PLAYABLE_BONUS - a.AMOUNT_PLAYABLE_BONUS)), 0) AS preBalance,
        isnull((a.AMOUNT_RELEASED_BONUS + a.AMOUNT_PLAYABLE_BONUS + a.AMOUNT_REAL), 0) AS amount,
        isnull((a.BALANCE_REAL + a.BALANCE_RELEASED_BONUS + a.BALANCE_PLAYABLE_BONUS), 0) AS balance,
        isnull((CASE WHEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS) < 0 THEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS) ELSE 0 END), 0) AS debit,
        isnull((CASE WHEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS) >= 0 THEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS) ELSE 0 END), 0) AS credit,
        a.game_id as game_id,
        g.name as gamename,
        p.name as platformName,
        p.code as platformCode,
        a.platform_tran_id as platformTranId,
        a.game_tran_id as gameTranId,
        isnull((a.BALANCE_RAW_LOYALTY - AMOUNT_RAW_LOYALTY), 0) as preBalanceLoyalty,
        isnull((CASE WHEN a.AMOUNT_RAW_LOYALTY < 0 THEN a.AMOUNT_RAW_LOYALTY ELSE 0 end), 0) AS loyaltyDebit,
        isnull((CASE WHEN a.AMOUNT_RAW_LOYALTY > 0 THEN a.AMOUNT_RAW_LOYALTY ELSE 0 end), 0) AS loyaltyCredit,
        isnull(a.BALANCE_RAW_LOYALTY, 0) AS balanceLoyalty,
        isnull(pc.PLATFORM_AMOUNT, 0) AS platformConvAmount,
        pc.PLATFORM_CURRENCY AS platformConvCurrency,
        pc.ID AS platformConvId,
        a.reference,
		aa.PARTYID --as PartyID
      from admin_all.ACCOUNT_TRAN a 
        left join admin_all.PLATFORM p on p.id = a.platform_id
        left join admin_all.GAME_INFO g on a.GAME_ID = g.GAME_ID and a.platform_id = g.PLATFORM_ID
        left join admin_all.TRANSACTION_PLATFORM_CONVERSION pc on pc.TRANSACTION_ID = a.id
        inner join admin_all.ACCOUNT aa on aa.id = a.account_id
      where a.datetime >= @DateFrom
        and a.datetime < @endDateLocal
		and exists(select* from #TransactionType tt where tt.TransactionType = a.TRAN_TYPE)
	)
	select x1.*,
		x0.USERID, x0.CURRENCY, u.USERID Agent--, u.PARTYID 
	from x0
		inner join x1 on x0.PARTYID = x1.PARTYID
		left join external_mpt.USER_CONF u on u.PARTYID = x0.ParentID
		and isnull(x0.user_type, 0) = 0
	--where (x1.RowNum > @PageSize * (@PageNumber - 1))
 --     and (x1.RowNum <= @pageSize * @PageNumber)
    ORDER BY x1.dateTime desc;
end

go
