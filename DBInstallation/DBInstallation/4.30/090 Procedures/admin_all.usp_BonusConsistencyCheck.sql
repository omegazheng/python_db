set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].[usp_BonusConsistencyCheck]
AS
  BEGIN
    SELECT
      *
    FROM
      (SELECT
         account.PARTYID AS                 PARTY_ID,
         account.id      AS                 ACCOUNT_ID,
         account.RELEASED_BONUS,
         ISNULL(bd.BONUS_RELEASED_BONUS, 0) BONUS_RELEASED_BONUS,
         ACCOUNT.PLAYABLE_BONUS,
         ISNULL(bd.BONUS_PLAYABLE_BONUS, 0) BONUS_PLAYABLE_BONUS,
         user_conf.USERID
       FROM account
         LEFT JOIN
         (
           SELECT
             bonuses.PARTYID,
             sum(bonuses.BONUS_RELEASED_BONUS) AS BONUS_RELEASED_BONUS,
             sum(bonuses.BONUS_PLAYABLE_BONUS) AS BONUS_PLAYABLE_BONUS
           FROM
             (
               SELECT
                 bonus.PARTYID,
                 CASE WHEN (bonus.STATUS = 'ACTIVE' OR bonus.STATUS = 'QUEUED' OR bonus.STATUS = 'SPENT' OR
                            bonus.STATUS = 'SPENT_ACTIVE' OR bonus.STATUS = 'QUALIFIED' OR bonus.STATUS = 'PENDING')
                   THEN (bonus.PLAYABLE_BONUS + bonus.PLAYABLE_BONUS_WINNINGS) ELSE 0 END AS BONUS_PLAYABLE_BONUS,
                 bonus.RELEASED_BONUS + bonus.RELEASED_BONUS_WINNINGS                   AS BONUS_RELEASED_BONUS
               FROM BONUS) bonuses
           GROUP BY bonuses.PARTYID
         ) BD
           ON account.PARTYID = BD.PARTYID
         LEFT JOIN external_mpt.user_conf ON user_conf.partyid = account.partyid
      ) r
    WHERE
      r.RELEASED_BONUS != r.BONUS_RELEASED_BONUS OR
      r.PLAYABLE_BONUS != r.BONUS_PLAYABLE_BONUS
  END

go
