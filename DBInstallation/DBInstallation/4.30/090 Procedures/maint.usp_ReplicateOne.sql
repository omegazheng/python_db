set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_ReplicateOne (@FriendlyName varchar(128))
as
begin
	set nocount on
	declare @ConfigurationID int = maint.fn_GetReplicationConfigurationID(@FriendlyName)
	declare @Date datetime = getdate()
	exec maint.usp_SetReplicateOneContextInfo
	begin try
		declare @SourceConnectionID int, @SourceTableOrQuery varchar(max), @Columns varchar(max),
				@FilterType varchar(100), @TargetConnectionID int, @TargetSchemaName varchar(128),
				@TargetObjectName varchar(128), @BatchSize int, @IsActive bit, 
				@SourceConnectionString varchar(max), @FullTargetTableName nvarchar(256)
		select
				@SourceConnectionID = SourceConnectionID, @SourceTableOrQuery = SourceTableOrQuery, @Columns = Columns,
				@FilterType = FilterType, @TargetConnectionID = TargetConnectionID, @TargetSchemaName = TargetSchemaName,
				@TargetObjectName = TargetObjectName, @BatchSize = BatchSize, @IsActive = IsActive,
				@SourceConnectionString = maint.fn_GetSQLConnectionStringByID(SourceConnectionID),
				@FullTargetTableName = TargetSchemaName + '.' + TargetObjectName
		from maint.ReplicationConfiguration
		where ConfigurationID = @ConfigurationID
		if @@rowcount = 0 or @IsActive = 0
		begin
			print 'Could not find replication configuration'
			goto ___End___
		end

		declare @FromValueQuery varchar(max), @FromValueQueryEvaluationType varchar(20), @ToValueQuery varchar(max), 
				@ToValueQueryEvaluationType varchar(20), @ToValueAdjustment varchar(max), @FromValueAdjustment varchar(max),
				@CurrentChangeTrackingVersion bigint, @CurrentRowVersion binary(8)
		select 
				@FromValueQuery = FromValueQuery, @FromValueQueryEvaluationType = FromValueQueryEvaluationType, @ToValueQuery = ToValueQuery, 
				@ToValueQueryEvaluationType = ToValueQueryEvaluationType, @ToValueAdjustment = ToValueAdjustment, @FromValueAdjustment = FromValueAdjustment
		from maint.FilterType
		where FilterType = @FilterType
		if @@rowcount = 0
		begin
			print 'Cound not find Filter Type.'
			goto ___End___
		end
		
		select	@CurrentChangeTrackingVersion = CurrentChangeTrackingVersion,
				@CurrentRowVersion = CurrentRowVersion
		from maint.Connection 
		where ConnectionID = @SourceConnectionID

		declare @FromValue sql_variant, @ToValue sql_variant
		select @FromValue = FromValue, @ToValue = ToValue
		from maint.ReplicationLastExecution
		where ConfigurationID = @ConfigurationID

		exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @StartDate = @Date

		declare @Param nvarchar(max), @Param1 nvarchar(max), @Param2 nvarchar(max), @SQL nvarchar(max), @Rows int = 0, @Error varchar(max)
		select	@Param = N'@SourceConnectionID int, @SourceTableOrQuery varchar(max), @Columns varchar(max), @FilterType varchar(30), @TargetConnectionID int, @TargetSchemaName varchar(128), @TargetObjectName varchar(128), @BatchSize int, @IsActive bit, @FromValueQuery varchar(max), @FromValueQueryEvaluationType varchar(20), @ToValueQuery varchar(max), @ToValueQueryEvaluationType varchar(20),@FromValueAdjustment varchar(max), @ToValueAdjustment varchar(max), @FromValue sql_variant, @ToValue sql_variant, @CurrentChangeTrackingVersion bigint, @CurrentRowVersion binary(8), @NewValue nvarchar(max) output',
				@Param1 = N'@SourceConnectionID int, @SourceTableOrQuery varchar(max), @Columns varchar(max), @FilterType varchar(30), @TargetConnectionID int, @TargetSchemaName varchar(128), @TargetObjectName varchar(128), @BatchSize int, @IsActive bit, @FromValueQuery varchar(max), @FromValueQueryEvaluationType varchar(20), @ToValueQuery varchar(max), @ToValueQueryEvaluationType varchar(20), @FromValueAdjustment varchar(max), @ToValueAdjustment varchar(max), @FromValue sql_variant, @ToValue sql_variant, @CurrentChangeTrackingVersion bigint, @CurrentRowVersion binary(8), @NewValue sql_variant output',
				@Param2 = N'@SourceConnectionID int, @SourceTableOrQuery varchar(max), @Columns varchar(max), @FilterType varchar(30), @TargetConnectionID int, @TargetSchemaName varchar(128), @TargetObjectName varchar(128), @BatchSize int, @IsActive bit, @FromValueQuery varchar(max), @FromValueQueryEvaluationType varchar(20), @ToValueQuery varchar(max), @ToValueQueryEvaluationType varchar(20), @FromValueAdjustment varchar(max), @ToValueAdjustment varchar(max), @FromValue sql_variant, @ToValue sql_variant, @CurrentChangeTrackingVersion bigint, @CurrentRowVersion binary(8), @NewFromValue nvarchar(max), @NewToValue nvarchar(max), @NewValue sql_variant output'
		if @FilterType = 'Snapshot'
		begin
			select @SQL = @SourceTableOrQuery
			exec @Rows = maint.usp_ConnectionBulkCopy @SourceConnectionID = @SourceConnectionID, @CommandText = @SQL, @TargetConnectionID = @TargetConnectionID, @TargetTable = @FullTargetTableName, @BatchSize = @BatchSize
			goto ___End___
		end
		
		if @FilterType = 'ChangeTracking'
		begin
			
			if maint.fn_IsChangeTrackingEnabled(@SourceConnectionString) = 0
			begin
				exec maint.usp_EnableChangeTracking @SourceConnectionString
			end
			if maint.fn_IsChangeTrackingEnabledOnTable(@SourceConnectionString, @SourceTableOrQuery) = 0
			begin
				exec maint.usp_EnableChangeTrackingOnTable @SourceConnectionString, @SourceTableOrQuery
			end
			select * into #SourceDefinition from maint.fn_GetTableSchema(@SourceConnectionString, @SourceTableOrQuery)
			delete #SourceDefinition where ColumnName in ('___IsDeleted___')
			if @Columns is not null
				select * from #SourceDefinition where PrimaryKeyOrder > 0 or ColumnName in (select Value from maint.SplitDelimitedString(@Columns, ','))
			
			select @FromValue = @ToValue
			--select @FromValue = cast(@ToValue as bigint) - 1
			if @FromValue < 0
				select @FromValue = 0
			select @ToValue = CurrentChangeTrackingVersion 
			from maint.Connection 
			where ConnectionID = @SourceConnectionID
			
			if @FromValue is null
			begin
				select @SQL = 'select 0 as ___IsDeleted___'+(select ','+quotename(ColumnName) from #SourceDefinition for xml path(''), type).value('.', 'varchar(max)')+' from ' + @SourceTableOrQuery
				select @FromValue = 0
			end
			else
			begin
				select @SQL = 'exec sp_executesql N''select '+(select '
			'+case when PrimaryKeyOrder>0 then 'c.'+quotename(ColumnName) else 'b.'+quotename(ColumnName) end+' as ' + quotename(ColumnName) + ',' from #SourceDefinition order by ColumnID for xml path(''), type).value('.', 'varchar(max)')+'
			case when c.SYS_CHANGE_OPERATION = ''''D'''' then 1 else 0 end ___IsDeleted___ 
		from changetable(changes '+@SourceTableOrQuery+', @Version) c 
			left join '+@SourceTableOrQuery+' b on '+stuff((select ' and c.'+quotename(ColumnName)+' = b.' + quotename(ColumnName) from #SourceDefinition where PrimaryKeyOrder > 0 order by PrimaryKeyOrder for xml path(''), type).value('.', 'varchar(max)'), 1, 5, '')+'
		where c.SYS_CHANGE_CONTEXT is null'', N''@Version bigint'','+cast(cast(@FromValue as bigint) as varchar(100))
		
			end
			
			exec @Rows = maint.usp_ConnectionBulkCopy @SourceConnectionID = @SourceConnectionID, @CommandText = @SQL, @TargetConnectionID = @TargetConnectionID, @TargetTable = @FullTargetTableName, @BatchSize = @BatchSize
			exec maint.usp_SetReplicationHistory @ConfigurationID =@ConfigurationID, @FromValue= @FromValue, @ToValue = @ToValue
			goto ___End___
		end
		
		declare @NewFromValue nvarchar(max), @NewToValue nvarchar(max), @NewFromValueAdjustment sql_variant, @NewToValueAdjustment sql_variant, @NewValue varchar(max)
		if @FromValueQuery is not null and @FromValueQueryEvaluationType <> 'None'
		begin
			select @SQL = 'select @NewValue = ' + @FromValueQuery
			exec sp_executesql @stmt = @SQL, @params  = @Param, @SourceConnectionID = @SourceConnectionID, @SourceTableOrQuery = @SourceTableOrQuery, @Columns = @Columns, @FilterType = @FilterType, @TargetConnectionID = @TargetConnectionID, @TargetSchemaName = @TargetSchemaName, @TargetObjectName = @TargetObjectName, @BatchSize = @BatchSize, @IsActive = @IsActive, @FromValueQuery = @FromValueQuery, @FromValueQueryEvaluationType = @FromValueQueryEvaluationType, @ToValueQuery = @ToValueQuery, @ToValueQueryEvaluationType = @ToValueQueryEvaluationType, @ToValueAdjustment = @ToValueAdjustment, @FromValue = @FromValue, @ToValue = @ToValue, @CurrentChangeTrackingVersion = @CurrentChangeTrackingVersion, @CurrentRowVersion = @CurrentRowVersion, @NewValue = @NewValue output, @FromValueAdjustment = @FromValueAdjustment
			select @NewFromValue = @NewValue
			if @FromValueQueryEvaluationType = 'Source'
			begin
				select @NewFromValue = cast(maint.fn_ConnectionExecuteScalarObject(@SourceConnectionID, @NewFromValue) as nvarchar(max))
			end
			else if @FromValueQueryEvaluationType = 'Target'
			begin
				select @NewFromValue = cast(maint.fn_ConnectionExecuteScalarObject(@TargetConnectionID, @NewFromValue) as nvarchar(max))
			end
		end
		
		if @ToValueQuery is not null and @ToValueQueryEvaluationType <> 'None'
		begin
			select @SQL = 'select @NewValue = ' + @ToValueQuery 
			exec sp_executesql @stmt = @SQL, @params  = @Param, @SourceConnectionID = @SourceConnectionID, @SourceTableOrQuery = @SourceTableOrQuery, @Columns = @Columns, @FilterType = @FilterType, @TargetConnectionID = @TargetConnectionID, @TargetSchemaName = @TargetSchemaName, @TargetObjectName = @TargetObjectName, @BatchSize = @BatchSize, @IsActive = @IsActive, @FromValueQuery = @FromValueQuery, @FromValueQueryEvaluationType = @FromValueQueryEvaluationType, @ToValueQuery = @ToValueQuery, @ToValueQueryEvaluationType = @ToValueQueryEvaluationType, @ToValueAdjustment = @ToValueAdjustment, @FromValue = @FromValue, @ToValue = @ToValue, @CurrentChangeTrackingVersion = @CurrentChangeTrackingVersion, @CurrentRowVersion = @CurrentRowVersion, @NewValue = @NewValue output, @FromValueAdjustment = @FromValueAdjustment
			select @NewToValue = @NewValue
			if @ToValueQueryEvaluationType = 'Source'
			begin
				select @NewToValue = cast(maint.fn_ConnectionExecuteScalarObject(@SourceConnectionID, @NewToValue) as nvarchar(max))
			end
			else if @ToValueQueryEvaluationType = 'Target'
			begin
				select @NewToValue = cast(maint.fn_ConnectionExecuteScalarObject(@TargetConnectionID, @NewToValue) as nvarchar(max))
			end
		end
		
		
		if @ToValueAdjustment is not null 
		begin
			select @SQL = 'select @NewValue = ' + @ToValueAdjustment 
			exec sp_executesql @stmt = @SQL, @params  = @Param2, @SourceConnectionID = @SourceConnectionID, @SourceTableOrQuery = @SourceTableOrQuery, @Columns = @Columns, @FilterType = @FilterType, @TargetConnectionID = @TargetConnectionID, @TargetSchemaName = @TargetSchemaName, @TargetObjectName = @TargetObjectName, @BatchSize = @BatchSize, @IsActive = @IsActive, @FromValueQuery = @FromValueQuery, @FromValueQueryEvaluationType = @FromValueQueryEvaluationType, @ToValueQuery = @ToValueQuery, @ToValueQueryEvaluationType = @ToValueQueryEvaluationType, @ToValueAdjustment = @ToValueAdjustment, @FromValue = @FromValue, @ToValue = @ToValue, @CurrentChangeTrackingVersion = @CurrentChangeTrackingVersion, @CurrentRowVersion = @CurrentRowVersion, @NewFromValue = @NewFromValue, @NewToValue = @NewToValue ,@NewValue = @NewToValueAdjustment output, @FromValueAdjustment = @FromValueAdjustment
		end
		if @FromValueAdjustment is not null 
		begin
			select @SQL = 'select @NewValue = ' + @FromValueAdjustment 
			exec sp_executesql @stmt = @SQL, @params  = @Param2, @SourceConnectionID = @SourceConnectionID, @SourceTableOrQuery = @SourceTableOrQuery, @Columns = @Columns, @FilterType = @FilterType, @TargetConnectionID = @TargetConnectionID, @TargetSchemaName = @TargetSchemaName, @TargetObjectName = @TargetObjectName, @BatchSize = @BatchSize, @IsActive = @IsActive, @FromValueQuery = @FromValueQuery, @FromValueQueryEvaluationType = @FromValueQueryEvaluationType, @ToValueQuery = @ToValueQuery, @ToValueQueryEvaluationType = @ToValueQueryEvaluationType, @ToValueAdjustment = @ToValueAdjustment, @FromValue = @FromValue, @ToValue = @ToValue, @CurrentChangeTrackingVersion = @CurrentChangeTrackingVersion, @CurrentRowVersion = @CurrentRowVersion, @NewFromValue = @NewFromValue, @NewToValue = @NewToValue ,@NewValue = @NewFromValueAdjustment output, @FromValueAdjustment = @FromValueAdjustment
		end

		select @SQL = replace(replace(@SourceTableOrQuery, '@___From___', isnull(@NewFromValue, '')), '@___To___', isnull(@NewToValue, ''))

		exec @Rows = maint.usp_ConnectionBulkCopy @SourceConnectionID = @SourceConnectionID, @CommandText = @SQL, @TargetConnectionID = @TargetConnectionID, @TargetTable = @FullTargetTableName, @BatchSize = @BatchSize
		
		exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @FromValue = @NewFromValueAdjustment, @ToValue = @NewToValueAdjustment
___End___:
	end try
	begin catch
		select @Error = error_message()
		exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @Error = @Error
	end catch
	select @Date = getdate()
	exec maint.usp_SetReplicationHistory @ConfigurationID = @ConfigurationID, @EndDate = @Date, @Rows = @Rows
	exec maint.usp_ClearReplicateOneContextInfo
	return @ConfigurationID
end

go
