set ansi_nulls, quoted_identifier on
go


create or alter procedure admin_all.freespinOption_insertMGGame
(
    @gameId VARCHAR (100),
    @gameName VARCHAR (100)
)
as

begin

    if EXISTS (select * from admin_all.PLATFORM where CODE = 'QFIRE_CAS')
        begin
            declare @platformId int
            select @platformId = ID from admin_all.PLATFORM where CODE = 'QFIRE_CAS'
            if EXISTS (select * from admin_all.GAME_INFO where GAME_ID = @gameId)
                begin
                    if NOT EXISTS(select * from admin_all.FREESPIN_OPTION where PLATFORM_ID=@platformId and GAME_ID=@gameId)
                        insert into admin_all.FREESPIN_OPTION(PLATFORM_ID, GAME_ID, GAME_NAME) values (@platformId, @gameId, @gameName)
                end
        end
end


go
