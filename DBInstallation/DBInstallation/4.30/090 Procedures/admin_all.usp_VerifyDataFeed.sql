set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_VerifyDataFeed 
(
	@BackFromMinutes int = 5,
	@BackToMinutes int = 35,
	@Autofix bit =0,
	@CheckIntervalInMinutes int = 20
)
as
begin 
	set nocount on
	declare @d2 datetime = dateadd(minute, -@BackFromMinutes, getdate()) 
	declare @d1 Datetime = dateadd(minute, -@BackToMinutes, getdate())
	declare @RegistryValue varchar(100)

	if @CheckIntervalInMinutes is not null
	begin
		select @RegistryValue = admin_all.fn_GetRegistry('DataFeed.LastVerificationTime')
		if @RegistryValue is null
		begin
			select @RegistryValue = convert(varchar(100),dateadd(minute, -(@CheckIntervalInMinutes + 10),getdate()), 120)
			exec admin_all.usp_SetRegistry 'DataFeed.LastVerificationTime', @RegistryValue
			select @RegistryValue = admin_all.fn_GetRegistry('DataFeed.LastVerificationTime')
		end
		if dateadd(minute, @CheckIntervalInMinutes, convert(datetime, @RegistryValue, 120)) > getdate()
			return
	end

	select ID, brand_ID 
		into #1
	from admin_all.ACCOUNT_TRAN
	where DATETIME >=@d1 and DATETIME < @d2

	delete a
	from #1 a
	where exists(select * from admin_all.DataFeed b where a.brand_id = b.BrandID and a.id = b.AccountTranID)

	insert into admin_all.DataFeedMissingRecordLog (LogDate, DateFrom, DateTo, Count)
		select getdate(), @d1, @d2, count(*) 
		from #1
		having count(*)  > 0

	if @Autofix = 0
	begin
		select count(*) from #1
		goto ___End___
	end 

	if not exists(select * from #1)
	begin
		print 'InSync'
		goto ___End___
	end
	insert into admin_all.DataFeed(
										BrandID, AccountTranID, Datetime, PartyID, UserID, 
										Currency, ProductID, ProductCode, ProductTranID, GameInfoID, 
										GameID, GameTranID, TranType, AmountReal, AmountPlayableBonus, 
										AmountReleasedBonus, BalanceReal, BalancePlayableBonus, BalanceReleasedBonus, 
										RollbackTranID, RollbackTranType
									)
			select
					u.BRANDID BrandID, t.ID AccountTranID, t.DATETIME Datetime, a.PARTYID PartyID, u.USERID UserID, 
					u.CURRENCY Currency, 

					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.PLATFORM_ID, t.PLATFORM_ID) else t.PLATFORM_ID end ProductID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(rp.CODE, p.CODE) else p.CODE end ProductCode, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.PLATFORM_TRAN_ID, t.PLATFORM_TRAN_ID) else t.PLATFORM_TRAN_ID end ProductTranID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(rgi.id, gi.ID) else gi.ID end GameInfoID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.GAME_ID, t.GAME_ID) else t.GAME_ID end GameID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.GAME_TRAN_ID, t.GAME_TRAN_ID) else t.GAME_TRAN_ID end GameTranID, 
					
					t.TRAN_TYPE TranType, t.AMOUNT_REAL AmountReal, t.AMOUNT_PLAYABLE_BONUS AmountPlayableBonus, 
					t.AMOUNT_RELEASED_BONUS AmountReleasedBonus, t.BALANCE_REAL BalanceReal, t.BALANCE_PLAYABLE_BONUS BalancePlayableBonus, t.BALANCE_RELEASED_BONUS BalanceReleasedBonus, 
					t.ROLLBACK_TRAN_ID RollbackTranID, r.TRAN_TYPE RollbackTranType
			from admin_all.ACCOUNT_TRAN t
				left join admin_all.ACCOUNT_TRAN r ON t.ROLLBACK_TRAN_ID = r.ID
				left join admin_all.ACCOUNT a ON t.ACCOUNT_ID = a.id
				left join external_mpt.USER_CONF u ON u.PARTYID = a.PARTYID
				left join admin_all.GAME_INFO gi on gi.GAME_ID = t.GAME_ID AND gi.PLATFORM_ID = t.PLATFORM_ID
				left join admin_all.platform p ON p.ID = t.PLATFORM_ID

				left join admin_all.GAME_INFO rgi on rgi.GAME_ID = r.GAME_ID AND rgi.PLATFORM_ID = r.PLATFORM_ID
				left join admin_all.platform rp ON rp.ID = r.PLATFORM_ID
			where t.ID in (select ID from #1)
			option(loop join)
___End___:
	if @CheckIntervalInMinutes is not null
	begin
		select @RegistryValue = convert(varchar(100),getdate(), 120)
		exec admin_all.usp_SetRegistry 'DataFeed.LastVerificationTime', @RegistryValue
	end
end 
go
