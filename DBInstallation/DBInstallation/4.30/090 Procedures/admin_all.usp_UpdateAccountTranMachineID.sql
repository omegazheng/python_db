set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_UpdateAccountTranMachineID
(
	@AccountTranID bigint,
	@MachineID int,
	@PlatformTranID nvarchar(100)
)
as
begin
	set nocount, xact_abort on 
	
	update t
		set t.MachineID = @MachineID, t.PLATFORM_TRAN_ID = @PlatformTranID
	from admin_all.ACCOUNT_TRAN t
	where t.ID = @AccountTranID
		and (
				isnull(t.MachineID, 0) <> isnull(@MachineID, 0)
				or
				isnull(t.PLATFORM_TRAN_ID, '') <> isnull(@PlatformTranID, '')
			) 

end
go
