set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_InsertNewGameWithUnknownGameCategory
	(
		@GameID nvarchar(100),
		@ProductID int = null,
		@ProductCode nvarchar(20) = null
	)
as
	begin
		Declare @GameCategoryId int
		select @GameCategoryId  = id from admin_all.GAME_CATEGORY where name = 'Unknown'
		if @GameCategoryId is null
			BEGIN
				INSERT INTO admin_all.GAME_CATEGORY (NAME) VALUES ('Unknown')
				Select @GameCategoryId = SCOPE_IDENTITY()
			END

		Declare @GameInfoID int

		if @ProductID is null
		begin
			select @ProductID = ID from admin_all.PLATFORM where CODE = @ProductCode
		END

		select @GameInfoID = id from admin_all.GAME_INFO where game_id = @GameID and PLATFORM_ID = @ProductID
		if @GameInfoID is null
		BEGIN
			INSERT INTO admin_all.GAME_INFO (PLATFORM_ID, GAME_ID, NAME, GAME_CATEGORY_ID, GAME_LAUNCH_ID)
			VALUES (@ProductID, @GameID, @GameID,@GameCategoryId, @GameID)
			Select @GameInfoID = SCOPE_IDENTITY()
		END
		SELECT @GameInfoID as GameInfoID
		end

go
