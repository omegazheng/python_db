set ansi_nulls, quoted_identifier on
go

-- get deposit, withdrawal by date, brand, currency
-- trace back to 32 days
create or alter procedure [admin_all].[omegasp_getAccountingByDate]
  (
    @date   DATETIME,
    @staffid INT
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @endDateLocal = DATEADD(DD, 1, @date)
    SET @startDateLocal = DATEADD(DAY, -32, CAST(GETDATE() AS DATE))

    SELECT
      'WITHDRAWAL' AS              TRAN_TYPE,
      CAST(p.REQUEST_DATE AS DATE) DATE,
      u.BRANDID                    BRAND_ID,
      u.CURRENCY,
      SUM(p.AMOUNT_REAL)           AMOUNT_REAL,
      0                            AMOUNT_PLAYABLE_BONUS,
      SUM(p.AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
    FROM admin_all.PAYMENT p
      WITH ( NOLOCK )
      JOIN admin_all.ACCOUNT
        ON p.ACCOUNT_ID = ACCOUNT.id
      JOIN EXTERNAL_MPT.USER_CONF
           u ON u.PARTYID = ACCOUNT.PARTYID
    WHERE
      p.TYPE = 'WITHDRAWAL' AND p.STATUS = 'COMPLETED'
      AND p.REQUEST_DATE >= @startDateLocal
      AND p.REQUEST_DATE < @endDateLocal
      AND u.BRANDID IN (SELECT BRANDID
                        FROM [admin_all].[STAFF_BRAND_TBL]
                        WHERE STAFFID = @staffid)
    GROUP BY CAST(p.REQUEST_DATE AS DATE), u.CURRENCY, u.BRANDID

    UNION

    SELECT
      'DEPOSIT'                    TRAN_TYPE,
      CAST(p.REQUEST_DATE AS DATE) DATE,
      u.BRANDID                    BRAND_ID,
      u.CURRENCY,
      SUM(p.AMOUNT_REAL)           AMOUNT_REAL,
      0                            AMOUNT_PLAYABLE_BONUS,
      SUM(p.AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
    FROM PAYMENT p
      WITH ( NOLOCK )
      JOIN ACCOUNT
        ON p.ACCOUNT_ID = ACCOUNT.id
      JOIN EXTERNAL_MPT.USER_CONF u
        ON u.PARTYID = ACCOUNT.PARTYID
    WHERE
      p.TYPE = 'DEPOSIT' AND p.STATUS = 'COMPLETED'
      AND p.REQUEST_DATE >= @startDateLocal
      AND p.REQUEST_DATE < @endDateLocal
    GROUP BY CAST(p.REQUEST_DATE AS DATE), u.CURRENCY, u.BRANDID

  END

go
