set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_UpdateJob 
(
	@JobName nvarchar(400),
	@Action nvarchar(20),--Start, Complete, Fail, Terminate, Remove,Enable, Disable
	@Message nvarchar(max) = null,
	@JobLocation nvarchar(20) = 'Omega' -- can be Omega, SQL
)
as
begin
	set nocount, xact_abort on
	
	if @JobLocation = 'Omega'
	begin
		if @Action not in ('Start', 'Complete', 'Fail', 'Terminate', 'Remove')
		begin 
			raiserror('Only Start, Complete, Fail, Terminate, and Remove actions are supported for Omega jobs', 16, 1)
			goto ___End___
		end 
		begin transaction
		exec sp_getapplock N'admin_all.usp_setJobLockStatus', N'Exclusive', 'Transaction', -1
		
		declare @JobTimeoutHours int = 3
		-- if the job lasts longer than 3 hours, we will rename the job
		if exists(
					select * 
					from admin_all.JobStatus 
					where JobName = @JobName 
						and Status = 'Running'
						and StartDate < dateadd(hour, -@JobTimeoutHours, getdate())
				)
		begin
			select @Message = 'Job took longer than 3 hours. It''s terminated by system.'
			
			update admin_all.JobStatus 
				set Status = 'Terminated',
					EndDate = getdate(),
					Message = @Message
			where JobName = @JobName
			if @JobName = 'ngr-update'
			begin
				delete from agency.job_progress
			end
		end
		if @Action in ('Start', 'Complete', 'Fail', 'Terminate')
			and (not exists(select * from admin_all.JobStatus where JobName = @JobName))
		begin
			insert into admin_all.JobStatus (JobName, Status, StartDate)
				values(@JobName, 'Created', getdate())
		end

		if @Action = 'Start'
		begin
			if exists(select * from admin_all.JobStatus where JobName = @JobName and Status = 'Running')
			begin
				raiserror('Job %s is already locked', 16, 1, @JobName)
				goto ___End___
			end
			update admin_all.JobStatus 
				set Status = 'Running' , 
					StartDate = getdate(),
					EndDate = null, 
					Message = @Message
			where JobName = @JobName
		end
		else if (@Action in ('Complete', 'Fail', 'Terminate', 'Remove'))
		begin
			if exists(select * from admin_all.JobStatus where JobName = @JobName and Status not in ('Complete', 'Fail', 'Terminate', 'Remove'))
			begin
				update admin_all.JobStatus 
					set Status = case @Action 
									when 'Complete' then 'Completed'
									when 'Fail' then 'Failed'
									when 'Terminate' then 'Terminated' 
									when 'Remove' then 'Removed' 
								end,
						EndDate = getdate(),
						Message = @Message
				where JobName = @JobName
			end
		end
		goto ___End___
	end
	--- when  it's a SQL Job
	if @Action not in ('Start', 'Terminate', 'Remove', 'Enable', 'Disable')
	begin 
		raiserror('Only Start, Terminate, Remove, Enable, and Disable actions are supported for SQL jobs', 16, 1)
		goto ___End___
	end
	begin try
		if not exists(select * from msdb.dbo.sysjobs where name = @JobName)
		begin
			raiserror('Could not find job %s.', 16, 1, @JobName)
			goto ___End___
		end
		if @Action = 'Start'
		begin
			exec msdb.dbo.sp_start_job @JobName
		end
		else if @Action = 'Terminate'
		begin
			exec msdb.dbo.sp_stop_job @JobName
		end
		else if @Action = 'Enabled'
		begin
			exec msdb.dbo.sp_update_job @job_name = @JobName, @enabled = 1
		end
		else if @Action = 'Disable'
		begin
			exec msdb.dbo.sp_update_job @job_name = @JobName, @enabled = 0
		end
	end try
	begin catch
		select @Message = error_message()
		if @Action = 'Start'
		begin
			raiserror('Job %s is already locked. Original error message: %s', 16, 1, @JobName, @Message)
			goto ___End___
		end
	end catch
___End___:
	if @@trancount > 0
		commit
end
go
