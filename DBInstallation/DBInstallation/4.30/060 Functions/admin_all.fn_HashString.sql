set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_HashString (@String nvarchar(max))
returns uniqueidentifier
as
begin
	return hashbytes('MD5', cast(upper(@String) as nvarchar(max)))
end

go
