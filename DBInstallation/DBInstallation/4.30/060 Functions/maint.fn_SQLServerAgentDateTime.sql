set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_SQLServerAgentDateTime(@date int, @time int)
returns datetime
as
begin
 return
  (
    convert(datetime,
          convert(nvarchar(4),@date / 10000) + N'-' + 
          convert(nvarchar(2),(@date % 10000)/100)  + N'-' +
          convert(nvarchar(2),@date % 100) + N' ' +        
          convert(nvarchar(2),@time / 10000) + N':' +        
          convert(nvarchar(2),(@time % 10000)/100) + N':' +        
          convert(nvarchar(2),@time % 100),
    120)
  )
end

go
