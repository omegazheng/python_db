set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetMD5Hash(@Data varbinary(max))
RETURNS uniqueidentifier
AS
BEGIN
	RETURN hashbytes('MD5', @Data)
END

go
