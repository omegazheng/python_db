set ansi_nulls, quoted_identifier on
go



create or alter function [agency].[fn_GetPaymentSumByTypeAndAgentAndDateRange]
    (@type varchar(10), @agent_id int, @start_date DATETIME, @end_date DATETIME) returns numeric(38,18)
AS
  BEGIN
    declare @value numeric(38,18)
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)

    if @user_type = 0
        set @value = (
            select sum(amount_real)
            from external_mpt.user_conf as player
                join admin_all.account as account on player.partyid = account.partyid
                    join admin_all.account_tran on account_tran.account_id = account.id
            where
                player.partyid = @agent_id

                and datetime >= @start_date and datetime < @end_date
                and tran_type = @type
                and reference is null
        )
    else
    begin
        declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);

        declare @subagents TABLE(partyid int)
        insert into @subagents
            select
                partyid
            from external_mpt.user_conf as player
            where
                player.user_type = 0
                and player.parentid is not null
                and player.hierarchy.IsDescendantOf(@agentHierarchy) = 1

        declare @accounts table (account_id int)
        insert into @accounts(account_id)
            select account.id
            from admin_all.account
            where
                account.partyid in (select partyid from @subagents)

        set @value = (
            select sum(amount_real)
            from admin_all.account_tran
            where
                datetime >= @start_date and datetime < @end_date
                and tran_type = @type
                and reference is null
                and account_id in (select * from @accounts)
        )
    end

    if @value is null
        set @value = 0

    return @value
  END

go
