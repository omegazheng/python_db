set ansi_nulls, quoted_identifier on
go



create or alter function [agency].[fn_GetTopTenEntryByAgentAndDateRange]
    (@start_date DATETIME, @end_date DATETIME) returns TABLE
AS
  return
    select
        partyid,
        parentid,
        user_type as userType,
        userid as userid,
        first_name + ' ' + last_name as name,
        hierarchy,
        [agency].[fn_GetNgrByAgentAndDateRange](partyid, @start_date, @end_date) as ngr,
        [agency].[fn_GetSignupsByAgentAndDateRange](partyid, @start_date, @end_date) as signups,
        [agency].[fn_GetActivesByAgentAndDateRange](partyid, @start_date, @end_date)  as actives

    from external_mpt.user_conf
    where user_type <> 0


go
