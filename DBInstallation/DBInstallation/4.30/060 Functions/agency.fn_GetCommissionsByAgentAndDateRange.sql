set ansi_nulls, quoted_identifier on
go
create or alter function agency.fn_GetCommissionsByAgentAndDateRange
    (@agent_id int, @start_date DATETIME, @end_date DATETIME) returns numeric(38,18)
AS
  BEGIN
    declare @value numeric(38,18)

    set @value = (
        select sum(value)
        from agency.commission_payment
        where agent_id = @agent_id
            and start_date >= @start_date
            and end_date <= @end_date
            and state not like '%REJECTED'
    )

    if @value is null
        set @value = 0

    return @value
  END

go
