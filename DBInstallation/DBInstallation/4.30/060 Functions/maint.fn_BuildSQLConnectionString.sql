set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_BuildSQLConnectionString(@InstanceName varchar(128), @DatabasName varchar(128), @UserName varchar(128), @Password varchar(128))
returns varchar(8000)
as
begin
	declare @Ret varchar(8000) 
	if @InstanceName is null
		return null
	select @Ret = 'Application Name=Omega Utility;Pooling=true;Enlist=False;Data Source='+@InstanceName
	if @DatabasName is not null
		select @Ret = @Ret +  ';Initial Catalog='+@DatabasName
	if @UserName is not null
	begin
		select @Ret = @Ret + ';Integrated Security=False;User ID='+@UserName
		if @Password is not null
			select @Ret = @Ret + ';Password='+@Password
	end
	else
	begin
		select @Ret = @Ret + ';Integrated Security=True;'
	end
	return @Ret;
end

go
