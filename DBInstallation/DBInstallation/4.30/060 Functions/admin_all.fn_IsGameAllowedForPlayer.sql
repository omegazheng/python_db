set ansi_nulls, quoted_identifier on
go


create or alter function [admin_all].[fn_IsGameAllowedForPlayer]
  (@partyId INT, @gameId NVARCHAR(250), @platformCode NVARCHAR(100))
  RETURNS BIT
AS
  BEGIN
    DECLARE @count INT = 0;
    DECLARE @isAllowed BIT = 1;

    set @count = (select count(*) from admin_all.game_info_excluded_brand
      where GAME_INFO_ID = (
        select GAME_INFO.id from game_info
          join platform on game_info.PLATFORM_ID = platform.id where GAME_ID = @gameId and platform.CODE = @platformCode)
            and BRAND_ID = (select brandid from external_mpt.USER_CONF where PARTYID = @partyId)
    )

    Set @isAllowed = (SELECT CASE WHEN @count <= 0 THEN 1 ELSE 0 END)
    RETURN @isAllowed;
  END

go
