set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[fn_IsTransfer]
    (@payment_id int, @amount numeric(38,18), @process_date datetime, @type varchar(25), @player_parent_id int) returns int
AS
  BEGIN
    if @type = 'DEPOSIT'
    begin
        if exists (
            select *
            from admin_all.account
                join admin_all.account_tran on account_tran.account_id = account.id
            where
                account.partyid = @player_parent_id
                and tran_type = 'P_DEPOSIT'
                and amount_real = -@amount
                and datetime >= @process_date
                and datetime < dateadd(millisecond, 777, @process_date)
        )
            return 1;
    end

    if @type = 'WITHDRAWAL'
    begin
        if exists (
            select *
            from admin_all.account
                join admin_all.account_tran on account_tran.account_id = account.id
            where
                account.partyid = @player_parent_id
                and tran_type = 'P_WITHDRAW'
                and amount_real = @amount
                and datetime >= @process_date
                and datetime < dateadd(millisecond, 777, @process_date)
        )
            return 1;
    end

    return 0;
  END

go
