set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_StartOfMonth(@Date date) 
returns date
as
begin
	return dateadd(month, datediff(month, '2000-01-01', @Date), '2000-01-01')
end

go
