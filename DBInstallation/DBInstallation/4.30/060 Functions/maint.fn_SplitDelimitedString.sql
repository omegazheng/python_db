set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_SplitDelimitedString(@String varchar(max), @Delimiter varchar(20))
returns table
as
return (
	with x0 as
	(
		select cast( '<R>' + replace(@String, @Delimiter, '</R><R>') + '</R>' as xml) c
	), x1 as 
	(
		select rtrim(ltrim(c1.value('.', 'varchar(max)'))) Value, row_number() over ( order by ( select 1 ) ) ID
		from x0
			cross apply x0.c.nodes('/R') n(c1)
	)
	select ID, Value from x1
)


go
