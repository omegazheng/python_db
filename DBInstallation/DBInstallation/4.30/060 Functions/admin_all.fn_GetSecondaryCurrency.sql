set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetSecondaryCurrency() 
returns bit 
as 
begin 
	return nullif(rtrim(ltrim(admin_all.fn_GetRegistry('multi.currency.secondary'))), '')
end


go
