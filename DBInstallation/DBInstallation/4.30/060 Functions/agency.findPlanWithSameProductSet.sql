set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[findPlanWithSameProductSet]
(@agent_id INT, @plan_id INT)
RETURNS INT
AS
BEGIN
  DECLARE @id INT;
  SELECT TOP 1 @id = commission.id
  FROM admin_all.commission
    JOIN admin_all.user_commission ON user_commission.commission_id = commission.id
    JOIN admin_all.commission_platform ON commission_platform.commission_id = commission.id
  WHERE
    user_commission.partyid = @agent_id
    AND commission_platform.platform_id IN (SELECT platform_id
                                            FROM admin_all.commission_platform
                                            WHERE commission_id = @plan_id)
    AND COMMISSION.STATE = 'ACTIVE'

  GROUP BY commission.id
  HAVING count(commission_platform.platform_id) = (SELECT count(platform_id)
                                                   FROM admin_all.commission_platform
                                                   WHERE commission_id = @plan_id)
  RETURN @id
END

go
