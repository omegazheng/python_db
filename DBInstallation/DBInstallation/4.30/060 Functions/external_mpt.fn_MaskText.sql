set ansi_nulls, quoted_identifier on
go

create or alter function [external_mpt].[fn_MaskText]
    (@String varchar(255)) returns varchar(255)
AS
  BEGIN
    select @String = replace(
                      replace(
                       replace(
                        replace(
                         replace(
                          replace(
                           replace(
                            replace(
                             replace(
                              replace(
                               replace(
                                replace(
                                 replace(
                                  replace(
                                   replace(
                                    replace(
                                     replace(
                                      replace(
                                        replace(
                                         replace(
                                          replace(@String,'o','e'),
                                                          'a','o'),
                                                          'i','a'),
                                                          'u','i'),
                                                          't','p'),
                                                          'm','x'),
                                                          'c','k'),
                                                          '0','3'),
                                                          '1','4'),
                                                          '2','5'),
                                                          '3','1'),
                                                          '4','2'),
                                                          '5','9'),
                                                          '6','6'),
                                                          '7','0'),
                                                          '8','7'),
                                                          '9','8'),
                                                          'd','th'),
                                                          'ee','e'),
                                                          'oo','or'),
                                                          'll','ii')
    return @String
  END

go
