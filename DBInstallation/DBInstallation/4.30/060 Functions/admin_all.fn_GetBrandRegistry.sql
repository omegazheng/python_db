set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetBrandRegistry (@BrandID int, @Key nvarchar(255))
returns nvarchar(max)
as
begin
	return isnull((select VALUE from admin_all.BRAND_REGISTRY where MAP_KEY = @Key and BRANDID = @BrandID), (select VALUE from admin_all.REGISTRY_HASH where MAP_KEY = @Key))
end


go
