set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_IsChangeTrackingEnabled(@ConnectionString varchar(max))
returns bit
as
begin
	return maint.fn_ExecuteSQLScalarString(@ConnectionString, 'select case when exists(select * from sys.change_tracking_databases where database_id = db_id()) then 1 else 0 end')
end

go
