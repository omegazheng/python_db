set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_IsMultiCurrencyEnabled() 
returns bit 
as 
begin 
	return case when isnull(rtrim(ltrim(admin_all.fn_GetRegistry('multi.currency.enabled'))), 'false') = 'true' then 1 else 0 end
end


go
