set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[minimumCriteriaAreSatisfied]
    (@candidate numeric(38,18), @agent_id int, @plan_id int, @start_date DATETIME, @end_date DATETIME) returns bit
AS
BEGIN
	declare @min_bet_count int
	declare @total_bet_count int
    declare @min_wager numeric(38,18)
    declare @total_wager numeric(38,18)

    set @min_bet_count = 0
    set @min_bet_count = (
        select cast(p.value as int)
        from admin_all.commission_parameter as p
            join admin_all.commission_structure as s on p.commission_structure_id = s.id
        where type = 'minBets'
            and s.commission_id = @plan_id
    )
    set @min_wager = 0
    set @min_wager = (
        select p.value
        from admin_all.commission_parameter as p
            join admin_all.commission_structure as s on p.commission_structure_id = s.id
        where type = 'minWager'
            and s.commission_id = @plan_id
    )

    select  @total_wager = sum(d.bet_amount),
            @total_bet_count = sum(d.settled_event_count)
        from admin_all.betting_detail as d
            join admin_all.account_tran as t on d.account_tran_id = t.id
                join admin_all.account as a on t.account_id = a.id
                    join external_mpt.user_conf as u on a.partyid = u.partyid
        where d.settlement_date >= @start_date
            and d.settlement_date < @end_date
            and t.platform_id in (select platform_id from admin_all.commission_platform where commission_id = @plan_id)
            and u.parentid = @agent_id
        group by u.parentid

    if @total_bet_count < @min_bet_count OR @total_wager < @min_wager
        return 0

    return 1

END

go
