set ansi_nulls, quoted_identifier on
go
create or alter function external_mpt.PartyIsSubordinateOf(@ChildPartyID int, @ParentPartyID int) 
returns bit 
as 
begin
	return
			case 
				when @ParentPartyID is null or @ChildPartyID is null then null
				when @ParentPartyID = @ChildPartyID then 0
				else 
					(
						select top 1 x1.Hierarchy.IsDescendantOf(x0.Hierarchy)
						from (
								select Hierarchy, Level
								from external_mpt.USER_CONF
								where PartyID = @ParentPartyID
							) x0
							cross join  (
								select Hierarchy, Level
								from external_mpt.USER_CONF
								where PartyID = @ChildPartyID
							) x1
					)		
			end
end
go
