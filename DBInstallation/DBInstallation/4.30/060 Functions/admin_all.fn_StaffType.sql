set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_StaffType(@StaffID int)
returns varchar(10)
as
begin
	if exists(select * from admin_all.StaffCompany where StaffID = @StaffID)
		return 'Company'
	if exists(select * from admin_all.StaffProduct where StaffID = @StaffID)
		return 'Product'
	return 'Other'
end
go
