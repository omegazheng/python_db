set ansi_nulls, quoted_identifier on
go

-- NOTE: better to set datefirst before use this function
-- e.g., SET DATEFIRST 1
create or alter function [admin_all].[fn_GetWeekOfYear]
  (@date DATETIME)
  RETURNS INT
AS
  BEGIN
    DECLARE @FirstDayOftheYear DATE, @value INT
    SELECT @FirstDayOftheYear = dateadd(YY, datediff(YY, '1900-01-01', @date), '1900-01-01')
    SET @value = (SELECT datepart(WK, @date) - CASE WHEN datepart(WEEKDAY, @FirstDayOftheYear) <> 1
      THEN 1 ELSE 0 END)

    RETURN @value
  END

go
