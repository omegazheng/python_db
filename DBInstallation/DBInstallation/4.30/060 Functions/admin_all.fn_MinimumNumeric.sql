set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.[fn_MinimumNumeric](@p1 NUMERIC(38, 6), @p2 NUMERIC(38, 6))
RETURNS NUMERIC(38, 6)
AS
BEGIN
  RETURN CASE WHEN @p1 < @p2
    THEN @p1
         ELSE @p2 END
END

go
