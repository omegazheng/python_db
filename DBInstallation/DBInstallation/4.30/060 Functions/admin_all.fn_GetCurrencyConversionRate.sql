set ansi_nulls, quoted_identifier on
go

create or alter function admin_all.fn_GetCurrencyConversionRate(@Date date, @BaseCurrency nchar(3), @ToCurrency nchar(3)) 
returns numeric(38,18)
as
begin
	if @BaseCurrency = @ToCurrency
		return 1
	return (
				select top 1 RATE
				from admin_all.CURRENCY_CONV 
				inner join admin_all.CURRENCY_CONV_RATE on CURRENCY_CONV.ID = CURRENCY_CONV_RATE.CURRENCY_CONV_ID
				where DATE < dateadd(day, 1, @Date) 
					and TO_CURRENCY = @ToCurrency
					and BASE_CURRENCY = @BaseCurrency
				order by DATE desc
		)
end

go
