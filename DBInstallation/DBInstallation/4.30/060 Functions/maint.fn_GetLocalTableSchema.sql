set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetLocalTableSchema(@ObjectName sysname)
returns table
as
return
(
	select cast(c.object_id as int) ObjectID, 
		object_schema_name(c.object_id) SchemaName,
		object_name(c.object_id) ObjectName,
		type_name(c.user_type_id) BaseType,
		c.name ColumnName, cast(c.column_id as int) ColumnID, cast(isnull(ic.key_ordinal,0) as int) PrimaryKeyOrder, c.max_length DataLength,
		case 
			when type_name(c.user_type_id) = 'timestamp' then 'binary(8)' 
			when type_name(c.user_type_id) = 'text' then 'varchar(max)' 
			when type_name(c.user_type_id) = 'ntext' then 'nvarchar(max)' 
			when type_name(c.user_type_id) = 'image' then 'varbianry(max)' 
			else type_name(c.user_type_id) 
		end +
		case 
			when type_name(c.user_type_id) in ('binary', 'char', 'nchar') then '('+cast(c.max_length as varchar(20))+')'
			when type_name(c.user_type_id) in ('nchar') then '('+cast(c.max_length/2 as varchar(20))+')'
			when type_name(c.user_type_id) in ('datetime2', 'datetimeoffset', 'time') then '('+cast(c.scale as varchar(20))+')'
			when type_name(c.user_type_id) in ('decimal', 'numeric') then '('+cast(c.precision as varchar(20))+','+cast(c.scale as varchar(20))+')'
			when type_name(c.user_type_id) in ('varchar') then '('+case when c.max_length = -1 then 'max' else cast(c.max_length as varchar(20)) end+')'
			when type_name(c.user_type_id) in ('nvarchar') then '('+case when c.max_length = -1 then 'max' else cast(c.max_length/2 as varchar(20)) end+')'
			when type_name(c.user_type_id) in ('varbinary') then '('+case when c.max_length = -1 then 'max' else cast(c.max_length as varchar(20)) end+')'
			else ''
		end DataType,
		cast(c.is_nullable as bit) Nullable,
		cast(case when c.is_identity = 1 or c.is_computed = 1 or type_name(c.user_type_id) = 'timestamp' then 1 else 0 end as bit) IsReadOnly,
		c.is_identity IsIdentity,
		c.is_computed IsComputed,
		case when type_name(c.user_type_id) = 'timestamp' then 1 else 0 end IsTimestamp,
		c.collation_name as CollationName
	from sys.all_columns c 
		left outer join sys.indexes i on i.object_id = c.object_id and i.is_primary_key = 1
		left outer join sys.index_columns ic on ic.object_id = c.object_id and ic.index_id = i.index_id and ic.column_id = c.column_id
	where c.object_id = object_id(@ObjectName)
)

go
