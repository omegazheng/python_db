set ansi_nulls, quoted_identifier on
go

create or alter function [agency].[fn_GetSportBetsByAgentAndDateRange]
    (@agent_id int, @start_date DATETIME, @end_date DATETIME) returns numeric(38,18)
AS
  BEGIN
    declare @value numeric(38,18)
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)

    if @user_type = 0
        set @value = (
            select sum(bets)
            from agency.player_ggr
                join admin_all.platform on platform.id = player_ggr.product_id
            where player_id = @agent_id
                and date >= @start_date and date < @end_date
                and platform_type = 'SPORTSBOOK'
        )
    else
        set @value = (
            select sum(bets)
            from (
                select 'direct' type, direct_ggr.id, bets
                from agency.direct_ggr
                    join admin_all.platform on platform.id = direct_ggr.product_id
                where agent_id = @agent_id
                    and date >= @start_date and date < @end_date
                    and platform_type = 'SPORTSBOOK'

                union

                select 'network' type, network_ggr.id, bets
                from agency.network_ggr
                    join admin_all.platform on platform.id = network_ggr.product_id
                where agent_id = @agent_id
                    and date >= @start_date and date < @end_date
                    and platform_type = 'SPORTSBOOK'
            ) x
        )

    if @value is null
        set @value = 0

    return @value
  END

go
