set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetReplicationErrors(@SessionID smallint)
returns table
as
return (
select c.ConfigurationID, c.Name, Error
from maint.ReplicationLastExecution l
	inner join maint.ReplicationConfiguration c on c.ConfigurationID = l.ConfigurationID
where Error is not null and SessionID = @SessionID
)

go
