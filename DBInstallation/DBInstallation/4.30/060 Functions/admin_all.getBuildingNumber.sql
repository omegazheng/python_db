set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.getBuildingNumber(@address varchar(1000))
  RETURNS varchar(40)
AS
  BEGIN
-- Determine the flat number
--declare @address varchar(1000) = 'Flat / 2 79 Harrow road'
declare @start_position int, @end_position int, @firstDelimiterIndex int
declare @temp_string varchar(200),  @flatNumber VARCHAR(40), @buildingNumber varchar(40)
declare @length int
declare @comma_position int

select @start_position = PATINDEX('%flat%', @address)
-- try to identify all the strings after flat
select @temp_string = LTRIM(SUBSTRING(@address, @start_position + 4, LEN(@address)))
 --print 'temp_string after trimming flat away is ' + @temp_string
select @start_position = PATINDEX('%[0-9a-zA-Z]%', @temp_string)
 --print @start_position
select @end_position = PATINDEX('%[ ,]%', @temp_string)
 --print @end_position
select @length = @end_position - @start_position
 --print 'length' + cast(@length as varchar)
if @length > 0
  BEGIN
    select @flatNumber = SUBSTRING(@temp_string, @start_position, @length)
  END
ELSE
  select @flatNumber = ''
--print 'flat number is ' + CAST(@flatNumber AS VARCHAR)




---------- Try to Determine the building number
-- declare @address varchar(1000) = 'Flat / 2 79 Harrow road'
-- declare @start_position int, @end_position int, @firstDelimiterIndex int
-- declare @temp_string varchar(200),  @flatNumber VARCHAR(40), @buildingNumber varchar(40)
-- declare @length int
-- declare @comma_position int

--print @temp_string

select @temp_string = REVERSE(@address)
-- first identify the number index as starting position

select @start_position = PATINDEX('%[0-9]%', @temp_string)
--- Calculate the index for white space before the number, this is used to cover if building number is like 22A, we would like to include the entire 22A
declare @white_space_before_number_index int
select @white_space_before_number_index = @start_position
while (@white_space_before_number_index > 0)
  BEGIN
    --PRINT @white_space_before_number_index
    SELECT @white_space_before_number_index = @white_space_before_number_index - 1
    IF CHARINDEX(' ', SUBSTRING(@temp_string,@white_space_before_number_index, 1)) > 0
      BEGIN
        --print 'white space index before number is ' + cast(@white_space_before_number_index as VARCHAR)
        BREAK
      END
  END
SELECT @start_position =  @white_space_before_number_index
-- print '@start_position =' + cast(@start_position as varchar)

-- attempt to identify the comma
select @end_position = PATINDEX('%[,]%', @temp_string)
IF @end_position = 0
    BEGIN
--       PRINT 'Comma is not found, trying to identify the next space char index...'
      declare @remaining_string VARCHAR(1000)
      SELECT @remaining_string = LTRIM(SUBSTRING(@temp_string, @start_position, LEN(@temp_string)))
--       print 'remaining string is ' + @remaining_string
      --select @start_position = 0
      select @end_position = CHARINDEX(' ', @remaining_string)
--       print '@end_position =' + cast(@end_position as varchar)
      select @length = @end_position
      select @buildingNumber = LTRIM(SUBSTRING(@temp_string, @start_position, @length))
    END
-- NORMAL CASE
ELSE
  BEGIN
--     print 'comma found as seperator'
--     print '@end_position =' + cast(@end_position as varchar);
    select @length = @end_position - @start_position
    IF @length  > 0
      select @buildingNumber = SUBSTRING(@temp_string, @start_position, @length)
    ELSE
      SELECT @buildingNumber = '';
  END

select @buildingNumber = LTRIM(REVERSE(@buildingNumber))
--print 'Building Number is ' + CAST(@buildingNumber AS VARCHAR)


IF @flatNumber = @buildingNumber
    BEGIN
--       print 'flatNumber is the same as buildingNumber'
      IF CHARINDEX('-', @buildingNumber) > 0
        BEGIN
--           print 'building number contains -'
          select @start_position = CHARINDEX('-', @buildingNumber);
          select @length = LEN(@buildingNumber) - @start_position;
          if @length > 0
            BEGIN
            SELECT @buildingNumber = SUBSTRING(@buildingNumber, @start_position, @length)
            END
        END
    END
ELSE
  BEGIN
--     print 'flatNumber is different from buildingNumber'
    IF CHARINDEX('-', @buildingNumber) > 0
      BEGIN
--         print 'building number contains -'
        select @start_position = 0;
        select @end_position = CHARINDEX('-', @buildingNumber);
        select @length = @end_position;
        SELECT @buildingNumber = SUBSTRING(@buildingNumber, @start_position, @length)
      END
  END
--print 'Building Number is ' + CAST(@buildingNumber AS VARCHAR)
return @buildingNumber
END
go
