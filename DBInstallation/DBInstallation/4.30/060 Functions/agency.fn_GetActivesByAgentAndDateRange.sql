set ansi_nulls, quoted_identifier on
go
create or alter function [agency].[fn_GetActivesByAgentAndDateRange]
(
	@agent_id int, 
	@start_date DATETIME, 
	@end_date DATETIME
) returns int
AS
BEGIN
    declare @value int
    declare @user_type int = (select user_type from external_mpt.user_conf where partyid = @agent_id)

    if @user_type = 0
	begin
        set @value = (
            select count(player.partyid)
            from external_mpt.user_conf as player
				inner loop join admin_all.account as account on player.partyid = account.partyid
			where exists(
						select *
						from admin_all.account_tran as t
						where account.id = t.account_id
							and t.datetime >= @start_date and t.datetime < @end_date
						)

            and player.partyid = @agent_id
        )
	end
    else
    begin
        declare @agentHierarchy hierarchyid = (select hierarchy from external_mpt.user_conf where partyid = @agent_id);
        set @value = (
            select count( player.partyid)
            from external_mpt.user_conf as player
				inner loop join admin_all.account as account on account.partyid = player.partyid
            where
                player.partyid <> @agent_id
                and player.user_type = 0
                and player.hierarchy.IsDescendantOf(@agentHierarchy) = 1
                and exists (
							select *
							from admin_all.account_tran as t 
							where t.datetime >= @start_date and t.datetime < @end_date
								and account.id = t.account_id
						 )
        )
    end

	return isnull(@value, 0)
 END

go
