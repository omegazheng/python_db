set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetLargeValue(@ValueID uniqueidentifier)
returns table
as
return
	(
		select ValueID, case when IsCompressed = 1 then decompress(Data) else Data end as Data
		from admin_all.LargeValue
		where ValueID = @ValueID
	)
go
