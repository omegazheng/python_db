set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_STAFF_AUTH_TBL on admin_all.STAFF_AUTH_TBL
for insert, update
as
begin
    if @@rowcount = 0
      return
	set nocount on
	insert into admin_all.STAFF_BRAND_TBL(STAFFID,BRANDID)
		select i.STAFFID, b.BRANDID
		from inserted i
		cross join admin.CASINO_BRAND_DEF b
		where i.SUPER_ADMIN = 1
			and not exists(select *
							from admin_all.STAFF_BRAND_TBL sb
							where sb.BRANDID = b.BRANDID
									and sb.STAFFID = i.STAFFID
							)
	update t
		set t.ACTIVE = 1, t.NON_WL_ALLOWED = 1
	from admin_all.STAFF_AUTH_TBL t
		inner join inserted s on t.STAFFID = s.STAFFID
	where s.SUPER_ADMIN = 1 and (isnull(t.ACTIVE, 0) = 0 or isnull(t.NON_WL_ALLOWED, 0) = 0)

	insert into admin_all.AUTH_USER_ROLE(USER_ID, USER_TYPE, ROLE_ID)
		select i.STAFFID, 'STAFF', 1
		from inserted i
		where i.SUPER_ADMIN = 1
			and not exists(select *
							from admin_all.AUTH_USER_ROLE a
							where i.STAFFID = a.USER_ID
								and a.USER_TYPE = 'STAFF'
								and a.ROLE_ID = 1
							)

	;with x0 as
	(
		select Permission
		from (values('ENABLE_EXPORT'),('SHOW_EMAIL'),('SHOW_MOBILE'),('SHOW_PHONE')) p(Permission)
	),
	x1 as
	(
		select i.STAFFID as UserID, x0.Permission
		from x0
			cross join inserted i
		where i.SUPER_ADMIN = 1
	)
	merge admin_all.AUTH_PERMISSION t
	using x1 as s on t.USER_ID = s.UserID and t.USER_TYPE = 'STAFF' and t.PERMISSION_TYPE = s.Permission
	when matched and t.ENABLE = 0 then
		update set t.ENABLE = 1
	when not matched then
		insert (USER_ID, USER_TYPE, PERMISSION_TYPE, ENABLE)
			values(s.UserID, 'STAFF', s.Permission, 1)
	;
 end

 
go
