set ansi_nulls, quoted_identifier on
go
create or alter trigger [admin_all].[TRI_ACCOUNT_TRAN] on [admin_all].[ACCOUNT_TRAN] 
instead of insert, update, delete
as 
begin 
	if @@rowcount = 0
		return
	set nocount on
	if not exists(select * from inserted) -- delete
	begin
		delete t from admin_all.ACCOUNT_TRAN_REALTIME t inner join deleted s on s.ID = t.ID
		delete t from admin_all.ACCOUNT_TRAN_ALL t inner join deleted s on s.ID = t.ID
	end
	if not exists(select * from deleted) 
	begin
		insert into admin_all.ACCOUNT_TRAN_REALTIME([ACCOUNT_ID],[DATETIME],[TRAN_TYPE],[AMOUNT_REAL],[BALANCE_REAL],[PLATFORM_TRAN_ID],[GAME_TRAN_ID],[GAME_ID],[PLATFORM_ID],[payment_id],[ROLLED_BACK],[ROLLBACK_TRAN_ID],[AMOUNT_RELEASED_BONUS],[AMOUNT_PLAYABLE_BONUS],[BALANCE_RELEASED_BONUS],[BALANCE_PLAYABLE_BONUS],[AMOUNT_UNDERFLOW],[AMOUNT_RAW_LOYALTY],[BALANCE_RAW_LOYALTY],[TRANSACTION_ON_HOLD_ID],[SSW_TRAN_ID],[REFERENCE], BRAND_ID, MachineID)
			select i.[ACCOUNT_ID],i.[DATETIME],i.[TRAN_TYPE],i.[AMOUNT_REAL],i.[BALANCE_REAL],i.[PLATFORM_TRAN_ID],i.[GAME_TRAN_ID],i.[GAME_ID],i.[PLATFORM_ID],i.[payment_id],i.[ROLLED_BACK],i.[ROLLBACK_TRAN_ID],i.[AMOUNT_RELEASED_BONUS],i.[AMOUNT_PLAYABLE_BONUS],i.[BALANCE_RELEASED_BONUS],i.[BALANCE_PLAYABLE_BONUS],i.[AMOUNT_UNDERFLOW],i.[AMOUNT_RAW_LOYALTY],i.[BALANCE_RAW_LOYALTY],i.[TRANSACTION_ON_HOLD_ID],i.[SSW_TRAN_ID],i.[REFERENCE], i.BRAND_ID, i.MachineID
			from inserted i
				--external_mpt.USER_CONF users
				--inner  join admin_all.account on users.PARTYID = account.PARTYID
				--inner  join inserted i on i.ACCOUNT_ID = account.id			
		goto ___Exit___
	end
	-- update
	update a
		set a.[ACCOUNT_ID]=i.[ACCOUNT_ID],a.[DATETIME]=i.[DATETIME],a.[TRAN_TYPE]=i.[TRAN_TYPE],a.[AMOUNT_REAL]=i.[AMOUNT_REAL],a.[BALANCE_REAL]=i.[BALANCE_REAL],a.[PLATFORM_TRAN_ID]=i.[PLATFORM_TRAN_ID],a.[GAME_TRAN_ID]=i.[GAME_TRAN_ID],a.[GAME_ID]=i.[GAME_ID],a.[PLATFORM_ID]=i.[PLATFORM_ID],a.[payment_id]=i.[payment_id],a.[ROLLED_BACK]=i.[ROLLED_BACK],a.[ROLLBACK_TRAN_ID]=i.[ROLLBACK_TRAN_ID],a.[AMOUNT_RELEASED_BONUS]=i.[AMOUNT_RELEASED_BONUS],a.[AMOUNT_PLAYABLE_BONUS]=i.[AMOUNT_PLAYABLE_BONUS],a.[BALANCE_RELEASED_BONUS]=i.[BALANCE_RELEASED_BONUS],a.[BALANCE_PLAYABLE_BONUS]=i.[BALANCE_PLAYABLE_BONUS],a.[AMOUNT_UNDERFLOW]=i.[AMOUNT_UNDERFLOW],a.[AMOUNT_RAW_LOYALTY]=i.[AMOUNT_RAW_LOYALTY],a.[BALANCE_RAW_LOYALTY]=i.[BALANCE_RAW_LOYALTY],a.[TRANSACTION_ON_HOLD_ID]=i.[TRANSACTION_ON_HOLD_ID],a.[SSW_TRAN_ID]=i.[SSW_TRAN_ID],a.[REFERENCE]=i.[REFERENCE],a.[BRAND_ID]=i.[BRAND_ID],a.[MachineID]=i.[MachineID]
	from admin_all.ACCOUNT_TRAN_REALTIME a
		inner join inserted i on a.ID = i.ID
		
	if (select PARSENAME ( base_object_name , 3) from sys.synonyms where object_id = object_id('admin_all.Synonym_ACCOUNT_TRAN_ALL')) is null
		or exists(select * from sys.databases where database_id = db_id((select PARSENAME ( base_object_name , 3) from sys.synonyms where object_id = object_id('admin_all.Synonym_ACCOUNT_TRAN_ALL'))) and is_read_only = 0)
	begin
		update a
			set a.[ACCOUNT_ID]=i.[ACCOUNT_ID],a.[DATETIME]=i.[DATETIME],a.[TRAN_TYPE]=i.[TRAN_TYPE],a.[AMOUNT_REAL]=i.[AMOUNT_REAL],a.[BALANCE_REAL]=i.[BALANCE_REAL],a.[PLATFORM_TRAN_ID]=i.[PLATFORM_TRAN_ID],a.[GAME_TRAN_ID]=i.[GAME_TRAN_ID],a.[GAME_ID]=i.[GAME_ID],a.[PLATFORM_ID]=i.[PLATFORM_ID],a.[payment_id]=i.[payment_id],a.[ROLLED_BACK]=i.[ROLLED_BACK],a.[ROLLBACK_TRAN_ID]=i.[ROLLBACK_TRAN_ID],a.[AMOUNT_RELEASED_BONUS]=i.[AMOUNT_RELEASED_BONUS],a.[AMOUNT_PLAYABLE_BONUS]=i.[AMOUNT_PLAYABLE_BONUS],a.[BALANCE_RELEASED_BONUS]=i.[BALANCE_RELEASED_BONUS],a.[BALANCE_PLAYABLE_BONUS]=i.[BALANCE_PLAYABLE_BONUS],a.[AMOUNT_UNDERFLOW]=i.[AMOUNT_UNDERFLOW],a.[AMOUNT_RAW_LOYALTY]=i.[AMOUNT_RAW_LOYALTY],a.[BALANCE_RAW_LOYALTY]=i.[BALANCE_RAW_LOYALTY],a.[TRANSACTION_ON_HOLD_ID]=i.[TRANSACTION_ON_HOLD_ID],a.[SSW_TRAN_ID]=i.[SSW_TRAN_ID],a.[REFERENCE]=i.[REFERENCE],a.[BRAND_ID]=i.[BRAND_ID],a.[MachineID]=i.[MachineID]
		from admin_all.Synonym_ACCOUNT_TRAN_ALL a
			inner join inserted i on a.ID = i.ID
		goto ___Exit___
	end
	set identity_insert admin_all.ACCOUNT_TRAN_REALTIME on
	insert into admin_all.ACCOUNT_TRAN_REALTIME([ID],[ACCOUNT_ID],[DATETIME],[TRAN_TYPE],[AMOUNT_REAL],[BALANCE_REAL],[PLATFORM_TRAN_ID],[GAME_TRAN_ID],[GAME_ID],[PLATFORM_ID],[payment_id],[ROLLED_BACK],[ROLLBACK_TRAN_ID],[AMOUNT_RELEASED_BONUS],[AMOUNT_PLAYABLE_BONUS],[BALANCE_RELEASED_BONUS],[BALANCE_PLAYABLE_BONUS],[AMOUNT_UNDERFLOW],[AMOUNT_RAW_LOYALTY],[BALANCE_RAW_LOYALTY],[TRANSACTION_ON_HOLD_ID],[SSW_TRAN_ID],[REFERENCE],[BRAND_ID],[MachineID])
			select i.[ID],i.[ACCOUNT_ID],i.[DATETIME],i.[TRAN_TYPE],i.[AMOUNT_REAL],i.[BALANCE_REAL],i.[PLATFORM_TRAN_ID],i.[GAME_TRAN_ID],i.[GAME_ID],i.[PLATFORM_ID],i.[payment_id],i.[ROLLED_BACK],i.[ROLLBACK_TRAN_ID],i.[AMOUNT_RELEASED_BONUS],i.[AMOUNT_PLAYABLE_BONUS],i.[BALANCE_RELEASED_BONUS],i.[BALANCE_PLAYABLE_BONUS],i.[AMOUNT_UNDERFLOW],i.[AMOUNT_RAW_LOYALTY],i.[BALANCE_RAW_LOYALTY],i.[TRANSACTION_ON_HOLD_ID],i.[SSW_TRAN_ID],i.[REFERENCE],i.[BRAND_ID], i.[MachineID]
			from inserted i 
				inner join admin_all.Synonym_ACCOUNT_TRAN_ALL a on a.ID = i.ID
			where not exists(select * from admin_all.ACCOUNT_TRAN_REALTIME r where r.id = i.id)
	set identity_insert admin_all.ACCOUNT_TRAN_REALTIME off
___Exit___:
	declare @ret int
	exec @ret = sp_getapplock @Resource = N'AccountTranHourlyAggregateCalculation', @LockMode = N'Shared', @LockOwner = 'Transaction', @LockTimeout = 0
	;with s as
	(
		select dateadd(hour, datediff(hour, '1990-01-01', DATETIME), '1990-01-01') Datetime, case when @ret >= 0 then 0 else 1 end CalculationInProgress from inserted
		union
		select dateadd(hour, datediff(hour, '1990-01-01', DATETIME), '1990-01-01') Datetime, case when @ret >= 0 then 0 else 1 end CalculationInProgress from deleted
	)
	insert into admin_all.AccountTranHourlyAggregateRecalculate(DateTime, CalculationInProgress)
		select DateTime, CalculationInProgress
		from s
		where not exists(
						select * 
						from admin_all.AccountTranHourlyAggregateRecalculate t 
						where t.Datetime = s.Datetime 
							and t.CalculationInProgress = s.CalculationInProgress
						);
	--merge admin_all.AccountTranHourlyAggregateRecalculate t
	--using s on t.Datetime = s.Datetime and t.CalculationInProgress = s.CalculationInProgress
	--when not matched then 
	--	insert (DateTime, CalculationInProgress)
	--		values(s.DateTime, s.CalculationInProgress)
	--;

end
go
