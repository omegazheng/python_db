set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_CurrencyConversionRate on admin_all.CurrencyConversionRate 
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @Date datetime = getdate()

	set nocount on
	;with x0 as
	(
		select CurrencyFrom, CurrencyTo
		from inserted
		union 
		select CurrencyFrom, CurrencyTo
		from deleted
	), x1 as 
	(
		select x0.CurrencyFrom, x0.CurrencyTo, c.RateID, c.Rate, c.EffectiveDate
		from x0
			left outer join admin_all.CurrencyConversionRate c on x0.CurrencyFrom = c.CurrencyFrom and x0.CurrencyTo = c.CurrencyTo and c.EffectiveDate <= @Date and c.ExpiryDate > @Date
	)
	merge admin_all.CurrencyConversionRateCurrent as t
	using x1 as s on t.CurrencyFrom = s.CurrencyFrom and t.CurrencyTo = s.CurrencyTo
	when not matched and s.RateID is not null then 
		insert (RateID, CurrencyFrom, CurrencyTo, Rate, LastUpdateDate, EffectiveDate)
			values(s.RateID, s.CurrencyFrom, s.CurrencyTo, s.Rate, @Date, s.EffectiveDate)
	when matched and s.RateID is not null and s.RateID <> t.RateID then
		update set t.RateID = s.RateID, t.Rate = s.Rate
	when matched and s.RateID is null then
		delete
	;
	--overlapping check
	if exists(
				select *
				from inserted i
					inner join  admin_all.CurrencyConversionRate c on i.CurrencyFrom = c.CurrencyFrom and i.CurrencyTo = c.CurrencyTo and i.RateID <> c.RateID
				where not(i.ExpiryDate < c.ExpiryDate or i.EffectiveDate >= c.ExpiryDate)
			)
	begin
		raiserror('Data range overlapping is found.', 16, 1)
		return
	end

	-- logging
	declare @InfoID uniqueidentifier
	exec admin_all.usp_GetCurrencyConversionChangeLogInfoID @InfoID output
	insert into admin_all.CurrencyConversionChangeLog(RateID, InfoID, Operation,OriginalValue)
		select 
				isnull(i.RateID, d.RateID), 
				@InfoID, 
				case 
					when d.RateID is null then 'I'
					when i.RateID is null then 'D'
					else 'U'
				end, 
				case when d.RateID is not null then (select nullif(d.CurrencyFrom, i.CurrencyFrom) CurrencyFrom, nullif(d.CurrencyTo, i.CurrencyTo) CurrencyTo, nullif(d.Rate, i.Rate) Rate, nullif(d.EffectiveDate, i.EffectiveDate) EffectiveDate, nullif(d.ExpiryDate, i.ExpiryDate) ExpiryDate for json path) end
		from inserted i
			full outer join deleted d on i.RateID = d.RateID
end


go
