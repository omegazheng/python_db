set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_BRAND_CONTENT on admin_all.BRAND_CONTENT
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on
	update admin_all.TIMESTAMP
		set LAST_UPDATE = getdate()
	where NAME = 'registryHash'
	if @@rowcount = 0
		insert into admin_all.TIMESTAMP(NAME, LAST_UPDATE)
				values('registryHash', getdate())
end
go
