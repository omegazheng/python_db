set ansi_nulls, quoted_identifier on
go
create or alter trigger [external_mpt].[TRI_external_mpt_USER_CONF_INS] on [external_mpt].[USER_CONF] 
for insert as 
begin 
	if @@rowcount = 0 return;
	set nocount on		
	update u
		set Hierarchy = hierarchyid::Parse(
			case when i.ParentID is null then '/' + cast(i.PartyID as varchar(20))+'/'   
				else u1.Hierarchy.ToString() + cast(i.PartyID as varchar(20))+'/'
			end)
	from external_mpt.USER_CONF  as u
		inner join inserted i on i.PartyID = u.PartyID
		left outer join external_mpt.USER_CONF u1 on i.ParentID = u1.PartyID;
	merge external_mpt.UserBrandCurrency t
	using inserted s on t.PartyId = s.PartyID
	when not matched then 
		insert (PartyID, BrandID, Currency)
			values(s.PartyID, s.BrandID, s.Currency)
	when matched and (s.BrandID <> t.BrandID or s.Currency <> t.Currency) then
		update set t.BrandID = s.BrandID
	;
end


go
