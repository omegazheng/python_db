if not exists(select * from master.sys.server_principals where name = 'admin_all')
	create login admin_all with password = 'admin_all' , check_policy = off;
go
if not exists(select * from sys.database_principals where name ='admin_all')
	create user admin_all for login  admin_all with default_schema = admin_all;
go
alter role db_owner add member admin_all
go
exec msdb..sp_executesql N'
if not exists(select * from sys.database_principals where name =''admin_all'')
	create user admin_all for login  admin_all;'
go
exec msdb..sp_executesql N'alter role SQLAgentUserRole add member admin_all;'