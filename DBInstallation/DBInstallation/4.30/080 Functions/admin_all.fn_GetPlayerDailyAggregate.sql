set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetPlayerDailyAggregate(@AggregateType tinyint, @StartDate datetime, @EndDate datetime)
returns table
as
return(
		select	ath.Datetime as Date, ath.ProductID, ath.PartyID, ath.Currency, ath.BrandID, ath.GameID,
				(PNL) as PNL,
				(Handle) as Handle,
				(HandleReal) HandleReal,
				(PNLReal) PNLReal,
				(HandleReleasedBonus) HandleReleasedBonus,
				(PNLReleasedBonus) PNLReleasedBonus,
				(HandlePlayableBonus) HandlePlayableBonus,
				(PNLPlayableBonus) PNLPlayableBonus,
				(InGamePlayableBonusRelease) InGamePlayableBonusRelease,
				(InGameReleasedBonusRelease) InGameReleasedBonusRelease,
				(TipsReal) TipsReal,
				(TipsPlayableBonus)  TipsPlayableBonus,
				(TipsReleasedBonus) TipsReleasedBonus,
				(ath.GameCount) GameCount,
				(BetCount) BetCount,
				(TranCount) TranCount,
				(TipsCount) TipsCount
		from admin_all.v_PlayerAggregateBase ath with(noexpand)
		where ath.Datetime >= @StartDate AND ath.Datetime < @EndDate
			and ath.AggregateType = @AggregateType
		--group by cast(ath.Datetime as Date), ath.ProductID, ath.PartyID, ath.Currency, ath.BrandID, ath.GameID
	)
go
