set ansi_nulls, quoted_identifier on
go
set xact_abort on
go
begin transaction
if not exists(select * from sys.columns where object_id = object_id('admin_all.AccountTranAggregate') and name = 'BonusPlanTran_AmountPlayableBonus')
	alter table admin_all.AccountTranAggregate add BonusPlanTran_AmountPlayableBonus numeric(38,18) 
if not exists(select * from sys.columns where object_id = object_id('admin_all.AccountTranAggregate') and name = 'BonusPlanTran_AmountPlayableBonusWinnings')
	alter table admin_all.AccountTranAggregate add BonusPlanTran_AmountPlayableBonusWinnings numeric(38,18) 
if not exists(select * from sys.columns where object_id = object_id('admin_all.AccountTranAggregate') and name = 'BonusPlanTran_AmountReleasedBonus')
	alter table admin_all.AccountTranAggregate add BonusPlanTran_AmountReleasedBonus numeric(38,18) 
if not exists(select * from sys.columns where object_id = object_id('admin_all.AccountTranAggregate') and name = 'BonusPlanTran_AmountReleasedBonusWinnings')
	alter table admin_all.AccountTranAggregate add BonusPlanTran_AmountReleasedBonusWinnings numeric(38,18) 
if not exists(select * from sys.columns where object_id = object_id('admin_all.AccountTranAggregate') and name = 'RawLoyalty')
	alter table admin_all.AccountTranAggregate add RawLoyalty numeric(38,18) 
commit
go

