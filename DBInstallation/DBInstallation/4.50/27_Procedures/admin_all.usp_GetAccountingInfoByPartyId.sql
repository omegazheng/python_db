SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
    SELECT *
    FROM DBO.SYSOBJECTS
    WHERE ID =
          OBJECT_ID(N'[admin_all].[usp_GetAccountingInfoByPartyId]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  DROP PROCEDURE [admin_all].[usp_GetAccountingInfoByPartyId]
GO

CREATE PROCEDURE [admin_all].[usp_GetAccountingInfoByPartyId]
    (@partyid int, @isAgentCashCount int = 0)
AS
  BEGIN
    SET DATEFIRST 1
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @userType int
    SET @endDateLocal = GETDATE()
    SET @startDateLocal = DATEADD(YEAR, 100, 0);

    BEGIN
        SELECT @userType = user_type
        FROM EXTERNAL_MPT.USER_CONF
        WHERE PARTYID = @partyid
    END

    create table #Temp (
        TRAN_TYPE varchar(10),
        DATE date,
        CURRENCY varchar(10),
        AMOUNT_REAL numeric(38,18),
        AMOUNT_PLAYABLE_BONUS numeric(38,18),
        AMOUNT_RELEASED_BONUS numeric(38,18)
    )

    IF (@isAgentCashCount = 1)
        BEGIN
            INSERT INTO #Temp
            SELECT
                'WITHDRAWAL'                TRAN_TYPE,
                cast(w.DATETIME as date)    DATE,
                u.CURRENCY,
                SUM(w.AMOUNT_REAL)          AMOUNT_REAL,
                0                           AMOUNT_PLAYABLE_BONUS,
                SUM(w.AMOUNT_BONUS_WINNINGS + w.AMOUNT_BONUS)  AMOUNT_RELEASED_BONUS
            from admin_all.WITHDRAWALS w
                with (NOLOCK)
                join admin_all.payment p
                    on w.payment_id = p.id
                join admin_all.ACCOUNT
                    on w.ACCOUNT_ID = ACCOUNT.id
                join external_mpt.USER_CONF u
                    on ACCOUNT.PARTYID = u.PARTYID
            where u.PARTYID = @partyId and p.STATUS = 'COMPLETED'
                and w.DATETIME >= @startDateLocal
                and w.DATETIME < @endDateLocal
                and (u.ParentID IS NULL OR p.METHOD!='CASH')
            group by cast(w.DATETIME as date), u.CURRENCY

            UNION

            select
                'WITHDRAWAL'                    TRAN_TYPE,
                cast(p.PROCESS_DATE as date)    DATE,
                u.CURRENCY,
                SUM(p.AMOUNT_REAL)              AMOUNT_REAL,
                0                               AMOUNT_PLAYABLE_BONUS,
                SUM(p.AMOUNT_RELEASED_BONUS)    AMOUNT_RELEASED_BONUS
            from admin_all.PAYMENT p
                with (NOLOCK)
                join admin_all.ACCOUNT
                    on p.ACCOUNT_ID = ACCOUNT.id
                join external_mpt.USER_CONF u
                    on ACCOUNT.PARTYID = u.PARTYID
            where u.PARTYID = @partyId and p.status = 'COMPLETED'
                and p.type = 'WITHDRAWAL'
                and p.PROCESS_DATE >= @startDateLocal
                and p.PROCESS_DATE < @endDateLocal
                and p.METHOD = 'CASH'
                and not exists(select id from admin_all.WITHDRAWALS w where p.id = w.payment_id)
            group by cast(p.PROCESS_DATE as date), u.CURRENCY
        END
    ELSE
        BEGIN
            INSERT INTO #Temp
            SELECT
                'WITHDRAWAL'                TRAN_TYPE,
                cast(w.DATETIME as date)    DATE,
                u.CURRENCY,
                SUM(w.AMOUNT_REAL)          AMOUNT_REAL,
                0                           AMOUNT_PLAYABLE_BONUS,
                SUM(w.AMOUNT_BONUS_WINNINGS + w.AMOUNT_BONUS)  AMOUNT_RELEASED_BONUS
            from admin_all.WITHDRAWALS w
                     with (NOLOCK)
                     join admin_all.payment p
                          on w.payment_id = p.id
                     join admin_all.ACCOUNT
                          on w.ACCOUNT_ID = ACCOUNT.id
                     join external_mpt.USER_CONF u
                          on ACCOUNT.PARTYID = u.PARTYID
            where u.PARTYID = @partyId and w.STATUS = 'COMPLETED'
              and w.DATETIME >= @startDateLocal
              and w.DATETIME < @endDateLocal
              and (u.ParentID IS NULL OR p.METHOD!='CASH')
            group by cast(w.DATETIME as date), u.CURRENCY
        END

    INSERT INTO #Temp
    SELECT
        'DEPOSIT'                    TRAN_TYPE,
        CAST(p.REQUEST_DATE AS DATE) DATE,
        u.CURRENCY,
        SUM(p.AMOUNT_REAL)           AMOUNT_REAL,
        0                            AMOUNT_PLAYABLE_BONUS,
        SUM(p.AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
    FROM admin_all.PAYMENT p
        WITH ( NOLOCK )
        JOIN admin_all.ACCOUNT
            ON p.ACCOUNT_ID = ACCOUNT.id
        JOIN EXTERNAL_MPT.USER_CONF u
            ON u.PARTYID = ACCOUNT.PARTYID
    WHERE
        p.TYPE = 'DEPOSIT' AND p.STATUS = 'COMPLETED'
        AND p.REQUEST_DATE >= @startDateLocal
        AND p.REQUEST_DATE < @endDateLocal
        AND u.partyid = @partyid
        AND (@isAgentCashCount = 1 OR u.ParentID IS NULL OR p.METHOD!='CASH')
    GROUP BY CAST(p.REQUEST_DATE AS DATE), u.CURRENCY

    SELECT
      tt.PERIOD                     AS PERIOD,
      tt.CURRENCY                   AS CURRENCY,
      tt.TRAN_TYPE                  AS TRAN_TYPE,
      SUM(tt.AMOUNT_REAL)           AS AMOUNT_REAL,
      SUM(tt.AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
      SUM(tt.AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
    FROM (
           SELECT
             'TODAY'               AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT_REAL           AS AMOUNT_REAL,
             AMOUNT_RELEASED_BONUS AS AMOUNT_RELEASED_BONUS,
             AMOUNT_PLAYABLE_BONUS AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE = CAST(@endDateLocal AS DATE)

           UNION

           SELECT
             'YESTERDAY'           AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             AMOUNT_REAL           AS AMOUNT_REAL,
             AMOUNT_RELEASED_BONUS AS AMOUNT_RELEASED_BONUS,
             AMOUNT_PLAYABLE_BONUS AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE = DATEADD(DAY, -1, CAST(@endDateLocal AS DATE))

           UNION

           SELECT
             'WTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(DD, 1 - DATEPART(DW, @endDateLocal), @endDateLocal) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

           UNION

           SELECT
             'MTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE >= CAST(DATEADD(MONTH, DATEDIFF(MONTH, 0, @endDateLocal), 0) AS DATE)
                 AND DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

           UNION

           SELECT
             'LTD'                      AS PERIOD,
             CURRENCY,
             TRAN_TYPE,
             SUM(AMOUNT_REAL)           AS AMOUNT_REAL,
             SUM(AMOUNT_RELEASED_BONUS) AS AMOUNT_RELEASED_BONUS,
             SUM(AMOUNT_PLAYABLE_BONUS) AS AMOUNT_PLAYABLE_BONUS
           FROM #Temp
           WHERE DATE <= CAST(@endDateLocal AS DATE)
           GROUP BY CURRENCY, TRAN_TYPE

         ) tt
    GROUP BY tt.PERIOD, tt.CURRENCY, TRAN_TYPE

    IF (OBJECT_ID('tempdb..#temp') IS NOT NULL)
      BEGIN
        DROP TABLE #Temp
      END
  END
GO
