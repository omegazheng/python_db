set quoted_identifier, ansi_nulls on
go
if object_id('rpt.usp_GetDataFeedByDate') is null
    exec('create procedure rpt.usp_GetDataFeedByDate as --')
go
alter procedure rpt.usp_GetDataFeedByDate
    (
        @BrandID bigint,
        @DateFrom datetime,
        @DateTo datetime = null
        )
    as
    begin
        set nocount, xact_abort  on
        select	AccountTranID as ID, PartyID as PARTYID, UserID as USERID, GameInfoID as gameInfoId, GameID as gameId,
                  GameTranID as gameTranId, ProductTranID platformTranId, TranType as tranType, Datetime as datetime,
                  AmountReal + AmountPlayableBonus + AmountReleasedBonus as amount,
                  BalanceReal as balanceReal,
                  BalancePlayableBonus as balancePlayableBonus,
                  BalanceReleasedBonus as balanceReleasedBonus,
                  BalanceReal + BalancePlayableBonus + BalanceReleasedBonus as balance,
                  Currency as CURRENCY, ProductCode platformCode, RollbackTranID rollbackTranId, RollbackTranType rollbackTranType
        from admin_all.DataFeed
        where BrandID = @BrandID
          and Datetime >= @DateFrom
          and Datetime < isnull(@DateTo, dateadd(day, 1, getdate()))
        --order by AccountTranID asc
    end
