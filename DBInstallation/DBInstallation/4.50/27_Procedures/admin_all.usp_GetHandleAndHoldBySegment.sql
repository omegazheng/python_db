SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
    SELECT *
    FROM DBO.SYSOBJECTS
    WHERE ID =
          OBJECT_ID(N'[admin_all].[usp_GetHandleAndHoldBySegment]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
  DROP PROCEDURE [admin_all].[usp_GetHandleAndHoldBySegment]
GO

CREATE PROCEDURE [admin_all].[usp_GetHandleAndHoldBySegment]
    (@startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @oneYearBack DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    SET @oneYearBack = DATEADD(YEAR, -1, @endDate)

SELECT
    PLATFORM_TYPE AS SEGMENT,
    ROUND(SUM(admin_all.fn_GetCurrencyConversionRate(SUMMARY_DATE, CURRENCY,'EUR')* RAW_HANDLE),2) AS HANDLE,
    CASE WHEN SUM(RAW_HANDLE) = 0 THEN 0
         ELSE ROUND(SUM(admin_all.fn_GetCurrencyConversionRate(SUMMARY_DATE, CURRENCY,'EUR') * RAW_PNL) / SUM(admin_all.fn_GetCurrencyConversionRate(SUMMARY_DATE, CURRENCY,'EUR')* RAW_HANDLE) * 100, 2)
        END AS HOLD
FROM (
         SELECT SUMMARY_DATE,
                CURRENCY,
                pl.PLATFORM_TYPE, sum(HANDLE) AS RAW_HANDLE,
                SUM(PNL) as RAW_PNL
         FROM [admin_all].[DW_GAME_PLAYER_DAILY] dw
                  JOIN admin_all.GAME_INFO g ON g.GAME_ID = dw.GAME_ID
                  JOIN admin_all.PLATFORM pl ON g.PLATFORM_ID = pl.ID
         WHERE dw.SUMMARY_DATE >= @startDateLocal
           AND dw.SUMMARY_DATE < @endDateLocal
           AND pl.PLATFORM_TYPE IN ('CASINO', 'BINGO', 'SPORTSBOOK')
        group by SUMMARY_DATE, CURRENCY, pl.PLATFORM_TYPE
     ) data
   GROUP BY PLATFORM_TYPE

   END
GO
