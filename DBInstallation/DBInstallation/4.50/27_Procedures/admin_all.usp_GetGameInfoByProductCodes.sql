SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('[admin_all].[usp_GetGameInfoByProductCodes]') is null
    exec('create procedure [admin_all].[usp_GetGameInfoByProductCodes] as --')
go
alter procedure [admin_all].[usp_GetGameInfoByProductCodes]
    (
        @PlatformCodes nvarchar(3000) = null,
        @GameID nvarchar(100) = null
    )
    as
    begin

        select gi.Id as gameInfoId, gi.GAME_ID as gameId, P.Id as platformId, P.CODE as platformCode
        from admin_all.GAME_INFO gi
                          join admin_all.PLATFORM p on p.ID = gi.PLATFORM_ID
        where
                p.CODE in (select Item from admin_all.fc_splitDelimiterString(@PlatformCodes, ','))
          and gi.GAME_ID=@GameID

    end
