set quoted_identifier, ansi_nulls on
go
if object_id('rpt.usp_GetDataFeedByID') is null
    exec('create procedure rpt.usp_GetDataFeedByID as --')
go
alter procedure rpt.usp_GetDataFeedByID
    (
        @BrandID bigint,
        @IDFrom bigint,
        @TopN int
        )
    as
    begin
        set nocount, xact_abort  on
        select	top (@TopN) AccountTranID as ID, PartyID as PARTYID, UserID as USERID, GameInfoID as gameInfoId, GameID as gameId,
                      GameTranID as gameTranId, ProductTranID platformTranId, TranType as tranType, Datetime as datetime,
                      AmountReal + AmountPlayableBonus + AmountReleasedBonus as amount,
                      BalanceReal as balanceReal,
                      BalancePlayableBonus as balancePlayableBonus,
                      BalanceReleasedBonus as balanceReleasedBonus,
                      BalanceReal + BalancePlayableBonus + BalanceReleasedBonus as balance,
                      Currency as CURRENCY, ProductCode platformCode, RollbackTranID rollbackTranId, RollbackTranType rollbackTranType
        from admin_all.DataFeed
        where BrandID = @BrandID
          and AccountTranID > @IDFrom
        order by AccountTranID asc
    end
