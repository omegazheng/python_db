set quoted_identifier, ansi_nulls on
if object_id('ADMIN_ALL.PAYMENT_GROUP') is null
begin
	create table ADMIN_ALL.PAYMENT_GROUP
	(
  ID  int identity
    constraint PK_admin_all_PAYMENT_GROUP
    primary key,
  ORGPAYMENTID int,
  PAYMENT_REF  int
	)
end