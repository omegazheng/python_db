set ansi_nulls, quoted_identifier on
go
if object_id('rpt.usp_GetMachinePerformance') is null
    begin
        exec('create procedure rpt.usp_GetMachinePerformance as --')
    end
go
alter procedure rpt.usp_GetMachinePerformance
    (
        @StaffID int,
        @DateFrom datetime,
        @DateTo datetime,
        @CompanyID int,
        @MachineID int = null
        )
    as
    begin
        set nocount on
        if isnull(@MachineID, @CompanyID) is null
            begin
                raiserror('@CompanyID or/and @MachineID should be provided.', 16, 1)
                return
            end
        select @DateFrom = cast(@DateFrom as date), @DateTo = dateadd(day, 1, cast(@DateTo as date))
        declare @StaffType varchar(10) = admin_all.fn_StaffType(@StaffID)
        declare @ComID int = (select sc.CompanyID
                              from admin_all.StaffCompany sc
                              where sc.StaffID = @StaffID)
        declare @SQL nvarchar(max)
        select @SQL = '
		select  ms.MachineID,
				sum(ms.eftcashwagered + ms.totalcashwagered) AmountReal, sum(ms.eftpromowagered + ms.totalpromowagered) PlayableBonus, sum(ms.GamesPlayed) GamesPlayed,
				sum(ms.win) TotalCreditWin, sum(ms.gamewon) GameWin, sum(ms.eftcashin + ms.coinsin + ms.notesin) AmountTransferIn,
				sum(ms.eftcashout + ms.coinsout + ms.notesout) AmountTransferOut, sum(ms.eftpromoin) BonusTransferIn,
				sum(ms.eftpromoout) BonusTransferOut, sum(ms.LoyaltyPoint) LoyaltyPoint,
				Count(*) Count
		from admin_all.MachineGameSummary ms
			inner join admin_all.Machine m on m.MachineID = ms.MachineID
			left join admin_all.Location l on l.LocationID = m.LocationID
			left join admin_all.PLATFORM p on p.id = m.ProductID
		where ms.Datetime >= @DateFrom and ms.Datetime < @DateTo
			and ' +case
                                             when @CompanyID is not null then 'l.CompanyID = @CompanyID '
                                             when @MachineID is not null then 'm.MachineID = @MachineID '
                                             when @CompanyID is not null  and @MachineID is not null  then 'l.CompanyID = @CompanyID and m.MachineID = @MachineID '
            end +'
			'+case when @StaffType = 'Company' then ' and
					l.CompanyID in (select f.CompanyID from admin_all.fn_ChildrenCompany(' + cast(@ComID as varchar(20)) + ') f
					)'
                   when @StaffType = 'Product' then '
			and exists(
					select *
					from admin_all.StaffProduct sp
					where sp.StaffID = @StaffID
						and sp.ProductID = m.ProductID

					)'
                   else ''
                          end+'
		group by ms.MachineID'
        exec sp_executesql @SQL, N'@StaffID int, @CompanyID int, @MachineID int, @DateFrom datetime, @DateTo datetime', @StaffID, @CompanyID, @MachineID, @DateFrom, @DateTo

    end
go
-- exec rpt.usp_GetMachinePerformance 1, '2018-01-01', '2018-02-01', 1
