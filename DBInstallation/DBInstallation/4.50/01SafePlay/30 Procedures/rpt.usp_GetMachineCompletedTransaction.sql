set ansi_nulls, quoted_identifier on
go
if object_id('rpt.usp_GetMachineCompletedTransaction') is null
    begin
        exec('create procedure rpt.usp_GetMachineCompletedTransaction as --')
    end
go
alter procedure rpt.usp_GetMachineCompletedTransaction
    (
        @StaffID int,	-- should be company staff? machines should be covered by staff company
        @startDate DATETIME,
        @endDate DATETIME
        )
    as
    begin
        set nocount on

        DECLARE @startDateLocal DATETIME
        DECLARE @endDateLocal DATETIME
        SET @startDateLocal = @startDate
        SET @endDateLocal = DATEADD(DD, 1, @endDate);

        select	mt.MachineTranID, mt.MachineID, mt.PartyID, eft.EFTID,
                  mt.StartDate, mt.EndDate,eft.AmountReal, eft.PlayableBonus, eft.ReleasedBonus ,eft.Status, eft.Reference, eft.TransferType,
                  eft.Datetime EFTDate,
                  u.FIRST_NAME + ' ' + u.LAST_NAME as UserName, u.USERID, u.CURRENCY, pl.CODE as ProductCode, pl.name as ProductName
        from admin_all.MachineTran mt
                 inner join admin_all.MachineTranEFT eft on mt.MachineTranID = eft.MachineTranID
                 inner join external_mpt. USER_CONF u on u.PARTYID = mt.PartyID
                 inner join admin_all.Machine m on m.MachineID = mt.MachineID
                 left join admin_all.PLATFORM pl on m.ProductID = pl.ID
        where eft.Status in ('Success', 'Canceled') and mt.StartDate >= @startDateLocal and mt.StartDate < @endDateLocal
    end
go
-- exec rpt.usp_GetMachineCompletedTransaction 1
