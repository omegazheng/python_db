
set ansi_nulls, quoted_identifier on
go

if object_id('admin_all.MachineGameSummary') is null
create table admin_all.MachineGameSummary
(
	SummaryID bigint IDENTITY(1,1) NOT NULL,
	PartyID int NOT NULL,
	DateTime datetime NOT NULL,
	MachineID int NOT NULL,
	GameID varchar(100) NULL,
	ProductID int NULL,
	GameInfoID int NULL,

	EFTCashWagered numeric(38, 18) NOT NULL,
    EFTPromoWagered numeric(38, 18) NOT NULL,
    GamesPlayed int NOT NULL,
    Win numeric(38, 18) NOT NULL,
    GameWon int NOT NULL,
    EFTCashIn numeric(38, 18) NOT NULL,
    EFTCashOut numeric(38, 18) NOT NULL,
    EFTPromoIn numeric(38, 18) NOT NULL,
    EFTPromoOut numeric(38, 18) NOT NULL,
    TotalCashWagered numeric(38, 18) NOT NULL,
    TotalPromoWagered numeric(38, 18) NOT NULL,
	CoinsIn numeric(38, 18) NOT NULL,
	CoinsOut numeric(38, 18) NOT NULL,
	NotesIn numeric(38, 18) NOT NULL,
	NotesOut numeric(38, 18) NOT NULL,
    TicketIn numeric(38, 18) NOT NULL,
    TicketOut numeric(38, 18) NOT NULL,
    PromoTicketIn numeric(38, 18) NOT NULL,
    PromoTicketOut numeric(38, 18) NOT NULL,

    WinLoss numeric(38, 18) NOT NULL,
    LoyaltyPoint bigint NOT NULL,
    CurrencyCode varchar(10) NULL,
	MachineTranID bigint NULL,
	EFTID bigint NULL,
	constraint PK_admin_all_MachineGameSummary primary key(Datetime, SummaryID)
) 
GO



--select '@' + name +' numeric(38,18),','isnull(@'+name+', 0), ',name + ', ', *from sys.columns where object_id = object_id('admin_all.MachineGameSummary')
