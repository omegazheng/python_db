if not exists(select * from sys.indexes where object_id = object_id('admin_all.MachinePlayGameSummary') and name = 'PK_admin_all_MachinePlayGameSummary')
    begin
        alter table admin_all.MachineGameSummary add constraint PK_admin_all_MachinePlayGameSummary primary key(Datetime, MachineID, PartyID, SummaryID)
    end

