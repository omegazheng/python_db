

update admin_all.REGISTRY_HASH set VALUE = replace(VALUE, 'john.huang@sqlnotes.info', 'john@omegasys.eu') where VALUE like '%john.huang@sqlnotes.info%'

update maint.AlertMailList set Recipients = replace(Recipients, 'john.huang@sqlnotes.info', 'john@omegasys.eu') where Recipients like '%john.huang@sqlnotes.info%'

begin try
update msdb.dbo.sysmail_account set replyto_address = 'john@omegasys.eu' WHERE  name = 'OmegaAlert' and replyto_address = 'john.huang@sqlnotes.info'
end try
begin catch
end catch