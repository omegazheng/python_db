
if not exists(select * from sys.columns where object_id = object_id('admin_all.STAFF_AUTH_TBL') and name = 'Country')
begin
	alter table admin_all.STAFF_AUTH_TBL add Country char(2) null
end


if not exists(select * from sys.columns where object_id = object_id('admin_all.STAFF_AUTH_TBL') and name = 'IP')
begin
	alter table admin_all.STAFF_AUTH_TBL add IP varchar(100)
end