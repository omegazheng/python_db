SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[PAYMENT]') = object_id and name = 'INFO')
  AND (select character_maximum_length from information_schema.columns where table_name = 'PAYMENT' and column_name = 'INFO') < 2048
  BEGIN
    ALTER TABLE [admin_all].[PAYMENT]
    ALTER COLUMN [INFO] NVARCHAR(2048)
  END
GO