set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_MapAffiliateRegistrationReport
(
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@Offset char(6) = null,
	@BrandID int = null,
	@CodeKey varchar(20) = 'btag',
	@OutputTarget varchar(20) = 'Table' -- File
)
as
begin

	set nocount on

    select 
			uc.PARTYID as PlayerID,
			b.BRANDNAME as BrandName,
			uc.COUNTRY CountryCode,
			utc.VALUE as TrackingParameter,
            cast(uc.REG_DATE as date) as AccountOpeningDate,
           cast(null as varchar(100)) as PromoCode
    into #temp
    from admin_all.USER_TRACKING_CODE utc
		inner join external_mpt.USER_CONF uc on uc.PARTYID = utc.PARTYID
		inner join admin.CASINO_BRAND_DEF b on b.BRANDID = uc.BRANDID
    where CODE_KEY = @CodeKey and uc.REG_DATE >= @DateFrom and uc.REG_DATE < @DateTo
		and uc.REGISTRATION_STATUS = 'PLAYER'
		and uc.BRANDID = @BrandID
	--from x0
	declare @Query nvarchar(max) = 'select PlayerID, BrandName, CountryCode, TrackingParameter, convert(varchar(30), AccountOpeningDate, 120) as AccountOpeningDate, PromoCode from #temp'
	if @OutputTarget = 'File'
	begin
		exec rpt.usp_GenerateReportFileExportContent @Query = @Query, 
				@Mapping = '<Mappings><Mapping SourceColumn="PlayerID"/><Mapping SourceColumn="BrandName"/><Mapping SourceColumn="CountryCode"/><Mapping SourceColumn="TrackingParameter"/><Mapping SourceColumn="AccountOpeningDate"/><Mapping SourceColumn="PromoCode"/></Mappings>',
				@Test = 0
		return
	end

	--select @OutputColumns = stuff((select ','+name as column_name from tempdb.sys.columns where object_id = object_id('tempdb..#temp') order by column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '');
    select * from #temp
	--select @OutputColumns
end
go
--exec rpt.usp_MapAffiliateRegistrationReport @DateFrom = '2019-12-11', @DateTo = '2019-12-12', @BrandID = 534,  @CodeKey = 'btag', @OutputTarget = 'Table'
--exec rpt.usp_MapAffiliateRegistrationReport @DateFrom = '2019-12-11', @DateTo = '2019-12-12', @BrandID = 534,  @CodeKey = 'btag', @OutputTarget = 'File'
--exec rpt.usp_DailyAffiliateRegistrationReport @DateFrom = '2019-12-11', @DateTo = '2019-12-12', @BrandID = 534, @Offset = null, @CodeKey = 'btag'
go

