set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GenerateReportFileExportContent
(
	@Query nvarchar(max),
	@Mapping xml, -- <Mappings><Mapping SourceColumn="dddd" ReportColumn="ddd"/></Mappings>
	@Separator nvarchar(1) = ',',
	@IncludeHeader bit = 1,
	@Test bit = 0
)
as
begin
	set nocount on
	declare @ReportFileExportID uniqueidentifier = rpt.fn_GetReportFileExportID()
	if @ReportFileExportID is null and @Test = 0
		return
	declare @SQL nvarchar(max), @Content nvarchar(max), @RowCount bigint, @FileHash uniqueidentifier, @FileSize bigint
	create table #ReportFileExportContent(ID bigint identity(1,1) primary key, Content nvarchar(max))
	if @IncludeHeader = 1
	begin
		insert into #ReportFileExportContent(Content)
			select stuff((select @Separator + cast(isnull(c.value('@ReportColumn','nvarchar(max)'), c.value('@SourceColumn','nvarchar(max)')) as nvarchar(max)) from @Mapping.nodes('/Mappings/Mapping') n(c)	for xml path(''),type).value('.', 'nvarchar(max)'), 1, 1, '')
	end
	select @SQL = cast('select ' as nvarchar(max)) + stuff((select '+'+quotename(@Separator, '''') + '+isnull(cast(' + cast(isnull(c.value('@SourceColumn','nvarchar(max)'), c.value('@ReportColumn','nvarchar(max)')) as nvarchar(max))+ ' as nvarchar(max)), '''')' from @Mapping.nodes('/Mappings/Mapping') n(c)	for xml path(''),type).value('.', 'nvarchar(max)'), 1, 5, '') 
				+ 'from (' + @Query+') a'
	insert into #ReportFileExportContent(Content)
		exec(@SQL)
	select @Content = (select cast(Content as nvarchar(max)) + cast(char(0x0d) + char(0x0a) as nvarchar(max)) from #ReportFileExportContent order by ID for xml path(''), type).value('.', 'nvarchar(max)')
	select	@FileSize = len(@Content),
			@RowCount = isnull((select max(ID) from #ReportFileExportContent), 0) + case when @IncludeHeader = 1 then -1 else 0 end,
			@FileHash = hashbytes('MD5', cast(@Content as varbinary(max)))
	--select @Content, @FileSize, @RowCount, @FileHash
	begin transaction
		update rpt.ReportFileExport
			set FileSize = @FileSize, FileHash = @FileHash, [RowCount] = @RowCount
		where ReportFileExportID = @ReportFileExportID
			and (
						isnull(FileSize, 0) <> @FileSize
					or isnull([RowCount], 0) <> @RowCount
					or isnull(FileHash, 'A60399F5-CDFD-4933-B36E-A75D699A941B') <> @FileHash
				)
		if @@rowcount <> 0
		begin
			merge rpt.ReportFileExportContent t 
			using (select @ReportFileExportID as ReportFileExportID) s on t.ReportFileExportID = s.ReportFileExportID
			when not matched then
				insert(ReportFileExportID, Content) values(@ReportFileExportID, compress(cast(@Content as varbinary(max))))
			when matched then
				update set Content = compress(cast(@Content as varbinary(max)))
			;
		end
	commit
	if @Test = 1
		select @Content
end
go
--exec rpt.usp_GenerateReportFileExportContent 'select top 100 PartyID, convert(varchar(20), BirthDate, 120) BirthDate, IP from external_mpt.USER_CONF', '<Mappings><Mapping SourceColumn="PartyID" ReportColumn="haha 1"/><Mapping SourceColumn="BirthDate" ReportColumn="haha 2"/><Mapping SourceColumn="IP" ReportColumn="haha 3"/></Mappings>'
--select newid()


--select top 100 * from (select top 100 * from external_mpt.USER_CONF) a
