set ansi_nulls, quoted_identifier on
go
--drop table rpt.ReportFileExport
go
if object_id('rpt.ReportFileExport') is null
begin
	create table rpt.ReportFileExport
	(
		ReportFileExportID uniqueidentifier not null,
		ParameterHash uniqueidentifier not null,
		Parameters xml not null,
		[RowCount] bigint,
		FileSize bigint,
		FileHash uniqueidentifier null,
		CreationDate datetime not null constraint DF_rpt_ReportFileExport_CreationDate default(getdate()),
		ExecutionDate datetime,
		CompletionDate datetime
		constraint PK_rpt_ReportFileExport primary key(ReportFileExportID) on Report,
		index IDX_rpt_ReportFileExport_CreationDate (CreationDate) on Report,
		index IDX_rpt_ReportFileExport_CompletionDate (CompletionDate) on Report,
		index IDX_rpt_ReportFileExport_ParameterHash unique (ParameterHash) on Report,
	
	) on Report
end
go
if not exists(select * from sys.indexes where name  = 'IDX_rpt_ReportFileExport_Parameters' and object_id = object_id('rpt.ReportFileExport'))
begin
	create primary xml index IDX_rpt_ReportFileExport_Parameters on rpt.ReportFileExport(Parameters);
end


--select * from sys.index_columns where index_id  = 256000 and object_id = object_id('rpt.ReportFileExport')
--exec maint.usp_ForceNamingConvention 'rpt.ReportFileExport', 0