set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetAffiliateReport
(
	@ReportFileExportID uniqueidentifier = null,
	@Type nvarchar(128) = null, --'Egass Registration Report', 'Deposit Report', 'Net Revenue Report'
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@Offset char(6) = null,
	@BrandIDs varchar(max) = null,
	@CodeKey varchar(20) = 'btag',
	@MetaDataOnly bit = 1,
	@IgnoreResult bit = 0
)
as
begin
	set nocount, xact_abort on
	declare @Parameters xml, @ParameterHash uniqueidentifier, @BatchJobID int, @Task nvarchar(max)
	begin transaction
	exec sp_getapplock 'rpt.usp_GetAffiliateReport', 'Exclusive', 'Transaction'
	if @ReportFileExportID is null
	begin
		select @Parameters = (select  @Type as [@Type], @DateFrom as [@DateFrom],	@DateTo as [@DateTo], @Offset as [@Offset], @BrandIDs as [@BrandIDs], @CodeKey as [@CodeKey] for xml path('Parameters'), type)
		
		select @ParameterHash = hashbytes('md5', cast(@Parameters as varbinary(max)))
		select @ReportFileExportID = ReportFileExportID
		from rpt.ReportFileExport
		where ParameterHash = @ParameterHash
		if @ReportFileExportID is null
		begin
			select @ReportFileExportID = newid()
			insert into rpt.ReportFileExport(ReportFileExportID, ParameterHash, Parameters)
				values(@ReportFileExportID, @ParameterHash, @Parameters)

			select @BatchJobID = BatchJobID from admin_all.BatchJob where Type = -2
			if @@rowcount  = 0
			begin
				exec admin_all.usp_CreateBatchJob @Name = 'ReportFileExport', @Type = -2, @BatchJobID = @BatchJobID output
			end
			select @Task = cast((select @ReportFileExportID as[@ReportFileExportID], @Type as [@Type], @DateFrom as [@DateFrom],	@DateTo as [@DateTo], @Offset as [@Offset], @BrandIDs as [@BrandIDs], @CodeKey as [@CodeKey] for xml path('Parameters'), type) as nvarchar(max))
			exec admin_all.usp_AddBatchJobTask @BatchJobID, @Task
			if not exists(select * from admin_all.BatchJob where Status = 'RUNNING' and BatchJobID = @BatchJobID)
			begin
				exec admin_all.usp_UpdateBatchJob @BatchJobID = @BatchJobID, @BatchJobStatus = 'RUNNING'
			end
		end
	end
	if @IgnoreResult = 1
		goto ___Exit___
	select r.ReportFileExportID, r.Parameters, r.[RowCount], r.FileSize, case when @MetaDataOnly = 1 then null else cast(decompress(rc.Content) as nvarchar(max)) end Content
	from rpt.ReportFileExport r
		left join rpt.ReportFileExportContent rc on rc.ReportFileExportID = r.ReportFileExportID
	where r.ReportFileExportID = @ReportFileExportID
___Exit___:
	commit
end
go
--exec rpt.usp_GetAffiliateReport @Type = 'Egass Registration Report', @DateFrom ='2001-01-01', @DateTo = '2020-01-01',@Offset = '+00:00',	@BrandIDs  = '1,2,3'


--declare @DateFrom datetime, @DateTo Datetime, @Offset char(6) = '+00:00', @BrandIDs varchar(max), @CodeKey varchar(20) = 'ClickId'

--select @DateFrom = dateadd(day, -1, cast(admin_all.fn_ConvertDatetimeZone(Getdate(), null, @Offset) as date))

--select @DateTo = Dateadd(day, 1, @DateFrom)

--exec rpt.usp_GetAffiliateReport @ReportFileExportID = null, @Type = 'Egass Registration Report', @DateFrom = @DateFrom, @DateTo = @DateTo, @Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1


----rollback