set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_ScheduleAffiliateReportFileExport
as
begin
	set nocount on
	declare @DateFrom datetime, @DateTo Datetime, @Offset char(6) = '+00:00', @BrandIDs varchar(max), @CodeKey varchar(20) = 'ClickId'

	if datepart(hour,admin_all.fn_ConvertDatetimeZone(Getdate(), null, @Offset)) < 3
		return
-- 	if cast(isnull(admin_all.fn_GetRegistry('affiliate.egass.Schedule.isEnabled'), '') as bit) <> 1
-- 		return

	select @DateFrom = dateadd(day, -1, cast(admin_all.fn_ConvertDatetimeZone(Getdate(), null, @Offset) as date))
	select @DateTo = Dateadd(day, 1, @DateFrom)


  declare c cursor local for
    select BRANDID from admin.CASINO_BRAND_DEF
  open c
  fetch next from c into @BrandIDs
  while @@fetch_status = 0
  begin
    if isnull(cast(admin_all.fn_GetBrandRegistry(@BrandIDs, 'affiliate.egass.Schedule.isEnabled') as bit), 0) = 1
      begin
	      exec rpt.usp_GetAffiliateReport @ReportFileExportID = null, @Type = 'Egass Registration Report', @DateFrom = @DateFrom, @DateTo = @DateTo,	@Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
	      exec rpt.usp_GetAffiliateReport @ReportFileExportID = null, @Type = 'Deposit Report', @DateFrom = @DateFrom, @DateTo = @DateTo,	@Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
	      exec rpt.usp_GetAffiliateReport @ReportFileExportID = null, @Type = 'Net Revenue Report', @DateFrom = @DateFrom, @DateTo = @DateTo,	@Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
	    end
	  fetch next from c into @BrandIDs
  end
  close c
  deallocate c
end
go



