set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_DailyAffiliateRegistrationReport
(
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@Offset char(6) = null,
	@BrandIDs varchar(max) = null,
	@CodeKey varchar(20) = 'btag',
	@OutputTarget varchar(20) = 'Table' -- File, Both
)
as
begin

	set nocount on
    select @DateFrom = admin_all.fn_ConvertDatetimeZone(isnull(@DateFrom, cast(getdate() as date)), @Offset, null)
	select @DateTo = admin_all.fn_ConvertDatetimeZone(isnull(@DateTo, dateadd(d, 1, @DateFrom)), @Offset, null)
	declare @RegCategory varchar(100) = admin_all.fn_GetRegistry('affiliate.egass.reg.category')
	create table #BrandID(BrandID int, BrandName nvarchar(1000), primary key (BrandID))
	insert into #BrandID(BrandID, BrandName)
		select BRANDID, BRANDNAME
		from admin.CASINO_BRAND_DEF
		where @BrandIDs is null
			or BRANDID in (
							select cast(value as int) 
							from (select value from string_split(@BrandIDs, ',') s)  a
						)

						--select * from admin_all.TIME_ZONE
    select utc.VALUE as btag,
           cbd.BrandName as brand,
            admin_all.fn_ConvertDatetimeZone(uc.REG_DATE, null, @Offset) as account_opening_date,
           uc.PARTYID as player_id,
           uc.USERID as username,
           uc.COUNTRY,
		   uc.LANGUAGE,
		   uc.IP
    into #temp
    from admin_all.USER_TRACKING_CODE utc

             join external_mpt.USER_CONF uc on uc.PARTYID = utc.PARTYID
             join #BrandID cbd on cbd.BrandID = uc.BRANDID
    where CODE_KEY = @CodeKey and uc.REG_DATE >= @DateFrom and uc.REG_DATE < @DateTo
		and uc.REGISTRATION_STATUS = 'PLAYER'
	--from x0
	declare @Query nvarchar(max) = 'select btag as ClickId, player_id as PIN, convert(varchar(100), cast(account_opening_date as date), 120) RegDate, COUNTRY as RegCountry, LANGUAGE as RegLanguage, IP RegIP, '+isnull(quotename(@RegCategory, ''''), 'NULL')+' RegCategory from #temp'
	if @OutputTarget = 'File'
	begin
		exec rpt.usp_GenerateReportFileExportContent @Query = @Query, 
				@Mapping = '<Mappings><Mapping SourceColumn="ClickId"/><Mapping SourceColumn="PIN"/><Mapping SourceColumn="RegDate"/><Mapping SourceColumn="RegCountry"/><Mapping SourceColumn="RegLanguage"/><Mapping SourceColumn="RegCategory"/><Mapping SourceColumn="RegIP"/></Mappings>',
				@Test = 0
		return
	end

	--select @OutputColumns = stuff((select ','+name as column_name from tempdb.sys.columns where object_id = object_id('tempdb..#temp') order by column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '');
    select * from #temp
	--select @OutputColumns
end
go
--exec rpt.usp_DailyAffiliateRegistrationReport @DateFrom = '2019-12-11', @DateTo = '2019-12-12', @BrandIDs = 534, @Offset = '+00:00', @CodeKey = 'btag', @OutputTarget = 'File'
--exec rpt.usp_DailyAffiliateRegistrationReport @DateFrom = '2019-12-11', @DateTo = '2019-12-12', @BrandIDs = 534, @Offset = null, @CodeKey = 'btag'
go

