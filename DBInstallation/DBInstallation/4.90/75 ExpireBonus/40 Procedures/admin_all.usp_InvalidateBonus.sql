set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_InvalidateBonus(@BonusID int)
as
begin
	set nocount, xact_abort on
	declare @Date datetime = getdate()
	declare @Status varchar(25), @PartyID int, @PlayableBonus numeric(38, 18), @PlayableBonusWinnings numeric(38, 18), @AccountTranID bigint
	
	
	select @Status = b.STATUS, @PartyID = b.PARTYID, @PlayableBonus = b.PLAYABLE_BONUS, @PlayableBonusWinnings = b.PLAYABLE_BONUS_WINNINGS
	from admin_all.BONUS b
	where EXPIRY_DATE < @Date
		and STATUS in ('ACTIVE', 'QUEUED', 'SPENT_ACTIVE', 'OPTED_IN')
		and exists(select * from admin_all.ACCOUNT a where a.PARTYID = b.PARTYID)
		and b.ID = @BonusID
	if @@rowcount = 0
		return
	begin transaction
	update admin_all.BONUS set STATUS = 'EXPIRED' where ID = @BonusID
	if @Status in ('ACTIVE', 'QUEUED', 'SPENT_ACTIVE')
	begin
		select @PlayableBonus = - (@PlayableBonusWinnings + @PlayableBonus)
		exec admin_all.usp_UpdateAccountInternal @partyID = @PartyID, @TranType = 'EXP_BONUS', @AmountReal = 0, @PlayableBonus = @PlayableBonus, @AccountTranID = @AccountTranID output
		insert into admin_all.USER_ACTION_LOG(Date, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, OLD_VALUE, NEW_VALUE)
			select @Date, null, 'ExpiredBonus', @PartyID, 'EXPIRE_BONUS', @AccountTranID, AMOUNT_PLAYABLE_BONUS, AMOUNT_PLAYABLE_BONUS
			from admin_all.ACCOUNT_TRAN
			where ID = @AccountTranID
	end
	else if @Status in ('OPTED_IN')
	begin
		insert into admin_all.USER_ACTION_LOG(Date, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, OLD_VALUE, NEW_VALUE)
			select @Date, null, 'ExpiredBonus', @PartyID, 'EXPIRE_BONUS', null, @Status, 'EXPIRED'
	end
	commit
end
go

