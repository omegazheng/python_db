set quoted_identifier, ansi_nulls on
go
create or alter trigger admin_all.TRI_BONUS_PLAN on admin_all.BONUS_PLAN
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on 
	insert into admin_all.BonusPlanAudit(BonusPlanID, PLAN_NAME, BONUS_CODE, START_DATE, END_DATE, MAX_TRIGGER_ALL, MAX_TRIGGER_PLAYER, PRIORITY, DEPOSIT_FIRST, DEPOSIT_MIN_AMOUNT, DEPOSIT_MAX_AMOUNT, AMOUNT, AWARD_TYPE, MULTIPLIER, EXPIRY_DAYS, TRIGGER_TYPE, WAGER_REQ, CREATED_DATE, STATUS, IS_PLAYABLE, PLAYABLE_BONUS_QUALIFIES, IS_AFFILIATE_SPECIFIC, IS_FREE_BET, IS_FREE_BET_RETURN, IS_PLAYABLE_WINNINGS_RELEASED, WAGER_REQUIREMENT_ACTION, IS_INCREMENTAL, INCREMENTAL_COUNT, IS_CANCEL_ON_WITHDRAWAL, LINKED_PARENT_BONUS_PLAN_ID, RECURRING_FREQUENCY, RECURRING_START_HOUR, RECURRING_START_MIN, RECURRING_END_HOUR, RECURRING_END_MIN, RECURRING_DAYS, RECURRING_REPEAT, RECURRING_REPEAT_UNTIL, MAX_TRIGGER_PER_RECURRING, MAX_TRIGGER_PER_RECURRING_DAY, TYPE, EXTERNAL_BONUS_PLAN_ID, SPORTSBOOK_PLATFORM_ID, EXTERNAL_BONUS_CODE, IS_SPORTSBOOK, SEQ_BP_MAP_ID, /*IS_OFFSIDE_SPORTSBOOK, */DESCRIPTION, IMAGE_URL, THIRD_PARTY, IS_CONTINUOUS, PRODUCT_ID, LastModifiedStaffID)
		select ID, PLAN_NAME, BONUS_CODE, START_DATE, END_DATE, MAX_TRIGGER_ALL, MAX_TRIGGER_PLAYER, PRIORITY, DEPOSIT_FIRST, DEPOSIT_MIN_AMOUNT, DEPOSIT_MAX_AMOUNT, AMOUNT, AWARD_TYPE, MULTIPLIER, EXPIRY_DAYS, TRIGGER_TYPE, WAGER_REQ, CREATED_DATE, STATUS, IS_PLAYABLE, PLAYABLE_BONUS_QUALIFIES, IS_AFFILIATE_SPECIFIC, IS_FREE_BET, IS_FREE_BET_RETURN, IS_PLAYABLE_WINNINGS_RELEASED, WAGER_REQUIREMENT_ACTION, IS_INCREMENTAL, INCREMENTAL_COUNT, IS_CANCEL_ON_WITHDRAWAL, LINKED_PARENT_BONUS_PLAN_ID, RECURRING_FREQUENCY, RECURRING_START_HOUR, RECURRING_START_MIN, RECURRING_END_HOUR, RECURRING_END_MIN, RECURRING_DAYS, RECURRING_REPEAT, RECURRING_REPEAT_UNTIL, MAX_TRIGGER_PER_RECURRING, MAX_TRIGGER_PER_RECURRING_DAY, TYPE, EXTERNAL_BONUS_PLAN_ID, SPORTSBOOK_PLATFORM_ID, EXTERNAL_BONUS_CODE, IS_SPORTSBOOK, SEQ_BP_MAP_ID, /*IS_OFFSIDE_SPORTSBOOK, */DESCRIPTION, IMAGE_URL, THIRD_PARTY, IS_CONTINUOUS, PRODUCT_ID, LastModifiedStaffID
		from deleted
	declare @OriginalMinBalanceThreshold numeric(38, 18), @OriginalMinBalanceAction varchar(20), @BonusPlanID int
	declare cTRI_BONUS_PLAN cursor local static for
		select d.MinBalanceThreshold, d.MinBalanceAction, i.ID
		from inserted i
			inner join deleted d on i.ID = d.ID
		where i.MinBalanceAction in ('EXPIRE', 'RELEASE')
			and i.MinBalanceThreshold > isnull(d.MinBalanceThreshold, 0)
	open cTRI_BONUS_PLAN
	fetch next from cTRI_BONUS_PLAN into @OriginalMinBalanceThreshold, @OriginalMinBalanceAction, @BonusPlanID
	while @@fetch_status = 0
	begin
		exec admin_all.usp_AdjustBonusWithMinThreshold null, @BonusPlanID, @OriginalMinBalanceThreshold, @OriginalMinBalanceAction
		fetch next from cTRI_BONUS_PLAN into @OriginalMinBalanceThreshold, @OriginalMinBalanceAction, @BonusPlanID
	end
	close cTRI_BONUS_PLAN
	deallocate cTRI_BONUS_PLAN
	declare @BonusID int
	declare cTRI_BONUS_PLAN cursor local for
		select b.ID
		from inserted i
			inner join admin_all.BONUS b on i.ID = b.BONUS_PLAN_ID
		where b.STATUS = 'OPTED_IN'
			and b.EXPIRY_DATE < getdate()
	open cTRI_BONUS_PLAN
	fetch next from cTRI_BONUS_PLAN into @BonusID
	while @@fetch_status = 0
	begin
		exec admin_all.usp_InvalidateBonus @BonusID
		fetch next from cTRI_BONUS_PLAN into @BonusID
	end
	close cTRI_BONUS_PLAN
	deallocate cTRI_BONUS_PLAN
end