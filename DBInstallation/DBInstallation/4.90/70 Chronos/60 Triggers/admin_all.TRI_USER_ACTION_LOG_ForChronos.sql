set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_USER_ACTION_LOG_ForChronos on admin_all.USER_ACTION_LOG
for insert
as 
begin
	if @@rowcount = 0
		return;
	if chronos.fn_IsEnabled() = 0
		return
	set nocount on
	declare @EventID bigint, @Value varchar(8000), @PartyID int, @ActionType varchar(50), @Value1 varchar(8000)
	declare c cursor local fast_forward for
		select i.ACTION_TYPE, i.PARTYID, rtrim(isnull(i.COMMENT, i.NEW_VALUE))
		from inserted i 
		where ACTION_TYPE in('DEPOSIT_LIMIT_REACHED', 'APPLY_FREESPIN')
			and nullif(rtrim(isnull(i.COMMENT, i.NEW_VALUE)), '')  is not null
	open c
	fetch next from c into @ActionType, @PartyID, @Value
	while @@fetch_status = 0
	begin
		insert into chronos.Event(PartyID, Name) values(@PartyID, case @ActionType when 'DEPOSIT_LIMIT_REACHED' then 'LimitReached' else 'FreeSpinAward' end)
		select @EventID = scope_identity()
		insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'Information', @Value)
		fetch next from c into @ActionType, @PartyID, @Value
	end
	close c
	deallocate c

	declare c cursor local fast_forward for
		select i.ACTION_TYPE, i.PARTYID, OLD_VALUE, NEW_VALUE
		from inserted i 
		where (ACTION_TYPE in('KYC_STATUS_CHANGE') or (ACTION_TYPE in ('DATA_EDIT') and FIELD_NAME like '%KYC Status%'))
			and isnull(OLD_VALUE, '') <> isnull(NEW_VALUE, '')
	open c
	fetch next from c into @ActionType, @PartyID, @Value, @Value1
	while @@fetch_status = 0
	begin
		insert into chronos.Event(PartyID, Name) values(@PartyID, 'KYCStatusChange')
		select @EventID = scope_identity()
		insert into chronos.EventData(EventID, Name, Value1, Value2) values(@EventID, 'preStatus', @Value, null), (@EventID, 'status', @Value1, null)
		fetch next from c into @ActionType, @PartyID, @Value, @Value1
	end
	close c
	deallocate c
end
go
--begin tran
--insert into admin_all.USER_ACTION_LOG([DATE], [STAFFID], [COMMENT], [PARTYID], [ACTION_TYPE], [ACTION_ID], [FIELD_NAME], [OLD_VALUE], [NEW_VALUE], [OPERATION_TYPE])
--select top 1 [DATE], [STAFFID], [COMMENT], [PARTYID], [ACTION_TYPE], [ACTION_ID], [FIELD_NAME], [OLD_VALUE], [NEW_VALUE], [OPERATION_TYPE]
--from admin_all.USER_ACTION_LOG  where ACTION_TYPE = 'KYC_STATUS_CHANGE'
--select top 10 * from chronos.Event order by 1 desc
--declare @aa int
--select top 1 @aa = EventID from chronos.Event order by 1 desc
--select top 10 * from chronos.EventData where EventID = @aa order by 1 desc
--rollback
