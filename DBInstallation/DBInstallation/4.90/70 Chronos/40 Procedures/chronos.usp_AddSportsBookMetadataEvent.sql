set ansi_nulls, quoted_identifier on
go
create or alter procedure chronos.usp_AddSportsBookMetadataEvent
(
	@SportsBookBetDetailsID int
)
as
begin
	set nocount on
	declare @PartyID int, @Json nvarchar(max), @EventID bigint, @Date datetime = getdate()
	select top 1 @PartyID = a.PARTYID
	from admin_all.ACCOUNT_TRAN at
		inner join admin_all.ACCOUNT a on at.ACCOUNT_ID = a.id
	where exists(
					select * 
					from admin_all.SPORTSBOOK_BET_DETAILS b
						inner join admin_all.PLATFORM p on p.CODE = b.PLATFORM_CODE
					where b.id = @SportsBookBetDetailsID
						and at.PLATFORM_ID = p.ID
						and at.GAME_TRAN_ID = b.WAGER_ID

				)
	if @PartyID is null
		return;

	--select @Json = (
						select d.CURRENCY, d.AMOUNT, d.TYPE, d.WAGER_ID, d.PLATFORM_CODE, d.STATUS, d.POTENTIAL_WIN, d.TOTAL_ODDS, d.MSG,
								(select s.MARKET, s.SELECTION, s.ODDS, s.PERIOD, s.EVENT, s.EVENT_PATHS, s.OUTCOME_ID, s.OUTCOME_DESCRIPTION, s.MATCH, s.MATCH_INFO, s.SPORT_NAME from admin_all.SPORTSBOOK_SELECTION s where s.BET_DETAILS_ID = d.ID for json auto) Selection 
						from admin_all.SPORTSBOOK_BET_DETAILS d 
						where d.ID = @SportsBookBetDetailsID
						for json auto
	--				)
	
	insert into chronos.Event(Name, PartyID, Date)
		values('SportsbookMetadata ', @PartyID, @Date)
	select @EventID = scope_identity()
	insert into chronos.EventData(EventID, Name, Value1, Value2, Date, Info) 
		values(@EventID, 'Json', @Date, null, @Date, @Json)
	
end
go
--begin transaction
--exec chronos.usp_AddSportsBookMetadataEvent 1
--rollback