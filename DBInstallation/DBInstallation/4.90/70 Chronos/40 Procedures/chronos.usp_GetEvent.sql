set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_GetEvent') is null
	exec('create procedure chronos.usp_GetEvent as --')
go
alter procedure chronos.usp_GetEvent @ConsumerID uniqueidentifier, @EventCategoryID int = -1
as
begin
	set nocount on 
	set xact_abort on
	set lock_timeout 0
	if @@trancount = 0
		throw 50000, 'chronos.usp_GetEvent must be run within a transaction', 16;
	declare @FromVersion bigint = 0, @CurrentVersion bigint = min_active_rowversion()
	select	@CurrentVersion = isnull((select  max(EventID) from chronos.Event), 0),
			@EventCategoryID = isnull(@EventCategoryID, -1)
	update a
		set 
				@FromVersion = FromVersion,
				ToVersion = @CurrentVersion
	from chronos.SynchronizationStatus a (rowlock)
	where Name = 'Event'
		and ConsumerID = @ConsumerID
		and EventCategoryID = @EventCategoryID
	if @@rowcount = 0
	begin
		insert into chronos.SynchronizationStatus(ConsumerID, Name, FromVersion, ToVersion, EventCategoryID)
			select @ConsumerID, 'Event', @FromVersion, @CurrentVersion, @EventCategoryID
	end
	if @FromVersion = 0
	begin
		if @EventCategoryID > -1
		begin
			;with x0 as
			(
				select CategoryID, Name 
				from chronos.EventCategory
				where CategoryID = 1
				union all
				select c.CategoryID,  c.Name 
				from chronos.EventCategory c 
					inner join x0 on c.ParentCategoryID = x0.CategoryID
			)
			select	 current_transaction_id() TransactionID, p.*, u.CURRENCY Currency, u.BRANDID BrandID,
					(
						select 
								case when len(ed.name) <=2 then lower(ed.Name) else stuff(ed.Name, 1, 1, lower(left(ed.Name, 1))) end Name, 
								ed.Date, 
								ed.Value1, 
								ed.Value2,
								ed.Info
						from chronos.EventData ed
						where ed.EventID = p.EventID
						order by ed.Name
						for json auto
					) EventData
			from Chronos.Event p
				left join external_mpt.USER_CONF u on u.PARTYID = p.PartyID
			where EventID <= @CurrentVersion
				and exists(select * from x0 where x0.Name = p.Name)
		end
		else
		begin
			select	 current_transaction_id() TransactionID, p.*, u.CURRENCY Currency, u.BRANDID BrandID, 
					(
						select 
								case when len(ed.name) <=2 then lower(ed.Name) else stuff(ed.Name, 1, 1, lower(left(ed.Name, 1))) end Name, 
								ed.Date, 
								ed.Value1, 
								ed.Value2,
								ed.Info
						from chronos.EventData ed
						where ed.EventID = p.EventID
						order by ed.Name
						for json auto
					) EventData
			from Chronos.Event p
				left join external_mpt.USER_CONF u on u.PARTYID = p.PartyID
			where EventID <= @CurrentVersion
		end
	end
	else
	begin
		if @EventCategoryID > -1
		begin
			;with x0 as
			(
				select CategoryID, Name 
				from chronos.EventCategory
				where CategoryID = 1
				union all
				select c.CategoryID,  c.Name 
				from chronos.EventCategory c 
					inner join x0 on c.ParentCategoryID = x0.CategoryID
			)
			select	 current_transaction_id() TransactionID, p.*,  u.CURRENCY Currency, u.BRANDID BrandID,
					(
						select 
								case when len(ed.name) <=2 then lower(ed.Name) else stuff(ed.Name, 1, 1, lower(left(ed.Name, 1))) end Name, 
								ed.Date, 
								ed.Value1, 
								ed.Value2,
								ed.Info
						from chronos.EventData ed
						where ed.EventID = p.EventID
						order by ed.Name
						for json auto
					) EventData
			from Chronos.Event p
				left join external_mpt.USER_CONF u on u.PARTYID = p.PartyID
			where EventID > @FromVersion and EventID <=@CurrentVersion
				and exists(select * from x0 where x0.Name = p.Name)
		end
		else
		begin
			select	 current_transaction_id() TransactionID, p.*,  u.CURRENCY Currency, u.BRANDID BrandID,
					(
						select 
								case when len(ed.name) <=2 then lower(ed.Name) else stuff(ed.Name, 1, 1, lower(left(ed.Name, 1))) end Name, 
								ed.Date, 
								ed.Value1, 
								ed.Value2,
								ed.Info
						from chronos.EventData ed
						where ed.EventID = p.EventID
						order by ed.Name
						for json auto
					) EventData
			from Chronos.Event p
				left join external_mpt.USER_CONF u on u.PARTYID = p.PartyID
			where EventID > @FromVersion and EventID <=@CurrentVersion
		end
	end
end
go
--begin transaction
----select * from chronos.EventCategory
----insert into chronos.EventCategory(ParentCategoryID, Name) values(1, 'Logout')
--exec chronos.usp_GetEvent 0x01, 1
--rollback


