set quoted_identifier, ansi_nulls on
go
create or alter view maint.v_ExtendedProperty
as

select 'Column' as ObjectType, object_schema_name(c.object_id) SchemaName, object_name(c.object_id) ObjectName, isnull(c.name, '') ColumnName, cast(p.value as nvarchar(4000)) Value
from sys.columns c
	outer apply fn_listextendedproperty ('MS_Description', 'schema', object_schema_name(c.object_id), case when objectpropertyex(c.object_id, 'IsUserTable') = 1 then 'table' else 'view' end, object_name(c.object_id), 'COLUMN', c.name) p  
where objectpropertyex(c.object_id, 'IsUserTable') = 1 or objectpropertyex(c.object_id, 'IsView') = 1
union all
select case when objectpropertyex(c.object_id, 'IsUserTable') = 1 then 'Table' else 'View' end as ObjectType, object_schema_name(c.object_id) SchemaName, c.name ObjectName, '' ColumnName, cast(p.value as nvarchar(4000)) as Value
from sys.tables c
	outer apply fn_listextendedproperty ('MS_Description', 'schema', object_schema_name(c.object_id), case when objectpropertyex(c.object_id, 'IsUserTable') = 1 then 'table' else 'view' end, c.name, null, null) p  
where objectpropertyex(c.object_id, 'IsUserTable') = 1 or objectpropertyex(c.object_id, 'IsView') = 1
go
--select * from maint.v_ExtendedProperty order by 1,2,3