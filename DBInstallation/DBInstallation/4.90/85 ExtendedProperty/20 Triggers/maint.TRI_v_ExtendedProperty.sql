set quoted_identifier, ansi_nulls on
go
create or alter trigger maint.TRI_v_ExtendedProperty on maint.v_ExtendedProperty
instead of insert, update, delete
as
begin
	set nocount on
	declare @ObjectType sysname, @SchemaName sysname, @ObjectName sysname, @ColumnName sysname, @OldValue nvarchar(4000), @NewValue nvarchar(4000), @ObjectType1 sysname
	declare c cursor local static for
		select 
				case when i.ObjectType is not null then i.ObjectType else d.ObjectType end ObjectType,
				case when i.ObjectType is not null then i.SchemaName else d.SchemaName end SchemaName,
				case when i.ObjectType is not null then i.ObjectName else d.ObjectName end ObjectName,
				case when i.ObjectType is not null then i.ColumnName else d.ColumnName end ColumnName,
				nullif(d.Value, '') OldValue, nullif(i.Value, '') NewValue,
				case when cast(objectpropertyex(object_id(quotename(case when i.ObjectType is not null then i.SchemaName else d.SchemaName end) + '.' + quotename(case when i.ObjectType is not null then i.ObjectName else d.ObjectName end)), 'IsUserTable') as bit) = 1 then 'table' else 'view' end
		from inserted i
			full outer join deleted d on i.ObjectType = d.ObjectType and i.SchemaName = d.SchemaName and i.ObjectName = d.ObjectName and i.ColumnName = d.ColumnName
		where isnull(nullif(d.Value, ''), '') <> isnull(nullif(i.Value, ''), '')
	open c 
	fetch next from c into @ObjectType, @SchemaName, @ObjectName, @ColumnName, @OldValue, @NewValue, @ObjectType1
	while @@fetch_status = 0
	begin
		if @ObjectType = 'COLUMN' 
		begin
			if @OldValue is null and @NewValue is not null
			begin
				exec sp_addextendedproperty N'MS_Description', @NewValue, N'SCHEMA', @SchemaName, @ObjectType1, @ObjectName, N'COLUMN', @ColumnName
			end
			if @OldValue is not null and @NewValue is null
			begin
				exec sp_dropextendedproperty N'MS_Description', N'SCHEMA', @SchemaName, @ObjectType1, @ObjectName, N'COLUMN', @ColumnName
			end
			else
			begin
				exec sp_updateextendedproperty N'MS_Description', @NewValue, N'SCHEMA', @SchemaName, @ObjectType1, @ObjectName, N'COLUMN', @ColumnName
			end
		end
		else
		begin
			if @OldValue is null and @NewValue is not null
			begin
				exec sp_addextendedproperty N'MS_Description', @NewValue, N'SCHEMA', @SchemaName, @ObjectType1, @ObjectName
			end
			if @OldValue is not null and @NewValue is null
			begin
				exec sp_dropextendedproperty N'MS_Description', N'SCHEMA', @SchemaName, @ObjectType1, @ObjectName
			end
			else
			begin
				exec sp_updateextendedproperty N'MS_Description', @NewValue, N'SCHEMA', @SchemaName, @ObjectType1, @ObjectName
			end
		end
		fetch next from c into @ObjectType, @SchemaName, @ObjectName, @ColumnName, @OldValue, @NewValue, @ObjectType1
	end
	close c 
	deallocate c
end