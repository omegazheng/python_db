set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobComposeFTPTaskContent
(
	@Type nvarchar(20), 
	@Address nvarchar(128), 
	@Port int, 
	@UserName nvarchar(128), 
	@Password nvarchar(128), 
	@FilePath nvarchar(1000),
	@FileName nvarchar(128),
	@TextContent nvarchar(max), 
	@BinaryContent varbinary(max)
)
returns nvarchar(max)
as
begin
	return	case 
				when @TextContent is not null then 
					cast(
							(
								select 
										@Type as [@Type], 
										@Address as [@Address], 
										@Port as [@Port], 
										@UserName as [@UserName], 
										@Password as [@Password],
										@FilePath as [@FilePath],
										@FileName as [@FileName],
										N'Text' as [@ContentType], 
										@TextContent 
								for xml path('FTP'), type
							) as nvarchar(max)
						)
				else
					cast(
							(
								select 
										@Type as [@Type], 
										@Address as [@Address], 
										@Port as [@Port], 
										@UserName as [@UserName], 
										@Password as [@Password],
										@FilePath as [@FilePath] ,
										@FileName as [@FileName],
										N'Binary' as [@ContentType], 
										@BinaryContent 
								for xml path('FTP'), type, binary base64
							) as nvarchar(max)
						)
			end
end 
go
/*
declare @Type nvarchar(20) ='FTP', @Address nvarchar(100)='192.168.1.1', @Port int=21, @UserName nvarchar(120)='abc', @Password nvarchar(120)='ABC', @FileName nvarchar(128) = 'dssdasda',
	@Content nvarchar(max) = 'hahahaha', @ContentBinary varbinary(max) = 0x123456789abcdef, @x as nvarchar(max)
select admin_all.fn_BatchJobComposeFTPTaskContent(@Type, @Address, @Port, @UserName, @Password, @FileName, null, @ContentBinary)
select admin_all.fn_BatchJobComposeFTPTaskContent(@Type, @Address, @Port, @UserName, @Password, @FileName, @Content, @ContentBinary)
*/