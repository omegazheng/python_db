set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypeRendererFTP
(
	@Content nvarchar(max)
)
returns table
as
return
(
	with x0 as
	(
		select cast(@Content as xml) Content
	)
	select 
		c.value('@Type', 'nvarchar(20)') Type,
		c.value('@Address', 'nvarchar(128)') Address,
		c.value('@Port', 'int') Port,
		c.value('@UserName', 'nvarchar(128)') UserName,
		c.value('@Password', 'nvarchar(128)') Password,
		c.value('@FilePath', 'nvarchar(1000)') FilePath,
		c.value('@FileName', 'nvarchar(128)') FileName,
		c.value('@ContentType', 'nvarchar(20)') ContentType,
		case when c.value('@ContentType', 'nvarchar(30)') in('Text') then c.value('.', 'nvarchar(max)') end TextContent,
		case when c.value('@ContentType', 'nvarchar(30)') in('Binary') then c.value('.', 'varbinary(max)') end BinaryContent
	from x0
		cross apply x0.Content.nodes('/FTP') f(c)
)
go
/*declare @Type nvarchar(20) ='FTP', @Address nvarchar(100)='192.168.1.1', @Port int=21, @UserName nvarchar(120)='abc', @Password nvarchar(120)='ABC', @ContentType nvarchar(20)='Text',
	@Content nvarchar(max) = 'hahahaha', @ContentBinary varbinary(max) = 0x123456789abcdef, @x as nvarchar(max)
select @x = cast(
					(select @Type as [@Type], @Address as [@Address], @Port as [@Port], @UserName as [@UserName], @Password as [@Password], @ContentType as [@ContentType], @Content 
					for xml path('FTP'), type)
					as nvarchar(max))
select * from admin_all.fn_BatchJobTypeRendererFTP(@x)
select @ContentType ='Binary'
select @x = cast(
					(select @Type as [@Type], @Address as [@Address], @Port as [@Port], @UserName as [@UserName], @Password as [@Password], @ContentType as [@ContentType], @ContentBinary 
					for xml path('FTP'), type, binary base64)
					as nvarchar(max))
select * from admin_all.fn_BatchJobTypeRendererFTP(@x)
*/