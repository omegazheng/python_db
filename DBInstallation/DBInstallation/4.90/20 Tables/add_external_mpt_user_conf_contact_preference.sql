
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Staff, User, Agent Enum
IF NOT EXISTS(select * from sys.columns where object_id('[external_mpt].[USER_CONF]') = object_id and name = 'ContactPreference')
    begin
        Alter table external_mpt.USER_CONF
            ADD [ContactPreference] nvarchar(10) NULL
    end