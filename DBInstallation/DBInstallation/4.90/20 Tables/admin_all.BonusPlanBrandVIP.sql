
set ansi_nulls, quoted_identifier on
go
--drop table admin_all.BonusPlanBrandVIP
go
if object_id('admin_all.BonusPlanBrandVIP') is null
    begin
        create table admin_all.BonusPlanBrandVIP
        (
            BonusPlanBrandVIPID int identity (1,1) not null,
            BonusPlanID int not null,
            VIPStatusID int not null
            constraint PK_admin_all_BonusPlanBrandVIP primary key(BonusPlanBrandVIPID),
            constraint FK_admin_all_BonusPlanBrandVIP_BonusPlanID_admin_all_Bonus_Plan
                foreign key (BonusPlanID) references admin_all.BONUS_PLAN(ID),
            constraint FK_admin_all_BonusPlanBrandVIP_VIPStatusID__admin_all_VIP_STATATUS
                foreign key (VIPStatusID) references admin_all.VIP_STATUS(ID),
            constraint UQ_admin_all_BonusPlanBrandVIP_BonusPlanID_VIPStatusID unique (BonusPlanID, VIPStatusID) with (ignore_dup_key = on)
        )
    end
go
