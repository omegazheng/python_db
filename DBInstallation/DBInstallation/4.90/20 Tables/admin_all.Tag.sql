set ansi_nulls, quoted_identifier on
go

go
if object_id('admin_all.Tag') is null
    begin
        create table admin_all.Tag
        (
            TagID int identity (1,1) not null,
            CreateDate [datetime] NOT NULL DEFAULT GETDATE(),
            ExpiryDate [datetime] NULL,
            Status nvarchar(15) NOT NULL,
            Code nvarchar(100) not null,
            Name nvarchar(255) not null,
            Priority [int] NOT NULL DEFAULT 0
            constraint PK_admin_all_Tag primary key(TagID),
            constraint UQ_admin_all_Tag_Code unique (Code) with (ignore_dup_key = on)
        )
    end
go

