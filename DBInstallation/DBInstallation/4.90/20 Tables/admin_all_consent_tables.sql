SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



IF object_id('[admin_all].[V_LatestConsent]') is not null
begin
    drop view admin_all.V_LatestConsent;
end 

if exists (select * from sys.columns where object_id('[admin_all].[UserConsentHistory]') = object_id)
begin 
    drop table admin_all.UserConsentHistory;
end
if exists (select * from sys.columns where object_id('[admin_all].[UserConsent]') = object_id)
begin 
    drop table admin_all.UserConsent;
end
if exists (select * from sys.columns where object_id('[admin_all].[ConsentVersion]') = object_id)
begin 
    drop table admin_all.ConsentVersion;
end
if exists (select * from sys.columns where object_id('[admin_all].[Consent]') = object_id)
begin 
    drop table admin_all.Consent;
end



-- Staff, User, Agent Enum
IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[Consent]') = object_id)
    begin
        CREATE TABLE admin_all.Consent (
            ConsentId int IDENTITY(1,1) NOT NULL,
            ConsentKey nvarchar(20)  NOT NULL,
            Name nvarchar(50)  NOT NULL,
            BrandId int NULL,
            IsMandatory bit NOT NULL constraint DF_admin_all_Consent_IsMandatory default (0),
            Status nvarchar(20)  NOT NULL,
            ParentKey nvarchar(20)  NULL,
            CreationDate datetime2(7) NOT NULL default current_timestamp,
            CONSTRAINT PK_admin_all_Consent PRIMARY KEY (ConsentId),
            CONSTRAINT UQ_admin_all_Consent_ConsentKey_BrandId UNIQUE (ConsentKey,BrandId)
        );
        
    end

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[ConsentVersion]') = object_id)
    begin
        CREATE TABLE admin_all.ConsentVersion (
            ConsentVersionId int IDENTITY(1,1) NOT NULL,
            ConsentId int NOT NULL,
            Version int NOT NULL,
            Status nvarchar(20)  NOT NULL,
            StartDate datetime2(7) NULL,
            ExpiryDate datetime2(7) NULL,
            CreationDate datetime2(7) NOT NULL  default current_timestamp,
            UpdateDate datetime2(7) NOT NULL  default current_timestamp,
            CONSTRAINT PK_admin_all_ConsentVersion PRIMARY KEY (ConsentVersionId),
            CONSTRAINT UQ_admin_all_ConsentVersion_ConsentId_Version UNIQUE (ConsentId,Version),
            CONSTRAINT FK_admin_all_ConsentVersion_Consent_ConsentId FOREIGN KEY (ConsentId) REFERENCES admin_all.Consent(ConsentId));
    
    end

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[UserConsent]') = object_id)
    begin
        CREATE TABLE admin_all.UserConsent (
        UserConsentId bigint IDENTITY(1,1) NOT NULL,
        PartyId int NOT NULL,
        ConsentVersionId int NOT NULL,
        Status nvarchar(20)  NOT NULL,
        CreationDate datetime2(7) NOT NULL  default current_timestamp,
        UpdateDate datetime2(7) NOT NULL  default current_timestamp,
        CONSTRAINT PK_admin_all_UserConsent PRIMARY KEY (UserConsentId),
        CONSTRAINT UQ_admin_all_UserConsent_PartyId_ConsentVersionId UNIQUE (PartyId,ConsentVersionId),
        CONSTRAINT FK_admin_all_UserConsent_ConsentVersion_ConsentVersionId FOREIGN KEY (ConsentVersionId) REFERENCES admin_all.ConsentVersion(ConsentVersionId));
    end


IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[UserConsentHistory]') = object_id)
    begin
        CREATE TABLE admin_all.UserConsentHistory (
            UserConsentHistoryId bigint IDENTITY(1,1) NOT NULL,
        UserConsentId bigint NOT NULL,
        PartyId int NOT NULL,
        ConsentVersionId int NOT NULL,
        Status nvarchar(20)  NOT NULL,
        CreationDate datetime2(7) NOT NULL,
        UpdateDate datetime2(7) NOT NULL,
        CONSTRAINT PK_admin_all_UserConsentHistory PRIMARY KEY (UserConsentHistoryId));
    end






