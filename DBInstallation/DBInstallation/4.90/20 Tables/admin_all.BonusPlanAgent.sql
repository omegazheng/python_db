
set ansi_nulls, quoted_identifier on
go
--drop table admin_all.BonusPlanAgent
go
if object_id('admin_all.BonusPlanAgent') is null
    begin
        create table admin_all.BonusPlanAgent
        (
            BonusPlanAgentID int identity (1,1) not null,
            BonusPlanID int not null,
            AgentID int not null,
            constraint PK_admin_all_BonusPlanAgent primary key(BonusPlanAgentID),
            constraint FK_admin_all_BonusPlanAgent_BonusPlanID_admin_all_Bonus_Plan
                foreign key (BonusPlanID) references admin_all.BONUS_PLAN(ID),
            constraint FK_admin_all_BonusPlanAgent_AgentID_external_mpt_User_Conf
                foreign key (AgentID) references external_mpt.User_Conf(PartyID),
            constraint UQ_admin_all_BonusPlanAgent_BonusPlanID_AgentID unique (BonusPlanID,AgentID) with (ignore_dup_key = on)
        )
    end
go
