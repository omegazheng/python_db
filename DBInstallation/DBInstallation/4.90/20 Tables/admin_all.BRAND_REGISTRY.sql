IF EXISTS(SELECT * FROM sys.key_constraints WHERE name = 'UQ_admin_all_BRAND_REGISTRY_BRANDID_MAP_KEY' AND parent_object_id = OBJECT_ID('[admin_all].[BRAND_REGISTRY]'))
      ALTER TABLE [admin_all].[BRAND_REGISTRY]  DROP CONSTRAINT UQ_admin_all_BRAND_REGISTRY_BRANDID_MAP_KEY

go
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BRAND_REGISTRY]') and name = 'Country')
    alter table admin_all.BRAND_REGISTRY add Country [char](2) NULL
go

IF NOT EXISTS(SELECT * FROM sys.key_constraints WHERE name = 'UQ_admin_all_BRAND_REGISTRY_BRANDID_MAP_KEY_COUNTRY' AND parent_object_id = OBJECT_ID('[admin_all].[BRAND_REGISTRY]'))
      ALTER TABLE [admin_all].[BRAND_REGISTRY]  ADD CONSTRAINT UQ_admin_all_BRAND_REGISTRY_BRANDID_MAP_KEY_COUNTRY UNIQUE (BRANDID,MAP_KEY,Country)
go