SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists(select *
          from ADMIN_ALL.REGISTRY_HASH
          where MAP_KEY = 'netent.useAlternativeCountry')
    begin
        UPDATE admin_all.REGISTRY_HASH
        SET admin_all.REGISTRY_HASH.MAP_KEY = 'useAlternativeCountry'
        WHERE admin_all.REGISTRY_HASH.MAP_KEY = 'netent.useAlternativeCountry'
    end

if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'useAlternativeCountry')
    begin
        INSERT INTO ADMIN_ALL.REGISTRY_HASH (MAP_KEY, VALUE) VALUES ('useAlternativeCountry', 'false');
    end

if exists(select *
          from ADMIN_ALL.REGISTRY_HASH
          where MAP_KEY LIKE 'netent.signupCountry.mapping%')
    begin
        UPDATE admin_all.REGISTRY_HASH
        SET admin_all.REGISTRY_HASH.MAP_KEY = REPLACE(admin_all.REGISTRY_HASH.MAP_KEY, 'netent.signupCountry.mapping',
                                                      'signup.alternateCountry.mapping')
        WHERE admin_all.REGISTRY_HASH.MAP_KEY LIKE 'netent.signupCountry.mapping%';

    end


go
