SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM dbo.CUSTOMER WHERE  NAME in (N'PAL'))
  BEGIN
    if not exists (select code from admin_All.payment_method where code = 'WPCGHOSTED_ECMCCOMDEBIT')
    begin
      INSERT INTO admin_all.payment_method (CODE, NAME, IS_MANUAL_BANK, PAYMENT_TIMEOUT, IS_CCLEVELREADY, IS_RG_ENABLED, IS_POPUP_REQ) VALUES ('WPCGHOSTED_ECMCCOMDEBIT', 'WP Mastercard Commerce Debit', 0, -1, 0, -1, 0);
      end
    if not exists (select code from admin_All.payment_method where code = 'WPCGHOSTED_ECMCDEBIT')
    begin
    INSERT INTO admin_all.payment_method (CODE, NAME, IS_MANUAL_BANK, PAYMENT_TIMEOUT, IS_CCLEVELREADY, IS_RG_ENABLED, IS_POPUP_REQ) VALUES ('WPCGHOSTED_ECMCDEBIT', 'WP Mastercard Debit ', 0, -1, 0, -1, 0);
    end
    if not exists (select code from admin_All.payment_method where code = 'WPCGHOSTED_VISACOMDEBIT')
    begin
    INSERT INTO admin_all.payment_method (CODE, NAME, IS_MANUAL_BANK, PAYMENT_TIMEOUT, IS_CCLEVELREADY, IS_RG_ENABLED, IS_POPUP_REQ) VALUES ('WPCGHOSTED_VISACOMDEBIT', 'WP Visa Commerce Debit', 0, -1, 0, -1, 0);
    end
    if not exists (select code from admin_All.payment_method where code = 'WPCGHOSTED_VISADEBIT')
    begin
    INSERT INTO admin_all.payment_method (CODE, NAME, IS_MANUAL_BANK, PAYMENT_TIMEOUT, IS_CCLEVELREADY, IS_RG_ENABLED, IS_POPUP_REQ) VALUES ('WPCGHOSTED_VISADEBIT', 'WP Visa Debit', 0, -1, 0, -1, 0);
    end
    if not exists (select code from admin_All.payment_method where code = 'WPCGHOSTED_VISAELECTRON')
    begin
    INSERT INTO admin_all.payment_method (CODE, NAME, IS_MANUAL_BANK, PAYMENT_TIMEOUT, IS_CCLEVELREADY, IS_RG_ENABLED, IS_POPUP_REQ) VALUES ('WPCGHOSTED_VISAELECTRON', 'WP Visa Electron', 0, -1, 0, -1, 0);
    end

  END
GO