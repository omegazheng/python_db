set ansi_nulls, quoted_identifier on
go
begin
	exec admin_all.usp_MigrateRegistryProperty @PropertyA ='Support.CrossBrand.Same.UserId', @PropertyB='allowCrossBrandDuplicatePlayers'
end
go
