set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_GetDuplicateUserByUKGC]
(
    @BirthDate DATETIME,
    @Email NVARCHAR(MAX),
    @FirstName NVARCHAR(MAX) = NULL,
    @LastName NVARCHAR(MAX) = NULL,
    @PostalCode NVARCHAR(MAX) = NULL,
    @MobilePhone NVARCHAR(MAX) = NULL,
    @NationalRegNumber NVARCHAR(MAX) = NULL,
    @primaryWalletUUID NVARCHAR(MAX) = NULL,
    @BrandId INT = NULL
)
AS
BEGIN
    IF @BrandId IS NULL
        BEGIN
            SELECT FIRST_NAME as firstName,
                   LAST_NAME as lastName,
                   BIRTHDATE as birthDate,
                   POSTAL_CODE as postalCode,
                   EMAIL as email,
                   BRANDID as brandId,
                   MOBILE_PHONE as mobilePhone,
                   NATIONAL_REG_NUMBER as nationalRegNumber,
                   PRIMARY_WALLET_UUID as primaryWalletUUID
            FROM external_mpt.USER_CONF
            WHERE FIRST_NAME = @FirstName OR LAST_NAME = @LastName OR POSTAL_CODE = @PostalCode OR BIRTHDATE = @BirthDate
               OR EMAIL = @Email OR MOBILE_PHONE = @MobilePhone OR NATIONAL_REG_NUMBER = @NationalRegNumber OR PRIMARY_WALLET_UUID = @primaryWalletUUID;
        END

    ELSE
        BEGIN
            SELECT FIRST_NAME as firstName,
                   LAST_NAME as lastName,
                   BIRTHDATE as birthDate,
                   POSTAL_CODE as postalCode,
                   EMAIL as email,
                   BRANDID as brandId,
                   MOBILE_PHONE as mobilePhone,
                   NATIONAL_REG_NUMBER as nationalRegNumber,
                   PRIMARY_WALLET_UUID as primaryWalletUUID
            FROM external_mpt.USER_CONF
            WHERE BRANDID = @BrandId
              AND (FIRST_NAME = @FirstName OR LAST_NAME = @LastName OR POSTAL_CODE = @PostalCode OR BIRTHDATE = @BirthDate
                OR EMAIL = @Email OR MOBILE_PHONE = @MobilePhone OR NATIONAL_REG_NUMBER = @NationalRegNumber OR PRIMARY_WALLET_UUID = @primaryWalletUUID);
        END
END
go
