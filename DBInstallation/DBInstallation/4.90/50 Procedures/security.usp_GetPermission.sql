set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_GetPermission
(
    @PrincipalID int = null,  --Principal or staff id
    @StaffID int = null
)
as
begin
    set nocount on
    if cast(admin_all.fn_GetRegistry('hide_mobile_email') as bit) = 1
        begin
            select *
            from (
                     values ('SHOW_PHONE', 'PHONE'),
                            ('SHOW_EMAIL','EMAIL'),
                            ('SHOW_MOBILE', 'MOBILE'),
                            ('ENABLE_EXPORT', 'ENABLE_EXPORT')
                 ) n(Name,p) where Name = 'DISABLED'
            return
        end
    if cast(admin_all.fn_GetRegistry('omega.principal-securable.enabled') as bit) = 0
        begin
            with x0 as
                     (
                         select *
                         from (
                                  values ('SHOW_PHONE', 'PHONE'),
                                         ('SHOW_EMAIL','EMAIL'),
                                         ('SHOW_MOBILE', 'MOBILE'),
                                         ('ENABLE_EXPORT', 'ENABLE_EXPORT')
                              ) n(Name,p)
                     )
            select distinct x0.Name
            from x0
                     left join admin_all.ROLE_PERMISSION p
                     inner join admin_all.STAFF_ROLE s on p.ROLE_ID = s.ROLE_ID and s.STAFF_ID = isnull(@PrincipalID, @StaffID)
                                on  isnull(case when p.TYPE <> 'ENABLE_EXPORT' then p.SUBTYPE end, p.TYPE) like '%' + x0.p + '%'
            where x0.Name like 'SHOW%' and p.TYPE is null
               or x0.Name = 'ENABLE_EXPORT' and isnull(case when p.TYPE <> 'ENABLE_EXPORT' then p.SUBTYPE end, p.TYPE) = 'ENABLE_EXPORT'
        end
    else
        begin
            select @PrincipalID = security.fn_GetPrincipalID(security.fn_ComposeSID('STAFF', @StaffID))
            select Name from [security].[fn_GetSecurableByPrincipal](@PrincipalID, 0) where Type = 'DATA' and HasPermission = 1 and SecurableID not in(10000)
        end
end
go


--exec security.usp_GetPermission 7334

--exec security.usp_GetPermission 7365
--exec security.usp_GetPermission 7344
--exec security.usp_GetPermission 7334
----select * from admin_all.ROLE_PERMISSION p 
----			inner join admin_all.STAFF_ROLE s on p.ROLE_ID = s.ROLE_ID and s.STAFF_ID = 7365

----select * from  admin_all.AUTH_PERMISSION
--select * from admin_all.ROLE_PERMISSION

--select * from admin_all.STAFF_ROLE where role_id between 22 and 31

--7316
