SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_SynchronizeCasinoGameList
(
    @PlatformID int,
    @GameListString varchar(8000),
    @DelimiterBetweenGames varchar(5),
    @DelimiterBetweenGameFields varchar(5),
    @UpdatedCount int output
)
as

BEGIN

  declare @SegmentID int
  select @SegmentID = id from admin_all.segment where name = 'Casino'
  if (@SegmentID is null)
      RAISERROR('Segment Casino does NOT exist', 16, 1);

  create table #Games(Game varchar(500))

  insert into #Games(Game)
  select ltrim(rtrim(Item))
  from admin_all.fc_splitDelimiterString(@GameListString, @DelimiterBetweenGames)
  where ltrim(rtrim(Item)) <> ''

  begin transaction
  declare @CurrentGame varchar(500), @GameID varchar(100), @GameName varchar(100), @GameLaunchID varchar(100),
      @GameCategoryName varchar(100), @SubPlatformCode varchar(100), @ImageUrl varchar(1000), @BackgroundImageUrl varchar(1000), @IsFreespinEnabled bit, @IsMobile bit
  set @UpdatedCount = 0
  while EXISTS(select Game from #Games)
    begin
        SELECT TOP 1 @CurrentGame = Game FROM #Games;

        select @GameID = GameID, @GameName = GameName, @GameLaunchID = GameLaunchID,
           @GameCategoryName = GameCategoryName, @SubPlatformCode = SubPlatformCode, @ImageUrl = ImageUrl, @BackgroundImageUrl = BackgroundImageUrl, @IsFreespinEnabled = IsFreespinEnabled, @IsMobile = IsMobile
        from admin_all.fc_splitDelimiterStringToGameInfoFields(@CurrentGame, @DelimiterBetweenGameFields)

        --         print '@GameID=' + @GameID
        --             + ', @GameName=' + @GameName
        --             + ', @GameCategoryID=' + cast(@GameCategoryID as varchar(100))
        --             + ', @SubPlatformID=' + cast(@SubPlatformID as varchar(100));

        --  sanity check
        if @GameID is null or LEN(@GameID) = 0
            begin
              RAISERROR('@GameID is empty', 16, 1);
              rollback;
            end
        if @GameName is null or LEN(@GameName) = 0
          begin
            RAISERROR('@GameName is empty', 16, 1);
            rollback;
          end
        if @GameLaunchID is null or LEN(@GameLaunchID) = 0
          begin
            RAISERROR('@GameLaunchID is empty', 16, 1);
            rollback;
          end
        if @GameCategoryName is null or LEN(@GameCategoryName) = 0
          begin
            RAISERROR('@GameCategoryName is empty', 16, 1);
            rollback;
          end
        if @SubPlatformCode is null or LEN(@SubPlatformCode) = 0
          begin
            RAISERROR('@SubPlatformCode is empty', 16, 1);
            rollback;
          end

        if @GameName = 'null'
            SET @GameName = @GameID

        if @GameLaunchID = 'null'
            SET @GameLaunchID = @GameID

        if @GameCategoryName = 'null'
            SET @GameCategoryName = 'Unknown'
        declare @GameCategoryID int;
        if exists (select ID from admin_all.GAME_CATEGORY where name = @GameCategoryName)
            select @GameCategoryID = ID from admin_all.GAME_CATEGORY where name = @GameCategoryName;
        else
            begin
                insert into admin_all.GAME_CATEGORY(NAME) values(@GameCategoryName)
                select @GameCategoryID = ID from admin_all.GAME_CATEGORY where name = @GameCategoryName;
            end

        declare @SubPlatformID int = null;
        if @SubPlatformCode <> 'null'
            begin
                if exists (select ID from admin_all.SUB_PLATFORM where PLATFORM_ID = @PlatformID and CODE = @SubPlatformCode)
                    select @SubPlatformID = ID from admin_all.SUB_PLATFORM where PLATFORM_ID = @PlatformID and CODE = @SubPlatformCode
                else
                    begin
                        insert into admin_all.SUB_PLATFORM(PLATFORM_ID, CODE, NAME) values (@PlatformID, @SubPlatformCode, @SubPlatformCode)
                        select @SubPlatformID = ID from admin_all.SUB_PLATFORM where PLATFORM_ID = @PlatformID and CODE = @SubPlatformCode
                    end
            end

        if @ImageUrl = 'null' or LEN(@ImageUrl) = 0
            select @ImageUrl = null;

        if @BackgroundImageUrl = 'null' or LEN(@BackgroundImageUrl) = 0
            select @BackgroundImageUrl = null;

        if NOT exists (select ID from admin_all.GAME_INFO where PLATFORM_ID = @PlatformID and GAME_ID = @GameID)
           begin
               insert into admin_all.GAME_INFO(PLATFORM_ID, GAME_ID, NAME, GAME_LAUNCH_ID, GAME_CATEGORY_ID, SUB_PLATFORM_ID, IMAGE_URL, BACKGROUND_IMAGE_URL, IS_FREESPIN_ENABLE, IS_TOUCH, SEGMENT_ID)
               values(@PlatformID, @GameID, @GameName, @GameLaunchID, @GameCategoryID, @SubPlatformID, @ImageUrl, @BackgroundImageUrl, @IsFreespinEnabled, @IsMobile, @SegmentID);
               set @UpdatedCount = @UpdatedCount + 1;
           end

        delete from #Games where Game = @CurrentGame;
    end
  print 'Update ' + cast(@UpdatedCount as varchar(100)) + ' records'
  commit

END

-- exec admin_all.usp_SynchronizeCasinoGameList
--      @PlatformID = 200,
--      @GameListString = 'EVO_LIVE_AMERICAN_ROULETTE_MST,Evolution Live American Roulette (MST),EVO_LIVE_AMERICAN_ROULETTE_MST,TestGameCategory,TestSubPlatform,testImageUrl,testBackgroundImageUrl,1,1;EVO_LIVE_AMERICAN_ROULETTE_SLD,Evolution Live American Roulette (SLD),null,null,null,null,null,null,null;',
--      @DelimiterBetweenGames = ';',
--      @DelimiterBetweenGameFields = ',',
--      @UpdatedCount = null
