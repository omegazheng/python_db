set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationBrand
as
begin
	set nocount on
    	--admin_all.language
	begin transaction
	;with s as 
	(
		select [iso2_code],[name],[iso3_code]
		from (
				values (N'ar',N'Arabic',N'ara')
					,(N'de',N'German',N'deu')
					,(N'el',N'Greek',N'ell')
					,(N'en',N'English',N'eng')
					,(N'es',N'Spanish',N'spa')
					,(N'fr',N'French',N'fre')
					,(N'id',N'Indonesian',N'ind')
					,(N'it',N'Italian',N'ita')
					,(N'ja',N'Japanese',N'jpn')
					,(N'ko',N'Korean',N'kor')
					,(N'nl',N'Dutch',N'spa')
					,(N'pt',N'Portuguese',N'por')
					,(N'ro',N'Romanian',N'ron')
					,(N'th',N'Thai',N'tha')
					,(N'tr',N'Turkish',N'tur')
					,(N'vi',N'Vietnamese',N'vie')
					,(N'zh',N'Chinese',N'chi')
			) v([iso2_code],[name],[iso3_code])
	)
	merge admin_all.language t
	using s on s.[iso2_code]= t.[iso2_code]
	when not matched then
		insert ([iso2_code],[name],[iso3_code])
		values(s.[iso2_code],s.[name],s.[iso3_code])
	when matched and (s.[name] is null and t.[name] is not null or s.[name] is not null and t.[name] is null or s.[name] <> t.[name] or s.[iso3_code] is null and t.[iso3_code] is not null or s.[iso3_code] is not null and t.[iso3_code] is null or s.[iso3_code] <> t.[iso3_code]) then 
		update set  t.[name]= s.[name], t.[iso3_code]= s.[iso3_code]
	;
	commit;

    begin transaction
        if not exists (select * from [admin_all].[CURRENCY])
            begin
                ;with s as
                    (
                    select [iso_code],[name],[numeric_code],[symbol],[is_default],[symbol_code],[NUMBER_DECIMAL_POINT],[is_virtual],[TYPE],[REFRESH_INTERVAL],[AlertThreshold],[StopThreshold]
                    from (
                             values (N'EUR',N'Euro',N'978       ',N' â‚¬        ',1,N'&euro;',2,0,N'C',1440,2160,2880)
                                  ,(N'USD',N'USD',N'840       ',N'$         ',0,N'$',2,0,N'C',1440,2160,2880)
                         ) v([iso_code],[name],[numeric_code],[symbol],[is_default],[symbol_code],[NUMBER_DECIMAL_POINT],[is_virtual],[TYPE],[REFRESH_INTERVAL],[AlertThreshold],[StopThreshold])
                    )
                    merge admin_all.currency t
                using s on s.[iso_code]= t.[iso_code]
                when not matched then
                    insert ([iso_code],[name],[numeric_code],[symbol],[is_default],[symbol_code],[NUMBER_DECIMAL_POINT],[is_virtual],[TYPE],[REFRESH_INTERVAL],[AlertThreshold],[StopThreshold])
                    values(s.[iso_code],s.[name],s.[numeric_code],s.[symbol],s.[is_default],s.[symbol_code],s.[NUMBER_DECIMAL_POINT],s.[is_virtual],s.[TYPE],s.[REFRESH_INTERVAL],s.[AlertThreshold],s.[StopThreshold])
                    ;
            end
    commit;

	----exec admin_all.usp_DataInitializationHelper @TableName = 'admin.casino_brand_def', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate= 0, @ExcludedColumns ='HOUSEID, COMMENTS, SKIN_NAME, FROM_EMAIL, EMAIL_SUBJECT_PREFIX, MOBILE_NAME, SECONDARY_WALLET_PASSWORD, SECONDARY_WALLET_PASSWORD, SECONDARY_WALLET_ENABLED, SECONDARY_WALLET_PLATFORM_ID, SEAMLESS_SECONDARY_WALLET_ENABLED, SSW_PRIMARY_WALLET_URL,SSW_PRIMARY_WALLET_CALLER_ID, SSW_PRIMARY_WALLET_CALLER_PASSWORD, PARENT_BRANDID, HAS_AGENCY'
	--admin.CASINO_BRAND_DEF
    begin transaction
        if not exists (select * from [admin].[CASINO_BRAND_DEF])
        begin
            set identity_insert [admin].[CASINO_BRAND_DEF] on;
            ;with s as
            (
                select [BRANDID],[BRANDNAME],[CREATED_DATE],[URL],[DEFAULT_LANGUAGE]
                from (
                        values (1,N'<BRAND_PLdACEHOLDER>',getdate(),N'<PS Domain PLACEHOLDER>',N'en')
                    ) v([BRANDID],[BRANDNAME],[CREATED_DATE],[URL],[DEFAULT_LANGUAGE])
            )
            merge admin.casino_brand_def t
            using s on s.[BRANDID]= t.[BRANDID]
            when not matched then
                insert ([BRANDID],[BRANDNAME],[CREATED_DATE],[URL],[DEFAULT_LANGUAGE])
                values(s.[BRANDID],s.[BRANDNAME],s.[CREATED_DATE],s.[URL],s.[DEFAULT_LANGUAGE])

            ;
            set identity_insert [admin].[CASINO_BRAND_DEF] off;
        end
    commit;

--exec usp_DataInitializationHelper @TableName = 'admin_all.Brand_Currency', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate= 0
--admin_all.Brand_Currency
    begin transaction
        if not exists (select * from [admin_all].[BRAND_CURRENCY])
            begin
                set identity_insert [admin_all].[BRAND_CURRENCY] on;
                ;with s as
                    (
                    select [ID],[BRANDID],[ISO_CODE],[IS_DEFAULT],[IS_ENABLED]
                    from (
                             values (1,1,N'EUR',1,1)
                         ) v([ID],[BRANDID],[ISO_CODE],[IS_DEFAULT],[IS_ENABLED])
                    )
                    merge admin_all.Brand_Currency t
                using s on s.[ID]= t.[ID]
                when not matched then
                    insert ([ID],[BRANDID],[ISO_CODE],[IS_DEFAULT],[IS_ENABLED])
                    values(s.[ID],s.[BRANDID],s.[ISO_CODE],s.[IS_DEFAULT],s.[IS_ENABLED])

                    ;
                set identity_insert [admin_all].[BRAND_CURRENCY] off;
            end
    commit;
end

