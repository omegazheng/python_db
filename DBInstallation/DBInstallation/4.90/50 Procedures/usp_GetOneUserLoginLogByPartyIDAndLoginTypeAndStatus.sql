SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetOneUserLoginLogByPartyIDAndLoginTypeAndStatus
(
    @PartyID int = null,
    @LoginType nvarchar(50) =  null,
    @Status nvarchar(50) = null
)
as

BEGIN
    if (@PartyID is null)
        RAISERROR('@PartyID cannot be null', 16, 1)
    if (@LoginType is null)
        RAISERROR('@LoginType cannot be null', 16, 1)

    if @Status is null
        select top 1 id, partyId, session_key sessionKey, login_type loginType, status
        from admin_all.user_login_log
        where partyId = @PartyID and login_type = @LoginType and status is null
        order by login_time desc
    else
        select top 1 id, partyId, session_key sessionKey, login_type loginType, status
        from admin_all.user_login_log
        where partyId = @PartyID and login_type = @LoginType and status = @Status
        order by login_time desc
END
