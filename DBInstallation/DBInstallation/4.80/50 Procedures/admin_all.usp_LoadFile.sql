set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_LoadFile
(
	@FilePath nvarchar(max), 
	@Text varchar(max) output,
	@FileType nvarchar(20) = 'SINGLE_CLOB'
)
as
begin	
	set nocount on
	declare @SQL nvarchar(max)
	select @SQL = '
	select @Text = Content
	from openrowset(bulk ''' + replace(@FilePath, '''', '''''')+  ''', single_blob) n(Content)
	'
	exec sp_executesql @SQL, N'@Text varchar(max) output', @Text output
end
go
/*
declare @Text nvarchar(max)
exec admin_all.usp_LoadFile 'c:\temp', @Text output
select *
from admin_all.fn_ReadLine(@Text)
--exec master..xp_cmdshell 'type c:\temp'
*/
