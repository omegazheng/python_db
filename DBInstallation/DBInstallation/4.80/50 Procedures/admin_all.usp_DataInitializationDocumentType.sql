set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_DataInitializationUserDocumentType
as
begin 
	begin transaction
	set identity_insert [admin_all].[user_document_type] on;
	;with s as
	(
		select [id],[type],[has_expiry], [has_number], [init_kyc_score], [reviewed_kyc_score]
		from (
				values(0,N'OTHERS',1, 1, 0, 0)
					 ,(1,N'IDENTITY_CARD',1, 1, 10, 15)
			) v([id],[type],[has_expiry], [has_number], [init_kyc_score], [reviewed_kyc_score])
	)
	merge admin_all.USER_DOCUMENT_TYPE t
	using s on s.[id]= t.[id] and s.[type] = t.[type]
	when not matched then
		insert ([id],[type],[has_expiry], [has_number], [init_kyc_score], [reviewed_kyc_score])
		values(s.[id],s.[type],s.[has_expiry], s.[has_number], s.[init_kyc_score], s.[reviewed_kyc_score])
	when matched then
		update set  t.[has_expiry]= s.[has_expiry], t.[has_number]= s.[has_number], t.[init_kyc_score]= s.[init_kyc_score],
		t.[reviewed_kyc_score]= s.[reviewed_kyc_score]
	;
	set identity_insert [admin_all].[user_document_type] off;
	commit;
end
