set ansi_nulls, quoted_identifier on
go

create or alter procedure [admin_all].usp_GetPlayerLoginLogs
    (@partyid int)
AS
  BEGIN
    select top 5
        logs.id,
        logs.login_time,
        logs.ip,
        country.name as country,
        country.iso2_code as country_code,
        logs.partyid,
        logs.login_type,
        logs.executed_by,
        logs.is_mobile,
        logs.device,
        logs.device_os,
        logs.browser
    from admin_all.user_login_log as logs
        left join admin_all.country on country.iso2_code = logs.country
    where partyid = @partyid and login_type='LOGIN' AND STATUS='SUCCESS'
    order by login_time desc
  END

go
