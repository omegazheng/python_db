SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetCurrencyByIsoCode
(
    @IsoCode nvarchar(10)
)
as

BEGIN
    select iso_code isoCode, name, numeric_code numericCode, symbol, is_default isDefault,
        symbol_code symbolCode, NUMBER_DECIMAL_POINT numberDecimalPoint, is_virtual isVirtual,
        type, REFRESH_INTERVAL refreshInterval, AlertThreshold, StopThreshold, precision
    from admin_all.CURRENCY
    where iso_code = @IsoCode
END
