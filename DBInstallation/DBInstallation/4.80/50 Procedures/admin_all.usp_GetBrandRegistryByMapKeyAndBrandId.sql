SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetBrandRegistryByMapKeyAndBrandId
(
    @MapKey nvarchar(255),
    @BrandId int
)
as
set nocount on
BEGIN
    select id, brandId, MAP_KEY mapKey, value, encrypted, KEY_IDX keyIdx
    from admin_all.BRAND_REGISTRY
    where MAP_KEY = @MapKey and BRANDID = @BrandId
END
