SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- use @partyid for player with one currency only
-- use @partyIds for player with multi-currency
-- DO NOT INPUT BOTH @partyid and @partyIds
create or alter procedure admin_all.usp_GetPlayerTransactionsReportPage
  (
    @partyid   INT = null,
    @startDate DATETIME,
    @endDate   DATETIME,
    @pageNum   INT,
    @pageSize  INT,
    @tranTypes VARCHAR(1000),
    @partyIds VARCHAR(1000) = null
  )
AS
 BEGIN
	set nocount on 
	create table #PartyID (PartyID int primary key)
	insert into #PartyID(PartyID)
		select PartyID
		from (
				select @partyid PartyID
				union
				select try_cast(rtrim(value) as int)
				from string_split(@partyIds,',')
			)a
		where PartyID is not null
	create table #TranType(TranType varchar(10) primary key)
	insert into #TranType(TranType)
		select distinct TranType
		from (
				select nullif(rtrim(ltrim(value)), '') TranType
				from string_split(@tranTypes, ',')
			) a
		where TranType is not null
	declare @SQL nvarchar(max) = '
    WITH TEMP AS (
      SELECT
        ROW_NUMBER()
        OVER (
          ORDER BY a.ID DESC )                                                  AS RowNum,
        a.ID,
        a.DATETIME                                                             as dateTime,
        a.TRAN_TYPE                                                            as tranType,
        (a.balance_real - a.amount_real)                                       as preBalanceReal,
        a.amount_real                                                          as amountReal,
        a.balance_real                                                         as balanceReal,
        (case when a.amount_real < 0
          then amount_real
          else 0 end)                                                           as playerDebit,
        (case when a.amount_real >= 0
          then amount_real
          else 0 end)                                                           as playerCredit,
        (a.BALANCE_RELEASED_BONUS - a.AMOUNT_RELEASED_BONUS)                   AS preBalanceReleasedBonus,
        a.AMOUNT_RELEASED_BONUS                                                AS amountReleasedBonus,
        a.BALANCE_RELEASED_BONUS                                               AS balanceReleasedBonus,
        (CASE WHEN a.AMOUNT_RELEASED_BONUS < 0
          THEN AMOUNT_RELEASED_BONUS
          ELSE 0 END)                                                           AS releasedBonusDebit,
        (CASE WHEN a.AMOUNT_RELEASED_BONUS >= 0
          THEN AMOUNT_RELEASED_BONUS
          ELSE 0 END)                                                           AS releasedBonusCredit,
        (a.BALANCE_PLAYABLE_BONUS - a.AMOUNT_PLAYABLE_BONUS)                   AS preBalancePlayableBonus,
        a.AMOUNT_PLAYABLE_BONUS                                                AS amountPlayableBonus,
        a.BALANCE_PLAYABLE_BONUS                                               AS balancePlayableBonus,
        (CASE WHEN a.AMOUNT_PLAYABLE_BONUS < 0
          THEN AMOUNT_PLAYABLE_BONUS
          ELSE 0 END)                                                           AS playableBonusDebit,
        (CASE WHEN a.AMOUNT_PLAYABLE_BONUS >= 0
          THEN AMOUNT_PLAYABLE_BONUS
          ELSE 0 END)                                                           AS playableBonusCredit,
        ((a.balance_real - a.amount_real)
          + (a.BALANCE_RELEASED_BONUS - a.AMOUNT_RELEASED_BONUS)
          + (a.BALANCE_PLAYABLE_BONUS - a.AMOUNT_PLAYABLE_BONUS))               AS preBalance,
        (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_PLAYABLE_BONUS + a.AMOUNT_REAL)    AS amount,
        (a.BALANCE_REAL + a.BALANCE_RELEASED_BONUS + a.BALANCE_PLAYABLE_BONUS) AS balance,
--         (CASE WHEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS) < 0
--           THEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS)
--           ELSE 0 END)                                                           AS debit,
--         (CASE WHEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS) >= 0
--           THEN (a.AMOUNT_RELEASED_BONUS + a.AMOUNT_REAL + a.AMOUNT_PLAYABLE_BONUS)
--           ELSE 0 END)                                                           AS credit,
        a.game_id                                                              as game_id,
        g.name                                                                 as gamename,
        p.name                                                                 as platformName,
        p.code                                                                 as platformCode,
        a.platform_tran_id                                                     as platformTranId,
        a.game_tran_id                                                         as gameTranId,
        (a.BALANCE_RAW_LOYALTY - AMOUNT_RAW_LOYALTY)                           as preBalanceLoyalty,
        (CASE WHEN a.AMOUNT_RAW_LOYALTY < 0
          THEN a.AMOUNT_RAW_LOYALTY
          ELSE 0 end)                                                           AS loyaltyDebit,
        (CASE WHEN a.AMOUNT_RAW_LOYALTY > 0
          THEN a.AMOUNT_RAW_LOYALTY
          ELSE 0 end)                                                           AS loyaltyCredit,
        a.BALANCE_RAW_LOYALTY                                                  AS balanceLoyalty,
        pc.PLATFORM_AMOUNT                                                     AS platformConvAmount,
        pc.PLATFORM_CURRENCY                                                   AS platformConvCurrency,
        pc.ID                                                                  AS platformConvId,
        a.reference,
        u.CURRENCY
      from
        admin_all.ACCOUNT_TRAN a with ( NOLOCK )
			inner join #TranType tt on tt.TranType = a.TRAN_TYPE
        left join admin_all.PLATFORM p with ( NOLOCK ) on p.id = a.platform_id
        left join admin_all.GAME_INFO g with ( NOLOCK ) on a.GAME_ID = g.GAME_ID and a.platform_id = g.PLATFORM_ID
        left join admin_all.TRANSACTION_PLATFORM_CONVERSION pc with ( NOLOCK ) on pc.TRANSACTION_ID = a.id
        join admin_all.ACCOUNT aa with ( NOLOCK ) on aa.id = a.account_id
        join external_mpt.user_conf u with ( NOLOCK ) on u.partyid = aa.partyid
      where
        a.datetime >= @startDate
        and a.datetime < @endDate
		'+
			case when exists(select * from #PartyID) then ' and u.partyid in (select PartyID from #PartyID)' else '' end
		+'
--        and CHARINDEX(a.TRAN_TYPE,@tranTypes) > 0
        --and (@partyid is null or u.partyid = @partyid)
        --and (@partyIds is null or u.partyid in (SELECT * FROM [admin_all].fc_splitDelimiterString(@partyIds, '','')))
    )
    SELECT
      distinct
      TEMP.*,
      temp.playableBonusDebit + temp.releasedBonusDebit + temp.playerDebit as debit,
      temp.playableBonusCredit + temp.releasedBonusCredit + temp.playerCredit as credit,
      bat.AccountTranId    as tranIdWithBonus
    FROM TEMP
      left join BonusPlan.BonusAccountTran bat on TEMP.id = bat.AccountTranId
    WHERE
      (RowNum > @pageSize * (@pageNum - 1))
      AND
      (RowNum <= @pageSize * @pageNum)
    ORDER BY RowNum;
	'
	--exec maint.usp_PrintString @SQL
	exec sp_executesql @SQL, N'@startDate DATETIME, @endDate   DATETIME,  @pageNum   INT,   @pageSize  INT', @startDate, @endDate, @pageNum, @pageSize
  END
  go
  --exec admin_all.usp_GetPlayerTransactionsReportPage @partyid=100108468, @startDate = '2016-01-01 0:0:0.000', @endDate = '2020-05-05 23:23:23.000', @pageNum = 1, @pageSize = 10,     @tranTypes='GAME_BET,GAME_WIN';