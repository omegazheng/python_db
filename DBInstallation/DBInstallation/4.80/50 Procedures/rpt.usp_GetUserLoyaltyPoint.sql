set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetUserLoyaltyPoint
(
	@StartDate datetime,
	@BrandID int
)
as
begin
	set nocount on
	select
	  @BrandID BrandID,
	  PartyID,
	  sum(RawLoyalty) as LoyaltyPoints
	from admin_all.AccountTranHourlyAggregate
	where TranType = 'GAME_BET'
	  and Datetime >= @StartDate
	  and BrandID = @BrandID
	  and AggregateType = 0
	group by PartyID
end
go
