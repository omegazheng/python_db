set ansi_nulls on
go
set quoted_identifier on
go
CREATE OR ALTER PROCEDURE  admin_all.usp_AddProductToCommissionsWithProduct
(
  @NewProduct nvarchar(50) = null,
  @ExistingProduct nvarchar(50) = null
)
as
begin
    declare @NewProductID int
        set @NewProductID = admin_all.fn_GetProduct(@NewProduct)
    declare @ExistingProductID int
    set @ExistingProductID =  admin_all.fn_GetProduct(@ExistingProduct)


    -- 1. Add brand product if required
    insert into admin_all.BRAND_PLATFORM (brand_id, platform_id, is_enabled)
    select brand_id, @NewProductID , IS_ENABLED from admin_all.BRAND_PLATFORM where PLATFORM_ID = @ExistingProductID
    and BRAND_ID not in
        (select brand_id from admin_all.BRAND_PLATFORM where PLATFORM_ID = @NewProductID)

    -- 2. add the product to commission platform
    insert into admin_all.COMMISSION_PLATFORM (COMMISSION_ID, PLATFORM_ID)
    select COMMISSION_ID, @NewProductID as platform_id from admin_all.COMMISSION_PLATFORM
    where PLATFORM_ID = @ExistingProductID
        and COMMISSION_ID not in (
            select COMMISSION_ID from admin_all.COMMISSION_PLATFORM where PLATFORM_ID = @NewProductID
        )
    return
end
