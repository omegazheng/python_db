set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetAccountRevenueByDate
(
	@StartDate Date,
	@EndDate Date,
	@BrandIDs varchar(max) = null,
	@Currencies varchar(max) = null
)
as
begin
	set nocount on
	create table #Brands (BrandID int primary key)
	create table #Currency (CurrencyID smallint primary key, Currency nvarchar(10) unique)

	insert into #Brands(BrandID)
		select BrandID
		from admin.CASINO_BRAND_DEF
		where @BrandIDs is null
			or BRANDID in (select cast(value as int ) from string_split(@BrandIDs, ','))
	insert into #Currency(CurrencyID, Currency)
		select CurrencyID, Code
		from rpt.Currency
		where @Currencies is null
			or Code in (select value  from string_split(@Currencies, ','))

	;with p as
	(
		select	p.Date, c.Currency, 
				sum(case when p.PaymentTypeID = 1 and p.PaymentStatusID = 1 then p.Amount else 0 end) Deposit,
				sum(case when p.PaymentTypeID = 2 and p.PaymentStatusID = 1 then p.Amount else 0 end) Withdrawal
		from rpt.PaymentAggregateDaily p
			inner join #Currency c on c.CurrencyID = p.CurrencyID
		where p.Date >= @StartDate
			and p.Date < @EndDate
			and exists(select * from #Brands b where b.BrandID = p.BrandID)
		group by c.Currency
	),
	a as
	(
		select a.Datetime, a.Currency, 
				sum(a.Handle) Handle, 
				sum(a.PNL) as PNL, 
				case when sum(a.Handle) = 0 then 0 else round(sum(a.PNL)/sum(a.Handle) * 100, 2) end Hold
		from admin_all.v_PlayerAggregateBase a
		where a.Datetime >= @StartDate
			and a.Datetime < @EndDate
			and exists(select * from #Brands b where b.BrandID = a.BrandID)
		group by a.Datetime, a.Currency
	)
	select 
			a.Datetime,
			a.Currency,
			a.Handle,
			a.Hold,
			a.PNL,
			isnull(p.Deposit, 0) Deposit,
			isnull(p.Withdrawal, 0) Withdrawal
	from a
		left join p on a.Currency = p.Currency and a.Datetime = p.Date
	order by 1,2	
end
	
go
