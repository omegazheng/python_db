set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetLastDayOfWeek(@Date date)
returns Date
as
begin
	return dateadd(day, (7- datepart(weekday, @Date)) , @Date)
end
go

