set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetFirstDayOfMonth(@Date date)
returns date
as
begin
	return datefromparts(year(@Date), month(@Date), 1)
end
go
--select rpt.fn_GetFirstDayOfMonth('2019-12-12')
