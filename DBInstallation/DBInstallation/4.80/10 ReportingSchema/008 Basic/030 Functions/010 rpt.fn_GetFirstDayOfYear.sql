set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetFirstDayOfYear(@Date date)
returns date
as
begin
	return datefromparts(year(@Date), 1, 1)
end
go
--select rpt.fn_GetFirstDayOfYear('2015-01-01')
