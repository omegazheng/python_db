set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_MinBinary(@Value1 varbinary(max), @Value2 varbinary(max))
returns varbinary(max)
as
begin
	return case when isnull(@Value1, 0x0) < isnull(@Value2, 0x0) then @Value1 else @Value2 end
end
go

