set ansi_nulls, quoted_identifier on
go
create or alter function rpt.fn_GetStartTimeOfHour(@Date datetime2(0))
returns datetime2(0)
as
begin
	return dateadd(hour, datediff(hour, '1990-01-01', @Date), '1990-01-01')
end
go
--select rpt.fn_GetFirstDayOfMonth('2019-12-12')
