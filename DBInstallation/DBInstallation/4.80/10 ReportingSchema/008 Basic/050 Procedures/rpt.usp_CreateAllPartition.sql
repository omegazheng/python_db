set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_CreateAllPartition
(
	@DateFrom date = null,
	@DateTo date = null
)
as
begin

	set nocount, xact_abort on 
	declare @pv date
	select @pv = max(cast(pv.value as date))
	from sys.partition_range_values pv 
		inner join sys.partition_functions pf on pv.function_id = pf.function_id
	where pf.name = 'PF_Report_Daily'
	if @@rowcount = 0 and @DateFrom is null
	begin
		select @DateFrom = (select min(datetime) from admin_all.ACCOUNT_TRAN_ALL)
	end
	if @DateFrom is null and @pv is not null
		select @DateFrom = @pv

	select @DateFrom = isnull(@DateFrom, getdate() )

	if @DateFrom < dateadd(year, -10, getdate())
		select @DateFrom = dateadd(year, -10, getdate())

	select @DateTo = isnull(@DateTo, dateadd(month, 1,getdate()))
	declare @SQL nvarchar(max), @Date datetime2(0)

	declare cCreateAllPartition cursor static local for
		with x0 as
		(
			select cast(@DateFrom as datetime2(0)) as Date
			union all
			select dateadd(day, 1, x0.Date)
			from x0
			where Date <=@DateTo
		)
		
		select x0.Date
		from x0
		union
		select dateadd(day, 1, @DateTo)
		union
		select rpt.fn_GetFirstDayOfWeek(dateadd(week, 1, @DateTo))
		union
		select rpt.fn_GetFirstDayOfMonth(dateadd(month, 1, @DateTo))
		union 
		select rpt.fn_GetFirstDayOfYear(dateadd(year, 1, @DateTo))
		union
		select dateadd(day, -1, @DateFrom)
		union
		select rpt.fn_GetFirstDayOfWeek(dateadd(week, -1, @DateFrom))
		union
		select rpt.fn_GetFirstDayOfMonth(dateadd(month, -1, @DateFrom))
		union 
		select rpt.fn_GetFirstDayOfYear(dateadd(year, -1, @DateFrom))
		order by 1
		option(maxrecursion 0)
	open cCreateAllPartition
	fetch next from cCreateAllPartition into @Date
	while @@fetch_status = 0
	begin
		if not exists(
						select *
						from sys.partition_range_values pv 
							inner join sys.partition_functions pf on pv.function_id = pf.function_id
						where pf.name = 'PF_Report_Daily'
							and cast(pv.value as date) = @Date
					)
		begin
			select @SQL = 'alter partition scheme PS_Report_Daily next used [Report]; alter partition function PF_Report_Daily() split range(' + quotename(convert(varchar(10), @Date, 120), '''') + '); alter partition scheme PS_Report_Daily next used [Report];'
			print @SQL
			exec(@SQL)
		end
		if not exists(
						select *
						from sys.partition_range_values pv 
							inner join sys.partition_functions pf on pv.function_id = pf.function_id
						where pf.name = 'PF_Report_Daily_Date'
							and cast(pv.value as date) = @Date
					)
		begin
			select @SQL = 'alter partition scheme PS_Report_Daily_Date next used [Report]; alter partition function PF_Report_Daily_Date() split range(' + quotename(convert(varchar(10), @Date, 120), '''') + '); alter partition scheme PS_Report_Daily_Date next used [Report];'
			print @SQL
			exec(@SQL)
		end
		if rpt.fn_GetFirstDayOfWeek(@Date) = @Date
		begin
			if not exists(
						select * 
						from sys.partition_range_values pv 
							inner join sys.partition_functions pf on pv.function_id = pf.function_id
						where pf.name = 'PF_Report_Weekly'
							and cast(pv.value as date) = @Date
					)
			begin
				select @SQL = 'alter partition scheme PS_Report_Weekly next used [Report]; alter partition function PF_Report_Weekly() split range(' + quotename(convert(varchar(10), @Date, 120), '''') + '); alter partition scheme PS_Report_Weekly next used [Report];'
				print @SQL
				exec(@SQL)
			end
		end
		if rpt.fn_GetFirstDayOfMonth(@Date) = @Date
		begin

			if not exists(
						select * 
						from sys.partition_range_values pv 
							inner join sys.partition_functions pf on pv.function_id = pf.function_id
						where pf.name = 'PF_Report_Monthly'
							and cast(pv.value as date) = @Date
					)
			begin
				select @SQL = 'alter partition scheme PS_Report_Monthly next used [Report]; alter partition function PF_Report_Monthly() split range(' + quotename(convert(varchar(10), @Date, 120), '''') + '); alter partition scheme PS_Report_Monthly next used [Report];'
				print @SQL
				exec(@SQL)
			end
		end
		if rpt.fn_GetFirstDayOfYear(@Date) = @Date
		begin
			if not exists(
						select * 
						from sys.partition_range_values pv 
							inner join sys.partition_functions pf on pv.function_id = pf.function_id
						where pf.name = 'PF_Report_Yearly'
							and cast(pv.value as date) = @Date
					)
			begin
				select @SQL = 'alter partition scheme PS_Report_Yearly next used [Report]; alter partition function PF_Report_Yearly() split range(' + quotename(convert(varchar(10), @Date, 120), '''') + '); alter partition scheme PS_Report_Yearly next used [Report];'
				print @SQL
				exec(@SQL)
			end
		end
		fetch next from cCreateAllPartition into @Date
	end
	close cCreateAllPartition
	deallocate cCreateAllPartition
end
go
--exec rpt.usp_CreateAllPartition '2011-01-01'
