set ansi_nulls, quoted_identifier on
--drop table rpt.PaymentStatus
go
if object_id('rpt.PaymentStatus') is null
begin
	create table rpt.PaymentStatus
	(
		PaymentStatusID smallint not null,
		PaymentStatus varchar(50) not null,
		Description varchar(128) null
		constraint PK_rpt_PaymentStatus primary key clustered (PaymentStatusID) on [Report],
		constraint UQ_rpt_PaymentStatus_PaymentStatus unique (PaymentStatus) on [Report]
	)  on [Report]
end
