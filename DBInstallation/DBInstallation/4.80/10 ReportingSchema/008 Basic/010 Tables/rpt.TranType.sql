set ansi_nulls, quoted_identifier on
--drop table rpt.TranType
go
if object_id('rpt.TranType') is null
begin
	create table rpt.TranType
	(
		TranTypeID smallint not null,
		TranType varchar(50) not null,
		Description varchar(128) null
		constraint PK_rpt_TranType primary key clustered (TranTypeID) on [Report],
		constraint UQ_rpt_TranType_TranType unique (TranType) on [Report]
	)  on [Report]
end
