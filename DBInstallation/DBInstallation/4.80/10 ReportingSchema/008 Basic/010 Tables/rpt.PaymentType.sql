set ansi_nulls, quoted_identifier on
--drop table rpt.PaymentType
go
if object_id('rpt.PaymentType') is null
begin
	create table rpt.PaymentType
	(
		PaymentTypeID smallint not null,
		PaymentType varchar(50) not null,
		Description varchar(128) null
		constraint PK_rpt_PaymentType primary key clustered (PaymentTypeID) on [Report],
		constraint UQ_rpt_PaymentType_PaymentType unique (PaymentType) on [Report]
	)  on [Report]
end
