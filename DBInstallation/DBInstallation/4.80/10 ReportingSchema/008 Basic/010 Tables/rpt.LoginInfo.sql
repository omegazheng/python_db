set ansi_nulls, quoted_identifier on
--drop table rpt.LoginInfo
go
if object_id('rpt.LoginInfo') is null
begin
	create table rpt.LoginInfo
	(
		LoginInfoID smallint identity(1,1) not null ,
		LoginInfo varchar(100) not null,
		Description varchar(128) null
		constraint PK_rpt_LoginInfo primary key clustered (LoginInfoID) on [Report],
		constraint UQ_rpt_LoginInfo_LoginInfo unique (LoginInfo) on [Report]
	)  on [Report]
end


--set identity_insert rpt.LoginInfo on
--insert into rpt.LoginInfo(LoginInfoID, LoginInfo) values('-1', 'UNKNOWN')
--set identity_insert rpt.LoginInfo off
