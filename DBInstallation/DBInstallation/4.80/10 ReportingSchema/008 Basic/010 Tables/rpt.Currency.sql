set ansi_nulls, quoted_identifier on
--drop table rpt.Currency
go
if object_id('rpt.Currency') is null
begin
	create table rpt.Currency
	(
		CurrencyID smallint not null,
		Code nvarchar(10) not null,
		Name varchar(128) not null
		constraint PK_rpt_Currency primary key clustered (CurrencyID) on [Report],
		constraint UQ_rpt_Currency_Code unique (Code) on [Report]
	)  on [Report]
end
