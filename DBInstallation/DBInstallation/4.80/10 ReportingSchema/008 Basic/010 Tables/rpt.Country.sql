set ansi_nulls, quoted_identifier on
--drop table rpt.Country
go
if object_id('rpt.Country') is null
begin
	create table rpt.Country
	(
		CountryID smallint not null,
		Code2 varchar(8) not null,
		Code3 varchar(8) not null,
		Name varchar(128) null
		constraint PK_rpt_Country primary key clustered (CountryID) on [Report],
		constraint UQ_rpt_Country_Code2 unique (Code2) on [Report],
		constraint UQ_rpt_Country_Code3 unique (Code3) on [Report]
	)  on [Report]
end
go

--insert into rpt.Country(CountryID, Code2, Code3, Name)
--	select numeric_code, iso2_code, iso3_code, name
--	from admin_all.COUNTRY
