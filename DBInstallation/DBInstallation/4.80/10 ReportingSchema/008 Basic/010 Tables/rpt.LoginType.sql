set ansi_nulls, quoted_identifier on
--drop table rpt.LoginType
go
if object_id('rpt.LoginType') is null
begin
	create table rpt.LoginType
	(
		LoginTypeID smallint not null,
		LoginType varchar(50) not null,
		Description varchar(128) null
		constraint PK_rpt_LoginType primary key clustered (LoginTypeID) on [Report],
		constraint UQ_rpt_LoginType_LoginType unique (LoginType) on [Report]
	)  on [Report]
end
