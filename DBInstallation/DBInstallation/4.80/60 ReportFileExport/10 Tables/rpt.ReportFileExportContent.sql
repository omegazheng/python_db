set ansi_nulls, quoted_identifier on
go
if object_id('rpt.ReportFileExportContent') is null
begin
	create table rpt.ReportFileExportContent
	(
		ReportFileExportID uniqueidentifier not null,
		Content varbinary(max) not null,
		constraint PK_rpt_ReportFileExportContent primary key(ReportFileExportID) on Report
	) on Report
end
	