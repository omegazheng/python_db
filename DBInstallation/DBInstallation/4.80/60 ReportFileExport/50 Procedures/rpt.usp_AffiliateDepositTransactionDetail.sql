set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_AffiliateDepositTransactionDetail
(
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@Offset char(6) = null,
	@BrandIDs varchar(max) = null,
	@CodeKey varchar(20) = 'btag',
	@OutputTarget varchar(20) = 'Table' -- File
)
as
begin

	set nocount on
    select @DateFrom = admin_all.fn_ConvertDatetimeZone(isnull(@DateFrom, cast(getdate() as date)), @Offset, null)
	select @DateTo = admin_all.fn_ConvertDatetimeZone(isnull(@DateTo, dateadd(d, 1, @DateFrom)), @Offset, null)

	create table #BrandID(BrandID int, BrandName nvarchar(1000), primary key (BrandID))
	insert into #BrandID(BrandID, BrandName)
		select BRANDID, BRANDNAME
		from admin.CASINO_BRAND_DEF
		where @BrandIDs is null
			or BRANDID in (
							select cast(value as int) 
							from (select value from string_split(@BrandIDs, ',') s)  a
						)

						--select * from admin_all.TIME_ZONE
    ;with x0 as
	(
		select pc.PartyID, pc.AssociatedPartyID, pc.AssociatedAccountID,
			cast(admin_all.fn_ConvertDatetimeZone(p.PROCESS_DATE, null, @Offset) as date) PROCESS_DATE,
			cast(admin_all.fn_ConvertDatetimeZone(p.REQUEST_DATE, null, @Offset) as date) REQUEST_DATE,
			pc.AssociatedCurrency,
			p.AMOUNT,
			p.AMOUNT_REAL,
			p.AMOUNT_RELEASED_BONUS,
			pc.Currency
    
		from admin_all.USER_TRACKING_CODE utc
			inner join external_mpt.USER_CONF uc on uc.PARTYID = utc.PARTYID
			inner join #BrandID cbd on cbd.BrandID = uc.BRANDID
			inner join external_mpt.v_UserPrimaryCurrency pc with(noexpand) on pc.PartyID = uc.PARTYID
			inner join admin_all.PAYMENT p on p.ACCOUNT_ID = pc.AssociatedAccountID
		where CODE_KEY = @CodeKey and p.PROCESS_DATE >= @DateFrom and p.PROCESS_DATE < @DateTo
			and uc.REGISTRATION_STATUS = 'PLAYER'
			and p.STATUS in( 'COMPLETED', 'DP_ROLLBACK')
			and p.TYPE = 'DEPOSIT'
	)
	select 
				PartyID, AssociatedPartyID, AssociatedAccountID, PROCESS_DATE, REQUEST_DATE, AssociatedCurrency, Currency,
				sum(AMOUNT) AMOUNT,
				sum(AMOUNT_REAL) AMOUNT_REAL,
				sum(AMOUNT_RELEASED_BONUS) AMOUNT_RELEASED_BONUS
				into #temp
				
	from x0
	group by PartyID, AssociatedPartyID, AssociatedAccountID, PROCESS_DATE, REQUEST_DATE, AssociatedCurrency, Currency
	--select @OutputColumns = stuff((select ','+name as column_name from tempdb.sys.columns where object_id = object_id('tempdb..#temp') order by column_id for xml path(''), type).value('.', 'varchar(max)'), 1, 1, '');
	if @OutputTarget = 'File'
	begin
		exec rpt.usp_GenerateReportFileExportContent @Query = 'select PartyID, convert(varchar(100), isnull(PROCESS_DATE, REQUEST_DATE), 120) AmountDate, convert(numeric(38,2), AMOUNT) Amount, Currency AmountCurrency, null AmountCategory from #temp', 
				@Mapping = '<Mappings><Mapping SourceColumn="PartyID" ReportColumn="PIN"/><Mapping SourceColumn="AmountDate"/><Mapping SourceColumn="Amount"/><Mapping SourceColumn="AmountCurrency"/><Mapping SourceColumn="AmountCategory"/></Mappings>',
				@Test = 0
		return
	end
    select * from #temp
end
go
--exec rpt.usp_AffiliateDepositTransactionDetail @DateFrom = '2000-01-01', @DateTo = '2030-1-01', @OutputTarget = 'File'
--select * from omegasys_dev.rpt.paymentStatus


--select convert(varchar(100), getdate(), 120)