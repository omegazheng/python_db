set ansi_nulls, quoted_identifier on
go

-- Currently maint is running hourly, it may change
create or alter procedure maint.usp_OmegaMaintenance
as
begin
	set nocount on
	exec maint.usp_UpdateStatistics
	exec admin_all.usp_CleanupSentEmails
	exec rpt.usp_ScheduleAffiliateReportFileExport
end
