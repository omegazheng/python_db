set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_ScheduleAffiliateReportFileExport
as
begin
	set nocount on
	if datepart(hour,getdate()) < 3
		return
	if cast(isnull(admin_all.fn_GetRegistry('ScheduleAffiliateReportFileExport'), '') as bit) <> 1
		return
	declare @DateFrom datetime, @DateTo Datetime, @Offset char(6) = '+00:00', @BrandIDs varchar(max), @CodeKey varchar(20) = 'ClickId'
	select @DateFrom = dateadd(day, -1, cast(admin_all.fn_ConvertDatetimeZone(Getdate(), null, @Offset) as date))
	select @DateTo = Dateadd(day, 1, @DateFrom)
	exec rpt.usp_GetAffiliateReport @ReportFileExportID = null, @Type = 'Egass Registration Report', @DateFrom = @DateFrom, @DateTo = @DateTo,	@Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
	exec rpt.usp_GetAffiliateReport @ReportFileExportID = null, @Type = 'Deposit Report', @DateFrom = @DateFrom, @DateTo = @DateTo,	@Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
	exec rpt.usp_GetAffiliateReport @ReportFileExportID = null, @Type = 'Net Revenue Report', @DateFrom = @DateFrom, @DateTo = @DateTo,	@Offset = @Offset, @BrandIDs = @BrandIDs, @CodeKey = @CodeKey, @MetaDataOnly = 0, @IgnoreResult = 1
end
go



