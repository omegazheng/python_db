set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.usp_DataInitializationBatchJob') is null
  exec('create procedure admin_all.usp_DataInitializationBatchJob as ')
go
alter procedure admin_all.usp_DataInitializationBatchJob
as
begin
	set nocount on
	--exec [admin_all].[usp_DataInitializationHelper] @TableName = 'admin_all.BatchJobType', @ForceIdentityColumnInsert = 1, @ForceMatchedDataUpdate = 1, @PrimaryKeys = null, @ExcludedColumns = null
	--admin_all.BatchJobType
	begin transaction
	set identity_insert [admin_all].[BatchJobType] on;
	;with s as 
	(
		select [BatchJobTypeID],[Name],[Description],[ProductID],[MaxAllowableTasks],[Parser],[Renderer],[Presenter]
		from (
				values (1,N'EXTERNAL_BONUS_KAMBI',N'EXTERNAL_BONUS_KAMBI',null,1000,N'admin_all.fn_BatchJobTypeParserJson',N'admin_all.fn_BatchJobTypeRendererKambiExnternal',N'admin_all.fn_BatchJobTypeRendererKambiExnternal')
					,(2,N'FREEBET_BONUS_KAMBI',N'FREEBET_BONUS_KAMBI',null,1000,N'admin_all.fn_BatchJobTypeParserJson',N'admin_all.fn_BatchJobTypeRendererKambiFreeBet',N'admin_all.fn_BatchJobTypeRendererKambiFreeBet')
					,(3,N'FREESPIN_EVERYMATRIX',N'FREESPIN_EVERYMATRIX',null,1,N'admin_all.fn_BatchJobTypeParserJson',N'admin_all.fn_BatchJobTypeRendererEveryMatrixFreespin',N'admin_all.fn_BatchJobTypeRendererEveryMatrixFreespin')
					,(4,N'MANUAL_BONUS',N'MANUAL_BONUS',null,1000,N'admin_all.fn_BatchJobTypeParserJson',N'admin_all.fn_BatchJobTypeRendererManualBonus',N'admin_all.fn_BatchJobTypeRendererManualBonus')
					-- Note: Negative number is the system job type that is only been used by OMEGA System Internal
					,(-1, N'SendEmail',N'SendEmail',null,1,N'admin_all.fn_BatchJobTypeParserOneTaskString',N'admin_all.fn_BatchJobTypeRendererEmail',N'admin_all.fn_BatchJobTypeRendererEmail')
					,(-2, N'ReportFileExport',N'Generate report and save in database.',null,1,N'admin_all.fn_BatchJobTypeParserOneTaskString',N'admin_all.fn_BatchJobTypeParserOneTaskString',N'admin_all.fn_BatchJobTypeParserOneTaskString')
			) v([BatchJobTypeID],[Name],[Description],[ProductID],[MaxAllowableTasks],[Parser],[Renderer],[Presenter])
	)
	merge admin_all.BatchJobType t
	using s on s.[BatchJobTypeID]= t.[BatchJobTypeID]
	when not matched then
		insert ([BatchJobTypeID],[Name],[Description],[ProductID],[MaxAllowableTasks],[Parser],[Renderer],[Presenter])
		values(s.[BatchJobTypeID],s.[Name],s.[Description],s.[ProductID],s.[MaxAllowableTasks],s.[Parser],s.[Renderer],s.[Presenter])
	when matched and (s.[Name] is null and t.[Name] is not null or s.[Name] is not null and t.[Name] is null or s.[Name] <> t.[Name] or s.[Description] is null and t.[Description] is not null or s.[Description] is not null and t.[Description] is null or s.[Description] <> t.[Description] or s.[ProductID] is null and t.[ProductID] is not null or s.[ProductID] is not null and t.[ProductID] is null or s.[ProductID] <> t.[ProductID] or s.[MaxAllowableTasks] is null and t.[MaxAllowableTasks] is not null or s.[MaxAllowableTasks] is not null and t.[MaxAllowableTasks] is null or s.[MaxAllowableTasks] <> t.[MaxAllowableTasks] or s.[Parser] is null and t.[Parser] is not null or s.[Parser] is not null and t.[Parser] is null or s.[Parser] <> t.[Parser] or s.[Renderer] is null and t.[Renderer] is not null or s.[Renderer] is not null and t.[Renderer] is null or s.[Renderer] <> t.[Renderer] or s.[Presenter] is null and t.[Presenter] is not null or s.[Presenter] is not null and t.[Presenter] is null or s.[Presenter] <> t.[Presenter]) then 
		update set  t.[Name]= s.[Name], t.[Description]= s.[Description], t.[ProductID]= s.[ProductID], t.[MaxAllowableTasks]= s.[MaxAllowableTasks], t.[Parser]= s.[Parser], t.[Renderer]= s.[Renderer], t.[Presenter]= s.[Presenter]
	;
	set identity_insert [admin_all].[BatchJobType] off;
	commit;
   

end
go
