set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetRandomString(@Str varchar(1000))
returns varchar(1000)
as
begin
	declare @ret varchar(1000) = '', @i int = 1, @c char(1)
	while @i <= len(@Str)
	begin
		select @c = substring(@Str, @i, 1)
		if @c in (' ', '-', '@', ',', '.')
			select @ret = @ret + @c
		else if isnumeric(@c) = 1
			select @ret = @ret + substring('0123456789', cast(Rand * 9 as int) + 1, 1)
			from maint.v_Rand
		else 
			select @ret = @ret + substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ', cast(Rand * 25 as int) + 1, 1)
			from maint.v_Rand
		select @i = @i + 1
	end
	return @ret
end
go

