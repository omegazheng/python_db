set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetCommissionTemplatesWithProduct
(
    @Product nvarchar(50)
)
    returns @ItemTable table(CommissionID int)
as
begin
    insert into @ItemTable(CommissionID)
    select COMMISSION_ID from admin_all.COMMISSION_PLATFORM where PLATFORM_ID = admin_all.fn_GetProduct(@Product)
    return
end
go

-- select CommissionID from admin_all.fn_GetCommissionTemplatesWithProduct('betsoft')
