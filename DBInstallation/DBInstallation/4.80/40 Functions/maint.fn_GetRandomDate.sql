set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetRandomDate(@Date datetime)
returns datetime
as
begin
	declare @ret datetime
	select @ret = dateadd(second, case when rand > 0.5 then 1 else -1 end * (Rand * 86500), dateadd(day, case when rand > 0.5 then 1 else -1 end * (Rand * 100) ,@Date))
	from maint.v_Rand
	return @ret
end
go

