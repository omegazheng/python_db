set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetProduct
(
    @Product nvarchar(50)
)
    returns int
as
begin
    Declare @ProductID int
    if isnumeric(@Product) = 1
        select @ProductID = ID from admin_all.Platform where ID = cast(@Product as int)
    else
        select top 1 @ProductID = ID from admin_all.Platform where code like @Product or name like @Product
    return @ProductID
end
go

-- select admin_all.fn_GetProduct('betsoft')
