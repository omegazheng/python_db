set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetCustomerAndSystemName(
    @SystemName nvarchar(100)
)
    returns nvarchar(100)
as
begin
    declare @ret nvarchar(200) = ''
    declare @CustomerName nvarchar(100) = ''

    select top 1 @CustomerName = ltrim(rtrim(name))
        from dbo.CUSTOMER;

    if (@SystemName = '')
        begin
            SET @ret = @CustomerName
        end
    else if (CHARINDEX(@CustomerName, @SystemName) = 1)
        begin
           SET @ret = @SystemName
        end
    else
        begin
           SET @ret = @CustomerName + ' - ' + @SystemName
        end

    return @ret
end
go
