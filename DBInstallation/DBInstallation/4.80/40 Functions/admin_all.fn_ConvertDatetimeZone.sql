set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_ConvertDatetimeZone(@Date datetime2(7), @FromOffset char(6) = null, @ToOffset char(6) = null)
returns datetime2(7)
as
begin
	return cast(switchoffset(todatetimeoffset(@Date, isnull(@FromOffset, admin_all.fn_GetCurrentTimeZone())), isnull(@ToOffset, admin_all.fn_GetCurrentTimeZone())) as datetime2(7))
end
go


