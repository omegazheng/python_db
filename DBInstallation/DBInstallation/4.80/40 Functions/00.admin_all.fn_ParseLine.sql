set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_ParseLine(@Text nvarchar(max), @Separator varchar(1))
returns table
as
return 
(
		select row_number() over(order by ID) ColumnID, 
			Value
		from (
					select 1 as ID, value
					from string_split(@Text, @Separator) v
			) a
)
go
