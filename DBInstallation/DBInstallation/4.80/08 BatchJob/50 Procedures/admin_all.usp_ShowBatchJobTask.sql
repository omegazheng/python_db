set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_ShowBatchJobTask
(
	@BatchJobID int
)
as
begin
	set nocount on
	declare @Presenter nvarchar(128), @SQL nvarchar(max)
	select  @Presenter = jt.Presenter
	from admin_all.BatchJob b 
		inner loop join admin_all.BatchJobType jt on jt.BatchJobTypeID = b.Type
	where b.BatchJobID = @BatchJobID
	select @SQL  = 'select t.*, a.*
	from admin_all.BatchJobTask t
		cross apply ' + @Presenter + '(t.Content) a
	where t.BatchJobID = @BatchJobID'
	exec sp_executesql @SQL, N'@BatchJobID int', @BatchJobID
end
go
--exec admin_all.usp_ShowBatchJobTask 1