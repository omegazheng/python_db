set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_AddBatchJobTaskByJson
(
	@BatchJobID int, 
	@Content nvarchar(max)
)
as
begin
    exec admin_all.usp_AddBatchJobTask  @BatchJobID, @Content
end
go
-- exec admin_all.usp_AddBatchJobTaskByJson 1, '[{"playerId":"1005145","bonusProgramId":10485}, {"playerId":"1005146","bonusProgramId":"10486"}]'

