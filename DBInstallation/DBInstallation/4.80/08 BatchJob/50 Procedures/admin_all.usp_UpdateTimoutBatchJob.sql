set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_UpdateTimeoutBatchJob
(
    @Timeout int = 1 --Timeout by day, default is 1 day
)
as
begin
    set nocount on
    begin transaction
        exec sp_getapplock 'BatchJob', 'Exclusive', 'Transaction', -1
        update BatchJobTask set status = 'FAILED' where status = 'RUNNING' AND BatchJobID in (
            select BatchJobID from BatchJob where Status = 'RUNNING' AND RunningTasks > 0
                                              AND StartDate <= getdate() - @Timeout)
    commit
end
