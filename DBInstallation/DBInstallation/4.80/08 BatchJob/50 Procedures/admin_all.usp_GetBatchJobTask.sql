set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetBatchJobTask
(
    @RunInd int = 0
)
as
begin
    set nocount on
    print @RunInd
    declare @RegVersion int = 0
    select @RegVersion = cast(value as int) from admin_all.REGISTRY_HASH where MAP_KEY = 'batch.job.version'
    if @RunInd < @RegVersion
        begin
            select * from admin_all.BatchJobTask where 1 = 0
            return
        end

    declare @TaskCount int, @BatchJobID int, @Renderer nvarchar(128), @SQL nvarchar(max)
    begin transaction

        begin try
            exec sp_getapplock 'BatchJob', 'Exclusive', 'Transaction', -1

            select top 1 @BatchJobID = b.BatchJobID, @TaskCount = jt.MaxAllowableTasks, @Renderer = jt.Renderer
            from admin_all.BatchJob b
                     inner loop join admin_all.BatchJobType jt on jt.BatchJobTypeID = b.Type
            where b.Status = 'RUNNING'
              and exists(select * from admin_all.BatchJobTask t where t.Status = 'PENDING' and b.BatchJobID = t.BatchJobID)
            order by b.RunningTasks/isnull(nullif(sum(cast(b.RunningTasks as float))  over(), 0), 1) - b.Priority/sum(cast(11- b.Priority as float )) over()
            if @@rowcount = 0
                begin
                    select * from admin_all.BatchJobTask where 1 = 0
                    goto ___Exit___;
                end
            select top (@TaskCount) BatchJobTaskID into #1
            from admin_all.BatchJobTask
            where BatchJobID = @BatchJobID
              and Status = 'PENDING'
            order by BatchJobTaskID

            update t
            set t.Status = 'RUNNING'
            from #1 s
                     inner loop join admin_all.BatchJobTask t on t.BatchJobTaskID = s.BatchJobTaskID

            select @SQL = 'select t.BatchJobTaskID, b.BatchJobID, B.StaffID,a.*
from #1 s
		inner loop join admin_all.BatchJobTask t on t.BatchJobTaskID = s.BatchJobTaskID
		inner loop join admin_all.BatchJob b on t.BatchJobID = b.BatchJobID
	cross apply ' + @Renderer + '(t.Content) a'
            exec(@SQL)
        end try
        begin catch
            rollback;
            throw;
        end catch
        ___Exit___:

    commit
end
go
--select * from admin_all.BatchJobTask
--select * from admin_all.BatchJobType
--select *  from admin_all.BatchJob a where BatchJobID = 2
--select * from admin_all.BatchJobTask where BatchJobID = 2
--insert into admin_all.BatchJobTask(BatchJobID, Status, Content)
--	select 2, 'PENDING', cast(N'{"playerId":"654654","bonusProgramId":10485}' as nvarchar(max))
--begin transaction
--exec admin_all.usp_UpdateBatchJob @BatchJobID = 2, @BatchJobStatus = 'RUNNING'
--select * from admin_all.BatchJobTask where BatchJobID = 2
--exec admin_all.usp_GetBatchJobTask
--select * from admin_all.BatchJobTask where BatchJobID = 2
----exec admin_all.usp_GetBatchJobTask
----select * from admin_all.BatchJobTask where BatchJobID = 2
----select * from admin_all.BatchJob
----select * from admin_all.BatchJobTask
--rollback
