set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CreateBatchJob
(
	@Name nvarchar(128)= null, 
	@Type int, 
	@Description nvarchar(256)= null, 
	@StaffID int =  null,
	@StopBatchWhenError bit = 0,
	@BrandID int = null,
	@BatchJobID int = null output
)
as
begin
	set nocount, xact_abort on
	insert into admin_all.BatchJob(Type, Name, Description, StaffID, StopBatchWhenError, BrandID)
		values(@Type, @Name, @Description, @StaffID, @StopBatchWhenError, @BrandID)
	select @BatchJobID = scope_identity()
end
go
/*
delete admin_all.BatchJobTask
delete admin_all.BatchJob
select * from admin_all.Batchjobtype
declare @i int
exec admin_all.usp_CreateBatchJob @BatchJobID = @i output, @Type = 2, @Name = 'test'
select @i
select * from admin_all.BatchJob
*/
