SET IDENTITY_INSERT admin_all.BatchJobType ON

if not exists(select * from admin_all.BatchJobType where BatchJobTypeID = 1)
    BEGIN
        INSERT INTO admin_all.BatchJobType (BatchJobTypeID, Name, Description, ProductID, MaxAllowableTasks, Parser, Renderer, Presenter) VALUES (1, 'EXTERNAL_BONUS_KAMBI', 'EXTERNAL_BONUS_KAMBI', null, 1000, 'admin_all.fn_BatchJobTypeParserJson', 'admin_all.fn_BatchJobTypeRendererKambiExnternal', 'admin_all.fn_BatchJobTypeRendererKambiExnternal');
    END

if not exists(select * from admin_all.BatchJobType where BatchJobTypeID = 2)
    BEGIN
        INSERT INTO admin_all.BatchJobType (BatchJobTypeID, Name, Description, ProductID, MaxAllowableTasks, Parser, Renderer, Presenter) VALUES (2, 'FREEBET_BONUS_KAMBI', 'FREEBET_BONUS_KAMBI', null, 1000, 'admin_all.fn_BatchJobTypeParserJson', 'admin_all.fn_BatchJobTypeRendererKambiFreeBet', 'admin_all.fn_BatchJobTypeRendererKambiFreeBet');
    END

if not exists(select * from admin_all.BatchJobType where BatchJobTypeID = 3)
    BEGIN
        INSERT INTO admin_all.BatchJobType (BatchJobTypeID, Name, Description, ProductID, MaxAllowableTasks, Parser, Renderer, Presenter) VALUES (3, 'FREESPIN_EVERYMATRIX', 'FREESPIN_EVERYMATRIX', null, 1, 'admin_all.fn_BatchJobTypeParserJson', 'admin_all.fn_BatchJobTypeRendererEveryMatrixFreespin', 'admin_all.fn_BatchJobTypeRendererEveryMatrixFreespin');
    END

if not exists(select * from admin_all.BatchJobType where BatchJobTypeID = 4)
    BEGIN
        INSERT INTO admin_all.BatchJobType (BatchJobTypeID, Name, Description, ProductID, MaxAllowableTasks, Parser, Renderer, Presenter) VALUES (4, 'MANUAL_BONUS', 'MANUAL_BONUS', null, 1000, 'admin_all.fn_BatchJobTypeParserJson', 'admin_all.fn_BatchJobTypeRendererManualBonus', 'admin_all.fn_BatchJobTypeRendererManualBonus');
    END

SET IDENTITY_INSERT admin_all.BatchJobType OFF
