set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetBatchJobs
(
    @BatchJobId int = null,
    @BatchJobType varchar(100) = null,
    @BatchStatus  varchar(100) = null,
    @BrandIds varchar(100) = null,
    @StaffID varchar(100) = null
)
as
begin
    set nocount on
  declare @SQL nvarchar(max)

 SELECT @SQL = 'SELECT
        j.BatchJobID,
        j.Name,
        j.Description,
        j.BrandID,
        j.StaffID,
        j.Type,
        j.Status,
        j.TotalTasks,
        j.ProcessedTasks,
        j.FailedTasks,
        j.PendingTasks,
        j.RunningTasks,
        j.CreationDate,
        j.StartDate,
        j.EndDate,
        j.LastExecutionDate,
        b.BRANDNAME,
        s.LOGINNAME as StaffName,
        t.NAME as TypeName
    FROM
        admin_all.BatchJob j
    LEFT JOIN admin.CASINO_BRAND_DEF b ON b.BRANDID = j.BrandID
    LEFT JOIN admin_all.STAFF_AUTH_TBL s ON s.STAFFID = j.StaffID
    LEFT JOIN admin_all.BatchJobType t ON t.BatchJobTypeID = j.Type
    WHERE 1=1 '
     + case when @BatchJobId is not null then ' and j.BatchJobID ='+ str(@BatchJobId) else '' end
     + case when @BatchJobType is not null then ' and j.Type in (select value from maint.fn_SplitDelimitedString(''' + @BatchJobType + ''', '',''))' else '' end
     + case when @BrandIds is not null then ' and j.BrandID in (select value from maint.fn_SplitDelimitedString(''' + @BrandIds + ''', '',''))' else '' end
     + case when @BatchStatus is not null then ' and j.status in (select value from maint.fn_SplitDelimitedString(''' + @BatchStatus + ''', '',''))' else '' end
     + case when @StaffID is not null then ' and j.StaffID ='+str(@StaffID) else '' end

	exec sp_executesql @SQL
end
go
--exec usp_GetBatchJobs @BatchJobId = 1, @BatchJobType = '1,2', @BatchStatus = 'RUNNING,PENDING'
