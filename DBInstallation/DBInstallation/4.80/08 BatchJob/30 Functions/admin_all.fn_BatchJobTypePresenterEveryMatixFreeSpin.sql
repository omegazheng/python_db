set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypePresenterEveryMatixFreeSpin
(
	@Content nvarchar(max)
)
returns table
as
return
(
	
	select u.PARTYID,u.USERID, u.CURRENCY, u.BRANDID, u.EMAIL
	from external_mpt.USER_CONF u 
	where u.PARTYID = cast(json_value(@Content, '$.partyID') as int)

)
go
--select * from admin_all.fn_BatchJobTypePresenterEveryMatixFreeSpin(cast(cast(N'{
--"partyID":9000,
--"subPlatformName":"HABANERO",
--"expiryDate":"2019-11-11 02:44:08.637",
--"GameInfoId":"12zodiacs",
--"rounds":1,
--"coins":1,
--"coinVal":20,
--"betLevel":20,
--"denomination":0.01,
--"coinSize":1,
--"lineVal":1,
--"coinValue":1
--}' as nvarchar(max)) as nvarchar(max)))


--select * from openjson(N'{"playerIds":["100062694", "100062695"],"rewardTemplateId":6595}', '$.playerIds')