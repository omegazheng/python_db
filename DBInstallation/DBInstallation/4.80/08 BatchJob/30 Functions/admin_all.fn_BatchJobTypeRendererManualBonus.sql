set ansi_nulls, quoted_identifier on
GO
create or alter function admin_all.fn_BatchJobTypeRendererManualBonus
(
    @Content nvarchar(max)
)
    returns table
        as
        return
            (
                with x1 as
                         (
                             select
                                 cast(json_value(@Content, '$.brandId')  as int) as BrandID,
                                 cast(json_value(@Content, '$.userId')  as  nvarchar(100)) as UserID,
                                 cast(json_value(@Content, '$.partyId')  as  integer) as PartyID,
                                 cast(json_value(@Content, '$.bonusAmount')  as numeric(38,18)) as BonusAmount,
                                 cast(json_value(@Content, '$.bonusGameType')  as nvarchar(100)) as BonusGameType,
                                 cast(json_value(@Content, '$.wagerRequirement')  as numeric(38,18)) as WagerRequirement,
                                 cast(json_value(@Content, '$.expiryDate')  as nvarchar(100)) as ExpiryDate,
                                 cast(json_value(@Content, '$.comment')  as nvarchar(100)) as Comment

                         )
                select x1.BrandID,x1.BonusAmount, x1.BonusGameType, x1.WagerRequirement,x1.ExpiryDate,x1.Comment,
                       u.USERID, u.PARTYID, u.CURRENCY, u.EMAIL
                from x1
                         left join external_mpt.USER_CONF u on u.PartyID = x1.PartyID 
            )
go

--select * from admin_all.fn_BatchJobTypeRendererManualBonus ('{"brandId":"1","partyId":"9999", "userId": "omegazheng", "bonusGameType":"CASINO","bonusAmount":"10","wagerRequirement":"100","expiryDate":"2020-12-31","comment":"Comment1"}'
