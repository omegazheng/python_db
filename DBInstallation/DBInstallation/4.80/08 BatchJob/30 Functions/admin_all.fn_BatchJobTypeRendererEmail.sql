set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypeRendererEmail
(
	@Content nvarchar(max)
)
returns table
as
return
(
	select 
		cast(json_value(value, '$.ProfieName')  as nvarchar(128)) as ProfieName,
		cast(json_value(value, '$.Recipients')  as nvarchar(max)) as Recipients,
		cast(json_value(value, '$.CopyRecipients')  as nvarchar(max)) as CopyRecipients,
		cast(json_value(value, '$.BlindCopyRecipients')  as nvarchar(max)) as BlindCopyRecipients,
		cast(json_value(value, '$.Subject')  as nvarchar(max)) as Subject,
		cast(json_value(value, '$.Body')  as nvarchar(max)) as Body,
		cast(json_value(value, '$.BodyFormat')  as nvarchar(20)) as BodyFormat, --Text HTML
		cast(json_value(value, '$.Importance')  as nvarchar(6)) as Importance, --'NORMAL','HIGH, LOW'
		cast(json_value(value, '$.Sesitivity')  as nvarchar(12)) as Sesitivity, --NORMAL, Personal, Private, Confidential
		cast(json_value(value, '$.ReplyTo')  as nvarchar(max)) as ReplyTo,
		cast(json_value(value, '$.FromAddress')  as nvarchar(max)) as FromAddress
		
	from openjson(@Content) p
)
go
--select *
--from admin_all.fn_BatchJobTypeRendererEmail('[{"ProfieName":"a","Recipients":"b","CopyRecipients":"copy","BlindCopyRecipients":"bcc","Subject":"test","Body":"body","BodyFormat":"TEXT","Imprtance":"NORMAL","Sesitivity":"NORMAL","ReplyTo":"reply","FromAddress":"dfdafasd"}]')