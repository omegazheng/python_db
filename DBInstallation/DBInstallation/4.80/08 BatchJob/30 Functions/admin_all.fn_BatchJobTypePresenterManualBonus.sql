set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypePresenterManualBonus
(
	@Content nvarchar(max)
)
returns table
as
return
(
	
	select u.PARTYID,u.USERID, u.CURRENCY, u.BRANDID, u.EMAIL
	from external_mpt.USER_CONF u 
	where u.USERID = cast(json_value(@Content, '$.userId') as varchar(100))

)
go
--select * from admin_all.fn_BatchJobTypePresenterManualBonus(cast(cast(N'{"userId":"huangshiwei37","bonusGameType":"SPORTSBOOK","bonusAmount":"10.00","wagerRequirement":"100.00","expiryDate":"2020-12-31","comment":"comment2"}]}' as nvarchar(max)) as nvarchar(max)))


--select * from openjson(N'{"playerIds":["100062694", "100062695"],"rewardTemplateId":6595}', '$.playerIds')