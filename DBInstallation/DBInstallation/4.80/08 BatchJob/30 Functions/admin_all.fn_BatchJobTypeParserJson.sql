set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypeParserJson
(
	@Content nvarchar(max)
)
returns table
as
return
(
	select cast(value as nvarchar(max)) Content
	from openjson(@Content) a

)
go
--select cast(Content as nvarchar(max)) from admin_all.fn_BatchJobTypeParserJson(cast(N'[{"playerId":"1005145","bonusProgramId":10485}, {"playerId":"1005146","bonusProgramId":"10486"}]' as nvarchar(max)))

