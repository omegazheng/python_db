set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_BatchJobTypePresenterKambiExternal
(
	@Content nvarchar(max)
)
returns table
as
return
(
	with x0 as
	(
		select @Content as Content
	),
	x1 as
	(
		select	cast(p.value as int) as PartyID, 
				cast(json_value(x0.Content, '$.bonusProgramId')  as int) as BonusProgramID
		from x0	
			cross apply openjson(x0.Content, '$.playerIds') p
	)
	select x1.PartyID, x1.BonusProgramID, u.USERID, u.CURRENCY, u.BRANDID, u.EMAIL
	from x1
		left join external_mpt.USER_CONF u on u.PARTYID = x1.PartyID
		


)
go
--select * from admin_all.fn_BatchJobTypePresenterKambiExternal(cast(cast(N'{"playerIds":["100062694", "100062695"],"bonusProgramId":10485}' as nvarchar(max)) as nvarchar(max)))

----select * from openjson(N'{"playerIds":["1005145","1005146"],"bonusProgramId":10485}', '$.playerIds')
--select * from admin_all.BONUS_PLAN

----select * from openjson(N'{"playerIds":["100062694", "100062695"],"rewardTemplateId":6595}', '$.playerIds')

--select object_name(object_id),* from sys.columns where name like '%wager%'