set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_admin_all_BatchJobTask on admin_all.BatchJobTask
for insert, delete, update
as
begin
	if @@rowcount = 0
		return
	set nocount on

	;with new as
	(
		select BatchJobID, EndDate, COMPLETED, PENDING, FAILED, RUNNING
		from (select Status, BatchJobID, max(EndDate) EndDate, count(*) as Count from inserted group by Status, BatchJobID) as s
		pivot
		(
			sum(Count) for Status in (COMPLETED, PENDING, FAILED, RUNNING)
		) as pvt
	),
	old as
	(
		select BatchJobID, COMPLETED, PENDING, FAILED, RUNNING
		from (select Status, BatchJobID from deleted) as s
		pivot
		(
			count(Status) for Status in (COMPLETED, PENDING, FAILED, RUNNING)
		) as pvt
	),
	d as
	(
		select 
				isnull(new.BatchJobID, old.BatchJobID) BatchJobID,
				new.EndDate,
				isnull(new.COMPLETED, 0) - isnull(old.COMPLETED, 0) as COMPLETED,
				isnull(new.PENDING, 0) - isnull(old.PENDING, 0) as PENDING,
				isnull(new.FAILED, 0) - isnull(old.FAILED, 0) as FAILED,
				isnull(new.RUNNING, 0) - isnull(old.RUNNING, 0) as RUNNING
		from new
			full outer join old on new.BatchJobID = old.BatchJobID
	),
	Delta as
	(
		select	BatchJobID, EndDate, COMPLETED + PENDING + FAILED + RUNNING TotalTasks,
				COMPLETED ProcessedTasks,
				PENDING PendingTasks,
				RUNNING RunningTasks,
				FAILED FailedTasks
		from d
		where COMPLETED <> 0 or PENDING <>0 or FAILED <>0 or RUNNING <>0
	)
	update b
		set b.TotalTasks = b.TotalTasks + Delta.TotalTasks,
			b.ProcessedTasks = b.ProcessedTasks + Delta.ProcessedTasks,
			b.PendingTasks = b.PendingTasks + Delta.PendingTasks,
			b.RunningTasks = b.RunningTasks + Delta.RunningTasks,
			b.FailedTasks = b.FailedTasks + Delta.FailedTasks,
			b.LastExecutionDate = isnull(Delta.EndDate, b.LastExecutionDate)
	from admin_all.BatchJob b
		inner join Delta on b.BatchJobID = Delta.BatchJobID

	; with x0 as
	(
		select 
				i.BatchJobTaskID,
				case 
					when d.Status <> 'RUNNING' and i.Status = 'RUNNING' then 'Starting' 
					when d.Status <> 'COMPLETED' and i.Status in ('COMPLETED', 'FAILED') then 'Completing'
					when d.Status <> 'PENDING' and i.Status = 'PENDING' then 'Reseting'
				end Status
		from inserted i
			inner join deleted d on i.BatchJobTaskID = d.BatchJobTaskID
	)
	update t
		set t.StartDate =	case 
								when x0.Status in ('Starting') then sysdatetime()
								when x0.Status in ('Reseting') then null
								else t.StartDate
							end,
			t.EndDate =		case 
								when x0.Status in ('Starting', 'Reseting') then null
								when x0.Status in ('Completing') then sysdatetime()
								else t.EndDate
							end
	from admin_all.BatchJobTask t
		inner join x0 on x0.BatchJobTaskID  = t.BatchJobTaskID
end
go
/*
begin transaction
declare @p varbinary(max) = cast(N'[{"CurrencyFrom":"CAD","CurrencyTo":"USD","Rate":0.75},{"CurrencyFrom":"USD","CurrencyTo":"CAD","Rate":1.33}]' as varbinary(max))
exec admin_all.usp_AddBatchJobTask  2, @p
select * from admin_all.BatchJobTask
select * from admin_all.BatchJob
rollback
*/