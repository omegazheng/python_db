set ansi_nulls, quoted_identifier on
go
---select * from admin_all.BatchJobTask
--drop table admin_all.BatchJobTask
go
if object_id('admin_all.BatchJobTask') is null
begin
	create table admin_all.BatchJobTask
	(
		BatchJobTaskID bigint identity(1,1) not null,
		BatchJobID int not null,
		Status nvarchar(30) not null constraint DF_admin_all_BatchJobTask_Status default('PENDING'),

		Content nvarchar(max) not null,
		ContentHash as cast(hashbytes('MD5', cast(Content as varbinary(max))) as uniqueidentifier),

		StartDate datetime2(7) null,
		EndDate datetime2(7) null,
		Info nvarchar(max) null,
		
		
		constraint PK_admin_all_BatchJobTask primary key (BatchJobTaskID),
		constraint FK_admin_all_BatchJobTask_BatchJob_batchJobID foreign key(batchJobID) references admin_all.BatchJob(BatchJobID),
		index IX_admin_all_BatchJobTask_BatchJobID_ContentHash unique (BatchJobID, ContentHash)with (ignore_dup_key = on),
		index IX_admin_all_BatchJobTask_Status (Status), 

	)
end
go
set xact_abort on
begin transaction
if exists(select * from sys.columns where name  = 'Content' and object_id = object_id('admin_all.BatchJobTask') and type_name(user_type_id) = 'varbinary')
begin
	if exists(select * from sys.indexes where name ='IX_admin_all_BatchJobTask_BatchJobID' and object_id = object_id('admin_all.BatchJobTask'))
		drop index admin_all.BatchJobTask.IX_admin_all_BatchJobTask_BatchJobID;
	if exists(select * from sys.indexes where name ='IDX_admin_all_BatchJobTask_BatchJobID_ContentHash' and object_id = object_id('admin_all.BatchJobTask'))
		drop index admin_all.BatchJobTask.IDX_admin_all_BatchJobTask_BatchJobID_ContentHash;
	alter table admin_all.BatchJobTask drop column ContentHash 
	alter table admin_all.BatchJobTask alter column Content nvarchar(max) not null
	alter table  admin_all.BatchJobTask add ContentHash as cast(hashbytes('MD5', cast(Content as varbinary(max))) as uniqueidentifier)
	create unique index IX_admin_all_BatchJobTask_BatchJobID_ContentHash on admin_all.BatchJobTask (BatchJobID, ContentHash) with (ignore_dup_key = on)
end
commit


--exec sp_helpindex 'admin_all.BatchJobTask'