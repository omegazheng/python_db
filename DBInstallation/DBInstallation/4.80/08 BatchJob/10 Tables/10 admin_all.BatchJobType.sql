set ansi_nulls, quoted_identifier on
go
---drop table admin_all.BatchJobType
go
if object_id('admin_all.BatchJobType') is null
begin
	create table admin_all.BatchJobType
	(
		BatchJobTypeID int not null identity(1,1),
		Name nvarchar(128) null,
		Description nvarchar(max),
		ProductID int,
		MaxAllowableTasks int not null constraint DF_admin_all_BatchJobType_MaxAllowableTasks default(1),
		
		Parser nvarchar(128) not null,
		Renderer nvarchar(128) not null,
		Presenter nvarchar(128) not null,
		constraint PK_admin_all_BatchJobType primary key (BatchJobTypeID),
		constraint [CK_admin_all_BatchJobType: Could not find Parser function] check (object_id(Parser) is not null),
		constraint [CK_admin_all_BatchJobType: Could not find Renderer function] check (object_id(Renderer) is not null),
		constraint [CK_admin_all_BatchJobType: Could not find Presenter function]check (object_id(Presenter) is not null),
		index IDX_admin_all_BatchJobType_Name unique (Name)
		
	)
end
go
--insert into admin_all.BatchJobType values('test', null, null, 13, 'admin_all.fn_BatchJobTypeParserTest', 'admin_all.fn_BatchJobTypeRendererTest', 'admin_all.fn_BatchJobTypePresenterTest')
-- Fix typo
set xact_abort on
if exists(select * from sys.columns where object_id= object_id('admin_all.BatchJobType') and name = 'Presentor')
begin
	begin transaction
	alter table admin_all.BatchJobType drop constraint [CK_admin_all_BatchJobType: Could not find Presentor function];
	exec sp_rename 'admin_all.BatchJobType.Presentor', 'Presenter', 'column';
	exec('alter table admin_all.BatchJobType  add constraint [CK_admin_all_BatchJobType: Could not find Presenter function]check (object_id(Presenter) is not null);')
	commit
end


go
--select object_id('admin_all.fn_BatchJobTypePresenterJson')