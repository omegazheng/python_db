set ansi_nulls, quoted_identifier on
--drop table audit.ClientInfo
go
if object_id('audit.ClientInfo') is null
begin
	create table audit.ClientInfo
	(
		ClientInfoID uniqueidentifier not null,
		ApplicationName nvarchar(128) not null,
		WorkStationName nvarchar(128) not null,
		ClientAddress nvarchar(128) not null,
		Transport nvarchar(128) not null,
		Protocol nvarchar(128) not null,
		LoginName nvarchar(128)  not null,
		CreationDate datetime2(7) not null constraint DF_audit_ClientInfo_CreationDate default(sysdatetime())
		constraint PK_audit_ClientInfo primary key(ClientInfoID) with(ignore_dup_key = on)
	)
end

