SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if not exists(select *
              from ADMIN_ALL.REGISTRY_HASH
              where MAP_KEY = 'evo.game.details.url')
    begin
        INSERT INTO ADMIN_ALL.REGISTRY_HASH (MAP_KEY, VALUE) VALUES ('evo.game.details.url', 'TBD');
    end

go
