if object_id('[admin_all].[Payment_PreDeploy]') is not null
begin
	if exists(
				select p1.rows, p2.rows
				from sys.partitions p1
					cross join sys.partitions p2
				where p1.object_id = object_id('admin_all.PAYMENT') and p1.index_id in(0,1)
					and p2.object_id = object_id('admin_all.PAYMENT_Predeploy') and p2.index_id in(0,1)
					and p1.rows <> p2.rows
			)
		and not exists(select * from sys.columns where object_id = object_id('admin_all.PAYMENT') and name = 'RowVersion')
	begin
		raiserror('Count of record in Payment and Payment_Predeploy does not match.', 16, 1)
		return
	end
	drop table [admin_all].[Payment_PreDeploy]
end

if object_id('admin_all.Payment_PreDeploy_Interface') is not null
begin
	drop view admin_all.Payment_PreDeploy_Interface
end

update maint.SynchronizationConfiguration set IsActive = 0 where name = 'PreDeployment-admin_all.PAYMENT'