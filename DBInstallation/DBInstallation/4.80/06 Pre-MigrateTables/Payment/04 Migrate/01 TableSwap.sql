if not exists(select * from sys.columns where object_id = object_id('admin_all.PAYMENT') and name = 'RowVersion')
begin
	exec maint.usp_SynchronizationEnableChangeTrackingOnDB
	begin transaction
	begin try
		-- lock table
		declare @ID int
		select top 1 @ID = id from admin_all.PAYMENT with(tablockx)
		--ensure records are replicated over
		exec maint.usp_SynchronizeOne 'PreDeployment-admin_all.PAYMENT' 
		---remove dependencies. This indexed view will be updated to use aggregate table.
		exec('create or alter view [admin_all].[v_PaymentLiveToDate] 
		as
			select	
				ACCOUNT_ID As AccountID,
				TYPE as Type, 
				METHOD as Method,
				STATUS as Status,
				sum(AMOUNT) Amount,
				sum(AMOUNT_REAL) AmountReal, 
				sum(AMOUNT_RELEASED_BONUS) AmountReleasedBonus, 
				sum(FEE) Fee,
				Count_Big(*) Count
			from admin_all.PAYMENT
			group by ACCOUNT_ID, TYPE, METHOD, STATUS')
		--swap table, foreign keys will be created, this might take some time.
		exec maint.usp_SwapTable 'admin_all.payment', 'admin_all.payment_predeploy'
	end try
	begin catch
		rollback;
		throw;
	end catch
	commit
end
go

