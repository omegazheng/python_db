if not exists(select * from sys.columns where object_id = object_id('admin_all.PAYMENT') and name = 'RowVersion')
begin
	if object_id('admin_all.PAYMENT_Predeploy') is not null
	begin
		exec maint.usp_SynchronizationEnableChangeTrackingOnDB
		if exists(
					select p1.rows, p2.rows
					from sys.partitions p1
						cross join sys.partitions p2
					where p1.object_id = object_id('admin_all.PAYMENT') and p1.index_id in(0,1)
						and p2.object_id = object_id('admin_all.PAYMENT_Predeploy') and p2.index_id in(0,1)
						and p1.rows > 0
						and p2.rows = 0
					)
		begin
			if exists(
						select *
						from sys.partitions p1
						where p1.object_id = object_id('admin_all.PAYMENT') 
							and p1.rows > 100000
					)
			begin
				exec maint.usp_CreatePreDeploymentJob
			end
			else
			begin
				exec maint.usp_SynchronizeOne 'PreDeployment-admin_all.PAYMENT'
			end
		end
	end
end
