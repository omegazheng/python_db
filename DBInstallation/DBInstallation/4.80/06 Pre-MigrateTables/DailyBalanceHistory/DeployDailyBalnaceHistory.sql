set ansi_nulls, quoted_identifier on
go
if not exists(select * from sys.indexes where object_id = object_id('admin_all.DAILY_BALANCE_HISTORY') and data_space_id = 1)
	return
begin transaction
begin try
	if object_id('admin_all.DAILY_BALANCE_HISTORY_Deploy') is not null 
		drop table admin_all.DAILY_BALANCE_HISTORY_Deploy
	exec sp_rename 'admin_all.DAILY_BALANCE_HISTORY', 'DAILY_BALANCE_HISTORY_Deploy', 'object'
	exec maint.usp_ForceNamingConvention 'admin_all.DAILY_BALANCE_HISTORY_Deploy'
	
	exec('	
create table admin_all.DAILY_BALANCE_HISTORY
(
	ID bigint identity(1,1),
	ACCOUNT_ID int not null,
	BALANCE_REAL numeric(38, 18) not null,
	DATETIME datetime not null,
	RELEASED_BONUS numeric(38, 18) not null constraint DF_admin_all_DAILY_BALANCE_HISTORY_RELEASED_BONUS  default (0),
	PLAYABLE_BONUS numeric(38, 18) not null constraint DF_admin_all_DAILY_BALANCE_HISTORY_PLAYABLE_BONUS  default (0),
	constraint PK_admin_all_DAILY_BALANCE_HISTORY primary key(DATETIME, ACCOUNT_ID),
	index IDX_admin_all_DAILY_BALANCE_HISTORY_ID unique(ID)
)'
	)




	
	exec('create or alter view [admin_all].[DAILY_BALANCE_HISTORY_Interface] as select cast([ACCOUNT_ID] as int) as [ACCOUNT_ID],cast([BALANCE_REAL] as numeric(38,18)) as [BALANCE_REAL],cast([DATETIME] as datetime) as [DATETIME],cast([RELEASED_BONUS] as numeric(38,18)) as [RELEASED_BONUS],cast([PLAYABLE_BONUS] as numeric(38,18)) as [PLAYABLE_BONUS],cast(0 as bit) as ___IsDeleted___ from [admin_all].[DAILY_BALANCE_HISTORY]')

	exec('create or alter trigger [admin_all].[TRI_DAILY_BALANCE_HISTORY_Interface] on [admin_all].[DAILY_BALANCE_HISTORY_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	merge [admin_all].[DAILY_BALANCE_HISTORY] t
	using inserted s on s.[DATETIME] = t.[DATETIME] and s.[ACCOUNT_ID] = t.[ACCOUNT_ID]
	when not matched and isnull(s.___IsDeleted___, 0) = 0 then
		insert ([ACCOUNT_ID],[BALANCE_REAL],[DATETIME],[RELEASED_BONUS],[PLAYABLE_BONUS])
			values (s.[ACCOUNT_ID],s.[BALANCE_REAL],s.[DATETIME],s.[RELEASED_BONUS],s.[PLAYABLE_BONUS])
	when matched and isnull(s.___IsDeleted___, 0) = 0 and ( s.[BALANCE_REAL] is null and t.[BALANCE_REAL] is not null or s.[BALANCE_REAL] is not null and t.[BALANCE_REAL] is null or s.[BALANCE_REAL] <> t.[BALANCE_REAL] or s.[RELEASED_BONUS] is null and t.[RELEASED_BONUS] is not null or s.[RELEASED_BONUS] is not null and t.[RELEASED_BONUS] is null or s.[RELEASED_BONUS] <> t.[RELEASED_BONUS] or s.[PLAYABLE_BONUS] is null and t.[PLAYABLE_BONUS] is not null or s.[PLAYABLE_BONUS] is not null and t.[PLAYABLE_BONUS] is null or s.[PLAYABLE_BONUS] <> t.[PLAYABLE_BONUS]) then
		update set t.[BALANCE_REAL] = s.[BALANCE_REAL],t.[RELEASED_BONUS] = s.[RELEASED_BONUS],t.[PLAYABLE_BONUS] = s.[PLAYABLE_BONUS]
	when matched and isnull(s.___IsDeleted___, 0) = 1 then
		delete
	;
end')
	exec maint.usp_CreateSynchronizationConfiguration
			@Name ='Deployment-admin_all.DAILY_BALANCE_HISTORY', 
			@InitializationQuery = 'select [ACCOUNT_ID], [BALANCE_REAL], [DATETIME], [RELEASED_BONUS], [PLAYABLE_BONUS], cast(0 as bit) ___IsDeleted___ from admin_all.DAILY_BALANCE_HISTORY_Deploy', 
			@FilterProcedure = 'maint.usp_SynchronizationFilterSnapshot', 
			@FilteredQuery = 'select [ACCOUNT_ID], [BALANCE_REAL], [DATETIME], [RELEASED_BONUS], [PLAYABLE_BONUS], cast(0 as bit) ___IsDeleted___ from admin_all.DAILY_BALANCE_HISTORY_Deploy', 
			@SourceTable = null, 
			@FilterField = null, 
			@TargetObject = 'admin_all.DAILY_BALANCE_HISTORY_Interface', 
			@TargetObjectFieldList = '[ACCOUNT_ID], [BALANCE_REAL], [DATETIME], [RELEASED_BONUS], [PLAYABLE_BONUS], [___IsDeleted___]', 
			@BatchSize = 1500, 
			@IsActive = 1,
			@ClearProgress = 1;
	exec('create or alter procedure maint.usp_DeployDailyBalanceHistory202003
as
begin
	set nocount on
	exec maint.usp_SynchronizeOne ''Deployment-admin_all.DAILY_BALANCE_HISTORY''
	declare @Rows bigint
	select @Rows = TotalRows
	from maint.SynchronizationProgress p
		inner join maint.SynchronizationConfiguration c on c.ConfigurationID = p.ConfigurationID
	where c.Name = ''Deployment-admin_all.DAILY_BALANCE_HISTORY''
	if exists(
			select * 
			from sys.partitions 
			where object_id = object_id(''admin_all.DAILY_BALANCE_HISTORY_Deploy'')
				and rows = @Rows
			)
	begin
		exec maint.usp_SendAlert ''DAILY_BALANCE_HISTORY migration is done sucessfully''
		drop table admin_all.DAILY_BALANCE_HISTORY_Deploy
		drop view admin_all.DAILY_BALANCE_HISTORY_Interface
		drop procedure maint.usp_DeployDailyBalanceHistory202003
		
	end
	else
	begin
		exec maint.usp_SendAlert  ''DAILY_BALANCE_HISTORY migration is FAILED'', ''DAILY_BALANCE_HISTORY migration is FAILED. Please ensure all the records in admin_all.DAILY_BALANCE_HISTORY_Deploy is copied over to admin_all.DAILY_BALANCE_HISTORY and clean up the objects
drop table admin_all.DAILY_BALANCE_HISTORY_Deploy
drop view admin_all.DAILY_BALANCE_HISTORY_Interface
drop procedure maint.usp_DeployDailyBalanceHistory202003
''
	end
end')

	exec maint.usp_CreateServiceJob @Name = 'Migrate Daily Balance Hisotry', @ProcedureName = 'maint.usp_DeployDailyBalanceHistory202003', @Description = 'Migrate Daily Balance Hisotry', @DeleteAfterRun = 3

end try
begin catch
	rollback;
	throw;
end catch
commit

