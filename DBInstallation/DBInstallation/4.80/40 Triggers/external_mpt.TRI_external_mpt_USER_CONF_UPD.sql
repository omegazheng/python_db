set ansi_nulls, quoted_identifier on
go

create or alter trigger [external_mpt].[TRI_external_mpt_USER_CONF_UPD] on [external_mpt].[USER_CONF] 
for update as 
begin 
	if @@rowcount = 0 return;
	set nocount on;
--	if UPDATE(BrandID) or UPDATE(Currency) 
	if exists(
					select *
					from inserted i 
						inner join deleted  d on i.PARTYID = d.PARTYID
					where isnull(i.BrandID, -1) <> isnull(d.BrandID, -1)
						or isnull(i.Currency, '') <> isnull(d.Currency, '')
				)
	begin
		raiserror ('Currency should not be changed!', 16, 1)
	    rollback
		return
	end
	
	if exists(
					select *
					from inserted i 
						inner join deleted  d on i.PARTYID = d.PARTYID
					where isnull(i.ParentID, -1) <> isnull(d.ParentID, -1)
				)
	begin
		declare @HIDs table(HID HierarchyId)
		;with map as
		(
			select d.Hierarchy.GetAncestor(1) OldParent, 
				case when i.ParentID is null  then hierarchyid::GetRoot () else p.Hierarchy end NewParent, 
				i.PartyID,
				d.Hierarchy OldHierarchy
			from inserted i 
				left outer join external_mpt.USER_CONF p on p.PartyID = i.ParentID
				inner join deleted d on d.PartyID = i.PartyID
			where isnull(i.ParentID, -1) <> isnull(d.ParentID, -1)
		)
		update u 
			set u.Hierarchy = u.Hierarchy.GetReparentedValue(map.OldParent, map.NewParent)
			output inserted.Hierarchy into @HIDs
		from external_mpt.USER_CONF u
			inner join map on u.Hierarchy.IsDescendantOf(map.OldHierarchy) = 1
		
		if exists(
					select* 
					from (select cast('<R>'+replace(HID.GetAncestor(1).ToString(),'/', '</R><R>')+'</R>' as xml) P from @HIDs)x0
					where exists(
											select *
											from  (
														select n.value('.', 'varchar(20)') v 
														from  x0.P.nodes('R') c(n)
													) x2
											where v <> ''
											group by v
											having count(*) > 1
									)
					)
			begin
					raiserror('Items are referenced multiple times in the hierarchy.', 16,1)
					rollback
					return
			end
	end
end
go
