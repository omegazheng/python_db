SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[CURRENCY]') = object_id and name = 'PRECISION')
  BEGIN
    begin transaction
	begin try
    ALTER TABLE [admin_all].[CURRENCY]
      ADD [PRECISION] int default 2 not null;
	exec('
			update admin_all.currency set precision =
			case
				when (number_decimal_point is not null) and (number_decimal_point > 2) then number_decimal_point
				else 2
			end
	')
	end try
	begin catch
		rollback;
		throw;
	end catch
    commit
  END
GO


