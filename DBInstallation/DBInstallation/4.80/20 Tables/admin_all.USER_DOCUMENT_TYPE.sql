SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[admin_all].[USER_DOCUMENT_TYPE]') AND type in (N'U'))
  BEGIN
    CREATE TABLE [admin_all].[USER_DOCUMENT_TYPE]
    (
      [ID]                      [int] IDENTITY (1,1)    NOT NULL,
      [TYPE]                    [NVARCHAR](50)          NOT NULL,
      [HAS_EXPIRY]              [bit]                   NOT NULL,
      [HAS_NUMBER]              [bit]                   NOT NULL,
      [INIT_KYC_SCORE]          [int]                   NOT NULL,
      [REVIEWED_KYC_SCORE]      [int]                   NOT NULL,
    )
  END
GO
