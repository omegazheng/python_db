SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[GAME_INFO]') = object_id and name = 'BACKGROUND_IMAGE_URL')
    begin
        ALTER TABLE [admin_all].[GAME_INFO]
            ADD [BACKGROUND_IMAGE_URL] [varchar](1000) NULL
    end
go
