SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


delete from admin_all.STAFF_LIMIT where CURRENCY not in (select iso_code from admin_all.CURRENCY)
go
IF NOT EXISTS (
        SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'admin_all.FK_admin_all_STAFF_LIMIT_Currency_Currency')
                                         AND parent_object_id = OBJECT_ID(N'admin_all.STAFF_LIMIT ')
    )
    begin
        alter table admin_all.STAFF_LIMIT
            add constraint FK_admin_all_STAFF_LIMIT_Currency_Currency foreign key(CURRENCY) references admin_all.CURRENCY(iso_code)
    end
go
