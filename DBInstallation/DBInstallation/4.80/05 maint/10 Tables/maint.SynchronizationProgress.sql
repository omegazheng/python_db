set ansi_nulls, quoted_identifier on
--drop table maint.SynchronizationProgress
go
if object_id('maint.SynchronizationProgress') is null
begin
	create table maint.SynchronizationProgress
	(
		ConfigurationID int not null,
		FromValue sql_variant,
		ToValue sql_variant,
		FromValueAdjusted sql_variant,
		ToValueAdjusted sql_variant,

		SessionID int not null,

		BatchCount int null,
		RowsInBatch int null,
		TotalRows bigint null,
		
		BatchStartDate datetime2(7) null,
		BatchEndDate datetime2(7) null,

		StartDate datetime2(7) not null,
		EndDate datetime2(7) null,

		Error varchar(max),
		SynchronizationHistoryID bigint,
		ApplicationName varchar(128),
		LoginName varchar(128),
		constraint PK_maint_SynchronizationProgress primary key(ConfigurationID),
		constraint FK_maint_SynchronizationProgress_SynchronizationConfiguration_ConfigurationID foreign key(ConfigurationID)references maint.SynchronizationConfiguration (ConfigurationID)
	)
end
go


