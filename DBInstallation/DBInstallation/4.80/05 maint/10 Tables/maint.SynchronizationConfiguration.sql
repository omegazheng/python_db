set ansi_nulls, quoted_identifier on
--drop table maint.SynchronizationConfiguration
go
if object_id('maint.SynchronizationConfiguration') is null
begin
	create table maint.SynchronizationConfiguration
	(
		ConfigurationID int not null identity(1,1),
		Name varchar(128) not null,

		InitializationQuery nvarchar(max) null,
		FilterProcedure nvarchar(256) not null,
		FilteredQuery nvarchar(max) not null,
		
		SourceTable nvarchar(256) null,
		FilterField nvarchar(128) null,

		TargetObject nvarchar(256) not null,
		TargetObjectFieldList nvarchar(max) not null,
		
		BatchSize int not null constraint DF_maint_SynchronizationConfiguration_BatchSize default(1500),
		IsActive bit not null constraint DF_maint_SynchronizationConfiguration_IsActive default(1),
		constraint PK_maint_SynchronizationConfiguration primary key (ConfigurationID),
		constraint UQ_maint_SynchronizationConfiguration_Name unique (Name),
		constraint [CK_maint_SynchronizationConfiguration_Could not find FilterProcedure] check(object_id(FilterProcedure) is not null),
	)
end