set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_ShowMissingIndexOnForeignKey
as
begin
	set nocount on;
	;with x0 as
	(
		select	SchemaName, ObjectName, IndexName, IndexColumns
		from maint.fn_GetIndexStructure(null)
		where SchemaName not in ('sys')
	), 
	x1 as
	(
		select *
		from maint.fn_GetForeignKeyStructure(null, null)
		where SchemaName not in ('sys')
	)
	select SchemaName, ParentTableName, ParentColumns, ReferencedSchemaName, ReferencedTableName, ReferencedColumns, ObjectName,
'if not exists(select * from sys.indexes where object_id = object_id(' + quotename(quotename(SchemaName) + '.' + quotename(ParentTableName), '''') + ') and name = ' + quotename('IDX_'+SchemaName +'_' + ParentTableName + '_' + replace(ParentColumns, ',', '_'), '''') +')
	create index IDX_'+SchemaName +'_' + ParentTableName + '_' + replace(ParentColumns, ',', '_') + ' on ' + quotename(SchemaName) + '.' + quotename(ParentTableName) 
+ '('+ParentColumns+');
' ExpectedIndex
	from x1
	where not exists(
					select *
					from x0 
					where x1.SchemaName = x0.SchemaName and x1.ParentTableName = x0.ObjectName
						and x0.IndexColumns + ',' like x1.ParentColumns + ',%'
				)
end
go
--exec maint.usp_ShowMissingIndexOnForeignKey
