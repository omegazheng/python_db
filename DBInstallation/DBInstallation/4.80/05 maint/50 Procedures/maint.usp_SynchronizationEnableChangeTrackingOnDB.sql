set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_SynchronizationEnableChangeTrackingOnDB
(
	@ServerName nvarchar(128) = null,
	@DatabaseName nvarchar(128) = null
)
as
begin
	set nocount on
	declare @SQL nvarchar(max)
	select @SQL = 'declare @SQL nvarchar(max)
	if not exists(select * from sys.change_tracking_databases where database_id = db_id()) 
	begin
		select @SQL = ''alter database ''+quotename(db_name())+'' set change_tracking = on;''
		exec(@SQL)
	end
	'
	
	exec maint.usp_SynchronizationExecuteSQL @ServerName, @DatabaseName, @SQL
end