set ansi_nulls on
GO
SET QUOTED_IDENTIFIER ON
GO
create or alter procedure maint.usp_SynchronizeOne (@FriendlyName varchar(128), @RaiseError bit = 1)
as
begin
	set nocount on
	declare @Date datetime2(7) = sysdatetime()
	begin try
		declare @ConfigurationID int, @InitializationQuery nvarchar(max), @FilterProcedure nvarchar(256), @FilteredQuery nvarchar(max), 
				@SourceTable nvarchar(256), @FilterField nvarchar(128), @TargetObject nvarchar(256), @BatchSize int, @TargetObjectFieldList nvarchar(max),
				@Error nvarchar(max)

		select	@ConfigurationID = ConfigurationID, @InitializationQuery = InitializationQuery, @FilterProcedure = FilterProcedure, 
				@FilteredQuery = FilteredQuery, @SourceTable = SourceTable, @FilterField = FilterField, 
				@TargetObject = TargetObject, @BatchSize = BatchSize, @TargetObjectFieldList = TargetObjectFieldList
		from maint.SynchronizationConfiguration
		where Name = @FriendlyName
			and IsActive = 1
		if @@rowcount = 0
		begin
			print 'Cannot found synchronization configuration'
			return
		end

		exec maint.usp_UpdateSynchronizationProgress @ConfigurationID = @ConfigurationID, @StartDate = @Date

		if object_id(@FilterProcedure) is null
		begin
			raiserror('Filter procedure does not exists', 16, 1);
			return;
		end
		--print 1
		if @FilterProcedure like '%ChangeTracking%' and @SourceTable is not null
		begin
			exec maint.usp_SynchronizationEnableChangeTracking null, null, @SourceTable
		end
		declare @FromValue sql_variant, @ToValue sql_variant, @FromValueAdjusted sql_variant, @ToValueAdjusted sql_variant, @ShouldInitializate bit
		
		exec @FilterProcedure @ConfigurationID, @FromValue output,  @ToValue output, @FromValueAdjusted output, @ToValueAdjusted output, @ShouldInitializate output
		--select @FromValue ,  @ToValue , @FromValueAdjusted , @ToValueAdjusted , @ShouldInitializate 
		exec sp_set_session_context N'IsSynchronizationInitialization', 0
		if @ShouldInitializate = 1
		begin
			if @FilterProcedure like '%ChangeTracking%'
			begin
				exec sp_set_session_context N'IsSynchronizationInitialization', 1
				exec maint.usp_SynchronizationCopy @ConfigurationID = @ConfigurationID, @SourceQuery = @InitializationQuery,@TargetTableName = @TargetObject, @TargetTableFieldList = @TargetObjectFieldList, @BatchSize = @BatchSize, @FromValue = @FromValue, @FromValueAdjusted = @FromValueAdjusted, @ToValue = @ToValue, @ToValueAdjusted = @ToValueAdjusted, @ConvertValueToDateType = 'bigint'
			end
			else
			begin
				exec sp_set_session_context N'IsSynchronizationInitialization', 1
				exec maint.usp_SynchronizationCopy @ConfigurationID = @ConfigurationID, @SourceQuery = @InitializationQuery,@TargetTableName = @TargetObject, @TargetTableFieldList = @TargetObjectFieldList, @BatchSize = @BatchSize, @FromValue = @FromValue, @FromValueAdjusted = @FromValueAdjusted, @ToValue = @ToValue, @ToValueAdjusted = @ToValueAdjusted;
			end
		end
		else
		begin
			if @FilterProcedure like '%ChangeTracking%'
			begin
				exec sp_set_session_context N'IsSynchronizationInitialization', 0
				exec maint.usp_SynchronizationCopy @ConfigurationID = @ConfigurationID, @SourceQuery = @FilteredQuery,@TargetTableName = @TargetObject, @TargetTableFieldList = @TargetObjectFieldList, @BatchSize = @BatchSize, @FromValue = @FromValue, @FromValueAdjusted = @FromValueAdjusted, @ToValue = @ToValue, @ToValueAdjusted = @ToValueAdjusted, @ConvertValueToDateType = 'bigint'
			end
			else 
			begin
				exec sp_set_session_context N'IsSynchronizationInitialization', 0
				exec maint.usp_SynchronizationCopy @ConfigurationID = @ConfigurationID, @SourceQuery = @FilteredQuery,@TargetTableName = @TargetObject, @TargetTableFieldList = @TargetObjectFieldList, @BatchSize = @BatchSize, @FromValue = @FromValue, @FromValueAdjusted = @FromValueAdjusted, @ToValue = @ToValue, @ToValueAdjusted = @ToValueAdjusted
			end
		end
		select @Date = sysdatetime();
		exec maint.usp_UpdateSynchronizationProgress @ConfigurationID = @ConfigurationID, @FromValue= @FromValue, @ToValue = @ToValue, @FromValueAdjusted = @FromValueAdjusted, @ToValueAdjusted = @ToValueAdjusted, @EndDate = @Date
		exec sp_set_session_context N'IsSynchronizationInitialization', null
	end try
	begin catch
		exec sp_set_session_context N'IsSynchronizationInitialization', null
		select @Error = error_message(), @Date = sysdatetime();
		exec maint.usp_UpdateSynchronizationProgress @ConfigurationID = @ConfigurationID, @Error = @Error, @EndDate = @Date;
		if @RaiseError = 1
			throw;
	end catch

end

go
--exec maint.usp_SynchronizeOne  'Report-rpt.PaymentStaging'