set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_SynchronizationEnableChangeTracking
(
	@ServerName nvarchar(128),
	@DatabaseName nvarchar(128),
	@TableName nvarchar(256)
)
as
begin
	set nocount, xact_abort on
	declare @SQL nvarchar(max)

	select @SQL = ' declare @SQL nvarchar(max), @TableName nvarchar(200)= '+quotename(@TableName, '''')+'
	if not exists(select * from sys.change_tracking_tables where object_id = object_id(@TableName))
	begin
		select @SQL = '' set nocount on
if not exists(select * from sys.change_tracking_databases where database_id = db_id())
begin
	raiserror(''''change tracking is not enabled at database level'''', 16, 1)
	return
end
if exists(select * from sys.change_tracking_tables where object_id = object_id(@TableName))
	return

declare @SQL nvarchar(max)
select @SQL = ''''alter table ''''+quotename(schema_name(schema_id))+''''.''''+quotename(name)+'''' enable change_tracking''''
from sys.tables
where object_id = object_id(@TableName)
if @@rowcount = 0
begin
	raiserror(''''Count not find table %s'''', 16, 1, @TableName)
	return
end

exec(@SQL)''
		select @SQL = ''exec sp_executesql N'''''' + replace(@SQL  , '''''''', '''''''''''') + '''''', N''''@TableName sysname'''', N'''''' + replace(@TableName, '''''''', '''''''''''') + ''''''''
		exec(@SQL)
	end'

	exec maint.usp_SynchronizationExecuteSQL @ServerName, @DatabaseName, @SQL
end
go
--exec maint.usp_SynchronizationCheckChangeTracking null, null, 'admin_all.accountjjkjk'

