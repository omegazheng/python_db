set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_SynchronizationCopy
(
	@ConfigurationID int, 
	@SourceQuery nvarchar(max),
	@TargetTableName nvarchar(256),
	@TargetTableFieldList nvarchar(max) = null,
	@BatchSize int = 10000,
	@FromValue sql_variant = null,
	@FromValueAdjusted sql_variant = null,
	@ToValue sql_variant = null,
	@ToValueAdjusted sql_variant = null,
	@ConvertValueToDateType nvarchar(128) = 'sql_variant'
)
as
begin
	set nocount, xact_abort on
	declare @SQL nvarchar(max), @Def nvarchar(max) = N'@ConfigurationID int,@FromValue sql_variant,@FromValueAdjusted sql_variant,@ToValue sql_variant,@ToValueAdjusted sql_variant'

	select @SQL = '
declare @F ' + @ConvertValueToDateType + ' = cast(@FromValue as ' + @ConvertValueToDateType + '), @FA ' + @ConvertValueToDateType + ' = cast(@FromValueAdjusted as ' + @ConvertValueToDateType + '), @T ' + @ConvertValueToDateType + ' = cast(@ToValue as ' + @ConvertValueToDateType + '), @TA ' + @ConvertValueToDateType + ' = cast(@ToValueAdjusted as ' + @ConvertValueToDateType + ');
declare @handle int, @RowCount int, @TotalRows int = 0, @BatchStartDate datetime2(7) = sysdatetime(), @BatchEndDate datetime2(7), @BatchCount int = 0, @rc int;
insert into ' + @TargetTableName + isnull('('+@TargetTableFieldList+')', '') + '
	exec sp_cursoropen @cursor = @handle output,@stmt = '''+replace(@SourceQuery, '''', '''''')+ ''', @scrollopt = 4112, @ccopt = 8193,@rowcount = @rc output, @paramdef = N''@ConfigurationID int,@FromValue ' + @ConvertValueToDateType + ',@FromValueAdjusted ' + @ConvertValueToDateType + ',@ToValue ' + @ConvertValueToDateType + ',@ToValueAdjusted ' + @ConvertValueToDateType + ''', @ConfigurationID = @ConfigurationID, @FromValue = @F, @FromValueAdjusted = @FA, @ToValue = @T, @ToValueAdjusted = @TA;
while(1=1)
begin
	--waitfor delay ''00:00:01''
	select @BatchStartDate = sysdatetime()
	insert into ' + @TargetTableName + isnull('('+@TargetTableFieldList+')', '') + '
		exec sp_cursorfetch @handle, 2,  0, '+cast(@BatchSize as varchar(20))+';
	select @RowCount = isnull(isnull(nullif(@@rowcount, 0), cast(session_context (N''SynchronizationRowCount'') as int)), 0)
	exec sp_set_session_context N''SynchronizationRowCount'', null
	select @TotalRows = @TotalRows + @RowCount, @BatchCount = @BatchCount + 1, @BatchEndDate = sysdatetime()
	exec maint.usp_SynchronizationCallBack @ConfigurationID, @BatchCount, @RowCount, @TotalRows, @BatchStartDate, @BatchEndDate;
	if @RowCount < '+cast(@BatchSize as varchar(20))+'
		break;
end
exec sp_cursorclose @handle
'
	exec sp_executesql @SQL, @Def, @ConfigurationID, @FromValue, @FromValueAdjusted, @ToValue, @ToValueAdjusted

end 
go
--exec maint.usp_SynchronizeOne  'Report-rpt.PaymentStaging'
--exec maint.usp_SynchronizeOne 'PreDeployment-admin_all.PAYMENT'
--alter table admin_all.ACCOUNT_TRAN_TEST add ConfigurationID int,FromValue sql_variant,FromValueAdjusted sql_variant,ToValue sql_variant,ToValueAdjusted sql_variant

--truncate table admin_all.ACCOUNT_TRAN_TEST
--exec maint.usp_SynchronizationCopy @ConfigurationID = 1,
--					@SourceQuery = 'select top 190 [ID], [ACCOUNT_ID], [DATETIME], [TRAN_TYPE], [AMOUNT_REAL], [BALANCE_REAL], [PLATFORM_TRAN_ID], [GAME_TRAN_ID], [GAME_ID], [PLATFORM_ID], [payment_id], [ROLLED_BACK], [ROLLBACK_TRAN_ID], [AMOUNT_RELEASED_BONUS], [AMOUNT_PLAYABLE_BONUS], [BALANCE_RELEASED_BONUS], [BALANCE_PLAYABLE_BONUS], [AMOUNT_UNDERFLOW], [AMOUNT_RAW_LOYALTY], [BALANCE_RAW_LOYALTY], [TRANSACTION_ON_HOLD_ID], [SSW_TRAN_ID], [REFERENCE], [BRAND_ID], [MachineID], @ConfigurationID, @FromValue, @FromValueAdjusted, @ToValue, @ToValueAdjusted, 0 from admin_all.ACCOUNT_TRAN_ALL a order by ID asc', 
--					--@SourceQuery = 'select top 190 [ID], [ACCOUNT_ID], [DATETIME], [TRAN_TYPE], [AMOUNT_REAL], [BALANCE_REAL], [PLATFORM_TRAN_ID], [GAME_TRAN_ID], [GAME_ID], [PLATFORM_ID], [payment_id], [ROLLED_BACK], [ROLLBACK_TRAN_ID], [AMOUNT_RELEASED_BONUS], [AMOUNT_PLAYABLE_BONUS], [BALANCE_RELEASED_BONUS], [BALANCE_PLAYABLE_BONUS], [AMOUNT_UNDERFLOW], [AMOUNT_RAW_LOYALTY], [BALANCE_RAW_LOYALTY], [TRANSACTION_ON_HOLD_ID], [SSW_TRAN_ID], [REFERENCE], [BRAND_ID], [MachineID], 0 from admin_all.ACCOUNT_TRAN_ALL a where  @ConfigurationID = 1234 order by ID asc', 
--					@TargetTableName = 'admin_all.ACCOUNT_TRAN_TEST_Interface',
--					@TargetTableFieldList = '[ID], [ACCOUNT_ID], [DATETIME], [TRAN_TYPE], [AMOUNT_REAL], [BALANCE_REAL], [PLATFORM_TRAN_ID], [GAME_TRAN_ID], [GAME_ID], [PLATFORM_ID], [payment_id], [ROLLED_BACK], [ROLLBACK_TRAN_ID], [AMOUNT_RELEASED_BONUS], [AMOUNT_PLAYABLE_BONUS], [BALANCE_RELEASED_BONUS], [BALANCE_PLAYABLE_BONUS], [AMOUNT_UNDERFLOW], [AMOUNT_RAW_LOYALTY], [BALANCE_RAW_LOYALTY], [TRANSACTION_ON_HOLD_ID], [SSW_TRAN_ID], [REFERENCE], [BRAND_ID], [MachineID], ConfigurationID, FromValue, FromValueAdjusted, ToValue, ToValueAdjusted, ___IsDeleted___',
--					@BatchSize = 100,
--					@FromValue  = 2,
--					@FromValueAdjusted = 3,
--					@ToValue = 4,
--					@ToValueAdjusted = 5
--select * from admin_all.ACCOUNT_TRAN_TEST
			