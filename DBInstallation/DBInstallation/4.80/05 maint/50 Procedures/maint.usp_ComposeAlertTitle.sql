set ansi_nulls , quoted_identifier on
go
create or alter procedure maint.usp_ComposeAlertTitle @Title varchar(max) output
as
begin
	set nocount on
	declare @CustomerName nvarchar(100), @SystemName nvarchar(128)
	select @CustomerName = isnull((select top 1 rtrim(name) from dbo.CUSTOMER), 'Unknown')
	select @SystemName = coalesce(admin_all.fn_GetRegistry('system.name'), admin_all.fn_GetRegistry('system.name.key'), 'Unknown')
	select @Title = quotename(@CustomerName, '<') + '-' + quotename(@SystemName, '<')+ '-' + quotename(isnull(rtrim(@@servername), 'Unknown'), '>')	+ isnull(@Title, 'No Title')
end
go
 --declare @x varchar(max)='haha'
 --exec maint.usp_ComposeAlertTitle @x output
 --select @x
