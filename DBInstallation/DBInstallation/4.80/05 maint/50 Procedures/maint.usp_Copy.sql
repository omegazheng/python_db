set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_Copy
(
	@SourceQuery nvarchar(max),
	@TargetTableName nvarchar(256),
	@TargetTableFieldList nvarchar(max) = null,
	@IdentityInsert bit = 0,
	@BatchSize int = 10000,
	@InfoPassedToCallBack nvarchar(max) = null,
	@CallBack nvarchar(max) = null
)
as
begin
	set xact_abort, nocount on
	declare @SQL nvarchar(max)
	select @SQL = cast(case when @IdentityInsert =1 then  'set identity_insert ' + @TargetTableName + ' on; ' else '' end as nvarchar(max)) + '
declare @handle int, @RowCount int, @TotalRows int = 0, @BatchStartDate datetime2(7) = sysdatetime(), @BatchEndDate datetime2(7), @BatchCount int = 0;

'+case when @CallBack is not null then 'exec sp_executesql @CallBack, N''@InfoPassedToCallBack nvarchar(max), @BatchCount int, @RowsInBatch int, @TotalRows bigint, @BatchStartDate datetime2(7), @BatchEndDate datetime2(7)'', @InfoPassedToCallBack, 0, -1, -1, @BatchStartDate, null;' else '' end +'

insert into ' + @TargetTableName + isnull('('+@TargetTableFieldList+')', '') + '
	exec sp_cursoropen @cursor = @handle output,@stmt = '''+replace(@SourceQuery, '''', '''''')+''', @scrollopt = 4, @ccopt = 1;
while(1=1)
begin
	select @BatchStartDate = sysdatetime()
	insert into ' + @TargetTableName + isnull('('+@TargetTableFieldList+')', '') + '
		exec sp_cursorfetch @handle, 2,  0, '+cast(@BatchSize as varchar(20))+';
	select @RowCount = @@rowcount
	select @TotalRows = @TotalRows + @RowCount, @BatchCount = @BatchCount + 1, @BatchEndDate = sysdatetime()
	'+case when @CallBack is not null then 'exec sp_executesql @CallBack, N''@InfoPassedToCallBack nvarchar(max), @BatchCount int, @RowsInBatch int, @TotalRows bigint, @BatchStartDate datetime2(7), @BatchEndDate datetime2(7)'', @InfoPassedToCallBack, @BatchCount, @RowCount, @TotalRows, @BatchStartDate, @BatchEndDate;' else '' end +'
	if @RowCount < '+cast(@BatchSize as varchar(20))+'
		break;
end
exec sp_cursorclose @handle
'+case when @CallBack is not null then 'exec sp_executesql @CallBack, N''@InfoPassedToCallBack nvarchar(max), @BatchCount int, @RowsInBatch int, @TotalRows bigint, @BatchStartDate datetime2(7), @BatchEndDate datetime2(7)'', @InfoPassedToCallBack, @BatchCount, -2, @TotalRows, null, @BatchEndDate;' else '' end +'
'+case when @IdentityInsert =1 then  'set identity_insert ' + @TargetTableName + ' off;' else '' end
--print @sql
	exec sp_executesql @SQL, N'@InfoPassedToCallBack nvarchar(max), @CallBack nvarchar(max)', @InfoPassedToCallBack , @CallBack 
end
go

--truncate table admin_all.ACCOUNT_TRAN_TEST
--exec maint.usp_Copy 'select top 190 [ID], [ACCOUNT_ID], [DATETIME], [TRAN_TYPE], [AMOUNT_REAL], [BALANCE_REAL], [PLATFORM_TRAN_ID], [GAME_TRAN_ID], [GAME_ID], [PLATFORM_ID], [payment_id], [ROLLED_BACK], [ROLLBACK_TRAN_ID], [AMOUNT_RELEASED_BONUS], [AMOUNT_PLAYABLE_BONUS], [BALANCE_RELEASED_BONUS], [BALANCE_PLAYABLE_BONUS], [AMOUNT_UNDERFLOW], [AMOUNT_RAW_LOYALTY], [BALANCE_RAW_LOYALTY], [TRANSACTION_ON_HOLD_ID], [SSW_TRAN_ID], [REFERENCE], [BRAND_ID], [MachineID], 0 from admin_all.ACCOUNT_TRAN_ALL a order by ID asc', 
--					'admin_all.ACCOUNT_TRAN_TEST_Interface',
--					'[ID], [ACCOUNT_ID], [DATETIME], [TRAN_TYPE], [AMOUNT_REAL], [BALANCE_REAL], [PLATFORM_TRAN_ID], [GAME_TRAN_ID], [GAME_ID], [PLATFORM_ID], [payment_id], [ROLLED_BACK], [ROLLBACK_TRAN_ID], [AMOUNT_RELEASED_BONUS], [AMOUNT_PLAYABLE_BONUS], [BALANCE_RELEASED_BONUS], [BALANCE_PLAYABLE_BONUS], [AMOUNT_UNDERFLOW], [AMOUNT_RAW_LOYALTY], [BALANCE_RAW_LOYALTY], [TRANSACTION_ON_HOLD_ID], [SSW_TRAN_ID], [REFERENCE], [BRAND_ID], [MachineID], ___IsDeleted___',
--					0,
--					100,
--					'aaa',
--					'print @InfoPassedToCallBack
--					print @BatchCount
--					print @RowsInBatch
--					print @TotalRows
--					print @BatchStartDate
--					print @BatchEndDate
--					print ''--------'''
--select * from admin_all.ACCOUNT_TRAN_TEST
			