set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SynchronizationFilterChangeTracking
(
	@ConfigurationID int,
	@FromValue sql_variant output, 
	@ToValue sql_variant output,
	@FromValueAdjusted sql_variant output,
	@ToValueAdjusted sql_variant output,
	@ShouldInitializate bit output
)
as
begin
	set nocount on
	select @ShouldInitializate = 0

	select @FromValue = cast(0 as bigint)
	select @ToValue = cast(isnull(change_tracking_current_version(), 0) as bigint)

	select @FromValue = ToValue
	from maint.SynchronizationProgress
	where ConfigurationID = @ConfigurationID
		--and FromValue is not null 
		and ToValue is not null
	if @@rowcount = 0
	begin
		select @ShouldInitializate = 1
	end
	select @FromValueAdjusted = @FromValue, @ToValueAdjusted = @ToValue
	return
end

