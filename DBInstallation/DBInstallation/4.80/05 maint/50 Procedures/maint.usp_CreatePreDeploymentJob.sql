set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CreatePreDeploymentJob
as
begin
	set nocount on 
	if maint.fn_GetServiceJobID('PreDeployment') is null
		exec maint.usp_CreateServiceJob @Name = 'PreDeployment', @ProcedureName = 'maint.usp_PreDeployment', @Description = 'Job''s used for predeployment. It will block entire predeployment process until maint.usp_RemovePreDeploymentJob is called or the job is removed manually.'
end