set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CheckPreDeploymentJob @RaiseAlert bit = 1
as
begin
	set nocount on 
	create table #1(job_id uniqueidentifier, job_name sysname, run_status int, run_date int, run_time int, run_duration int, operator_emailed nvarchar(20), operator_netsent nvarchar(20), operator_paged nvarchar(20), retries_attempted int, server nvarchar(30))
	declare @JobID uniqueidentifier = maint.fn_GetServiceJobID('PreDeployment')
	
	set rowcount 1
	insert into #1
		exec msdb..sp_help_jobhistory @job_id = @JobID, @step_id = 0, @run_status = 1
	set rowcount 0
	if @JobID is not null
	begin
		if @RaiseAlert = 0
		begin
			if exists(select * from #1) 
				return 1
			else
				return -1
		end
		else
		begin
			if exists(select * from #1) 
			begin
				select * from #1
				raiserror('Pre-deployment job is successfully executed at least once . Manually invoke maint.usp_RemovePreDeploymentJob to remove the job and run installation again.', 16, 1);
				return 1
			end 
			else
			begin
				raiserror('Pre-deployment is in progress. Please wait until it is complete successfully.', 16, 1)
				return -1
			end
		end
	end
	return 0
end