set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_SynchronizationFilterSnapshot
(
	@ConfigurationID int,
	@FromValue sql_variant output, 
	@ToValue sql_variant output,
	@FromValueAdjusted sql_variant output,
	@ToValueAdjusted sql_variant output,
	@ShouldInitializate bit output
)
as
begin
	set nocount on
	select @ShouldInitializate = 1
	return
end