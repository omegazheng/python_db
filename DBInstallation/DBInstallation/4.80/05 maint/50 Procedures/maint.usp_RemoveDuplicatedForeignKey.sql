set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_RemoveDuplicatedForeignKey 
(
	@ParentObjectName nvarchar(255) = null, 
	@ReferencedObjectName nvarchar(255) = null,   
	@RemoveDirectly bit = 0
)
as
begin
	set nocount on;
	declare @SQL nvarchar(max);
	with x0 as
	(
		select 
				ObjectName,SchemaName, ParentTableName, ParentColumns, ReferencedSchemaName,ReferencedTableName , ReferencedColumns,
				row_number() over(partition by SchemaName, ParentTableName, ParentColumns, ReferencedSchemaName,ReferencedTableName, ReferencedColumns order by ObjectID) RowNumber
		from maint.fn_GetForeignKeyStructure(@ParentObjectName, @ReferencedObjectName)
		where SchemaName not in ('sys')
	)
	select 
			'alter table ' + quotename(SchemaName) + '.' + quotename(ParentTableName) + ' drop constraint ' + quotename(ObjectName) + ';' SQL,
			identity(int, 1,1) ID
			into #Query
	from x0
	where RowNumber > 1
	order by 1
	exec maint.usp_RunOrShowQuery @RemoveDirectly
end
go
--exec maint.usp_RemoveDuplicatedForeignKey 0