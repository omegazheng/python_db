set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CreateAllServiceJobs
as
begin
	set nocount on 
	declare @JobID uniqueidentifier, @cmd nvarchar(max), @ScheduleID int, @ScheduleUID uniqueidentifier
	
	if object_id('tempdb..#Schedule') is not null
		drop table #Schedule	
	create table #Schedule(
					schedule_id int, schedule_uid uniqueidentifier,schedule_name sysname, enabled int, freq_type int,
					freq_interval int, freq_subday_type int, freq_subday_interval int, freq_relative_interval int, freq_recurrence_factor int,
					active_start_date int, active_end_date int, active_start_time int, active_end_time int, date_created datetime,
					schedule_description nvarchar(4000), job_count int
	)
	insert into #Schedule
		exec msdb..sp_help_schedule

	select @ScheduleUID = schedule_uid, @ScheduleID = schedule_id from #Schedule where schedule_name = 'Omega Jobs - Every 5 minutes'
	if @@rowcount = 0
		exec  msdb.dbo.sp_add_schedule @schedule_uid = @ScheduleUID output, @schedule_id = @ScheduleID output, @schedule_name=N'Omega Jobs - Every 5 minutes', @enabled=1, @freq_type=4, @freq_interval=1, @freq_subday_type=4, @freq_subday_interval=5, @freq_relative_interval=0, @freq_recurrence_factor=0, @active_start_date=20170720, @active_end_date=99991231, @active_start_time=0, @active_end_time=235959


	if not exists(select * from msdb.dbo.sysjobs where name = '(maint) Omega Alert')
	begin
		exec msdb.dbo.sp_add_job @job_name=N'(maint) Omega Alert', @enabled=1, @notify_level_eventlog=0, @notify_level_email=0, @notify_level_netsend=0, @notify_level_page=0, @delete_level=0, @description=N'Monitor system and send alert', /*@category_name=N'Omega Jobs',*/ @owner_login_name=N'admin_all', @job_id = @JobID output
		select @cmd = 'exec '+quotename(db_name())+'.maint.usp_MonitorAll';
		exec msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=@cmd, @step_id=1, @cmdexec_success_code=0, @on_success_action=1, @on_success_step_id=0, @on_fail_action=2, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=N'exec [omegasys_demo].maint.usp_MonitorAll', @database_name=N'master', @flags=0
		exec msdb.dbo.sp_attach_schedule @job_id=@JobID,@schedule_id = @ScheduleID
		exec msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1
		exec msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)'
	end
	if @@servername like 'EC2AMAZ-%'
		exec msdb..sp_update_job @job_name=N'(maint) Omega Alert', @enabled = 0
	exec maint.usp_CreateServiceJob 'CopyAccountTranRealtimeToAccountTranAll', 'admin_all.usp_CopyAccountTranRealtimeToAccountTranAll', 'Move data from account_tran_realtime to account_tran_all', 0, 10
	exec maint.usp_CreateServiceJob 'Generate Game Winner List', 'admin_all.usp_GenerateGameWinnerList', 'Populate game winner list from Account_Tran table', 0, 10
	exec maint.usp_CreateServiceJob 'Generate Account Tran Hourly Aggregate', 'admin_all.usp_CalculateAccountTranHourlyAggregate', 'Populate Account Tran Hourly Aggregate from Account_Tran table', 0, 60
	exec maint.usp_CreateServiceJob 'Generate DW Game Player Daily', 'admin_all.usp_PopulateDWGamePlayerDaily', 'Generate DW Game Player Daily table', 0, 600
	exec maint.usp_CreateServiceJob 'Generate Data Feed', 'admin_all.usp_PopulateDataFeed', 'Generate Data Feed', 0, 10
	exec maint.usp_RemoveServiceJob '(maint) Update Statistics'
	exec maint.usp_CreateServiceJob 'Omega Maintenance', 'maint.usp_OmegaMaintenance', 'Various database maintenance work', 0, 3600
	--exec maint.usp_CreateServiceJob 'Generate Payment Aggregate', 'rpt.usp_CalculatePaymentAggregate', 'Populate Payment Aggregate from Payment table', 0, 60
	--exec maint.usp_CreateServiceJob 'Generate UserLogin Aggregate', 'rpt.usp_CalculateUserLoginAggregate', 'Populate UserLogin Aggregate', 0, 60
	exec maint.usp_CreateServiceJob 'Omega Service', 'admin_all.usp_OmegaService', 'Perform various tasks at backend', 0, 10
end

go
