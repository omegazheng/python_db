set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetObjectDefinition
(
	@ExistingFullObjectName nvarchar(255),
	@NewObjectName nvarchar(255) -- this dos not include schema name, the new schema will be same as existing schema
)
returns @ret table (ID int identity(1,1) primary key,Type varchar(50) not null, Description varchar(max), Definition nvarchar(max), NewDefinition nvarchar(max))
as
begin
	declare @ExistingObjectID int = object_id(@ExistingFullObjectName)
	declare @SchemaName sysname = object_schema_name(@ExistingObjectID), @ExistingObjectName sysname = object_name(@ExistingObjectID), @NewFullObjectName nvarchar(300)
	select @ExistingFullObjectName = quotename(@SchemaName) + '.' + quotename(@ExistingObjectName)
	select @NewObjectName = isnull(@NewObjectName, @ExistingObjectName), @NewFullObjectName = maint.fn_ReplaceObjectName(@ExistingFullObjectName, @NewObjectName)
	if @NewObjectName is null
		return
	--AddColumn 
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'AddColumn', 
				quotename(t.SchemaName) + '.' + quotename(t.ObjectName) + quotename(t.ColumnName),
--Defintion for existing object
				case 
					when t.ComputedColumnDefinition is null then 
						'if not exists(select * from sys.columns where object_id = object_id('+quotename(@ExistingFullObjectName, '''')+') and name = '+quotename(t.ColumnName, '''')+')
begin
	alter table ' + @ExistingFullObjectName + ' add ' + quotename(t.ColumnName) + ' ' + t.DataTypeByUserType + case when t.IsNullable = 1 then ' null' else ' not null ' end 
	+case when t.IsIdentity = 1 then ' identity(1,1) ' else '' end+
	+ case 
		when d.ObjectID is not null then 
			'constraint ' + quotename(d.ExpectedDefaultObjectName) + ' default' + d.Definition
		else ''
	end+';
end
'
					else
						'if not exists(select * from sys.columns where object_id = object_id('+quotename(@ExistingFullObjectName, '''')+') and name = '+quotename(t.ColumnName, '''')+')
begin
	alter table ' + @ExistingFullObjectName + ' add ' + quotename(t.ColumnName) + ' as ' + t.ComputedColumnDefinition + 
	';
end
'
				end,
--Defintion for new pbject
				case 
					when t.ComputedColumnDefinition is null then 
						'if not exists(select * from sys.columns where object_id = object_id('+quotename(@NewFullObjectName, '''')+') and name = '+quotename(t.ColumnName, '''')+')
begin
	alter table ' + @NewFullObjectName + ' add ' + quotename(t.ColumnName) + ' ' + t.DataTypeByUserType + case when t.IsNullable = 1 then ' null' else ' not null ' end 
	+case when t.IsIdentity = 1 then ' identity(1,1) ' else '' end+
	+ case 
		when d.ObjectID is not null then 
			'constraint ' + quotename(maint.fn_ReplaceFirstOccurence(d.ExpectedDefaultObjectName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))) + ' default' + d.Definition
		else ''
	end+';
end
'
					else
						'if not exists(select * from sys.columns where object_id = object_id('+quotename(@NewFullObjectName, '''')+') and name = '+quotename(t.ColumnName, '''')+')
begin
	alter table ' + @NewFullObjectName + ' add ' + quotename(t.ColumnName) + ' as ' + t.ComputedColumnDefinition + 
	';
end
'
				end
		from maint.fn_GetTableStructure(@ExistingFullObjectName) t
			left outer join maint.fn_GetDefaultStructure(@ExistingFullObjectName) d on t.ObjectID = d.ParentObjectID and t.ColumnID = d.ParentColumnID
		order by t.ColumnID
	--Create table
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'CreateTable',
				@ExistingFullObjectName,
				'create table ' + @ExistingFullObjectName + '('
					+ stuff(
							(
								select 
										',' + quotename(t.ColumnName) 
										+	case 
												when t.IsComputed = 1 then ' as ' + t.ComputedColumnDefinition 
												else
													' ' + t.DataTypeByUserType + case when t.IsNullable = 1 then ' null' else ' not null' end 
													+case when t.IsIdentity = 1 then ' identity(1,1) ' else '' end+
													+ case 
														when d.ObjectID is not null then 
															' constraint ' + quotename(d.ExpectedDefaultObjectName) + ' default' + d.Definition
														else ''
													end
											end
								from maint.fn_GetTableStructure(@ExistingFullObjectName) t
									left outer join maint.fn_GetDefaultStructure(@ExistingFullObjectName) d on t.ObjectID = d.ParentObjectID and t.ColumnID = d.ParentColumnID
								order by t.ColumnID
								for xml path(''), type
							).value('.', 'nvarchar(max)') , 1, 1, '')+')',
				'create table ' + @NewFullObjectName + '('
					+ stuff(
							(
								select 
										',' + quotename(t.ColumnName) 
										+	case 
												when t.IsComputed = 1 then ' as ' + t.ComputedColumnDefinition 
												else
													' ' + t.DataTypeByUserType + case when t.IsNullable = 1 then ' null' else ' not null' end 
													+case when t.IsIdentity = 1 then ' identity(1,1) ' else '' end+
													+ case 
														when d.ObjectID is not null then 
															' constraint ' + quotename(maint.fn_ReplaceFirstOccurence(d.ExpectedDefaultObjectName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))) + ' default' + d.Definition
														else ''
													end
											end
								from maint.fn_GetTableStructure(@ExistingFullObjectName) t
									left outer join maint.fn_GetDefaultStructure(@ExistingFullObjectName) d on t.ObjectID = d.ParentObjectID and t.ColumnID = d.ParentColumnID
								order by t.ColumnID
								for xml path(''), type
							).value('.', 'nvarchar(max)') , 1, 1, '')+')'
	--Index
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	case 
					when i.IsPrimaryKey = 1 then 'AddPrimaryKey' 
					when i.IsUniqueConstraint = 1 then 'AddUniqueKey'
					else 'AddIndex'
				end,
				case 
					when i.IsPrimaryKey = 1 then quotename(@SchemaName) + '.' + i.ExpectedIndexName
					when i.IsUniqueConstraint = 1 then quotename(@SchemaName) + '.' + i.ExpectedIndexName
					else @ExistingFullObjectName  + '.' + i.ExpectedIndexName
				end,
				'if not exists(select * from sys.indexes where object_id =  object_id(' + quotename(@ExistingFullObjectName, '''')+ ') and name = ' + quotename(i.ExpectedIndexName, '''') + ')
begin
	' 
		+	case 
				when  i.IsPrimaryKey = 1 then
					'alter table ' + @ExistingFullObjectName + ' add constraint ' + quotename(i.ExpectedIndexName) + ' primary key ' + lower(i.IndexType) 
				when i.IsUniqueConstraint = 1 then 
					'alter table ' + @ExistingFullObjectName + ' add constraint ' + quotename(i.ExpectedIndexName) + ' unique ' + lower(i.IndexType) 
				else 
					'create ' + case when i.IsUnique = 1 then 'unique ' else '' end 
					+ lower(i.IndexType) + ' index ' + quotename(i.ExpectedIndexName) + ' on ' + @ExistingFullObjectName
			end
		+ '(' + i.IndexColumnDefinition + ')'
		+ case when i.IncludedColumns is not null then ' include(' + i.IncludedColumns + ')'  else '' end
		+ case when i.FilterDefinition is not null then ' where ' + i.FilterDefinition else '' end 
		+ ' with(pad_index = off'+case when i.ignore_dup_key = 1 then ',ignore_dup_key=on' else '' end + isnull('data_compression=' + i.Compression , '')+')'
		+ ' on ' + quotename(i.Partition) + isnull(i.PartitionColumn , '') 
		+ '
end',
				'if not exists(select * from sys.indexes where object_id =  object_id(' + quotename(@NewFullObjectName, '''')+ ') and name = ' + quotename(maint.fn_ReplaceFirstOccurence(i.ExpectedIndexName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName)), '''') + ')
begin
	' 
		+	case 
				when  i.IsPrimaryKey = 1 then
					'alter table ' + @NewFullObjectName + ' add constraint ' + quotename(maint.fn_ReplaceFirstOccurence(i.ExpectedIndexName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))) + ' primary key ' + lower(i.IndexType) 
				when i.IsUniqueConstraint = 1 then 
					'alter table ' + @NewFullObjectName + ' add constraint ' + quotename(maint.fn_ReplaceFirstOccurence(i.ExpectedIndexName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))) + ' unique ' + lower(i.IndexType) 
				else 
					'create ' + case when i.IsUnique = 1 then 'unique ' else '' end 
					+ lower(i.IndexType) + ' index ' + quotename(maint.fn_ReplaceFirstOccurence(i.ExpectedIndexName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))) + ' on ' + @NewFullObjectName
			end
		+ '(' + i.IndexColumnDefinition + ')'
		+ case when i.IncludedColumns is not null then ' include(' + i.IncludedColumns + ')'  else '' end
		+ case when i.FilterDefinition is not null then ' where ' + i.FilterDefinition else '' end 
		+ ' with(pad_index = off'+case when i.ignore_dup_key = 1 then ',ignore_dup_key=on' else '' end + isnull('data_compression=' + i.Compression , '')+')'
		+ ' on ' + quotename(i.Partition) + isnull(i.PartitionColumn , '') 
		+ '
end'		
		from maint.fn_GetIndexStructure(@ExistingFullObjectName) i
	-- add default
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'AddDefault',
				quotename(@SchemaName) + '.' + quotename(d.ExpectedDefaultObjectName),
				'if object_id(' + quotename(quotename(@SchemaName) + '.' + quotename(d.ExpectedDefaultObjectName), '''') + ') is null
begin
	alter table ' + @ExistingFullObjectName + ' add constraint ' + quotename(d.ExpectedDefaultObjectName)+ ' default ' + d.Definition +' for ' + quotename(d.ParentColumnName) + ';
end',
				'if object_id(' + quotename(quotename(@SchemaName) + '.' + quotename(maint.fn_ReplaceFirstOccurence(d.ExpectedDefaultObjectName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))), '''') + ') is null
begin
	alter table ' + @NewFullObjectName + ' add constraint ' + quotename(maint.fn_ReplaceFirstOccurence(d.ExpectedDefaultObjectName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName)))+ ' default ' + d.Definition +' for ' + quotename(d.ParentColumnName) + ';
end'
		from maint.fn_GetDefaultStructure(@ExistingFullObjectName) d
--add FK
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'AddForeignKey',
				quotename(@SchemaName) + '.' + quotename(f.ExpectedObjectName),
				'if object_id('+quotename(quotename(@SchemaName) + '.' + quotename(f.ExpectedObjectName), '''')+') is null
begin
	alter table ' + @ExistingFullObjectName + case when f.IsDisabled = 1 then ' with nocheck' else ' with check' end
	+' add constraint ' 
	+ f.ExpectedObjectName + ' foreign key (' + f.ParentColumns + ') references ' + quotename(f.ReferencedSchemaName) + '.' + quotename(f.ReferencedTableName) +'(' + f.ReferencedColumns + ')'+
	+ case when f.DeleteAction <> 'NO_ACTION' then ' on delete '+lower(f.DeleteAction) else '' end
	+ case when f.UpdateAction <> 'NO_ACTION' then ' on update '+lower(f.UpdateAction) else '' end
	+'
end', 
				'if object_id('+quotename(quotename(@SchemaName) + '.' + quotename(maint.fn_ReplaceFirstOccurence(f.ExpectedObjectName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))), '''')+') is null
begin
	alter table ' + @NewFullObjectName + case when f.IsDisabled = 1 then ' with nocheck' else ' with check' end
	+' add constraint ' 
	+ maint.fn_ReplaceFirstOccurence(f.ExpectedObjectName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName)) 
	+ ' foreign key (' + f.ParentColumns + ') references ' + quotename(f.ReferencedSchemaName) + '.' + quotename(f.ReferencedTableName) +'(' + f.ReferencedColumns + ')'+
	+ case when f.DeleteAction <> 'NO_ACTION' then ' on delete '+lower(f.DeleteAction) else '' end
	+ case when f.UpdateAction <> 'NO_ACTION' then ' on update '+lower(f.UpdateAction) else '' end
	+'
end'
		from maint.fn_GetForeignKeyStructure(@ExistingFullObjectName, null) f
--Drop FK
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'DropForeignKey',
				quotename(@SchemaName) + '.' + quotename(f.ObjectName),
				'if object_id('+quotename(quotename(@SchemaName) + '.' + quotename(f.ObjectName), '''')+') is not null
begin
	alter table ' + @ExistingFullObjectName + ' drop constraint ' + quotename(f.ObjectName)+ ';
end', 
				null
		from maint.fn_GetForeignKeyStructure(@ExistingFullObjectName, null) f
--add FK referencing
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'AddForeignKeyReferencing',
				quotename(f.ReferencedSchemaName) + '.' + quotename(f.ExpectedObjectName),
				'if object_id('+quotename(quotename(@SchemaName) + '.' + quotename(f.ExpectedObjectName), '''')+') is null
begin
	alter table ' + quotename(SchemaName) + '.' + quotename(ParentTableName) + case when f.IsDisabled = 1 then ' with nocheck' else ' with check' end
	+' add constraint ' 
	+ f.ExpectedObjectName + ' foreign key (' + f.ParentColumns + ') references ' + quotename(f.ReferencedSchemaName) + '.' + quotename(f.ReferencedTableName) +'(' + f.ReferencedColumns + ')'+
	+ case when f.DeleteAction <> 'NO_ACTION' then ' on delete '+lower(f.DeleteAction) else '' end
	+ case when f.UpdateAction <> 'NO_ACTION' then ' on update '+lower(f.UpdateAction) else '' end
	+'
end',
				'if object_id('+quotename(quotename(@SchemaName) + '.' 
					+ quotename(maint.fn_ReplaceFirstOccurence(f.ExpectedObjectName, 
						f.SchemaName +'_' +f.ParentTableName + case when f.SchemaName = @SchemaName then '' else '_' + @SchemaName end + '_' +ReferencedTableName, 
						f.SchemaName +'_' +f.ParentTableName + case when f.SchemaName = @SchemaName then '' else '_' + @SchemaName end + '_' +@NewObjectName
							)), '''')
					+') is null
begin
	alter table ' + quotename(f.SchemaName) + '.' + quotename(f.ParentTableName) + case when f.IsDisabled = 1 then ' with nocheck' else ' with check' end
	+' add constraint ' 
	+ maint.fn_ReplaceFirstOccurence(f.ExpectedObjectName, 
						f.SchemaName +'_' +f.ParentTableName + case when f.SchemaName = @SchemaName then '' else '_' + @SchemaName end + '_' +ReferencedTableName, 
						f.SchemaName +'_' +f.ParentTableName + case when f.SchemaName = @SchemaName then '' else '_' + @SchemaName end + '_' +@NewObjectName
							)
	+ ' foreign key (' + f.ParentColumns + ') references ' + quotename(f.ReferencedSchemaName) + '.' + quotename(f.ReferencedTableName) +'(' + f.ReferencedColumns + ')'+
	+ case when f.DeleteAction <> 'NO_ACTION' then ' on delete '+lower(f.DeleteAction) else '' end
	+ case when f.UpdateAction <> 'NO_ACTION' then ' on update '+lower(f.UpdateAction) else '' end
	+'
end'

		from maint.fn_GetForeignKeyStructure(null, @ExistingFullObjectName) f

--Drop FK referencing
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'DropForeignKeyReferencing',
				quotename(f.ReferencedSchemaName) + '.' + quotename(f.ObjectName),
				'if object_id('+quotename(quotename(f.SchemaName) + '.' + quotename(f.ObjectName), '''')+') is not null
begin
	alter table ' + quotename(f.SchemaName) + '.' + quotename(f.ParentTableName) +' drop constraint ' 
	+ quotename(f.ObjectName) + ';
end',
				null

		from maint.fn_GetForeignKeyStructure(null, @ExistingFullObjectName) f

	-- add check constraint
	insert into @ret(Type, Description, Definition, NewDefinition)
		select	'AddCheckConstraint',
				quotename(c.SchemaName) + '.' + quotename(c.CheckConstraintName),
				'if object_id('+quotename(quotename(c.SchemaName) + '.' + quotename(c.CheckConstraintName), '''')+') is null
begin
	alter table ' + @ExistingFullObjectName + case when c.IsDisabled = 1 then ' with nocheck' else ' with check' end + ' add constraint ' + quotename(c.CheckConstraintName) + ' check ' + c.Definition + ';
end' ,
				'if object_id('+quotename(quotename(c.SchemaName) + '.' + quotename(maint.fn_ReplaceFirstOccurence(c.CheckConstraintName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))), '''')+') is null
begin
	alter table ' + @ExistingFullObjectName + case when c.IsDisabled = 1 then ' with nocheck' else ' with check' end + ' add constraint ' + quotename(maint.fn_ReplaceFirstOccurence(c.CheckConstraintName, maint.fn_ObjectNameToNamingConvention(@ExistingFullObjectName), maint.fn_ObjectNameToNamingConvention(@NewFullObjectName))) + ' check ' + c.Definition + ';
end'
		from maint.fn_GetCheckConstraintStructure(@ExistingFullObjectName) c
	return
end
go
--select * from maint.fn_GetObjectDefinition('external_mpt.User_Conf', 'User_Conf_Deployment') where Type not in('AddColumn')
--select * from maint.fn_GetTableStructure('external_mpt.User_Conf')
--select * from maint.fn_GetObjectDefinition('admin_all.account', 'Account_Deployment') 
--select * from maint.fn_GetObjectDefinition('admin_all.AccountTranAggregateHourlyPendingCalculation1', 'admin_all.AccountTranAggregateHourlyPendingCalculation1_Deployment')
--select * from maint.fn_GetTableStructure('admin_all.account')
--select * from maint.fn_GetDefaultStructure(null)

--select * from maint.fn_GetForeignKeyStructure(null, 'admin_all.account')
--select * from maint.fn_GetForeignKeyStructure('admin_all.account', null)

