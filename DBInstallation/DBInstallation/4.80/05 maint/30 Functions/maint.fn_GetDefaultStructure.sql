set ansi_nulls, quoted_identifier on 
go

create or alter function maint.fn_GetDefaultStructure(@ParentObjectName nvarchar(255) = null)
returns table
as
return  (
			select 
					d.object_id as ObjectID,
					object_schema_name(d.object_id) SchemaName,
					d.name as DefaultObjectName,
					maint.fn_GetExpectedName('Default', object_schema_name(d.object_id), object_name(d.parent_object_id), col_name(d.parent_object_id, d.parent_column_id), null, null) ExpectedDefaultObjectName,
					d.parent_object_id ParentObjectID,
					d.parent_column_id ParentColumnID,
					object_name(d.parent_object_id) ParentObjectName,
					col_name(d.parent_object_id, d.parent_column_id) ParentColumnName,
					d.definition Definition,
					d.is_system_named IsSystemNamed
			from sys.default_constraints d
			where @ParentObjectName is null or d.parent_object_id = object_id(@ParentObjectName)
		)
go
--select * from maint.fn_GetDefaultStructure(null) where DefaultObjectName<>ExpectedDefaultObjectName

