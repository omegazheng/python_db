set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_GetSynchronizationRange(@Name varchar(128))
returns table
as
return  (
			select p.ConfigurationID, p.FromValue, p.ToValue, p.FromValueAdjusted, p.ToValueAdjusted, p.Error
			from maint.SynchronizationProgress p
				inner join maint.SynchronizationConfiguration c on p.ConfigurationID = c.ConfigurationID
			where c.Name = @Name
		)