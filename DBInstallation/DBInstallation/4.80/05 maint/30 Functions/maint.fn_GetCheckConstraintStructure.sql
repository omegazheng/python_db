set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_GetCheckConstraintStructure(@ParentObjectName nvarchar(255)=null)
returns table
as
return  (
			select 
					cc.object_id as ObjectID,
					object_schema_name(cc.object_id) SchemaName,
					cc.name as CheckConstraintName,
					cc.parent_object_id ParentObjectID,
					object_name(cc.parent_object_id) ParentObjectName,
					col_name(cc.parent_object_id, cc.parent_column_id) ParentColumnName,
					cc.definition Definition,
					cc.is_disabled IsDisabled,
					cc.is_system_named IsSystemNamed,
					cc.is_not_trusted IsNotTrusted
			from sys.check_constraints cc
			where @ParentObjectName is null or cc.parent_object_id = object_id(@ParentObjectName)
		)
go
--select * from maint.fn_GetCheckConstraintStructure(null) 

