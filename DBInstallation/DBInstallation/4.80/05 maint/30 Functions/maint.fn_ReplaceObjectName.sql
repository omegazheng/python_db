set ansi_nulls, quoted_identifier on
go
create or alter function maint.fn_ReplaceObjectName(@FullName nvarchar(257), @Replacement nvarchar(128))
returns nvarchar(257)
as
begin
	return case when parsename(@FullName, 2) is not null then quotename(parsename(@FullName, 2)) + '.' else '' end +  quotename(parsename(@Replacement, 1))
end
go
--select maint.fn_ReplaceObjectName('11.[22]', '33')
