set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_GetServiceJobID(@Name nvarchar(255))
returns uniqueidentifier
as
begin
	return (select job_id from msdb.dbo.sysjobs where name = maint.fn_GetServiceJobName(@Name)) 
end