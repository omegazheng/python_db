set ansi_nulls, quoted_identifier on
go
create or alter view maint.v_SynchronizationProgress
as

select c.ConfigurationID, c.Name, c.BatchSize, c.IsActive, case when p.ToValue is null then 1 else 0 end NeedInitialization,
	p.BatchCount,
	p.TotalRows, TotalRows*1.0/nullif(datediff(second, StartDate, isnull(EndDate, getdate())), 0) RowsPerSecond,  datediff(second, StartDate, isnull(EndDate, getdate()))*1.0/nullif(BatchCount,0) SecondPerBatch, 
	p.RowsInBatch, p.StartDate, p.EndDate, datediff_big(second, StartDate, isnull(EndDate, getdate())) DurationInSecond, p.Error, p.FromValue, p.ToValue, p.FromValueAdjusted, p.ToValueAdjusted
from maint.SynchronizationConfiguration c
	left join maint.SynchronizationProgress p on c.ConfigurationID = p.ConfigurationID


--select * from maint.v_ActiveSessions where SessionID = 1471


--select top 100 * from rpt.AccountTranStaging order by 1 desc

--select count(*) from  admin_all.ACCOUNT_TRAN_ALL


--select * from admin_all.BRAND_GAME_JP_RATIO


--select * from [tempdb].[CWT_PrimaryKey]
--select * from external_mpt.USER_CONF where CURRENCY not in (select iso_code from admin_all.CURRENCY)