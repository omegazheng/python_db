if not exists(select * from sys.columns where object_id = object_id('admin_all.Staff_Login_Log') and name = 'login_attempts')
	alter table admin_all.Staff_Login_Log add login_attempts integer not null
	    constraint DF_admin_all_Staff_Login_Log_login_attempts default 0