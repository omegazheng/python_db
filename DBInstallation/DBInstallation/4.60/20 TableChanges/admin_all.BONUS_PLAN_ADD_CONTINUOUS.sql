SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN]') = object_id and name = 'IS_CONTINUOUS')
  BEGIN
    ALTER TABLE [admin_all].[BONUS_PLAN]
      ADD [IS_CONTINUOUS] BIT NOT NULL DEFAULT 0
  END
GO