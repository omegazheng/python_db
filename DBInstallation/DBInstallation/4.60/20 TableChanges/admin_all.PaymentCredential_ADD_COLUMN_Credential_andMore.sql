SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[PaymentCredential]') = object_id and name = 'Credential')
   BEGIN
     ALTER TABLE [admin_all].[PaymentCredential]
       ADD [Credential] nvarchar(50);
   END
GO


IF EXISTS(select * from sys.indexes where name = 'UQ_admin_all_PAYMENTCREDENTIAL_ACCOUNTID_INFO' and object_id = object_id('admin_all.PaymentCredential'))
BEGIN
  ALTER TABLE admin_all.PaymentCredential DROP CONSTRAINT UQ_admin_all_PAYMENTCREDENTIAL_ACCOUNTID_INFO
END

