SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN_FREEPLAY]') = object_id and name = 'LINE_VALUE')
  BEGIN
    ALTER TABLE [admin_all].[BONUS_PLAN_FREEPLAY]
      ADD [LINE_VALUE] NVARCHAR(50) null
  END
GO