SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN]') = object_id and name = 'PRODUCT_ID')
  BEGIN
    ALTER TABLE [admin_all].[BONUS_PLAN]
      ADD [PRODUCT_ID] int NULL;

    ALTER TABLE admin_all.BONUS_PLAN
      ADD CONSTRAINT FK_admin_all_BONUS_PLAN_PLATFORM_PRODUCT_ID
      FOREIGN KEY (PRODUCT_ID) REFERENCES admin_all.PLATFORM (ID);

    create index IDX_admin_all_BONUS_PLAN_PRODUCT_ID ON admin_all.BONUS_PLAN(PRODUCT_ID)
  END
GO


