SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN_FREEPLAY]') = object_id and name = 'DENOMINATION')
  IF ((select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'ADMIN_ALL' and TABLE_NAME = 'BONUS_PLAN_FREEPLAY' and COLUMN_NAME = 'DENOMINATION') = 'numeric')
      BEGIN
        ALTER TABLE [admin_all].[BONUS_PLAN_FREEPLAY]
          ALTER COLUMN [DENOMINATION] NVARCHAR(255)
      END
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN_FREEPLAY]') = object_id and name = 'COST')
    IF ((select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'ADMIN_ALL' and TABLE_NAME = 'BONUS_PLAN_FREEPLAY' and COLUMN_NAME = 'COST') = 'numeric')
        BEGIN
            ALTER TABLE [admin_all].[BONUS_PLAN_FREEPLAY]
                ALTER COLUMN [COST] NVARCHAR(255)
        END
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN_FREEPLAY]') = object_id and name = 'COIN_SIZE')
    IF ((select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'ADMIN_ALL' and TABLE_NAME = 'BONUS_PLAN_FREEPLAY' and COLUMN_NAME = 'COIN_SIZE') = 'numeric')
        BEGIN
            ALTER TABLE [admin_all].[BONUS_PLAN_FREEPLAY]
                ALTER COLUMN [COIN_SIZE] NVARCHAR(255)
        END
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN_FREEPLAY]') = object_id and name = 'ALLOW_MULTIPLE_ASSIGN')
    IF ((select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'ADMIN_ALL' and TABLE_NAME = 'BONUS_PLAN_FREEPLAY' and COLUMN_NAME = 'ALLOW_MULTIPLE_ASSIGN') = 'binary')
        BEGIN
            ALTER TABLE [admin_all].[BONUS_PLAN_FREEPLAY]
                ALTER COLUMN [ALLOW_MULTIPLE_ASSIGN] bit
        END
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN_FREEPLAY]') = object_id and name = 'MIN_BET')
    IF ((select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'ADMIN_ALL' and TABLE_NAME = 'BONUS_PLAN_FREEPLAY' and COLUMN_NAME = 'MIN_BET') = 'binary')
        BEGIN
            ALTER TABLE [admin_all].[BONUS_PLAN_FREEPLAY]
                ALTER COLUMN [MIN_BET] bit
        END
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[BONUS_PLAN_FREEPLAY]') = object_id and name = 'IS_ROUND_BET')
    IF ((select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'ADMIN_ALL' and TABLE_NAME = 'BONUS_PLAN_FREEPLAY' and COLUMN_NAME = 'IS_ROUND_BET') = 'binary')
        BEGIN
            ALTER TABLE [admin_all].[BONUS_PLAN_FREEPLAY]
                ALTER COLUMN [IS_ROUND_BET] bit
        END
GO