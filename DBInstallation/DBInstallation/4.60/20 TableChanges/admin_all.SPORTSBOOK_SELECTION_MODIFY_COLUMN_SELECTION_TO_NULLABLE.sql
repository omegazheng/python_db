
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(select * from sys.columns where object_id('[admin_all].[SPORTSBOOK_SELECTION]') = object_id and name = 'SELECTION')
  BEGIN
    ALTER TABLE [admin_all].[SPORTSBOOK_SELECTION]
    ALTER COLUMN [SELECTION] varchar(2000) null
  END
GO