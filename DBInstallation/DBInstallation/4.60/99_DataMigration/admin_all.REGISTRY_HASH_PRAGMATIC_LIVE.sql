if exists(select *
          from admin_all.PLATFORM
          where CODE in ('PRAGMATIC_LIVE'))
    begin
        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'pragmatic.live.cashierUrl')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('pragmatic.live.cashierUrl', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'pragmatic.live.lobbyUrl')
            BEGIN
                IF EXISTS(SELECT * FROM admin_all.REGISTRY_HASH WHERE MAP_KEY in ('topgame.lobbyUrl'))
                    BEGIN
                        insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                        SELECT 'pragmatic.live.lobbyUrl', VALUE FROM admin_all.REGISTRY_HASH WHERE MAP_KEY in ('topgame.lobbyUrl')
                    END
                ELSE
                    insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                    VALUES ('pragmatic.live.lobbyUrl', 'TBD');
            END

        if not exists(select *
                      from admin_all.REGISTRY_HASH
                      where MAP_KEY = 'pragmatic.live.styleName')
            BEGIN
                insert into admin_all.REGISTRY_HASH (MAP_KEY, VALUE)
                VALUES ('pragmatic.live.styleName', 'TBD');
            END
    END
