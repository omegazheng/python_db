set xact_abort on
begin transaction
delete a
from (select *, row_number() over(partition by PRODUCT_ID, GAME_TRAN_ID order by id) rn from admin_all.GAME_INSTANCE_DETAIL) a
where rn > 1
if not exists(select * from sys.indexes where name = 'UQ_admin_all_GAME_INSTANCE_DETAIL_PRODUCT_ID_GAME_TRAN_ID' and object_id = object_id('admin_all.GAME_INSTANCE_DETAIL'))
begin
	alter table admin_all.GAME_INSTANCE_DETAIL add constraint UQ_admin_all_GAME_INSTANCE_DETAIL_PRODUCT_ID_GAME_TRAN_ID unique(PRODUCT_ID, GAME_TRAN_ID)with (ignore_dup_key = on)
end
if not exists(select * from sys.indexes where name = 'UQ_admin_all_GAME_INSTANCE_DETAIL_PRODUCT_ID_GAME_TRAN_ID' and object_id = object_id('admin_all.GAME_INSTANCE_DETAIL') and ignore_dup_key = 1)
begin
	alter table admin_all.GAME_INSTANCE_DETAIL drop constraint UQ_admin_all_GAME_INSTANCE_DETAIL_PRODUCT_ID_GAME_TRAN_ID;
	alter table admin_all.GAME_INSTANCE_DETAIL add constraint UQ_admin_all_GAME_INSTANCE_DETAIL_PRODUCT_ID_GAME_TRAN_ID unique(PRODUCT_ID, GAME_TRAN_ID)with (ignore_dup_key = on)
end
commit


