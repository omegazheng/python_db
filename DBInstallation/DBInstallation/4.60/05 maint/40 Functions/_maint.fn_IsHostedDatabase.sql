set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_IsHostedDatabase()
returns bit
as
begin
	if maint.fn_IsAmazonDatabase() = 1
		return 1
	if maint.fn_IsAzureDatabase() = 1
		return 1
	return 0
end
