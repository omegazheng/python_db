set ansi_nulls, quoted_identifier on
go
if object_id('maint.fn_IsLinux') is null
	exec('create function maint.fn_IsLinux() returns bit as begin return 1 end')
go
alter function maint.fn_IsLinux() 
returns bit 
as 
begin 
	return case when @@version like '%linux%' then 1 else 0 end
end