set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseCollaction()
returns varchar(max)
begin
	return (
			select 'Database collation must be case-insensitive' 
			from sys.fn_helpcollations() c
				inner join sys.databases d on c.name = d.collation_name
			where d.name = db_name()
				and c.description not like '%, case-insensitive%'
			)
end