set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_IsAzureDatabase()
returns bit
as
begin
	if @@version like '%Microsoft SQL Azure%'
		return 0
	return 0
end
