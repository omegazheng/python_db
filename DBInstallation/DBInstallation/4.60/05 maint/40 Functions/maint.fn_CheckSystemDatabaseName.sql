set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseName()
returns varchar(max)
begin
	return (select top 1 'Database name must be omega_prod' from dbo.CUSTOMER where name <> 'DEMO' and db_name() <> 'omega_prod')
end