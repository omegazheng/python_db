set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseTrustWorthy()
returns varchar(max)
begin
	return (select 'Database property Trustworthy should be enabled.' from sys.databases where name = db_name() and is_trustworthy_on = 0)
end
