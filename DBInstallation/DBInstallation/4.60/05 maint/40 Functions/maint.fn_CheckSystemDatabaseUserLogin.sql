set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseUserLogin()
returns varchar(max)
begin
	return (
			case when not exists(
										select * 
										from sys.database_principals dp
											inner join sys.server_principals sp on dp.sid = sp.sid
										where dp.name = 'admin_all'
									)
				 then 'Could not find login for user admin_all' 
			end
			)
end