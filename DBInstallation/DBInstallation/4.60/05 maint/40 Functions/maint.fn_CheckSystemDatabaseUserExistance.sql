set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseUserExistance()
returns varchar(max)
begin
	return (case when not exists(select * from sys.database_principals where name = 'admin_all') then 'Could not find admin_all user' end )
end