set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemFileGrowth()
returns varchar(max)
begin
	return (select top 1 'Minimum file growth for data and log files should be greater than 200MB' from sys.database_files where growth < 25600)
end