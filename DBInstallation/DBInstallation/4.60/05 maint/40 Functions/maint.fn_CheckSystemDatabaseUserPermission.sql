set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseUserPermission()
returns varchar(max)
begin
	return (case when isnull(is_rolemember('db_owner', 'admin_all'),0) <>1 then 'User admin_all is not a member of db_owner' end)
end