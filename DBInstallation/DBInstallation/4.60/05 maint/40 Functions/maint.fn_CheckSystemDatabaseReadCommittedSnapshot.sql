set ansi_nulls, quoted_identifier on 
go
create or alter function maint.fn_CheckSystemDatabaseReadCommittedSnapshot()
returns varchar(max)
begin
	return (
			select 'Read Committed Snapshot must be turned on' 
			from sys.databases 
			where name = db_name() 
				and is_read_committed_snapshot_on <> 1
			)
end