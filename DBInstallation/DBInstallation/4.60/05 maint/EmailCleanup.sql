

update admin_all.REGISTRY_HASH set VALUE = replace(VALUE, 'john.huang@sqlnotes.info', 'john@omegasys.eu') where VALUE like '%john.huang@sqlnotes.info%'

update admin_all.REGISTRY_HASH set VALUE = replace(VALUE, 'john@omegasys.eu', 'SQLAlert@omegasys.eu') where VALUE like '%john@omegasys.eu%'


update maint.AlertMailList set Recipients = replace(Recipients, 'john.huang@sqlnotes.info', 'john@omegasys.eu') where Recipients like '%john.huang@sqlnotes.info%'

update maint.AlertMailList set Recipients = replace(Recipients, 'john@omegasys.eu', 'SQLAlert@omegasys.eu') where Recipients like '%john@omegasys.eu%'
update maint.AlertMailList set Recipients = replace(Recipients, 'zheng@omegasys.eu;', '') where Recipients like '%zheng@omegasys.eu;%'
update maint.AlertMailList set Recipients = replace(Recipients, 'mak@omegasys.eu;', '') where Recipients like '%mak@omegasys.eu;%'
update maint.AlertMailList set Recipients = replace(Recipients, 'leo@omegasys.eu;', '') where Recipients like '%leo@omegasys.eu;%'
update maint.AlertMailList set Recipients = replace(Recipients, 'robbie@omegasys.eu;', '') where Recipients like '%robbie@omegasys.eu;%'
update maint.AlertMailList set Recipients = replace(Recipients, ' ', '') where Recipients like '% %'

begin try
update msdb.dbo.sysmail_account set replyto_address = 'SQLAlert@omegasys.eu' WHERE  name = 'OmegaAlert' and replyto_address <> 'SQLAlert@omegasys.eu'
end try
begin catch
end catch


