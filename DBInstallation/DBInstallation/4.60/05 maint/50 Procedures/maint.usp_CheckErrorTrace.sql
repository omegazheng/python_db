set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_CheckErrorTrace
as
begin
	set nocount on
	exec maint.usp_CreateErrorTrace
	declare @x xml
	select @x = t.target_data 
	from sys.dm_xe_sessions s
		inner join sys.dm_xe_session_targets t on s.address = t.event_session_address
	where s.name = maint.fn_GetErrorTraceName()
	insert into maint.ErrorTrace(EventDateUTC, EventSequence, ErrorNumber, Severity, State, IsUserDefined, ErrorCategory, ErrorCategoryText, ErrorDestination, ErrorDestinationText, IsIntercepted, ErrorMessage, SQLText, SessionServerPrincipalname, SessionInstanceName, DatabaseName, ContextInfo, HostName, ApplicationName)
	select  EventDateUTC, EventSequence, ErrorNumber, Severity, State, IsUserDefined, ErrorCategory, ErrorCategoryText, ErrorDestination, ErrorDestinationText, IsIntercepted, ErrorMessage, SQLText, SessionServerPrincipalname, SessionInstanceName, DatabaseName, ContextInfo, HostName, ApplicationName
	from maint.fn_ExtractErrorEvent(@x) x
	where not exists(select * from maint.ErrorTrace e where e.EventDateUTC = x.EventDateUTC and e.EventSequence = x.EventSequence)
	
	delete maint.ErrorTrace where EventDateUTC < dateadd(month, -3, getutcdate())

	if admin_all.fn_GetRegistry('database.errorTraceAlert.enabled') = '0'
		return
	declare @DateFrom datetime, @DateTo datetime
	select @DateFrom = isnull(convert(datetime, admin_all.fn_GetRegistry('database.errorTraceAlert.date'), 121), '2000-01-01')
	select @DateTo = getutcdate()
	if datediff(minute, @DateFrom, @DateTo) < 1440
		return

	---additional check 
	declare @SQL nvarchar(max), @FunctionName nvarchar(max)
	declare c cursor static for 
		select  'maint.' + name
		from sys.objects
		where schema_id = schema_id('maint')
			and type = 'FN'
			and name like 'fn!_CheckSystem%' escape '!'
		order by 1
	open c 
	fetch next from c into @FunctionName
	while @@fetch_status = 0
	begin
		select @SQL = 'select @SQL = '+@FunctionName+'()'
		exec sp_executesql @SQL, N'@SQL varchar(max) output', @SQL output
		if @SQL is not null
			exec maint.usp_CreateErrorTraceEntry @SQL
		fetch next from c into @FunctionName
	end
	close c
	deallocate c
	
	if not exists(select * from maint.ErrorTrace where EventDateUTC > @DateFrom and EventDateUTC <= @DateTo)
		return
	declare @Body nvarchar(max)
	select @Body = '<html>
<head>
  <title></title>
</head>
<body>
<table border="1" style="border-collapse:collapse">
<tr><th>Count</th><th>ErrorNumber</th><th>ErrorMessage</th><th>SQLText</th><th>DatabaseName</th><th>HostName</th><th>ApplicationName</th></tr>
'+
	(
			select 
					(select Count  as td for xml path(''), type),
					(select ErrorNumber as td for xml path(''), type), 
					(select ErrorMessage as td for xml path(''), type), 
					(select SQLText as td for xml path(''), type),  
					(select DatabaseName as td for xml path(''), type), 
					(select HostName as td for xml path(''), type), 
					(select ApplicationName as td for xml path(''), type)
			from (
					select	count(*)  Count,
							ErrorNumber, 
							ErrorMessage, 
							SQLText,  
							DatabaseName, 
							HostName, 
							ApplicationName
					from maint.ErrorTrace
					where EventDateUTC > @DateFrom
						and EventDateUTC <= @DateTo
						--and SQLText not like '%QRTZ%'
					group by ErrorNumber, ErrorMessage, SQLText,  DatabaseName, HostName, ApplicationName
				) x
				order by Count desc, ErrorMessage asc
				for xml path('tr')
		)
+'

</table>
</body>
</html>'
--print @body

	exec maint.usp_SendAlert 'Exceptions found', @Body, 1, 'HTML'
	select @Body = convert(varchar(max), @DateTo, 121)
	exec admin_all.usp_SetRegistry 'database.errorTraceAlert.date', @Body
end
go
--
go



