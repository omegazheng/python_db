set ansi_nulls,quoted_identifier on
go
create or alter procedure maint.usp_CreateErrorTrace
as
begin
	set nocount on 
	declare @SQL nvarchar(max)
	if not exists(select * from  sys.server_event_sessions where name = maint.fn_GetErrorTraceName())
	begin
		select @SQL = '
create event session '+quotename(maint.fn_GetErrorTraceName())+' on server 
add event sqlserver.error_reported
(
    action
	(
		package0.collect_system_time,
		package0.event_sequence,
		sqlserver.client_app_name,
		sqlserver.client_hostname,
		sqlserver.context_info,
		sqlserver.database_name,
		sqlserver.server_instance_name,
		sqlserver.session_server_principal_name,
		sqlserver.sql_text
	)
	where (
				sqlserver.equal_i_sql_unicode_string(sqlserver.database_name,N'+quotename(rtrim(db_name()), '''')+') 
				and severity > 10
				and (not (sqlserver.like_i_sql_unicode_string([sqlserver].[sql_text],N''%QRTZ%'')))
			)
)
add target package0.ring_buffer with (max_memory = 8192 KB,event_retention_mode = allow_single_event_loss, max_dispatch_latency = 30 seconds, max_event_size = 0 KB, memory_partition_mode = none, track_causality = off, startup_state = on)
'
		exec(@SQL)
	end
	if not exists(select * from sys.dm_xe_sessions where name = maint.fn_GetErrorTraceName())
	begin
		select @SQL = 'alter event session '+quotename(maint.fn_GetErrorTraceName())+' on server state = start'
		exec(@SQL)
	end
end
go
if is_srvrolemember('sysadmin') = 1
	exec maint.usp_CreateErrorTrace