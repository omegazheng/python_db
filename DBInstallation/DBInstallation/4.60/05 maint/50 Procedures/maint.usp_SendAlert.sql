set ansi_nulls, quoted_identifier on
go
if object_id('maint.usp_SendAlert') is null
	exec('create procedure maint.usp_SendAlert as --')
go
alter procedure maint.usp_SendAlert 
(
	@Subject nvarchar(255) = 'mail test',
	@Body nvarchar(max) = null,
	@TestMode bit = 0,
	@BodyFormat varchar(20) = 'TEXT'
)
as
begin
	set nocount on
	declare @Recipients varchar(max)
	select @Recipients = Recipients
	from maint.AlertMailList
	where Customer = 'OMEGA'
	select @Recipients = Recipients + @Recipients
	from maint.AlertMailList
	where Customer collate database_default = (select rtrim(name) from dbo.CUSTOMER)
	--print @Recipients
	if @TestMode = 1
		select @Recipients = 'SQLAlert@omegasys.eu'
	exec maint.usp_ComposeAlertTitle @Subject output
	exec maint.usp_SendMail @profile_name = 'OmegaAlert', @recipients=@Recipients, @subject = @Subject, @body = @Body, @body_format = @BodyFormat
end
go

