set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_CreateErrorTraceEntry
(
	@Message varchar(max)
)
as
begin
	set nocount on
	declare @EventSequence bigint, @EventDateUTC datetime = getutcdate()
	select @EventSequence = isnull((select min(EventSequence) from maint.ErrorTrace where EventDateUTC = @EventDateUTC), 0) - 1
	insert into maint.ErrorTrace(EventDateUTC, EventSequence, ErrorNumber, Severity, State, IsUserDefined, ErrorCategory, ErrorCategoryText, ErrorDestination, ErrorDestinationText, IsIntercepted, ErrorMessage, SQLText, SessionServerPrincipalname, SessionInstanceName, DatabaseName, ContextInfo, HostName, ApplicationName)
			select @EventDateUTC EventDateUTC, 
					@EventSequence EventSequence, 
					-1 ErrorNumber, -1 Severity, -1 State, 1 IsUserDefined, 2 ErrorCategory, 'SERVER' ErrorCategoryText, '0x00000002' ErrorDestination, 'USER' ErrorDestinationText, 0 IsIntercepted, 
					@Message ErrorMessage, 
					null SQLText, 
					'Unknown' SessionServerPrincipalname, rtrim(@@servername) SessionInstanceName, db_name() DatabaseName, null ContextInfo, rtrim(@@servername) HostName, 'Server' ApplicationName
end