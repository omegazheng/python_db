
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
go
if object_id('maint.usp_MonitorAll') is null
begin
	exec('create procedure maint.usp_MonitorAll as --')
end
GO
alter procedure maint.usp_MonitorAll
as
begin
	set nocount on
	exec maint.usp_MonitorCPUUsage
	exec maint.usp_MonitorLongRunningQuery
	exec maint.usp_CheckErrorTrace
end