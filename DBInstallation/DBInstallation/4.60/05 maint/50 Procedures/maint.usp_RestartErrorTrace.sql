set ansi_nulls,quoted_identifier on
go
create or alter procedure maint.usp_RestartErrorTrace
as
begin
	set nocount on
	if not exists(select * from  sys.server_event_sessions where name = maint.fn_GetErrorTraceName())
	begin
		exec maint.usp_CreateErrorTrace
		return
	end
	declare @SQL nvarchar(max)
	if exists(select * from sys.dm_xe_sessions where name = maint.fn_GetErrorTraceName())
	begin
		select @SQL = 'alter event session '+quotename(maint.fn_GetErrorTraceName())+' on server state = stop'
		exec(@SQL)
	end
	if not exists(select * from sys.dm_xe_sessions where name = maint.fn_GetErrorTraceName())
	begin
		select @SQL = 'alter event session '+quotename(maint.fn_GetErrorTraceName())+' on server state = start'
		exec(@SQL)
	end
end