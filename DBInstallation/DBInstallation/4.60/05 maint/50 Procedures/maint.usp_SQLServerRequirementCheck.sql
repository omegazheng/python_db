set ansi_nulls, quoted_identifier on 
go
create or alter procedure maint.usp_SQLServerRequirementCheck
as
begin
	set nocount on
	declare @SQL nvarchar(max), @FunctionName nvarchar(max)

	declare c cursor static for 
		select  'maint.' + name
		from sys.objects
		where schema_id = schema_id('maint')
			and type = 'FN'
			and name like 'fn!_CheckSystem%' escape '!'
		order by 1
	open c 
	fetch next from c into @FunctionName
	while @@fetch_status = 0
	begin
		select @SQL = 'select @SQL = '+@FunctionName+'()'
		exec sp_executesql @SQL, N'@SQL varchar(max) output', @SQL output
		--print '--Running ' + @FunctionName
		if @SQL is not null
			print @SQL
		fetch next from c into @FunctionName
	end
	close c
	deallocate c
end
go

--exec maint.usp_SQLServerRequirementCheck