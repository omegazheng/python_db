SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('admin_all.usp_CalculateAccountTranHourlyAggregateForOne') is null
	exec('create procedure admin_all.usp_CalculateAccountTranHourlyAggregateForOne as --')
go
alter procedure admin_all.usp_CalculateAccountTranHourlyAggregateForOne
(
	@Datetime datetime
)
as 
begin
	set nocount, xact_abort on 
	begin transaction
	declare @Currency nvarchar(10), @IsMultiCurrencyEnabled bit
	select @IsMultiCurrencyEnabled = admin_all.fn_IsMultiCurrencyEnabled()
	if  @IsMultiCurrencyEnabled = 1
	begin
		create table #Currencies (Currency nvarchar(10) primary key, AggregateType tinyint not null unique)
		select @Currency = admin_all.fn_GetPrimaryCurrency() 
		if @Currency is not null
			insert into #Currencies values(@Currency , 1)
		select @Currency = admin_all.fn_GetSecondaryCurrency() 
		if @Currency is not null
			insert into #Currencies values(@Currency , 2)
	end
	insert into admin_all.AccountTranAggregatePendingCalculation(Period, Date)
			values	('D', @Datetime), 
					('M', datefromparts (year(@Datetime), month(@Datetime), 1)),
					('Y', datefromparts (year(@Datetime), 1, 1))
	if @IsMultiCurrencyEnabled = 0
	begin
		;with s as
		(
			select	cast(0 as tinyint) as AggregateType,  aa.PARTYID as PartyID, u.BRANDID, u.CURRENCY Currency, a.TRAN_TYPE as TranType, isnull(a.PLATFORM_ID, -1) as ProductID, isnull(a.GAME_ID, '-UNKNOWN-') as GameID, 
					sum(a.AMOUNT_REAL) as AmountReal, sum(a.AMOUNT_RELEASED_BONUS) as AmountReleasedBonus, sum(a.AMOUNT_PLAYABLE_BONUS) as AmountPlayableBonus,
					count(*) as TranCount, count(distinct case when a.TRAN_TYPE = 'GAME_BET' then cast(isnull(a.PLATFORM_ID, -1) as varchar(120)) + '-' + isnull(a.GAME_TRAN_ID, '') end) GameCount,
					sum(a.AMOUNT_RAW_LOYALTY) as RawLoyalty, 
					sum(bat.AmountPlayableBonus) BonusPlanTran_AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) BonusPlanTran_AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) BonusPlanTran_AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) BonusPlanTran_AmountReleasedBonusWinnings
			from admin_all.ACCOUNT_TRAN a with(forceseek)
				inner join admin_all.ACCOUNT aa on aa.id = a.ACCOUNT_ID
				inner join external_mpt.USER_CONF u on u.PARTYID = aa.PARTYID
				outer apply (select sum(bat.AmountPlayableBonus) AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) AmountReleasedBonusWinnings from BonusPlan.BonusAccountTran bat where bat.AccountTranId = a.ID) bat
			where a.DATETIME >=@Datetime
				and a.DATETIME < dateadd(hour, 1, @Datetime)
				and a.ROLLED_BACK = 0
			group by aa.PARTYID, a.TRAN_TYPE, a.PLATFORM_ID, a.GAME_ID,u.BRANDID, u.CURRENCY
		),
		t as(
			select * from admin_all.AccountTranHourlyAggregate where Datetime = @Datetime and AggregateType = cast(0 as tinyint)
		)
		merge t
		using s on t.AggregateType = s.AggregateType and t.PartyID = s.PartyID and t.TranType = s.TranType and t.ProductID = s.ProductID and t.GameID = s.GameID
		when not matched then
			insert (AggregateType, Datetime, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, TranCount, GameCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)
				values(0, @Datetime, s.BrandID, s.PartyID, s.Currency, s.TranType, s.ProductID, s.GameID, s.AmountReal, s.AmountReleasedBonus, s.AmountPlayableBonus, s.TranCount, s.GameCount, s.BonusPlanTran_AmountPlayableBonus, s.BonusPlanTran_AmountPlayableBonusWinnings, s.BonusPlanTran_AmountReleasedBonus, s.BonusPlanTran_AmountReleasedBonusWinnings, s.RawLoyalty)
		when matched and (s.Currency <> t.Currency or s.AmountReal <> t.AmountReal or s.AmountReleasedBonus <> t.AmountReleasedBonus or s.AmountPlayableBonus <> t.AmountPlayableBonus  or s.TranCount <> t.TranCount or s.GameCount <> t.GameCount or isnull(s.BonusPlanTran_AmountPlayableBonus, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonus,0) or isnull(s.BonusPlanTran_AmountPlayableBonusWinnings, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonusWinnings, 0) or isnull(s.BonusPlanTran_AmountReleasedBonus, 0) <> isnull(t.BonusPlanTran_AmountReleasedBonus, 0) or isnull(s.BonusPlanTran_AmountReleasedBonusWinnings, 0)<> isnull(t.BonusPlanTran_AmountReleasedBonusWinnings, 0) or isnull(s.RawLoyalty,0) <> isnull(t.RawLoyalty, 0)) then 
			update set t.Currency = s.Currency, t.AmountReal = s.AmountReal, t.AmountReleasedBonus = s.AmountReleasedBonus, t.AmountPlayableBonus = s.AmountPlayableBonus, t.TranCount = s.TranCount, t.GameCount = s.GameCount, t.BonusPlanTran_AmountPlayableBonus = s.BonusPlanTran_AmountPlayableBonus, t.BonusPlanTran_AmountPlayableBonusWinnings = s.BonusPlanTran_AmountPlayableBonusWinnings, t.BonusPlanTran_AmountReleasedBonus = s.BonusPlanTran_AmountReleasedBonus, t.BonusPlanTran_AmountReleasedBonusWinnings = s.BonusPlanTran_AmountReleasedBonusWinnings, t.RawLoyalty = s.RawLoyalty
		when not matched by source then 
			delete
		;
	end
	else
	begin
		--regular
		;with s0 as
		(
			select	a.ACCOUNT_ID, a.TRAN_TYPE as TranType, isnull(a.PLATFORM_ID, -1) as ProductID, isnull(a.GAME_ID, '-UNKNOWN-') as GameID, 
					sum(a.AMOUNT_REAL) as AmountReal, sum(a.AMOUNT_RELEASED_BONUS) as AmountReleasedBonus, sum(a.AMOUNT_PLAYABLE_BONUS) as AmountPlayableBonus,
					count(*) as TranCount, count(distinct case when a.TRAN_TYPE = 'GAME_BET' then cast(isnull(a.PLATFORM_ID, -1) as varchar(120)) + '-' + isnull(a.GAME_TRAN_ID, '') end) GameCount,
					sum(a.AMOUNT_RAW_LOYALTY) as RawLoyalty, 
					sum(bat.AmountPlayableBonus) BonusPlanTran_AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) BonusPlanTran_AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) BonusPlanTran_AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) BonusPlanTran_AmountReleasedBonusWinnings
			from admin_all.ACCOUNT_TRAN a with(forceseek)
				outer apply (select sum(bat.AmountPlayableBonus) AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) AmountReleasedBonusWinnings from BonusPlan.BonusAccountTran bat where bat.AccountTranId = a.ID) bat
			where a.DATETIME >=@Datetime
				and a.DATETIME < dateadd(hour, 1, @Datetime)
				and a.ROLLED_BACK = 0
			group by a.ACCOUNT_ID, a.TRAN_TYPE, a.PLATFORM_ID, a.GAME_ID
		), s as
		(
			select	cast(0 as tinyint) as AggregateType, s0.TranType, s0.ProductID, s0.GameID, s0.AmountReal, s0.AmountPlayableBonus, s0.AmountReleasedBonus, s0.TranCount, s0.GameCount,
					uc.PartyID, uc.AssociatedCurrency Currency, uc.BrandID,
					s0.RawLoyalty, s0.BonusPlanTran_AmountPlayableBonus, s0.BonusPlanTran_AmountPlayableBonusWinnings, s0.BonusPlanTran_AmountReleasedBonus, s0.BonusPlanTran_AmountReleasedBonusWinnings
			from s0
				inner join external_mpt.v_UserPrimaryCurrency uc on uc.AssociatedAccountID = s0.ACCOUNT_ID
		),
		t as(
			select * from admin_all.AccountTranHourlyAggregate where Datetime = @Datetime and AggregateType = cast(0 as tinyint)
		)
		merge t
		using s on t.AggregateType = s.AggregateType and t.PartyID = s.PartyID and t.TranType = s.TranType and t.ProductID = s.ProductID and t.GameID = s.GameID
		when not matched then
			insert (AggregateType, Datetime, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, TranCount, GameCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)
				values(0, @Datetime, s.BrandID, s.PartyID, s.Currency, s.TranType, s.ProductID, s.GameID, s.AmountReal, s.AmountReleasedBonus, s.AmountPlayableBonus, s.TranCount, s.GameCount, s.BonusPlanTran_AmountPlayableBonus, s.BonusPlanTran_AmountPlayableBonusWinnings, s.BonusPlanTran_AmountReleasedBonus, s.BonusPlanTran_AmountReleasedBonusWinnings, s.RawLoyalty)
		when matched and (s.Currency <> t.Currency or s.AmountReal <> t.AmountReal or s.AmountReleasedBonus <> t.AmountReleasedBonus or s.AmountPlayableBonus <> t.AmountPlayableBonus  or s.TranCount <> t.TranCount or s.GameCount <> t.GameCount or isnull(s.BonusPlanTran_AmountPlayableBonus, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonus,0) or isnull(s.BonusPlanTran_AmountPlayableBonusWinnings, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonusWinnings, 0) or isnull(s.BonusPlanTran_AmountReleasedBonus, 0) <> isnull(t.BonusPlanTran_AmountReleasedBonus, 0) or isnull(s.BonusPlanTran_AmountReleasedBonusWinnings, 0)<> isnull(t.BonusPlanTran_AmountReleasedBonusWinnings, 0) or isnull(s.RawLoyalty,0) <> isnull(t.RawLoyalty, 0)) then 
			update set t.Currency = s.Currency, t.AmountReal = s.AmountReal, t.AmountReleasedBonus = s.AmountReleasedBonus, t.AmountPlayableBonus = s.AmountPlayableBonus, t.TranCount = s.TranCount, t.GameCount = s.GameCount, t.BonusPlanTran_AmountPlayableBonus = s.BonusPlanTran_AmountPlayableBonus, t.BonusPlanTran_AmountPlayableBonusWinnings = s.BonusPlanTran_AmountPlayableBonusWinnings, t.BonusPlanTran_AmountReleasedBonus = s.BonusPlanTran_AmountReleasedBonus, t.BonusPlanTran_AmountReleasedBonusWinnings = s.BonusPlanTran_AmountReleasedBonusWinnings, t.RawLoyalty = s.RawLoyalty
		when not matched by source then 
			delete
		;
		--primary and secondary
		;with s0 as
		(
			select	c.AggregateType, c.Currency, a.ACCOUNT_ID, a.TRAN_TYPE as TranType, isnull(a.PLATFORM_ID, -1) as ProductID, isnull(a.GAME_ID, '-UNKNOWN-') as GameID, 
					sum(ace.AmountReal) as AmountReal, sum(ace.ReleasedBonus) as AmountReleasedBonus, sum(ace.PlayableBonus) as AmountPlayableBonus,
					count(*) as TranCount, count(distinct case when a.TRAN_TYPE = 'GAME_BET' then cast(isnull(a.PLATFORM_ID, -1) as varchar(120)) + '-' + isnull(a.GAME_TRAN_ID, '') end) GameCount,
					sum(a.AMOUNT_RAW_LOYALTY) as RawLoyalty, 
					sum(bat.AmountPlayableBonus) BonusPlanTran_AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) BonusPlanTran_AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) BonusPlanTran_AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) BonusPlanTran_AmountReleasedBonusWinnings
			from admin_all.ACCOUNT_TRAN a with(forceseek)
				inner join admin_all.AccountTranCurrencyExtension ace on ace.AccountTranID = a.ID
				inner join #Currencies c on c.Currency = ace.Currency
				outer apply (select sum(bat.AmountPlayableBonus) AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) AmountReleasedBonusWinnings from BonusPlan.BonusAccountTran bat where bat.AccountTranId = a.ID) bat
			where a.DATETIME >=@Datetime
				and a.DATETIME < dateadd(hour, 1, @Datetime)
				and a.ROLLED_BACK = 0
			group by c.AggregateType, c.Currency, a.ACCOUNT_ID, a.TRAN_TYPE, a.PLATFORM_ID, a.GAME_ID
		), 
		s as
		(
			select	s0.AggregateType, s0.Currency, s0.TranType, s0.ProductID, s0.GameID, s0.AmountReal, s0.AmountPlayableBonus, s0.AmountReleasedBonus, s0.TranCount, s0.GameCount,
					uc.PartyID, uc.BrandID,
					s0.RawLoyalty, s0.BonusPlanTran_AmountPlayableBonus, s0.BonusPlanTran_AmountPlayableBonusWinnings, s0.BonusPlanTran_AmountReleasedBonus, s0.BonusPlanTran_AmountReleasedBonusWinnings
			from s0
				inner join external_mpt.v_UserPrimaryCurrency uc on uc.AssociatedAccountID = s0.ACCOUNT_ID
		),
		t as(
			select * from admin_all.AccountTranHourlyAggregate where Datetime = @Datetime and AggregateType in(1,2)
		)
		merge t
		using s on t.AggregateType = s.AggregateType and t.PartyID = s.PartyID and t.TranType = s.TranType and t.ProductID = s.ProductID and t.GameID = s.GameID
		when not matched then
			insert (AggregateType, Datetime, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, TranCount, GameCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)
				values(s.AggregateType, @Datetime, s.BrandID, s.PartyID, s.Currency, s.TranType, s.ProductID, s.GameID, s.AmountReal, s.AmountReleasedBonus, s.AmountPlayableBonus, s.TranCount, s.GameCount, s.BonusPlanTran_AmountPlayableBonus, s.BonusPlanTran_AmountPlayableBonusWinnings, s.BonusPlanTran_AmountReleasedBonus, s.BonusPlanTran_AmountReleasedBonusWinnings, s.RawLoyalty)
		when matched and (s.Currency <> t.Currency or s.AmountReal <> t.AmountReal or s.AmountReleasedBonus <> t.AmountReleasedBonus or s.AmountPlayableBonus <> t.AmountPlayableBonus  or s.TranCount <> t.TranCount or s.GameCount <> t.GameCount or isnull(s.BonusPlanTran_AmountPlayableBonus, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonus,0) or isnull(s.BonusPlanTran_AmountPlayableBonusWinnings, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonusWinnings, 0) or isnull(s.BonusPlanTran_AmountReleasedBonus, 0) <> isnull(t.BonusPlanTran_AmountReleasedBonus, 0) or isnull(s.BonusPlanTran_AmountReleasedBonusWinnings, 0)<> isnull(t.BonusPlanTran_AmountReleasedBonusWinnings, 0) or isnull(s.RawLoyalty,0) <> isnull(t.RawLoyalty, 0)) then 
			update set t.Currency = s.Currency, t.AmountReal = s.AmountReal, t.AmountReleasedBonus = s.AmountReleasedBonus, t.AmountPlayableBonus = s.AmountPlayableBonus, t.TranCount = s.TranCount, t.GameCount = s.GameCount, t.BonusPlanTran_AmountPlayableBonus = s.BonusPlanTran_AmountPlayableBonus, t.BonusPlanTran_AmountPlayableBonusWinnings = s.BonusPlanTran_AmountPlayableBonusWinnings, t.BonusPlanTran_AmountReleasedBonus = s.BonusPlanTran_AmountReleasedBonus, t.BonusPlanTran_AmountReleasedBonusWinnings = s.BonusPlanTran_AmountReleasedBonusWinnings, t.RawLoyalty = s.RawLoyalty
		when not matched by source then 
			delete
		;
		--User Primary
		;with s as
		(
			select	cast(3 as tinyint) AggregateType, a.TRAN_TYPE as TranType, isnull(a.PLATFORM_ID, -1) as ProductID, isnull(a.GAME_ID, '-UNKNOWN-') as GameID, 
					uc.PartyID, uc.Currency, uc.BrandID,
					sum(ace.AmountReal) as AmountReal, sum(ace.ReleasedBonus) as AmountReleasedBonus, sum(ace.PlayableBonus) as AmountPlayableBonus,
					count(*) as TranCount, count(distinct case when a.TRAN_TYPE = 'GAME_BET' then cast(isnull(a.PLATFORM_ID, -1) as varchar(120)) + '-' + isnull(a.GAME_TRAN_ID, '') end) GameCount,
					sum(a.AMOUNT_RAW_LOYALTY) as RawLoyalty, 
					sum(bat.AmountPlayableBonus) BonusPlanTran_AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) BonusPlanTran_AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) BonusPlanTran_AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) BonusPlanTran_AmountReleasedBonusWinnings
			from admin_all.ACCOUNT_TRAN a with(forceseek)
				inner join external_mpt.v_UserPrimaryCurrency uc with(noexpand) on uc.AssociatedAccountID = a.ACCOUNT_ID
				inner join admin_all.AccountTranCurrencyExtension ace on ace.AccountTranID = a.ID and uc.Currency = ace.Currency
				outer apply (select sum(bat.AmountPlayableBonus) AmountPlayableBonus, sum(bat.AmountPlayableBonusWinnings) AmountPlayableBonusWinnings, sum(bat.AmountReleasedBonus) AmountReleasedBonus, sum(bat.AmountReleasedBonusWinnings) AmountReleasedBonusWinnings from BonusPlan.BonusAccountTran bat where bat.AccountTranId = a.ID) bat
			where a.DATETIME >=@Datetime
				and a.DATETIME < dateadd(hour, 1, @Datetime)
				and a.ROLLED_BACK = 0
			group by a.TRAN_TYPE, a.PLATFORM_ID, a.GAME_ID, uc.PartyID, uc.Currency, uc.AssociatedPartyID, uc.AssociatedCurrency, uc.BrandID
		),
		t as(
			select * from admin_all.AccountTranHourlyAggregate where Datetime = @Datetime and AggregateType = cast(3 as tinyint)
		)
		merge t
		using s on t.AggregateType = s.AggregateType and t.PartyID = s.PartyID and t.TranType = s.TranType and t.ProductID = s.ProductID and t.GameID = s.GameID
		when not matched then
			insert (AggregateType, Datetime, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, TranCount, GameCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)
				values(s.AggregateType, @Datetime, s.BrandID, s.PartyID, s.Currency, s.TranType, s.ProductID, s.GameID, s.AmountReal, s.AmountReleasedBonus, s.AmountPlayableBonus, s.TranCount, s.GameCount, s.BonusPlanTran_AmountPlayableBonus, s.BonusPlanTran_AmountPlayableBonusWinnings, s.BonusPlanTran_AmountReleasedBonus, s.BonusPlanTran_AmountReleasedBonusWinnings, s.RawLoyalty)
		when matched and (s.Currency <> t.Currency or s.AmountReal <> t.AmountReal or s.AmountReleasedBonus <> t.AmountReleasedBonus or s.AmountPlayableBonus <> t.AmountPlayableBonus  or s.TranCount <> t.TranCount or s.GameCount <> t.GameCount or isnull(s.BonusPlanTran_AmountPlayableBonus, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonus,0) or isnull(s.BonusPlanTran_AmountPlayableBonusWinnings, 0) <> isnull(t.BonusPlanTran_AmountPlayableBonusWinnings, 0) or isnull(s.BonusPlanTran_AmountReleasedBonus, 0) <> isnull(t.BonusPlanTran_AmountReleasedBonus, 0) or isnull(s.BonusPlanTran_AmountReleasedBonusWinnings, 0)<> isnull(t.BonusPlanTran_AmountReleasedBonusWinnings, 0) or isnull(s.RawLoyalty,0) <> isnull(t.RawLoyalty, 0)) then 
			update set t.Currency = s.Currency, t.AmountReal = s.AmountReal, t.AmountReleasedBonus = s.AmountReleasedBonus, t.AmountPlayableBonus = s.AmountPlayableBonus, t.TranCount = s.TranCount, t.GameCount = s.GameCount, t.BonusPlanTran_AmountPlayableBonus = s.BonusPlanTran_AmountPlayableBonus, t.BonusPlanTran_AmountPlayableBonusWinnings = s.BonusPlanTran_AmountPlayableBonusWinnings, t.BonusPlanTran_AmountReleasedBonus = s.BonusPlanTran_AmountReleasedBonus, t.BonusPlanTran_AmountReleasedBonusWinnings = s.BonusPlanTran_AmountReleasedBonusWinnings, t.RawLoyalty = s.RawLoyalty
		when not matched by source then 
			delete
		;
	end
	commit
end

