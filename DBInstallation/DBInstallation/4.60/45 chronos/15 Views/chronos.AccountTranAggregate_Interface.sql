set ansi_nulls, quoted_identifier on
go
if object_id('[chronos].[AccountTranAggregate_Interface]') is null
	exec('create view [chronos].[AccountTranAggregate_Interface] as select 1 as one')
go

alter view [chronos].[AccountTranAggregate_Interface] as select cast([Period] as char(1)) as [Period],cast([AggregateType] as tinyint) as [AggregateType],cast([Date] as date) as [Date],cast([BrandID] as int) as [BrandID],cast([PartyID] as int) as [PartyID],cast([Currency] as nchar(6)) as [Currency],cast([TranType] as varchar(10)) as [TranType],cast([ProductID] as int) as [ProductID],cast([GameID] as varchar(100)) as [GameID],cast([AmountReal] as numeric(38,18)) as [AmountReal],cast([AmountReleasedBonus] as numeric(38,18)) as [AmountReleasedBonus],cast([AmountPlayableBonus] as numeric(38,18)) as [AmountPlayableBonus],cast([GameCount] as int) as [GameCount],cast([TranCount] as int) as [TranCount],  ___IsDeleted___ from [chronos].[AccountTranAggregate]
GO


