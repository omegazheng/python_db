set ansi_nulls, quoted_identifier on
go
create or alter view [chronos].[Party_Interface] 
as 
	select cast([PartyID] as int) as [PartyID],cast([UserID] as varchar(100)) as [UserID],cast([Nickname] as varchar(100)) as [Nickname],cast([Email] as varchar(100)) as [Email],cast([Title] as varchar(100)) as [Title],cast([FirstName] as varchar(100)) as [FirstName],cast([LastName] as varchar(100)) as [LastName],cast([Gender] as varchar(20)) as [Gender],cast([BirthDate] as datetime) as [BirthDate],cast([Address] as varchar(512)) as [Address],cast([City] as varchar(100)) as [City],cast([Province] as varchar(100)) as [Province],cast([Country] as varchar(100)) as [Country],cast([Mobile] as varchar(100)) as [Mobile],cast([Language] as varchar(100)) as [Language],cast([Currency] as varchar(100)) as [Currency],cast([RegistrationDate] as datetime) as [RegistrationDate],cast([VIPStatus] as int) as [VIPStatus],cast([Hierarchy] as hierarchyid) as [Hierarchy],cast([UserType] as int) as [UserType], ___IsDeleted___ 
	from [chronos].[Party]
go

