
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
create or alter view chronos.UserTag_Interface
as 
	select cast(Code as varchar(10)) as Code,cast(PartyID as int) as PartyID, cast(Name as varchar(255)) Name,  ___IsDeleted___ from chronos.UserTag
GO


