set ansi_nulls, quoted_identifier on 
go
create or alter trigger chronos.TRI_Event on chronos.Event
for insert
as
begin
	if @@rowcount = 0
		return
	set nocount on
		
	insert into chronos.EventData(EventID, Name, Value1, Date)
		select i.EventID, 'AccountBalanceReal', a.BALANCE_REAL, i.Date
		from inserted i
			inner join admin_all.ACCOUNT a on i.PartyID = a.PARTYID	
		-- where a.BALANCE_REAL <> 0
	insert into chronos.EventData(EventID, Name, Value1, Date)
		select i.EventID, 'AccountBalanceReleasedBonus', a.RELEASED_BONUS, i.Date
		from inserted i
			inner join admin_all.ACCOUNT a on i.PartyID = a.PARTYID	
		-- where a.RELEASED_BONUS <> 0
	insert into chronos.EventData(EventID, Name, Value1, Date)
		select i.EventID, 'AccountBalancePlayableBonus', a.PLAYABLE_BONUS, i.Date
		from inserted i
			inner join admin_all.ACCOUNT a on i.PartyID = a.PARTYID	
		-- where a.PLAYABLE_BONUS <> 0
end
