set ansi_nulls, quoted_identifier on
go
create or alter trigger admin_all.TRI_BONUS_ForChronos on admin_all.BONUS 
for insert, delete, update 
as 
begin
	if @@rowcount = 0
		return
		
	if chronos.fn_IsEnabled() = 0
		return
	set nocount on
	declare @OldStatus varchar(10), @NewStatus varchar(10),
			@PartyID int, @EventID bigint, @TriggerType varchar(25),
			@ReleasedBonus numeric(38, 18), @ReleasedBonusWinnings numeric(38, 18), @PlayableBonus numeric(38, 18), @PlayableBonusWinnings numeric(38, 18),
			@BonusID int, @AmountWagered numeric(38, 18), @WagerRequirement numeric(38,18), @BonusCode varchar(50), 
			@TriggerDate datetime
		

	declare c cursor fast_forward local for
		select isnull(i.PARTYID, d.PARTYID), i.STATUS, d.STATUS, bp.TRIGGER_TYPE, i.RELEASED_BONUS, i.RELEASED_BONUS_WINNINGS, isnull(i.ID, d.ID), isnull(i.PLAYABLE_BONUS, 0), isnull(i.PLAYABLE_BONUS_WINNINGS, 0),
			isnull(i.AMOUNT_WAGERED, 0), isnull(i.WAGER_REQUIREMENT, 0), bp.BONUS_CODE, i.TRIGGER_DATE
		from inserted i
			full outer join deleted d on i.ID = d.ID
			left join admin_all.BONUS_PLAN bp on bp.ID = i.BONUS_PLAN_ID
		where isnull(i.STATUS, '') <> isnull(d.STATUS, '')
	open c
	fetch next from c into @PartyID, @NewStatus, @OldStatus, @TriggerType, @ReleasedBonus,@ReleasedBonusWinnings, @BonusID, @PlayableBonus, @PlayableBonusWinnings, @AmountWagered, @WagerRequirement, @BonusCode, @TriggerDate
	while @@fetch_status = 0
	begin
		select @EventID = null

		if @NewStatus in ('ACTIVE','QUEUED')
		begin
			insert into chronos.Event(Name, PartyID) values('BonusAward', @PartyID); select @EventID = scope_identity();
		end
		else if @NewStatus = 'RELEASED'
		begin
			insert into chronos.Event(Name, PartyID) values('BonusRelease', @PartyID); select @EventID = scope_identity();
		end
		else if @NewStatus = 'QUALIFIED'
		begin
			insert into chronos.Event(Name, PartyID) values('BonusQualified', @PartyID); select @EventID = scope_identity();
		end
		else if @NewStatus = 'EXPIRED'
		begin
			insert into chronos.Event(Name, PartyID) values('BonusExpired', @PartyID); select @EventID = scope_identity();
		end
		else if @NewStatus = 'SPENT'
		begin
			insert into chronos.Event(Name, PartyID) values('BonusSpent', @PartyID); select @EventID = scope_identity();
		end
		else if @NewStatus = 'CANCELED'
		begin
			insert into chronos.Event(Name, PartyID) values('BonusCanceled', @PartyID); select @EventID = scope_identity();
		end
		if @EventID is null
			goto ___Next___;

		if isnull(@TriggerType, '')<>'' insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'Type', @TriggerType);
		if isnull(@BonusCode, '')<>'' insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'Code', @BonusCode);
		if @TriggerDate is not null insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'DateCreated', @TriggerDate);
		
		if isnull(@ReleasedBonus + @ReleasedBonusWinnings, 0) <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'ReleasedBonus', @ReleasedBonus + @ReleasedBonusWinnings);
		if isnull(@PlayableBonus + @PlayableBonusWinnings, 0) <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'PlayableBonus', @PlayableBonus + @PlayableBonusWinnings);
		if isnull(@AmountWagered + @WagerRequirement, 0) <> 0 insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'WageringAmount', @AmountWagered + @WagerRequirement);

		insert into chronos.EventData(EventID, Name, Value1) values(@EventID, 'BonusID', @BonusID);
___Next___:
		fetch next from c into @PartyID, @NewStatus, @OldStatus, @TriggerType, @ReleasedBonus,@ReleasedBonusWinnings, @BonusID, @PlayableBonus, @PlayableBonusWinnings, @AmountWagered, @WagerRequirement, @BonusCode, @TriggerDate
	end
	close c
	deallocate c
end