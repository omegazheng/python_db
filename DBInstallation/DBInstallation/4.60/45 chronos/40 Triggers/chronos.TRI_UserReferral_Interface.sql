
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
if object_id('[chronos].[TRI_UserReferral_Interface]') is null
	exec('create trigger [chronos].[TRI_UserReferral_Interface] on [chronos].[UserReferral_Interface]
instead of insert
as
begin
	return
end')
go
alter trigger [chronos].[TRI_UserReferral_Interface] on [chronos].[UserReferral_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [chronos].[UserReferral] t
	using inserted s on s.[UserReferralID] = t.[UserReferralID]
	when not matched then
		insert ([UserReferralID],[PartyID],[ReferredPartyID], ___IsDeleted___)
			values (s.[UserReferralID],s.[PartyID],s.[ReferredPartyID], isnull(s.___IsDeleted___, 0))
	when matched and (isnull(s.___IsDeleted___, 0) <> t.___IsDeleted___ or s.[PartyID] is null and t.[PartyID] is not null or s.[PartyID] is not null and t.[PartyID] is null or s.[PartyID] <> t.[PartyID] or s.[ReferredPartyID] is null and t.[ReferredPartyID] is not null or s.[ReferredPartyID] is not null and t.[ReferredPartyID] is null or s.[ReferredPartyID] <> t.[ReferredPartyID]) then
		update set	t.[PartyID] = case isnull(s.___IsDeleted___, 0) when 0 then s.[PartyID] else t.[PartyID] end,
					t.[ReferredPartyID] = case isnull(s.___IsDeleted___, 0) when 0 then s.[ReferredPartyID] else t.[ReferredPartyID] end,
					t.___IsDeleted___ = isnull(s.___IsDeleted___, 0)
	;
	
end
GO

