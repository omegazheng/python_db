
set ansi_nulls, quoted_identifier on
if object_id('admin_all.TRI_STAFF_AUTH_TBL_ForChronos') is null
	exec('create trigger admin_all.TRI_STAFF_AUTH_TBL_ForChronos on admin_all.STAFF_AUTH_TBL for insert as return')
go
alter trigger admin_all.TRI_STAFF_AUTH_TBL_ForChronos on admin_all.STAFF_AUTH_TBL
for insert, update
as 
begin
	if @@rowcount = 0
		return
	if chronos.fn_IsEnabled() = 0
		return
	declare @PartyID int, @SessionKey nvarchar(100), @LoginType varchar(20), @LoginTime datetime, @Date datetime = getdate(), 
			@EventID bigint, @IP varchar(100), @Country char(2), @Attempts int

	declare c cursor local fast_forward for
		select -isnull(i.STAFFID, d.STAFFID), i.COOKIE, case when i.COOKIE is null or i.COOKIE is null and d.COOKIE is null and isnull(i.LOGIN_ATTEMPTS, 0) > 0 then 'LOGIN' else 'LOGOUT' end LOGIN_TYPE, getdate() login_time, null ip, i.Country, i.LOGIN_ATTEMPTS
		from inserted i
			full outer join deleted d on i.STAFFID = d.STAFFID
		where (
					isnull(i.COOKIE, '') <> isnull(d.COOKIE, '')
				and (i.COOKIE is null or d.COOKIE is null)
			) or isnull(i.LOGIN_ATTEMPTS, 0)<>isnull(d.LOGIN_ATTEMPTS, 0)
			or i.COOKIE is null and d.COOKIE is null and isnull(i.LOGIN_ATTEMPTS, 0) > 0
	open c
	fetch next from c into @PartyID, @SessionKey, @LoginType, @LoginTime, @IP, @Country, @Attempts
	while @@fetch_status = 0
	begin
		select @EventID = null
		if @LoginType = 'LOGIN' and @SessionKey is not null
		begin
			insert into chronos.Event(Name, PartyID, Date)
				values('Login', @PartyID, @Date)
			select @EventID = scope_identity()
			insert into chronos.EventData(EventID, Name, Value1, Value2, Date) 
					values(@EventID, 'LoginTime', @LoginTime, null, @Date)

			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID,  'LoginDateTimeForSessionDuration', 1, @Date

			insert into chronos.EventData(EventID, Name, Value1, Value2, Date) 
					select @EventID, 'DaysSinceLastLogin', datediff(day, Date, @Date), null, @Date
					from chronos.UserSessionStaging
					where PartyID = @PartyID
						and Name = 'LogoutDate'
						and datediff(day, Date, @Date) > 0
		end
		else if @LoginType = 'LOGIN' and @SessionKey is null
		begin
			insert into chronos.Event(Name, PartyID, Date)
				values('LoginFailed', @PartyID, @Date)
			select @EventID = scope_identity()
		end 
		else if @LoginType in('LOGOUT', 'TIMEOUT')
		begin
			insert into chronos.Event(Name, PartyID, Date)
				values('Logout', @PartyID, @Date)
			select @EventID = scope_identity()

			insert into chronos.EventData(EventID, Name, Value1, Date)
					select @EventID, 'SessionDurationInSec', datediff(second, Date, @Date), @Date
					from chronos.UserSessionStaging
					where PartyID = @PartyID
						and Name = 'LoginDateTimeForSessionDuration'
						and datediff(second, Date, @Date) > 0

				insert into chronos.EventData(EventID, Name, Value1, Value2, Date)
					select @EventID, x.Name, x.Value, null, x.Date
					from (
							delete s
								output deleted.Name, deleted.Value, deleted.Date
							from chronos.UserSessionStaging s
							where s.PartyID = @PartyID
						) x
					where x.Name not in ('LoginDateTimeForSessionDuration')

			insert into chronos.EventData(EventID, Name, Value1, Value2, Date) 
				values(@EventID, 'LogoutTime', @LoginTime, null, @Date)
			exec chronos.usp_UpdateUserSessionStaging 1, @PartyID,  'LogoutDate', 1, @Date
		end
		if @EventID is not null
		begin
			if @IP is not null insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'IP', @IP, null, @Date)
			if @SessionKey is not null insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'SessionKey', @SessionKey, null, @Date)
			if @Attempts > 0 insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'LoginAttempts', @Attempts, null, @Date)
			if @Country is not null insert into chronos.EventData(EventID, Name, Value1, Value2, Date) values(@EventID, 'Country', @Country, null, @Date)
		end
		fetch next from c into @PartyID, @SessionKey, @LoginType, @LoginTime, @IP, @Country, @Attempts
	end	
	close c
	deallocate c
end
go


--set xact_abort on
--begin transaction
--select * from admin_all.STAFF_AUTH_TBL where STAFFID = 7314
--update admin_all.STAFF_AUTH_TBL set cookie = 'aaa' where STAFFID = 7314

--select * from chronos.Event where partyid < 0 order by 1 desc
--select top 100 *  from chronos.EventData order by 1 desc
--update admin_all.STAFF_AUTH_TBL set cookie = 'bbb' where STAFFID = 7314

--select * from chronos.Event where partyid < 0 order by 1 desc
--select top 100 *  from chronos.EventData order by 1 desc

--update admin_all.STAFF_AUTH_TBL set cookie = null  where STAFFID = 7314

--select * from chronos.Event where partyid < 0 order by 1 desc
--select top 100 *  from chronos.EventData order by 1 desc

--rollback