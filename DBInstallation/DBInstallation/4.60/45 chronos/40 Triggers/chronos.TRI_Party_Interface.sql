set ansi_nulls, quoted_identifier on
go
create or alter trigger [chronos].[TRI_Party_Interface] on [chronos].[Party_Interface]
instead of insert
as
begin
	if @@rowcount = 0
		return
	set nocount on;
	
	merge [chronos].[Party] t
	using inserted s on s.[PartyID] = t.[PartyID]
	when not matched then
		insert ([PartyID],[UserID],[Nickname],[Email],[Title],[FirstName],[LastName],[Gender],[BirthDate],[Address],[City],[Province],[Country],[Mobile],[Language],[Currency],[RegistrationDate],[VIPStatus],[Hierarchy],[UserType], [___IsDeleted___])
			values (s.[PartyID],s.[UserID],s.[Nickname],s.[Email],s.[Title],s.[FirstName],s.[LastName],s.[Gender],s.[BirthDate],s.[Address],s.[City],s.[Province],s.[Country],s.[Mobile],s.[Language],s.[Currency],s.[RegistrationDate],s.[VIPStatus],s.[Hierarchy],s.[UserType], isnull([___IsDeleted___], 0))

	when matched and (isnull(s.___IsDeleted___, 0) <> isnull(t.___IsDeleted___, 0) or  s.[UserID] is null and t.[UserID] is not null or s.[UserID] is not null and t.[UserID] is null or s.[UserID] <> t.[UserID] or s.[Nickname] is null and t.[Nickname] is not null or s.[Nickname] is not null and t.[Nickname] is null or s.[Nickname] <> t.[Nickname] or s.[Email] is null and t.[Email] is not null or s.[Email] is not null and t.[Email] is null or s.[Email] <> t.[Email] or s.[Title] is null and t.[Title] is not null or s.[Title] is not null and t.[Title] is null or s.[Title] <> t.[Title] or s.[FirstName] is null and t.[FirstName] is not null or s.[FirstName] is not null and t.[FirstName] is null or s.[FirstName] <> t.[FirstName] or s.[LastName] is null and t.[LastName] is not null or s.[LastName] is not null and t.[LastName] is null or s.[LastName] <> t.[LastName] or s.[Gender] is null and t.[Gender] is not null or s.[Gender] is not null and t.[Gender] is null or s.[Gender] <> t.[Gender] or s.[BirthDate] is null and t.[BirthDate] is not null or s.[BirthDate] is not null and t.[BirthDate] is null or s.[BirthDate] <> t.[BirthDate] or s.[Address] is null and t.[Address] is not null or s.[Address] is not null and t.[Address] is null or s.[Address] <> t.[Address] or s.[City] is null and t.[City] is not null or s.[City] is not null and t.[City] is null or s.[City] <> t.[City] or s.[Province] is null and t.[Province] is not null or s.[Province] is not null and t.[Province] is null or s.[Province] <> t.[Province] or s.[Country] is null and t.[Country] is not null or s.[Country] is not null and t.[Country] is null or s.[Country] <> t.[Country] or s.[Mobile] is null and t.[Mobile] is not null or s.[Mobile] is not null and t.[Mobile] is null or s.[Mobile] <> t.[Mobile] or s.[Language] is null and t.[Language] is not null or s.[Language] is not null and t.[Language] is null or s.[Language] <> t.[Language] or s.[Currency] is null and t.[Currency] is not null or s.[Currency] is not null and t.[Currency] is null or s.[Currency] <> t.[Currency] or s.[RegistrationDate] is null and t.[RegistrationDate] is not null or s.[RegistrationDate] is not null and t.[RegistrationDate] is null or s.[RegistrationDate] <> t.[RegistrationDate] or s.[VIPStatus] is null and t.[VIPStatus] is not null or s.[VIPStatus] is not null and t.[VIPStatus] is null or s.[VIPStatus] <> t.[VIPStatus] or s.[Hierarchy] is null and t.[Hierarchy] is not null or s.[Hierarchy] is not null and t.[Hierarchy] is null or s.[Hierarchy] <> t.[Hierarchy] or s.[UserType] is null and t.[UserType] is not null or s.[UserType] is not null and t.[UserType] is null or s.[UserType] <> t.[UserType]) then
		update set	t.[PartyID] = case isnull(s.___IsDeleted___, 0) when 0 then s.[PartyID] else t.[PartyID] end,
					t.[UserID] = case isnull(s.___IsDeleted___, 0) when 0 then s.[UserID] else t.[UserID] end,
					t.[Nickname] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Nickname] else t.[Nickname] end,
					t.[Email] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Email] else t.[Email] end,
					t.[Title] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Title] else t.[Title] end,
					t.[FirstName] = case isnull(s.___IsDeleted___, 0) when 0 then s.[FirstName] else t.[FirstName] end,
					t.[LastName] = case isnull(s.___IsDeleted___, 0) when 0 then s.[LastName] else t.[LastName] end,
					t.[Gender] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Gender] else t.[Gender] end,
					t.[BirthDate] = case isnull(s.___IsDeleted___, 0) when 0 then s.[BirthDate] else t.[BirthDate] end,
					t.[Address] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Address] else t.[Address] end,
					t.[City] = case isnull(s.___IsDeleted___, 0) when 0 then s.[City] else t.[City] end,
					t.[Province] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Province] else t.[Province] end,
					t.[Country] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Country] else t.[Country] end,
					t.[Mobile] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Mobile] else t.[Mobile] end,
					t.[Language] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Language] else t.[Language] end,
					t.[Currency] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Currency] else t.[Currency] end,
					t.[RegistrationDate] = case isnull(s.___IsDeleted___, 0) when 0 then s.[RegistrationDate] else t.[RegistrationDate] end,
					t.[VIPStatus] = case isnull(s.___IsDeleted___, 0) when 0 then s.[VIPStatus] else t.[VIPStatus] end,
					t.[Hierarchy] = case isnull(s.___IsDeleted___, 0) when 0 then s.[Hierarchy] else t.[Hierarchy] end,
					t.[UserType] = case isnull(s.___IsDeleted___, 0) when 0 then s.[UserType] else t.[UserType] end,
					t.___IsDeleted___ = isnull(s.___IsDeleted___, 0)
	;
end
GO



/*

select 't.['+name+'] = case isnull(s.___IsDeleted___, 0) when 0 then s.['+name+'] else t.['+name+'] end,'
from sys.columns
where object_id = object_id('[chronos].[Party_Interface]')
order by column_id


select * from maint.replicationconfiguration
*/