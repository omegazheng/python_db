set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_GetUserProfileDone') is null
	exec('create procedure chronos.usp_GetUserProfileDone as --')
go
alter procedure chronos.usp_GetUserProfileDone @ConsumerID uniqueidentifier, @TransactionID bigint
as
begin
	set nocount on 
	set xact_abort on
	set lock_timeout 0
	if current_transaction_id() <> isnull(@TransactionID, 0)
		throw 50000, 'chronos.usp_GetUserProfileDone must be run within the same transaction', 16;
	update a
		set FromVersion = ToVersion 
	from chronos.SynchronizationStatus a (rowlock)
	where Name = 'UserProfile'
		and ConsumerID = @ConsumerID
end
go
--delete chronos.SynchronizationStatus
--begin transaction
--declare @TransactionID bigint
--select @TransactionID = current_transaction_id()
--exec chronos.usp_GetUserProfile
--exec chronos.usp_GetUserProfileDone @TransactionID
--commit


--update Chronos.Party set ___IsDeleted___ = 0 where PartyID = 10000
--select * from Chronos.Party where PartyID = 10000
--select * from chronos.SynchronizationStatus
