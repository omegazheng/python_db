set ansi_nulls, quoted_identifier on
if object_id('chronos.usp_UpdateWinLossEventData') is null
begin
	exec('create procedure chronos.usp_UpdateWinLossEventData as --')
end
go
alter procedure chronos.usp_UpdateWinLossEventData
(
	@EventID bigint,
	@PartyID int
)
as
begin
	set nocount on
	insert into chronos.EventData(EventID, Name, Value1, Date)
		select @EventID, 'WinsInARow', WinsInARow, LastUpdateDate
		from chronos.PartyWinLoss
		where PartyID = @PartyID
			and WinsInARow > 0
	insert into chronos.EventData(EventID, Name, Value1, Date)
		select @EventID, 'WinsInARowAmountRR', WinsInARowAmountRR, LastUpdateDate
		from chronos.PartyWinLoss
		where PartyID = @PartyID
			and WinsInARowAmountRR <> 0
	insert into chronos.EventData(EventID, Name, Value1, Date)
		select @EventID, 'LossInARow', LossInARow, LastUpdateDate
		from chronos.PartyWinLoss
		where PartyID = @PartyID
			and LossInARow> 0
	insert into chronos.EventData(EventID, Name, Value1, Date)
		select @EventID, 'LossInARowAmountRR', LossInARowAmountRR, LastUpdateDate
		from chronos.PartyWinLoss
		where PartyID = @PartyID
			and LossInARowAmountRR<> 0
end;

