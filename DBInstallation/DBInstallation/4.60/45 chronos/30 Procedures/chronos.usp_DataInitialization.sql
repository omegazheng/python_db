set ansi_nulls, quoted_identifier on
go
if object_id('chronos.usp_DataInitialization') is null
	exec('create procedure chronos.usp_DataInitialization  as --')
go
alter procedure chronos.usp_DataInitialization
as
begin
	set nocount on 
	declare @ConnectionName varchar(128) = 'Chronos-Dummy'
	if not exists(select  * from maint.Connection where Name = @ConnectionName)
	begin 
		insert into maint.Connection(Name, Comment, ServerName, DatabaseName, UserName, Password, IsContextConnection, CurrentChangeTrackingVersion, CurrentRowVersion, AutoUpdateVersion)
			select @ConnectionName Name, @ConnectionName Comment, @ConnectionName ServerName, @ConnectionName DatabaseName, null UserName, null Password, 0 IsContextConnection, 0 CurrentChangeTrackingVersion, 0 CurrentRowVersion, 0 AutoUpdateVersion
	end
	declare @ConnectionID int
	select @ConnectionID = ConnectionID from maint.Connection where Name = @ConnectionName
	;with s as 
	(
		select [Name],[Comment],[SourceConnectionID],[SourceTableOrQuery],[Columns],[FilterType],[TargetConnectionID],[TargetSchemaName],[TargetObjectName],[BatchSize],[IsActive]
		from (
				values (@ConnectionName+'-Party',N'Populate chronos',@ConnectionID,
						N'[external_mpt].[User_Conf]',
						'<Mapping><Column Source="PartyID" Target="PartyID"/><Column Source="UserID" Target="UserID"/><Column Source="NICKNAME" Target="Nickname"/><Column Source="EMAIL" Target="Email"/><Column Source="TITLE" Target="Title"/><Column Source="FIRST_NAME" Target="FirstName"/><Column Source="LAST_NAME" Target="LastName"/><Column Source="GENDER" Target="Gender"/><Column Source="BIRTHDATE" Target="BirthDate"/><Column Source="ADDRESS" Target="Address"/><Column Source="CITY" Target="City"/><Column Source="PROVINCE" Target="Province"/><Column Source="COUNTRY" Target="Country"/><Column Source="MOBILE_PHONE" Target="Mobile"/><Column Source="LANGUAGE" Target="Language"/><Column Source="CURRENCY" Target="Currency"/><Column Source="REG_DATE" Target="RegistrationDate"/><Column Source="VIP_STATUS" Target="VIPStatus"/><Column Source="Hierarchy" Target="Hierarchy"/><Column Source="user_type" Target="UserType"/></Mapping>',
						N'ChangeTracking',@ConnectionID,N'[chronos]',
						N'[Party_Interface]',1500,1),

						(@ConnectionName+'-Staff',N'Populate chronos',@ConnectionID,
						N'admin_all.STAFF_AUTH_TBL',
						'<Mapping><Column Source="STAFFID" Target="PartyID"/><Column Source="LOGINNAME" Target="UserID"/><Column Source="EMAIL" Target="Email"/><Column Source="FIRST_NAME" Target="FirstName"/><Column Source="LAST_NAME" Target="LastName"/><Column Source="PHONE" Target="Mobile"/><Column Source="ACTIVE" Target="Active"/><Column Source="REG_DATE" Target="RegistrationDate"/>></Mapping>',
						N'ChangeTracking',@ConnectionID,N'[chronos]',
						N'[Staff_Interface]',1500,1),

						(@ConnectionName+'-UserReferral',N'Populate chronos',@ConnectionID,
						N'admin_all.USER_REFERRAL',
						'<Mapping><Column Source="ID" Target="UserReferralID"/><Column Source="PARTYID" Target="PartyID"/><Column Source="REFERRED_PARTYID" Target="ReferredPartyID"/></Mapping>',
						N'ChangeTracking',@ConnectionID,N'[chronos]',
						N'[UserReferral_Interface]',1500,1),

						(@ConnectionName+'-UserTrackingCode',N'Populate chronos',@ConnectionID,
						N'admin_all.USER_TRACKING_CODE',
						'<Mapping><Column Source="ID" Target="UserTrackingCodeID"/><Column Source="PARTYID" Target="PartyID"/><Column Source="CODE_KEY" Target="CodeKey"/><Column Source="VALUE" Target="Value"/></Mapping>',
						N'ChangeTracking',@ConnectionID,N'[chronos]',
						N'[UserTrackingCode_Interface]',1500,1),

						(@ConnectionName+'-VIPStatus',N'Populate chronos',@ConnectionID,
						N'admin_all.VIP_STATUS',
						'<Mapping><Column Source="ID" Target="VIPStatusID"/><Column Source="code" Target="Code"/></Mapping>',
						N'ChangeTracking',@ConnectionID,N'[chronos]',
						N'[VIPStatus_Interface]',1500,1),

						(@ConnectionName+'-AccountTranAggregate',N'Populate chronos',@ConnectionID,
						N'admin_all.AccountTranAggregate',
						'<Mapping><Column Source="Period" Target="Period"/><Column Source="AggregateType" Target="AggregateType"/><Column Source="Date" Target="Date"/><Column Source="BrandID" Target="BrandID"/><Column Source="PartyID" Target="PartyID"/><Column Source="Currency" Target="Currency"/><Column Source="TranType" Target="TranType"/><Column Source="ProductID" Target="ProductID"/><Column Source="GameID" Target="GameID"/><Column Source="AmountReal" Target="AmountReal"/><Column Source="AmountReleasedBonus" Target="AmountReleasedBonus"/><Column Source="AmountPlayableBonus" Target="AmountPlayableBonus"/><Column Source="GameCount" Target="GameCount"/><Column Source="TranCount" Target="TranCount"/></Mapping>',
						N'ChangeTracking',@ConnectionID,N'[chronos]',
						N'[AccountTranAggregate_Interface]',1500,1),
						
						(@ConnectionName+'-UserTag',N'Populate chronos',@ConnectionID,
						N'admin_all.UserTag',
						'<Mapping><Column Source="PartyID" Target="PartyID"/><Column Source="Code" Target="Code"/><Column Source="Name" Target="Name"/></Mapping>',
						N'ChangeTracking',@ConnectionID,N'[chronos]',
						N'[UserTag_Interface]',1500,1)
			) v([Name],[Comment],[SourceConnectionID],[SourceTableOrQuery],[Columns],[FilterType],[TargetConnectionID],[TargetSchemaName],[TargetObjectName],[BatchSize],[IsActive])
	)
	merge maint.ReplicationConfiguration t
	using s on s.[Name]= t.[Name]
	when not matched then
		insert ([Name],[Comment],[SourceConnectionID],[SourceTableOrQuery],[Columns],[FilterType],[TargetConnectionID],[TargetSchemaName],[TargetObjectName],[BatchSize],[IsActive])
		values(s.[Name],s.[Comment],s.[SourceConnectionID],s.[SourceTableOrQuery],s.[Columns],s.[FilterType],s.[TargetConnectionID],s.[TargetSchemaName],s.[TargetObjectName],s.[BatchSize],s.[IsActive])
	when matched and (s.[Comment] is null and t.[Comment] is not null or s.[Comment] is not null and t.[Comment] is null or s.[Comment] <> t.[Comment] or s.[SourceConnectionID] is null and t.[SourceConnectionID] is not null or s.[SourceConnectionID] is not null and t.[SourceConnectionID] is null or s.[SourceConnectionID] <> t.[SourceConnectionID] or s.[SourceTableOrQuery] is null and t.[SourceTableOrQuery] is not null or s.[SourceTableOrQuery] is not null and t.[SourceTableOrQuery] is null or s.[SourceTableOrQuery] <> t.[SourceTableOrQuery] or s.[Columns] is null and t.[Columns] is not null or s.[Columns] is not null and t.[Columns] is null or s.[Columns] <> t.[Columns] or s.[FilterType] is null and t.[FilterType] is not null or s.[FilterType] is not null and t.[FilterType] is null or s.[FilterType] <> t.[FilterType] or s.[TargetConnectionID] is null and t.[TargetConnectionID] is not null or s.[TargetConnectionID] is not null and t.[TargetConnectionID] is null or s.[TargetConnectionID] <> t.[TargetConnectionID] or s.[TargetSchemaName] is null and t.[TargetSchemaName] is not null or s.[TargetSchemaName] is not null and t.[TargetSchemaName] is null or s.[TargetSchemaName] <> t.[TargetSchemaName] or s.[TargetObjectName] is null and t.[TargetObjectName] is not null or s.[TargetObjectName] is not null and t.[TargetObjectName] is null or s.[TargetObjectName] <> t.[TargetObjectName] or s.[BatchSize] is null and t.[BatchSize] is not null or s.[BatchSize] is not null and t.[BatchSize] is null or s.[BatchSize] <> t.[BatchSize] or s.[IsActive] is null and t.[IsActive] is not null or s.[IsActive] is not null and t.[IsActive] is null or s.[IsActive] <> t.[IsActive]) then 
		update set  t.[Comment]= s.[Comment], t.[SourceConnectionID]= s.[SourceConnectionID], t.[SourceTableOrQuery]= s.[SourceTableOrQuery], t.[Columns]= s.[Columns], t.[FilterType]= s.[FilterType], t.[TargetConnectionID]= s.[TargetConnectionID], t.[TargetSchemaName]= s.[TargetSchemaName], t.[TargetObjectName]= s.[TargetObjectName], t.[BatchSize]= s.[BatchSize], t.[IsActive]= s.[IsActive]
	;
	--select name [@Source], name [@Target] from sys.columns  where object_id = object_id('chronos.Party_Interface') for xml path('Column'), root('Mapping')
end
go
exec chronos.usp_DataInitialization

--select * from admin_all.VIP_STATUS
--exec [admin_all].[usp_DataInitializationHelper] 'maint.ReplicationConfiguration', 0, 1, 'Name'
--select name from sys.columns  where object_id = object_id('external_mpt.USER_CONF')

--select * from maint.ReplicationConfiguration order by 1 desc

--select * from admin_all.VIP_STATUS