set ansi_nulls, quoted_identifier on
if object_id('chronos.usp_UpdatePartyWinLoss') is null
begin
	exec('create procedure chronos.usp_UpdatePartyWinLoss as --')
end
go
alter procedure chronos.usp_UpdatePartyWinLoss
(
	@PartyID int,
	@IsWin bit,
	@AmountRR numeric(38, 18)
)
as
begin
	set nocount on
	
	update t with(rowlock)
		set t.LastUpdateDate = getdate(),
			t.WinsInARow = case when @IsWin = 1 then 
											case when t.WinsInARow >=0 then t.WinsInARow + 1
												else 1
											end
								else
											0
							end,
			t.WinsInARowAmountRR = case when @IsWin = 1 then 
											case when t.WinsInARow >=0 then t.WinsInARowAmountRR + isnull(@AmountRR,0)
												else isnull(@AmountRR,0)
											end
								else
											0
							end,
			t.LossInARow = case when @IsWin = 0 then 
											case when t.LossInARow >=0 then t.LossInARow + 1
												else 1
											end
								else
											0
							end,
			t.LossInARowAmountRR = case when @IsWin = 0 then 
											case when t.LossInARow >=0 then t.LossInARowAmountRR + isnull(@AmountRR,0)
												else isnull(@AmountRR,0)
											end
								else
											0
							end
	from chronos.PartyWinLoss t 
	where t.PartyID = @PartyID 
	if @@rowcount = 0
		insert into chronos.PartyWinLoss(PartyID, WinsInARow, LossInARow, LastUpdateDate, WinsInARowAmountRR, LossInARowAmountRR)
			values(@PartyID, @IsWin, case when @IsWin = 0 then 1 else 0 end, getdate(), case when @IsWin = 1 then isnull(@AmountRR,0) else 0 end, case when @IsWin = 0 then isnull(@AmountRR,0) else 0 end)
	;
end;

