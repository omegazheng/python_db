set ansi_nulls, quoted_identifier on
go
if object_id('chronos.fn_IsEnabled') is null
begin
	exec('create function chronos.fn_IsEnabled() returns int as begin return 1; end')
end
go
alter function chronos.fn_IsEnabled()
returns bit
as
begin
	return case when isnull(admin_all.fn_GetRegistry('Chronos.Enabled'), 0) = 1 then 1 else 0 end
end


