set ansi_nulls, quoted_identifier on
go
if object_id('chronos.fn_IsDeleted') is null
begin
	exec('create function chronos.fn_IsDeleted() returns int as begin return 1; end')
end
go
alter function chronos.fn_IsDeleted(@v1 bit, @v2 bit)
returns bit
as
begin
	return case when @v1 =1 or @v2 = 1 then 1 else 0 end
end