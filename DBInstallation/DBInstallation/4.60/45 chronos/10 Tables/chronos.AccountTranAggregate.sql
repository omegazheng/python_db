set ansi_nulls, quoted_identifier on
go
if object_id('Chronos.AccountTranAggregate') is null
begin
	create table chronos.AccountTranAggregate
	(
		Period char(1) not null, -- Day, Month, Year
		AggregateType tinyint NOT NULL,
		Date date NOT NULL,
		BrandID int NOT NULL,
		PartyID int NOT NULL,
		Currency nchar(3) NULL,
		TranType varchar(10) NOT NULL,
		ProductID int NOT NULL,
		GameID varchar(100) NOT NULL,
		AmountReal numeric(38, 18) NOT NULL,
		AmountReleasedBonus numeric(38, 18) NOT NULL,
		AmountPlayableBonus numeric(38, 18) NOT NULL,
		GameCount int NOT NULL,
		TranCount int NOT NULL,
		___IsDeleted___ bit not NULL constraint DF_chronos_AccountTranAggregate____IsDeleted___ default(0),
		___RowVersion___ rowversion not null,
		constraint UQ_chronos_AccountTranAggregate____RowVersion___ unique(___RowVersion___),
		constraint PK_Chronos_AccountTranAggregate primary key(Period, Date, AggregateType, TranType, PartyID, ProductID,GameID ASC)WITH (Data_compression = Page)
	)
end

if not exists(select * from sys.indexes where object_id = object_id('Chronos.AccountTranAggregate') and name = 'IDX_CS_Chronos_AccountTranAggregate')
begin
	create nonclustered columnstore index IDX_CS_Chronos_AccountTranAggregate on Chronos.AccountTranAggregate(Period,AggregateType, Date, BrandID, PartyID, Currency, TranType, ProductID, GameID, AmountReal, AmountReleasedBonus, AmountPlayableBonus, GameCount, TranCount)
end