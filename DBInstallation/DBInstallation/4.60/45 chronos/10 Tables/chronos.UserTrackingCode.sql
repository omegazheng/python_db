set ansi_nulls, quoted_identifier on
go
if object_id('chronos.UserTrackingCode') is null
begin
	create table chronos.UserTrackingCode
	(
		UserTrackingCodeID int not null,
		PartyID int NOT NULL,
		___IsDeleted___ bit not NULL constraint DF_chronos_UserTrackingCode____IsDeleted___ default(0),
		___RowVersion___ rowversion not null
		constraint PK_chronos_UserTrackingCode primary key (UserTrackingCodeID),
		constraint UQ_chronos_UserTrackingCode____RowVersion___ unique(___RowVersion___)
	);
end
go
if not exists(select * from sys.indexes where object_id = object_id('chronos.UserTrackingCode') and name = 'IDX_chronos_UserTrackingCode_PartyID')
begin
	create index IDX_chronos_UserTrackingCode_PartyID on chronos.UserTrackingCode(PartyID)
end
--sp_help 'admin_all.user_tracking_code'