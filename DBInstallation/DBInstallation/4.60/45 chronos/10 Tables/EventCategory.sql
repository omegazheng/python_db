set ansi_nulls, quoted_identifier on
go
if object_id('chronos.EventCategory') is null
begin
	create table chronos.EventCategory
	(
		CategoryID int not null identity(1,1),
		ParentCategoryID int null,
		Name varchar(128) not null,
		Description varchar(max),
		constraint PK_chronos_EventCategory primary key (CategoryID),
		constraint UQ_chronos_EventCategory_ParentCategoryID_Name unique (ParentCategoryID, Name),
		constraint FK_chronos_EventCategory_ParentCategoryID foreign key (ParentCategoryID) references chronos.EventCategory(CategoryID)
	)
end



