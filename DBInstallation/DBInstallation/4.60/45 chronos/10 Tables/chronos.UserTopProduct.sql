set ansi_nulls, quoted_identifier on
go
if object_id('chronos.UserTopProduct') is null
begin
	create table chronos.UserTopProduct
	(
		PartyID int NOT NULL,
		Rank int not null,
		ProductCode nvarchar(100) not null, 
		GameID nvarchar(100) not null,
		GameName nvarchar(100) not null,
		Handle numeric(38, 18) not null,
		___IsDeleted___ bit not NULL constraint DF_chronos_UserTopProduct____IsDeleted___ default(0),
		___RowVersion___ rowversion not null
		constraint PK_chronos_UserTopProduct primary key (PartyID, Rank),
		constraint UQ_chronos_UserTopProduct____RowVersion___ unique(___RowVersion___)
		
	);
end
go 

