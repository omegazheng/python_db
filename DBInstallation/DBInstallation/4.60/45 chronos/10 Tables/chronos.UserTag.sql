set ansi_nulls, quoted_identifier on
go
if Object_id('chronos.UserTag') is null
begin
	create table chronos.UserTag
	(
		Code varchar(10) not null,
		PartyID int not null,
		Name varchar(255) not null,
		___IsDeleted___ bit not null constraint DF_chronos_UserTag__IsDeleted___ default(0),
		___RowVersion___ timestamp not null,
		constraint PK_chronos_UserTag primary key(Code, PartyID)

	)
end
