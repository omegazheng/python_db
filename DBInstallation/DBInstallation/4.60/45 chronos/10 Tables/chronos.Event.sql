set ansi_nulls, quoted_identifier on
if object_id('chronos.Event') is null
begin
	create table chronos.Event
	(
		EventID bigint not null identity(1,1),
		Name varchar(50) not null,
		PartyID int not null,
		Date datetime not null constraint DF_chronos_Event_Date default(getdate()),
		constraint PK_chronos_Event primary key (EventID)
	)
end
go
if not exists(select * from sys.indexes where name  = 'IDX_chronos_Event_Date' and object_id = object_id('chronos.Event'))
begin
	create index IDX_chronos_Event_Date on chronos.Event(Date)
end
