set ansi_nulls, quoted_identifier on
go
if object_id('chronos.Party') is null
begin
	create table chronos.Party
	(
		PartyID int NOT NULL,
		UserID varchar(100) NOT NULL, 
		Nickname varchar(100) NULL,
		Email varchar(100) NULL,
		Title varchar(100) null,
		FirstName varchar(100) NULL,
		LastName varchar(100) null,
		Gender varchar(20) null,
		BirthDate datetime null,
		Address varchar(512) null,
		City varchar(100) null,
		Province varchar(100) null,
		Country varchar(100) null,
		Mobile varchar(100) NULL,
		Language varchar(100),
		Currency varchar(100),
		RegistrationDate datetime, 
		VIPStatus int,
		Hierarchy Hierarchyid,
		UserType int,
		___IsDeleted___ bit not NULL constraint DF_chronos_Party____IsDeleted___ default(0),
		___RowVersion___ rowversion not null
		constraint PK_chronos_Party primary key ( PartyID),
		constraint UQ_chronos_Party____RowVersion___ unique(___RowVersion___)
	);
end
/*
drop table chronos.Party
alter table chronos.party add UserType int
select object_schema_name(object_id), object_name(object_id),type_name(system_type_id),* from sys.columns where name like '%mobile%' order by 1,2
select object_schema_name(object_id), object_name(object_id),type_name(system_type_id),* from sys.columns where object_id = object_id('external_mpt.user_conf')
select object_schema_name(object_id), object_name(object_id),type_name(system_type_id),* from sys.columns where object_id = object_id('admin_all.VIP_Status')

select * from admin_all.USER_TRACKING_CODE 
select * from admin_all.VIP_STATUS
select * from admin_all.COUNTRY
select * from admin_all.USER_REFERRAL

select REF_FRIENDID,* from external_mpt.USER_CONF where  REF_FRIENDID is not null

sp_help 'admin_all.USER_TRACKING_CODE'
select * from sys.tables where name like '%ref%' order by 1


*/