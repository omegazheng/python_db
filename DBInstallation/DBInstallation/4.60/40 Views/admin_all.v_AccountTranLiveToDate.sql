set ansi_nulls, quoted_identifier on
go

go
if isnull(objectproperty(object_id('admin_all.v_AccountTranLiveToDate'), 'IsSchemaBound'), 0) = 0
exec('create or alter view admin_all.v_AccountTranLiveToDate with schemabinding
as
	select	
		AggregateType,
		PartyID, 
		TranType,
		sum(AmountReal) AmountReal, 
		sum(AmountReleasedBonus) AmountReleasedBonus, 
		sum(AmountPlayableBonus) AmountPlayableBonus,
		Count_Big(*) Count
	from admin_all.AccountTranHourlyAggregate
	group by AggregateType,TranType,PartyID
')
go
if not exists(select * from sys.indexes where object_id = object_id('[admin_all].[v_AccountTranLiveToDate]') and name = 'PK_admin_all_v_AccountTranLiveToDate')
CREATE UNIQUE CLUSTERED INDEX [PK_admin_all_v_AccountTranLiveToDate]
    ON [admin_all].[v_AccountTranLiveToDate]([AggregateType] ASC, [TranType] ASC, [PartyID] ASC);
GO
