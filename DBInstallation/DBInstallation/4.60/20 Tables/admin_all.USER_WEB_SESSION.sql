if exists(select * from sys.columns where object_id = object_id('admin_all.USER_WEB_SESSION') and name = 'IP' and max_length = 25)
	alter table admin_all.USER_WEB_SESSION alter column IP varchar(50)