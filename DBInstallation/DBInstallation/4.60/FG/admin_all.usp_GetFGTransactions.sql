SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_GetFGTransactions
    @StartDate DATETIME,
    @EndDate DATETIME,
    @PartyId int = null,
    @BrandId int = null,
    @RollbackTranId bigint = null,
    @PlatformTranId nvarchar(50) = null,
    @GameId nvarchar(50) = null,
    @TranType nvarchar(20) = null
AS
begin

    DECLARE @SQL nvarchar(max);
    DECLARE @SQL_where nvarchar(max);

    if @StartDate IS NULL or @EndDate IS NULL
    begin
            raiserror ('Data is required.', 16, 1)
            return
    end


    SET @SQL = 'select uc.PARTYID, at.* from admin_all.ACCOUNT_TRAN at
                     join admin_all.ACCOUNT a on a.id = at.ACCOUNT_ID
                     join external_mpt.USER_CONF uc on uc.PARTYID = a.PARTYID '
    SET @SQL_where = ' where '

    IF @StartDate IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' at.DATETIME >= ''' + CONVERT(nvarchar(25), @StartDate, 121) + ''''
        END

    IF @EndDate IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' and at.DATETIME < ''' + CONVERT(nvarchar(25), @EndDate, 121) + ''''
        END

    IF @PartyId IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' and uc.PARTYID =' + CAST(@PartyId AS VARCHAR(20))
        END

    IF @BrandId IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' and uc.BRANDID =' + CAST(@BrandId AS VARCHAR(20))
        END

    IF @RollbackTranId IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' and at.ROLLBACK_TRAN_ID =' + CAST(@RollbackTranId AS VARCHAR(100))
        END

    IF @PlatformTranId IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' and at.PLATFORM_TRAN_ID like ''' + @PlatformTranId + ''''
        END

    IF @GameId IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' and at.GAME_ID = ''' + @GameId + ''''
        END

    IF @TranType IS NOT NULL
        BEGIN
            set @SQL_where = @SQL_where + ' and at.TRAN_TYPE = ''' + @TranType + ''''
        END

    -- exec
    SET @SQL = @SQL + @SQL_where
    PRINT @SQL
    exec(@SQL)


end

go

-- Demo
-- exec admin_all.usp_GetFGTransactions @StartDate = '2019-09-27', @EndDate = '2019-09-28'


-- exec admin_all.usp_GetFGTransactions @StartDate = '2019-09-27', @EndDate = '2019-09-28', @PartyId = 100144890,
--      @BrandId = 1, @RollbackTranId = 8237497, @PlatformTranId = 'RB-%',
--      @GameId = 'NOVO_SPORTSBOOK', @TranType = 'RSV_CANCEL'

-- LSL Prod
-- exec admin_all.usp_GetFGTransactions @StartDate = '2019-09-30', @EndDate = '2019-10-01', @PlatformTranId = 'RB-%', @PartyId =4122458

-- exec admin_all.usp_GetFGTransactions @StartDate = '2019-09-30', @EndDate = '2019-10-01', @PartyId = 4122458,
--      @BrandId = 188, @RollbackTranId = 3865363639, @PlatformTranId = 'RB-%',
--      @GameId = '100310', @TranType = 'GAME_BET'

-- select at.* from admin_all.ACCOUNT_TRAN at
--                      join admin_all.ACCOUNT a on a.id = at.ACCOUNT_ID
--                      join external_mpt.USER_CONF uc on uc.PARTYID = a.PARTYID
-- where
--         at.DATETIME >= '2019-09-27' and at.DATETIME <'2019-09-28'
--   and uc.PARTYID = 100144890
--   and uc.BRANDID = 1
--   and at.ROLLBACK_TRAN_ID = 8237497
--   and at.PLATFORM_TRAN_ID like 'RB-%'
--   and at.GAME_ID = 'NOVO_SPORTSBOOK'
--   and at.TRAN_TYPE = 'RSV_CANCEL'

-- Usage
-- Params:
--      StartDate (required)
--      EndDate (required)
--      PartyId
--      BrandId
--      RollbackTranId
--      PlatformTranId
--      GameId
--      TranType
--
-- Note:
--      StartDate and EndDate are required.
--      SSW can be separated by brandId
--      PlatformTranId should using '%', Internal logic using like (PLATFORM_TRAN_ID like 'RB%')
--      Better not query for a long time range.
