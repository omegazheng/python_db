set ansi_nulls, quoted_identifier on
go
set xact_abort on
go

if not exists(
				select * 
				from sys.indexes i 
					inner join sys.index_columns ic on i.object_id = ic.object_id and i.index_id = ic.index_id
				where i.name = 'IDX_CS_admin_all_AccountTranHourlyAggregate' 
					and  i.object_id = object_id('admin_all.AccountTranHourlyAggregate')
					and col_name(ic.object_id, ic.column_id) = 'BonusPlanTran_AmountPlayableBonus'
				)
begin
	begin transaction
	create nonclustered columnstore index IDX_CS_admin_all_AccountTranHourlyAggregate ON admin_all.AccountTranHourlyAggregate(AggregateType,DateTime,BrandID,PartyID,Currency,TranType,ProductID,GameID,AmountReal,AmountReleasedBonus,AmountPlayableBonus,GameCount,TranCount, BonusPlanTran_AmountPlayableBonus, BonusPlanTran_AmountPlayableBonusWinnings, BonusPlanTran_AmountReleasedBonus, BonusPlanTran_AmountReleasedBonusWinnings, RawLoyalty)with (drop_existing = on) 
	exec admin_all.usp_InitializeAccountTranHourlyAggregate null, null
	commit
end

GO




