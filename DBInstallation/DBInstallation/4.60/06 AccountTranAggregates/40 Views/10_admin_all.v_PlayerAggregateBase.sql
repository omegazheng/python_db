set quoted_identifier, ansi_nulls on
go
if object_id('admin_all.v_PlayerAggregateBase') is null
	exec('create view admin_all.v_PlayerAggregateBase as select 1 as ione')
go
alter view admin_all.v_PlayerAggregateBase --with schemabinding
as
select ath.AggregateType, ath.Date Datetime, ath.ProductID, ath.PartyID, ath.Currency, ath.BrandID, ath.GameID,
		sum(
				CASE
				WHEN ath.TranType in ('GAME_PLAY', 'GAME_WIN', 'GAME_BET', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
				THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
				ELSE 0
				END) as PNL,
		sum(
				CASE
				WHEN ((ath.TranType = 'GAME_PLAY' AND (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) < 0) OR
					ath.TranType in ('GAME_BET','STAKE_DEC', 'REFUND'))
				THEN (ath.AmountReal + ath.AmountReleasedBonus+ ath.AmountPlayableBonus) * -1
				ELSE 0
				END) as Handle,
		sum(
				CASE
				WHEN (ath.TranType = 'GAME_PLAY' AND ath.AmountReal < 0) OR ath.TranType in ('GAME_BET','STAKE_DEC', 'REFUND')
				THEN ath.AmountReal * -1
				ELSE 0
				END) HandleReal,
		sum(
			CASE
			WHEN ath.TranType in ('GAME_PLAY', 'GAME_WIN', 'GAME_BET', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
			THEN (ath.AmountReal) * -1
			ELSE 0
			END) PNLReal,
		sum(
				CASE
				WHEN (ath.TranType = 'GAME_PLAY' AND ath.AmountReleasedBonus < 0) OR ath.TranType in ('GAME_BET','STAKE_DEC', 'REFUND')
				THEN ath.AmountReleasedBonus * -1
				ELSE 0
				END) HandleReleasedBonus,
		sum(
				CASE
				WHEN ath.TranType in ('GAME_PLAY', 'GAME_WIN', 'GAME_BET', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
				THEN ath.AmountReleasedBonus * -1
				ELSE 0
				END) PNLReleasedBonus,
		sum(
				CASE
				WHEN (ath.TranType = 'GAME_PLAY' AND ath.AmountPlayableBonus < 0) OR ath.TranType in ('GAME_BET','STAKE_DEC', 'REFUND')
				THEN ath.AmountPlayableBonus * -1
				ELSE 0
				END) HandlePlayableBonus,
		sum(
				CASE
				WHEN ath.TranType in ('GAME_PLAY', 'GAME_WIN', 'GAME_BET', 'CASH_OUT', 'STAKE_DEC', 'REFUND')
				THEN ath.AmountPlayableBonus * -1
				ELSE 0
				END) PNLPlayableBonus,
		sum(
				CASE
				WHEN (ath.TranType = 'BONUS_REL' AND ath.ProductID <> -1 AND ath.GameID <> '-UNKNOWN-')
				THEN ath.AmountPlayableBonus * -1
				ELSE 0
				END
			) InGamePlayableBonusRelease,
		sum(
				CASE
				WHEN (ath.TranType = 'BONUS_REL' AND ath.ProductID <> -1 AND ath.GameID <> '-UNKNOWN-')
				THEN ath.AmountReleasedBonus * -1
				ELSE 0
				END
			) InGameReleasedBonusRelease,
		sum(
			CASE
			WHEN ath.TranType = 'TIPS'
			THEN (ath.AmountReal) * -1
			ELSE 0
			END) TipsReal,
		sum(
			CASE
			WHEN ath.TranType = 'TIPS'
			THEN (ath.AmountPlayableBonus) * -1
			ELSE 0
			END)  TipsPlayableBonus,
		sum(
			CASE
			WHEN ath.TranType = 'TIPS'
			THEN (ath.AmountReleasedBonus) * -1
			ELSE 0
			END) TipsReleasedBonus,
		sum(ath.GameCount) GameCount,
		sum(CASE WHEN ath.TranType = 'GAME_BET' THEN ath.TranCount else 0 end) BetCount,
		sum(TranCount) TranCount,
		sum(CASE WHEN ath.TranType = 'TIPS' THEN ath.TranCount else 0 END) TipsCount,
		count_big(*) Count
	from admin_all.AccountTranAggregate ath
where (
				ath.TranType in ('GAME_PLAY', 'GAME_WIN', 'GAME_BET', 'CASH_OUT', 'STAKE_DEC', 'REFUND', 'TIPS')  OR
				(ath.TranType = 'BONUS_REL' AND ath.ProductID <> -1 AND ath.GameID <> '-UNKNOWN-')
		) and Period = 'D'
group by ath.AggregateType,  ath.Date, ath.ProductID, ath.PartyID, ath.Currency, ath.BrandID, ath.GameID
