SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
if object_id('admin_all.usp_CancelWithdraw') is null
	exec ('create procedure admin_all.usp_CancelWithdraw as--')
GO
GO
ALTER procedure [admin_all].[usp_CancelWithdraw]
  (
    @PartyID          int,
    @PaymentID        int,
    @TranType         varchar(100)
  )
as
  BEGIN
    set nocount on
    set xact_abort on

    DECLARE @BonusID int
    DECLARE @AccountTranID bigint
    DECLARE @AccountId INT
    begin transaction


    -- Lock the account first

    SELECT
      @AccountId = ID
    FROM admin_all.ACCOUNT with(rowlock, updlock)
    WHERE PARTYID = @PartyID;

    if @AccountId is null
    BEGIN
      rollback
      raiserror('Cannot locate the account record ', 16, 1, @PartyID)
      return
    END
    --   Going to Payment -> WithRequest -> Bonus
    --   take fund from bonus(es) and come up with final amount

    declare cBonus cursor local static for
      select
        bonus.ID,
        wr.id,
        isnull(wr.AMOUNT_REAL,0),
        isnull(wr.AMOUNT_FROM_BONUS,0),
        isnull(wr.AMOUNT_FROM_BONUS_WINNINGS,0)
      from admin_all.payment payment
        inner join admin_all.Withdrawal_request wr on payment.id = wr.PAYMENT_ID
        left join admin_all.BONUS bonus  on bonus.id = wr.bonus_id
      where payment.id = @PaymentID
      order by bonus.STATUS desc, TRIGGER_DATE

    DECLARE @amountReal NUMERIC(38, 18)
    DECLARE @amountBonusWin NUMERIC(38, 18)
    DECLARE @amountBonus NUMERIC(38, 18)
    DECLARE @WrId int
    DECLARE @WrAmountReal NUMERIC(38, 18)
    DECLARE @WrAmountBonus NUMERIC(38, 18)
    DECLARE @WrAmountBonusWinning NUMERIC(38, 18)
    DECLARE @BonusReleasedBonusDelta NUMERIC(38, 18)
    DECLARE @BonusReleasedBonusWinningDelta NUMERIC(38, 18)
    declare @resultAfter numeric(38,18)

    select @amountReal = 0, @amountBonusWin = 0,  @amountBonus = 0


--     Go thru WR to find out how many to put back in real and bonus
    open cBonus
    fetch next from cBonus
    into @BonusID, @WrId, @WrAmountReal, @WrAmountBonus, @WrAmountBonusWinning
    while @@fetch_status = 0
      begin

        select  @BonusReleasedBonusDelta = 0, @BonusReleasedBonusWinningDelta = 0

        if (@WrAmountReal > 0)
          select @amountReal = @amountReal + @WrAmountReal


        if (@BonusID is not null)
          begin
            if (@WrAmountBonusWinning > 0)
              begin
                select @amountBonusWin = @amountBonusWin + @WrAmountBonusWinning
                select @BonusReleasedBonusWinningDelta =  @WrAmountBonusWinning
              end
            if (@WrAmountBonus > 0)
              begin
                select @amountBonus = @amountBonus + @WrAmountBonus
                select @BonusReleasedBonusDelta =  @WrAmountBonus
              end

            -- print'bonus id '+Convert(varchar(11), @BonusID)
            -- print'@BonusReleasedBonusDelta '+ Convert(varchar(11), @BonusReleasedBonusDelta)
            -- print'@BonusReleasedBonusWinningDelta '+Convert(varchar(11), @BonusReleasedBonusWinningDelta)

            if (@BonusReleasedBonusDelta > 0 or @BonusReleasedBonusWinningDelta > 0)
              BEGIN
                -- print'update  @BonusReleasedBonusDelta '+Convert(varchar(11), @BonusReleasedBonusDelta)
                -- print'update  @BonusReleasedBonusWinningDelta '+Convert(varchar(11), @BonusReleasedBonusWinningDelta)

                update admin_all.bonus
                set RELEASED_BONUS = RELEASED_BONUS + @BonusReleasedBonusDelta,
                  @resultAfter = RELEASED_BONUS_WINNINGS = RELEASED_BONUS_WINNINGS + @BonusReleasedBonusWinningDelta
                where
                  id = @BonusID

                -- print'update  @resultAfter '+Convert(varchar(100), @resultAfter)
              END
          end
--         Delete WR record
        if (@WrId is not null)
          delete admin_all.WITHDRAWAL_REQUEST WHERE ID = @WrId

        fetch next from cBonus
        into @BonusID, @WrId, @WrAmountReal, @WrAmountBonus, @WrAmountBonusWinning
      end -- while loop

    DECLARE @ReleasedBonus    numeric(38, 18)
    select @ReleasedBonus =  @amountBonus + @amountBonusWin

    -- print'@ReleasedBonus '+Convert(varchar(11), @ReleasedBonus)
    update admin_all.ACCOUNT
      set BALANCE_REAL      = BALANCE_REAL + @amountReal,
          RELEASED_BONUS = RELEASED_BONUS + @ReleasedBonus
    where PARTYID = @PartyID


    DECLARE @PlayableBonus    numeric(38, 18)
    select @PlayableBonus = 0

    insert into admin_all.ACCOUNT_TRAN
    (ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL, payment_id,
     ROLLED_BACK, ROLLBACK_TRAN_ID, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS,
     AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY, BRAND_ID)
      select
        a.ID,
        getdate()                           DATETIME,
        @TranType                           TRAN_TYPE,
        @amountReal                         AMOUNT_REAL,
        a.BALANCE_REAL,
        @PaymentID                          payment_id,
        0                                   ROLLED_BACK,
        null                                ROLLBACK_TRAN_ID,
        @ReleasedBonus                      AMOUNT_RELEASED_BONUS,
        @PlayableBonus                      AMOUNT_PLAYABLE_BONUS,
        a.RELEASED_BONUS                    BALANCE_RELEASED_BONUS,
        a.PLAYABLE_BONUS                    BALANCE_PLAYABLE_BONUS,
        0                                   AMOUNT_RAW_LOYALTY,
        a.RAW_LOYALTY_POINTS                BALANCE_RAW_LOYALTY,
        u.BRANDID                           BRAND_ID
      from admin_all.ACCOUNT a
        inner join external_mpt.USER_CONF u on a.PARTYID = u.PARTYID
      where u.PARTYID = @PartyID;

    select @AccountTranID = cast(session_context(N'AccountTranID') as bigint)
    commit
    select
      @AccountTranID as AccountTranID,
      *
    from admin_all.ACCOUNT
    where PARTYID = @PartyID


  end
