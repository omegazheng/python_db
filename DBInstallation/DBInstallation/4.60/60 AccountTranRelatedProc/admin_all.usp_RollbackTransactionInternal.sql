
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('admin_all.usp_RollbackTransactionInternal') is null
	exec('create procedure admin_all.usp_RollbackTransactionInternal as --')
go
ALTER procedure admin_all.usp_RollbackTransactionInternal
(
	@TransactionID bigint = null, 
	@BrandID int = null,
	@GameTranID nvarchar(100) = null, 
	@ProductID int = null,
	@ProductTranID nvarchar(100) = null,
	@RollbackTransactionID bigint = null output
)
as
begin
	set nocount, xact_abort on
	declare @AccountID int, @DATETIME datetime, @TRAN_TYPE varchar(10), @AMOUNT_REAL numeric(38,18), @BALANCE_REAL numeric(38,18), @PLATFORM_TRAN_ID nvarchar(100), @GAME_TRAN_ID nvarchar(100), @GAME_ID varchar(100), @PLATFORM_ID int, @payment_id int, @ROLLED_BACK int, @AMOUNT_RELEASED_BONUS numeric(38,18), @AMOUNT_PLAYABLE_BONUS numeric(38,18), @BALANCE_RELEASED_BONUS numeric(38,18), @BALANCE_PLAYABLE_BONUS numeric(38,18), @AMOUNT_UNDERFLOW numeric(38,18), @AMOUNT_RAW_LOYALTY numeric(38,18), @BALANCE_RAW_LOYALTY numeric(38,18), @TRANSACTION_ON_HOLD_ID bigint, @SSW_TRAN_ID bigint, @REFERENCE varchar(100)
	declare @A_BALANCE_REAL numeric(38,18), @A_RELEASED_BONUS numeric(38,18), @A_PLAYABLE_BONUS numeric(38,18), @A_RAW_LOYALTY_POINTS numeric(38,18), @A_SECONDARY_BALANCE numeric(38,18), @A_UNPAID_CIT numeric(38,18), @A_CIT numeric(38,18), @MachineID int
	if @TransactionID is null
	begin
		select @TransactionID = ID 
		from admin_all.ACCOUNT_TRAN 
		where BRAND_ID = @BrandID
			and PLATFORM_TRAN_ID = @ProductTranID
			and PLATFORM_ID = @ProductID
	end
	select @TransactionID = ID, @AccountID = ACCOUNT_ID
	from admin_all.ACCOUNT_TRAN
	where ID = @TransactionID
	if @@rowcount = 0
	begin
		raiserror('TransactionID could not be found.', 16,1)
		return
	end
	select @RollbackTransactionID=ID from admin_all.ACCOUNT_TRAN where ROLLBACK_TRAN_ID = @TransactionID
	if @@rowcount > 0
		return

	begin transaction
	select	@AccountID = id, @A_BALANCE_REAL = BALANCE_REAL, @A_RELEASED_BONUS = RELEASED_BONUS, @A_PLAYABLE_BONUS = PLAYABLE_BONUS, 
			@A_RAW_LOYALTY_POINTS = RAW_LOYALTY_POINTS, @A_SECONDARY_BALANCE = SECONDARY_BALANCE, 
			@A_UNPAID_CIT = UNPAID_CIT, @A_CIT = CIT
	from admin_all.ACCOUNT with(Rowlock, xlock) where id = @AccountID
	select	@TRAN_TYPE = TRAN_TYPE, @AMOUNT_REAL =  AMOUNT_REAL, @BALANCE_REAL = BALANCE_REAL, @PLATFORM_TRAN_ID = PLATFORM_TRAN_ID, 
			@GAME_TRAN_ID = GAME_TRAN_ID, @GAME_ID = GAME_ID, @ProductID = PLATFORM_ID, @payment_id = payment_id, 
			@ROLLED_BACK = ROLLED_BACK, /*@RollbackTransactionID = ROLLBACK_TRAN_ID,*/ @AMOUNT_RELEASED_BONUS = AMOUNT_RELEASED_BONUS, 
			@AMOUNT_PLAYABLE_BONUS = AMOUNT_PLAYABLE_BONUS, @BALANCE_PLAYABLE_BONUS = @BALANCE_RELEASED_BONUS, @BALANCE_PLAYABLE_BONUS = BALANCE_PLAYABLE_BONUS, 
			@AMOUNT_UNDERFLOW = AMOUNT_UNDERFLOW, @AMOUNT_RAW_LOYALTY = AMOUNT_RAW_LOYALTY, @BALANCE_RAW_LOYALTY = BALANCE_RAW_LOYALTY, 
			@TRANSACTION_ON_HOLD_ID = TRANSACTION_ON_HOLD_ID, @SSW_TRAN_ID = SSW_TRAN_ID, @REFERENCE = REFERENCE, @BrandID = BRAND_ID,
			@MachineID = MachineID
	from admin_all.ACCOUNT_TRAN
	where ID = @TransactionID

	select	@AMOUNT_REAL = -@AMOUNT_REAL, @AMOUNT_PLAYABLE_BONUS = -@AMOUNT_PLAYABLE_BONUS, @AMOUNT_RELEASED_BONUS= -@AMOUNT_RELEASED_BONUS,
			@AMOUNT_RAW_LOYALTY = -@AMOUNT_RAW_LOYALTY, @AMOUNT_UNDERFLOW = -@AMOUNT_UNDERFLOW,
			@DATETIME = getdate(), @GAME_TRAN_ID = isnull(@GameTranID, @GAME_TRAN_ID),
			@ROLLED_BACK =0
	select	@A_BALANCE_REAL = @A_BALANCE_REAL + isnull(@AMOUNT_REAL, 0),
			@A_RELEASED_BONUS = @A_RELEASED_BONUS + isnull(@AMOUNT_RELEASED_BONUS, 0),
			@A_PLAYABLE_BONUS = @A_PLAYABLE_BONUS + isnull(@AMOUNT_PLAYABLE_BONUS, 0),
			@A_RAW_LOYALTY_POINTS = @A_RAW_LOYALTY_POINTS + isnull(@AMOUNT_RAW_LOYALTY, 0)
	select	@BALANCE_PLAYABLE_BONUS = @A_PLAYABLE_BONUS,
			@BALANCE_RAW_LOYALTY = @A_RAW_LOYALTY_POINTS,
			@BALANCE_REAL = @A_BALANCE_REAL,
			@BALANCE_RELEASED_BONUS = @A_RELEASED_BONUS
	
	update admin_all.ACCOUNT
		set BALANCE_REAL = @A_BALANCE_REAL,
			RELEASED_BONUS = @A_RELEASED_BONUS,
			PLAYABLE_BONUS = @A_PLAYABLE_BONUS,
			RAW_LOYALTY_POINTS = @A_RAW_LOYALTY_POINTS
	where ID = @AccountID
	insert into admin_all.ACCOUNT_TRAN(
										ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL,
										PLATFORM_TRAN_ID, GAME_TRAN_ID, GAME_ID, PLATFORM_ID, PAYMENT_ID,
										ROLLED_BACK, ROLLBACK_TRAN_ID, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS,
										BALANCE_PLAYABLE_BONUS, AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY, TRANSACTION_ON_HOLD_ID, SSW_TRAN_ID,
										REFERENCE, BRAND_ID, MachineID
									)
		values(
										@AccountID, @DATETIME, @TRAN_TYPE, @AMOUNT_REAL, @BALANCE_REAL,
										@PLATFORM_TRAN_ID, @GAME_TRAN_ID, @GAME_ID, @PLATFORM_ID, @PAYMENT_ID,
										@ROLLED_BACK, @TransactionID, @AMOUNT_RELEASED_BONUS, @AMOUNT_PLAYABLE_BONUS, @BALANCE_RELEASED_BONUS,
										@BALANCE_PLAYABLE_BONUS, @AMOUNT_RAW_LOYALTY, @BALANCE_RAW_LOYALTY, @TRANSACTION_ON_HOLD_ID, @SSW_TRAN_ID,
										@REFERENCE, @BrandID, @MachineID
			)
	select @RollbackTransactionID = cast(session_context(N'AccountTranID') as bigint)

	insert into admin_all.ACCOUNT_TRAN_UNDERFLOW(ID, ACCOUNT_ID, DATETIME, AMOUNT_REAL, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS)
		select @RollbackTransactionID ID, @AccountID ACCOUNT_ID, @DATETIME DATETIME, -AMOUNT_REAL, -AMOUNT_RELEASED_BONUS, -AMOUNT_PLAYABLE_BONUS
		from admin_all.ACCOUNT_TRAN_UNDERFLOW
		where ID = @TransactionID
-- multi currency support
	insert into admin_all.AccountTranCurrencyExtension(AccountTranID, RateID, Currency, AmountReal, ReleasedBonus, PlayableBonus)
		select @RollbackTransactionID, RateID, Currency,- AmountReal, -ReleasedBonus, -PlayableBonus
		from admin_all.AccountTranCurrencyExtension 
		where AccountTranID = @TransactionID
		/*
-- Game bonus bucket
	if @TRAN_TYPE not in ('GAME_BET', 'GAME_WIN')
		goto ___EndBonusBucket___;
	declare @GamebonusBucket table(ID bigint primary key, BonusID int, AmountReal numeric(38, 18) not null, ReleasedBonus numeric(38,8) not null, ReleasedBonusWinnings numeric(38,8) not null, PlayableBonus numeric(38,8) not null, PlayableBonusWinnings numeric(38,8) not null, AmountWagered numeric(38,8), BonusStatus varchar(25))
	insert into @GamebonusBucket(ID, BonusID, AmountReal, ReleasedBonus, ReleasedBonusWinnings, PlayableBonus, PlayableBonusWinnings, AmountWagered, BonusStatus)
		select gbb.ID, gbb.BONUS_ID, gbb.AMOUNT_REAL, gbb.RELEASED_BONUS, gbb.RELEASED_BONUS_WINNINGS, gbb.PLAYABLE_BONUS, gbb.PLAYABLE_BONUS_WINNINGS, b.AMOUNT_WAGERED, b.STATUS
		from admin_all.GAME_BONUS_BUCKET gbb
			left outer join admin_all.BONUS b on b.ID = gbb.BONUS_ID
		where gbb.ACCOUNT_ID = @AccountID
			and GAME_TRAN_ID = @GAME_TRAN_ID
			and (@GAME_ID is null or GAME_ID = @GAME_ID)
			and PLATFORM_ID = @ProductID
	if @@rowcount = 0
	begin
		insert into admin_all.GAME_BONUS_BUCKET(
												GAME_TRAN_ID, PLATFORM_ID, BONUS_ID, CREATE_DATE, LAST_UPDATE_DATE, 
												AMOUNT_REAL, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS, 
												MAX_RELEASED_BONUS, MAX_PLAYABLE_BONUS, ACCOUNT_ID, AMOUNT_FREE_BET, GAME_ID
												)
			values(
					@GAME_TRAN_ID, @ProductID, null, getdate() , getdate(), 
					@AMOUNT_REAL + @AMOUNT_PLAYABLE_BONUS + @AMOUNT_RELEASED_BONUS, 0 , 0, 0, 0, 
					0, 0, @AccountID, 0, @GAME_ID)
		goto ___EndBonusBucket___
	end

___EndBonusBucket___:*/
___Exit___:
	commit
end