set quoted_identifier, ansi_nulls on
go
create or alter procedure admin_all.usp_InactivateExpiredBonusPlan
as
begin
	set nocount on 
	update b
		set STATUS = 'NOT_ACTIVE', LastModifiedStaffID = -1
	from admin_all.BONUS_PLAN b
	where b.END_DATE < getdate()
		and STATUS in('ACTIVE')
end