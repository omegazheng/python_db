set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CheckPlayerLoggingInByPartyIdAndOrSessionKey
(
    @SessionKey [nvarchar](100) = null,
    @PartyId int = null,
    @Timeout int,
    @Status int = null output
)
as
begin

    set nocount on
    if (@SessionKey is not null and @PartyId is null)
        if exists(select * from admin_all.USER_WEB_SESSION
                  where SESSION_KEY = @SessionKey and DATEDIFF(MINUTE, LAST_ACCESS_TIME, GETDATE()) < @Timeout)
            select @Status = 1
        else
            select @Status = 0
    if (@SessionKey is null and @PartyId is not null)
        if exists(select * from admin_all.USER_WEB_SESSION
                  where PARTYID = @PartyId and DATEDIFF(MINUTE, LAST_ACCESS_TIME, GETDATE()) < @Timeout)
            select @Status = 1
        else
            select @Status = 0
    if (@SessionKey is not null and @PartyId is not null)
        if exists(select * from admin_all.USER_WEB_SESSION
                  where SESSION_KEY = @SessionKey and PARTYID = @PartyId and DATEDIFF(MINUTE, LAST_ACCESS_TIME, GETDATE()) < @Timeout)
            select @Status = 1
        else
            select @Status = 0
    if (@SessionKey is null and @PartyId is null)
        RAISERROR('@SessionKey and @PartyId cannot be null at the same time', 16, 1)

end

go
