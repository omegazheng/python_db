SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_UpdateSignupConfirmationCode
(
    @PartyID INT,
    @ConfirmationCode NVARCHAR(255),
    @result INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT, xact_abort ON
    UPDATE external_mpt.USER_CONF
    SET CONFIRMATION_CODE= CONCAT('CONFIRMED-', CONFIRMATION_CODE)
    where PARTYID = @PartyID and CONFIRMATION_CODE = @ConfirmationCode
    select @result = @@rowcount

END
