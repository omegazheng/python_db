set ansi_nulls on
go
set quoted_identifier on
go
/**
Commit the reserve transaction:
Insert A Rsv_Commit Transaction only. Not insert Game Bet here.
*/
CREATE or ALTER procedure admin_all.usp_CommitTransactionWithoutGameBet
(
  @RsvTranID bigint = null,
  @RsvPlatformTranID nvarchar(100) = null,
  @PlatformID int = null,
  @RsvCommitPlatformTranID nvarchar(100) = null,
  @Reference varchar(300) = null,
  @RsvCommitTranID bigint = null output,
  @Status int = null output
)
as
begin
  set nocount, xact_abort on
  declare @ACCOUNT_ID int, @DATETIME datetime, @AMOUNT_REAL numeric(38,18), @PLATFORM_TRAN_ID nvarchar(100), @GAME_TRAN_ID nvarchar(100), @GAME_ID varchar(100), @PLATFORM_ID int, @PAYMENT_ID int, @AMOUNT_RELEASED_BONUS numeric(38,18), @AMOUNT_PLAYABLE_BONUS numeric(38,18), @AMOUNT_UNDERFLOW numeric(38,18), @AMOUNT_RAW_LOYALTY numeric(38,18), @RSV_REFERENCE varchar(300), @BRAND_ID int;
  declare @A_BALANCE_REAL numeric(38,18), @A_RELEASED_BONUS numeric(38,18), @A_PLAYABLE_BONUS numeric(38,18), @A_RAW_LOYALTY_POINTS numeric(38,18);
  declare @ROLLED_BACK int

  if @RsvTranID is not null
      begin
          select @RsvTranID = ID, @PLATFORM_ID = PLATFORM_ID from admin_all.ACCOUNT_TRAN where ID = @RsvTranID
      end
  else
      begin
          select @RsvTranID = ID, @PLATFORM_ID = PLATFORM_ID from admin_all.ACCOUNT_TRAN
          where PLATFORM_TRAN_ID = @RsvPlatformTranID
            and PLATFORM_ID = @PlatformID
            and TRAN_TYPE = 'RESERVE'
      end
  if @@rowcount = 0
      begin
          select @Status = -1; --  'Reserve_Tran_Not_Found'
		      return
      end

  select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where ROLLBACK_TRAN_ID = @RsvTranID and PLATFORM_ID = @PLATFORM_ID and TRAN_TYPE <> 'RSV_COMMIT'
  if @@rowcount > 0
		  begin
          select @Status = -2; -- Reserve_Tran_Already_Canceled
		      return
		  end

  select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where ROLLBACK_TRAN_ID = @RsvTranID and PLATFORM_ID = @PLATFORM_ID and TRAN_TYPE = 'RSV_COMMIT'
	if @@rowcount > 0
      begin
        select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where PLATFORM_TRAN_ID = @RsvCommitPlatformTranID AND PLATFORM_ID = @PLATFORM_ID AND TRAN_TYPE = 'RSV_COMMIT';
        select @Status = 1;  --   success, already processed
        return
      end

  select @ACCOUNT_ID = ACCOUNT_ID, @AMOUNT_REAL =  AMOUNT_REAL,
      @PLATFORM_TRAN_ID = PLATFORM_TRAN_ID, @GAME_TRAN_ID = GAME_TRAN_ID, @GAME_ID = GAME_ID, @PLATFORM_ID = PLATFORM_ID, @PAYMENT_ID = payment_id,
      @AMOUNT_RELEASED_BONUS = AMOUNT_RELEASED_BONUS, @AMOUNT_PLAYABLE_BONUS = AMOUNT_PLAYABLE_BONUS,
      @AMOUNT_UNDERFLOW = AMOUNT_UNDERFLOW, @AMOUNT_RAW_LOYALTY = AMOUNT_RAW_LOYALTY,
      @RSV_REFERENCE = REFERENCE, @BRAND_ID = BRAND_ID
  from admin_all.ACCOUNT_TRAN where ID = @RsvTranID;

  select @A_BALANCE_REAL = BALANCE_REAL, @A_RELEASED_BONUS = RELEASED_BONUS, @A_PLAYABLE_BONUS = PLAYABLE_BONUS,
      @A_RAW_LOYALTY_POINTS = RAW_LOYALTY_POINTS
  from admin_all.ACCOUNT with(Rowlock, xlock) where id = @ACCOUNT_ID;

  select @DATETIME = getdate(), @AMOUNT_REAL = isnull(@AMOUNT_REAL, 0), @AMOUNT_RELEASED_BONUS = isnull(@AMOUNT_RELEASED_BONUS, 0),
      @AMOUNT_PLAYABLE_BONUS = isnull(@AMOUNT_PLAYABLE_BONUS, 0), @AMOUNT_RAW_LOYALTY = isnull(@AMOUNT_RAW_LOYALTY, 0),
      @RsvCommitPlatformTranID = isnull(@RsvCommitPlatformTranID, @PLATFORM_TRAN_ID),
      @ROLLED_BACK = 0,
      @Reference = isnull(@Reference, @RSV_REFERENCE);
  insert into admin_all.ACCOUNT_TRAN(
        ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL,
        PLATFORM_TRAN_ID, GAME_TRAN_ID, GAME_ID, PLATFORM_ID, PAYMENT_ID,
        ROLLED_BACK, ROLLBACK_TRAN_ID, AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS,
        BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS,
        AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY,
        REFERENCE, BRAND_ID
      )
  values(
        @ACCOUNT_ID, @DATETIME, 'RSV_COMMIT', -@AMOUNT_REAL, @A_BALANCE_REAL - @AMOUNT_REAL,
        @RsvCommitPlatformTranID, @GAME_TRAN_ID, @GAME_ID, @PLATFORM_ID, @PAYMENT_ID,
        @ROLLED_BACK, @RsvTranID, -@AMOUNT_RELEASED_BONUS, -@AMOUNT_PLAYABLE_BONUS,
        @A_RELEASED_BONUS - @AMOUNT_RELEASED_BONUS, @A_PLAYABLE_BONUS - @AMOUNT_PLAYABLE_BONUS,
        -@AMOUNT_RAW_LOYALTY, @A_RAW_LOYALTY_POINTS - @AMOUNT_RAW_LOYALTY,
        @Reference, @BRAND_ID
      );

  select @RsvCommitTranID=ID from admin_all.ACCOUNT_TRAN where PLATFORM_TRAN_ID = @RsvCommitPlatformTranID AND PLATFORM_ID = @PLATFORM_ID AND TRAN_TYPE = 'RSV_COMMIT';
  select @Status = 0;  --   success
  return
end