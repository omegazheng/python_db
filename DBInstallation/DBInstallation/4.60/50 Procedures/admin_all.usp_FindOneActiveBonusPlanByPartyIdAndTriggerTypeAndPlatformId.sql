set ansi_nulls on
go
set quoted_identifier on
go
CREATE OR ALTER PROCEDURE  admin_all.usp_FindOneActiveBonusPlanByPartyIdAndTriggerTypeAndPlatformId
(
  @PartyID int = null,
  @TriggerType nvarchar(100) = null,
  @PlatformID int = null,
  @BonusPlanID int = null output
)
as
begin
    DECLARE @BRANDID INT;

    SELECT @BRANDID = brandid
    FROM   external_mpt.user_conf
    WHERE  partyid = @PartyID;

    SELECT top 1 @BonusPlanID = bp.id
    FROM   admin_all.bonus_plan_brand bpbrand
           LEFT JOIN admin_all.bonus_plan bp
                  ON bpbrand.bonus_plan_id = bp.id
           LEFT JOIN admin_all.platform p
                  ON bp.product_id = p.id
    WHERE  bpbrand.brand_id = @BRANDID
           AND bp.start_date <= Getdate()
           AND bp.end_date > Getdate() - 1
           AND bp.status = 'ACTIVE'
           AND bp.trigger_type = @TriggerType
           AND bp.product_id = @PlatformID
    ORDER  BY priority;

    select @BonusPlanID
    return
end

-- exec admin_all.usp_FindOneActiveBonusPlanByPartyIdAndTriggerTypeAndPlatformId @PartyID = 100118986, @TriggerType = 'PRODUC_BON', @PlatformID = 161