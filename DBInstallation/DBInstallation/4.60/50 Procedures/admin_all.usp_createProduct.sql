set ansi_nulls on
go
set quoted_identifier on
go

if object_id(N'[admin_all].[usp_CreateProduct]') is null
  exec('create procedure admin_all.usp_CreateProduct as --')
go
alter procedure admin_all.usp_CreateProduct
(
  @Code nvarchar(50),
  @ProductType nvarchar (50) = null,
  @WalletType nvarchar(50) = null,
  @ProductPassword nvarchar(50) = null,
  @ProductID int = null output,
  @ReturnRecord bit = 1
)
as
begin
	set nocount, xact_abort on
	select @ProductID = ID from admin_all.platform where platform.code = @Code
	if @@rowcount > 0
		goto ___End___
	declare @SegmentID int = 0
	select	@ProductType = isnull(@ProductType, 'CASINO'),
			@WalletType = isnull(@WalletType, 'GAME_PLAY'),
			@ProductPassword = isnull(@ProductPassword, @Code)
	select @SegmentID = id from admin_all.segment where name = @ProductType
	begin transaction
	insert into admin_all.platform (code, name, is_enabled, username, password, wallet_type,
									platform_type, convertjackpots, session_limit_web, session_limit_mobile,
									reality_check_web, reality_check_mobile, excluded_countries,
									provider_id, segment_id)
     values (	@Code, @Code, 1, @Code, admin_all.fn_getMD5(@ProductPassword), @WalletType,
				@ProductType, 1, 'iframe',
				'iframe', 'iframe', 'iframe', null,
				null, @SegmentID)
     select @ProductID = scope_identity()
     Update admin_all.platform set provider_id = @ProductID where id = @ProductID
	 commit
___End___:
	if @ReturnRecord = 1
		select * from admin_all.PLATFORM where id = @ProductID
end

go