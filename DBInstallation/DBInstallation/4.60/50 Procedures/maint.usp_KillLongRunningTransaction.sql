set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_KillLongRunningTransaction
as
begin
	set nocount on 
	declare @Threshold int = isnull(try_cast(admin_all.fn_GetRegistry('database.KillLongRunningTransaction.threshold(minute)') as int), 0)
	if @Threshold = 0
		return
	select *, datediff(minute, TransactionStartTime, getdate()) Duration into #1
	from maint.v_DatabaseTransactions
	where TransactionStartTime <= dateadd(minute, -@Threshold, getdate())
		and SessionID <> @@spid
	if @@rowcount = 0
		return

	declare @SQL nvarchar(max)
	select @SQL = (select 'kill ' + cast(SessionID as varchar(20)) + ';' from #1 for xml path(''));
	--print @SQL
	exec(@SQL)
	
	select @SQL = '<html>
<head>
  <title></title>
</head>
<body>
<table border="1" style="border-collapse:collapse">
<tr>
	<th>TransactionStartTime</th>
	<th>Duration(min)</th>
	<th>SessionID</th>
	<th>ResourceLocked</th>
	<th>Inputbuffer</th>
	<th>ClientName</th>
	<th>ClientAddress</th>
	<th>ApplicationName</th>
	<th>LoginName</th>
	<th>NetTransport</th>
</tr>
'+
	(
			select  
					(select TransactionStartTime as td for xml path(''), type),
					(select Duration as td for xml path(''), type),
					(select SessionID as td for xml path(''), type), 
					(select ResourceLocked as td for xml path(''), type), 
					(select Inputbuffer as td for xml path(''), type),  
					(select ClientName as td for xml path(''), type), 
					(select ClientAddress as td for xml path(''), type), 
					(select ApplicationName as td for xml path(''), type),
					(select LoginName as td for xml path(''), type),
					(select NetTransport as td for xml path(''), type)
			from (
					select *
					from #1
					
				) x
				order by TransactionStartTime
				for xml path('tr')
		)
+'

</table>
</body>
</html>'
	--print @SQL
		exec maint.usp_SendAlert 'Long Running Transaction found and killed', @SQL, 1, 'HTML'
end
go

--exec maint.usp_KillLongRunningTransaction