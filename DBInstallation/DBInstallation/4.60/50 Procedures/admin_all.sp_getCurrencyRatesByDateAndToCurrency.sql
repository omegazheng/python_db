set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.sp_getCurrencyRatesByDateAndToCurrency
(
	@toCurrency VARCHAR(3), @date DATE
)
as
begin
	set nocount on
	select CurrencyFrom as FROM_CURRENCY, EffectiveDate as DATE, CurrencyTo TO_CURRENCY, Rate as RATE
	from admin_all.CurrencyConversionRate
	where CurrencyTo = cast(@toCurrency as nchar(3))
		and EffectiveDate <= @date
		and ExpiryDate > @date
end
go
--set statistics io, time on
--exec admin_all.sp_getCurrencyRatesByDateAndToCurrency 'cad', '2019-10-22'

----select * from maint.v_ActiveSessions
