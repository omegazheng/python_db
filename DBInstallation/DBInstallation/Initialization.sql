--print '$(DatabaseName)'
--print '$(DataPath)'
--print '$(LogPath)'
set ansi_nulls, quoted_identifier, nocount on
begin try
declare @DatabaseName sysname = '$(DatabaseName)'
declare @Proc sysname = quotename(@DatabaseName) + '..sp_executesql'
--declare @HkeyLocal nvarchar(18), @MSSqlServerRegPath nvarchar(31), @InstanceRegPath sysname
--select @HkeyLocal=N'HKEY_LOCAL_MACHINE', @MSSqlServerRegPath=N'SOFTWARE\Microsoft\MSSQLServer'
--select @InstanceRegPath=@MSSqlServerRegPath + N'\'+isnull(cast(SERVERPROPERTY('InstanceName') as nvarchar(128)),N'MSSQLServer')
declare @DefaultData nvarchar(max), @DefaultLog nvarchar(max)
select	@DefaultData = cast(serverproperty('InstanceDefaultDataPath') as nvarchar(1000)), 
		@DefaultLog = cast(serverproperty('InstanceDefaultLogPath') as nvarchar(1000))
--exec master.dbo.xp_instance_regread @HkeyLocal, N'Software\Microsoft\MSSQLServer\MSSQLServer', N'DefaultData', @DefaultData output
--declare @MasterData nvarchar(512)
--exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer\Parameters', N'SqlArg0', @MasterData output
--select @MasterData=substring(@MasterData, 3, 255)
--select @MasterData=substring(@MasterData, 1, len(@MasterData) - charindex('\', reverse(@MasterData)))
--select @DefaultData = coalesce(cast(serverproperty('InstanceDefaultDataPath') as nvarchar(128)),@DefaultData, @MasterData, 'Unknown') 

--select @HkeyLocal=N'HKEY_LOCAL_MACHINE', @MSSqlServerRegPath=N'SOFTWARE\Microsoft\MSSQLServer'
--select @InstanceRegPath=@MSSqlServerRegPath + N'\'+isnull(cast(SERVERPROPERTY('InstanceName') as nvarchar(128)),N'MSSQLServer')

--exec master.dbo.xp_instance_regread @HkeyLocal, N'Software\Microsoft\MSSQLServer\MSSQLServer', N'DefaultLog', @DefaultLog output
--declare @MasterLog nvarchar(512)
--exec master.dbo.xp_instance_regread N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer\Parameters', N'SqlArg2', @MasterLog output
--select @MasterLog=substring(@MasterLog, 3, 255)
--select @MasterLog=substring(@MasterLog, 1, len(@MasterLog) - charindex('\', reverse(@MasterLog)))
--select @DefaultLog = coalesce(cast(serverproperty('InstanceDefaultLogPath') as nvarchar(128)), @DefaultLog, @MasterLog, 'Unknown') 
if @@version like '%linux%'
begin
	if right(@DefaultData, 1) <> '/'
		select @DefaultData = @DefaultData + '/'
	if right(@DefaultLog, 1) <> '/'
		select @DefaultLog = @DefaultData + '/'
end 
else
begin
	if right(@DefaultData, 1) <> '\'
		select @DefaultData = @DefaultData + '\'
	if right(@DefaultLog, 1) <> '\'
		select @DefaultLog = @DefaultData + '\'
end
declare @SQL nvarchar(max)
if db_id(@DatabaseName) is null
begin
	select @SQL = cast('CREATE DATABASE ' as nvarchar(max)) + quotename(@DatabaseName)+' ON  PRIMARY  ( NAME = N''' + @DatabaseName + '_Data'', FILENAME = N''' + @DefaultData + @DatabaseName + '_Data.mdf'', MAXSIZE = UNLIMITED, FILEGROWTH = 102400KB )
LOG ON (NAME = N''' + @DatabaseName + '_Log'', FILENAME = N''' +@DefaultLog + @DatabaseName + '_Log.ldf'', MAXSIZE = 2048GB , FILEGROWTH = 102400KB )'
	exec(@SQL)
	select @SQL = 'ALTER DATABASE '+ quotename(@DatabaseName)+' SET ANSI_NULL_DEFAULT OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET ANSI_NULLS ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET ANSI_PADDING ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET ANSI_WARNINGS ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET ARITHABORT ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET AUTO_CLOSE OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET AUTO_SHRINK OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET AUTO_UPDATE_STATISTICS ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET CURSOR_CLOSE_ON_COMMIT OFF; 
ALTER DATABASE '+ quotename(@DatabaseName)+' SET CURSOR_DEFAULT  GLOBAL;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET CONCAT_NULL_YIELDS_NULL ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET NUMERIC_ROUNDABORT OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET QUOTED_IDENTIFIER ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET RECURSIVE_TRIGGERS OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET  ENABLE_BROKER;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET AUTO_UPDATE_STATISTICS_ASYNC OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET DATE_CORRELATION_OPTIMIZATION OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET TRUSTWORTHY ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET ALLOW_SNAPSHOT_ISOLATION OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET PARAMETERIZATION FORCED;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET READ_COMMITTED_SNAPSHOT ON;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET HONOR_BROKER_PRIORITY OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET RECOVERY SIMPLE;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET  MULTI_USER;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET PAGE_VERIFY CHECKSUM;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET DB_CHAINING OFF;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF );
ALTER DATABASE '+ quotename(@DatabaseName)+' SET TARGET_RECOVERY_TIME = 0 SECONDS;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET DELAYED_DURABILITY = FORCED;
ALTER DATABASE '+ quotename(@DatabaseName)+' SET  READ_WRITE;
'
	exec(@SQL)
end
declare @DatabaseVersion varchar(100)
select @SQL = 'select @DatabaseVersion = ''0.0.0''
if object_id(''dbo.DatabaseVersion'') is null
begin
	create table dbo.DatabaseVersion
	(	
		Version varchar(100) not null, 
		Date datetime not null constraint DF_dbo_DatabaseVersion_Date default(GetUTCDate())
	)
end
if exists(select * from sys.columns where object_id = object_id(''dbo.DatabaseVersion'') and name = ''Version'' and type_name(system_type_id) = ''money'')
begin
	alter table dbo.DatabaseVersion alter column Version varchar(100) not null
	update dbo.DatabaseVersion
		set Version = cast(cast(cast(Version as numeric(10,2))*100 as int)/100 %10 as varchar(100)) + ''.'' + 
						cast(cast(cast(Version as numeric(10,2))*100 as int)/10 %10 as varchar(100)) + ''.'' +
						cast(cast(cast(Version as numeric(10,2))*100 as int) % 10 as varchar(100)) 
end
if not exists(select * from dbo.DatabaseVersion)
begin
	insert into dbo.DatabaseVersion(Version)
		values(@DatabaseVersion)
end

if object_id(''dbo.DatabaseUpdateBatch'') is null
begin
	create table dbo.DatabaseUpdateBatch
	(
		BatchID bigint not null identity(1,1) constraint PK_dbo_DatabaseUpdateBatch primary key, 
		HasError bit not null constraint DF_dbo_DatabaseUpdateBatch_HasError default(0),  
		StartDate datetime not null constraint DF_dbo_DatabaseUpdateBatch_Date default(GetUTCDate()),  
		EndDate datetime null,
		LoginName varchar(128) not null constraint DF_dbo_DatabaseUpdateBatch_LoginName default(system_user)
	)
end

if object_id(''dbo.DatabaseUpdateLog'') is null
begin
	create table dbo.DatabaseUpdateLog
	(
		
		BatchID bigint not null , 
		BatchLogID bigint not null identity(1,1),
		Version varchar(100) not null,
		Path varchar(max) not null,
		FileName varchar(max) not null,
		Query nvarchar(max),
		Message varchar(max),
		IsErrorMessage bit not null constraint DF_dbo_DatabaseUpdateLog_IsErrorMessage default(0),
		StartDate datetime not null constraint DF_dbo_DatabaseUpdateLog_Date default(GetUTCDate()),  
		EndDate datetime null,
		constraint PK_dbo_DatabaseUpdateLog primary key(BatchID, BatchLogID) with(Data_compression = page)
	)
end
if exists(select * from sys.columns where object_id = object_id(''dbo.DatabaseUpdateLog'') and name  = ''Path'')
begin
	alter table dbo.DatabaseUpdateLog drop column Path
end

if not exists(select * from sys.columns where object_id = object_id(''dbo.DatabaseUpdateLog'') and name  = ''Query'')
begin
	alter table dbo.DatabaseUpdateLog add Query nvarchar(max);
end
if object_id(''dbo.DF_dbo_DatabaseUpdateLog_IsErrorMessage'') is not null
begin
	alter table dbo.DatabaseUpdateLog drop constraint DF_dbo_DatabaseUpdateLog_IsErrorMessage
end

if exists(select * from sys.columns where object_id = object_id(''dbo.DatabaseUpdateLog'') and name  = ''IsErrorMessage'')
begin
	alter table dbo.DatabaseUpdateLog drop column IsErrorMessage
end

if exists(select * from sys.columns where object_id = object_id(''dbo.DatabaseUpdateLog'') and name = ''Version'' and type_name(system_type_id) = ''money'')
begin
	alter table dbo.DatabaseUpdateLog alter column Version varchar(100) not null;
    update dbo.DatabaseUpdateLog
		set Version = cast(cast(cast(Version as numeric(10,2))*100 as int)/100 %10 as varchar(100)) + ''.'' + 
						cast(cast(cast(Version as numeric(10,2))*100 as int)/10 %10 as varchar(100)) + ''.'' +
						cast(cast(cast(Version as numeric(10,2))*100 as int) % 10 as varchar(100)) 
end

exec(''if exists(select * from sys.columns where name = ''''Messge'''' and object_id =  object_id(''''dbo.DatabaseUpdateLog''''))
begin
	exec sp_rename ''''dbo.DatabaseUpdateLog.Messge'''', ''''Message'''', ''''COLUMN''''
end'')

if not exists(select * from sys.indexes where name = ''IDX_dbo_DatabaseUpdateLog_BatchLogID'' and object_id = object_id(''dbo.DatabaseUpdateLog''))
	create index IDX_dbo_DatabaseUpdateLog_BatchLogID on dbo.DatabaseUpdateLog(BatchLogID)

delete dbo.DatabaseUpdateLog where BatchLogID < ident_current(''dbo.DatabaseUpdateLog'') -1000000

exec(''create or alter procedure dbo.CreateDatabaseUpdateLog (@BatchID bigint output, @BatchLogID bigint output, @Version varchar(100), @FileName varchar(max), @Query nvarchar(max), @Message varchar(max))
as
begin
	set nocount on
	update dbo.databaseVersion set Version = @Version, Date = GetUTCDate() --where Version <= @Version
	if @BatchID = -1
	begin
		insert into dbo.DatabaseUpdateBatch default values
			select @BatchID = scope_identity()
	end
	if @BatchLogID = -1
	begin
		insert into dbo.DatabaseUpdateLog(BatchID, Version, FileName, Query)
			values(@BatchID, @Version, @FileName, @Query)
		select @BatchLogID = scope_identity()
		goto ___Exit___
	end
	update dbo.DatabaseUpdateLog
		set Message = @Message,
			EndDate = GetUTCDate()
	where BatchID = @BatchID
		and BatchLogID = @BatchLogID
		
	update dbo.DatabaseUpdateBatch
		set EndDate = GetUTCDate(),
			HasError =  case when HasError = 1 or nullif(ltrim(rtrim(@Message)), '''''''') is not null then 1 else 0 end
	where BatchID = @BatchID
___Exit___:
	select @BatchID BatchID, @BatchLogID BatchLogID
end'')'
exec @Proc @SQL, N'@DatabaseVersion varchar(100) output', @DatabaseVersion output
select @DefaultData DefaultData, @DefaultLog DefaultLog, @DatabaseVersion DatabaseVersion
end try
begin catch
	throw;
end catch