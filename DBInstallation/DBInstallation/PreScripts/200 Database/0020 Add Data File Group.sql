--print '$(DatabaseName)'
--print '$(DataPath)'
--print '$(LogPath)'
GO
use [$(DatabaseName)]
go
if not exists (select * from sys.filegroups where name = 'Data')
begin
	alter database [$(DatabaseName)] add filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data1, filename = '$(DataPath)$(DatabaseName)_Data1.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data2, filename = '$(DataPath)$(DatabaseName)_Data2.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data3, filename = '$(DataPath)$(DatabaseName)_Data3.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data4, filename = '$(DataPath)$(DatabaseName)_Data4.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data5, filename = '$(DataPath)$(DatabaseName)_Data5.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data6, filename = '$(DataPath)$(DatabaseName)_Data6.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data7, filename = '$(DataPath)$(DatabaseName)_Data7.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Data8, filename = '$(DataPath)$(DatabaseName)_Data8.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Data];
	alter database [$(DatabaseName)] modify filegroup [Data] default
end
GO


