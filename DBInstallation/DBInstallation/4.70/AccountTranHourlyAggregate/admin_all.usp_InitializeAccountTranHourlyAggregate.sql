set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_InitializeAccountTranHourlyAggregate
(
	@DateFrom datetime = null,
	@DateTo datetime = null
)
as
begin
	set nocount on
	if @DateFrom is null or @DateTo is null
		select @DateFrom = isnull(@DateFrom, min(datetime)), @DateTo = isnull(@DateTo, max(datetime)) from admin_all.ACCOUNT_TRAN
	begin transaction
	exec sp_getapplock @Resource = N'AccountTranHourlyAggregateCalculation', @LockMode = N'Exclusive', @LockOwner = 'Transaction', @LockTimeout = -1
	merge admin_all.AccountTranAggregateHourlyPendingCalculation0 t
	using (
			select distinct dateadd(hour, datediff(hour, '1990-01-01', DATETIME), '1990-01-01') Datetime
			from admin_all.ACCOUNT_TRAN
			where DATETIME >= @DateFrom and DATETIME <=@DateTo
			)s on t.Datetime = s.Datetime and t.Slot = 0
	when not matched then 
		insert (DateTime, Slot)
			values(s.DateTime, 0)
	;
	commit
end

go
