set ansi_nulls, quoted_identifier on
go


if not exists(select * from sys.columns where object_id = object_id('admin_all.PLAYER_LOCK_REASON') AND name = 'code')
alter table admin_all.PLAYER_LOCK_REASON ADD code varchar(255);

go
