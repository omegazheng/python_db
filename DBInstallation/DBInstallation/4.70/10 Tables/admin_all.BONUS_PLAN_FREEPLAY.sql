set ansi_nulls, quoted_identifier on
go


if not exists(select * from sys.columns where object_id = object_id('admin_all.BONUS_PLAN_FREEPLAY') AND name = 'AWARD_TYPE')
alter table admin_all.BONUS_PLAN_FREEPLAY ADD AWARD_TYPE varchar(30);

if not exists(select * from sys.columns where object_id = object_id('admin_all.BONUS_PLAN_FREEPLAY') AND name = 'ADDITIONAL_AWARD')
alter table admin_all.BONUS_PLAN_FREEPLAY ADD ADDITIONAL_AWARD nvarchar(max);


go


