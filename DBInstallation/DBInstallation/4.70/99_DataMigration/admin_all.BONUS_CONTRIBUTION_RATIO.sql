
set xact_abort on
if exists(select * from sys.indexes where object_id = object_id('admin_all.BONUS_CONTRIB_RATIO') and name = 'IDX_admin_all_BONUS_CONTRIB_RATIO_BRANDID')
    begin
        drop index admin_all.BONUS_CONTRIB_RATIO.IDX_admin_all_BONUS_CONTRIB_RATIO_BRANDID;
    end

if not exists(select * from sys.indexes where object_id = object_id('admin_all.BONUS_CONTRIB_RATIO') and name = 'IDX_admin_all_BONUS_CONTRIB_RATIO_BRANDID_GAME_INFO_ID')
    begin
        begin transaction
            delete a
            from (
                     select *, row_number() over (partition by BRANDID, GAME_INFO_ID order by ID asc) rn
                     from admin_all.BONUS_CONTRIB_RATIO
                 ) a
            where rn > 1;
            create unique index IDX_admin_all_BONUS_CONTRIB_RATIO_BRANDID_GAME_INFO_ID on admin_all.BONUS_CONTRIB_RATIO(BRANDID, GAME_INFO_ID) with(ignore_dup_key=on);
        commit
    end
