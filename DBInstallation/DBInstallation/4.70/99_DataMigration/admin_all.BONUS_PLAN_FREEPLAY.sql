-- Migrate existing BIA External Bonus to the new data structure
insert into admin_all.BONUS_PLAN_FREEPLAY (BONUS_PLAN_ID, PRODUCT_ID, AWARD_TYPE)
 (
    select bp.ID as BONUS_PLAN_ID, bp.SPORTSBOOK_PLATFORM_ID as PRODUCT_ID, 'EXTBONUSPROGRAM' as AWARD_TYPE from admin_all.BONUS_PLAN bp
    where
            bp.TYPE = 'EXTERNAL'
            and bp.ID not in (
        select BONUS_PLAN_ID from admin_all.BONUS_PLAN_FREEPLAY
    )  and bp.SPORTSBOOK_PLATFORM_ID is not null
)

go
