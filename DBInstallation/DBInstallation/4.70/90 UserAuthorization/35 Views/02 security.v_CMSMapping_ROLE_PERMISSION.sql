set ansi_nulls, quoted_identifier on
go
create or alter view security.v_CMSMapping_ROLE_PERMISSION
as
with x0 as
	(
		select  * 
		from (
				values('HIDE_DATA', 'EMAIL', 'SHOW_EMAIL'),
						('HIDE_DATA', 'PHONE', 'SHOW_PHONE'),
						('HIDE_DATA', 'MOBILEPHONE', 'SHOW_MOBILE'),
						('ENABLE_EXPORT', NULL, 'ENABLE_EXPORT')
			) pm(TYPE, SUB_TYPE, SecurableName)
	),
	x1 as
	(
		select  RolePrincipalID, ROLE_ID
		from security.v_CMSMapping_ROLE
	),
	x2 as 
	(
		select x0.TYPE, x0.SUB_TYPE, x1.ROLE_ID, ps.SecurableID, x0.SecurableName, s.Name, Count(ps.SecurableID) over(partition by x1.ROLE_ID) RN, ps.PrincipalID
		from x0	
			cross apply x1
			left join security.Securable s on s.Type = 'Data' and s.Name = x0.SecurableName 
			left outer join security.PrincipalSecurable ps on ps.PrincipalID = x1.RolePrincipalID and s.SecurableID = ps.SecurableID
														
		)
		select TYPE, SUB_TYPE, ROLE_ID 
		from x2
		where RN > 0
			and (
					TYPE = 'HIDE_DATA' and SecurableID is null
					or
					TYPE = 'ENABLE_EXPORT' and SecurableID is not null
				)
		union 
		select 'ENABLE_EXPORT', null, 1
go
--select * from security.v_CMSMapping_ROLE_PERMISSION
	--where ROLE_ID = 22
--order by ROLE_ID, TYPE




--select * from security.Securable where 
		--select * from admin_all.ROLE_PERMISSION
		--select 


		
			
			