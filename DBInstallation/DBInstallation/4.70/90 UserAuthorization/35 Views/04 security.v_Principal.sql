set ansi_nulls, quoted_identifier on
go
create or alter view security.v_Principal
as
select	p.PrincipalID, p.Name, p.Type, p.Description, p.SID, 
		p.CreatedBy, p.CreationDate, p.ModifiedBy, p.ModificationDate, 
		sid.ID, 
		isnull(u.USERID,s.LOGINNAME) UserID, 
		isnull(s.FIRST_NAME, u.FIRST_NAME) FirstName, 
		isnull(s.LAST_NAME, u.LAST_NAME) LastName, 
		u.NICKNAME NickName,
		isnull(u.ACTIVE_FLAG, s.ACTIVE) IsActive,
		isnull(u.LOCKED_STATUS, s.LOCKED_STATUS) LockStatus,

		isnull(cu.USERID,cs.LOGINNAME) CreatedByUserID, 
		isnull(cs.FIRST_NAME, cu.FIRST_NAME) CreatedByFirstName, 
		isnull(cs.LAST_NAME, cu.LAST_NAME) CreatedByLastName,
		cu.NICKNAME CreatedByNickName,

		isnull(mu.USERID,ms.LOGINNAME) ModifiedByUserID, 
		isnull(ms.FIRST_NAME, mu.FIRST_NAME) ModifiedByFirstName, 
		isnull(ms.LAST_NAME, mu.LAST_NAME) ModifiedByLastName,
		mu.NICKNAME ModifiedByNickName
from security.Principal p
	left join security.Principal c on p.CreatedBy = c.PrincipalID
	left join security.Principal m on p.ModifiedBy = m.PrincipalID

	outer apply security.fn_ExtractSID(p.SID) sid
	left join external_mpt.USER_CONF u on u.PARTYID = cast(sid.ID as int) and p.Type = 'AGENT'
	left join admin_all.STAFF_AUTH_TBL s on s.STAFFID = cast(sid.ID as int) and p.Type = 'STAFF'

	outer apply security.fn_ExtractSID(c.SID) csid
	left join external_mpt.USER_CONF cu on cu.PARTYID = cast(csid.ID as int) and c.Type = 'AGENT'
	left join admin_all.STAFF_AUTH_TBL cs on cs.STAFFID = cast(csid.ID as int) and c.Type = 'STAFF'

	outer apply security.fn_ExtractSID(m.SID) msid
	left join external_mpt.USER_CONF mu on mu.PARTYID = cast(msid.ID as int) and m.Type = 'AGENT'
	left join admin_all.STAFF_AUTH_TBL ms on ms.STAFFID = cast(msid.ID as int) and m.Type = 'STAFF'
go

--select * from security.v_Principal