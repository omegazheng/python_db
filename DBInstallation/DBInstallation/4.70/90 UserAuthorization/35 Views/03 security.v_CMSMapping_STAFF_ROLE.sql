set ansi_nulls, quoted_identifier on
go
create or alter view security.v_CMSMapping_STAFF_ROLE
as
with xx as
(
	select r.ROLE_ID, r.RolePrincipalID, rm.MemberPrincipalID, cast(sid.ID as int) as STAFF_ID
	from security.v_CMSMapping_ROLE r
		inner join security.RoleMember rm on rm.RolePrincipalID = r.RolePrincipalID
		inner join security.Principal p1 on p1.PrincipalID = rm.MemberPrincipalID
		cross  apply security.fn_ExtractSID(p1.SID) sid
	where p1.Type = 'STAFF'
	
),
x as
(
	select p.PrincipalID, cast(sid.ID as int) as STAFF_ID
	from security.Principal p
		cross apply security.fn_ExtractSID(p.SID) sid
	where p.Type = 'STAFF'
		and exists(select  * from admin_all.STAFF_AUTH_TBL s where s.STAFFID = cast(sid.ID as int))
),
x0 as
(
	select x.PrincipalID, RolePrincipalID, MemberPrincipalID
	from x
		cross apply security.fn_GetPrincipalRole(x.PrincipalID)
	where RolePrincipalID >=1000
),
x1 as
(
	select ps.PrincipalID, ps.SecurableID
	from security.PrincipalSecurable  ps
		inner join x on x.PrincipalID = ps.PrincipalID
),
x2 as
(
	select	x.PrincipalID, x.STAFF_ID,s.SecurableID, s.ParentSecurableID, s.Type,
			cast(case when exists(select * from x1 where s.SecurableID = x1.SecurableID and x1.PrincipalID = x.PrincipalID) then 1 else 0 end as bit) IsDirectGranted,
			case when exists((select x0.RolePrincipalID from x0 where x0.PrincipalID = x.PrincipalID and (exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID) or  x0.RolePrincipalID = 1 )) ) then 1 end ParentRoleGranted,
			0 as IsInherited
	from security.Securable s
		cross join x
	where s.ParentSecurableID is null
	union all
	select	x2.PrincipalID, x2.STAFF_ID, s.SecurableID, s.ParentSecurableID, s.Type,
			cast(case when exists(select * from x1 where s.SecurableID = x1.SecurableID and x2.PrincipalID = x1.PrincipalID) then 1 else 0 end as bit) IsDirectGranted,
			case when exists((select x0.RolePrincipalID from x0 where x2.PrincipalID = x0.PrincipalID and exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID))) then 1 end ParentRoleGranted,
			case when x2.IsInherited = 1 or x2.IsDirectGranted = 1 or x2.ParentRoleGranted is not null then 1 else 0 end as IsInherited
	from security.Securable s
		inner join x2 on x2.SecurableID = s.ParentSecurableID
)

select xx.ROLE_ID, xx.RolePrincipalID, xx.MemberPrincipalID, xx.STAFF_ID
from xx
where exists(select  * from admin_all.STAFF_AUTH_TBL s where s.STAFFID = xx.Staff_ID)
union all
select -x2.SecurableID as ROLE_ID, NULL as RolePrincipalID, x2.PrincipalID MemberPrincipalID, x2.STAFF_ID --into #1
from x2
where (
		x2.IsInherited = 1 or x2.IsDirectGranted = 1 or x2.ParentRoleGranted is not null or x2.Type = 'PUBLIC'
	)
	and x2.Type <> 'DEPRECATED'

--select * from #1 
go
--select * from security.v_CMSMapping_STAFF_ROLE

--drop table #1