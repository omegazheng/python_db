set ansi_nulls, quoted_identifier on
go
create or alter view security.v_Securable
as
with x0 as
(
	select s.SecurableID, ParentSecurableID, 1 Grantable
	from security.Securable s
--	where SecurableID = 173
	where exists(select * from security.fn_GetSecurityContext() where PrincipalID = 1)
		or exists(
					select * 
					from security.fn_GetSecurityContext() sc
						cross apply security.fn_GetPrincipalSecurable(sc.PrincipalID) ps
					where ps.SecurableID = s.SecurableID
				)
),
x1 as
(
	select x0.SecurableID, x0.ParentSecurableID, Grantable
	from x0
	union all
	select s.SecurableID, s.ParentSecurableID,  0 as Grantable
	from security.Securable s
		inner join x1 on x1.ParentSecurableID = s.SecurableID 
),
x2 as
(
	select x0.SecurableID, x0.ParentSecurableID, Grantable
	from x0
	union all
	select s.SecurableID, s.ParentSecurableID, 1
	from security.Securable s
		inner join x2 on x2.SecurableID = s.ParentSecurableID
),
x3 as
(
	select * from x1
	union 
	select * from x2
)
select s.*, x3.Grantable
from security.Securable s
	inner join x3 on s.SecurableID = x3.SecurableID
go
--select * from security.v_Securable