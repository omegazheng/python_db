set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_GetPrincipalID
(
	@SID varchar(128)
)
returns int
as
begin
	return (select PrincipalID from security.Principal where SID = @SID)
end