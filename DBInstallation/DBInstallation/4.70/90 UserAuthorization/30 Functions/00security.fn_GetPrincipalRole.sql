set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_GetPrincipalRole
(
	@PrincipalID int
)
returns table
as
return 
(
	with x0 as
	(
		select rm.RolePrincipalID, rm.MemberPrincipalID
		from security.RoleMember rm
		where MemberPrincipalID = @PrincipalID
		union all
		select rm.RolePrincipalID, rm.MemberPrincipalID
		from security.RoleMember rm
			inner join x0 on rm.MemberPrincipalID = x0.RolePrincipalID
	)
	select 
		RolePrincipalID, MemberPrincipalID
	from x0 
)