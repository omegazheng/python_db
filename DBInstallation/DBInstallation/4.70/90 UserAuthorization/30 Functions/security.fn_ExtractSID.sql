set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_ExtractSID
(
	@SID varchar(128)
)
returns table
as
return 
(
	select left(@SID, charindex('|', @SID) -1) as PrincipalType,  stuff(@SID, 1, charindex('|', @SID), '') as ID
)
go

--select * from security.fn_ExtractSID('sss|8rurijof')