set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_ComposeSID
(
	@PrincipalType varchar(30),
	@ID sql_variant = null
)
returns varchar(128)
as
begin
	return case when @PrincipalType in ('ROLE') then null else  upper(@PrincipalType)+'|'+cast(@ID as varchar(100)) end
end