set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_GetSID
(
	@PrincipalID int
)
returns varchar(128)
as
begin
	return (select SID from security.Principal where PrincipalID = @PrincipalID)
end