set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_GetOperationByPrincipal(@PrincipalID int)
returns table
as
return 
(
with x0 as
(
	select RolePrincipalID, MemberPrincipalID
	from security.fn_GetPrincipalRole(@PrincipalID)
),
x1 as
(
	select SecurableID
	from security.PrincipalSecurable 
	where PrincipalID = @PrincipalID
),
x2 as
(
	select	s.SecurableID, s.ParentSecurableID, 
			cast(case when exists(select * from x1 where s.SecurableID = x1.SecurableID) then 1 else 0 end as bit) IsDirectGranted,
			case when exists((select x0.RolePrincipalID from x0 where exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID) or (x0.RolePrincipalID = 1 and s.type in ('API', 'DATA')))) then 1 end ParentRoleGranted,
			0 as IsInherited
	from security.Securable s
	where s.ParentSecurableID is null
	union all
	select	s.SecurableID, s.ParentSecurableID,
			cast(case when exists(select * from x1 where s.SecurableID = x1.SecurableID) then 1 else 0 end as bit) IsDirectGranted,
			case when exists((select x0.RolePrincipalID from x0 where exists(select * from security.PrincipalSecurable ps1 where ps1.PrincipalID = x0.RolePrincipalID and ps1.SecurableID = s.SecurableID))) then 1 end ParentRoleGranted,
			case when x2.IsInherited = 1 or x2.IsDirectGranted = 1 or x2.ParentRoleGranted is not null then 1 else 0 end as IsInherited
	from security.Securable s
		inner join x2 on x2.SecurableID = s.ParentSecurableID
)
select distinct so.Operation, oa.Method, oa.API
from x2
	inner join security.SecurableOperation so on so.SecurableID = x2.SecurableID
	left join security.OperationAPI oa on oa.Operation = so.Operation
where (
		x2.IsInherited = 1 or x2.IsDirectGranted = 1 or x2.ParentRoleGranted is not null
		or exists(select * from security.Securable s where s.SecurableID = x2.SecurableID and s.Type = 'PUBLIC')
	)
	and not exists(select * from security.Securable s where s.SecurableID = x2.SecurableID and s.Type = 'DEPRECATED')
)
go



--select * from security.Securable where Type in ('PUBLIC')
--select * from security.fn_GetOperationByPrincipal(6511)
/*

begin transaction

set identity_insert security.Principal on
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(21, 'John Test 1', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(22, 'John Test 2', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(23, 'John Test 3', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(24, 'John Test 4', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(25, 'John Test 5', 'ROLE', -1, getdate(), -1, getdate())
insert into security.Principal(PrincipalID, Name, type, CreatedBy, CreationDate, ModifiedBy, ModificationDate) values(26, 'John Test 6', 'ROLE', -1, getdate(), -1, getdate())
set identity_insert security.Principal off

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(23, 109, 'Grant', -1, getdate())

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(23, 122, 'Grant', -1, getdate())

--insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
--	values(24, 131, 'Grant', -1, getdate())

--insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
--	values(22, 50, 'Grant', -1, getdate())


	--insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	--values(22, 116, 'Grant', -1, getdate())

insert into security.PrincipalSecurable(PrincipalID, SecurableID, Status, CreatedBy, CreationDate)
	values(25, 116, 'Grant', -1, getdate())

insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
			values(24, 23, -1, getdate())
insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
			values(25, 24, -1, getdate())

insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
			values(22, 23, -1, getdate())


--insert security.RoleMember (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
--			values(1, 25, -1, getdate())


select * from security.fn_GetOperationByPrincipal(23)

rollback
*/
