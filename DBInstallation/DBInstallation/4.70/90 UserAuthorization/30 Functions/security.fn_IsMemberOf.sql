set ansi_nulls, quoted_identifier on
go
create or alter function security.fn_IsMemberOf(@MemberPrincipalID int, @RolePrincipalID int)
returns bit
as
begin
	if exists(
				select * 
				from security.fn_GetPrincipalRole(@MemberPrincipalID) a 
				where a.RolePrincipalID = @RolePrincipalID
			)
		return 1
	return 0
end