set ansi_nulls, quoted_identifier on
go
create or alter trigger security.TRI_RoleMember on security.RoleMember
for insert, update, delete
as
begin
	if @@rowcount = 0
		return
	set nocount on
	declare @t table(RolePrincipalID int,  MemberPrincipalID int)
	if exists(
				select * 
				from inserted i
				where exists(select * from security.Principal r where r.PrincipalID = i.RolePrincipalID and r.Type <> 'ROLE')
			)
	begin 
		throw 50000, 'Only ROLE can have member principals.', 16;
	end 
	begin try
		;with x0 as
		(
			select RolePrincipalID,  MemberPrincipalID
			from inserted
			union all
			select a.RolePrincipalID, a.MemberPrincipalID
			from security.RoleMember a
				inner join x0 on a.RolePrincipalID = x0.MemberPrincipalID
		)
		insert into @t(RolePrincipalID,  MemberPrincipalID)
			select RolePrincipalID,  MemberPrincipalID 
			from x0 option(maxrecursion 2)
	end try
	begin catch
		throw 50000, 'Member-Role circular reference is found', 16;
	end catch
	
	
	insert into security.Audit(TableName, OldValues, NewValues, PrincipalID, SID)
	select	'RoleMember', 
			(select d.* for xml raw, type) OldValues,
			(select i.* for xml raw, type) NewValues,
			isnull(sc.PrincipalID, -1),
			isnull(sc.SID, 'Unknown')
	from deleted d
		full outer join inserted i on i.RolePrincipalID = d.RolePrincipalID and i.MemberPrincipalID = d.MemberPrincipalID
		outer apply security.fn_GetSecurityContext() sc
end
go

