set ansi_nulls, quoted_identifier on
go
create or alter trigger security.TRI_OperationAPI on security.OperationAPI
for insert, update, delete
as
begin
	if @@rowcount = 0
		return
	set nocount on 
	insert into security.Audit(TableName, OldValues, NewValues, PrincipalID, SID)
	select	'OperationAPI', 
			(select d.* for xml raw, type) OldValues,
			(select i.* for xml raw, type) NewValues,
			isnull(sc.PrincipalID, -1),
			isnull(sc.SID, 'Unknown')
	from deleted d
		full outer join inserted i on i.Operation = d.Operation and i.Method = d.Method and i.API = d.API
		outer apply security.fn_GetSecurityContext() sc
end
go

