set ansi_nulls, quoted_identifier on
go
if object_id('security.SecurableOperation') is null 
begin
	create table security.SecurableOperation
	(
		SecurableID int not null,
		Operation varchar(128) not null, 
		constraint PK_security_SecurableOperation primary key (SecurableID, Operation),
		constraint UQ_security_SecurableOperation unique(Operation, SecurableID),
		constraint FK_security_SecurableOperation_Securable foreign key(SecurableID) references security.Securable (SecurableID),
		constraint FK_security_SecurableOperation_Operation foreign key(Operation) references security.Operation (Operation),
	)
end
go
--drop table 
/*
insert into security.SecurableOperation
select ROLE_ID, OPERATION_NAME from admin_all.AUTH_ROLE_OPERATION
*/