set ansi_nulls, quoted_identifier on
go
if object_id('security.OperationAPI') is null 
begin
	create table security.OperationAPI
	(
		Operation varchar(128) not null,
		Method varchar(10) not null,
		API varchar(600) not null,
		constraint PK_security_OperationAPI primary key (Operation, Method, API),
		constraint FK_security_OperationAPI_Operation foreign key (Operation) references security.Operation(Operation)
	)
end
--drop table security.OperationAPI
/*
insert into security.OperationAPI
select * from admin_all.AUTH_OPERATION_API
*/

--select * from security.OperationAPI