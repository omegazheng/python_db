set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.Resource') is null
begin
	create table admin_all.Resource
	(
        Operation nvarchar (255) NULL,
        Url [nvarchar](255) NULL
	)
end
go

if not exists (select * from admin_all.RESOURCE)
begin
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'2KRuleReportView', N'/j/TwoKRuleReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AddRole', N'/j/role/Role.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AddUser', N'/j/staff/StaffAdd.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AffiliateActivityReportView', N'/j/reports/AffiliateActivityReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AlertLogsView', N'/j/admin/AlertLog.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AMLAlertReportView', N'/j/reports/PlayerAMLReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BalanceTransactionTagConfigView', N'/j/config/AdjustBalanceTransactionTag.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BankingActivityReportView', N'/j/reports/BankingActivityReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BatchedWithdrawalsReportView', N'/j/reports/BatchedWithdrawals.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BigWinsReportView', N'/j/reports/BigWinsReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BingoReportView', N'/j/reports/ParlayBingoReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BonusListView', N'/j/bonus/ListBonuses.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BonusPlanAccountingSummaryView', N'/j/bonus/BonusPlanAccountingSummary.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BonusPlansListView', N'/j/bonus/ListBonusPlans.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BonusPlanSummaryView', N'/j/bonus/BonusPlanSummary.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BonusWagerContributionRatioView', N'/j/admin/BonusWagerContributionRatio.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BrandContentView', N'/j/admin/BrandContent.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BrandCountryConfigurationView', N'/j/config/BrandCountry.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BrandListView', N'/j/config/BrandConfig.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BrandLoyaltyView', N'/j/config/BrandLoyalty.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BrandRegistryHashView', N'/j/admin/BrandRegistry.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CashLiabilityReportView', N'/j/reports/CashLiabilityReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CCLevelReportView', N'/j/reports/CCLEVELReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CommentTagView', N'/j/config/CommentTag.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CoolDownPeriodConfigView', N'/j/config/PlayerDepositLimitCoolDownPeriodConfig.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/CreateBonusPlan.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreditCardLimitView', N'/j/config/CreditCardLevel.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CRMQueryBuilder', N'/j/crm/CrmQueryBuilder.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CurrencyConversionsListView', N'/j/ListCurrencyConversion.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DepositLimitConfigView', N'/j/DepositLimit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DepositsAndWithdrawalsReportView', N'/j/reports/DepositsAndWithdrawalsReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DevView', N'')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DuplicateAccountReportView', N'/j/DuplicateAccountsReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'EmailExport', N'/j/EmailExport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'EmailTemplatesView', N'/j/EmailTemplate.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'FirstDepositReportView', N'/j/reports/FirstDepositReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameCategoryView', N'/j/admin/BonusWagerContributionRatio.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/admin/GameCategory.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameLoyaltyView', N'/j/config/LoyaltyGameConfig.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GoogleSearchTrackingReportView', N'/j/reports/GoogleTrackingReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'HomeDashboardView', N'/j/CmsDashboard.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'IovationReportView', N'/j/IovationReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JackpotContributionRatioView', N'/j/admin/GameJackpotContributionRatio.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JackpotContributionReportView', N'/j/report/revenue/RevenueByJPContribution.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/JobRunner.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KPIAccountingReportView', N'/j/kpi/KpiAccounting.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KPIAccountingSnapshotReportView', N'/j/kpi/KpiAccountingSnapshot.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KPIDashboardView', N'/j/kpi/KpiDashboard.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KPIEventsReportView', N'/j/kpi/KpiRevenueCompare.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KPIRevenueReportView', N'/j/kpi/KpiRevenue.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KPIRevenueSnapshotView', N'/j/kpi/KpiRevenueSnapshot.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KYCReportView', N'/j/KycReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'LoyaltyCurrencyRatioView', N'/j/config/LoyaltyCurrencyRatio.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'LoyaltyItemView', N'/j/config/LoyaltyItemConfig.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'LoyaltyPointExpirationReportView', N'/j/reports/LPExpirationReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'LoyaltyPointUpcomingExpirationReportView', N'/j/reports/LPUpcomingExpirationReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'LoyaltyPurchaseHistoryView', N'/j/loyalty/ListPurchase.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'ManualBankAgentsView', N'/j/admin/ManualBankAgent.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'ManualBankReportView', N'/j/cashier/ManualBankPayments.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGameCategoryView', N'/j/config/OrderMobileGameCategory.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/AdjustBalance.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/BettingDetailUpdator.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/BonusTranUpdaterJob.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/CreateEncryptionKeys.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/DailyEmailExportUploaderJob.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/DailyIncomeAccessUploaderJob.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/GameInfo.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/RenewEncryptionKeys.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/SswTranLog.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'JobRunner', N'/j/admin/UpdateMasterKey.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerBonusDelete', N'/j/bonus/CancelBonus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/ChangeBonusPlanStatus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/EditBonusPlan.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/EditBonusPlanTournWin.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/ReleaseSpentBonus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/SequentialBonusPlans.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/ViewBonusPlan.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/ViewBonusPlanTournWin.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/bonus/ViewFreeBetBonusBalance.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerBulkUpload', N'/j/BulkUploadStep2.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Cashier', N'/j/cashier/ManualBankProcess.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Cashier', N'/j/cashier/PlayerSearch.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Cashier', N'/j/cashier/PragmaticBingoPrePurchase.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Cashier', N'/j/cashier/Withdrawal.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/CmsLanguageSwitcher.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/ComTradeGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandConfigExport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandConfigImport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandConfigurationBrandAlias.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandConfigurationBrandPlatform.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandConfigurationCountry.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandConfigurationGameProviders.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandConfigurationSignUp.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-PlatformView', N'/j/config/BrandLanguage.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoEdit', N'/j/config/GameInfo.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGamesView', N'/j/config/LoadMobileImage.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGameCategoryView', N'/j/config/MobileGameCategory.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGameCategoryView', N'/j/config/MobileGameCategoryLink.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGameCategoryView', N'/j/config/MobileGameCategoryTranslation.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGameCategoryView', N'/j/config/MobileGameTranslation.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGameCategoryView', N'/j/config/OrderMobileGameCategoryLink.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'SubPlatformConfigView', N'/j/config/SubPlatformConfig.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CurrencyConversionsListView', N'/j/CreateCurrencyConversion.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CurrencyConversionsListView', N'/j/CreateCurrencyConversionRate.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'SystemStatusView', N'/j/dev/SystemStatus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/DigitainSportsBookGetOrderView.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/DisplayMessage.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BatchedWithdrawalsReportView', N'/j/DownloadBatchWithdrawal.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DuplicateAccountReportView', N'/j/DuplicateAccountSearchReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TagCountReportView', N'/j/DuplicateTagStatus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/EditCurrencyConversion.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KYCReportView', N'/j/ExperianCheck.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerFreePlayReport', N'/j/freePlay/FreePlayPlan.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/IgtGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'IovationReportView', N'/j/IovationCheckChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'KPIRevenueReportView', N'/j/kpi/KpiRevenueCompare.action_disable')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/KycAgeStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/KycStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/NetentGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/NYXGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/ParlayGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (NULL, N'/j/player/AbstractPlayer.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ActiveStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/AddComment.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/AddCommentSimple.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/AdjustBalance.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/AdjustLoyalty.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerBonusApply', N'/j/player/ApplyBonusCode.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoEdit', N'/j/player/BIABetSlipDetail.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/BlueprintGameDetail.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/player/BonusIncrementalViewDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'LoyaltyItemView', N'/j/player/CashInLoyalty.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/CCLevelChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerTransactionReportView', N'/j/player/DashurTransactionDetail.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/player/DisplayMessageStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/DocumentDownload.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/DocumentUpload.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/DocumentView.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/EditEGameToken.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/EditPlayer.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/EditPlayerLimits.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerFreePlayReport', N'/j/player/freePlayPromotion.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/FullPlayerProfile.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoEdit', N'/j/player/GamePreferenceChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/GreentubeGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/KillUserSessions.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/LeanderGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ListComments.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ListKYCHistory.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ListPlayerWalletTransfer.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ListReferFriend.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ListUserActionLog.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/LockedStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/LoginHistory.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/NetentGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/NolimitCityGameDetail.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/nullLoginByAdmin.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/PlayCheck.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/player/PlayerBonusDashboard.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerBonusView', N'/j/player/PlayerBonusList.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WhitelistEdit', N'/j/player/PlayerCCWhiteList.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/PlayerDashboard.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDepositLimitEdit', N'/j/player/PlayerDepositLimit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDepositReportView', N'/j/player/PlayerDeposits.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DuplicateAccountReportView', N'/j/player/PlayerDuplicates.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/PlayerEGame.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerGamePlayReportView', N'/j/player/PlayerEGameHistory.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerFreePlayReport', N'/j/player/PlayerFreePlayPromotion.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/PlayerGameDetailList.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerGamePlayReportView', N'/j/player/PlayerGamePlaySummary.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerTransactionReportView', N'/j/player/PlayerTransactions.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TransactionsReportView', N'/j/player/PlayerTransfers.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WithdrawalsReportView', N'/j/player/PlayerWithdrawals.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/RegistrationStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ResetPassword.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/player/SendCompleteRegistration.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/SendConfirmation.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'EmailControl', N'/j/player/SendWithdrawalLockedEmail.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/SubscriptionStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/player/tempGameDetail.html')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/TrackingCode.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'CreateBonus', N'/j/player/ViewBonusAccountTransaction.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'Maintenance-GameView', N'/j/player/ViewGameInstance.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/ViewPlayerTickets.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TransactionsReportView', N'/j/player/ViewSportsBetTransaction.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (NULL, N'/j/player/ViewSportsTicket.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDashboardEdit', N'/j/player/VIPStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/player/WinnersListStatusChange.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerSearch', N'/j/PlayerSearch.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/PlayNGoGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/PlaysonGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DevView', N'/j/preview/AffiliateParentPreview.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AlertLogsView', N'/j/preview/AlertLogPreview.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AffiliateActivityReportView', N'/j/preview/BalancePreview.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BankingActivityReportView', N'/j/preview/BankInfoPreview.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DevView', N'/j/preview/PaymentPreview.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DevView', N'/j/preview/PlatformConversionPreview.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'DevView', N'/j/preview/PlayerPreview.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/ProcessWithdrawal.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BatchedWithdrawalsReportView', N'/j/ProcessWithdrawalBatch.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'BatchedWithdrawalsReportView', N'/j/ProcessWithdrawalBatchBank.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'GameInfoView', N'/j/QuickSpinGameDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WithdrawalsReportView', N'/j/reports/BatchedWithdrawalDetails.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/reports/BatchStatementUpload.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RoleListView', N'/j/role/RoleMember.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RoleListView', N'/j/role/RolePermission.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RoleListView', N'/j/role/RoleResource.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/role/StaffMemberRole.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WorkingReportView', N'/j/staff/StaffAccessLog.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WorkingReportView', N'/j/staff/StaffActionLog.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/staff/StaffActiveStatus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AddUser', N'/j/staff/StaffEdit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/staff/StaffLockedStatus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/staff/StaffMemberLimit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AddUser', N'/j/staff/StaffOwnPasswordEdit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AddUser', N'/j/staff/StaffPasswordEdit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/staff/StaffReportEmail.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/staff/StaffView.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/role/RoleMember.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RoleListView', N'/j/testAddResource.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RoleListView', N'/j/UnauthorizedResourceAccess.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerSignup', N'j/PlayerSignup.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TagCountReportView', N'j/TagCountReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'', N'/j/reports/BatchStatementUpload.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AddUser', N'/j/role/StaffRoleRedirect.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/role/StaffRoleRedirect.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerRegistrationView', N'/j/RegistrationsReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerFirstDepositView', N'/j/reports/FirstDepositReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerPlayingView', N'/j/PlayersPlaying.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerChangeLogView', N'/j/reports/PlayerChangeReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerBulkUploadNew', N'/j/BulkUploadStep1.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerBulkUploadNew', N'/j/BulkUploadStep2.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'MobileGamesView', N'/j/config/MobileGame.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PaymentMessageReportView', N'/j/reports/PaymentMessageReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PaymentMethodConfigView', N'/j/config/PaymentMethodPerBrand.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PaymentMethodDisplayConfigView', N'/j/config/PaymentMethodDisplay.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PendingWithdrawalsReportView', N'/j/PendingWithdrawals.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlatformConfigView', N'/j/config/PlatformConfig.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerAgingReportView', N'/j/PlayerAging.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerBulkUpload', N'/j/BulkUploadStep1.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerChangeReportView', N'/j/reports/PlayerChangeReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerCommentTagReportView', N'/j/PlayerCommentTagReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDefaultDepositLimitConfigView', N'/j/PlayerDefaultDepositLimit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerDepositActivityReportView', N'/j/reports/PlayerDepositsActivity.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerExport', N'/j/PlayerExport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerLockReasonReportView', N'/j/reports/PlayerLockReasonReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerPlayingReportView', N'/j/PlayersPlaying.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerSearchReportView', N'/j/PlayerSearch.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerSegmentationReportView', N'/j/reports/PlayerSegmentationReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerSignup', N'/j/PlayerSignup.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PlayerSpendAnalysisView', N'/j/reports/PlayerPaymentReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'PokerReportView', N'/j/reports/PokerReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'ProtectionPolicyReset', N'/j/config/ProtectionPolicy.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RegistrationReportView', N'/j/RegistrationsReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RegistrationsNoActivityReportView', N'/j/reports/RegistrationsNoActivityReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RegistryHashView', N'/j/admin/RegistryHash.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'ReorderBonus', N'/j/bonus/OrderBonusPlans.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByAffiliateReportView', N'/j/report/revenue/RevenueByAffiliate.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByBrandReportView', N'/j/report/revenue/RevenueByBrand.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByDayReportView', N'/j/report/revenue/RevenueByDay.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByGameByPlayerReportView', N'/j/report/revenue/RevenueByGameByPlayer.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByGameCategoryReportView', N'/j/report/revenue/RevenueByGameCategory.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByGameReportView', N'/j/report/revenue/RevenueByGame.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByPeriodReportView', N'/j/report/revenue/RevenueByPeriod.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByPeriodReportView_DISABLED', N'/j/report/revenue/RevenueByPeriod.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByPlayerByDayByGameReportView', N'/j/report/revenue/RevenueByPlayerByDayByGame.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByPlayerByDayReportView', N'/j/report/revenue/RevenueByPlayerByDay.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByPlayerReportView', N'/j/report/revenue/RevenueByPlayer.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueByProductReportView', N'/j/report/revenue/RevenueByPlatform.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RevenueBySubProductReportView', N'/j/report/revenue/RevenueBySubPlatform.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'RoleListView', N'/j/role/Role.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'SystemStatusView', N'/j/reports/SystemStatus.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TagCountReportView', N'/j/TagCountReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TradeDoublerReportExport', N'/j/reports/TradeDoublerReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TransactionsReportView', N'/j/reports/TransactionReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'TransactionTagReportView', N'/j/reports/TransactionTagReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'UserListView', N'/j/staff/StaffList.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'VIPBankConfigView', N'/j/admin/ManualBankVIPConfig.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WithdrawalLimitConfigView', N'/j/WithdrawalLimit.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WithdrawalsReportView', N'/j/reports/WithdrawalsReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'WorkingReportView', N'/j/staff/WorkingReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'XNumberOfDepositReportView', N'/j/reports/OnlyNDepositReport.action')
INSERT [admin_all].[RESOURCE] ([OPERATION], [URL]) VALUES (N'AgentNetworkManage', N'/agency')
end
go

