set ansi_nulls, quoted_identifier on
go
if object_id('security.RoleMember') is null 
begin
	create table security.RoleMember
	(
		RolePrincipalID int not null,
		MemberPrincipalID int not null,
		CreatedBy int not null,
		CreationDate datetime not null,
		constraint PK_security_RoleMember primary key (RolePrincipalID, MemberPrincipalID) with(ignore_dup_key = on),
		constraint PQ_security_RoleMember unique (MemberPrincipalID, RolePrincipalID) with(ignore_dup_key = on),
		constraint FK_security_RoleMember_RolePrincipalID_Principal foreign key (RolePrincipalID) references security.Principal(PrincipalID),
		constraint FK_security_RoleMember_MemberPrincipalID_Principal foreign key (RolePrincipalID) references security.Principal(PrincipalID)
	)
end

--drop table security.RoleMember