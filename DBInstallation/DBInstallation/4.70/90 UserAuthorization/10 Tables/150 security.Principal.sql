set ansi_nulls, quoted_identifier on
go
if object_id('security.Principal') is null 
begin
	create table security.Principal
	(
		PrincipalID int identity(1000, 1) not null,
		Name nvarchar(128) not null,
		Type varchar(30) not null,
		Description nvarchar(512) null,
		SID varchar(128), 
		CreatedBy int not null,
		CreationDate datetime not null,
		ModifiedBy int not null,
		ModificationDate datetime not null,
		constraint PK_security_Principal primary key (PrincipalID),
		constraint UQ_security_Principal_Name unique (Name),
		index IDX_security_Principal_Type_SID(SID) where SID is not null
	)
end
go
--drop table security.Principal
