set ansi_nulls, quoted_identifier on
go
if object_id('security.Audit') is null 
begin
	create table security.Audit
	(
		AuditID bigint identity(1,1) not null,
		AUditDate datetime not null constraint DF_security_Audit_AuditDate default(getdate()),
		ApplicationName varchar(128) constraint DF_security_Audit_ApplicationName default(app_Name()),
		HostName varchar(128) constraint DF_security_Audit_HostName default(host_name()),
		LoginName varchar(128) constraint DF_security_Audit_LoginName default(system_user),
		TableName varchar(128) not null,
		OldValues xml,
		NewValues xml,
		PrincipalID int not null,
		SID varchar(128) not null,
		constraint PK_security_Audit primary key (TableName, AuditID) with(data_compression = page)
	)
	exec sp_tableoption 'security.Audit', 'large value types out of row', 1
end
--drop table security.Audit