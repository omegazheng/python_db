set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_Revoke
(
	@PrincipalID int,
	@SecurableIDs varchar(max)
)
as
begin
	set nocount, xact_abort on
	
	delete s
	from security.PrincipalSecurable s
	where PrincipalID = @PrincipalID 
		and exists(
					select v
					from (
							select try_cast(rtrim(ltrim(value)) as int)  as v
							from string_split(@SecurableIDs, ',')
						) a
					where v = s.SecurableID
				)
end