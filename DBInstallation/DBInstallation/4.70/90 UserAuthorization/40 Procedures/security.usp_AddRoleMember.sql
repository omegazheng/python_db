set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_AddRoleMember
(
	@RolePrincipalID varchar(max),
	@MemberPrincipalIDs varchar(max),
	@OverWriteType  varchar(20) = 'NONE' -- ROLE, MEMBER
)
as
begin

	set nocount, xact_abort on
	--if security.fn_IsAdmin() = 0
	--	throw 50000, 'Only administrator can perform this operation', 16;
	declare @PrincipalID int
	select @PrincipalID = isnull((select PrincipalID from security.fn_GetSecurityContext()), 0)


	select distinct v as MemberPrincipalID into #MemberPrincipals
	from (
			select try_cast(rtrim(ltrim(value)) as int)  as v
			from string_split(@MemberPrincipalIDs, ',')
		) a
	where v is not null

	select distinct v as RolePrincipalID into #RolePrincipals
	from (
			select try_cast(rtrim(ltrim(value)) as int)  as v
			from string_split(@RolePrincipalID, ',')
		) a
	where v is not null

	if exists(
				select *
				from security.Principal p
					left join #RolePrincipals r on r.RolePrincipalID = p.PrincipalID
					left join #MemberPrincipals m on m.MemberPrincipalID = p.PrincipalID
				where (r.RolePrincipalID is not null and m.MemberPrincipalID is not null and r.RolePrincipalID = m.MemberPrincipalID) --- add role to itself
					or (r.RolePrincipalID is not null and p.Type <> 'ROLE') -- role member is not a ROLE
				)
	begin
		throw 50000, 'Please ensure RolePrincipals are ROLE principals and ensure it''s not added to itself.', 16;
	end
	
	;with s as
	(
		select RolePrincipalID, MemberPrincipalID
		from #RolePrincipals
			cross apply #MemberPrincipals
				
	),
	t as 
	(
		select *
		from security.RoleMember rm
		where @OverWriteType = 'ROLE' and exists(select * from #MemberPrincipals mp where mp.MemberPrincipalID = rm.MemberPrincipalID)
			or @OverWriteType = 'MEMBER' and exists(select * from #RolePrincipals rp where rp.RolePrincipalID = rm.RolePrincipalID)
			or @OverWriteType = 'NONE'
	)
	merge t 
	using s on t.RolePrincipalID = s.RolePrincipalID and t.MemberPrincipalID= s.MemberPrincipalID
	when not matched then
		insert (RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
			values(s.RolePrincipalID, s.MemberPrincipalID, @PrincipalID, getdate())
	when not matched by source and @OverWriteType in ('ROLE', 'MEMBER') then
		delete
	--output inserted.*
	;
end
