set ansi_nulls, quoted_identifier on
go
if object_id('security.usp_DataInitializationPrincipal') is null
  exec('create procedure security.usp_DataInitializationPrincipal as ')
go
alter procedure security.usp_DataInitializationPrincipal
as
begin
    set nocount on
    begin transaction
        set identity_insert [security].[Principal] on;
        ;with s as
            (
            select [PrincipalID],[Name],[Type],[Description], [SID], [CreatedBy], [CreationDate], [ModifiedBy], [ModificationDate]
            from (
                values (N'1',N'System',N'ROLE', N'System Reserved Role with Role Management Capability', null , N'-1', N'2019-01-01T00:00:00', N'-1', N'2019-01-01T00:00:00'),
                    (N'2',N'Omega Staff',N'STAFF', N'System Reserved Staff', 'STAFF|-1' , N'-1', N'2019-01-01T00:00:00', N'-1', N'2019-01-01T00:00:00')
                 ) v([PrincipalID],[Name],[Type],[Description], [SID], [CreatedBy], [CreationDate], [ModifiedBy], [ModificationDate])
            )
            merge security.Principal t
        using s on s.[PrincipalID]= t.[PrincipalID]
        when not matched then
            insert ([PrincipalID],[Name],[Type],[Description], [SID], [CreatedBy], [CreationDate], [ModifiedBy], [ModificationDate])
            values(s.[PrincipalID],s.[Name],s.[Type],s.[Description], s.[SID],
                   s.[CreatedBy], s.[CreationDate], s.[ModifiedBy], s.[ModificationDate])
        when matched and (s.[Name] is null and t.[Name] is not null or s.[Name] is not null and t.[Name] is null or s.[Name] <> t.[Name]) then
            update set t.[Type]= s.[Type], t.[SID]= s.[SID], t.[CreatedBy]= s.[CreatedBy], t.[CreationDate]= s.[CreationDate],
                       t.[ModifiedBy]= s.[ModifiedBy], t.[ModificationDate]= s.[ModificationDate]
        ;
        set identity_insert [security].[Principal] off;
    commit;

    begin transaction
        ;with s as
            (
            select [RolePrincipalID],[MemberPrincipalID],[CreatedBy], [CreationDate]
            from (
                     values (N'1',N'2', N'-1', N'2019-01-01T00:00:00')
                 ) v([RolePrincipalID],[MemberPrincipalID], [CreatedBy], [CreationDate])
            )
            merge security.RoleMember t
        using s on s.[RolePrincipalID]= t.[RolePrincipalID] and s.[MemberPrincipalID] = t.[MemberPrincipalID]
        when not matched then
            insert ([RolePrincipalID],[MemberPrincipalID],[CreatedBy], [CreationDate])
            values(s.[RolePrincipalID],s.[MemberPrincipalID], s.[CreatedBy], s.[CreationDate]);
    commit;

    begin transaction
        ;with s as
            (
            select [PrincipalID],[SecurableID],[Status],[CreatedBy],[CreationDate]
            from (
                     values (N'1',N'1', N'GRANT', N'-1', N'2019-01-01T00:00:00')
                 ) v([PrincipalID],[SecurableID], [Status], [CreatedBy], [CreationDate])
            )
            merge security.PrincipalSecurable t
        using s on s.[PrincipalID]= t.[PrincipalID] and s.[SecurableID] = t.[SecurableID]
        when not matched then
            insert ([PrincipalID],[SecurableID],[Status],[CreatedBy], [CreationDate])
            values(s.[PrincipalID],s.[SecurableID],s.[Status], s.[CreatedBy], s.[CreationDate]);
    commit;
end
