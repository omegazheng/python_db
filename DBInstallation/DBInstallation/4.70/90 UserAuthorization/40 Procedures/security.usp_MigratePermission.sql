set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_MigratePermission
as
begin
	set nocount, xact_abort on
	if object_id('admin_all.Role') is null or object_id('admin_all.STAFF_ROLE') is null or object_id('admin_all.ROLE_RESOURCE') is null
		return
	declare @OperationAction table(Operation varchar(128), Action varchar(600))
	
	if object_id('admin_all.RESOURCE') is not null
	begin
		insert into @OperationAction(Operation, Action)
			exec('select distinct OPERATION, URl from admin_all.RESOURCE where OPERATION is not null and URL is not null')
	end
	declare @Date datetime = getdate()
	declare @CMSAdminRoleID int = 1
	select @CMSAdminRoleID = ID
	from admin_all.ROLE
	where ROLE_NAME = 'Administrator'

	begin transaction


	select identity(int, 501, 1) as PrincipalID, cast(ID as int) as ID, ROLE_NAME, ROLE_DESCRIPTION into #Role from admin_all.Role where ID >0 order by ID 

	create unique clustered index #Role on #Role(ID)
	update r
		set r.ROLE_NAME = r.ROLE_NAME +'('+cast(r.PrincipalID as varchar(10))+')'
	from #Role r
	where not exists(select * from security.Principal p where p.PrincipalID = r.PrincipalID)
		and exists(select * from security.Principal p where p.Name = r.ROLE_NAME)

	set identity_insert security.Principal on
	insert into security.Principal(PrincipalID, Name, Type, Description, SID, CreatedBy, CreationDate, ModifiedBy, ModificationDate)
		select case when r.ID =  @CMSAdminRoleID then 1 else r.PrincipalID end, ROLE_NAME, 'ROLE', ROLE_DESCRIPTION, NULL, -1, @Date, -1, @Date
		from #Role r
		where not exists(select * from security.Principal p where p.PrincipalID = case when r.ID =  @CMSAdminRoleID then 1 else r.PrincipalID end)
	set identity_insert security.Principal off
	

	
	;with x0 as
	(
		select distinct security.fn_ComposeSID('STAFF', sr.STAFFID) as Name, security.fn_ComposeSID('STAFF', sr.STAFFID) SID
		from (select STAFFID from admin_all.STAFF_AUTH_TBL)sr
			inner join admin_all.STAFF_AUTH_TBL s on sr.STAFFID = s.STAFFID
	)
	insert into  security.Principal(Name, SID, CreatedBy, CreationDate, ModifiedBy, ModificationDate, Type, Description)
		select Name, SID, -1, @Date, -1, @Date, 'STAFF', null
		from x0
		where not exists(select * from security.Principal p where p.SID = x0.SID)
	;
	--select * from security.RoleMember
	;with x0 as
	(
		select security.fn_GetPrincipalID( security.fn_ComposeSID('STAFF', sr.STAFF_ID)) MemberPrincipalID, case when s.SUPER_ADMIN = 1 then 1 else case when r.ID =  @CMSAdminRoleID then 1 else r.PrincipalID end end RolePrincipalID
		from admin_all.STAFF_ROLE sr
			inner join #Role r on r.ID = sr.ROLE_ID
			inner join admin_all.STAFF_AUTH_TBL s on s.STAFFID = sr.STAFF_ID 
		where sr.ROLE_ID > 0
	)
	insert into security.RoleMember(RolePrincipalID, MemberPrincipalID, CreatedBy, CreationDate)
		select distinct RolePrincipalID, MemberPrincipalID, -1, @Date
		from  x0
		where not exists(select * from security.RoleMember rm where rm.RolePrincipalID = x0.RolePrincipalID and rm.MemberPrincipalID = x0.MemberPrincipalID)

	--select * from security.RoleMember
	
	--select * from security.Operation
	--select * from security.Operation order by 1
	insert into security.Operation(Operation, Description)
		select distinct Operation, Operation
		from @OperationAction 
		where Operation not in (select Operation from security.Operation)
	
	--select * from security.Operation order by 1
	--select * from security.OperationAPI order by 1	,2,3
	insert into security.OperationAPI(Operation, API, Method)
		select oa.Operation, oa.Action, 'ACTION'
		from @OperationAction oa
		where not exists(select * from security.OperationAPI soa where soa.Operation = oa.Operation and soa.API = oa.Action and soa.Method = 'ACTION')
	--select * from security.OperationAPI order by 1	,2,3
	--select * from security.PrincipalSecurable
	
	;with x0 as
	(	
		select distinct case when r.ID =  @CMSAdminRoleID then 1 else r.PrincipalID end PrincipalID, so.SecurableID
		from admin_all.ROLE_RESOURCE rr
			inner join #Role r on rr.ROLE_ID = r.ID
			inner join @OperationAction oa on oa.Action = rr.URL
			inner join  security.SecurableOperation so on so.Operation = oa.Operation
		where r.ID <> 1 and rr.ROLE_ID >0
	)
	insert into security.PrincipalSecurable(PrincipalID, SecurableID, CreatedBy, CreationDate, Status)
		select distinct x0.PrincipalID, x0.SecurableID, -1, @Date, 'GRANT'
		from x0
		where not exists(select * from security.PrincipalSecurable ps where ps.PrincipalID = x0.PrincipalID and ps.SecurableID = x0.SecurableID)
	--select * from security.PrincipalSecurable

	
	;with x0 as
	(
		select  * 
		from (
				values('HIDE_DATA', 'EMAIL', 'SHOW_EMAIL'),
						('HIDE_DATA', 'PHONE', 'SHOW_PHONE'),
						('HIDE_DATA', 'MOBILEPHONE', 'SHOW_MOBILE'),
						('ENABLE_EXPORT', '', 'ENABLE_EXPORT')
			) pm(Type, SubType, SecurableName)
	),
	x1 as
	(
		select distinct ROLE_ID
		from admin_all.ROLE_PERMISSION
		where Role_ID <>1
			and ROLE_ID > 0
	),
	x2 as
	(
		select x1.ROLE_ID, x0.SecurableName
		from x0 
			cross join x1
			left join admin_all.ROLE_PERMISSION rp on rp.ROLE_ID = x1.ROLE_ID and rp.TYPE = x0.Type and isnull(rp.SUBTYPE, '') = x0.SubType  and rp.ROLE_ID > 0
		where x0.Type = 'HIDE_DATA' and rp.ROLE_ID is null
			or x0.Type = 'ENABLE_EXPORT' and rp.ROLE_ID is not null
	)
	insert into security.PrincipalSecurable(PrincipalID, SecurableID, CreatedBy, CreationDate, Status)
		select distinct case when r.ID = @CMSAdminRoleID then 1 else r.PrincipalID end, s.SecurableID, -1, @Date, 'GRANT'
		from x2
			inner join security.Securable s on s.Type = 'Data' and s.Name = x2.SecurableName
			inner join #Role r on x2.ROLE_ID = r.ID
		where not exists(select * from security.PrincipalSecurable ps where ps.PrincipalID = case when r.ID = @CMSAdminRoleID then 1 else r.PrincipalID end and ps.SecurableID = s.SecurableID)

	insert into security.PrincipalSecurable(PrincipalID, SecurableID, CreatedBy, CreationDate, Status)
		select distinct p.PrincipalID, s.SecurableID, -1, @Date, 'GRANT'
		from admin_all.AUTH_PERMISSION ap
			cross apply(
							select a.SID, security.fn_GetPrincipalID(a.SID) PrincipalID
							from (
										select security.fn_ComposeSID('STAFF', ap.USER_ID) SID
								) a
						) p
			inner join security.Securable s on s.Name = ap.PERMISSION_TYPE and s.Type = 'Data'
		where ap.USER_TYPE = 'STAFF' 
			and p.PrincipalID is not null and s.SecurableID is not null
			and not exists(select * from security.PrincipalSecurable ps where ps.PrincipalID = p.PrincipalID and ps.SecurableID = s.SecurableID)



	;with x0 as
	(
		select distinct security.fn_ComposeSID(case when ur.USER_TYPE = 'agency' then 'AGENT' else 'STAFF' end, USER_ID) as Name, security.fn_ComposeSID(case when ur.USER_TYPE = 'agency' then 'AGENT' else 'STAFF' end, USER_ID) SID, case when ur.USER_TYPE = 'agency' then 'AGENT' else 'STAFF' end Type
		from admin_all.AUTH_USER_ROLE ur
			left join admin_all.STAFF_AUTH_TBL s on s.STAFFID = ur.USER_ID and ur.USER_TYPE = 'STAFF'
			left join external_mpt.USER_CONF u on u.PARTYID = ur.USER_ID and ur.USER_TYPE = 'agency'
		where (s.STAFFID is not null or u.PARTYID is not null)
	)
	insert into  security.Principal(Name, SID, CreatedBy, CreationDate, ModifiedBy, ModificationDate, Type, Description)
		select Name, SID, -1, @Date, -1, @Date, x0.Type, null
		from x0
		where not exists(select * from security.Principal p where p.SID = x0.SID)
	;
	
	
	;with x0 as
	(	
		select distinct security.fn_GetPrincipalID(security.fn_ComposeSID(case when ur.USER_TYPE = 'agency' then 'AGENT' else 'STAFF' end, USER_ID)) PrincipalID, ROLE_ID SecurableID 
		from admin_all.AUTH_USER_ROLE ur
	)
	insert into security.PrincipalSecurable(PrincipalID, SecurableID, CreatedBy, CreationDate, Status)
		select distinct x0.PrincipalID, x0.SecurableID, -1, @Date, 'GRANT'
		from x0
		where 
			--not exists(
			--				select * 
			--				from security.fn_GetPrincipalRole(PrincipalID) a 
			--				where a.RolePrincipalID = 1
			--		)
			--and 
			not exists(select * from security.PrincipalSecurable ps where ps.PrincipalID = x0.PrincipalID and ps.SecurableID = x0.SecurableID)
			and x0.PrincipalID is not null
			and x0.SecurableID is not null
	--select * from security.PrincipalSecurable
	commit
end
go
--exec security.usp_MigratePermission
--begin transaction
----delete security.RoleMember where MemberPrincipalID between 502 and 513
----delete security.RoleMember where RolePrincipalID between 502 and 513
----delete security.PrincipalSecurable where PrincipalID between 502 and 513

--exec security.usp_RemovePrincipal 502
--exec security.usp_RemovePrincipal 503
--exec security.usp_RemovePrincipal 504
--exec security.usp_RemovePrincipal 505
--exec security.usp_RemovePrincipal 506
--exec security.usp_RemovePrincipal 507
--exec security.usp_RemovePrincipal 508
--exec security.usp_RemovePrincipal 509
--exec security.usp_RemovePrincipal 510
--exec security.usp_RemovePrincipal 511
--exec security.usp_RemovePrincipal 512
--exec security.usp_RemovePrincipal 513
--exec security.usp_MigratePermission
--select * from security.principal 
--commit