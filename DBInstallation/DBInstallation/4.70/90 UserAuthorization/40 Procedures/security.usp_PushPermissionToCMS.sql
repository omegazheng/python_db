set ansi_nulls, quoted_identifier on
go
create or alter procedure security.usp_PushPermissionToCMS
as
begin
	set nocount on
	begin transaction

	declare @SQL nvarchar(max)
	select @SQL = (select 'alter table '+quotename(object_schema_name(referenced_object_id)) + '.' + quotename(object_id(referenced_object_id)) + ' drop constraint ' + quotename(name) + ';' from sys.foreign_keys where referenced_object_id = object_id('admin_all.ROLE') for xml path(''), type).value('.', 'varchar(max)')
	exec(@SQL)

	select @SQL = (select 'alter table '+quotename(object_schema_name(referenced_object_id)) + '.' + quotename(object_id(referenced_object_id)) + ' drop constraint ' + quotename(name) + ';' from sys.foreign_keys where referenced_object_id = object_id('admin_all.ROLE_RESOURCE') for xml path(''), type).value('.', 'varchar(max)')
	exec(@SQL)

	select * into #Role from security.v_CMSMapping_ROLE
	--select * from security.v_CMSMapping_ROLE
	--select * from admin_all.ROLE
	set identity_insert admin_all.ROLE on
	;merge admin_all.ROLE t
	using #Role s on t.ID = s.ROLE_ID
	when not matched by source then
		delete
	when not matched then 
		insert(ID, ROLE_NAME, ROLE_DESCRIPTION)
			values(s.ROLE_ID, 'System-'+ cast(s.ROLE_ID as varchar(20)), 'System-'+ cast(s.ROLE_ID as varchar(20)))
	;
	set identity_insert admin_all.ROLE off


	select distinct ROLE_ID, API into #RoleResource from security.v_CMSMapping_ROLE_RESOURCE  
	--select * from admin_all.ROLE
	merge admin_all.ROLE_RESOURCE t
	using #RoleResource s on s.ROLE_ID = t.ROLE_ID and s.API = t.URL
	when not matched by source then
		delete
	when not matched then
		insert (ROLE_ID, URL)
			values(s.ROLE_ID, s.API)
	;

	--sp_help 'admin_all.ROLE_PERMISSION'
	select * into #RolePermission from security.v_CMSMapping_ROLE_PERMISSION
	;merge admin_all.ROLE_PERMISSION t
	using #RolePermission s on s.ROLE_ID = t.ROLE_ID and t.TYPE = s.TYPE and isnull(t.SUBTYPE, '') = isnull(s.SUB_TYPE, '') and t.SCOPE = 'PAGE'
	when not matched by source then
		delete
	when not matched then
		insert (ROLE_ID, TYPE, SUBTYPE, SCOPE)
			values(s.ROLE_ID, s.TYPE, s.SUB_TYPE, 'PAGE')
	;
	
	select * into #StaffRole from security.v_CMSMapping_STAFF_ROLE

	delete  #StaffRole where ROLE_ID not in (select ROLE_ID from #Role)
	;merge admin_all.STAFF_ROLE t
	using #StaffRole s on s.STAFF_ID = t.STAFF_ID and s.ROLE_ID = t.ROLE_ID
	when not matched by source then
		delete
	when not matched then 
		insert (STAFF_ID, ROLE_ID)
			values(s.STAFF_ID, s.ROLE_ID)
	;
	--select * from admin_all.STAFF_ROLE
	commit
end
go
--exec security.usp_PushPermissionToCMS


