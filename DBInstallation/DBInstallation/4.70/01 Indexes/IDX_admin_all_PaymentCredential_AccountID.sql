if not exists(select * from sys.indexes where object_id = object_id('ADMIN_ALL.PaymentCredential') and name = 'IDX_ADMIN_ALL_PaymentCredential_ACCOUNTID')
create index IDX_ADMIN_ALL_PaymentCredential_ACCOUNTID on ADMIN_ALL.PaymentCredential(ACCOUNTID)
