set quoted_identifier, ansi_nulls on 
go
if not exists(select * from sys.indexes where name = 'IDX_admin_all_GAME_MESSAGE_PartyID' and object_id = object_id('admin_all.GAME_MESSAGE'))
	create index IDX_admin_all_GAME_MESSAGE_PartyID on admin_all.GAME_MESSAGE(PartyID) include(notified)