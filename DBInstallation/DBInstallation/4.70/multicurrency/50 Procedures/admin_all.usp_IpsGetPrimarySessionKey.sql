set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_IpsGetPrimarySessionKey(
    @sessionKey varchar(50)
)

as
begin
    select case
               when ua.IsPrimary = 1 then @sessionKey
               else
                   (select uws_full.SESSION_KEY
                    from external_mpt.UserAssociatedAccount ua_full
                             with (NOLOCK)
                             join admin_all.USER_WEB_SESSION uws_full on ua_full.AssociatedPartyID = uws_full.PARTYID
                    where uws_full.PartyID = ua.PartyID
                      and ua_full.IsPrimary = 1)
               end
    from admin_all.USER_WEB_SESSION uws
             join external_mpt.UserAssociatedAccount ua on uws.PARTYID = ua.AssociatedPartyID
    where uws.SESSION_KEY = @sessionKey
end

go