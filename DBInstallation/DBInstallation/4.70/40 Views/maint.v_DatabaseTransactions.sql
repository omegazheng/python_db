set ansi_nulls, quoted_identifier on
go
create or alter view maint.v_DatabaseTransactions
as
select t.database_transaction_begin_time TransactionStartTime, l.request_session_id SessionID, t.transaction_id TransactionID, l.c ResourceLocked, b.event_info Inputbuffer, s.host_name ClientName, c.client_net_address ClientAddress, s.program_name ApplicationName, s.login_name LoginName, c.net_transport NetTransport
from sys.dm_tran_database_transactions  t
	cross apply(select request_session_id, count(*) c from sys.dm_tran_locks l where t.transaction_id = l.request_owner_id group by request_session_id) l
	cross apply sys.dm_exec_input_buffer ( l.request_session_id, null ) b
	left join sys.dm_exec_sessions s on s.session_id = l.request_session_id
	left outer join sys.dm_exec_connections c on c.session_id = l.request_session_id
where t.database_id = db_id()
	and t.database_transaction_type = 1