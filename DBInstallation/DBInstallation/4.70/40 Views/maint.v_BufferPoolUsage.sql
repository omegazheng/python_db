set ansi_nulls, quoted_identifier on
go
create or alter view maint.v_BufferPoolUsage
as
select  
		object_schema_name(i.object_id) SchemaName,
		object_name(i.object_id) ObjectName,
		i.name IndexName,
		p.partition_number PartitionNumber,
		m.page_level PageLevel,
		m.page_type PageType,
		au.type_desc AllocationUnitType,
		m.is_in_bpool_extension IsInBufferPoolExtention,
		m.numa_node NumaNode,
		count_big(*) TotalPages,
		cast(count_big(*) as numeric(38,2)) * cast(8 as numeric(38,2))/1024 TotalSizeInMB,
		cast(sum(cast(m.free_space_in_bytes as bigint)) as numeric(38, 2)) / cast(1024 as numeric(38,2))/ cast(1024 as numeric(38,2)) as FreeSpaceInMB
from sys.dm_os_buffer_descriptors m
	left join sys.allocation_units au on au.allocation_unit_id = m.allocation_unit_id
	left join sys.partitions p on p.partition_id = au.container_id and au.type = 2 or p.hobt_id = au.container_id and au.type = 1
	left join sys.indexes i on i.object_id = p.object_id and i.index_id = p.index_id
where m.database_id = db_id()
group by object_schema_name(i.object_id), object_name(i.object_id), i.name, p.partition_number, m.page_level, m.page_type, au.type_desc, m.is_in_bpool_extension, m.numa_node
go

--select * from maint.v_BufferPoolUsage


--select * from sys.dm_os_buffer_descriptors