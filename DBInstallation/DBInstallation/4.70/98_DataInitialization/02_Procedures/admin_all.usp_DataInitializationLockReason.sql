set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_DataInitializationLockReason
as
begin
	set nocount on
	set identity_insert [admin_all].[PLAYER_LOCK_REASON] on;
	begin transaction

	;with plr as
	(
		select [id],[code],[reason]
		from (
				values
					 (1,N'CLOSED_DUPLICATE',N'Closed Duplicate')
					,(2,N'PEP_SANCTIONS',N'PEP/Sanctions')
					,(3,N'AML_CFT',N'AML/CFT')
				    ,(4,N'CUSTOMER_REQUEST',N'Customer Request')
					,(5,N'RESTRICTED_COUNTRY',N'Restricted Country')
					,(6,N'UNDERAGE',N'Underage')
					,(7,N'KYC',N'KYC')
				    ,(8,N'SELF_EXCLUSION',N'Self-exclusion')
					,(9,N'ANONYMOUS',N'Anonymous or Fictitious Account')
					,(10,N'FRAUD',N'Fraud - Other')
			) v([id],[code],[reason])
	)
	merge admin_all.PLAYER_LOCK_REASON pr
	using plr on plr.[id]= pr.[id]
	when not matched then
		insert ([id],[code],[reason])
		values(plr.[id], plr.[code], plr.[reason])
	when matched and (plr.[id] is null and pr.[id] is not null or plr.[id] is not null and pr.[code] is null or plr.[code] <> pr.[code]) then
		update set pr.[code]= plr.[code], pr.[reason]= plr.[reason]
    ;

	commit
	set identity_insert [admin_all].[PLAYER_LOCK_REASON] off;
end
go
