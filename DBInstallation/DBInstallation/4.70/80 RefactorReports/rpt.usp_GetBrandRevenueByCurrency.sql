set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetBrandRevenueByCurrency
(
	@Brands varchar(max)
)
as
begin
	set nocount on
	create table #Brands(BrandID int primary key)
	insert into #Brands
		select distinct v
		from (select try_cast(rtrim(ltrim(value)) as int) v from string_split(@Brands, ',')) t
		where v is not null

	select SUM(BALANCE_REAL + RELEASED_BONUS) AS CASH_HOLD, CURRENCY
	from admin_all.ACCOUNT a
		inner join external_mpt.USER_CONF u on a.PARTYID = u.PARTYID
		inner join #Brands b on b.BrandID = u.BRANDID
	GROUP BY CURRENCY
end
