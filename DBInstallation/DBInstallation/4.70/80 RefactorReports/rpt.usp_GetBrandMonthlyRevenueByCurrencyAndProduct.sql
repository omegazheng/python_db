set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetBrandMonthlyRevenueByCurrencyAndProduct
(
	@DateFrom datetime,
	@DateTo datetime,
	@Brands varchar(max)
)
as
begin
	set nocount on
	create table #Brands(BrandID int primary key)
	insert into #Brands
		select distinct v
		from (select try_cast(rtrim(ltrim(value)) as int) v from string_split(@Brands, ',')) t
		where v is not null

	SELECT
		DW_ALL_MONTHS.month as DISPLAY_MONTH,
		CURRENCY,
		PLATFORM,
		PNL,
		HANDLE
	FROM admin_all.DW_ALL_MONTHS
		LEFT JOIN (
					SELECT
						  MONTH,
						  PLATFORM,
						  CURRENCY,
						  SUM(PNL_REAL+ PNL_RELEASED_BONUS+ PNL_PLAYABLE_BONUS) PNL,
						  SUM(HANDLE_REAL+ HANDLE_RELEASED_BONUS+ HANDLE_PLAYABLE_BONUS) HANDLE
					FROM admin_all.DW_GAME_PLAYER_DAILY dw
						LEFT JOIN admin_all.PLATFORM p ON dw.PLATFORM = p.NAME
					WHERE p.WALLET_TYPE = 'GAME_PLAY' 
						and MONTH >=  @DateFrom
						and MONTH <=   @DateTo
						and BRAND_ID in (select BrandID from #Brands)
					GROUP BY MONTH, CURRENCY, PLATFORM
				) AS REVENUE ON DW_ALL_MONTHS.MONTH = REVENUE.MONTH
	WHERE DW_ALL_MONTHS.MONTH >=  @DateFrom
		and DW_ALL_MONTHS.MONTH <=  @DateTo
	order by dw_all_months.month, PLATFORM
end
