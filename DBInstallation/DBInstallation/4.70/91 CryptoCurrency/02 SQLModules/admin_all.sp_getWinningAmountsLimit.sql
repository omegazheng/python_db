set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[sp_getWinningAmountsLimit]
  (
    @startDate DATETIME,
    @endDate   DATETIME,
    @brands    VARCHAR(255),
    @numLimit  INT
  )
AS
  BEGIN
    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    DECLARE @numLimitLocal INT
    DECLARE @brandsLocal VARCHAR(255)
    SET @startDateLocal = @startDate
    SET @endDateLocal = @endDate
    SET @numLimitLocal = @numLimit
    SET @brandsLocal = @brands
    DECLARE @tempTable AS TABLE
    (
      USERID        VARCHAR(255),
      NET_WIN       NUMERIC(38, 18),
      NET_WIN_BONUS NUMERIC(38, 18),
      CURRENCY      NVARCHAR(10)
    );

    INSERT INTO @tempTable (USERID, NET_WIN, NET_WIN_BONUS, CURRENCY)
      SELECT
        USERID,
        SUM((GAMES.PNL_REAL + GAMES.PNL_RELEASED_BONUS) * -1) AS NET_WIN,
        SUM(GAMES.PNL_PLAYABLE_BONUS * -1)                    AS NET_WIN_BONUS,
        USERS.CURRENCY
      FROM
        admin_all.DW_GAME_PLAYER_DAILY AS GAMES
        JOIN
        external_mpt.USER_CONF AS USERS
          ON
            GAMES.PARTY_ID = USERS.PARTYID
      WHERE
        SUMMARY_DATE >= @startDateLocal
        AND SUMMARY_DATE <= @endDateLocal
        AND (@brandsLocal IS NULL OR Games.BRAND_ID IN (@brandsLocal))
      GROUP BY USERID, USERS.CURRENCY;

    SELECT *
    FROM
      (
        SELECT TOP (@numLimitLocal) *
        FROM @tempTable
        ORDER BY NET_WIN DESC
      ) a
    UNION
    SELECT *
    FROM
      (
        SELECT TOP (@numLimitLocal) *
        FROM @tempTable
        ORDER BY NET_WIN ASC
      ) b
  END;

go
