set ansi_nulls, quoted_identifier on
go

create or alter procedure admin_all.usp_ReportBaseRevenueReport
  (
    @StartDate Datetime = null,
    @EndDate Datetime = null,

    @GroupBy nvarchar(max) = null,
    @FundTypes nvarchar(100) = 'all', --'All, Real, ReleasedBonus, PlayableBonus'
    @DateMode nvarchar(100) = 'REVENUE', --'REVENUE or REGISTRATION'

    @Currencies nvarchar(max) = null,
    @PlatformIDs nvarchar(max)= null,
    @Countries nvarchar(max) = null,
    @VIPStatus nvarchar(max) = null,
    @BrandIDs nvarchar(max) = null,
    @OrderBy nvarchar(max) = null,
    @InGameReleaseStr nvarchar(100) = null
    )
  as
  begin
    set nocount on

    declare @HasNULLVIPSettings bit = 0

    create table #FundType(FundType varchar(20) primary key with(ignore_dup_key=on))
    if @FundTypes like '%all%'
      begin
        insert into #FundType(FundType) values('REAL')
        insert into #FundType(FundType) values('RELEASED_BONUS')
        insert into #FundType(FundType) values('PLAYABLE_BONUS')
      end
    if @FundTypes like '%real%'
      insert into #FundType(FundType) values('REAL')
    if @FundTypes like '%Release%'
      insert into #FundType(FundType) values('RELEASED_BONUS')
    if @FundTypes like '%Playable%'
      insert into #FundType(FundType) values('PLAYABLE_BONUS')
    create table #Currency(Currency nvarchar(10) primary key with(ignore_dup_key=on))
    insert into #Currency(Currency)
    select c.iso_code
    from admin_all.fc_splitDelimiterString(@Currencies, ',') a
           inner join admin_all.CURRENCY c on c.iso_code = rtrim(ltrim(a.Item))
    if @@rowcount = (select count(*) from admin_all.CURRENCY)
      delete #Currency
    create table #Platform(PlatformID int primary key with(ignore_dup_key=on))
    insert into #Platform(PlatformID)
    select b.ID
    from admin_all.fc_splitDelimiterString(@PlatformIDs, ',') a
           inner join admin_all.PLATFORM b on isnumeric(a.Item) = 1 and cast(a.Item as int)= b.ID
    if @@rowcount = (select count(*) from admin_all.PLATFORM)
      delete #Platform
    create table #Country(Country char(2) primary key with(ignore_dup_key=on))
    insert into #Country(Country)
    select c.iso2_code
    from admin_all.fc_splitDelimiterString(@Countries, ',') a
           inner join admin_all.Country c on c.iso2_code = rtrim(ltrim(a.Item))
    if @@rowcount = (select count(*) from admin_all.COUNTRY)
      delete #Country

    -- 		VIPStatus
    if(select count(*) from admin_all.fc_splitDelimiterString(@VIPStatus, ',') a where isnumeric(a.Item) = 1 and cast(a.Item as int) = -1) > 0
      begin
        set @HasNULLVIPSettings = 1
      end

    create table #VIPStatus(VIPStatusID int primary key with(ignore_dup_key=on))
    insert into #VIPStatus(VIPStatusID)
    select b.ID
    from admin_all.fc_splitDelimiterString(@VIPStatus, ',') a
           inner join admin_all.VIP_STATUS b on isnumeric(a.Item) = 1 and cast(a.Item as int)= b.ID
    if (@@rowcount = (select count(*) from admin_all.VIP_STATUS))
      delete #VIPStatus
    --

    create table #Brand(BrandID int primary key with(ignore_dup_key=on))
    insert into #Brand(BrandID)
    select b.BRANDID
    from admin_all.fc_splitDelimiterString(@BrandIDs, ',') a
           inner join admin.CASINO_BRAND_DEF b on isnumeric(a.Item) = 1 and cast(a.Item as int)= b.BRANDID
    if @@rowcount = (select count(*) from admin.CASINO_BRAND_DEF)
      delete #Brand


    select @GroupBy = isnull(@GroupBy,' ')
    select @StartDate = isnull(@StartDate, DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0))
    select @EndDate = isnull(@EndDate, GETDATE())

    set @Currencies = isnull((select Currency + ',' from #Currency FOR xml path('')), 'N/A')
    print 'Currencies: ' + @Currencies
    set @PlatformIDs = isnull((select CAST(PlatformID AS nvarchar(255)) + ',' from #Platform FOR xml path('')), 'N/A')
    print 'PlatformIDs: ' + isnull(@PlatformIDs, 'N/A')
    set @Countries = isnull((select CAST(Country AS nvarchar(255)) + ',' from #Country FOR xml path('')), 'N/A')
    print 'Countries: ' + isnull(@Countries, 'N/A')

    set @VIPStatus = isnull((select CAST(VIPStatusID AS nvarchar(255)) + ',' from #VIPStatus FOR xml path('')), 'N/A')
    print 'VIPStatus: ' + isnull(@VIPStatus, 'N/A')

    set @BrandIDs = isnull((select CAST(BrandID AS nvarchar(255)) + ',' from #Brand FOR xml path('')), 'N/A')
    print 'BrandIDs: ' + isnull(@BrandIDs, 'N/A')

    print @StartDate
    print @EndDate


    declare @SQL nvarchar(max)
    select @SQL = 'select
	' + @GroupBy + ' daily.CURRENCY, daily.PARTY_ID,
	max(daily.IS_NEW) IS_NEW,
	sum('+stuff((select '+HANDLE_'+FundType  from #FundType for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '')+') as HANDLE,
	sum('+stuff((select '+PNL_'+FundType  from #FundType for xml path(''), type).value('.', 'nvarchar(max)'), 1, 1, '')+ @InGameReleaseStr +') as HOLD,
	sum(GAME_COUNT) as GAME_COUNT,
	sum(BET_COUNT) as BET_COUNT
	into #1
  from admin_all.dw_all_days
    left outer join (
      select p_daily.*, gi.SUB_PLATFORM_ID, sp.CODE as SUB_PLATFORM_CODE
      from DW_GAME_PLAYER_DAILY as p_daily
       left  join admin_all.GAME_INFO gi on gi.GAME_ID = p_daily.GAME_ID and p_daily.PLATFORM_ID = gi.PLATFORM_ID
       left  join admin_all.SUB_PLATFORM sp on sp.id = gi.SUB_PLATFORM_ID
    ) daily
    on  daily.summary_date = dw_all_days.DAY

-- from admin_all.DW_GAME_PLAYER_DAILY as daily
	inner join external_mpt.USER_CONF as users on daily.party_id = users.partyid
where '+case when isnull(@DateMode, '') = 'REVENUE' then 'daily.summary_date >= @StartDate and daily.summary_date <= @EndDate' else 'users.REG_DATE >= @StartDate and users.REG_DATE <= @EndDate' end + '
	' + case when exists(select * from #Currency) then 'and daily.CURRENCY in  (select c.Currency from #Currency c)' else '' end +'
	' + case when exists(select * from #Platform) then 'and daily.PLATFORM_ID in  (select c.PlatformID from #Platform c)' else '' end +'
	' + case when exists(select * from #Country) then 'and daily.COUNTRY in  (select c.Country from #Country c)' else '' end +'
	and(1=1'

      + case when exists(select * from #VIPStatus)
               then ' and (users.VIP_STATUS in  (select c.VIPStatusID from #VIPStatus c) ' +
                    case
                      when @HasNULLVIPSettings = 1 then ' or users.VIP_STATUS is null'
                      else ' '
                      end + ')'
             else +
               case
                 when @HasNULLVIPSettings = 1 then ' '
                 else ' and users.VIP_STATUS is not null'
                 end
                    end
      + ')' + case when exists(select * from #Brand) then ' and daily.BRAND_ID in  (select c.BrandID from #Brand c)' else '' end +'
group by ' + @GroupBy + ' daily.CURRENCY, daily.PARTY_ID
--select * from #1 where currency = ''CNY'' order by party_id
select
	' + @GroupBy + ' daily.CURRENCY,
	sum(HANDLE) as HANDLE,
	sum(HOLD) as HOLD,
	sum(GAME_COUNT) as GAME_COUNT,
	sum(BET_COUNT) as BET_COUNT,
	count(PARTY_ID) as PLAYER_COUNT,
	count(case when IS_NEW = 1 then PARTY_ID end) as NEW_PLAYER_COUNT
from #1 daily
group by ' + @GroupBy + ' daily.CURRENCY'
      + isnull(' order by '+ nullif(rtrim(@OrderBy), ''), '')
    print @SQL
    exec sp_executesql @SQL, N'@StartDate Datetime,	@EndDate Datetime', @StartDate, @EndDate
  end

go
