set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetAgencySummary
(
	@AgentID int,
	@DateFrom datetime,
	@DateTo datetime,
	@Currency nvarchar(10) = 'USD'
)
as
begin
	DECLARE @endDateLocal DATETIME
	SET @endDateLocal = DATEADD(DD, 1, @DateTo);
	set nocount on
	;with x0 as 
	(
		select u.PARTYID TopMostPartyID, u.PARTYID, u.ParentID, u.UserID TopMostUserID, u.Currency, u.CURRENCY TopMostCurrency
		from external_mpt.USER_CONF u
		where user_type <> 0 and (@AgentID is null and ParentID is null or  @AgentID is not null and  ParentID = @AgentID)
		union all
		select x0.TopMostPartyID, u1.PARTYID, u1.ParentID, x0.TopMostUserID, u1.Currency, x0.TopMostCurrency
		from external_mpt.USER_CONF u1
			inner join x0 on x0.PARTYID = u1.ParentID
	),	x1 as
	(
		select x0.TopMostPartyID, x0.TopMostUserID, x0.TopMostCurrency, x0.CURRENCY, sum(a.AmountReal) Amount, cast(a.Datetime as date) Day
		from x0 
			inner join admin_all.AccountTranHourlyAggregate a on a.PartyID = x0.PartyID and a.AggregateType = cast(0 as tinyint) and a.Datetime >= @DateFrom and a.Datetime < @endDateLocal and a.TranType in ('GAME_BET', 'GAME_WIN')
		group by x0.TopMostPartyID, x0.TopMostUserID, x0.TopMostCurrency, x0.CURRENCY, cast(a.Datetime as date)
	), x2 as
	(
		select x1.TopMostPartyID PartyID, x1.TopMostCurrency Currency, x1.TopMostUserID Agent, isnull(sum(Amount * r.Rate),0) as Amount, Day
		from x1
			outer apply admin_all.fn_GetCurrencyConversionRate1(null, x1.CURRENCY, @Currency) r
		group by x1.TopMostPartyID, x1.TopMostCurrency, x1.TopMostUserID, Day
	)
	select *, sum(Amount) over(partition by Day) DayTotal, sum(Amount) over() GrandTotal
	from x2
	order by Day desc, Amount desc-- Agent 
end

go
