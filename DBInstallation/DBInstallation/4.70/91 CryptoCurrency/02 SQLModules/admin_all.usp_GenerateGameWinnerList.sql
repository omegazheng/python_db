set quoted_identifier, ansi_nulls on
go
if object_id('admin_all.usp_GenerateGameWinnerList') is null
	exec('create procedure admin_all.usp_GenerateGameWinnerList as --')
go
alter procedure admin_all.usp_GenerateGameWinnerList
as
begin
	set nocount, xact_abort on
	
	declare @FundType nvarchar(max), @TopN int, @RetentionPeriod int, @LastLoadedAccountTranID bigint, @MinAccountTranID bigint,
			@MaxAccountTranID bigint, @Now datetime = getdate(), @BatchSize int = 30000, @DefaultCurrency nvarchar(10)
	select	@FundType = admin_all.fn_GetRegistry('GameWinnerList.FundType'),
			@TopN = admin_all.fn_GetRegistry('GameWinnerList.TopN'),
			@RetentionPeriod = admin_all.fn_GetRegistry('GameWinnerList.RetentionPeriod'),
			@LastLoadedAccountTranID = admin_all.fn_GetRegistry('GameWinnerList.LastLoadedAccountTranID')
	if @FundType is null
	begin
		exec admin_all.usp_SetRegistry 'GameWinnerList.FundType', 'REAL'  ---can be REAL, RELEASED_BONUS, PLAYABLE_BONUS
		select	@FundType = admin_all.fn_GetRegistry('GameWinnerList.FundType')
	end 
	if @TopN is null
	begin
		exec admin_all.usp_SetRegistry 'GameWinnerList.TopN', 100
		select @TopN = admin_all.fn_GetRegistry('GameWinnerList.TopN')
	end
	if @RetentionPeriod is null
	begin
		exec admin_all.usp_SetRegistry 'GameWinnerList.RetentionPeriod', 31
		select @RetentionPeriod = admin_all.fn_GetRegistry('GameWinnerList.RetentionPeriod')
	end
	if @LastLoadedAccountTranID is null
	begin
		exec admin_all.usp_SetRegistry 'GameWinnerList.LastLoadedAccountTranID', 0
		select @LastLoadedAccountTranID = admin_all.fn_GetRegistry('GameWinnerList.LastLoadedAccountTranID')
	end
	
	select @MinAccountTranID = (select top 1 ID  from admin_all.ACCOUNT_TRAN_ALL a   where DATETIME >= dateadd(day, -@RetentionPeriod, getdate())  and ID > 0 order by DATETIME asc)
	if @MinAccountTranID is null
		select @MinAccountTranID = (select top 1 ID  from admin_all.ACCOUNT_TRAN_REALTIME a   where DATETIME >= dateadd(day, -@RetentionPeriod, getdate())  and ID > 0 order by DATETIME asc)
	
	if @MinAccountTranID > @LastLoadedAccountTranID
	begin
-- 		The processed data is already out of the time range that we are interested, so forget them all
		truncate table admin_all.GameWinnerList
	end
	else
	begin
-- 		Part of the data is still within the time range, so continue from the last position
		select @MinAccountTranID = @LastLoadedAccountTranID
	end

	select @MaxAccountTranID = (select  max(ID)  from admin_all.ACCOUNT_TRAN_REALTIME a)
	if @MaxAccountTranID is null
		select @MaxAccountTranID = isnull((select  max(ID)  from admin_all.ACCOUNT_TRAN_ALL a), 0)

	--create table #GameTran(AccountID int, GameInfoID int not null, GameTranID nvarchar(100) not null primary key(GameTranID, GameInfoID, AccountID))
	create table #GameList 
	(
		Datetime datetime not null,
		GameInfoID int not null,
		GameTranIDHash Uniqueidentifier not null,
		AccountTranID bigint not null,
		BrandID int not null,
		PartyID int not null,
		BetAmount numeric(38, 18) not null,
		WonAmount numeric(38,18) not null,
		Currency nvarchar(10) collate database_default not null,
		--PayoutRate as (WonAmount*100/nullif(BetAmount,0)),
		primary key clustered (GameInfoID, GameTranIDHash, PartyID) --with(ignore_dup_key = on)
		
	)
	
	create table #GameListDate(Datetime datetime primary key)

	declare @d datetime
	declare @i int = 0, @AccountID int, @GameTranID nvarchar(100), @GameID varchar(100), @ProductID int
	declare c cursor local fast_forward for
		select distinct PLATFORM_ID, GAME_TRAN_ID, ACCOUNT_ID, GAME_ID
		from admin_all.ACCOUNT_TRAN a
		where a.ID > @MinAccountTranID and a.id <= @MaxAccountTranID
			and PLATFORM_ID is not null
			and GAME_TRAN_ID is not null
			and ACCOUNT_ID is not null
			and TRAN_TYPE = 'GAME_WIN'
	open c
	fetch next from c into @ProductID, @GameTranID, @AccountID, @GameID
	while @@fetch_status = 0
	begin
		;with x0 as 
		(
			select 
				dateadd(hour, datediff(hour, '1990-01-01', max(a.DATETIME)), '1990-01-01') as DateTime, 
				max(A.ID) AccountTranID, 
				-sum(
					case when a.TRAN_TYPE = 'GAME_BET' then
						case 
							when @FundType like '%PLAYABLE%' then a.AMOUNT_REAL + a.AMOUNT_RELEASED_BONUS + a.AMOUNT_PLAYABLE_BONUS
							when @FundType like '%RELEASE%' then a.AMOUNT_REAL + a.AMOUNT_RELEASED_BONUS
							else a.AMOUNT_REAL
						end 
						else 0
					end
					) BetAmount,
				sum(
					case when a.TRAN_TYPE = 'GAME_WIN' then
						case 
							when @FundType like '%PLAYABLE%' then a.AMOUNT_REAL + a.AMOUNT_RELEASED_BONUS + a.AMOUNT_PLAYABLE_BONUS
							when @FundType like '%RELEASE%' then a.AMOUNT_REAL + a.AMOUNT_RELEASED_BONUS
							else a.AMOUNT_REAL
						end 
						else 0
					end
					) WonAmount
			from admin_all.account_tran a 
			where a.TRAN_TYPE in ('GAME_BET', 'GAME_WIN') 
				and a.ROLLED_BACK = 0 
				and A.ACCOUNT_ID = @AccountID
				and A.PLATFORM_ID = @ProductID
				and A.GAME_TRAN_ID = @GameTranID
				and a.GAME_ID = @GameID
				and a.ID <= @MaxAccountTranID
		)
		insert into #GameList(Datetime, GameInfoID, GameTranIDHash, BetAmount, WonAmount, AccountTranID, PartyID, BrandID, Currency)
			select x0.DateTime, gi.ID GameInfoID, admin_all.fn_HashString(@GameTranID),x0.BetAmount, x0.WonAmount, x0.AccountTranID, u.PARTYID, u.BRANDID, u.CURRENCY
			from x0
				inner join admin_all.ACCOUNT a on a.id = @AccountID
				inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
				inner join admin_all.GAME_INFO gi on gi.GAME_ID = @GameID and gi.PLATFORM_ID = @ProductID
			where x0.DateTime is not null --and u.WINNERS_LIST = 1
		fetch next from c into @ProductID, @GameTranID, @AccountID, @GameID
	end
	close c
	deallocate c
	
	select @DefaultCurrency = iso_code  from admin_all.CURRENCY where is_default = 1

	insert into #GameListDate(Datetime)
		select distinct Datetime
		from #GameList

-- 	In GameWinnerList, Remove all records that also exist in #GameList
	delete gw
	from admin_all.GameWinnerList gw
	where exists(select * from #GameList gl where gl.GameInfoID = gw.GameInfoID and gl.GameTranIDHash = gw.GameTranIDHash and gw.PartyID = gl.PartyID)

-- 	Getting old data into GameList ?? Don't understand this.
	insert into #GameList(Datetime, GameInfoID, GameTranIDHash, AccountTranID, BrandID, PartyID, BetAmount, WonAmount, Currency)
		select Datetime, GameInfoID, GameTranIDHash, AccountTranID, BrandID, PartyID, BetAmount, WonAmount, Currency
		from admin_all.GameWinnerList gw
		where exists(select * from #GameListDate gld where gld.Datetime = gw.Datetime)
			
	;with x0 as
	(
		select	Datetime, GameInfoID, GameTranIDHash, AccountTranID, BrandID, PartyID, BetAmount, WonAmount, Currency,
				row_number() over (partition by Datetime, BrandID, Currency order by WonAmount desc) RN
		from #GameList
	),
	x1 as
	(
		select Datetime, GameInfoID, GameTranIDHash, AccountTranID, BrandID, PartyID, BetAmount, WonAmount, Currency, Rank, ConvertedWonAmount
		from admin_all.GameWinnerList gw
		where exists(select * from #GameListDate gld where gld.Datetime = gw.Datetime)
	)
	merge x1 as t
	using (select * from x0 where x0.RN <=@TopN) s on t.Datetime = s.Datetime and t.GameInfoID = s.GameInfoID and t.GameTranIDHash = s.GameTranIDHash and t.PartyID = s.PartyID
	when matched and (t.AccountTranID <> s.AccountTranID and t.BrandID = s.BrandID and t.BetAmount<>s.BetAmount and t.WonAmount<> s.WonAmount and t.Currency <> s.Currency and t.Rank <> s.RN) then
		update set t.AccountTranID = s.AccountTranID, t.BrandID = s.BrandID, t.BetAmount=s.BetAmount, t.WonAmount= s.WonAmount, t.Currency = s.Currency, t.Rank = s.RN--, t.ConvertedWonAmount = admin_all.fn_GetCurrencyConversionRate(s.Datetime, s.Currency, @DefaultCurrency) * s.WonAmount
	when not matched then
		insert(Datetime, GameInfoID, GameTranIDHash, AccountTranID, BrandID, PartyID, BetAmount, WonAmount, Currency, Rank)--, ConvertedWonAmount) 
			values(s.Datetime, s.GameInfoID, s.GameTranIDHash, s.AccountTranID, s.BrandID, s.PartyID, s.BetAmount, s.WonAmount, s.Currency, s.RN)--, admin_all.fn_GetCurrencyConversionRate(s.Datetime, s.Currency, @DefaultCurrency) * s.WonAmount)
	when not matched by source then
		delete
	;
	delete admin_all.GameWinnerList where Datetime < dateadd(day, -@RetentionPeriod, getdate())

	
	update gw
		set ConvertedWonAmount = case when gw.Currency = @DefaultCurrency then gw.WonAmount else gw.WonAmount * cc.RATE end
	from admin_all.GameWinnerList gw
		inner join #GameListDate gld on gld.Datetime = gw.Datetime
		outer apply (
						select top 1 RATE
						from admin_all.CURRENCY_CONV 
							inner join admin_all.CURRENCY_CONV_RATE on CURRENCY_CONV.ID = CURRENCY_CONV_RATE.CURRENCY_CONV_ID
						where DATE < dateadd(day, 1, cast(gw.Datetime as date)) 
							and TO_CURRENCY = @DefaultCurrency
							and BASE_CURRENCY = gw.Currency
						order by DATE desc
					) cc
	exec admin_all.usp_SetRegistry 'GameWinnerList.LastLoadedAccountTranID', @MaxAccountTranID
end
go
