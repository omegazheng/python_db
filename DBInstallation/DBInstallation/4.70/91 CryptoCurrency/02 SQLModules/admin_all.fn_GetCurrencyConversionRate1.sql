set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_GetCurrencyConversionRate1(@Date datetime, @CurrencyFrom nvarchar(10), @CurrencyTo nvarchar(10))
returns table 
as 
return 
(
	--select top 1 RateID, Rate, EffectiveDate
	--from (
			select  RateID, Rate, EffectiveDate
			from admin_all.CurrencyConversionRateCurrent
			where CurrencyFrom = @CurrencyFrom 
				and CurrencyTo = @CurrencyTo
				and @Date is null
			union all
			select RateID, Rate, EffectiveDate
			from admin_all.CurrencyConversionRate
			where CurrencyFrom = @CurrencyFrom 
				and CurrencyTo = @CurrencyTo
				and EffectiveDate <= @Date
				and ExpiryDate > @Date
	--	) r
) 

go
