set ansi_nulls, quoted_identifier on
go

-- if freePlay_plan_id != null, get the active FreePlayPlanCurrencyStakeList by brandId
-- id id != null, just get that FreePlayPlanCurrencyStake

create or alter procedure admin_all.usp_GetFreePlayPlanCurrencyStake
  (
    @FREEPLAY_PLAN_ID INT,
    @ID INT,
    @CURRENCY NVARCHAR(10)
  )
as
  begin

    IF @ID IS NOT NULL
      BEGIN
        select  id as id,
                freeplay_plan_id as freePlayPlanId,
                currency as currency,
                isnull(bet_per_round,0) as betPerRound
        from
          admin_all.FREEPLAY_PLAN_CURRENCY_STAKE
        where
          ID = @ID
      END
    ELSE
      IF (@FREEPLAY_PLAN_ID IS NOT NULL) and (@CURRENCY is not null)
        BEGIN
          select  id as id,
                  freeplay_plan_id as freePlayPlanId,
                  currency as currency,
                  isnull(bet_per_round,0) as betPerRound
          from
            admin_all.FREEPLAY_PLAN_CURRENCY_STAKE
          where
            freeplay_plan_id = @FREEPLAY_PLAN_ID
            and CURRENCY = @CURRENCY
        END

      ELSE IF @FREEPLAY_PLAN_ID IS NOT NULL
        BEGIN
          select  id as id,
                  freeplay_plan_id as freePlayPlanId,
                  currency as currency,
                  isnull(bet_per_round,0) as betPerRound
          from
            admin_all.FREEPLAY_PLAN_CURRENCY_STAKE
          where
            freeplay_plan_id = @FREEPLAY_PLAN_ID
        END

  end

go
