set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_InsertAccountTranCurrencyExtension
(
	@AccountTranID bigint,
	@PartyID int = null, 
	@Datetime datetime = null,
	@CurrencyFrom nvarchar(10) = null,
	@AmountReal numeric(38,18) = null,
	@ReleasedBonus numeric(38, 18) = null,
	@PlayableBonus numeric(38, 18) = null
)
as
begin
	set nocount on
	if @CurrencyFrom is null
	begin
		if @PartyID is null
		begin
			select @CurrencyFrom = u.CURRENCY, @AmountReal = isnull(at.AMOUNT_REAL,0), @ReleasedBonus = isnull(at.AMOUNT_RELEASED_BONUS, 0), @PlayableBonus = isnull(at.AMOUNT_PLAYABLE_BONUS, 0), @Datetime = at.DATETIME
			from admin_all.ACCOUNT_TRAN at
				inner join admin_all.ACCOUNT a on a.id = at.ACCOUNT_ID
				inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
			where at.ID = @AccountTranID
		end
	end
	if @AmountReal is null or @ReleasedBonus is null or @PlayableBonus is null or @Datetime is null
	begin
		select @AmountReal = isnull(at.AMOUNT_REAL,0), @ReleasedBonus = isnull(at.AMOUNT_RELEASED_BONUS, 0), @PlayableBonus = isnull(at.AMOUNT_PLAYABLE_BONUS, 0), @Datetime = at.DATETIME
		from admin_all.ACCOUNT_TRAN at
		where at.ID = @AccountTranID
	end
	declare @Currency table (CurrencyTo nvarchar(30) not null primary key)
	insert into @Currency(CurrencyTo)
		select CURRENCY
		from (
				select aa.CURRENCY
				from external_mpt.v_UserPrimaryCurrency aa with(noexpand)
				where aa.AssociatedPartyID = @PartyID
				union
				select admin_all.fn_GetPrimaryCurrency() 
				union
				select admin_all.fn_GetSecondaryCurrency() 
			) c
		where CURRENCY is not null
	insert into admin_all.AccountTranCurrencyExtension(AccountTranID, RateID, Currency, AmountReal, ReleasedBonus, PlayableBonus)
		select @AccountTranID, r.RateID, c.CurrencyTo, @AmountReal * r.Rate, @ReleasedBonus * r.Rate, @PlayableBonus * r.Rate
		from @Currency c
			outer apply admin_all.fn_GetCurrencyConversionRate1(null, @CurrencyFrom, c.CurrencyTo) r
end
go
