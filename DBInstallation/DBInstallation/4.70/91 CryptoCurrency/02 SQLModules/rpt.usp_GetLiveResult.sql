set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_GetLiveResult
(
	@AgentID int,
	@DateFrom datetime,
	@DateTo datetime
)
as
begin
	DECLARE @endDateLocal DATETIME
	SET @endDateLocal = DATEADD(DD, 1, @DateTo);
	set nocount on
	declare @Currency nvarchar(10)
	select @Currency = CURRENCY from external_mpt.USER_CONF where PARTYID = @AgentID
	;with x0 as 
	(
		select u.PARTYID, u.ParentID, u.UserID, u.Currency
		from external_mpt.USER_CONF u
		where ParentID = @AgentID
		union all
		select u.PARTYID, u.ParentID, u.UserID, u.Currency
		from external_mpt.USER_CONF u
			inner join x0 on x0.PARTYID = u.ParentID
	),	x1 as
	(
		select x0.CURRENCY, 
			 at.AmountReal, at.AmountReleasedBonus, at.AmountPlayableBonus,
			 p.Withdrawal, p.Deposit
		from x0 
			outer apply(
							select  
									sum(case when a.TranType in ('GAME_BET', 'GAME_WIN') then a.AmountReal else 0 end) AmountReal, 
									sum(case when a.TranType in ('GAME_BET', 'GAME_WIN') then a.AmountReleasedBonus else 0 end) AmountReleasedBonus, 
									sum(case when a.TranType in ('CRE_BONUS', 'MAN_BONUS') then a.AmountPlayableBonus else 0 end) AmountPlayableBonus
							from admin_all.AccountTranHourlyAggregate a 
							where a.PartyID = x0.PartyID  
								and a.AggregateType = cast(0 as tinyint) 
								and a.Datetime >= @DateFrom 
								and a.Datetime < @endDateLocal
								and a.TranType in ('GAME_BET', 'GAME_WIN', 'CRE_BONUS', 'MAN_BONUS')
						) at
			outer apply(
							select 
									sum(case when p.Type = 'WITHDRAWAL' then p.Amount else 0 end) Withdrawal, 
									sum(case when p.Type = 'DEPOSIT' then p.Amount else 0 end) Deposit
							from admin_all.ACCOUNT a
								inner join admin_all.PAYMENT p on p.ACCOUNT_ID = a.id
							where a.PARTYID = x0.PARTYID
								and p.TYPE in ('WITHDRAWAL', 'DEPOSIT')
								and p.STATUS = 'COMPLETED'
								and p.PROCESS_DATE >= @DateFrom
								and p.PROCESS_DATE < @endDateLocal

					) p
	)
	select 
		sum(isnull(r.Rate * x1.AmountReal, 0)) as AmountReal,
		sum(isnull(r.Rate * x1.AmountReleasedBonus, 0)) as AmountReleasedBonus,
		sum(isnull(r.Rate * x1.AmountPlayableBonus, 0)) as AmountPlayableBonus,
		sum(isnull(r.Rate * x1.Withdrawal, 0)) Withdrawal,
		sum(isnull(r.Rate * x1.Deposit, 0)) Deposit
	from x1
		cross apply admin_all.fn_GetCurrencyConversionRate1(null, x1.Currency, @Currency) r

end

go
