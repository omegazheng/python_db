if not exists(select * from sys.indexes i where object_id = object_id('[external_mpt].[USER_CONF]') and name = 'IDX_external_mpt_USER_CONF_BRANDID_CURRENCY')
	create nonclustered index [IDX_external_mpt_USER_CONF_BRANDID_CURRENCY] on [external_mpt].[USER_CONF]([BRANDID],[CURRENCY]) include([PARTYID],[USERID]);
go