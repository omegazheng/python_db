if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BRAND_CCLEVEL_LIMIT]') and name = 'IDX_admin_all_BRAND_CCLEVEL_LIMIT_CCLEVEL_ID_ISO_CODE')
	create unique nonclustered index [IDX_admin_all_BRAND_CCLEVEL_LIMIT_CCLEVEL_ID_ISO_CODE] on [admin_all].[BRAND_CCLEVEL_LIMIT]([CCLEVEL_ID],[ISO_CODE]) ;
go