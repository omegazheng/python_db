
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[CONVERSION_RATE_TEMP]') and name = 'FROM_CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[CONVERSION_RATE_TEMP] alter column [FROM_CURRENCY] nvarchar(10)  not null;