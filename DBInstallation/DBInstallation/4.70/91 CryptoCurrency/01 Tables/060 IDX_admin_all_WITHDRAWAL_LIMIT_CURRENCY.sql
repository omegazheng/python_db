if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[WITHDRAWAL_LIMIT]') and name = 'IDX_admin_all_WITHDRAWAL_LIMIT_CURRENCY')
	create nonclustered index [IDX_admin_all_WITHDRAWAL_LIMIT_CURRENCY] on [admin_all].[WITHDRAWAL_LIMIT]([CURRENCY]) ;
go