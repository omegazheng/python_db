if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[TOURNAMENT_CURRENCY]') and name = 'IDX_admin_all_TOURNAMENT_CURRENCY_CURRENCY')
	create nonclustered index [IDX_admin_all_TOURNAMENT_CURRENCY_CURRENCY] on [admin_all].[TOURNAMENT_CURRENCY]([CURRENCY]) ;
go