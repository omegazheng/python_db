if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BRAND_JACKPOTS]') and name = 'IDX_admin_all_BRAND_JACKPOTS_CURRENCY')
	create nonclustered index [IDX_admin_all_BRAND_JACKPOTS_CURRENCY] on [admin_all].[BRAND_JACKPOTS]([CURRENCY]) ;
go