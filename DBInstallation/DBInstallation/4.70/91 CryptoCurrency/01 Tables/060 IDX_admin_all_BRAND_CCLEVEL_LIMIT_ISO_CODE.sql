if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BRAND_CCLEVEL_LIMIT]') and name = 'IDX_admin_all_BRAND_CCLEVEL_LIMIT_ISO_CODE')
	create nonclustered index [IDX_admin_all_BRAND_CCLEVEL_LIMIT_ISO_CODE] on [admin_all].[BRAND_CCLEVEL_LIMIT]([ISO_CODE]) ;
go