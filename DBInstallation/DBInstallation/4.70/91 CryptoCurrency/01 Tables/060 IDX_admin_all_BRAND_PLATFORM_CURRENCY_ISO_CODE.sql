if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BRAND_PLATFORM_CURRENCY]') and name = 'IDX_admin_all_BRAND_PLATFORM_CURRENCY_ISO_CODE')
	create nonclustered index [IDX_admin_all_BRAND_PLATFORM_CURRENCY_ISO_CODE] on [admin_all].[BRAND_PLATFORM_CURRENCY]([ISO_CODE]) ;
go