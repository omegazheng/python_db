if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[CONVERSION_RATE_TEMP]') and name = 'IDX_admin_all_CONVERSION_RATE_TEMP_TO_CURRENCY')
	create nonclustered index [IDX_admin_all_CONVERSION_RATE_TEMP_TO_CURRENCY] on [admin_all].[CONVERSION_RATE_TEMP]([TO_CURRENCY]) ;
go