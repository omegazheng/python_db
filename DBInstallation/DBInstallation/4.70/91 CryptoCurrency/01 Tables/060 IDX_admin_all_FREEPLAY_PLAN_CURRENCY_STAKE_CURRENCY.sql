if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]') and name = 'IDX_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY')
	create nonclustered index [IDX_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY] on [admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE]([CURRENCY]) ;
go