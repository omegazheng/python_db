
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[INPAY_BANK_INFO]') and name = 'CURRENCY_CODE' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[INPAY_BANK_INFO] alter column [CURRENCY_CODE] nvarchar(10)  not null;