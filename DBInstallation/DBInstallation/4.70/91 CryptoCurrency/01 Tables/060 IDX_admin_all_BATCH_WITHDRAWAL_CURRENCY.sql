if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[BATCH_WITHDRAWAL]') and name = 'IDX_admin_all_BATCH_WITHDRAWAL_CURRENCY')
	create nonclustered index [IDX_admin_all_BATCH_WITHDRAWAL_CURRENCY] on [admin_all].[BATCH_WITHDRAWAL]([CURRENCY]) ;
go