if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[PAYMENT_METHOD_PER_BRAND]') and name = 'IDX_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY')
	create nonclustered index [IDX_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY] on [admin_all].[PAYMENT_METHOD_PER_BRAND]([CURRENCY]) ;
go