if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[PLATFORM_CURRENCY]') and name = 'IDX_admin_all_PLATFORM_CURRENCY_PLATFORM_CURRENCY')
	create nonclustered index [IDX_admin_all_PLATFORM_CURRENCY_PLATFORM_CURRENCY] on [admin_all].[PLATFORM_CURRENCY]([PLATFORM_CURRENCY]) ;
go