
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[WITHDRAWAL_UKASH_VOUCHER]') and name = 'ISSUED_CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[WITHDRAWAL_UKASH_VOUCHER] alter column [ISSUED_CURRENCY] nvarchar(10)  not null;