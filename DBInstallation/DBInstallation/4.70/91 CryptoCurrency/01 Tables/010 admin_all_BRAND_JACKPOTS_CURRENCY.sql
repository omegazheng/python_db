
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BRAND_JACKPOTS]') and name = 'CURRENCY' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[BRAND_JACKPOTS] alter column [CURRENCY] nvarchar(10)  not null;