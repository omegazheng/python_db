if object_id('[admin_all].[FK_admin_all_BATCH_WITHDRAWAL_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[BATCH_WITHDRAWAL] add constraint [FK_admin_all_BATCH_WITHDRAWAL_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CURRENCY_CODE]') is null
		alter table [admin_all].[BONUS_PLAN_CURRENCY] add constraint [FK_admin_all_BONUS_PLAN_CURRENCY_CURRENCY_CURRENCY_CODE] foreign key ([CURRENCY_CODE]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_BRAND_CCLEVEL_LIMIT_CURRENCY_ISO_CODE]') is null
		alter table [admin_all].[BRAND_CCLEVEL_LIMIT] add constraint [FK_admin_all_BRAND_CCLEVEL_LIMIT_CURRENCY_ISO_CODE] foreign key ([ISO_CODE]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_BRAND_CURRENCY_CURRENCY_ISO_CODE]') is null
		alter table [admin_all].[BRAND_CURRENCY] add constraint [FK_admin_all_BRAND_CURRENCY_CURRENCY_ISO_CODE] foreign key ([ISO_CODE]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_BRAND_JACKPOTS_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[BRAND_JACKPOTS] add constraint [FK_admin_all_BRAND_JACKPOTS_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_BRAND_PLATFORM_CURRENCY_CURRENCY_ISO_CODE]') is null
		alter table [admin_all].[BRAND_PLATFORM_CURRENCY] add constraint [FK_admin_all_BRAND_PLATFORM_CURRENCY_CURRENCY_ISO_CODE] foreign key ([ISO_CODE]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_FROM_CURRENCY]') is null
		alter table [admin_all].[CONVERSION_RATE_TEMP] add constraint [FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_FROM_CURRENCY] foreign key ([FROM_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_TO_CURRENCY]') is null
		alter table [admin_all].[CONVERSION_RATE_TEMP] add constraint [FK_admin_all_CONVERSION_RATE_TEMP_CURRENCY_TO_CURRENCY] foreign key ([TO_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_CURRENCY_CONV_CURRENCY_BASE_CURRENCY]') is null
		alter table [admin_all].[CURRENCY_CONV] add constraint [FK_admin_all_CURRENCY_CONV_CURRENCY_BASE_CURRENCY] foreign key ([BASE_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_CURRENCY_CONV_RATE_CURRENCY_TO_CURRENCY]') is null
		alter table [admin_all].[CURRENCY_CONV_RATE] add constraint [FK_admin_all_CURRENCY_CONV_RATE_CURRENCY_TO_CURRENCY] foreign key ([TO_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_DEPOSIT_LIMIT_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[DEPOSIT_LIMIT] add constraint [FK_admin_all_DEPOSIT_LIMIT_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[FREEPLAY_PLAN_CURRENCY_STAKE] add constraint [FK_admin_all_FREEPLAY_PLAN_CURRENCY_STAKE_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_PAYMENT_CURRENCY_CONV_CURRENCY]') is null
		alter table [admin_all].[PAYMENT] add constraint [FK_admin_all_PAYMENT_CURRENCY_CONV_CURRENCY] foreign key ([CONV_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CONVERSION_CURRENCY]') is null
		alter table [admin_all].[PAYMENT_METHOD_PER_BRAND] add constraint [FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CONVERSION_CURRENCY] foreign key ([CONVERSION_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[PAYMENT_METHOD_PER_BRAND] add constraint [FK_admin_all_PAYMENT_METHOD_PER_BRAND_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLATFORM_CURRENCY]') is null
		alter table [admin_all].[PLATFORM_CURRENCY] add constraint [FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLATFORM_CURRENCY] foreign key ([PLATFORM_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLAYER_CURRENCY]') is null
		alter table [admin_all].[PLATFORM_CURRENCY] add constraint [FK_admin_all_PLATFORM_CURRENCY_CURRENCY_PLAYER_CURRENCY] foreign key ([PLAYER_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_TOURNAMENT_CURRENCY_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[TOURNAMENT_CURRENCY] add constraint [FK_admin_all_TOURNAMENT_CURRENCY_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_TOURNAMENT_POINTS_ACC_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[TOURNAMENT_POINTS_ACC] add constraint [FK_admin_all_TOURNAMENT_POINTS_ACC_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_TRANSACTION_PLATFORM_CONVERSION_CURRENCY_PLATFORM_CURRENCY]') is null
		alter table [admin_all].[TRANSACTION_PLATFORM_CONVERSION] add constraint [FK_admin_all_TRANSACTION_PLATFORM_CONVERSION_CURRENCY_PLATFORM_CURRENCY] foreign key ([PLATFORM_CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_WITHDRAWAL_LIMIT_CURRENCY_CURRENCY]') is null
		alter table [admin_all].[WITHDRAWAL_LIMIT] add constraint [FK_admin_all_WITHDRAWAL_LIMIT_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[external_mpt].[FK_external_mpt_USER_CONF_CURRENCY_CURRENCY_CURRENCY]') is null
		alter table [external_mpt].[USER_CONF] add constraint [FK_external_mpt_USER_CONF_CURRENCY_CURRENCY_CURRENCY] foreign key ([CURRENCY]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyFrom]') is null
		alter table [admin_all].[CurrencyConversionRate] add constraint [FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyFrom] foreign key ([CurrencyFrom]) references [admin_all].[CURRENCY]([iso_code]);
if object_id('[admin_all].[FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyTo]') is null
		alter table [admin_all].[CurrencyConversionRate] add constraint [FK_admin_all_CurrencyConversionRate_CURRENCY_CurrencyTo] foreign key ([CurrencyTo]) references [admin_all].[CURRENCY]([iso_code]);