if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[DW_GAME_PLAYER_DAILY]') and name = 'IDX_admin_all_DW_GAME_PLAYER_DAILY_CURRENCY_BRAND_ID_SUMMARY_DATE')
	create nonclustered index [IDX_admin_all_DW_GAME_PLAYER_DAILY_CURRENCY_BRAND_ID_SUMMARY_DATE] on [admin_all].[DW_GAME_PLAYER_DAILY]([CURRENCY],[BRAND_ID],[SUMMARY_DATE]) include([PARTY_ID],[PNL]);
go