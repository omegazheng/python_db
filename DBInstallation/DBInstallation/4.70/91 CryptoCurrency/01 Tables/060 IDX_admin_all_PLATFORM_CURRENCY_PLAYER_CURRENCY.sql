if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[PLATFORM_CURRENCY]') and name = 'IDX_admin_all_PLATFORM_CURRENCY_PLAYER_CURRENCY')
	create nonclustered index [IDX_admin_all_PLATFORM_CURRENCY_PLAYER_CURRENCY] on [admin_all].[PLATFORM_CURRENCY]([PLAYER_CURRENCY]) ;
go