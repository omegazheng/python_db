if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[CURRENCY_CONV]') and name = 'IDX_admin_all_CURRENCY_CONV_BASE_CURRENCY_DATE')
	create nonclustered index [IDX_admin_all_CURRENCY_CONV_BASE_CURRENCY_DATE] on [admin_all].[CURRENCY_CONV]([BASE_CURRENCY],[DATE]) include([ID]);
go