
if not exists(select * from sys.columns where object_id = object_id('[admin_all].[BRAND_PLATFORM_CURRENCY]') and name = 'ISO_CODE' and type_name(user_type_id) = 'nvarchar' and max_length = 20)
	alter table [admin_all].[BRAND_PLATFORM_CURRENCY] alter column [ISO_CODE] nvarchar(10)  not null;