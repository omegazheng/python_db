if not exists(select * from sys.indexes i where object_id = object_id('[admin_all].[RAFFLE_CURRENCY]') and name = 'PK_admin_all_RAFFLE_CURRENCY')
	alter table [admin_all].[RAFFLE_CURRENCY] add constraint[PK_admin_all_RAFFLE_CURRENCY] primary key clustered([CURRENCY]);
go