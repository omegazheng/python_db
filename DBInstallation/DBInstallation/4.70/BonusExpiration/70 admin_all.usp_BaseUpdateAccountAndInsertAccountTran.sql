set ansi_nulls, quoted_identifier on
go
create or alter procedure [admin_all].[usp_BaseUpdateAccountAndInsertAccountTran]
(
	@PartyID			int,
	@AmountReal			numeric(38,18) = 0,
	@AmountReleasedBonus		numeric(38,18) = 0,
	@AmountPlayableBonus		numeric(38,18) = 0,
  @AmountRawLoyalty	bigint = 0,
	@TranType			varchar(10),
	@PlatformID			int = null,
	@PlatformTranID		nvarchar(100) = null,
	@GameTranID			nvarchar(100) = null,
	@GameID				varchar(100) = null,
	@PaymentID			int = null,
	@RollbackTranID bigint = null,
	@Reference			varchar(100) = null,
	@MachineID			int = null,
	@AccountTranID		bigint = null output
)
as
begin
	set nocount on
	set xact_abort on
  declare @AccountReal numeric(38,18), @AccountReleasedBonus numeric(38,18), @AccountPlayableBonus numeric(38,18),
      @AccountRawLoyalty bigint, @AccountID int, @BrandID int

	select @BrandID = admin_all.fn_GetBrandIdFromPartyId(@PartyID)
	select @AccountID = id from admin_all.ACCOUNT where PARTYID = @PartyID

	begin transaction
 	exec admin_all.usp_LockAccountForBalanceUpdate @AccountID = @AccountID

	update admin_all.ACCOUNT with (ROWLOCK)
		set @AccountReal = BALANCE_REAL        = BALANCE_REAL + @AmountReal,
			@AccountReleasedBonus = RELEASED_BONUS = RELEASED_BONUS + @AmountReleasedBonus,
			@AccountPlayableBonus   = PLAYABLE_BONUS = PLAYABLE_BONUS + @AmountPlayableBonus,
			@AccountRawLoyalty    = RAW_LOYALTY_POINTS = RAW_LOYALTY_POINTS + @AmountRawLoyalty
	where PARTYID = @PartyID

  if @AccountReal < 0
      begin
        RAISERROR('Insufficient real fund', 16, 1);
        rollback
      end
  if @AccountReleasedBonus < 0
      begin
        RAISERROR('Insufficient released bonus', 16, 1);
        rollback
      end
  if @AccountPlayableBonus < 0
      begin
        RAISERROR('Insufficient playable bonus', 16, 1);
        rollback
      end

	insert into admin_all.ACCOUNT_TRAN
	(ACCOUNT_ID, DATETIME, TRAN_TYPE, AMOUNT_REAL, BALANCE_REAL,
	 PLATFORM_TRAN_ID, GAME_TRAN_ID, GAME_ID, PLATFORM_ID,
	 payment_id, ROLLED_BACK, ROLLBACK_TRAN_ID,
	 AMOUNT_RELEASED_BONUS, AMOUNT_PLAYABLE_BONUS, BALANCE_RELEASED_BONUS, BALANCE_PLAYABLE_BONUS,
	 AMOUNT_RAW_LOYALTY, BALANCE_RAW_LOYALTY, REFERENCE, BRAND_ID, MachineID )
  values
	 (@AccountID, getdate(), @TranType, @AmountReal, @AccountReal,
	  @PlatformTranID, @GameTranID, @GameID, @PlatformID,
	  @PaymentID, null, @RollbackTranID,
	  @AmountReleasedBonus, @AmountPlayableBonus, @AccountReleasedBonus, @AccountPlayableBonus,
	  @AmountRawLoyalty, @AccountRawLoyalty, @Reference, @BrandID, @MachineID
	 )

  select @AccountTranID = cast(session_context(N'AccountTranID') as bigint)
  commit
end

