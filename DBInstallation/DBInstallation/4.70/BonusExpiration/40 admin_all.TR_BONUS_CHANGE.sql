SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create or alter trigger admin_all.TR_BONUS_CHANGE on admin_all.BONUS
for insert, update
as
begin
	if @@rowcount = 0
		return
	set nocount on 

	insert into admin_all.BONUS_HISTORY (BONUS_ID, PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS)		
		select ID, PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS 
		from inserted 
	declare @BonusID int
	declare cTR_BONUS_CHANGE cursor local static for 
		select b.ID
		from inserted b
			inner join admin_all.BONUS_PLAN bp on b.BONUS_PLAN_ID = bp.ID
		where bp.MinBalanceAction in ('EXPIRE', 'RELEASE')
				and bp.MinBalanceThreshold <> 0
				and b.PLAYABLE_BONUS_WINNINGS + b.PLAYABLE_BONUS <= bp.MinBalanceThreshold
				and (b.PLAYABLE_BONUS_WINNINGS<> 0 or  b.PLAYABLE_BONUS <> 0)
				and b.STATUS in ('ACTIVE', 'QUEUED')
	open cTR_BONUS_CHANGE
	fetch next from cTR_BONUS_CHANGE into @BonusID
	while @@fetch_status = 0
	begin
		exec admin_all.usp_AdjustBonusWithMinThreshold @BonusID = @BonusID, @InvokedFromBonus = 1
		fetch next from cTR_BONUS_CHANGE into @BonusID
	end
	close cTR_BONUS_CHANGE
	deallocate cTR_BONUS_CHANGE

end