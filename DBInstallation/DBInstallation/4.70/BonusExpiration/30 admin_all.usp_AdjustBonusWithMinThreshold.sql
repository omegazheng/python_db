set ansi_nulls, quoted_identifier on 
go
create or alter procedure admin_all.usp_AdjustBonusWithMinThreshold
(
	@BonusID int = null,
	@BonusPlanID int = null,
	@OriginalMinBalanceThreshold numeric(38, 18) = null,
	@OriginalMinBalanceAction varchar(20) = null,
	@InvokedFromBonus bit = null
)
as
begin
	set nocount, xact_abort on
	declare @MinBalanceThreshold numeric(38, 18), @MinBalanceAction varchar(20),
			@PlayableBonusWinnings numeric(38, 18), @PlayableBonus numeric(38, 18), @PartyID int, @Balance numeric(38, 18), @Balance1 numeric(38, 18), @AccountTranID bigint, @BrandID int,
			@ReleasedBonus numeric(38, 18), @ReleasedBonusWinnings numeric(38, 18), @Date datetime = getdate(), @IsPlayable bit
	begin transaction

	
	if @BonusID is not null
	begin
		declare c cursor local static for 
			select b.ID, b.PLAYABLE_BONUS_WINNINGS, b.PLAYABLE_BONUS, b.PARTYID, u.BRANDID, b.RELEASED_BONUS_AMOUNT, b.RELEASED_BONUS_WINNINGS_AMOUNT, bp.MinBalanceThreshold, bp.MinBalanceAction, bp.ID, bp.IS_PLAYABLE
			from admin_all.BONUS_PLAN bp
				inner join admin_all.BONUS b on b.BONUS_PLAN_ID = bp.ID
				inner join external_mpt.USER_CONF u on u.PARTYID = b.PARTYID
			where bp.MinBalanceAction in ('EXPIRE', 'RELEASE')
				and bp.MinBalanceThreshold <> 0
				and b.ID = @BonusID
				and b.PLAYABLE_BONUS_WINNINGS + b.PLAYABLE_BONUS <= bp.MinBalanceThreshold
				and (b.PLAYABLE_BONUS_WINNINGS<> 0 or  b.PLAYABLE_BONUS <> 0)
				and b.STATUS in ('ACTIVE', 'QUEUED')
	end
	else if @BonusPlanID is not null
	begin
		declare c cursor local static for 
			select b.ID, b.PLAYABLE_BONUS_WINNINGS, b.PLAYABLE_BONUS, b.PARTYID, u.BRANDID, b.RELEASED_BONUS_AMOUNT, b.RELEASED_BONUS_WINNINGS_AMOUNT, bp.MinBalanceThreshold, bp.MinBalanceAction, bp.ID, bp.IS_PLAYABLE
			from admin_all.BONUS_PLAN bp
				inner join admin_all.BONUS b on b.BONUS_PLAN_ID = bp.ID
				inner join external_mpt.USER_CONF u on u.PARTYID = b.PARTYID
			where bp.MinBalanceAction in ('EXPIRE', 'RELEASE')
				and bp.MinBalanceThreshold <> 0
				and bp.ID = @BonusPlanID
				and b.PLAYABLE_BONUS_WINNINGS + b.PLAYABLE_BONUS <= bp.MinBalanceThreshold
				and (b.PLAYABLE_BONUS_WINNINGS<> 0 or  b.PLAYABLE_BONUS <> 0)
				and b.STATUS in ('ACTIVE', 'QUEUED')
	end
	else
	begin
		declare c cursor local static for 
			select b.ID, b.PLAYABLE_BONUS_WINNINGS, b.PLAYABLE_BONUS, b.PARTYID, u.BRANDID, b.RELEASED_BONUS_AMOUNT, b.RELEASED_BONUS_WINNINGS_AMOUNT, bp.MinBalanceThreshold, bp.MinBalanceAction, bp.ID, bp.IS_PLAYABLE
				from admin_all.BONUS_PLAN bp
					inner join admin_all.BONUS b on b.BONUS_PLAN_ID = bp.ID
					inner join external_mpt.USER_CONF u on u.PARTYID = b.PARTYID
				where bp.MinBalanceAction in ('EXPIRE', 'RELEASE')
					and bp.MinBalanceThreshold <> 0
					and b.PLAYABLE_BONUS_WINNINGS + b.PLAYABLE_BONUS <= bp.MinBalanceThreshold
					and (b.PLAYABLE_BONUS_WINNINGS<> 0 or  b.PLAYABLE_BONUS <> 0)
					and b.STATUS in ('ACTIVE', 'QUEUED')
	end
	open c 
	fetch next from c into @BonusID, @PlayableBonusWinnings, @PlayableBonus, @PartyID, @BrandID, @ReleasedBonus, @ReleasedBonusWinnings, @MinBalanceThreshold, @MinBalanceAction, @BonusPlanID, @IsPlayable
	while @@fetch_status = 0
	begin
		if @MinBalanceAction = 'EXPIRE'
		begin
			select @Balance = - (@PlayableBonusWinnings + @PlayableBonus)
			exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = 0, @ReleasedBonus = 0, @PlayableBonus = @Balance, @AmountSecondary = 0,
														@TranType = 'EXP_BONUS', @PlatformID = null, @PlatformTranID = null, @GameTranID = null, @GameID = null,
														@PaymentID = null, @AmountRawLoyalty = 0, @Reference = 'Expired Bonus when balance is below threshold.', @Cit = 0,
														@DateTime = null, @UpdateBonuses = 0, @MachineID = null, @AccountTranID = @AccountTranID output
		
			insert into BonusPlan.BonusAccountTran (BonusID, PartyID, BrandID, DateTime, TranType, AmountPlayableBonus, AmountPlayableBonusWinnings, AmountReleasedBonus, AmountReleasedBonusWinnings, AmountWageredContribution, BalancePlayableBonus, BalancePlayableBonusWinnings, BalanceReleasedBonus, BalanceReleasedBonusWinnings, AccountTranId)
				Values (@BonusID, @PartyID, @BrandID, @Date, 'EXP_BONUS', -@PlayableBonus, -@PlayableBonusWinnings, 0, 0, 0, 0, 0, @ReleasedBonus, @ReleasedBonusWinnings, @AccountTranID)

			update admin_all.BONUS
				set PLAYABLE_BONUS_WINNINGS = PLAYABLE_BONUS_WINNINGS - @PlayableBonusWinnings,
					PLAYABLE_BONUS = PLAYABLE_BONUS - @PlayableBonus,
					STATUS = 'EXPIRED',
					EXPIRY_DATE = @Date
			where ID = @BonusID

			if (@PlayableBonusWinnings <> 0 OR @PlayableBonus <> 0)
			begin
				insert into admin_all.USER_ACTION_LOG(DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
					values(@Date, null, 'Expired Bonus when balance is below threshold.', @PartyID, 'EXPIRE_BONUS', @BonusID, 'PLAYABLE_BONUS', @PlayableBonusWinnings + @PlayableBonus, 0, null)
			end

		end
		if @MinBalanceAction = 'RELEASE'
		begin
			
			select	@Balance = - (@PlayableBonusWinnings + @PlayableBonus),
					@Balance1 = (@PlayableBonusWinnings + @PlayableBonus) 

			exec admin_all.usp_UpdateAccountInternal	@PartyID = @PartyID, @AmountReal = 0, @ReleasedBonus = @Balance1, @PlayableBonus = @Balance, @AmountSecondary = 0,
														@TranType = 'BONUS_REL', @PlatformID = null, @PlatformTranID = null, @GameTranID = null, @GameID = null,
														@PaymentID = null, @AmountRawLoyalty = 0, @Reference = 'Release Bonus when balance is below threshold.', @Cit = 0,
														@DateTime = null, @UpdateBonuses = 0, @MachineID = null, @AccountTranID = @AccountTranID output
		
			insert into BonusPlan.BonusAccountTran (BonusID, PartyID, BrandID, DateTime, TranType, AmountPlayableBonus, AmountPlayableBonusWinnings, AmountReleasedBonus, AmountReleasedBonusWinnings, AmountWageredContribution, BalancePlayableBonus, BalancePlayableBonusWinnings, BalanceReleasedBonus, BalanceReleasedBonusWinnings, AccountTranId)
				Values (@BonusID, @PartyID, @BrandID, @Date, 'BONUS_REL', -@PlayableBonus, -@PlayableBonusWinnings, @PlayableBonus, @PlayableBonusWinnings, 0, 0, 0, @ReleasedBonus, @ReleasedBonusWinnings, @AccountTranID)

			update admin_all.BONUS
				set RELEASED_BONUS = RELEASED_BONUS + @PlayableBonus,
					RELEASED_BONUS_AMOUNT = RELEASED_BONUS_AMOUNT + @PlayableBonus,
					RELEASED_BONUS_WINNINGS = RELEASED_BONUS_WINNINGS + @PlayableBonusWinnings,
					RELEASED_BONUS_WINNINGS_AMOUNT = RELEASED_BONUS_WINNINGS_AMOUNT + @PlayableBonusWinnings,
					STATUS = 'RELEASED',
					RELEASE_DATE = @Date
			where ID = @BonusID

			if (@PlayableBonusWinnings <> 0 or @PlayableBonus <> 0)
			begin
				insert into admin_all.USER_ACTION_LOG(DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
					values(@Date, null, 'Released Bonus when balance is below threshold.', @PartyID, 'BONUS_REL', @BonusID, 'RELEASED_BONUS', 0, @PlayableBonus + @PlayableBonusWinnings, null)
			end
			if @IsPlayable = 1
				exec admin_all.usp_UpdateGameBonusBucketWhenReleaseBonus @BonusID
		end
		if isnull(@OriginalMinBalanceThreshold, @MinBalanceThreshold) <> @MinBalanceThreshold
		begin
			insert into admin_all.USER_ACTION_LOG(DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
				values(@Date, null, 'Bonus plan is changed.', @PartyID, 'BONUS_PLAN_CHANGE', @BonusPlanID, 'MinBalanceThreshold', @OriginalMinBalanceThreshold, @MinBalanceThreshold, null)
		end
		if isnull(@OriginalMinBalanceAction, @MinBalanceAction) <> @MinBalanceAction
		begin
			insert into admin_all.USER_ACTION_LOG(DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, OLD_VALUE, NEW_VALUE, OPERATION_TYPE)
				values(@Date, null, 'Bonus plan is changed.', @PartyID, 'BONUS_PLAN_CHANGE', @BonusPlanID, 'MinBalanceAction', @OriginalMinBalanceAction, @MinBalanceAction, null)
		end
		if @InvokedFromBonus = 1
		begin
			insert into admin_all.BONUS_HISTORY (BONUS_ID, PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS)		
				select ID, PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, AMOUNT_WITHDRAWN, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS 
				from admin_all.BONUS
				where id = @BonusID
		end

		fetch next from c into @BonusID, @PlayableBonusWinnings, @PlayableBonus, @PartyID, @BrandID, @ReleasedBonus, @ReleasedBonusWinnings, @MinBalanceThreshold, @MinBalanceAction, @BonusPlanID, @IsPlayable
	end
	close c
	deallocate c
	commit
end
go

--set xact_abort on
--begin transaction
--update admin_all.ACCOUNT set PLAYABLE_BONUS = 1000  where PARTYID = 100109104
----select * from admin_all.BONUS_PLAN where id = 172

--update admin_all.BONUS set STATUS = 'ACTIVE' where STATUS not in ('CANCELED') and PLAYABLE_BONUS > 0 and PLAYABLE_BONUS_WINNINGS > 0 and id = 1127
--select * from admin_all.BONUS where STATUS not in ('CANCELED') and PLAYABLE_BONUS > 0 and PLAYABLE_BONUS_WINNINGS > 0 and id = 1127

--update admin_all.BONUS_PLAN set MinBalanceThreshold = 1000, MinBalanceAction = 'RELEASE' where id = 172


--	select b.ID, b.PLAYABLE_BONUS_WINNINGS, b.PLAYABLE_BONUS, b.PARTYID, u.BRANDID, b.RELEASED_BONUS_AMOUNT, b.RELEASED_BONUS_WINNINGS_AMOUNT, bp.MinBalanceThreshold, bp.MinBalanceAction, bp.ID
--			from admin_all.BONUS_PLAN bp
--				inner join admin_all.BONUS b on b.BONUS_PLAN_ID = bp.ID
--				inner join external_mpt.USER_CONF u on u.PARTYID = b.PARTYID
--			where bp.MinBalanceAction in ('EXPIRE', 'RELEASE')
--				and bp.MinBalanceThreshold <> 0
--				and bp.ID = 172
--				and b.PLAYABLE_BONUS_WINNINGS + b.PLAYABLE_BONUS <= bp.MinBalanceThreshold
--				and (b.PLAYABLE_BONUS_WINNINGS<> 0 or  b.PLAYABLE_BONUS <> 0)
--				and b.STATUS in ('ACTIVE', 'QUEUED')



--select * from admin_all.BONUS where id = 1127
--select * from admin_all.ACCOUNT where PARTYID = 100109104
--select * from admin_all.ACCOUNT_TRAN where ACCOUNT_ID = 172201 and DATETIME > dateadd(day, -1, getdate())
--select * from BonusPlan.BonusAccountTran where BonusID = 1127 
--select top 50 * from admin_all.USER_ACTION_LOG where PARTYID = 100109104 order by date desc
--rollback



--set xact_abort on
--begin transaction
--update admin_all.ACCOUNT set PLAYABLE_BONUS = 1000  where PARTYID = 100109104
--select * from admin_all.BONUS_PLAN where id = 172

--update admin_all.BONUS set STATUS = 'ACTIVE' where STATUS not in ('CANCELED') and PLAYABLE_BONUS > 0 and PLAYABLE_BONUS_WINNINGS > 0 and id = 1127
--select * from admin_all.BONUS where STATUS not in ('CANCELED') and PLAYABLE_BONUS > 0 and PLAYABLE_BONUS_WINNINGS > 0 and id = 1127

--update admin_all.BONUS_PLAN set MinBalanceThreshold = 100, MinBalanceAction = 'RELEASE' where id = 172


	
--update admin_all.BONUS set PLAYABLE_BONUS_WINNINGS = 0 where id = 1127

--select * from admin_all.BONUS where id = 1127
--select * from admin_all.ACCOUNT where PARTYID = 100109104
--select * from admin_all.ACCOUNT_TRAN where ACCOUNT_ID = 172201 and DATETIME > dateadd(day, -1, getdate())
--select * from BonusPlan.BonusAccountTran where BonusID = 1127 
--select top 50 * from admin_all.USER_ACTION_LOG where PARTYID = 100109104 order by date desc
--rollback
