set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CalculateReleaseBonusAmount
(
	@BonusID int,
	@ReleasedBonusAmount numeric(38,18) output,
	@ReleasedBonusWinningsAmount numeric(38, 18) output,
	@MaxReleaseLimitAmount numeric(38, 18) = null output
)
as

begin
  declare @BonusPlanID int, @PaymentID int, @BonusAmount numeric(38, 18), @PlayableBonus numeric(38, 18), @PlayableBonusWinnings numeric(38, 18)
  declare @MaxReleaseEnabled bit, @MaxReleaseMultiplierType nvarchar(30), @MaxReleaseMultiplierValue numeric(38, 18)

  select @BonusPlanID = BONUS_PLAN_ID, @PaymentID = PAYMENT_ID, @BonusAmount = AMOUNT, @PlayableBonus = PLAYABLE_BONUS, @PlayableBonusWinnings = PLAYABLE_BONUS_WINNINGS
  from admin_all.BONUS where id = @BonusID
  select @MaxReleaseEnabled = MAX_RELEASE_ENABLED, @MaxReleaseMultiplierType = MAX_RELEASE_MULTIPLIER_TYPE, @MaxReleaseMultiplierValue = MAX_RELEASE_MULTIPLIER_VALUE
  from admin_all.BONUS_PLAN where id = @BonusPlanID

  if (@MaxReleaseEnabled is null or @MaxReleaseEnabled = 0)
    begin
      select @ReleasedBonusAmount = @PlayableBonus, @ReleasedBonusWinningsAmount = @PlayableBonusWinnings, @MaxReleaseLimitAmount = null
    end
  else
    begin
      if @MaxReleaseMultiplierType = 'BONUS'
        begin
          select @MaxReleaseLimitAmount = @MaxReleaseMultiplierValue * @BonusAmount
        end
      else if @MaxReleaseMultiplierType = 'DEPOSIT'
        begin
          declare @PaymentAmount numeric(38, 18)
          select @PaymentAmount = AMOUNT from admin_all.PAYMENT where ID = @PaymentID
          select @MaxReleaseLimitAmount = @MaxReleaseMultiplierValue * @PaymentAmount
        end
      else
        begin
          RAISERROR('MaxReleaseMultiplierType is invalid', 16, 1);
        end

      -- preconditions
      if @MaxReleaseLimitAmount < 0
        RAISERROR('MaxReleaseLimitAmount cannot be negative', 16, 1);
      if @PlayableBonusWinnings < 0
        RAISERROR('PlayableBonusWinnings cannot be negative', 16, 1);
      if @PlayableBonus < 0
        RAISERROR('PlayableBonus cannot be negative', 16, 1);

      declare @RemainingMaxReleaseLimitAmount numeric(38, 18) = @MaxReleaseLimitAmount
      select @ReleasedBonusWinningsAmount =
      case
        when @RemainingMaxReleaseLimitAmount <= 0 then 0
        when @RemainingMaxReleaseLimitAmount <= @PlayableBonusWinnings then @RemainingMaxReleaseLimitAmount
        else @PlayableBonusWinnings
      end

      select @RemainingMaxReleaseLimitAmount = @RemainingMaxReleaseLimitAmount - @ReleasedBonusWinningsAmount
      select @ReleasedBonusAmount =
      case
        when @RemainingMaxReleaseLimitAmount <= 0 then 0
        when @RemainingMaxReleaseLimitAmount <= @PlayableBonus then @RemainingMaxReleaseLimitAmount
        else @PlayableBonus
      end

      -- sanity check
      if @ReleasedBonusWinningsAmount < 0
        RAISERROR('ReleasedBonusWinningsAmount cannot be negative', 16, 1);
      if @ReleasedBonusAmount < 0
        RAISERROR('ReleasedBonusAmount cannot be negative', 16, 1);
      if @ReleasedBonusWinningsAmount + @ReleasedBonusAmount > @MaxReleaseLimitAmount
        RAISERROR('Total released bonus amount cannot exceed MaxReleaseLimitAmount', 16, 1);

    end
end
