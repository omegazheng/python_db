create or alter procedure rpt.usp_AML
(
    @DateFrom date = null,
    @DateTo date =  null,
    @TargetCurrency nvarchar(3) = null,
    @DailyLimit numeric(38,18) = null,
    @7DaysLimit numeric(38,18) = null,
    @30DaysLimit numeric(38,18) = null,
    @90DaysLimit numeric(38,18) = null,
    @180DaysLimit numeric(38,18) = null,
    @PTDLimit numeric(38,18) = null,
    @LTDLimit numeric(38,18) = null,
    @BrandID int = null,
	@StaffID int = null,
    @ReturnExceededOnly bit = 1
)
as
begin
    set nocount on
    if (@TargetCurrency is null)
        begin
            Select @TargetCurrency = (Select top 1 iso_code from admin_all.CURRENCY where is_default = 1)
            Select @TargetCurrency = ISNULL(@TargetCurrency, 'EUR')
        end
    if (@DateFrom is null)
        begin
            declare @DaysBack int = -1 * isnull(admin_all.fn_GetRegistry('aml.alert.startDate.daysBack'), 180);
            select @DateFrom =  dateadd(day, @DaysBack, getdate());
        end

    if (@DateTo is null)
        Select @DateTo = dateadd(day,1, getdate());
    else
        Select @DateTo = dateadd(day,1, @DateTo);

    if (@DailyLimit is null)
        Select @DailyLimit = admin_all.fn_GetRegistry('aml.alert.daily')

    if (@7DaysLimit is null)
        Select @7DaysLimit = admin_all.fn_GetRegistry('aml.alert.7day')

    if (@30DaysLimit is null)
        Select @30DaysLimit = admin_all.fn_GetRegistry('aml.alert.30day')

    if (@90DaysLimit is null)
        Select @90DaysLimit = admin_all.fn_GetRegistry('aml.alert.90day')

    if (@180DaysLimit is null)
        Select @90DaysLimit = admin_all.fn_GetRegistry('aml.alert.180day')
		
        ;with d as
                  (
                      select @DateFrom as Date
                      union all
                      select dateadd(Day, 1, d.Date)
                      from d
                      where Date < @DateTo
                  ),
              ltds as
                  (
                      select p.ACCOUNT_ID as AccountID, 
							sum(case when p.TYPE = 'DEPOSIT' then p.AMOUNT_REAL else 0 end) Deposit,
							sum(case when p.TYPE = 'WITHDRAWAL' then p.AMOUNT_REAL else 0 end) Withdrawal
                      from admin_all.PAYMENT p
                               inner join admin_all.ACCOUNT a on a.id = p.ACCOUNT_ID
                               inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
                      where p.TYPE in('DEPOSIT', 'WITHDRAWAL') and p.STATUS = 'COMPLETED'
                        and (@BrandID is null or @BrandID= u.PARTYID)
						and (  
								@StaffID is null
								or
								exists(select * from admin_all.STAFF_BRAND_TBL sb where sb.BRANDID = u.BRANDID and sb.STAFFID = @StaffID)
							)
                      group by p.ACCOUNT_ID
                  ),
			  ltd as 
			    (
					  select uc.PartyID, sum(ltds.Deposit * r.Rate) DepositLTD, sum(ltds.Withdrawal * r.Rate) WithdrawalLTD
					  from ltds
						inner join external_mpt.v_UserPrimaryCurrency uc with(noexpand) on uc.AssociatedAccountID = ltds.AccountID
                               cross apply admin_all.fn_GetCurrencyConversionRate1(dateadd(day, 1, @DateTo), uc.AssociatedCurrency, uc.Currency) r
					  group by uc.PartyID
				),
              p0 as
                  (
                      select cast(p.PROCESS_DATE as date) Date, p.ACCOUNT_ID as AccountID, 
							sum(case when p.TYPE = 'DEPOSIT' then p.AMOUNT_REAL else 0 end) Deposit,
							sum(case when p.TYPE = 'WITHDRAWAL' then p.AMOUNT_REAL else 0 end) Withdrawal
                      from admin_all.PAYMENT p
                               inner join admin_all.ACCOUNT a on a.id = p.ACCOUNT_ID
                               inner join external_mpt.USER_CONF u on u.PARTYID = a.PARTYID
                      where p.TYPE in ('DEPOSIT', 'WITHDRAWAL') and p.STATUS = 'COMPLETED'
                        and p.PROCESS_DATE >=@DateFrom and p.PROCESS_DATE <=@DateTo
                        and (@BrandID is null or @BrandID= u.PARTYID)
						and (  
								@StaffID is null
								or
								exists(select * from admin_all.STAFF_BRAND_TBL sb where sb.BRANDID = u.BRANDID and sb.STAFFID = @StaffID)
							)
                      group by  cast(p.PROCESS_DATE as date), p.ACCOUNT_ID
                  ),
              p1 as
                  (
                      select uc.PartyID, p0.Date, uc.Currency, 
							sum(p0.Deposit * r.Rate) Deposit, sum(p0.Withdrawal * r.Rate) Withdrawal
                      from p0
                               inner join external_mpt.v_UserPrimaryCurrency uc with(noexpand) on uc.AssociatedAccountID = p0.AccountID
                               cross apply admin_all.fn_GetCurrencyConversionRate1(dateadd(day, 1, p0.Date), uc.AssociatedCurrency, uc.Currency) r
                      group by uc.PartyID, p0.Date, uc.Currency
                  )
                 ,p as
                  (
                      select p1.PartyID, p1.Date, p1.Deposit, p1.Withdrawal, p1.Currency, p1.Deposit * r.Rate ConvertedDeposit, p1.Withdrawal * r.Rate ConvertedWithDrawal, 
							ltd.DepositLTD * r.Rate ConvertedDepositLTD, ltd.WithdrawalLTD * r.Rate ConvertedWithdrawalLTD, ltd.DepositLTD, ltd.WithdrawalLTD
                      from p1
                               cross apply admin_all.fn_GetCurrencyConversionRate1(dateadd(day, 1, p1.Date), p1.Currency, @TargetCurrency) r
							   inner join ltd on ltd.PartyID = p1.PartyID
                  )
                 ,a as
                  (
                      select distinct PartyID, p.Currency
                      from p
                  ),
              s as
                  (
                      select d.Date, a.PartyID, p.Deposit, p.Withdrawal, isnull(p.Currency, a.Currency) Currency , p.ConvertedDeposit, p.ConvertedWithDrawal, p.ConvertedDepositLTD, p.ConvertedWithdrawalLTD, p.DepositLTD, p.WithdrawalLTD
                      from d
                               cross join a
                               left join p on p.Date = d.Date and p.PartyID = a.PartyID
                  ),
              r as
                  (
                      select s.Date, s.PartyID, s.Deposit, s.Withdrawal, s.Currency, s.ConvertedDeposit, s.ConvertedWithDrawal,
                             first_value(s.date) over(partition by s.PartyID order by s.Date rows between 6 preceding and current row) DateFrom7,
                             Last_value(s.date) over(partition by s.PartyID order by s.Date rows between 6 preceding and current row) DateTo7,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 6 preceding and current row) Deposit7,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 6 preceding and current row) ConvertedDeposit7,
							 sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 6 preceding and current row) Withdrawal7,
                             sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 6 preceding and current row) ConvertedWithdrawal7,


                             first_value(s.date) over(partition by s.PartyID order by s.Date rows between 29 preceding and current row) DateFrom30,
                             Last_value(s.date) over(partition by s.PartyID order by s.Date rows between 29 preceding and current row) DateTo30,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 29 preceding and current row) Deposit30,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 29 preceding and current row) ConvertedDeposit30,
							 sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 29 preceding and current row) Withdrawal30,
                             sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 29 preceding and current row) ConvertedWithdrawal30,

                             first_value(s.date) over(partition by s.PartyID order by s.Date rows between 89 preceding and current row) DateFrom90,
                             Last_value(s.date) over(partition by s.PartyID order by s.Date rows between 89 preceding and current row) DateTo90,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 89 preceding and current row) Deposit90,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 89 preceding and current row) ConvertedDeposit90,
							 sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 89 preceding and current row) Withdrawal90,
                             sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 89 preceding and current row) ConvertedWithdrawal90,

                             first_value(s.date) over(partition by s.PartyID order by s.Date rows between 179 preceding and current row) DateFrom180,
                             Last_value(s.date) over(partition by s.PartyID order by s.Date rows between 179 preceding and current row) DateTo180,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 179 preceding and current row) Deposit180,
                             sum(s.Deposit) over(partition by s.PartyID order by s.Date rows between 179 preceding and current row) ConvertedDeposit180,
							 sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 179 preceding and current row) Withdrawal180,
                             sum(s.Withdrawal) over(partition by s.PartyID order by s.Date rows between 179 preceding and current row) ConvertedWithdrawal180,

--                          first_value(s.date) over(partition by s.PartyID order by s.Date) DateFromPTD,
--                          Last_value(s.date) over(partition by s.PartyID order by s.Date) DateToPTD,
                             sum(s.Deposit) over(partition by s.PartyID) DepositPTD,
                             sum(s.ConvertedDeposit) over(partition by s.PartyID) ConvertedDepositPTD,
							 sum(s.Withdrawal) over(partition by s.PartyID) WithdrawalPTD,
                             sum(s.ConvertedWithDrawal) over(partition by s.PartyID) ConvertedWithdrawalPTD,

                             max(s.DepositLTD) over(partition by s.PartyID) DepositLTD,
                             max(s.ConvertedDepositLTD) over(partition by s.PartyID) ConvertedDepositLTD,
							 max(s.WithdrawalLTD) over(partition by s.PartyID) WithdrawalLTD,
                             max(s.ConvertedWithDrawalLTD) over(partition by s.PartyID) ConvertedWithdrawalLTD
                      from s
                  ),
              r1 as
                  (
                      select r.Date, r.PartyID, 
							 r.Deposit, r.Withdrawal, r.Currency, r.ConvertedDeposit, r.ConvertedWithDrawal, isnull(r.ConvertedDeposit, 0) + isnull(r.ConvertedWithDrawal, 0) ConvertedTotal,
                             case when isnull(r.ConvertedDeposit, 0) > @DailyLimit or isnull(r.ConvertedWithDrawal, 0) > @DailyLimit or isnull(r.ConvertedDeposit, 0) + isnull(r.ConvertedWithDrawal, 0) > @DailyLimit then 1 end ExceedDailyLimit,

                             r.DateFrom7, r.DateTo7, r.Deposit7, r.Withdrawal7, r.ConvertedDeposit7, r.ConvertedWithdrawal7, isnull(r.ConvertedDeposit7, 0) + isnull(r.ConvertedWithDrawal7, 0) ConvertedTotal7, 
							 case when isnull(r.ConvertedDeposit7, 0) > @7DaysLimit or isnull(r.ConvertedWithDrawal7, 0) > @7DaysLimit or isnull(r.ConvertedDeposit7, 0) + isnull(r.ConvertedWithDrawal7, 0) > @7DaysLimit then 1 end Exceed7Limit,

							 r.DateFrom30, r.DateTo30, r.Deposit30, r.Withdrawal30, r.ConvertedDeposit30, r.ConvertedWithdrawal30, isnull(r.ConvertedDeposit30, 0) + isnull(r.ConvertedWithDrawal30, 0) ConvertedTotal30, 
							 case when isnull(r.ConvertedDeposit30, 0) > @30DaysLimit or isnull(r.ConvertedWithDrawal30, 0) > @30DaysLimit or isnull(r.ConvertedDeposit30, 0) + isnull(r.ConvertedWithDrawal30, 0) > @30DaysLimit then 1 end Exceed30Limit,

                             r.DateFrom90, r.DateTo90, r.Deposit90, r.Withdrawal90, r.ConvertedDeposit90, r.ConvertedWithdrawal90, isnull(r.ConvertedDeposit90, 0) + isnull(r.ConvertedWithDrawal90, 0) ConvertedTotal90, 
							 case when isnull(r.ConvertedDeposit90, 0) > @90DaysLimit or isnull(r.ConvertedWithDrawal90, 0) > @90DaysLimit or isnull(r.ConvertedDeposit90, 0) + isnull(r.ConvertedWithDrawal90, 0) > @90DaysLimit then 1 end Exceed90Limit,

                             r.DateFrom180, r.DateTo180, r.Deposit180, r.Withdrawal180, r.ConvertedDeposit180, r.ConvertedWithdrawal180, isnull(r.ConvertedDeposit180, 0) + isnull(r.ConvertedWithDrawal180, 0) ConvertedTotal180, 
							 case when isnull(r.ConvertedDeposit180, 0) > @180DaysLimit or isnull(r.ConvertedWithDrawal180, 0) > @180DaysLimit or isnull(r.ConvertedDeposit180, 0) + isnull(r.ConvertedWithDrawal180, 0) > @180DaysLimit then 1 end Exceed180Limit,

                             r.DepositPTD, r.WithdrawalPTD, r.ConvertedDepositPTD, r.ConvertedWithdrawalPTD, isnull(r.ConvertedDepositPTD, 0) + isnull(r.ConvertedWithDrawalPTD, 0) ConvertedTotalPTD, 
							 case when isnull(r.ConvertedDepositPTD, 0) > @PTDLimit or isnull(r.ConvertedWithDrawalPTD, 0) > @PTDLimit or isnull(r.ConvertedDepositPTD, 0) + isnull(r.ConvertedWithDrawalPTD, 0) > @PTDLimit then 1 end ExceedPTDLimit,

                             r.DepositLTD, r.WithdrawalLTD, r.ConvertedDepositLTD, r.ConvertedWithdrawalLTD, isnull(r.ConvertedDepositLTD, 0) + isnull(r.ConvertedWithDrawalLTD, 0) ConvertedTotalLTD, 
							 case when isnull(r.ConvertedDepositLTD, 0) > @LTDLimit or isnull(r.ConvertedWithDrawalLTD, 0) > @LTDLimit or isnull(r.ConvertedDepositLTD, 0) + isnull(r.ConvertedWithDrawalLTD, 0) > @LTDLimit then 1 end ExceedLTDLimit
                      from r

                  )
         select r1.*,u.USERID, u.KYC_STATUS,
                @TargetCurrency as TargetCurrency
         from r1
                  inner join external_mpt.user_conf u on r1.PartyID = u.PARTYID
         where @ReturnExceededOnly = 0
            or ExceedDailyLimit = 1
            or Exceed7Limit = 1
            or Exceed30Limit = 1
            or Exceed90Limit = 1
            or Exceed180Limit = 1
            or ExceedPTDLimit = 1
            or ExceedLTDLimit = 1
         order by r1.PartyID, r1.Date
         option(maxrecursion 0)
end
go
--exec rpt.usp_AML
--rpt.usp_AML1

--select * from external_mpt.USER_CONF where USERID = 'omegazheng'
