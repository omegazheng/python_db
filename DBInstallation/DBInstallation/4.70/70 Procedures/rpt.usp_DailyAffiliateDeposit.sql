set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_DailyAffiliateDeposit
(
	@DateFrom date = null,
	@DateTo date = null,
	@BrandIDs varchar(max) = null, 
	@OutputColumns varchar(max) output
)
as
begin
	set nocount on
	create table #BrandID(BrandID int, BrandName nvarchar(1000), PaymentFeeRate decimal(38,18), GameRoyaltyRate numeric(38, 18), Currency varchar(10) primary key (BrandID))
	declare @CurrencyType varchar(100) = isnull(admin_all.fn_GetRegistry('external.export.affiliate.targetCurrency'), 'USD')
	insert into #BrandID(BrandID, BrandName, PaymentFeeRate, GameRoyaltyRate, Currency)
		select BRANDID, BRANDNAME, 
				try_cast(admin_all.fn_GetBrandRegistry(BRANDID, 'external.export.affiliate.paymentFeeRate') as numeric(38, 18)), 
				try_cast(admin_all.fn_GetBrandRegistry(BRANDID, 'external.export.affiliate.gameRoyaltyRate') as numeric(38, 18)),

				case 
					when @CurrencyType = 'CURRENCY_DEFAULT' then c.iso_code
					when @CurrencyType = 'BRAND_CURRENCY_DEFAULT' then bc.ISO_CODE
					when @CurrencyType = 'PLAYER_PRIMARY' then 'zzzzzzzzzz'
					else @CurrencyType
				end
		from admin.CASINO_BRAND_DEF b
			outer apply (select top 1 ISO_CODE from admin_all.BRAND_CURRENCY bc where IS_DEFAULT = 1 and IS_ENABLED = 1 and bc.BRANDID = b.BRANDID) bc
			outer apply (select top 1 ISO_CODE from admin_all.CURRENCY where is_default = 1) c
		where @BrandIDs is null
			or BRANDID in (
							select cast(value as int) 
							from (select value from string_split(@BrandIDs, ',') s)  a
						)
	select @DateFrom = isnull(@DateFrom, dateadd(day, -1, getdate()))
	select @DateTo = isnull(@DateTo, getdate())

	select BrandID, Ratio, GameInfoID
		into #Ratio
	from (
			select
					brandid AS BrandID,
					ratio   AS Ratio,
					GAME_INFO_ID GameInfoID,
					row_number() over(partition by brandid, GAME_INFO_ID order by last_update desc) rn
			from admin_all.BRAND_GAME_JP_RATIO 
			where last_update < @DateTo
		) a
	where rn = 1
	create unique clustered index i on #Ratio(BrandID, GameInfoID)
	; with x0 as
	(
		select	t.VALUE as BTAG, b.BrandName as BRAND, aa.Date as TRANSACTION_DATE, aa.PartyID as PARTYID, case when @CurrencyType = 'PLAYER_PRIMARY' then aa.Currency else isnull(b.Currency, aa.Currency) end Currency,
				cast(isnull(cr.Rate, 1) * sum(isnull(case when aa.TranType in ('CHARGE_BCK') then - (aa.AmountReal + aa.AmountReleasedBonus) end, 0)) as numeric(38, 18)) as ChargeBack,
				cast(isnull(cr.Rate, 1) * sum(isnull(case when aa.TranType in ('DEPOSIT') then aa.AmountReal + aa.AmountReleasedBonus end, 0)) as numeric(38, 18)) as Deposit,
				--sum(isnull(case when aa.TranType in ('GAME_BET') then -(aa.AmountReal + aa.AmountReleasedBonus) end, 0)) as Bets,
				cast(isnull(cr.Rate, 1) * sum(isnull(case when aa.TranType in ('GAME_BET', 'GAME_WIN') then -(aa.AmountReal + aa.AmountReleasedBonus) end, 0)) as numeric(38, 18)) as Revenue,
				cast(isnull(cr.Rate, 1) * sum(isnull(case when aa.TranType in ('BONUS_REL') then aa.AmountReleasedBonus end, 0)) as numeric(38, 18)) as Bonus,
				cast(isnull(cr.Rate, 1) * sum(isnull(case when aa.TranType in ('GAME_BET') then -(aa.AmountReal + aa.AmountReleasedBonus) end, 0)) as numeric(38, 18)) as Stake,

				cast(isnull(cr.Rate, 1) * 
				sum(
					cast(
							isnull(case when aa.TranType in ('GAME_BET', 'GAME_WIN') then -(aa.AmountReal + aa.AmountReleasedBonus) end, 0) -- Revenue
							- isnull(case when aa.TranType in ('BONUS_REL') then aa.AmountReleasedBonus end, 0) -- Bonus
							- isnull(case when aa.TranType in ('GAME_BET', 'MACHIN_EFT') then -(aa.AmountReal + aa.AmountReleasedBonus) end * r.Ratio/100, 0) --JPC
							- isnull(case when aa.TranType in ('DEPOSIT') then aa.AmountReal * b.PaymentFeeRate/100.0 end, 0) --PaymentFee
							- isnull(case when aa.TranType in ('GAME_BET') then -(aa.AmountReal + aa.AmountReleasedBonus) * b.GameRoyaltyRate / 100.0 end, 0) -- GameRoyalty
							- isnull(case when aa.TranType in ('CHARGE_BCK') then - (aa.AmountReal + aa.AmountReleasedBonus) end, 0) --ChargeBack
							as numeric(38,18)
						 )
					) as numeric(38, 18))NGR,
				cast(sum(cast(isnull(case when aa.TranType in ('GAME_BET') then 1 end, 0) as numeric(38, 18))) as numeric(38, 18)) as BetCount,
				cast(isnull(cr.Rate, 1) * sum(cast(isnull(case when aa.TranType in ('GAME_BET', 'MACHIN_EFT') then -(aa.AmountReal + aa.AmountReleasedBonus) end * r.Ratio/100, 0) as numeric(38, 18))) as numeric(38, 18)) JPC,
				cast(isnull(cr.Rate, 1) * sum(cast(isnull(case when aa.TranType in ('DEPOSIT') then aa.AmountReal * b.PaymentFeeRate/100.0 end, 0) as numeric(38, 18))) as numeric(38, 18)) as PaymentFee,
				cast(isnull(cr.Rate, 1) * sum(cast(isnull(case when aa.TranType in ('GAME_BET') then -(aa.AmountReal + aa.AmountReleasedBonus) * b.GameRoyaltyRate / 100.0 end, 0) as numeric(38, 18))) as numeric(38, 18)) as GameRoyalty
		from admin_all.AccountTranAggregate aa
			inner join #BrandID b on b.BrandID = aa.BrandID
			inner join external_mpt.USER_CONF u on u.PARTYID = aa.PartyID
			inner join admin_all.USER_TRACKING_CODE t on t.PARTYID = u.PARTYID and CODE_KEY = 'btag' 
			left join admin_all.GAME_INFO on aa.GameID = GAME_INFO.GAME_ID AND aa.ProductID = GAME_INFO.PLATFORM_ID
			left join #Ratio r on r.BrandID = aa.BrandID and r.GameInfoID = GAME_INFO.ID
			outer apply (select cast(Rate as numeric(38,18)) Rate from admin_all.fn_GetCurrencyConversionRate1(dateadd(day, 1, aa.Date), aa.Currency, case when @CurrencyType = 'PLAYER_PRIMARY' then aa.Currency else b.Currency end) cr) cr
		where aa.Period='D'
			and aa.Date >= @DateFrom
			and aa.Date < @DateTo
			and aa.TranType in ('GAME_BET', 'GAME_WIN', 'DEPOSIT', 'CHARGE_BCK', 'BONUS_REL', 'MACHIN_EFT')
			and aa.AggregateType = case when @CurrencyType = 'PLAYER_PRIMARY' then 3 else 0 end
		group by t.VALUE , b.BrandName , aa.Date, aa.PartyID, case when @CurrencyType = 'PLAYER_PRIMARY' then aa.Currency else isnull(b.Currency, aa.Currency) end, cr.Rate
	)
	select * into #ret0 from x0;
	
	select BTAG, BRAND, TRANSACTION_DATE, PARTYID, Type + '_' + Currency Type,  Amount into #ret
	--from x0
	from(
			select  BTAG, BRAND, TRANSACTION_DATE, PARTYID, Currency, ChargeBack, Deposit, Revenue, Bonus, Stake, NGR, BetCount, JPC, PaymentFee, GameRoyalty
			from #ret0
		) p
			unpivot (Amount for Type in (ChargeBack, Deposit, Revenue, Bonus, Stake, NGR, BetCount, JPC, PaymentFee, GameRoyalty)) p1;
		
	create table #Columns(Type varchar(100), ID int identity(1,1))
	if exists(select * from #ret)
	begin
		insert into #Columns(Type)
			select t +'_' + Currency Type--, identity(int, 1, 1) ID into 
			from (select distinct Currency from #ret0) c
				cross apply (values(1, 'ChargeBack'), (2, 'Deposit'), (3, 'GameRoyalty'), (4, 'Revenue'), (5, 'Bonus'), (6, 'Stake'), (7, 'NGR'), (8, 'BetCount'), (9, 'JPC'), (10, 'PaymentFee')) t(o, t)
			order by c.Currency, t.o
	end
	else
	begin
		insert into #Columns(Type)
			select t +'_' + Currency Type--, identity(int, 1, 1) ID into #Columns
			from (select @CurrencyType as Currency ) c
				cross apply (values(1, 'ChargeBack'), (2, 'Deposit'), (3, 'GameRoyalty'), (4, 'Revenue'), (5, 'Bonus'), (6, 'Stake'), (7, 'NGR'), (8, 'BetCount'), (9, 'JPC'), (10, 'PaymentFee')) t(o, t)
			order by c.Currency, t.o
	end

	select @OutputColumns = 'BTAG, BRAND,TRANSACTION_DATE,PARTYID'+isnull((select ',' + c.Type from #Columns c order by c.ID for xml path(''), type). value('.', 'nvarchar(max)'), '');

	declare @SQL nvarchar(max)
	select @SQL = '
					select BTAG, BRAND, TRANSACTION_DATE, PARTYID'+(select ', ' + quotename(c.Type) from #Columns c order by c.ID for xml path(''), type). value('.', 'nvarchar(max)')+'
					from (
							select BTAG, BRAND, TRANSACTION_DATE, PARTYID, Type,  Amount
							from #ret
						) p
					pivot(
							sum(Amount)
							for Type in ('+stuff((select ', ' + quotename(c.Type) from #Columns c order by c.ID for xml path(''), type). value('.', 'nvarchar(max)'), 1, 2, '') +')
						) p1
					Order by TRANSACTION_DATE, BTAG, BRAND, PARTYID
				'
	exec(@SQL)
	
end
go
--declare @OutputColumns varchar(max)
--exec rpt.usp_DailyAffiliateDeposit @DateFrom = '2020-01-01', @OutputColumns = @OutputColumns output
--select @OutputColumns
--go
--select * from admin_all.AccountTranAggregate where Period = 'D' and PartyID in(select a.PartyID from admin_all.USER_TRACKING_CODE a)

--select * from admin_all.USER_TRACKING_CODE utc where CODE_KEY = 'BTAG'

--select utc.VALUE as btag,
--       uc.BRANDID as brand,
--       uc.REG_DATE as account_opening_date,
--       uc.PARTYID as player_id,
--       uc.USERID as username,
--       uc.COUNTRY
--from admin_all.USER_TRACKING_CODE utc
--    join external_mpt.USER_CONF uc on uc.PARTYID = utc.PARTYID
--where CODE_KEY = 'btag' and uc.REG_DATE > '2019-01-01'