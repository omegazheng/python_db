set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_BatchUpdateUserActionLogWithCommentPair
(
	@PartyIDs varchar(max),
	@StaffID int,
	@ActionType varchar(50),
	@CommentPair varchar(max)
)
as
begin
	set nocount on

	create table #CommentPairTable(PartyId int not null, Comment nvarchar(max) not null)

	insert into #CommentPairTable(PartyId, Comment)
	select json_value(value, '$.partyId') PartyId, json_value(value, '$.comment') Comment
	from openjson(@CommentPair)

	insert into admin_all.USER_ACTION_LOG(DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE)
		select getdate(), @StaffID, c.Comment, u.PARTYID, @ActionType
		from #CommentPairTable c
		    inner join external_mpt.USER_CONF u ON c.PartyId = u.PARTYID

end
go
--begin transaction
-- exec admin_all.usp_BatchUpdateUserActionLogWithCommentPair @PartyIDs= '10000', @StaffId = '7343',
--      @ActionType = 'EXT_BONUS', @CommentPair = '[{"partyId":100089922,"comment":"KambiBatchAssignBonusProgram id=0,status=ACTIVE,expirationDate=TueFeb2514:35:57PST2020,bonusProgramId=145209,playerId=100089922,currency=BR,currencies=null)"},{"partyId":100105978,"comment":"KambiBatchAssignBonusProgram(id=1,status=ACTIVE,expirationDate=TueFeb2514:35:57PST2020,bonusProgramId=145209,playerId=100105978,currency=BR,currencies=null)"},{"partyId":100121504,"comment":"KambiBatchNotAssignBonusPrograme(playerId=100121504,errorMessage=Exception1)"},{"partyId":100123034,"comment":"KambiBatchNotAssignBonusPrograme(playerId=100123034,errorMessage=Exception1)"}]'
--select top 10 * from admin_all.USER_ACTION_LOG order by 1 desc
--rollback
