set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_ListInactiveUsers
(
	@DaysBack int = 180,
	@BrandIDs varchar(max) = null
)
as
begin
	set nocount on
	declare @Date datetime = dateadd(d, -@DaysBack, getdate())
	declare @SQL nvarchar(max)
	select @SQL = '
	select 
			 u.BRANDID, u.PARTYID, u.PRIMARY_WALLET_UUID, u.REG_DATE, l.LoginTime, a.Date LastBettingTime
	from external_mpt.USER_CONF u
		left join admin_all.account ac on ac.partyid = u.partyid
		outer apply (select top 1 l.login_time LoginTime from admin_all.USER_LOGIN_LOG l where u.PARTYID = l.partyid and LOGIN_TYPE = ''LOGIN'' order by login_time desc ) l
		--outer apply (select top 1 Date from admin_all.AccountTranAggregate aggr where aggr.TranType = ''GAME_BET'' and aggr.PartyID = u.PARTYID and Period = ''D'' and AggregateType = 0 order by date desc) a
		outer apply (select top 1 datetime Date from admin_all.ACCOUNT_TRAN aggr where aggr.Tran_Type = ''GAME_BET'' and aggr.account_id = ac.id order by datetime desc) a
	where (u.REG_DATE < @Date)
		and (l.LoginTime is null or l.LoginTime < @Date)
		and (a.Date is null or a.Date < @Date)
		'+ case when @BrandIDs is not null then 'and u.BrandID in('+@BrandIDs+')' else '' end
	exec sp_executesql @SQL, N'@Date datetime', @Date
end
go
--exec rpt.usp_ListInactiveUsers @BrandIDs = '1'