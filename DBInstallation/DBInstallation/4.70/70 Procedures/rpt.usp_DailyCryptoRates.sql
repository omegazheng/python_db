set ansi_nulls, quoted_identifier on
go
create or alter procedure rpt.usp_DailyCryptoRates
(
	@Date date = null,
	@OutputColumns varchar(max) = null output
)
as
begin
    DECLARE @DateFrom DATETIME
    DECLARE @DateTo DATETIME
	set nocount on
    SET @DateFrom = isnull(@Date, getdate())
    SET @DateTo = DATEADD(DD, 1, @DateFrom);
	select @OutputColumns = 'Coin,Price(USD)';
    with x0 as(
        select max(RateID) as RateID,  CurrencyFrom, CurrencyTo from admin_all.CurrencyConversionRate ccr
        where ccr.EffectiveDate > @DateFrom and ccr.EffectiveDate < @DateTo  and CurrencyTo = N'USD'
          and CurrencyFrom in (select iso_code from admin_all.CURRENCY where TYPE = 'c')
        group by CurrencyFrom, CurrencyTo
    )
    select
        CURRENCY.iso_code as Coin,
        ccr2.Rate as [Price(USD)]
    from admin_all.CURRENCY
             left join x0 on admin_all.CURRENCY.iso_code = x0.CurrencyFrom
             left join admin_all.CurrencyConversionRate ccr2 on x0.RateID = ccr2.RateID
    where CURRENCY.TYPE = 'c';

end
go
--exec rpt.usp_DailyCryptoRates '2000-01-01'
go

