set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_TrimDownTestDatabase
(
	@Exceptions varchar(max) = null
)
as
begin
	set nocount on 
	if object_id('maint.DisabledTriggers') is not null
	begin
		throw 500000, 'maint.DisabledTriggers is in the database, please ensure all triggers in this table are enabled, drop this table, and continue', 16
	end
	
	exec('select  ''enable trigger '' + quotename(object_schema_name(parent_id)) + ''.''  + quotename(name) + '' on '' + quotename(object_schema_name(parent_id)) + ''.'' + quotename(object_name(parent_id)) + '';'' EnableTrigger, 
		''disable trigger '' + quotename(object_schema_name(parent_id)) + ''.''  + quotename(name) + '' on '' + quotename(object_schema_name(parent_id)) + ''.'' + quotename(object_name(parent_id)) + '';'' DisableTrigger
		into maint.DisabledTriggers
	from sys.triggers
	where is_disabled = 0
	')

	create table #ExceptObjects(ObjectID int primary key with(ignore_dup_key=on))
	insert into #ExceptObjects
	select object_id(Name)
	from (
			values 
			('[admin].[CASINO_BRAND_DEF]'), 

			('admin_all.ACCOUNT'),
			
			 ('[admin_all].[AlertCode]'),('admin_all.AUTH_OPERATION'),('admin_all.AUTH_OPERATION_API'),('admin_all.AUTH_PERMISSION'),('admin_all.AUTH_ROLE'),
			('admin_all.AUTH_ROLE_OPERATION'), ('admin_all.AUTH_USER_ROLE'), ('admin_all.AUTHORITY'),

			('[admin_all].[BRAND_ACTION]'), ('[admin_all].[BRAND_ALIAS]'), ('[admin_all].[BRAND_AREA_CODE]'), ('[admin_all].[BRAND_CCLEVEL]'), ('[admin_all].[BRAND_CCLEVEL_LIMIT]'),
			('[admin_all].[BRAND_CONTENT]'), ('[admin_all].[BRAND_COUNTRY]'), ('[admin_all].[BRAND_CURRENCY]'), ('[admin_all].[BRAND_LANGUAGE]'), ('[admin_all].[BRAND_REGISTRY]'),

			('[admin_all].[COMMENT_TAG]'), ('[admin_all].[COUNTRY]'), ('[admin_all].[CURRENCY]'),

			('[admin_all].[DW_ALL_DAYS]'), ('[admin_all].[DW_ALL_MONTHS]'), ('[admin_all].[DW_ALL_YEARS]'),

			('[admin_all].[EMAIL_TEMPLATE]'), ('[admin_all].[ENCRYPTION_KEYS]'),

			('[admin_all].GAME_CATEGORY'), ('[admin_all].[GAME_INFO]'), ('[admin_all].[GAME_PREFERENCE]'),

			('[admin_all].[GEO_IP_OVERRIDE]'), ('[admin_all].[GEO_IP_RANGE]'),

			('[admin_all].[IOVATION_EVIDENCE]'),

			('[admin_all].[LANGUAGE]'), 

			('[admin_all].[PAYMENT_METHOD]'), ('[admin_all].[PAYMENT_REJECT_REASON]'), ('[admin_all].[PLATFORM]'),

			('[admin_all].[REGISTRY_HASH]'), ('[admin_all].[ROLE]'), ('[admin_all].[ROLE_PERMISSION]'), ('[admin_all].[ROLE_RESOURCE]'), ('[admin_all].[segment]'),

			('[admin_all].[STAFF_AUTH_TBL]'), ('[admin_all].[STAFF_ROLE]'), ('[admin_all].[SUB_PLATFORM]'), ('[admin_all].[SYSTEM_TAB]'),

			('[admin_all].[TIME_ZONE]'),

			('[admin_all].[VIP_STATUS]'),
			
			('[dbo].[DatabaseVersion]'), ('[dbo].[sysdiagrams]'),

			('[external_mpt].[USER_CONF]'),

			('[maint].[QueryEvaluationType]'), ('[maint].[FilterType]')

		)n(Name)
	if @Exceptions is not null
	begin
		insert into #ExceptObjects(ObjectID)
			select object_id(rtrim(ltrim(value)))
			from string_split(@Exceptions, ',')
	end

	declare @SQL nvarchar(max), @ObjectID int, @i int = 0
	select @SQL = (select  cast(DisableTrigger as nvarchar(max)) from maint.DisabledTriggers for xml path(''), type) .value('.', 'nvarchar(max)')
	exec(@SQL)
	select @SQL = 'alter database '+quotename(db_name())+' set recovery simple'
	exec(@SQL)
	update admin_all.BONUS_PLAN set SEQ_BP_MAP_ID = null
	while @i < 100
	begin

		declare c1 cursor local static for 
			select t.object_id 
			from sys.tables t 
			where exists(select * from sys.partitions p where p.object_id = t.object_id and p.rows > 0) 
				and not exists(select * from #ExceptObjects eo where eo.ObjectID = t.object_id)
	
		open c1
		if @@cursor_rows = 0
			break;
		fetch next from c1 into @ObjectID
		while @@fetch_status = 0
		begin
			select @SQL = ' set nocount on 
							begin try
								while exists(select * from sys.partitions where object_id = @ObjectID and rows > 0)
									delete top(10000) ' + quotename(object_schema_name(@ObjectID))+'.'+ quotename(object_name(@ObjectID)) + '
								print ' + quotename('Records in ' +quotename(object_schema_name(@ObjectID))+'.'+ quotename(object_name(@ObjectID)) + ' are removed.', '''') + '
							end try
							begin catch
								print ''	'' + error_message()
							end catch
							'
			exec sp_executesql @SQL, N'@ObjectID int', @ObjectID
			fetch next from c1 into @ObjectID
		end
		close c1
		deallocate c1
		select @i = @i + 1
	end

	select @SQL = (select  cast(EnableTrigger as nvarchar(max)) from maint.DisabledTriggers for xml path(''), type) .value('.', 'nvarchar(max)')
	exec(@SQL)


	if object_id('maint.DisabledTriggers') is not null
		drop table maint.DisabledTriggers

	select object_schema_name(t.object_id) SchemaName, object_name(t.object_id) TableName, sum(p.rows) Rows, case when e.ObjectID is null then 'Failed to Remove' else 'Reserved' end Status
	from sys.tables t 
		inner join sys.partitions p on p.object_id = t.object_id and p.index_id in(0,1)
		left join #ExceptObjects e on e.ObjectID = t.object_id
	where p.rows > 0
	group by object_schema_name(t.object_id), object_name(t.object_id), case when e.ObjectID is null then 'Failed to Remove' else 'Reserved' end
	order by 1,2
	select @SQL = (select 'dbcc shrinkfile('+quotename(name, '''')+') with NO_INFOMSGS;' from sys.database_files for xml path(''))
	exec(@SQL)
end
go


--exec maint.usp_TrimDownTestDatabase


/*


declare @x nvarchar(max)
select @x = (
select distinct 'kill ' + cast(request_session_id as varchar(20))+';' from sys.dm_tran_locks where resource_database_id = db_id('omega_dev1') for xml path('')
)
exec(@x)
drop  database [omega_dev1] 

*/
