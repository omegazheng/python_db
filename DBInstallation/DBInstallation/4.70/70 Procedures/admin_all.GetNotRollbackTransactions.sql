set ansi_nulls, quoted_identifier on
go

IF EXISTS(
        SELECT *
        FROM DBO.SYSOBJECTS
        WHERE ID =
              OBJECT_ID(N'[admin_all].[usp_GetNotRollbackTransactions]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
    DROP PROCEDURE [admin_all].[usp_GetNotRollbackTransactions]
GO

create or alter PROCEDURE [admin_all].[usp_GetNotRollbackTransactions]
(
    @AccountId INT,
    @ProductId INT,
    @GameTranId NVARCHAR(100)
)
AS
BEGIN
    set nocount on

    SELECT a.*
    FROM admin_all.ACCOUNT_TRAN a
    WHERE a.account_id = @AccountId
      AND a.platform_id = @ProductId
      and a.game_tran_id = @GameTranId
      and a.TRAN_TYPE = 'GAME_BET'
      and a.ROLLBACK_TRAN_ID is null
      and not exists(select * from admin_all.ACCOUNT_TRAN r where a.ID = r.ROLLBACK_TRAN_ID)
end
go

-- exec [admin_all].[usp_GetNotRollbackTransactions] 1000004 , 3, '1803499669'

-- SELECT * FROM admin_all.ACCOUNT_TRAN
-- WHERE account_id = 1000004 AND platform_id = 3 and game_tran_id ='1803499669' and TRAN_TYPE = 'GAME_BET'
--   and ROLLBACK_TRAN_ID is null
--   AND id  not in (
--     select ROLLBACK_TRAN_ID from ACCOUNT_TRAN
--     where account_id = 1000004 AND platform_id = 3 and game_tran_id ='1803499669' and TRAN_TYPE = 'GAME_BET'
--       and ROLLBACK_TRAN_ID is not null


-- exec admin_all.usp_GetNotRollbackTransactions @AccountId=1000004, @ProductId=3, @GameTranId='1803499669'
