set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_SearchSSWTran
(
	@BrandIDs varchar(max) = null,
	@PartyIDs varchar(max) = null,
	@SeqNumbers varchar(max) = null,
	@OmegaSessionKeys varchar(max) = null,
	@RequestTimeStart datetime = null,
	@RequestTimeEnd datetime = null,
	@IsErrorCodeNull bit = null,
	@IsErrorMessageNull bit = null,
	@TopN int  = null,
	@Orderby nvarchar(max) = 'ID desc'
)
as
begin
	set nocount on 
	create table #BrandID (BrandID int primary key);
	create table #PartyID (PartyID int primary key);
	create table #SeqNumber (SeqNumber varchar(100) collate database_default primary key with(ignore_dup_key = on));
	create table #OmegaSessionKey (OmegaSessionKey varchar(32) collate database_default primary key with(ignore_dup_key = on));
	insert into #BrandID(BrandID)
		select BRANDID
		from admin.CASINO_BRAND_DEF b
		where exists(
							select *
							from string_split(@BrandIDs, '.') s
							where try_cast(rtrim(ltrim(s.value)) as int) = b.BRANDID
					)
	insert into #PartyID(PartyID)
		select PartyID
		from external_mpt.USER_CONF b
		where exists(
							select *
							from string_split(@PartyIDs, '.') s
							where try_cast(rtrim(ltrim(s.value)) as int) = b.PartyID
					)
	insert into #SeqNumber(SeqNumber)
		select distinct v
		from (
				select left(nullif(rtrim(ltrim(s.value)), ''), 100)  v
				from string_split(@SeqNumbers, '.') s
			) a
		where a.v is not null
	insert into #OmegaSessionKey(OmegaSessionKey)
		select distinct v
		from (
				select left(nullif(rtrim(ltrim(s.value)), ''), 32)  v
				from string_split(@OmegaSessionKeys, '.') s
			) a
		where a.v is not null
	
	declare @SQL nvarchar(max)
	select @TopN = isnull(@TopN, 1000)
	select @SQL = '
		select top ('+cast(@TopN as varchar(20))+') DATEDIFF ( millisecond , REQUEST_TIME, RESPONSE_TIME) SSWResponseTimeInMillisecond, * 
		from admin_all.SSW_TRAN t 
		where (1=1)
			'+case when exists(select * from #BrandID) then 'and exists(select * from #BrandID b where b.BrandID = t.BRAND_ID)' else '' end+'
			'+case when exists(select * from #PartyID) then 'and exists(select * from #PartyID b where b.PartyID = t.PARTY_ID)' else '' end+'
			'+case when exists(select * from #SeqNumber) then 'and exists(select * from #SeqNumber b where b.SeqNumber = t.SEQ_NUMBER)' else '' end+'
			'+case when exists(select * from #OmegaSessionKey) then 'and exists(select * from #OmegaSessionKey b where b.OmegaSessionKey = t.OMEGA_SESSION_KEY)' else '' end+'
			'+case when @RequestTimeStart is not null then 'and t.REQUEST_TIME>=@RequestTimeStart' else '' end+'
			'+case when @RequestTimeEnd is not null then 'and t.REQUEST_TIME<@RequestTimeEnd' else '' end+'
			'+case when @IsErrorCodeNull is not null then 'and t.ERROR_CODE' +  case @IsErrorCodeNull when 1 then ' is null ' else ' is not null' end else '' end+'
			'+case when @IsErrorMessageNull is not null then 'and t.ERROR_MESSAGE' +  case @IsErrorMessageNull when 1 then ' is null ' else ' is not null' end else '' end+'
	'  + case when @Orderby is not null then 'order by ' + @Orderby else '' end
	exec sp_executesql @SQL, N'@RequestTimeStart datetime, @RequestTimeEnd datetime', @RequestTimeStart, @RequestTimeEnd
end
go
--exec admin_all.usp_SearchSSWTran 92, '100144609', null, 'IU9LNXMXMN1L294AABGIRXBB9VRA831N', '2019-08-07', '2019-08-07 5:00', 0, 0