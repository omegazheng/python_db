set ansi_nulls, quoted_identifier on
go
/**
  change log: 2020-01-23
  to avoid showing associated accounts
  updated: INNER JOIN [external_mpt].[UserAssociatedAccount] ua WITH (NOLOCK) ON ua.PartyID = u.partyid and ua.IsPrimary = 1
 */
create or alter procedure [admin_all].[usp_GetPlayerRegistration]
  (@staffid int, @brands nvarchar(2000), @startDate DATETIME, @endDate DATETIME)
AS
  BEGIN
    set nocount on
    create table #Brands (
      BrandID int primary key
    )
    insert into #Brands (BrandID)
    SELECT distinct BRANDID
    FROM [admin_all].[STAFF_BRAND_TBL]
    WHERE STAFFID = @staffid
      AND BRANDID IN (select cast(Item as int) brandId from admin_all.fc_splitDelimiterString(@brands, ','))

    DECLARE @startDateLocal DATETIME
    DECLARE @endDateLocal DATETIME
    SET @startDateLocal = @startDate
    SET @endDateLocal = DATEADD(DD, 1, @endDate);
    DECLARE @hideME bit;

    SELECT @hideME = admin_all.fn_GetRegistry('hide_mobile_email')
    print @hideME
    print @hideME

    SELECT u.PARTYID,
           u.USERID,
           u.NICKNAME,
           u.FIRST_NAME,
           u.LAST_NAME,
           u.CURRENCY,
           u.BRANDID,
           b.BRANDNAME,
           u.COUNTRY,
           c.NAME,
           u.REG_DATE,
           u.ACTIVE_FLAG,
           u.LOCKED_STATUS,
           plr.code as lockReason,
           case when @hideME != 1 then u.email
           else null end as email,
           u.PHONE,
           u.PHONE2,
           case when @hideME != 1 then u.MOBILE_PHONE
           else null end as mobilePhone
    FROM [external_mpt].[user_conf] u WITH (NOLOCK)
           INNER JOIN [external_mpt].[UserAssociatedAccount] ua WITH (NOLOCK) ON ua.PartyID = u.partyid and ua.IsPrimary = 1
           LEFT JOIN [admin].[casino_brand_def] b WITH (NOLOCK) ON b.brandid = u.brandid
           LEFT JOIN [admin_all].[country] c on u.country = c.iso2_Code
           left JOIN admin_all.PLAYER_LOCK_REASON as plr ON plr.id = u.player_lock_reason_id
    WHERE u.REG_DATE >= @startDateLocal
      AND u.REG_DATE < @endDateLocal
      AND u.brandId IN (SELECT x.BrandID FROM #Brands x)
    ORDER BY u.REG_DATE DESC
  END

go
