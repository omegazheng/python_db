
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_CreateUser
    @UserID nvarchar(30) = null,
    @Password NVARCHAR(30) = 'Omega123',
    @Currency NVARCHAR(10) = null,
    @FirstName nvarchar(20) = '',
    @LastName nvarchar(20) = '',
    @Address nvarchar(50) = '',
    @City nvarchar(20) = '',
    @Province nvarchar(20) = '',
    @Country nvarchar(2) = null,
    @PostCode nvarchar(20) = '',
    @Phone nvarchar(20) = '',
    @Email nvarchar(20) = null,
    @BirthDate date = '1900-01-01',
    @BrandID int = 1,
    @RegStatus nvarchar(20) =  'PLAYER',
    @Mobile nvarchar(20) = '',
    @UUID nvarchar(95) = null,
    @BalanceReal numeric(12, 2) = 0
AS
begin
    if @UserID is null select @UserID = substring(CONVERT(varchar(40), NEWID()), 0, 9);
    if @UUID is null select @UUID = @UserID;
    if @Email is null select @Email =  '';
    if (@RegStatus <>  'QUICK_REG' and @RegStatus <> 'PLAYER' and  @RegStatus <>  'OPEN' and  @RegStatus <> 'REGISTERED'
        and  @RegStatus <> 'AGENT' and  @RegStatus <> 'CARD_REG' )
        begin
            raiserror('Invalid Registration Status Provided. %s', 16,1, @RegStatus)
        end

    if @Currency is null
        select @Currency = iso_code from admin_all.BRAND_CURRENCY where is_default = 1 AND BRANDID = @BrandID
    if @Currency is null
        select @Currency = iso_code from admin_all.CURRENCY where is_default = 1
    if @Currency is null
        select @Currency = 'EUR'

    if @Country is null
        select @Country = country from admin_all.BRAND_COUNTRY where BRANDID = @BrandID and IS_DEFAULT = 1
    if @Country is null
        select @Country = 'GB'

    declare @PartyID INTEGER;
    begin transaction
        insert into  EXTERNAL_MPT.USER_CONF
        (USERID, PASSWORD, Currency, ACTIVE_FLAG, FIRST_NAME, LAST_NAME,
         ADDRESS, CITY, PROVINCE, COUNTRY, POSTAL_CODE, PHONE, EMAIL,
         BIRTHDATE, REG_DATE, GENDER, BRANDID, REGISTRATION_STATUS, MOBILE_PHONE, LOCKED_STATUS, PRIMARY_WALLET_UUID)
        values
        (
            @UserID, pwdencrypt(@Password), @Currency, 1, @FirstName, @LastName,
            @Address,  @City,@Province, @Country,  @PostCode, @Phone, @Email,
            @BirthDate , getdate(), 'N',  @BrandID,  @RegStatus,  @Mobile, 'NOT_LOCKED', @UUID
        )

        select @PartyID = SCOPE_IDENTITY();
        print @PartyID;
        insert into admin_all.account (PARTYID, BALANCE_REAL) values (@PartyID, @BalanceReal)

        select a.id, a.PARTYID, a.BALANCE_REAL, a.RELEASED_BONUS, a.PLAYABLE_BONUS, a.RAW_LOYALTY_POINTS,
               u.*
        from external_mpt.user_conf u join admin_all.account a on a.partyId = u.partyId where userid = @UserID

        exec admin_all.usp_IpsCreatePrimaryAssociateAccount @PartyID =  @PartyID
end
    commit
go


-- exec admin_all.usp_CreateUser @UserID = 'omegazheng', @Currency = 'EUR', @BrandID = 1, @BalanceReal=1000
