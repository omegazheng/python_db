SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(
        SELECT *
        FROM DBO.SYSOBJECTS
        WHERE ID =
              OBJECT_ID(N'[admin_all].[usp_GetAggregateAccountTranForPeriod]')
          AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
    DROP PROCEDURE [admin_all].[usp_GetAggregateAccountTranForPeriod]
GO

CREATE PROCEDURE [admin_all].[usp_GetAggregateAccountTranForPeriod]
(
    @PrimaryPartyId INT,
    @StartDate datetime,
    @EndDate datetime,
    @TranType VARCHAR(255)
)
AS
BEGIN
    set nocount, xact_abort on
    declare @Amount numeric(38,18)
	create table #AccountTranAggregateByPrimary (Amount numeric(38,18))

    ;with aa(AssociatePartyId) as
    (
        select AssociatedPartyID from external_mpt.UserAssociatedAccount where PartyID = @PrimaryPartyId
    ), atha(AmountReal) as
    (
        select AmountReal from admin_all.AccountTranHourlyAggregate
        where PartyID in (select AssociatePartyId from aa)
          and TranType = @TranType
          and Datetime >= @StartDate
          and Datetime < dateadd(hour, -1, @EndDate)
          and AggregateType = 3
    )
    insert into #AccountTranAggregateByPrimary(Amount)
    (
        select atha.AmountReal from atha
    )

    ;with aa(AssociatePartyId) as
    (
        select AssociatedPartyID from external_mpt.UserAssociatedAccount where PartyID = @PrimaryPartyId
    ), a(Id) as
    (
        select ID from admin_all.ACCOUNT where PARTYID in (select AssociatePartyId from aa)
    ), actce(AmountReal) as
    (
        select ace.AmountReal from admin_all.ACCOUNT_TRAN act
        join admin_all.AccountTranCurrencyExtension ace on act.ID = ace.AccountTranID
        where act.ACCOUNT_ID in (select Id from a)
            and act.TRAN_TYPE = @TranType
            and act.DateTime >= @EndDate
            and act.DateTime < dateadd(hour, 1, @EndDate)
    )
    insert into #AccountTranAggregateByPrimary(Amount)
    (
        select actce.AmountReal from actce
    )

    set @Amount = (select sum(Amount) from #AccountTranAggregateByPrimary)

    IF (OBJECT_ID('tempdb..#AccountTranAggregateByPrimary') IS NOT NULL)
    BEGIN
        DROP TABLE #AccountTranAggregateByPrimary
    END

    select @Amount
END
GO
--
-- exec admin_all.usp_GetAggregateAccountTranForPeriod
--      @PrimaryPartyId=100250739, @TranType='GAME_BET',
--      @StartDate='2019-12-16T00:00:00Z', @EndDate='2019-12-16T14:00:00Z'
