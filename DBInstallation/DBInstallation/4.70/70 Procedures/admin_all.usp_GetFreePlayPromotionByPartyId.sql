set ansi_nulls, quoted_identifier on
go
-- use @partyid for player with one currency only
-- use @partyIds for player with multi-currency
-- DO NOT INPUT BOTH @partyid and @partyIds
-- add currency as output
create or alter procedure admin_all.usp_GetFreePlayPromotionByPartyId
  (
    @partyId INT = null,
    @startDate datetime = null,
    @toDate datetime = null,
    @partyIds VARCHAR(1000) = null
  )
as
  begin

    if @toDate is not null
      select @toDate =  DATEADD(DD, 1, @toDate)

    select  p.id as id,
            p.freeplay_plan_id as freePlayPlanId,
            p.code as code,
            p.status as status,
            p.trigger_date as triggerDate,
            p.last_updated_time as lastUpdatedTime,
            p.is_completed as isCompleted,
            p.party_id as partyId,
            isnull(p.total_balance,0) as totalBalance,
            isnull(p.remaining_balance,0) as remainingBalance,
            isnull(p.amount_won,0) as amountWon,
            p.default_stake_level as defaultStakeLevel,
            g.GAME_ID as gameId,
            p.GAME_INFO_ID as gameInfoId,
            p.line as line,
            isnull(p.coin,0) as coin,
            isnull(p.denomination,0) as denomination,
            isnull(p.bet_per_round,0) as betPerRound,
            u.currency
    from
      admin_all.FREEPLAY_PROMOTION p
      join admin_all.GAME_INFO g on g.ID = p.GAME_INFO_ID
      join EXTERNAL_MPT.USER_CONF u on u.PARTYID = p.party_id
    where
      (trigger_date >= @startDate or @startDate is null)
      and (trigger_date < @toDate or @toDate is null)
      and (@partyid is null or PARTY_ID = @partyid)
      and (@partyIds is null or PARTY_ID in (SELECT * FROM [admin_all].fc_splitDelimiterString(@partyIds, ',')))
    ORDER BY
      id DESC
  end

go
