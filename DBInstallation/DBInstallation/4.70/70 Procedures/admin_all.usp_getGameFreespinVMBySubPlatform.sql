set ansi_nulls on
go
set quoted_identifier on
go
CREATE OR ALTER PROCEDURE  admin_all.usp_getGameFreespinVMBySubPlatform
(
  @subPlatformCode nvarchar(100) = null
)
as
begin
    select fo.ID as id, fo.PLATFORM_ID as platformId, fo.GAME_ID as gameId, fo.GAME_NAME as gameName, fo.LINES as lines,
        fo.COINS as coins, fo.DENOMINATIONS as denominations, g.GAME_LAUNCH_ID as gameLaunchId
from admin_all.FREESPIN_OPTION fo
  join admin_all.GAME_INFO g on g.GAME_ID=fo.GAME_ID and g.PLATFORM_ID=fo.PLATFORM_ID
join admin_all.PLATFORM p on p.ID = fo.PLATFORM_ID
 join admin_all.SUB_PLATFORM sp on sp.PLATFORM_ID=p.ID and g.SUB_PLATFORM_ID=sp.id
where sp.CODE = @subPlatformCode order by GAME_NAME asc;
    return
end

-- exec admin_all.usp_getGameFreespinVMBySubPlatform @subPlatformCode = 'PLAYNGO'