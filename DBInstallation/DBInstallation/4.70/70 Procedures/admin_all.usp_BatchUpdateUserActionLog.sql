set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_BatchUpdateUserActionLog
(
	@PartyIDs varchar(max),
	@StaffID int,
	@ActionType varchar(50),
	@Comment varchar(max)
)
as
begin
	set nocount on 
	insert into admin_all.USER_ACTION_LOG(DATE, STAFFID, COMMENT, PARTYID, ACTION_TYPE)
		select getdate(), @StaffID, @Comment, u.PARTYID, @ActionType
		from external_mpt.USER_CONF u
		where exists(select * from string_split(@PartyIDs, ',') p where try_cast(p.value as int) = u.PARTYID)
end
go
--begin transaction
--exec admin_all.usp_BatchUpdateUserActionLog '100122231,100252084',1,'sdd','sss'
--select top 10 * from admin_all.USER_ACTION_LOG order by 1 desc
--rollback