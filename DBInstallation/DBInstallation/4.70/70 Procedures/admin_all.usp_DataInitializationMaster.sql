set ansi_nulls, quoted_identifier on
go
if object_id('admin_all.usp_DataInitializationMaster') is null
	exec('create procedure admin_all.usp_DataInitializationMaster as --')
go
alter procedure admin_all.usp_DataInitializationMaster
as
begin
	set nocount on
	/*
	select '	exec '+object_schema_name(object_id)+'.'+ name +';'
	from sys.procedures
	where name like 'usp!_DataInitialization%' escape '!'
		and name not in ('usp_DataInitializationHelper', 'usp_DataInitializationMaster')
	order by 1
	*/
	--NEVER COMMENT OUT THE CODE IN THIS PROCEDURE PLEASE.
	--exec admin_all.usp_DataInitializationAuthority;
	--exec admin_all.usp_DataInitializationAuthorization;
	exec admin_all.usp_DataInitializationBonusPlan
	exec admin_all.usp_DataInitializationCurrency
	exec admin_all.usp_DataInitializationDWTables;
	exec admin_all.usp_DataInitializationEncryptionKeys;
	exec admin_all.usp_DataInitializationGameCategory
	exec admin_all.usp_DataInitializationIovationEvidence;
	exec admin_all.usp_DataInitializationMaint;
	exec admin_all.usp_DataInitializationPlatform;
	exec admin_all.usp_DataInitializationRegistryHash;
	exec admin_all.usp_DataInitializationSegment;
	exec admin_all.usp_DataInitializationSystemTab;
	exec admin_all.usp_DataInitializationTemplate;
	exec admin_all.usp_DataInitializationTimeZone;
	exec admin_all.usp_DataInitializationAgentPermission;
    exec admin_all.usp_DataInitializationLockReason;
	exec chronos.usp_DataInitialization;
	exec security.usp_DataInitializationSecurable;
	exec security.usp_DataInitializationPrincipal;
	exec admin_all.usp_MigrateIdentityCardNumberToUserConf;
	exec admin_all.usp_CreateOmegaSymmetricKeyGeneral
end
go
