set ansi_nulls on
go
set quoted_identifier on
go

if object_id(N'[admin_all].[usp_CreateProductCore4]') is null
  exec('create procedure admin_all.usp_CreateProductCore4 as --')
go
alter procedure admin_all.usp_CreateProductCore4
(
  @Code nvarchar(50),
  @Name nvarchar(50),
  @Username nvarchar(50),
  @ProductType nvarchar (50) = null,
  @WalletType nvarchar(50) = null,
  @ProductPassword nvarchar(50) = null,
  @ProductID int = null output,
  @SegmentID int = null,
  @ExcludedCountries nvarchar(50) = null
)
as
begin
	set nocount, xact_abort on
	select @ProductID = ID from admin_all.platform where platform.code = @Code
	if @@rowcount > 0
		goto ___End___
	declare @SegmentIDBackup int = 0
	select	@ProductType = isnull(@ProductType, 'CASINO'),
			@WalletType = isnull(@WalletType, 'GAME_PLAY'),
			@ProductPassword = isnull(@ProductPassword, @Code)
	select @SegmentIDBackup = id from admin_all.segment where name = @ProductType
	select @SegmentID = isnull(@SegmentID, @SegmentIDBackup)
	begin transaction
	insert into admin_all.platform (code, name, is_enabled, username, password, wallet_type,
									platform_type, convertjackpots, session_limit_web, session_limit_mobile,
									reality_check_web, reality_check_mobile, excluded_countries,
									provider_id, segment_id)
     values (	@Code, @Name, 1, @Username, admin_all.fn_getMD5(@ProductPassword), @WalletType,
				@ProductType, 1, 'iframe',
				'iframe', 'iframe', 'iframe', @ExcludedCountries,
				null, @SegmentID)
     select @ProductID = scope_identity()
     Update admin_all.platform set provider_id = @ProductID where id = @ProductID
	 commit
___End___:

end

go