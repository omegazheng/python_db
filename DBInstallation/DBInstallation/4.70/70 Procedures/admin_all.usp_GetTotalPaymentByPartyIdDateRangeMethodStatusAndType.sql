SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE admin_all.usp_GetTotalPaymentByPartyIdDateRangeMethodStatusAndType
    @PartyID int = null,
    @StartDate datetime = null,
    @EndDate datetime = null,
    @PaymentType nvarchar(300) = 'DEPOSIT',
    @PaymentStatuses nvarchar(1000) = 'PENDING, COMPLETED',
    @PaymentMethod nvarchar(100) = null
AS
begin
    set nocount on
    if @StartDate IS NULL or @EndDate IS NULL
        begin
            raiserror ('Date is required.', 16, 1)
            return
        end
    if @PartyID IS NULL
        begin
            raiserror ('Date is required.', 16, 1)
            return
        end
    create table #PaymentStatus(PaymentStatus nvarchar(100) primary key  WITH (IGNORE_DUP_KEY = ON))
    insert into #PaymentStatus(PaymentStatus)
    select distinct cast(rtrim(ltrim(value)) as nvarchar(100)) as Status
    from string_split(@PaymentStatuses, ',')

    create table #PaymentType(PaymentType nvarchar(100) primary key  WITH (IGNORE_DUP_KEY = ON))
    insert into #PaymentType(PaymentType)
    select distinct cast(rtrim(ltrim(value)) as nvarchar(100)) as Type
    from string_split(@PaymentType, ',')

    declare @SQL nvarchar(max)
    select @SQL = '
        select Isnull(sum(payment.amount),0) as total from
            external_mpt.USER_CONF users
            join admin_all.account account  on users.partyId = account.partyId
            join admin_all.payment payment  on payment.ACCOUNT_ID  = account.ID
        where
            payment.request_date >= @StartDate and
            payment.request_date < @EndDate and
            users.PARTYID = @PartyID and
            payment.type  in (select PaymentType from #PaymentType) and
            payment.status in (select PaymentStatus from #PaymentStatus)
            ' + case when @PaymentMethod IS NOT NULL then 'and payment.method = @PaymentMethod' else '' end + '
    '
    exec sp_executesql @SQL, N'@PartyId int, @StartDate datetime, @EndDate datetime, @PaymentMethod nvarchar(100)',
        @PartyID, @StartDate, @EndDate, @PaymentMethod
END
go
-- exec  admin_all.usp_GetTotalPaymentByPartyIdDateRangeMethodStatusAndType
--     @PartyID = 100252130, @StartDate =  '2020-02-19', @EndDate = '2020-02-21',
--       @PaymentType = 'DEPOSIT', @PaymentStatuses = 'PENDING, COMPLETED',
--       @PaymentMethod = 'OMEGA_BANK'
