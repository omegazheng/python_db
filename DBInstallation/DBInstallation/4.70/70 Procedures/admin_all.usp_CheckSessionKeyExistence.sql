set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_CheckSessionKeyExistence
(
  @SessionKey [nvarchar](100) = null,
  @PartyId int = null,
  @Status int = null output
)
as
begin

  if (@SessionKey is null)
    RAISERROR('@SessionKey cannot be null', 16, 1)

	if (@PartyId is null)
	  if (exists(select * from admin_all.USER_WEB_SESSION where SESSION_KEY = @SessionKey)
	    or exists(select * from admin_all.USER_LOGIN_LOG where SESSION_KEY = @SessionKey))
      select @Status = 1
    else
      select @Status = 0
  else
    if (exists(select * from admin_all.USER_WEB_SESSION where SESSION_KEY = @SessionKey and PARTYID = @PartyId)
	    or exists(select * from admin_all.USER_LOGIN_LOG where SESSION_KEY = @SessionKey and PARTYID = @PartyId))
      select @Status = 1
    else
      select @Status = 0

end

go
