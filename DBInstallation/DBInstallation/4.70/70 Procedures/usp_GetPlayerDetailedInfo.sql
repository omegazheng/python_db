SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create or alter procedure admin_all.usp_GetPlayerDetailedInfo
(@partyid int)
AS
BEGIN
	select
		vip_status as vipLevel,
		country as country,
		first_name + ' ' + last_name as name,

		registration_status as registrationStatus,
		reg_date as regDate,
		brand.brandname as brand,
		brand.BRANDID as brandId,
		vip_status as vipStatus,
		kyc_status as kycStatus,
		kyc_age_status as kycAgeStatus,

		active_flag as accountStatus,
		locked_status as lockStatus,
		plr.code as lockReason,
		locked_until as lockedUntil,
		winners_list as winnersList,
		display_message as displayMessage,
		allow_emails as subscription,
		user_type,
		userid,
		nickname,
		user_conf.partyid,
		gender,
		cclevel_id as cclevel,
		bcc.CCL_NAME as cclevelName,

		email as email,
		phone as phone,
		mobile_phone as mobilePhone,
		iovation_status as iovationStatus,
		iovation_check as iovationCheck,
		raw_loyalty_points as rawLoyaltyPoints,
		PRIMARY_WALLET_UUID as primaryWalletUuid,

		currency,
		NUMBER_DECIMAL_POINT as currencyNumberDecimalPoint,

		account.balance_real as realMoney,
		account.released_bonus as withdrawableBonus,
		account.playable_bonus as playableBonus

	from external_mpt.user_conf
			 join admin.casino_brand_def as brand on brand.brandid = user_conf.brandid
			 join admin_all.account on account.partyid = user_conf.partyid
			 left JOIN admin_all.CURRENCY on CURRENCY.symbol = user_conf.CURRENCY
		 	 left JOIN admin_all.BRAND_CCLEVEL as bcc ON bcc.ID = USER_CONF.CCLEVEL_ID
			 left JOIN admin_all.PLAYER_LOCK_REASON as plr ON plr.id = USER_CONF.player_lock_reason_id
	where user_conf.partyid = @partyid

END
GO
