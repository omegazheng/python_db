SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE admin_all.usp_UpdateExternalJobInfo
(
    @InternalReferenceID nvarchar(50) = null,
    @ExternalJobProvider nvarchar(50) =  null,
    @ExternalJobID nvarchar(255) = null,
    @Status nvarchar(50) = null,
    @CreatedTime datetime = null,
    @LastUpdateTime datetime = null,
    @Request nvarchar(max) = null,
    @Response nvarchar(max) = null
)
as

BEGIN
  if (@InternalReferenceID is not null)
    update admin_all.ExternalJobInfo
    set
      STATUS = isNull(@Status, STATUS),
      CREATED_TIME = isNUll(@CreatedTime, CREATED_TIME),
      LAST_UPDATE_TIME = isNUll(@LastUpdateTime, LAST_UPDATE_TIME),
      REQUEST = isNUll(@Request, REQUEST),
      RESPONSE = isNUll(@Response, RESPONSE)
    where
      INTERNAL_REFERENCE_ID = @InternalReferenceID;
  else if (@ExternalJobProvider is not null and @ExternalJobID is not null)
    update admin_all.ExternalJobInfo
    set
      STATUS = isNull(@Status, STATUS),
      CREATED_TIME = isNUll(@CreatedTime, CREATED_TIME),
      LAST_UPDATE_TIME = isNUll(@LastUpdateTime, LAST_UPDATE_TIME),
      REQUEST = isNUll(@Request, REQUEST),
      RESPONSE = isNUll(@Response, RESPONSE)
    where
      EXTERNAL_JOB_PROVIDER = @ExternalJobProvider and EXTERNAL_JOB_ID = @ExternalJobID;
  else
    RAISERROR('Either @InternalReferenceID or @ExternalJobProvider and @ExternalJobID must be not null', 16, 1)
END
