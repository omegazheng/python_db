set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_PopulateDataFeed
as
begin
	set nocount, xact_abort  on
	declare @SQL nvarchar(max), @MinAccountTranID bigint, @DaysBack int = 32, @BatchSize int = 10000, @MaxAccountTranID bigint
	declare c cursor static for
		select 'alter partition scheme PS_DataFeed next used '+(select quotename(name) from sys.filegroups where is_default = 1)+';
	ALTER PARTITION FUNCTION PF_DataFeed ()  SPLIT RANGE ('+cast(BRANDID as varchar(100))+');  '
		from admin.CASINO_BRAND_DEF
		where BRANDID not in (
								select cast(value as int)
								from sys.partition_functions pf
									inner join sys.partition_range_values pr on pf.function_id = pr.function_id
								where pf.name = 'PF_DataFeed'
							)
	open c
	fetch next from c into @SQL
	while @@fetch_status = 0
	begin
		exec(@SQL)
		fetch next from c into @SQL
	end
	close c
	deallocate c
	
	select @MaxAccountTranID = isnull((select max(ID) from admin_all.ACCOUNT_TRAN_REALTIME ), 0)
	if @MaxAccountTranID = 0
		select @MaxAccountTranID = isnull((select max(ID) from admin_all.ACCOUNT_TRAN_ALL ), 0)

	select @MinAccountTranID = AccountTranID
	from (
			select top 1 bf.AccountTranID
			from admin.CASINO_BRAND_DEF b
				cross apply (select max(AccountTranID) AccountTranID from  admin_all.DataFeed df where df.BrandID = b.BRANDID) bf
			order by 1 desc
		) a
	if @MinAccountTranID is null
	begin
		select @MinAccountTranID = isnull((select top 1 ID from admin_all.ACCOUNT_TRAN_REALTIME where DATETIME < cast(cast(dateadd(day, -@DaysBack, getdate()) as date) as datetime) order by DATETIME desc), 0)
		if @MinAccountTranID = 0
			select @MinAccountTranID = isnull((select top 1 ID from admin_all.ACCOUNT_TRAN_ALL where DATETIME < cast(cast(dateadd(day, -@DaysBack, getdate()) as date) as datetime) order by DATETIME desc), 0)
	end
	
	waitfor delay '00:00:05'
	while exists(select * from admin_all.ACCOUNT_TRAN where ID > @MinAccountTranID and ID <= @MaxAccountTranID)
	begin
		insert into admin_all.DataFeed(
										BrandID, AccountTranID, Datetime, PartyID, UserID, 
										Currency, ProductID, ProductCode, ProductTranID, GameInfoID, 
										GameID, GameTranID, TranType, AmountReal, AmountPlayableBonus, 
										AmountReleasedBonus, BalanceReal, BalancePlayableBonus, BalanceReleasedBonus, 
										RollbackTranID, RollbackTranType
									)
			select
					u.BRANDID BrandID, t.ID AccountTranID, t.DATETIME Datetime, a.PARTYID PartyID, u.USERID UserID, 
					u.CURRENCY Currency, 

					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.PLATFORM_ID, t.PLATFORM_ID) else t.PLATFORM_ID end ProductID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(rp.CODE, p.CODE) else p.CODE end ProductCode, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.PLATFORM_TRAN_ID, t.PLATFORM_TRAN_ID) else t.PLATFORM_TRAN_ID end ProductTranID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(rgi.id, gi.ID) else gi.ID end GameInfoID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.GAME_ID, t.GAME_ID) else t.GAME_ID end GameID, 
					case when t.ROLLBACK_TRAN_ID is not null then isnull(r.GAME_TRAN_ID, t.GAME_TRAN_ID) else t.GAME_TRAN_ID end GameTranID, 
					
					t.TRAN_TYPE TranType, t.AMOUNT_REAL AmountReal, t.AMOUNT_PLAYABLE_BONUS AmountPlayableBonus, 
					t.AMOUNT_RELEASED_BONUS AmountReleasedBonus, t.BALANCE_REAL BalanceReal, t.BALANCE_PLAYABLE_BONUS BalancePlayableBonus, t.BALANCE_RELEASED_BONUS BalanceReleasedBonus, 
					t.ROLLBACK_TRAN_ID RollbackTranID, r.TRAN_TYPE RollbackTranType
			from admin_all.ACCOUNT_TRAN t
				left join admin_all.ACCOUNT_TRAN r ON t.ROLLBACK_TRAN_ID = r.ID
				left join admin_all.ACCOUNT a ON t.ACCOUNT_ID = a.id
				left join external_mpt.USER_CONF u ON u.PARTYID = a.PARTYID
				left join admin_all.GAME_INFO gi on gi.GAME_ID = t.GAME_ID AND gi.PLATFORM_ID = t.PLATFORM_ID
				left join admin_all.platform p ON p.ID = t.PLATFORM_ID

				left join admin_all.GAME_INFO rgi on rgi.GAME_ID = r.GAME_ID AND rgi.PLATFORM_ID = r.PLATFORM_ID
				left join admin_all.platform rp ON rp.ID = r.PLATFORM_ID
			where t.ID > @MinAccountTranID and t.id <= @MinAccountTranID + @BatchSize and t.ID<=@MaxAccountTranID
			option(loop join)
 			select @MinAccountTranID = @MinAccountTranID + @BatchSize
	end
	
	declare @Retention int = 32, @BrandID int, @Date datetime
	select @Date = dateadd(day, -@Retention, getdate())

	declare c cursor local static for
		select brandid from admin.CASINO_BRAND_DEF order by 1
	open c
	fetch next from c into @BrandID
	while @@fetch_status = 0
	begin
		--raiserror('%d', 0, 0, @BrandID) with nowait
		while (1=1)
		begin
			delete top(10000) a
			from admin_all.DataFeed a with(index = IDX_admin_all_DataFeed_BrandID_Datetime)
			where BrandID = @BrandID 
				and Datetime <= @Date
			option(maxdop 1 )
			if @@rowcount = 0
				break;
		end
		fetch next from c into @BrandID
	end
	close c
	deallocate c
	exec admin_all.usp_VerifyDataFeed @BackFromMinutes = 5,	@BackToMinutes = 35, @Autofix = 1, @CheckIntervalInMinutes = 20
end

go
