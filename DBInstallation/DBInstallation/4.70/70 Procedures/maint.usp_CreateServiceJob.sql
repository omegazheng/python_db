set ansi_nulls, quoted_identifier on
go
create or alter procedure maint.usp_CreateServiceJob 
(
	@Name nvarchar(255), 
	@ProcedureName sysname, 
	@Description nvarchar(max), 
	@DeleteAfterRun int = 0, -- 0, job will never be removed. 3, job will be removed after first run
	@Interval int = 10, -- in second
	@DailyAt int = null --hhmmss
)
as
begin
	set nocount, xact_abort on 
	if @DeleteAfterRun not in (0, 3)
	begin
		raiserror('only 0 and 3 can be accepted. 0, job will never be removed. 3, job will be removed after first run', 16, 1)
		return
	end
	if object_id('tempdb..#Schedule') is not null
		drop table #Schedule	
	create table #Schedule(
					schedule_id int, schedule_uid uniqueidentifier,schedule_name sysname, enabled int, freq_type int,
					freq_interval int, freq_subday_type int, freq_subday_interval int, freq_relative_interval int, freq_recurrence_factor int,
					active_start_date int, active_end_date int, active_start_time int, active_end_time int, date_created datetime,
					schedule_description nvarchar(4000), job_count int
	)
	if object_id('tempdb..#JobSchedule') is not null
		drop table #JobSchedule
	create table #JobSchedule(
					schedule_id int,schedule_name sysname, enabled int, freq_type int,
					freq_interval int, freq_subday_type int, freq_subday_interval int, freq_relative_interval int, freq_recurrence_factor int,
					active_start_date int, active_end_date int, active_start_time int, active_end_time int, date_created datetime,
					schedule_description nvarchar(4000), next_run_date int, mext_run_time int, schedule_uid uniqueidentifier, job_count int
	)

	insert into #Schedule
		exec msdb..sp_help_schedule
	
	declare @schedule_uid uniqueidentifier, @schedule_id int, @job_name sysname, @command sysname, @job_id uniqueidentifier, @ScheduleName sysname, @ScheduleType int
	
	select	@job_name = @Name +' - '+quotename(db_name()),
			@command = 'exec '+quotename(db_name())+'.' + @ProcedureName
	

	if @Interval is not null
	begin
		if @Interval <= 100
		begin
			select @ScheduleName = 'Omega Jobs - Every '+ cast (@Interval as varchar(20)) +' seconds'
			select @ScheduleType = 2
		end
		if @Interval > 100
		begin
			select @Interval = @Interval / 60 + case when @Interval % 60 > 0 then 1 else 0 end
			select @ScheduleName = 'Omega Jobs - Every '+ cast (@Interval as varchar(20)) +' minutes'
			select @ScheduleType = 4
		end

		if @Interval > 100
		begin
			select @Interval = @Interval / 60 + case when @Interval % 60 > 0 then 1 else 0 end
			select @ScheduleName = 'Omega Jobs - Every '+ cast (@Interval as varchar(20)) +' hours'
			select @ScheduleType = 8
		end
		if @Interval > 100
		begin
			raiserror('Invalid Schedule', 16, 1)
			return
		end
	end
	if @DailyAt is not null
	begin
		select @ScheduleName = 'Omega Jobs - Daily at '+ stuff(stuff(right('000000'+cast(@DailyAt as varchar(6)), 6), 3,0,':'), 6, 0, ':')
	end

	if @ScheduleName is null
	begin
		raiserror('Unsupported schedule %d', 16, 1, @Interval)
		return
	end
	select @schedule_uid = schedule_uid, @schedule_id = schedule_id from #Schedule where schedule_name = @ScheduleName
	if @@rowcount = 0
	begin
		if @Interval is not null
		begin
			exec  msdb.dbo.sp_add_schedule	@schedule_uid = @schedule_uid output, @schedule_id = @schedule_id output, 
											@schedule_name=@ScheduleName, @enabled=1, 
											@freq_type=4, @freq_interval=1, @freq_subday_type=@ScheduleType, @freq_subday_interval=@Interval, 
											@freq_relative_interval=0, @freq_recurrence_factor=0, @active_start_date=20170720, 
											@active_end_date=99991231, @active_start_time=0, @active_end_time=235959
		end
		if @DailyAt is not null
		begin
			exec  msdb.dbo.sp_add_schedule	@schedule_uid = @schedule_uid output, @schedule_id = @schedule_id output, 
											@schedule_name=@ScheduleName, @enabled=1, 
											@freq_type=4, @freq_interval=1, @freq_subday_type=1, @freq_subday_interval=0, 
											@freq_relative_interval=0, @freq_recurrence_factor=0, @active_start_date=20170720, 
											@active_end_date=99991231, @active_start_time=@DailyAt, @active_end_time=235959
		end
	end

	begin transaction
	select @job_id = job_id from msdb.dbo.sysjobs where name = @job_name
	if @@rowcount = 0
	begin
		exec msdb.dbo.sp_add_job @job_name= @job_name, @enabled=1, @notify_level_eventlog=0, @notify_level_email=0, @notify_level_netsend=0, @notify_level_page=0, @delete_level=@DeleteAfterRun, @description=@Description, /*@category_name=N'Omega Jobs', @owner_login_name=N'admin_all',*/ @job_id = @job_id output
		exec msdb.dbo.sp_add_jobstep @job_id=@job_id, @step_name=@command, @step_id=1, @cmdexec_success_code=0,  @on_success_action=1, @on_success_step_id=0, @on_fail_action=2, @on_fail_step_id=0, @retry_attempts=0, @retry_interval=0, @os_run_priority=0, @subsystem=N'TSQL', @command=@command, @database_name=N'master', @flags=0
		exec msdb.dbo.sp_attach_schedule @job_id=@job_id,@schedule_id = @schedule_id
		exec msdb.dbo.sp_update_job @job_id = @job_id, @start_step_id = 1
		exec msdb.dbo.sp_add_jobserver @job_id = @job_id, @server_name = N'(local)'
	end
	delete #Schedule;
	insert into #JobSchedule
		exec msdb..sp_help_jobschedule @job_name = @job_name
	declare cCreateServiceJob cursor local static for 
		select schedule_id from #JobSchedule where schedule_id <> @schedule_id
	open cCreateServiceJob
	fetch next from cCreateServiceJob into @schedule_id
	while @@fetch_status = 0
	begin
		exec msdb.dbo.sp_attach_schedule @job_id=@job_id,@schedule_id = @schedule_id
		fetch next from cCreateServiceJob into @schedule_id
	end
	close cCreateServiceJob
	deallocate cCreateServiceJob
	commit
end
go

--set xact_abort on
--begin transaction
--exec maint.usp_CreateServiceJob 'test', 'sp_help', 'test'
--commit
go

--msdb..sp_delete_job @job_name='test - [omega_prod]'