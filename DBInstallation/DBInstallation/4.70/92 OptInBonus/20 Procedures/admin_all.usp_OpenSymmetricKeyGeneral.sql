set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_OpenSymmetricKeyGeneral
as
begin
	set nocount on
	declare @SQL nvarchar(max)
	if not exists(select * from sys.openkeys where database_name = db_name() and key_name = 'OmegaSymmetricKeyGeneral')
	begin
		select @SQL = 'open symmetric key OmegaSymmetricKeyGeneral decryption by password = '+quotename(admin_all.fn_GetOmegaSymmetricKeyGeneralPassword(), '''')+';'
		exec(@SQL)
	end
end
go
