set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_GetBonusPlansWithOptInContext
(
	@SessionKey nvarchar(100), 
	@PartyID int, 
	@BonusPlanIDs varchar(max)
)
as
begin
	set nocount on 
	exec admin_all.usp_OpenSymmetricKeyGeneral
	;with x0 as
	(
		select try_cast(value as int) BonusPlanID, @SessionKey SessionKey, @PartyID PartyID, getdate() CreationDate
		from string_split(@BonusPlanIDs, ',')
	),
	x1 as
	(
		select BonusPlanID, 
			convert(varchar(max), admin_all.fn_EncryptBySymmetricKeyGeneral(cast((select BonusPlanID as [@BonusPlanID], SessionKey as [@SessionKey], PartyID as [@PartyID], CreationDate as [@CreationDate] for xml path('OptInContext'), type) as varbinary(max))), 2) OptInContext--,
			--(select BonusPlanID as [@BonusPlanID], SessionKey as [@SessionKey], PartyID as [@PartyID], CreationDate as [@CreationDate] for xml path('OptInContext'), type) aaa
		from x0 
		where BonusPlanID is not null
	)
	select *
	from x1

end
go
--exec admin_all.usp_GetBonusPlansWithOptInContext 'dfadf654kjhkjhkjhjkkjhkjhkjhjkhhhjkbmfadfasd', 91429641, '1,2,3,4,5,6,7'



--select convert(varbinary(max), , 2)