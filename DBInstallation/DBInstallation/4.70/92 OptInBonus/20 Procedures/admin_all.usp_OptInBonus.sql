set ansi_nulls, quoted_identifier on
go
create or alter procedure admin_all.usp_OptInBonus
(
	@SessionKey nvarchar(100), 
	@PartyID int, 
	@OptInContext varchar(max)
)
as
begin
	set nocount, xact_abort on
	exec admin_all.usp_OpenSymmetricKeyGeneral
	declare @BonusPlanID int, @CreationDate datetime, @OptInDate datetime = getdate(), @BonusID int, @LockResource nvarchar(255), @ExpiryDays int, @MaxBonusPerPlayer int, @MaxBonusAll int, @OptInHandle uniqueidentifier,@Comment varchar(max)

	begin transaction
	;with x as
	(
		select admin_all.fn_DecryptBySymmetricKeyGeneral(convert(varbinary(max), @OptInContext, 2)) BinaryContext
	),
	x0 as
	(
		select cast(BinaryContext as xml) XMLcontext, hashbytes('MD5', BinaryContext) OptInHandle
		from x
	),
	x1 as
	(
		select n.value('@BonusPlanID', 'int') BonusPlanID, n.value('@SessionKey', 'nvarchar(100)') SessionKey, n.value('@PartyID', 'int') PartyID, n.value('@CreationDate', 'datetime') CreationDate, OptInHandle
		from x0
			cross apply XMLcontext.nodes('/OptInContext') c(n)
	)
	select @BonusPlanID = BonusPlanID, @CreationDate = CreationDate, @OptInHandle = OptInHandle
	from x1
	where isnull(@SessionKey, '') = isnull(SessionKey, '')
		and PartyID = @PartyID
	if @@rowcount = 0
	begin
        select @Comment = 'Opt In Failed. SessionKey=' + @SessionKey + ', BonusPlanID=' + cast(@BonusPlanID as nvarchar(15)) + ', Reason=Invalid OptInID';
        insert into admin_all.USER_ACTION_LOG(DATE,  PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, NEW_VALUE, COMMENT)
        values(@OptInDate, @PartyID, 'FAIL_OPT_IN_BONUS', @BonusID, 'OptInID', cast(@OptInContext as varchar(max)), @Comment);
        commit;
        throw 50000, @Comment, 16;
	end

	Select @BonusID = BonusID from admin_all.OptInBonusHandle where OptInHandle = @OptInHandle
	if @BonusID is not null
    begin
        select @Comment = 'Opt In Failed. SessionKey=' + @SessionKey + ', BonusPlanID=' + cast(@BonusPlanID as nvarchar(15)) + ', Reason=OptInID has been used by BonusID=' + cast(@BonusID as nvarchar(15));
        insert into admin_all.USER_ACTION_LOG(DATE,  PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, NEW_VALUE, COMMENT)
        values(@OptInDate, @PartyID, 'FAIL_OPT_IN_BONUS', @BonusID, 'OptInID', cast(@OptInContext as varchar(max)), @Comment);
        commit;
        throw 50000, @Comment, 16;
    end


	select @LockResource = cast(@BonusPlanID as nvarchar(20)) + '-BonusPlan'
	exec sp_getapplock @LockResource, 'Exclusive', 'Transaction', -1


	select @ExpiryDays = EXPIRY_DAYS, @MaxBonusPerPlayer = MAX_TRIGGER_PLAYER, @MaxBonusAll = MAX_TRIGGER_ALL
	from admin_all.BONUS_PLAN
		where id = @BonusPlanID
	if isnull(@MaxBonusPerPlayer, 0) >0 or isnull(@MaxBonusAll, 0) > 0
	begin
		if exists(
					
					select *
					from (
							select count(*) CountAll, count(case when PARTYID = @PartyID then 1 end) CountUser
							from admin_all.BONUS 
							where BONUS_PLAN_ID = @BonusPlanID 
								and STATUS not in ('OPTED_OUT')
						) x0
					where isnull(@MaxBonusPerPlayer, 0) > 0 and CountUser >= isnull(@MaxBonusPerPlayer, 0)
						or isnull(@MaxBonusAll, 0) > 0 and CountAll >= isnull(@MaxBonusAll, 0) 
					
				)

		begin
            select @Comment = 'Opt In Failed. SessionKey=' + @SessionKey + ', BonusPlanID=' + cast(@BonusPlanID as nvarchar(15)) + ', Reason=Exceeded bonus max trigger';
            insert into admin_all.USER_ACTION_LOG(DATE,  PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, NEW_VALUE, COMMENT)
            values(@OptInDate, @PartyID, 'FAIL_OPT_IN_BONUS', @BonusID, 'OptInID', cast(@OptInContext as varchar(max)), @Comment);
            commit;
			throw 50000, @Comment, 16
		end
	end
	insert into admin_all.BONUS(
									PARTYID, BONUS_PLAN_ID, TRIGGER_DATE, EXPIRY_DATE, STATUS, 
									AMOUNT_WAGERED, WAGER_REQUIREMENT, AMOUNT, PAYMENT_ID, RELEASE_DATE, 
									AMOUNT_WITHDRAWN, RELEASED_BONUS_WINNINGS, RELEASED_BONUS, PLAYABLE_BONUS_WINNINGS, PLAYABLE_BONUS, 
									RELEASED_BONUS_AMOUNT, RELEASED_BONUS_WINNINGS_AMOUNT, FREE_BET_BALANCE, BONUS_INCREMENTAL_ID, EXTERNAL_BONUS_ID,
									OptInDate
							)
		select						@PartyID PARTYID, @BonusPlanID BONUS_PLAN_ID, dateadd(year, 99, @OptInDate) TRIGGER_DATE, dateadd(day, isnull(@ExpiryDays, 99), @OptInDate) EXPIRY_DATE, 'OPTED_IN' STATUS,
									0 AMOUNT_WAGERED, 0 WAGER_REQUIREMENT, 0 AMOUNT, null PAYMENT_ID, null RELEASE_DATE, 
									0 AMOUNT_WITHDRAWN, 0 RELEASED_BONUS_WINNINGS, 0 RELEASED_BONUS, 0 PLAYABLE_BONUS_WINNINGS, 0 PLAYABLE_BONUS, 
									0 RELEASED_BONUS_AMOUNT, 0 RELEASED_BONUS_WINNINGS_AMOUNT, 0 FREE_BET_BALANCE, null BONUS_INCREMENTAL_ID, null EXTERNAL_BONUS_ID,
									@OptInDate
		
	select @BonusID = scope_identity()
	--- if error out below, the OptInContext has already been used for opt-in
	insert into admin_all.OptInBonusHandle(OptInHandle, BonusID) values(@OptInHandle, @BonusID)
	select @Comment = 'Opt In Succeeded. SessionKey=' + @SessionKey + ', BonusPlanID=' + cast(@BonusPlanID as nvarchar(15)) + ', BonusID=' + cast(@BONUSID as nvarchar(20));
	insert into admin_all.USER_ACTION_LOG(DATE,  PARTYID, ACTION_TYPE, ACTION_ID, FIELD_NAME, NEW_VALUE, COMMENT)
		values(@OptInDate, @PartyID, 'OPT_IN_BONUS', @BonusID, 'OptInID', cast(@OptInContext as varchar(max)), @Comment)
    select @BonusID as BonusID
	commit
end
go
--exec admin_all.usp_OptInBonus 'dfadf654kjhkjhkjhjkkjhkjhkjhjkhhhjkbmfadfasd', 91429641, 0x00B2070A42F8144491422B99D8C05522010000006C1775096A39EF7C0E90CAD0C40C4FBDFF0AA4BCB25109DDDF83B34CBD1F7BB08B1E9CE04A10C3A5569C5E9D6DA4DC08842AA55E179B4B5647C573E423DABB4E8CB69E9C600D311071D4CDB988961B39A1624DA75C5181AED75A1FE8BBE9C9FE39F4BB8242EDEEE3CDB601F57EAC65FDED35EA0ED1F6BD396823CD62419BFFC809D1ED8ECEF0D6D5B1423A03D9605ED48067F14ED47373865DD145838923259B403CB8239B240A7F0B046D2C60AB7E5AE3D73961D2D8AECEBE0EB23BC7D391A86AF3F127684431C8E173DC0C27B7AE9A6F4CC321C3CFD9004E6B09C634F6CA4D86B96B78CFCBD35752990453481C9BA3DD98CB757EC9735D3550D776B2EBE69CCBAD8DA8D00DB63EE4D535B2C7D389C90C692A8865F8669F2F66F2495C59380058CE01EAFF5F5B43F1958D2FE90117923F626EE508FE46597FCEE5CCD3511F557765BC040C3DEE3761647C3E822DAB1A
--select * from admin_all.fn_GetLargeValue('85975272-B196-3CAB-2706-520AFECBE4C9')
--select * from admin_all.OptInBonusLog

--select * from admin_all.bonus where ID = 157463
--select MAX_TRIGGER_PLAYER, * from admin_all.BONUS_PLAN
