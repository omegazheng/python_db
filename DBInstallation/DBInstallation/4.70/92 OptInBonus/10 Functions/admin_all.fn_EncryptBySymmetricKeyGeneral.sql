set ansi_nulls, quoted_identifier on
go
create or alter function admin_all.fn_EncryptBySymmetricKeyGeneral(@PlainText varbinary(max))
returns varbinary(max)
as
begin
	return EncryptByKey(Key_GUID('OmegaSymmetricKeyGeneral'), @PlainText)
end
go


