set ansi_nulls, quoted_identifier on
go

create or alter function [admin_all].[fc_splitDelimiterStringToGameInfoFields](
  @StringWithDelimiter VARCHAR(8000),
  @Delimiter VARCHAR(8))

  RETURNS @ItemTable TABLE(GameID VARCHAR(100), GameName varchar(100), GameLaunchID varchar(100),
    GameCategoryName varchar(100), SubPlatformCode varchar(100), IsMobile bit)

AS
  BEGIN
    declare @ErrorString varchar(1000);
    declare @MaxLen int;
    set @MaxLen = LEN(@StringWithDelimiter)

    DECLARE @StartingPosition int;
    DECLARE @Item varchar(100);
    DECLARE @GameID varchar(100), @GameName varchar(100), @GameLaunchID varchar(100),
        @GameCategoryName varchar(100), @SubPlatformCode varchar(100), @IsMobile bit

    IF LEN(@StringWithDelimiter) = 0 OR @StringWithDelimiter IS NULL
      RETURN;

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    if @StartingPosition = 1
      begin
        set @ErrorString = cast('Error:: @StringWithDelimiter should not begin with the delimiter' as int);
        return;
      end
    SET @GameID = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @GameID = ltrim(rtrim(@GameID))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @GameName = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @GameName = ltrim(rtrim(@GameName))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @GameLaunchID = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @GameLaunchID = ltrim(rtrim(@GameLaunchID))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @GameCategoryName = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @GameCategoryName = ltrim(rtrim(@GameCategoryName))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    SET @SubPlatformCode = SUBSTRING(@StringWithDelimiter, 1, @StartingPosition - 1)
    SET @SubPlatformCode = ltrim(rtrim(@SubPlatformCode))
    SET @StringWithDelimiter = SUBSTRING(@StringWithDelimiter, @StartingPosition + LEN(@Delimiter), @MaxLen)

    SET @StartingPosition = CHARINDEX(@Delimiter, @StringWithDelimiter);
    if @StartingPosition > 0
      begin
        set @ErrorString = cast('Error:: Expected 5 delimiters in @StringWithDelimiter but found more than 3' as int);
        return;
      end
    SET @Item = ltrim(rtrim(@StringWithDelimiter))
    SET @IsMobile =
    case
        when @Item = 'null' then 0 -- default
        when @Item = 'true' then 1
        when @Item = 'false' then 0
        else cast(@Item as bit)
    end

    insert into @ItemTable(GameID, GameName, GameLaunchID, GameCategoryName, SubPlatformCode, IsMobile)
    values(@GameID, @GameName, @GameLaunchID, @GameCategoryName, @SubPlatformCode, @IsMobile)

    RETURN
  END

go

-- select * into #TestGameInfo
-- from admin_all.fc_splitDelimiterStringToGameInfoFields('TestGameId,TestGameName,TestGameLaunchId,TestGameCategory,TestSubPlatform,true', ',')
-- select * from #TestGameInfo