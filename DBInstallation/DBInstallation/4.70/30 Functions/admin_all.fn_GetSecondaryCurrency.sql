SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if object_id('admin_all.fn_GetSecondaryCurrency') is null
	exec ('create function admin_all.fn_GetSecondaryCurrency() returns bit as begin return 1 end')
go
alter function admin_all.fn_GetSecondaryCurrency() 
returns varchar(10)
as 
begin 
	return nullif(rtrim(ltrim(admin_all.fn_GetRegistry('multi.currency.secondary'))), '')
end

