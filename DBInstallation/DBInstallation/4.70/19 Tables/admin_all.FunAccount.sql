set ansi_nulls, quoted_identifier on
go

if object_id('admin_all.FunAccount') is null
    begin
        CREATE TABLE admin_all.FunAccount
        (
--             [ID] [int] IDENTITY(1,1) NOT NULL,
            [PARTYID] [int] NOT NULL,
            BALANCE [numeric](38, 0) NOT NULL

            CONSTRAINT UK_admin_all_FunAccount_PARTYID UNIQUE(PARTYID),
            index IDX_admin_all_FunAccount_PARTYID (PARTYID),
        )

        IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[admin_all].[FK_FUN_ACCOUNT]') AND parent_object_id = OBJECT_ID(N'[admin_all].[FUN_ACCOUNT]'))
        ALTER TABLE [admin_all].[FunAccount]  WITH CHECK ADD  CONSTRAINT [FK_FUN_ACCOUNT] FOREIGN KEY([PARTYID])
            REFERENCES [external_mpt].[USER_CONF] ([PARTYID])
    end

GO

