set ansi_nulls, quoted_identifier on
go

if object_id('admin_all.ExternalJobInfo') is null
begin
	CREATE TABLE admin_all.ExternalJobInfo
	(
		ID bigint identity(1,1) PRIMARY KEY,
		INTERNAL_REFERENCE_ID nvarchar(50),
		EXTERNAL_JOB_PROVIDER nvarchar(50),
		EXTERNAL_JOB_ID nvarchar(255),
		STATUS nvarchar(50),
		CREATED_TIME datetime,
		LAST_UPDATE_TIME datetime,
		REQUEST nvarchar(max),
		RESPONSE nvarchar(max)

		CONSTRAINT UK_admin_all_ExternalJobInfo_INTERNAL_REFERENCE_ID UNIQUE(INTERNAL_REFERENCE_ID),
		CONSTRAINT UK_admin_all_ExternalJobInfo_EXTERNAL_JOB_PROVIDER_EXTERNAL_JOB_ID UNIQUE(EXTERNAL_JOB_PROVIDER, EXTERNAL_JOB_ID),
		index IDX_admin_all_ExternalJobInfo_INTERNAL_REFERENCE_ID (INTERNAL_REFERENCE_ID),
		index IDX_admin_all_ExternalJobInfo_EXTERNAL_JOB_PROVIDER_EXTERNAL_JOB_ID (EXTERNAL_JOB_PROVIDER, EXTERNAL_JOB_ID)
	)
end

GO

