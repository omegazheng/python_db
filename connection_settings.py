import json
from file_utils import load_json


def load_conn_settings(database_name=None):
    connection_settings = {}
    connection_settings = load_json('connection.settings')
    if database_name != None:
        connection_settings['database'] = database_name
    if connection_settings['database'] == None:
        raise Exception('database name is undefined!')
    return connection_settings
