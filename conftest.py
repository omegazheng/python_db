import pytest
import pyodbc
import textwrap
import json
import db_utils
from connection_utils import Context
from connection_settings import load_conn_settings


@pytest.fixture(scope='module')
def connection_settings():
    settings = load_conn_settings()
    yield settings


@pytest.fixture(scope='module')
def master(connection_settings):
    connection_settings["database"] = "master"
    master_cnxn = cnxn(connection_settings)
    yield master_cnxn
    master_cnxn.close()


@pytest.fixture(scope='module')
def execution_cnxn(connection_settings):
    execution_cnxn = cnxn(connection_settings)
    yield execution_cnxn
    execution_cnxn.commit()
    execution_cnxn.close()


@pytest.fixture(scope='module')
def log_cnxn(connection_settings):
    log_cnxn = cnxn(connection_settings)
    yield log_cnxn
    log_cnxn.commit()
    log_cnxn.close()


def cnxn(connection_settings):
    database_name = get_database_name(connection_settings)
    cnxn = pyodbc.connect('DRIVER={};SERVER={};DATABASE={};UID={};PWD={}'.format(
        connection_settings['driver'],
        connection_settings['server'],
        database_name,
        connection_settings['username'],
        connection_settings['password']), autocommit=True)
    return cnxn


def get_database_name(connection_settings):
    database_name = 'master'
    if 'database' in connection_settings:
        database_name = connection_settings['database']
    return database_name


@pytest.fixture
def cursor(execution_cnxn):
    cursor = execution_cnxn.cursor()
    yield cursor
    execution_cnxn.commit()


@pytest.fixture
def context(execution_cnxn, log_cnxn, connection_settings, cursor):
    context = Context(get_database_name(connection_settings))
    context.log_cnxn = log_cnxn
    context.cnxn = execution_cnxn
    return context


@pytest.fixture
def database(execution_cnxn, cursor, connection_settings):
    db = cursor.execute('select db_name()').fetchval()
    yield db
