set nocount on
declare @DatabaseVersion varchar(max)
select @DatabaseVersion = '0'
if object_id('dbo.DatabaseVersion') is null
begin
	create table dbo.DatabaseVersion
	(
		Version varchar(max) not null,
		Date datetime not null constraint DF_dbo_DatabaseVersion_Date default(GetUTCDate())
	)
end

if not exists(select *
from dbo.DatabaseVersion)
begin
	insert into dbo.DatabaseVersion
		(Version)
	values(@DatabaseVersion)
end
go
if object_id('dbo.DatabaseUpdateBatch') is null
begin
	create table dbo.DatabaseUpdateBatch
	(
		BatchID bigint not null identity(1,1) constraint PK_dbo_DatabaseUpdateBatch primary key,
		HasError bit not null constraint DF_dbo_DatabaseUpdateBatch_HasError default(0),
		StartDate datetime not null constraint DF_dbo_DatabaseUpdateBatch_Date default(GetUTCDate()),
		EndDate datetime null,
		LoginName varchar(128) not null constraint DF_dbo_DatabaseUpdateBatch_LoginName default(system_user)
	)
end
go
if object_id('dbo.DatabaseUpdateLog') is null
begin
	create table dbo.DatabaseUpdateLog
	(

		BatchID bigint not null ,
		BatchLogID bigint not null identity(1,1),
		Version varchar(max) not null,
		Path varchar(max) not null,
		FileName varchar(max) not null,
		Message varchar(max),
		IsErrorMessage bit not null constraint DF_dbo_DatabaseUpdateLog_IsErrorMessage default(0),
		StartDate datetime not null constraint DF_dbo_DatabaseUpdateLog_Date default(GetUTCDate()),
		EndDate datetime null,
		constraint PK_dbo_DatabaseUpdateLog primary key(BatchID, BatchLogID)
	)
end
go

create or alter function dbo.GetVersion(@BasePath nvarchar(max), @ScriptPath nvarchar(max)) 
returns varchar(max) 
as 
begin
	select @ScriptPath = stuff(@ScriptPath, 1, len(@BasePath)+1, '')
	if charindex('/', @ScriptPath) = 0
		return null
	return substring(@ScriptPath, 1, charindex('/', @ScriptPath) -1)
end 
go

create or alter procedure dbo.IsScriptDeployable
	@BasePath nvarchar(max),
	@ScriptPath nvarchar(max)
as
begin
	set nocount on
	declare @Version varchar(max), @DBVersion varchar(max)
	select @Version = dbo.GetVersion(@BasePath, @ScriptPath)
	if @Version is null
	begin
		raiserror('not deployable', 16, 1)
		return
	end
	select @DBVersion = Version
	from dbo.DatabaseVersion
	if @DBVersion > @Version
	begin
		raiserror('not deployable', 16, 1)
	end
end
go
--exec dbo.IsScriptDeployable '/home/John/Documents', '/home/John/Documents/0.00/procedures/sql1.sql'

create or alter procedure dbo.StartDeployment
as
begin
	set nocount on
	insert into dbo.DatabaseUpdateBatch
		(HasError, StartDate, LoginName)
	values(0, getutcdate(), system_user)
end
go

create or alter procedure dbo.CreateDatabaseUpdateLog
	(@BatchID bigint output,
	@BatchLogID bigint output,
	@Version varchar(max),
	@Path varchar(max),
	@FileName varchar(max),
	@Message varchar(max),
	@IsErrorMessage bit)
as
begin
	set nocount on
	update dbo.databaseVersion set Version = @Version, Date = GetUTCDate() where Version <= @Version
	if @BatchID = -1
        begin
		insert into dbo.DatabaseUpdateBatch
		default values
		select @BatchID = scope_identity()
	end
	if @BatchLogID = -1
        begin
		insert into dbo.DatabaseUpdateLog
			(BatchID, Version, Path, FileName)
		values(@BatchID, @Version, @Path, @FileName)
		select @BatchLogID = scope_identity()
		return
	end
	update dbo.DatabaseUpdateLog
            set Message = @Message,
                IsErrorMessage = @IsErrorMessage,
                EndDate = GetUTCDate()
        where BatchID = @BatchID
		and BatchLogID = @BatchLogID

	update dbo.DatabaseUpdateBatch
            set EndDate = GetUTCDate(),
                HasError =  case when HasError = 1 or @IsErrorMessage = 1 then 1 else 0 end
        where BatchID = @BatchID
	select @BatchLogID = -1
end
