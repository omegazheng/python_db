--print '$(DatabaseName)'
--print '$(DataPath)'
--print '$(LogPath)'
GO
use [$(DatabaseName)]
go
if not exists (select * from sys.filegroups where name = 'Report')
begin
	alter database [$(DatabaseName)] add filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report1, filename = '$(DataPath)$(DatabaseName)_Report1.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report2, filename = '$(DataPath)$(DatabaseName)_Report2.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report3, filename = '$(DataPath)$(DatabaseName)_Report3.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report4, filename = '$(DataPath)$(DatabaseName)_Report4.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report5, filename = '$(DataPath)$(DatabaseName)_Report5.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report6, filename = '$(DataPath)$(DatabaseName)_Report6.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report7, filename = '$(DataPath)$(DatabaseName)_Report7.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
	alter database [$(DatabaseName)] add file(name = $(DatabaseName)_Report8, filename = '$(DataPath)$(DatabaseName)_Report8.ndf', size = 100MB, maxsize = unlimited, filegrowth = 200MB) to filegroup [Report];
end
GO


