import json
import re
import textwrap
import pytest
import pyodbc
import file_utils
import glob
import os
from termcolor import colored
from pathlib import Path


should_skip = [
    ('__test', True),
    ('.test', True),
    ('test__', False),
    ('_', False)
]

version_check_pass = [
    ("4.90", "4.90"),
    ("4.90", "4.100"),
    ("4.90", "/a/b/c/4.90"),
    ("4.9", "4.9.2"),
    ("5.2.2", "5.3")
]


version_check_fail = [
    ("4.100", "4.90"),
    ("5.2.2", "5.2")
]


@pytest.mark.parametrize("dir, expected", should_skip)
def test_skip(dir, expected):
    result = file_utils.skip(dir)
    assert result == expected


def test_glob():
    files = glob.glob(
        '**/4.30/000 CreateSchema/02 CreateSchemas.sql', recursive=True)
    assert len(files) == 1


def test_list_dir_that_meets_version_requirements():
    root_dir = "list_dir_test"
    Path(os.getcwd() + root_dir + '/').mkdir(parents=True, exist_ok=True)
    Path(os.getcwd() + root_dir + '/4.80').mkdir(parents=True, exist_ok=True)
    Path(os.getcwd() + root_dir + '/4.90').mkdir(parents=True, exist_ok=True)
    Path(os.getcwd() + root_dir + '/3.2.3.a').mkdir(parents=True, exist_ok=True)
    Path(os.getcwd() + root_dir + '/test').mkdir(parents=True, exist_ok=True)
    dirs = os.listdir(root_dir)
    print("Directories Under %s: %s" % (root_dir, dirs))
    result = file_utils.list_dir_that_meets_version_requirements(
        root="list_dir_test", current_version="4.80")
    assert ["4.90", "4.80"].sort() == result.sort()

    result = file_utils.list_dir_that_meets_version_requirements(
        root="list_dir_test", current_version="4.90")
    assert ["4.90"] == result


@pytest.mark.parametrize("current_versiom, version_folder", version_check_pass)
def test_version_ok_valid_number(current_versiom, version_folder):
    assert file_utils.is_folder_version_greater_than_or_equals_to_current_version(
        current_versiom, version_folder)


@pytest.mark.parametrize("current_versiom, version_folder", version_check_fail)
def test_version_ok_failed(current_versiom, version_folder):
    assert file_utils.is_folder_version_greater_than_or_equals_to_current_version(
        current_versiom, version_folder) == False
