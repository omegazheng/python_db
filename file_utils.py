import json
import os
import re
from packaging import version as ver


def load_json(file_name):
    with open(file_name) as json_file:
        connection_settings = json.load(json_file)
    return connection_settings


def is_sql_file(file):
    return file.endWith('.sql')


def skip(d):
    return re.match(r'\.\w+', d) != None or re.match(r'__\w+', d) != None


def skip_dir(current_version, version_folder):
    if not is_folder_version_greater_than_or_equals_to_current_version(current_version, version_folder):
        print("Skip Directory %s, current version %s" %
              (version_folder, current_version))
        return True
    return False


def is_folder_version_greater_than_or_equals_to_current_version(current_version, folder_version):
    '''
    If a folder doesn't follow x.y.z.n format, where x.y.z.n are numbers, the comparison would 
    still try to convert the folder to semantic versioning, and compare it anyways.
    NOTE that for semantic version abc <  1.0.0
    '''
    ok = True
    try:
        current_version = current_version.split('/')[-1]
        folder_version = folder_version.split('/')[-1]
        folder_version = ver.parse(folder_version)
        current = ver.parse(current_version)
        ok = folder_version >= current
        return ok
    except ValueError:
        print("Folder Name does not follow semantic versioning format. Would be included by default.")
    return ok


def listdir_fullpath(d):
    return [os.path.join(d, f) for f in os.listdir(d)]


def list_dir_that_meets_version_requirements(root, current_version, include_path=False):
    version_folders = []
    for name in os.listdir(root):
        if not os.path.isdir((os.path.join(root, name))):
            continue
        if is_folder_version_greater_than_or_equals_to_current_version(current_version, name):
            if include_path:
                name = root + "/" + name
            version_folders.append(name)
    version_folders.sort()
    return version_folders


def get_sql_file(directory, target_version):
    sql_files = []
    files_and_directories = sorted(listdir_fullpath(directory))
    directories = [d for d in files_and_directories if (os.path.isdir(d))]

    directories[:] = sorted([d for d in directories if (
        not skip(d)) and not skip_dir(d, target_version)])

    for dir in directories:
        sub_directory_files = get_sql_file(dir, target_version)
        if sub_directory_files:
            sql_files += sorted(sub_directory_files)

    sql_files += [os.path.abspath(i)
                  for i in files_and_directories if i.endswith('.sql')]
    return sql_files


if __name__ == '__main__':
    directory = "./db_migration_scripts"
    current_version = "4.30"
    subdirs = list_dir_that_meets_version_requirements(
        directory, current_version, include_path=True)
    print(subdirs)
