import re
import string_utils as utils
import pytest

testdata = [
    ('''
            CREATE DATABASE [$(DatabaseName)]
            ON  PRIMARY
            ( NAME = N'$(DatabaseName)_Data', FILENAME = N'$(DataPath)$(DatabaseName)_Data.mdf' , MAXSIZE = UNLIMITED, FILEGROWTH = 102400KB )
            LOG ON
            ( NAME = N'$(DatabaseName)_Log', FILENAME = N'$(LogPath)$(Database)_Log.ldf' , MAXSIZE = 2048GB , FILEGROWTH = 102400KB )
      ''',
     '''
            CREATE DATABASE [{DatabaseName}]
            ON  PRIMARY
            ( NAME = N'{DatabaseName}_Data', FILENAME = N'{DataPath}{DatabaseName}_Data.mdf' , MAXSIZE = UNLIMITED, FILEGROWTH = 102400KB )
            LOG ON
            ( NAME = N'{DatabaseName}_Log', FILENAME = N'{LogPath}{Database}_Log.ldf' , MAXSIZE = 2048GB , FILEGROWTH = 102400KB )
     '''
     )
]


def test_regex_c_sharp_variable_in_dollar_braces_match():
    ''' Match 1
            Full match	0-7	$(Test)
            Group 1.	0-7	$(Test)
        Match 2
            Full match	12-18	$(ABC)
            Group 1.	12-18	$(ABC)
        NOTE: Code was generated from https://regex101.com/
     '''
    test_str = '''ABCD $(Test)TEST
    	$(ABC)
        RETURN
    '''
    matches = re.finditer(
        utils.REGEX_SQL_VARIABLES, test_str, re.MULTILINE)
    for matchNum, match in enumerate(matches, start=1):
        print("Match {matchNum} was found at {start}-{end}: {match}"
              .format(matchNum=matchNum, start=match.start(), end=match.end(), match=match.group()))

    for groupNum in range(0, len(match.groups())):
        groupNum = groupNum + 1
        print("Group {groupNum} found at {start}-{end}: {group}".format(groupNum=groupNum,
                                                                        start=match.start(
                                                                            groupNum),
                                                                        end=match.end(
                                                                            groupNum),
                                                                        group=match.group(groupNum)))

    assert matchNum == 2
    assert groupNum == 1


@pytest.mark.parametrize("input, expected", testdata)
def test_convert_sql_variables_replace_c_sharp_variable_in_dollar_with_pathon_variable(input, expected):
    result = utils.convert_sql_variables(input)
    assert result.strip() == expected.strip()


def test_sorted():
    list = ["010 Assemblies", "000 CreateSchema", "a", "B"]
    newlist = sorted(list)
    print(newlist)


def test_match():
    assert re.match(r'\.\D', '.git') != None


def test_string_format_with_unused_variables():
    with pytest.raises(KeyError):
        test = "test {a}, {b}"
        test.format(b='value-b')


def test_string_format():
    text = '''select object_name(object_id('{ObjectName}'))'''.format(
        ObjectName="test")
    print(text)
    assert 'test' in text


def test_escase_sql_string():
    sql = "'te'st"
    result = utils.escape_sql_string(sql)
    assert "''te''st" == result
