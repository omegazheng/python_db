import os


def read_sql_statements(file, delimitor='GO\n'):
    statements = []
    if '.sql' not in file:
        return statements
    error_line = ''
    error_statement = ''
    try:
        with open(file, mode='r') as f:
            statement = ''
            for line in f.readlines():
                error_line = line
                if (delimitor.strip() != line.strip().upper()):
                    statement += line
                else:
                    if (statement.strip() != ''):
                        statements.append(statement)
                    statement = ''
                error_statement = statement

            if statement.strip() != '':
                statements.append(statement)

    except Exception as err:
        print("Error parsing file {}\n, line {}\n, statement {}\n, error {}\n".format(
            file, error_line, error_statement, err))
    return statements


if __name__ == '__main__':
    dir = os.path.dirname(os.path.abspath(__file__))
    test_file = os.path.join(dir, 'DeployObjects.sql')
    statements = read_sql_statements(file=test_file)
    for statement in statements:
        print(statement)
        print('-------------------------')
